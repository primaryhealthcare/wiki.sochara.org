---
title: SOCHARA CDs and DVDs
description: 
published: true
date: 2023-01-19T10:35:47.961Z
tags: 
editor: markdown
dateCreated: 2023-01-19T07:21:59.692Z
---

# SOCHARA CDs and DVDs

### Unorganized

- DVD-R, SONY. Says 16-4-16 on it. Contains 480 images from SOCHARA Silver Jubilee event, about 2.6GB in total. Backup link: https://www.idrive.com/idrive/sync/Archives/sochara/Silver%20Jubilee%20Event%20photos/2016-04-16

- DVD-R, SONY. Says 15-4-16 on it. 365 images from SOCHARA Silver Jubilee event, about 2.0GB in total. 

- SDFI - Chennai Presents Mezhugu a tamil short film make by stephen j msfs. Short movie in Video CD format. [mezhugu](/mezhugu)

- ReadIRIS Pro - software CD

- HP Laserjet - software CD

- From Community Health action to people's health movement - faulty

- விடியலைத்தேடி... சமூக விழிப்புணர்வு பாடல்கள் - faulty

- நலவாழ்விற்கான மக்கள் செயல்பாடு - video - https://www.youtube.com/watch?v=QNBhVpqPC3E

- ReMINdS - Resource material on Mental health, Injuries, Neuroepidemiology and Substance-use disorders for Public Health Community - contents include acts and policies, research, management guidelines, training resources, reports, conventions, WHO resources, etc related to the topic. [nimhans-reminds](/nimhans-reminds)

- Holy Cross CRHP - Satvidya - About [Holy Cross CRHP](/holy-cross-crhp)

- BITS Pilani - Masters in Public Health - Brochure and student profiles of 2011

- 47 CNN IBN feature on JSS & Binayak Sen - 20 minutes+ video

- Clean Your Act 45 min English - Centre for Science and Environment - likely https://www.cseindia.org/clean-your-act-8420

- Arogyada Hakku - songs of health rights of the people

- Clinical Procedure Unit - Dept of Essential Technology - WHO Geneva - 48 PDF files, 7 video files - Actual content is an HTML based toolkit "WHO Integrated Management for Emergency & Essential Surgical Care (IMEESC) Toolkit" - possibly the same as https://apps.who.int/iris/handle/10665/44884 - The disc is visibly damaged, but seems to work at least partially.

- "Experiencing Options" by International Services Association - just a 7.1MB, 82 page pdf of the same title. Says First Edition, 2003. Back up at https://www.idrive.com/idrive/sync/Archives/miscellaneous/INSA%20India

- Regional Meeting on Application of Epidemiological Principles for Public Health Action, WHO-SEARO, 26-27 Feruary 2009 - presentations from this meeting. 22 files, 52 MB - includes a presentation by Ravi Narayan on "The New Public Health Paradigm in Partnerships and Action" - Backup of this presentation alone is at https://archives.sochara.org/s/sochara/item/6

- Silver Jubilee Celebration Disc 3 - Navaspoorthi Kendra on 5-12-08 - video CD of 1 hour+ content

- Silver Jubilee Celebration Disc 1

- Sprays of Misery - possibly same as https://www.youtube.com/watch?v=xP6o9ghpdGc - Disc is in VCD format

- 2009 IPHU Videos Day 4 morning 1 - see [IPHU Bangalore 2009](/iphu-bangalore-2009) for contents

- 2009 IPHU Videos Day 4 morning 2 - see [IPHU Bangalore 2009](/iphu-bangalore-2009) for contents

- learning to survive a film by CHETNA support MacArthur

- CHC Bangalore Reviving Hopes Realising Rights - 28 minutes video RHRR

- Maanasi of sound mind - 26 minutes

- EQUINET - publications 1998-2009 Turning Values into Action: Equity in Health in east and southern Africa - 184MB files in pdfs, mp3s, etc. About 254 files. Publications by [EQUINET](/equinet)

- Analysing Health Systems to Make Them Stronger - Studies in Health Sevices Organization & Policy, 27, 2010 - possibly same as https://www.researchgate.net/publication/47798100_Analysing_Health_Systems_to_make_them_stronger - a few pdfs about 13MB

- Candles in the Wind - 32minutes+ video - related to disability

- Guide to Economic Evaluation in Health Promotion - Centre for Health Promotion University of Toronto

- Sexual and reproductive health publications and web site 2008 - WHO Department of Reproductive Health and Research

- Women and Disaster Campaign - Huairou Commission Disaster Brief (e-update) www.huairou.org info@disasterwatch.net

- research-matters.net Knowledge Translation 4 Bridging the `know-do` gap A resource for researchers November 2008 a work in progress

- research-matters.net Knowledge Translation Bridging the `know-do` gap A resource for researchers November 2008 a work in progress


### CD Tower without label

- Documentaries: A Healer is born, Dying for Drugs, Sama Can we see the baby bamp, SJ - Doctors & Medicines, Vandana Shiva - The Future of Food and Seed, where healing is a tradition - all in different different formats

- Community mental health and development program in South India - Basic Needs India Promoting mental health and development 2008-2011

- MSC unet plan - likely corrupted CD

- UNAIDS Library of current documents version 1.1 December 2001

- CD-R empty

- Sudhee Networks - We are 13 this year - doesn't look like a CD there're labels on both sides

- Moserbear - empty

- Training Program for Religious, Charitable & Educational Institutions STARTT Professional Training Program Series

- SOCHARA Annual report 2009-10

- National AIDS Programme Management - a set of training modules WHO

- Music 1 - lots of English songs

- Text Health information
  ICMI at district level
  'Integrated Management of Pregnancy and child birth'
  'Key District indicators for PHC'
  'Management of TB'
  'National AIDS programme management modules'
  'On Being Incharge'
  'RBM guidelines'


- SWF CHC Presentation - 16/03/2011 - Staff Welfare Fund - CHC - An introduction & financial analysis

- Tsunami Response watch - one stop humanitarian website - the actual website backup

- Long and Short Version - CHLP 2002 introduction video Long and Short

- 17-3-2016 to 22-3-2016 Street Play on Health


### CD Tower named "CHC backups"

- TADAKAL fact finding report - Radha & Team, CHC

- Dr Mohan Deshpande Visit to CHC songs, dance & photos

- CHLP Photos

- Photos

- Empty CD

- Moserbear - CHLP Newsletter May 2010

- CPHE Meet presentation

- JAAK Newsletter draft

- TADAKAL Fact finding report - Radha & Team CHC

- PPP - Banner & Logo

- Mitanian - 26-12-05?

- Report for mico - people's health movement report document couple of pages

- Two kids in rags? -  WBB trust PATH Canada supported by CIDA - Tobacco & Poverty - a few exe files

- SHRC - Mitanin final report - Dec 2005

- SHRC - Mitanin final report - Dec 2005

- Janas... a 4 page pdf

- JAA-K Logo - just the logo as a couple of PDFs

- 8 empty CDs

- JAA - K Health Denial Cases - Bidar - District CB Training Belgaum 18-10-2008 & NRHM Awareness Programme - BIRDS College 18-10-2008

