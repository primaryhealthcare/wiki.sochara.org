---
title: Institutional Murders
description: 
published: true
date: 2023-08-12T15:44:52.379Z
tags: 
editor: markdown
dateCreated: 2023-08-12T15:44:52.379Z
---

# Institutional Murders

This page will serve as a list of people murdered by institutions due to caste discrimination and other power hierarchies in India.

## 2023
* [Swapnadeep Kundu](./swapnadeep-kundu)