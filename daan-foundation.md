---
title: Development Action Awareness Nationwide: D.A.A.N. Foundation
description: 
published: true
date: 2023-02-16T06:42:26.582Z
tags: 
editor: markdown
dateCreated: 2023-02-16T06:42:26.582Z
---

# Development Action Awareness Nationwide: D.A.A.N. Foundation

> Here at the Development Action Awareness Nationwide, we are developing a program that work on issues related to awareness and education. We focus on youth aged 4 to 15 on issues related to gender, sexual reproductive health, differences in the body which come with the age and other social issues like caste, religion and gender. We work with the overall capacity-building of youth, our efforts manifest themselves in education, understanding and love, and the goal is to make independent individuals. 
> Our goal is also to make positive impacts on community projects driven by non-governmental organizations, to promote education and awareness among the youth and other community members. To develop an over all understanding of the youth with cross-cultural experiences. We invite all those who wish to explore rural India and wish to visit our villages and centers, we welcome all those who wish to learn from the beautiful countryside of this beautiful country. 
~ http://www.daanfoundation.org/about.html