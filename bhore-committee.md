---
title: Bhore Committee
description: 
published: true
date: 2023-06-23T06:05:57.465Z
tags: 
editor: markdown
dateCreated: 2023-06-23T06:05:57.465Z
---

# Bhore Committee


## Reports

### Volume I
Survey

* http://www.nihfw.org/Doc/Reports/bhore%20Committee%20Report%20VOL-1%20.pdf

### Volume II
Recommendations

* http://www.nihfw.org/Doc/Reports/Bhore%20Committee%20Report%20-%20Vol%20II.pdf

### Volume III

Appendices

* http://www.nihfw.org/Doc/Reports/Bhore%20Committee%20Report-%203.pdf