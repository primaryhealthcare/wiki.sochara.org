---
title: Rural Travel Fellowship
description: 
published: true
date: 2023-02-17T10:38:10.142Z
tags: 
editor: markdown
dateCreated: 2023-02-17T10:38:10.142Z
---

# Rural Travel Fellowship

A fellowship for doctors to travel and work in many rural hospitals over the span of an year.

https://ruralsensitisationprogram.org/thi-fellowship-program/