---
title: National Health Committees India
description: 
published: true
date: 2023-06-23T06:14:13.805Z
tags: 
editor: markdown
dateCreated: 2023-06-23T05:59:00.188Z
---

# National Health Committees in India

1. [Bhore Committee, 1946](./bhore-committee)
2. [Mudaliar Committee, 1962](./mudaliar-committee)
3. [Chadha Committee, 1963](./chadha-committee)
4. [Mukerji Committee, 1965](./mukerji-committee)
5. [Jungalwalla Committee, 1967](./jungalwalla-committee)
6. [Kartar Singh Committee, 1973](./kartar-singh-committee)
7. [Shrivastav Committee, 1975](./srivastava-committee)
8. [Bajaj Committee, 1986](./bajaj-committee)

References
* http://www.nihfw.org/ReportsOfNCC.html
* https://communitymedicine4asses.files.wordpress.com/2021/04/important-health-planning-committees-in-india.pdf
