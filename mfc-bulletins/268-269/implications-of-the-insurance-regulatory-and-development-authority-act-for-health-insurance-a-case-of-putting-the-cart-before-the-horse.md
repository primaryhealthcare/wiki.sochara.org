---
title: "Implications of the Insurance Regulatory and Development Authority Act for Health Insurance A case of putting the cart before the horse"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Implications of the Insurance Regulatory and Development Authority Act for Health Insurance A case of putting the cart before the horse from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC268-269.pdf](http://www.mfcindia.org/mfcpdfs/MFC268-269.pdf)*

Title: Implications of the Insurance Regulatory and Development Authority Act for Health Insurance A case of putting the cart before the horse

Authors: V. R. Muraleedharan

medico friend circle bulletin 268 269

5.

Jan-Feb. 2000

Health of Women Workers in the· Beedi Industry Meena Gopal

In conventional literature, the issue of occupational health and concern for workers' health and safety arises mainly in factory and industrial situations. However, nearly ninety per cent of the workforce in India is within the unorganised sector (Gopalan, 1995) where levels of technology are low, there are decentralised processes of production and conditions of work are deplorable. The workforce remains scattered and the system of contracting and sub-contracting, employing predominantly migrant labour, women and child workers prevails. Information on occupational health and within this sector is rather scanty. Within this sector, home-based production forms a substantial part. There has been a tremendous growth of home-based work in the last two decades as entrepreneurs, in order to reduce the escalating production costs within the factory sector, decentralise production by subdividing the entire production process into elements that need not be carried on at one place. Big companies sub-contract their work to smaller units or contractors who distribute the work to the workers who work at home according to the requirements of the employers and hand over the finished products to them (Pandey,1989), Home based work relies largely on female labour in comparison to the factory or shop where fewer women are employed, and includes a range of work from making agarbattis and rolling beedis to stitching and hemming for garment factories, preparing lace for export, packaging for pharmaceutical industries and preparing papads, masala, pickle, etc. for food industries and others under a sub-

contracting system (GOI, 1988). Thus the organisation and distribution of work within the home-based production offers less protection to workers, and leads to the nonrecognition of issues of occupational health of workers. . This paper highlights issues of occupational health of women workers within the home-based beedi industry through a case study of the beedi industry in a block the Tenkasi taluk of Tirunelveli district (Gopal, 1997). As home-based workers, women's occupational health derives from their conditions of work and their relations at work In such industries, while the impact of toxic products or hazardous raw material remains latent, what is ignored are the indirect risks to health or the conditions conducive to ill-health of workers. These risks remain unrecognised or undervalued while workers' own perceptions indicate their awareness of the causation of illhealth. It is these issues that are discussed in this paper. We do not attempt here to measure morbidity which requires a separate design and -tools but we use categories through which people deal with their illness by addressing the conditions in which they work and live, and the subjective perception of illness. The beedi industry: relations of production The beedi industry in Tirunelveli is nearly seventy years old and has today nearly 5.5 lakh workers working within it. The system of manufacturing beedis through contractors

SNDT College. Mumbai.

and sub-contractors is the one prevalent today. At the village level, in the 'company shops', the contractors for the trade-mark holding company operate by issuing passbooks to the home-based workers in their name, giving them raw material and collecting the beedis after paying them the wages, as well as all the benefits due to them. In the other type called 'commission shops', the principal or trade-mark holding company operates through a main contractor, who in turn hands over raw material to numerous subcontractors who maintain links with the workers issuing them raw material and collecting the finished product from them. The sub-contractors exercise arbitrary powers against the workers because of their weakened links with their principal companies, and the informality governing their relations with workers. As far as the legal aspects are concerned, the worker status of the beedi, worker is ensured if the company provided her with a Central Excise passbook, a log book, a service card and a leave card,' Two log books should be, provided to the worker by the employer in accordance with the Beedi and Cigar Workers (Conditions of Work) Act, 1996. The employers do not give log books to the workers but obtain signatures from workers for having received log books exploiting their illiteracy and vulnerable position. Most of the workers do not have any of the above documents to prove their worker's status. They cannot approach the Labour Officer for a non-employment case or claim minimum wage, maternity benefit and other privileges. The implementation of the Beedi workers Welfare Fund is formed out of the cess collected from the manufacturers out of the beedis produced and sold for provision of welfare measures such as scholarships for the school going children of the beedi workers, and assistance for some medical benefits. The workers tend not to gain' as they are not aware of their rights and claims, while the employers find it burdensome to actually obtain for workers what is their due and therefore benefit from their ignorance. At the level of the shops, varying production relations were maintained with the workers. Women are either employed as passbook holders; as 'joint' workers who did not have separate passbooks but rolled along with passbook holders with no benefits except wages; or were given raw material on a 'chittai' (little notebook). Usually young girls who were just beginning to roll beedis or older women whose beedi quality may not meet' the match of the best quality are issued raw material on a 'chittai' but without the recognition of a passbook. Women's disparate presence in the production process presented a fertile

1.

Personal communication with the Inspectress of Labour. Tirunelveli district, Tamil Nadu, June 1997

area for their further manipulation and exploitation in the working of the industry. Women's own limitations were the basic lack of knowledge of the outside world, their illiteracy, their feeling of lack of control of their worker status and their relationships at the shop. At the shops, women workers are in constant fear that the beedis they deliver may be rejected depending on the checking clerk's whim, or they may not be issued sufficient raw material for the following day as a penalty. Due to the brief time they spent at the shops, women hardly got to knew about the intricacies of the working of the shops and the dynamics of the larger industry. There was the unwritten yet exploitative practice of col1ect extra bundles in the system of 'podu vandal' which was unaccounted that the sub-contractor took as his own 'commission'. Then they did not permit workers to keep left-over raw material for long periods of time, practised quota in issuing raw material, and indulged in sudden closures, putting workers to great difficulty when they needed work most. Such practices by which the employers have an upper hand in terms of work and could control women's labour was only possible where there was no common site of work, where workers could come together, where there are no limits to the hours of work, and where remuneration is in terms of piece-rate work. Women on the other hand, despite losing out on many benefits, choose to roll for the commission shops, when entry into the company shops was not easy, due to the socio-economic condition of their families and the indispensability of beedi work in their struggle for survival. Since the process of production is split into many phases, it is divided between the actual workers and others in the household and neighborhood of the worker, who support her by folding, cutting the leaves, or bundling the beedis. Child labour becomes prevalent as children become assistant and younger girls are goaded into beedi rolling Another means of survival within this stressful situation was by evolving differing working relations with other 'women workers, such as borrowing and lending beedis, rolling for a multiplicity of shops with the help of assistance at home, and the beedi chit. "Through these mechanisms, while women sought to help one another, they unwittingly passed on the terms of exploitation onto one another and drove a wedge in their unity against employers. These ranged from having 'joint' workers rolling along with them if they were passbook holders; using younger girls as assistant and as proxy deliverers of the beedis at the shop; The Vatti (interest) beedi whereby women who could raw material which they sold to other women if they fell short, thus gaining from the short-changing practices indulged in at the shops. Unaware of the real reasons for which certain terms and

conditions are imposed on them, they try to compete with each other while perpetually depending on each other as well. The result is that their own labour process exercises divisiveness. Indirect implications of work on health

We explore below the conditions under which women work and live, including the routines of their day to day existence and the adjustments women make within it, apart from women's own perceptions about themselves and their work, and the community understanding of it. Within the household which is the workplace, Women manage their daily beedi work within tension-ridden routines making adjustments with respect to food, sleep, rest, leisure, other household work and time for their social responsibilities. These demands of their families and the expectations of the social milieu in which they live create high levels of tension which are by themselves conducive to illnesses, both mental and physical. Conditions that indirectly affect health or increase risks to the health of the women and which are also perceived by them as contributory causes to their ill-health are many. One of the chief risks within the production process is the direct exposure to the substance. Seated with quantities of tobacco in trays on their laps, women are constantly inhaling the pungent fumes while working. Mari had become allergic to the tobacco dust and had developed boils in her mouth. She had to spend Rs. 150 seeking a private practitioner in Tenkasi to continue with her beedi rolling. Another woman had to completely stop beedi work due to severe burning in her throat' and discomfort in her stomach. She lamented that nothing could be more unfortunate for her than giving up beedi work. The flavour of the tobacco also pervades the entire vicinity of work putting at risk not only the women but also other members of the household, children and even little babies, whom the women breast feed while working. Girls and women in keeping with the pressure from the shops and the demands within their own homes, delay their meals on return from the shops or skip meals while going to report, keep working for long hours in a seated position without rest, sustain themselves on coffee to ward off hunger, and sometimes, work into the late hours of the night to fulfill quota of beedi rolling. Improper food habits and subsistence on coffee most of the day does take its toll on the health of women. Indications from the responses of women are that a substantial proportion from the poorer and middle income households feels pressured to eat and manage with less food given the demands of their occupation and its working hours. Being seated constantly to roll except when having to go to the shop, makes them

hardly hungry enough to eat a meal during the day and so they keep consuming coffee. This soon becomes a regular practice where they are habituated to eating less and become unable to eat more. The pressure to work without rest or leisure is seen in a typical example of a woman working in the company shop: She hurries off to the shop in the morning when it is her time to submit the beedis. On her return she does not sit to eat but to roll the 10 odd bundles she's borrowed from the 'one who has to report a couple of hours later and so, it goes on till she breaks her fast at around 2 pm. Just before that she wets her leaves and makes them ready for cutting. After her meal of the leftover-rice-in-water she sits along with the other women and girls who gather near her verandah and begins to cut her leaves and roll. Like some women, especially of the poorer households, she does not care for food or sleep or rest. They sometimes just get up in the middle of the nigh: and roll beedis, are careless about eating their food, and work like an addict not caring for their health or well-being. Within the household, there would be demands from parents or husbands for their beedi incomes for expenses of the household or to save for dowry or jewellery. At home as women manage both their household work and beedi rolling, their lives are held taut by, a tension-filled routine. Many race against such a routine often at the risk of wearing themselves out. For instance, women who are widowed and lack sufficient control over the younger people of the house, work very hard, not keeping regular times for food and rest. Others are more resigned and opt out of such a tension, but have to lose out on many benefits that beedi money brings in. In one case, a woman said, "Beedi work makes one fall ill; one is often tempted to fall asleep feeling dull and lethargic and thus one may even fail to go to the shop! I am lenient with' my daughter as she is my only offspring, and I do not force her to roll for the company shop where it is difficult to keep timings and roll good beedis. If she rolls well for this commission shop itself, she can earn Rs. 120 per week, but we roll very little. When at the end of the week we get our wages we feel ashamed; others get in hundreds while we get in tens". In both cases, they lose out on something-the former, her health, and the latter, monetary incentive. Various adjustments are made by the women, which not only takes a toll on them but spills over to the other people especially the vulnerable in the household. In houses, where there are very young children, they crawl around and play with the raw material. Other women breast-feed their babies with the beedi tray on their laps. In certain households, work in the house as well as beedi

work gets divided among children and other older or younger women, and sometimes even men. Daughters sometimes cook the evening meals and monitor the fire, while their mothers go to the shop to submit the last beedis of the day, or they take the place of their mothers in reporting at the shop. Thus, other members too carry the burden of the beedi work on their shoulders and in the process of sharing the transferred costs, also share the risks with the women who are the main workers. Apart from the pains and acnes that their bodies earned, their

minds are the perpetual terrains of worry. "If worry remains, then where is the confidence you get from your work? Beedi work cats us... it makes you die quickly!" This remains a constant refrain of the women. One of the prime worries is the fear of the shop owner. The girls do admit that, "If we feel dull or tired of beedi work, we will put it away and sleep. People at home too do not say anything. But we cannot afford to do that often. Who will face the ship-owner? Yes, we have to be punctual otherwise he won't give us raw material. We are actually scared of him". The fear of the shop owner is rooted in the arbitrary practices that they indulged in at level of the shop. On Thursdays they could just refuse to give raw material saying that women have left-over raw material, which they have to produce as beedis the next day or he may just break their beedis saying they are not upto the mark. Even the women who have links with the union have no impact on the owners for whose commission shop they also rolled. The combination of the physical strain and mental worry and fear compounds their risks to illness. Women's inability to organise to resist and tackle the ship-owners arbitrariness and substitute their worry with confidence as workers can be traced to the inter-linkages between low self-esteem of women in the community and the dominant ideology of contempt for beedi work. Beedi work is termed as "women's work" by the men and none of the men do it to cam a living. Added to this, is the contempt for women's capacities itself. Even when they destroy themselves for others, the image of women being inadequate is retained by women themselves, "The birth' of a girl means ruin and destruction. Even if she is able to cam via beedi work after a while, it goes to another house". As one woman said, "as women we have to behave ourselves; we don't have to shout at or fight with the shop men. We go out with our men. I have no confidence in going out alone even to the PHC which is just less than a km away. I need him to accompany me". Direct implications of beedi work Women are very eloquent about the various losses that result from their beedi work: loss of sleep, lack of proper

food subsisting only on coffee most of the day, slow loss of one's health being afflicted by aches, pains and illnesses, loss of leaves that fall short in the raw material and lead to cuts in the expenses on health. The direct implications of beedi work are perceived and reported by the women in the form of symptom of ill health. The distribution of the responses regarding these groups of symptoms across the household categories tell us where the burden of the work organisation fails most and which household lose and which gain from the labour process in terms of health. Women perceive and express the symptoms, its occurrence and its causality in very specific terms and relate it to their work. Our explorations depend primarily on women's perceptions of illness and go beyond the technical notion of exposure to hazardous or injurious substances and processes of work, into social relations and conditions of life of the home based beedi industry. The various symptoms that are reported by the women have been categorised into symptom-groups (see Table 1l. There are thus four main symptom groups and one that is clumped together as 'others'. Of these main symptom groups, 'aches and pains related to beedi work' are 'essentially postural as women sit with bent backs for hours. Headache, burning of the eyes, pain in the legs, and numbness of the fingers are also a result of constant delicate work and concentration required. The next is 'coughs' which is related to the irritation due to exposure to tobacco (and poor eating habits according to the women l. 'giddiness', which includes breathlessness, is the other symptom-group 'stomach related pains include stomach pains, cramps gas, and spasmodic pains leading to diarrhoea. The last group of symptoms, termed 'others' includes piles, urinary burning, and white discharge, joint, pain, and swelling. Fevers, palpitation, wheezing and worry. Table 2 tells us that the aches and pains related to beedi work are the most prevalent responses (65%) and an' uniformly distributed across the well-off and the poorer households. Beedi workers perception of causality reveals that women are keenly aware of the causes of their symptoms. These could also be grouped into categories such as (1) being seated constantly and doing the rolling, lack of opportunity for resting the back,' trying to keep awake at night, exposure to the heat of tobacco and the hurried running to the shop to deliver the beedis; (2) poor eating habits such as constant subsistence on coffee to word off hunger and lack of timely eating of food; (31 insufficient rest due to constant exertion and heavy work are closely linked with their occupation; and (4) pregnancies and laparoscopic sterilisations. Women felt that in periods following these they have health problems. The category of 'other' reasons also include old age, blood

Table 1 - Symptoms Reported by the Women Beedi Workers

Symptom Groups .

Symptoms described Backache, neck ache, headache, burning of the eyes, pain in the legs

Aches and pains related to beedi work Coughs

numbness of the fingers Exposure to tobacco

Giddiness

Giddiness, breathlessness

Stomach related pains

Stomach pains, cramps, gas, spasmodic pains leading to diarrhoea

Others

Piles, urinary burning, white discharge, palpitation, wheezing, fevers, worry, joint pains and swelling

i

pressure; problem of nerves, and worry. The linking up of the causes to the symptom-groups not only reflects women's own awareness and their ability to articulate their situation but also the differences of awareness across economic groups. With respect to 'cough', a maximum of 62.1 % of responses from poorer households related it to beedi work processes (see table 3). Among the well-off households, it was only 23.1 % responses which related it to beedi work processes; and interestingly, -1:5.3% responses related it to poor eating habits and subsisting only on coffee, while 23.1% responses attributed it to weaknesses following pregnancies and laparoscopic surgery with the vulnerability to develop cough. In the case of 'giddiness', 50% responses from well off households related it to beedi work processes, with 16.7% also identifying improper food habits. In the poorer households, 35.3% responses identified beedi work processes and 26.5% responses attributed it to lack of rest. Women's perceptions .of causality indicates that while responses are maximum in relating the symptoms to beedi work processes with mild variation, other reasons like improper food habits, lack of rest and heavy work, followed by pregnancy related causes are also perceived by women. What is evident, however, from this articulation of causes is women's notion of the complexity of factors involved, and their

perception of the inter-linkages of health problems. Weak constitutions are built up by a combination-of a lack of rest, endless work, poor food habits, aggravatedin cases by pregnancy related reasons, and manifested in the course of their beedi work. What is interesting in the vivid perception of women of the relationship between their ailments and beedi work and lack of rest and pressures of work. Beedi making is not only a full time job unlike the popular perception that women do it in their spare time, but it also penetrates all possible and available time in women's lives at the cost of their health. References 1. Gopal, M. (997). Labour Process and its Impact on the Lives of Women Workers: A study of the Beedi Industry in Keelapavoor Block of Tirunelveli District, Tamil Nadu, Unpublished thesis, Centre of Social Medicine and Community Health, Jawaharlal Nehru University, New Delhi. 2. Gopalan, S. (1995). Women and Employment in India, New Delhi, HarAnand Publications. 3. Government of India. (1988). Shramshakti : Report of the national Commission of Self-Employed Women and Women in the Informal Sector, Department of Women and Child Development, New Delhi. 4. Pandey, D. (989). 'Problems of Home Workers under Domestic Outwork and Domestic Artisan Production', in B.B. Patel (ed.) Problems of Homebased workers in India, New Delhi, Oxford University press and IBH Publishing Company, pp. 59-73.

,

Table 2 – Women Beedi Workers Reporting Symptoms Symptoms Groups Aches and pains related to beedi worker Coughs Giddiness Stomach related pains Others Total responses

Women of well-off Households 61 (64.9%)

Women of Poorer Households 93 (65.0%)

7 (7.4%) 6 (6.4%) 11 (11.7%) 9 (9.6%) 94 (100)

16 (11.2%) 15 (10.5%) 9 (6.3%) 10 (7.0%) 143 (100)

Total Responses 154 (65.0%) 23 (9.7%) 21 (8.9%) 20 (8.4%) 19 (8.0%) 237 (100)

AN EPIDEMIOLOGICAL REVIEW OF THE INJECTABLE CONTRACEPTIVE, DEPOPFOVERA By C SATHYAMALA

Published by: Medico Friend Circle; Forum For Women's Health DepoProvera has recently been introduced into the Indian market. Yet protests by women's health and consumer groups have prevented its introduction into the national family welfare programme. In India unlike in the West the carcinogenic potential of Depo-Provera, although an important consideration, is not the central argument to oppose its introduction as a contraceptive. In the Indian context, there are in fact other

serious limitations that would also apply to women in other developing countries as well as to tow income and disadvantaged women from developed countries. The approval by the Indian Drugs Control Authority is linked to licensing by the USFDA in 1992. With the approval of the USFDA, it appears as though the last word has been said on the safety of Depo-Provera as a contraceptive. The review of literature presented in this monograph is to enable the reader to weigh the risks and benefits of the use of Depo-Provera as a temporary method of contraception.

Price:

India: Rs.1 00.00 Developing Countries: USS 5.00 Other Countries: USS 10.00

Table 3- Women Beedi workers perception of causes of symptoms C A USE Symptom Groups Related Poor to beedi eating work habits

S

No rest; constant exertion

Following pregnancies L.S.

Unable to Specify.

Total responses.

women of well-off house-holds Aches and pains elated to beedi (110) work coughs Giddiness

57 (58.8%)

4 (4.1%)

18 (18.6%)

6 (6.2%)

4

8 (4.1%)

97 (8.2%)

3 (23.10/0)

2 (15.3%)

3 (23.1%)

3 (23.1%)

1 (7.7%)

1 (7.7%)

13 (100)

6

2 (16.7%)

1

1

-

(8.3%)

(8.3%)

-

2 (16.7%)

(100)

-

-

(50%)

Stomach related

2

-

3

2

(42.8%)

(28.6%)

-

1 (9.1%)

1 (9.1%)

1 (9.1%)

2 (18.2%)

11 (100)

5

29 (17.9%)

17 (10.5%)

4: (2.5%)

10 (6.2%)

162 (100)

6

2

-

(20.7%)

(6.9%)

3 (10.3%)

(l00)

(28.6%) Other

6 (54.5%)

Women of poorer house-holds 97 Aches and pains elated to beedi A·59.8%) Work Cough 18 (62.1%) Giddiness Stomach related

12

12

(3.1%)

-

7 (100)

29

4 (11.8%)

9

5

3

1

34

(35.3%)

(26.5%)

(14.7%)

(8.8%)

(2.9%)

(100)

4 . (18.2%)

2 (9.1%)

8 (36.4%),

4 (18.2%)

13 (4.5%)

(13.6%)

22 (100)

9 (56.3%)

-

4 (25%)

1 (6.2%)

-

2 (12.5%)

16 (100)

•

Source (for both tables): unpublished Ph.d Thesis, Meena Gopal, ‘Labour Process and its Impact on the Lives of Women Worker: A study of the Beedi Industry in Keelapavoor block of Tirunelveli District. Tamil Nadu', Centre of Social Medicine and Community Health, Jawaharlal Nehru University, 1997.

Can Human Tuberculosis be Controlled in India Without Control of TB in Domestic Animals? Ashok Kale, Shailesh Deshpande, S.V. Gore.

In India the national tuberculosis control programme (NTCP) has been in operation since. 1962. Yet there is no sign of any success. The lacunae in implementation of this NTCP have been discussed by many authors. In our view, the issue is beyond just proper implementation of the NTC Programme. We argue that Tuberculosis in domestic animals has been an important reservoir of TB infection in India as in other countries, and hence unless we also control TB in domestic animals, we cannot achieve TB control in humans, even if we are able to implement the NTC programme efficiently and completely. This zoonotic aspect of TB has been surprisingly totally neglected by experts, despite the fact that control of TB in domestic animals (not only through pasteurization of milk) has been an important part of the TB control programmes since early twentieth century in developed countries: In part I of this article, we present some evidence as regards the role of bovine tuberculosis in human TB, and in part II, as regards inter-relationship between canine and human TB. We request readers of the MFC Bulletin to send their critical comments to us, as we plan to pursue this approach. A few studies including a study of M. bovis and M. tuberculosis with Polymerase Chain Reaction follow up human contacts and tuberculin test positive cattle etc. may be launched to clinch the issue. Let us work together on this issue.

Part-1

Role of Cattle TB in human TB

Bovine tuberculosis was one of the first animal diseases that I was demonstrated to be communicable to man . In the British congress of Tuberculosis held from 22nd to 26th July, 1901, in London, Dr. Robert Koch announced that the human subject is immune against infection with bovine bacilli, or is so slightly susceptible that it is not necessary to take any steps to counteract the risk of infection; Mr. Fadyean (1901) amongst others, opposed the view and a Royal Commission' was appointed to inquire and report with respect to tuberculosis. • Whether the disease in animals and man is one and the same. • Whether animals and man can be reciprocally infected with it. • Under what condition, if at all, the transmission of the

disease from animals to man takes place and what are the circumstances favorable or unfavorable to such 2 transmission . The report of the tuberculosis commission (in brief) states— 1. We prefer to regard these two types as varieties of the same bacillus and the lesions which they produce, whether in man or in other mammals, as manifestations of the same disease. 2. We must conclude that mammals and man can be reciprocally infected with the disease (tuberculosis). The possible danger to man through reciprocity in this sense was of course, the more important question presented to us and as we have conclusively shown that many cases of fatal tuberculosis in the human beings can be infected with the bovine type, even the pulmonary form of the disease in man being sometimes caused by the bovine tubercle bacillus. 3. The commission points out that transmission of tuberculosis from animals to man must obviously be mainly dependent on the susceptibility of any given human being to this disease and on the opportunities afforded to such animal for transferring its acquired and developed infection to the 3 human subject . Thus the work of the Royal Commission contributed greatly to our understanding of the epidemiology of tuberculosis.

Tuberculosis Control Programme in Western Countries (1) Control of reservoir and source in cattle: Following the report of the Royal Commission, all the western countries planned and implemented the programme of control of tuberculosis in cattle to destroy an important reservoir and source of tubercle bacilli contributing to spread of infection in human beings. Programme first required the detection of the infectious cattle. Only relatively small proportion of tuberculous cows can be detected by clinical method or microscopic examination of discharges. The' tuberculin test provides the chief method of diagnosis and is used in all schemes for the control of tuberculosis, It provides the chief method of control of TB. Whenever applied systematically, this

strategy of TB control depends on the application of the tuberculin test and the segregation of the reacting from the non reacting animals together with disinfection of the stalls 5 and cowsheds. The striking fact about primary tuberculous infection in man is that as a rule lesions heal, giving rise to the familiar nonprogressive globular little lesions of the primary complex. Hence a large proportion of the human population reacts to the tuberculin test but does' not spread infection. It is the person with bronchogenic phthisis of the 'reinfections type' who chiefly spreads the disease. In cattle the position is quite different. Only about 10% of British cattle would have reacted to the tuberculin test in 1952 but almost all of these had open though not acute, pulmonary tuberculosis- of the, 6 bronchogenic type. Therefore, considering the above statements a dictum can be formed i.e., 'All cattle reacting to tuberculin test are infected and almost all of which are infectious.'

(2) Bacteriogical methods: \

All the human specimens are cultured on to two slopes of Lowenstein-Jensen medium, one containing glycerol and the other 0.5% pyruvate which encourages the growth of 7 Mycobacterium bovis.

(3) BCG vaccination and detection of paediatric infections: Because of the equivocal result of a protective effect in the controlled trials conducted in the USA, that country has been one of the very few countries not to have instituted a routine 8 BCG vaccination policy. In the USA the Mantoux test with 5TU is used, whereas in the U.K. where -the national BCG programme complicates 9 the interpretation of skin tests, the Heaf test is preferred.

Results of Tuberculosis Programme in Western Countries

Control

1. Effect on tuberculosis mortality in children: The eradication of tuberculosis amongst the dairy herds was the chief reason for the disappearance of this infection in children. Another public health measure however, which contributed to this is the pasteurization of milk," 2. Effect on tuberculosis mortality -In general population: The 22nd annual conference of the N.A.P.T. was held in the conference Hall of the country hall, Westminster Bridge London, from July 16th to 18th, 1936. Sir Robert Philip (Chairman of the Association) followed

with some introductory remarks, in which he condemned the criticisms made of lavish expenditure in bovine tuberculosis administration without' adequate results and of apathy in regard to tackling the problem as a whole. The official figures showing the 75% drop in the tuberculosis mortality in the previous 50 years were enough for the 12 country.

Missing Links in the Indian Human Tuberculosis Control Programme. 1. Lack of Control program in cattle: The Bhore Committee Report of 1946 states that "at present, the bovine type of the organism is of no importance in the causation of human tuberculosis in India. There is however, need for vigilance. There is the possibility of the spread of the disease among cattle from existing infected animals, although they may be few in number. Imported 13 cattle may also prove to be a source of danger.” Based on the available information incidence of tuberculosis in cattle lingers around 5% to 10%. But there' is a distinct difference between the reactors and clinical cases. It is not necessary for the reactor animals to undergo a Clinical 14 course of disease though infective. These two statements collectively point out to the importance of bovine type as a source of infection on the epidemiology of human tuberculosis, 50 year after the report of Bhore Committee, a point which has probably been missed by our planners.

2. Aerogenous transmission from infected cattle: In India we have never paid serious attention to Aerogenous route of transmission of M. bovis. Man usually acquires bovine type of infection by the ingestion of contaminated milk, but a more dangerous infection is by the Aerogenous route, which usually occurs in the cowshed and gives rise to typical pulmonary tuberculosis. 15 There has to this day, however, been a surprising reluctance to accept that human pulmonary tuberculosis due to M. 16 bovis can likewise be transmitted by inhalation. In an investigation of 5021 cases of human tuberculosis in south-east England between 1977 and l979, 137 cases were found to be caused by M. bovis. Some of the strains were classical bovine seen mainly in Europeans. The others were Afro-Asian bovine strains and where seen, mainly in immigrants. There was no evidence that the infection could 17 have come from milk.

3. Isolation of M. bovis: The culturing of all samples in two different tubes is not practiced in India routinely. Therefore, the true prevalence

of M. bovis infection in India is largely unknown, thus hampering our efforts in the control of tuberculosis. 4. Paediatric Case Finding:

0.55°C frequently occurs. In addition, some loss of appetite and sudden onset may be the first sign of importance seen by owners.

Due to BCG vaccination to all newborn children, the tuberculin test has lost its diagnostic value. In this situation, routine-use of Heaf test is very essential for paediatric case· detection and thereafter "for taking appropriate and curative measures to decrease the morbidity and mortality in this age group constituting one third of our total population. Conclusion

Pathological Findings: The primary pulmonary complex in the dog and cat is most often complete and comparable to the Ghon lesion of children and it occurs most frequently in the lung.

Considering the above strategy and the success achieved by the western countries therein, a pilot research project may be undertaken locally to evaluate chiefly the role of Aerogenous transmission of tubercle bacilli from cattle to human, leading to pulmonary tuberculosis in humans, the results of which might influence our National Tuberculosis Control Programme.

PART II Table 1 summarises the known degrees of pathogenicity of the 3 principal types of tubercle bacilli for various species of animals (Cobette 1917). According to Cheyroollis' 592 cases of tuberculosis in the dog had been recorded in "the literature upto 1951. Of these 65.7%were infected with the human tubercle bacilli, 32% with bovine, and 2% with atypical bacilli. In Sweden, Hjarre, found human tubercle bacilli in 70% and the bovine type in 30%.2 Other investigators confirmed that in the dog the relative frequency of infection with M. tuberculosis to that M. bovis is approximately 3:3.3 Experimental work has revealed that the dog is equally susceptible to the bovine and human tubercle bacilli U.6. Feldman" speculated that the predominance of M. tuberculosis over M. bovis in spontaneous disease is dependent on fortuitous circumstance rather than on a difference susceptibility of the animal to one or the other of these two organisms. Herlitz reported two cases where the dogs almost certainly infected the human, having probably being infected themselves by human in the neighborhood". Urwitz described five cases of tuberculosis where one dog infected five humans'. Clinical Aspect: In the dog and cat the signs of tuberculosis are extremely variable. The thoracic form of disease is the most common type of tuberculosis affecting the dog. Early pulmonary tuberculosis in the dog can be accompanied by a short, husky, non-productive cough. Dyspnoea develops rapidly with progressive lung disease. Elevation of temperature over the normal end 38.6 +/-

Tuberculin Test: The tuberculin test in dogs and cats is notoriously unreliable8. In 1960, Mitchi described the use of BCG and, applied it intradermally for the diagnosis of tuberculosis calls the dog.10 Awad11, Luder 12 confirmed the value of this test and reported that in the dog intradermal BCG test as a diagnostic aid had marked advantages over the PPD test in terms of both sensitivity and specificity.

In 1955, a study was performed in Glasgow to decide whether household pets are overlooked as a factor contributing to the incidence in humans 13. The first part included examination of human in contact with the sputum positive humans. The first part of the result was based on the 14 tuberculous dogs and here the examination of just over half of the (34/60) human contacts produced nine patients who had tuberculous lesions in varying need of attention. The author concluded a veterinary diagnosis of tuberculosis calls for early and complete examination of the human in contact with the animals. The extra effort required to cover this small group would be negligible in terms of the probable yield of new cases of active tuberculosis or, perhaps more important, latent exacerbations of known 'inactive' lesions. The second part of the results was based on the investigation of the 20 dogs and 15 cats living in contact with 37 sputum positive humans. M. tuberculosis of human variety were recovered from swabs of alimentary tracts of two dogs and two cats. In Glasgow, the incidence of tuberculosis in morbid dogs was estimated as 2 per 1000 and in autopsies as 2:2percent. All tubercle bacilli recovered were of human origin. In another study in the year 1961 in Glasgow 14 out of a total of 70 apparently healthy animals that were in contact with active tuberculosis in human being were investigated; 48 by bacteriological methods and 22 by BCG testing. Among the 48 animals examined by culture of rectal and Pharyngeal swabs, human tubercle bacilli were recovered from 7 animals4 dogs and 3 cats. Eleven out of 22 animals tested with BCG..:-8 dogs and 3 cats had early reactions. The 7 animals in which specimen’s positive for tubercle bacilli had been found by culture were re-examined within 3 months and 14 found to be negative for tubercle bacilli. In the same study 14, 30 families in hotels having contacts with 31 dogs that died of tuberculosis were investigated. There was a marked reluctance on the part of most owners to submit either themselves or their families to

Table 1: Pathogenicity of types of M. Tuberculosis Type of Tubercle Bacilli Animal Species

Bovine

Human

Avian

Atypical

Guinea pig

++++

++++

0

0

Rabbit

++++

+

++++

0

Mouse

++++

+++

++++

0 or ++++

Hamster

+++

++

0

0 or +++

Anthropoids & Monkey

++++

++++

0

0 or ++

Goat

+++

+

+++

?

Horse

+++

+

?

?

Dog

+++

+++

?

?

Cat

+++

+++

?

?

Cattle

++++

+

+

?

Swine

++

+

++++

?

Parrot cockatoo

++++

++++

++

?

Domestic fowl

0

0

++++

?

'0' indicates that natural infection occurs rarely. Although temporarily progressive lesions may he produced by the infection of relatively large numbers of living bacilli. .

examination. Montoux testing of children was allowed in only two families. Despite these shortcomings it was generally possible to trace at least one person with an active lesion per animal. 354 persons of all ages lived in fairly close contact with the morbid 31 dogs during their life times. Among them, 41 persons had evidence of active tuberculosis. Considered as a whole, the contacts of tuberculous household pets constituted a group with an exceptionally high yield of tuberculosis. The researchers concluded "The question of whether the pet more often infected the humans, or the human the pet, remained open. But the investigation made it clear that the risk from the tuberculosis pet differed little from that from the tuberculous human. It was certain that even a partial examination of the accessible human contacts will produce at least one active case of the disease in every 9 human contacts examined."

Relevance of Tuberculous Dogs in Indian Setup

help of BCG test, the human contacts can be investigated. In the rural areas, dogs with symptoms of clinical tuberculosis can be' screened on similar lines and the contacts of positive dogs investigated further in Primary Health Centers. A Pilot project will be most appropriate by a Medico Veterinary Group. For correspondence: Dr. Ashok Kale D-17, Talajai Greens Society, Dhankawadi, Pune: 411043 PH.570874

References: Part 1 1. Kilpatrick G.S. I Chapter25 I Global tuberculosis. Theory and Practice of Public Health, 5th Edn. Hobson W. Pg. 338 Oxford Medical Publication. 2. Francis J.0958L Tuberculosis in animals and man. A study in comparative Pathology. Appendix to the report of the British Royal Commission, Pg. 319 Cassell & Co. Ltd. London.

In our country stray and pet dogs are very common. They move freely in all the social groups. As dogs are equally susceptible to human and bovine strains, they may be working . 3. Power W.H. (1911). The Final Report of the Royal Commission on Tuberculosis. 8M,] vol. 2 pg. 123. as source of tubercle bacilli in urban as well as rural settings. 15 Tuberculosis in dogs was treated successfully by Montry . 4. Francis J. (958). Tuberculosis in Animals and man. A study in 16 17 comparative path logy. Summary. pg. 103, 104. Cassell & Co. Ltd. Bignozzi and Collionot with INH, Streptomycin and PAS London. a few decades ago. With the (Contd. Oil page 12)

6.

The "Virus" Of Communalism: What Will Be Our Response? Ravi Narayan

In Germany, the Nazis came first for the communists and I did not speak up because 1 was not a communist. Then they came f0l" the Jews and I did not speak up because I was not a Jew. Then they come for the trade unions, and I did not speak up because 1 was not a trade unionist Then they came for the Catholics, and I was a protestant and so I did not speak up. Then they came for me, and by th-it time there was no one left to speak for anyone. Martin Niemoller 0892·1984) Recently, we have witnessed the re-emergence of a particularly virulent form of an old 'virus'- the 'virus' of communalism, with explosive outbreaks all over the country. The recent epidemic even reached pandemic dimensions, spreading rapidly to our immediate neighbours, then on to the Middle East and finally all the way to the UK as well. Bombay, among the most cosmopolitan of all our metropolises, also suffered a particularly vicious attack, the acute phase of which lasted for over 10 days. Since Independence, this 'virus' has been localised to a few endemic pockets showing sporadic outbreaks. \ However, in recent years it has seen a re-emergence with

greater severity,1/2 and the December- January outbreaks in 199 have proved beyond doubt that the virulent ‘virus’ is going to be with us for a long, long time. Much has been written about this 'virus' ill the recent past, but our knowledge of its socio-epidemiology is still relatively confused and our skills in its prevention are rather undeveloped. However what little i-s known is enough to establish that, if left .unchecked by concerted public health action, this 'virus" could well prove to be the greatest threat to the physical, mental and social wellbeing of the Indian people that we have had to face in the last few decades. For example, it is DOW relatively well-estalilished that: The 'virus' affects the minds of people especially the young, and makes them indulge in pre-meditated ants of' violence, especially directed towards innocent and defenseless people of communities and faiths perceived as different from their own. Epidemics thus create the double burden not only of deranged affected youth, but addi-

tionally, and more poignantly, of the innocent victims of these violent acts. It thrives- in urban pocket, particularly in over-crowded slims, affecting young people, who are pre-disposed towards violence due to unemployment, urban lumpenisation and alienation. While males are usually affected, the episode in Bombay has shown the disturbing trend towards female affliction as well. The 'virus' thrives in an unstable political climate and has its roots in religious bigotry, cultural fanaticism and ethnic chauvinism. While Germany saw a particularly virulent subspecies in the early part of the century, similar episodes have been seen in Iran more recently and in some states of Russia and parts of Eastern Europe as Well. The most disconcerting aspect of the available epidemiological evidence is that each epidemic leaves behind shattered families, devastated households, traumatised and scarred individuals, particularly women and children, and pushes whole communities into a vicious cycle of fear, distrust, anxiety, anger and deep antipathy. All this should be adequate evidence to jolt us out of out usual apathy and it is time we sat up and reflected on what we are going to do— each of us as individuals, each of us as members of an institution, and all of us as members of a national network, involved with the health' of the people. What will be our response to control or eradicate this 'virus’? I believe there are many possibilities open to us: • 'We could get involved in bridge building efforts between communities of all faiths and cultures wring educational efforts —both formal and non-formal. • We could initiate collective dialogues to build new attitudes, greater harmony and increasing trust and respect between different communities. • We could tackle prejudice, animosity and unhealthy stereotyping, by focussing our efforts on the youths and strengthening the value reorientation of our educational system.

Coordinator of the Society for Community Health Awareness Research and Action, Bangalore, Please also refer mfc resolution in Calcutta, 1993 (MFCH 192-193, MARCH-April, 1993). Source: CMJI. 1993; 18 (2): 9-11.

By doing this, we would have helped to create an environment that immunises minds against the effects of this 'virus'.

(Contd. from page 10) 7. Ibid, Ch. I. The Incidence of Bovine Tuberculosis, pg. 3.

Our efforts would have been focussed on primary preven- 8.Mitchison D.A.H972) Bacteriological aspects of mycobacterial infections. tion, that is, health promotion and specific protection. We BMJ. Vol 1, pg. 424. could get involved in pastoral and counseling initiatives at 7. Smitch P.G. (1994). B.C.G. vaccination. Clinical tuberculosis, 1st Edn. the community level, reaching out to affected communities Davies PDO, pg. 307. Chapman & Hall Medical, Cambridge. of patients and victims, providing a supporting hand to the 8. Litch A.G. (1994). Control of Tuberculosis, 1st Edn. Davies PDO, pg devastated; courage to the affected; comfort to the 315, Champman and Hall Medical Cambridge. . distressed, and various other supportive services that would 9. Textbook of Personal and Community Health 7th Edn. Diehl. H. Pg429. help families and communities to come to terms with the crisis and get beyond it. Our efforts would have been 10. Report of the Health Survey Development Committee (1946) Sir. Bhore, J. Vol. II, 'pg. 402. The manager of publications, Delhi. focussed on secondary prevention, that is, on early 12. Anbumani T. (1995). Current status of Mycobacterial infection in diagnosis and treatment. We could get involved in the active provision of the palliative and 'patch up' services that our institutions are now fairly renowned for, allover the country. Reaching out to victims of the acute epidemics, we could provide holistic care- primarily curative, but in an atM6spnere of concern and service and with sensitivity. Our efforts would then have been focussed on tertiary prevention that is disability limitation and rehabilitation. All the three levels of prevention - the sheet anchor of public health are urgently required and the challenge is getting greater, day by day. However, there is still another type of response, which seems to be unfortunately and inexplicably more popular than the alternative outlined above. It is a response characterised by the combination of the following types of reaction: • This is none of my business! • It does not affect me or my community! • I do not have time to do much, because I am so busy! • I have neither the skill nor the inclination! • This problem is not of my calling! • There are people better qualified and skilled to deal with it! • It is only a passing aberration! It is this response that allowed the 'virus' of caste ism to strike deep roots in the country. It is the same response that allowed Nazism to devastate Europe in the early part of this century. It is the same response that allowed apartheid to affect the mental health of generations of South Africans. It is the same response that for generations, and through the centuries, has allowed man's brutality against man. If left unchecked by concerted public health action, this 'virus' could well prove to be the greatest threat to the , physical, mental and social well-being of the Indian people. What will be mfc’s response? The choice will face us squarely in the days ahead.

•

livestock. Proceedings of the National Workshop on Mycobacterial infections in Livestock. pg.2. Dept. of preventive and social Medical, Madras Vetermaing College, Madras. 13. Francies J. (1958). Summary. 'Tuberculosis in Animals and man. A study in comparative pathology. Pg. 105, Casesell & co. Ltd., London.

14. Topley & Wilson's Principles of Bacteriology, Virologagy and Immunity. 8th Edition. Smith GR. and Easman C.S.F., Vol. 3 ch3.7, pg 99, Edward Arnold, London, 15. Christie A.B. (1987) Tuberculosis: Non tuberculous mycobacteria in Infectious Diseases. 4th edn. Vol1 Ch. 17, pg 504-Churchill Livingston, Edinburgh. 16. Venkatraminh P. (1995) Tuberculosis in Animals., Textbook of pulmonary and Extra Pulmonary tuberculosis. 2nd Edn., S. Satys Sri,. Ch. 46 pg. 192, Interprint.

Part II 1. Cheryrolls J.,: Etiology of Tuberculosis in the dog.' Thesis, Alfort., 1951. p79. 2. Hijarre. A. Dogs & Cats as Source of Tuberculosis in Man, Acta Tub. Scan. 1939. 13:103. 3. Feldman. Tuberculosis in dogs. J. TechnMeth.1942. 22: 49. 4.Mills. Experimental tuberculosis in dog: Primary Infections, Amer Rev. Tuber.1940. 42.28. 5. Feldman. The Pathogenicity for dogs of bacilli of avian tub. JAM Vet. Med. Assn. 1930.29:399. 6. Leon. The Pathogenicity of Photochromogens in dogs. ARRD. 1964. 90: 809. 7. Herlity Act. Tub Scan. 1939. 13:125. 8. Urwitz Act Tub. Scan. 1949. 23:211. 9. Hungerford. Diseases of Livestock. Page 854. 10. Michi, V.; BCG in the diagnosis of tuberculosis in dogs, Clio. Vet Milano, 1961. 84:180. 11. Awad F.I. Interadermal BCG test for the diagnosis of tuberculosis in dogs, Deutsch, Tierra, Wschr., 1962.69: 623. 12. Lander, I.M.; Tuberculosis in the dog and its relationship in infection in man. Proc. 17th World Vet Congr. (Hanover), 1963. 2: 1119. 13. Hawthorne V.M., Lander I.M., Jarrett W.F .H., Tuberculosis in Man, dog and cat, Brit. Medical Journal, 1957.2:675. 14. Hawthorne V.M. and Lander I.M.: Tub. In Man, dog and cat, ARRD. 1962. 85: 858.

•

"Implications of the Insurance Regulatory and Development Authority Act for Health Insurance A case of putting the cart before the horse V. R. Muraleedharan The Insurance Regulatory and Development Authority (IRDA) Act has finally come into being (from early December 1999). It allows, among other things, foreign and domestic private companies to enter into health insurance market. The passage of this Bill is considered by many as "a landmark of sorts", and is even claimed by a leading national Newspaper as "the road to true reform"; I t foresees and promises' several definite benefits and claims to have safeguards adequate enough to protect the poor in particular. Now, it remains to be seen whether and how far the expected benefits actually get delivered in a manner desirable to the "people" of the country - the word "people" must denote not just the insured, the rich and the poor patients, but all stakeholders. The purpose of this note is to initiate a discussion on this topic. The purpose is not to examine the various clauses of the Bill and/or to evaluate the likelihood of achieving the stated objectives and the constraints thereof. The purpose is two-fold: (a) First, to provide a (quick) summary of the major concerns of the various sections of the people including those of the public policymakers. This is given in the form of a series of questions. It is a kind of a check-list reflecting the various dimensions of the debate; and (b) Second, to put forward an argument that the introduction of this Act is a perfect example of "putting the cart before the horse” by our public policy making body. Most articles (including the popular ones) published during the last few years on health insurance in India raise one or more of the following concerns: 1.

Is it too early to let the private health insurance companies into the market?

2.

Will private insurance companies bother about the poor?

3.

Will the private insurance companies collude with private health care providers to maximize their profits')

4.

How and to what extent the private insurance companies be regulated? By what processes?

5.

What legitimate role (s) can patients play in running the system efficiently? How well can their interests be protected? Are they adequately informed about their rights and responsibilities?

6.

What role(s) can providers (namely the hospitals physicians and other health professionals) play? How well their legitimate interests be protected? Are they adequately informed of their rights and responsibilities?

7.

What do we do with the existing public insurance companies? Are they equipped to deal with health insurance market? Why cannot they deliver what we expect of private insurance companies? Why have they not yet done so?

8.

How do we ensure a healthy and fair competitive system amongst the insurance companies?

9.

Should and can costs of care be contained? Should and can premiums be contained and regulated?

10. How do we measure their performance? 11. What impacts they would have in setting standards of care? 12. Do we have the institutional capacity to regulate these companies? 13. What mechanisms will private insurance companies adopt to contain costs of care, and to ensure quality? How much of professional autonomy would they restrict? 14. How prepared are the private providers themselves? To what extent the objectives of the private providers and those of the insurance companies match? Would they ever match. 15. How does the Act enable us to fulfill our dreams of having a Panchayati Raj system? It need not positively enable us, but it should not be a hindrance to working towards our dreams.

The list could perhaps contain another dozen such questions. But the billion-dollar question is: Does anyone have any answer to any of these questions? As yet, we have had no private company with any experience in health insurance market to assess what might happen in future. But, we can only speculate and make some informed guesses. One need not fall from the top of a 50storey building to test whether or not he will break hi", bones! But there is one thing he must ensure if he insists on testing his hypothesis: he must not drag others with him. It is perhaps pardonable if he was going to fall with

Indian Institute of Technology, Madras, Tamilnadu 600036

others from a wall that is only one foot high. Throwing open the health insurance market to the private insurance companies is like this "mad-man" who wants to test the strength of his bones falling from the top of the 50-storey building. We have plenty of horror stories from the US managed care model to help us avoid many pitfalls. But a mistake of this kind, once committed, becomes almost unrectifiable, as their experience shows. This leads us to the second part of this note:

People's Health Assembly 2000 · (4-8

December

2000,

near

Dhaka,

Bangladesh) The global Health crisis The world is currently facing a global health crisis, characterised by growing inequities within and between countries. Despite medical advances and increasing average life expectancy, there is distributing evidence of rising disparities in health status among people worldwide. Enduring poverty with all its facets and, In addition, the HIVI AIDS epidemic and related problems are leading to reversals of previous health gains. This development is associated with widening gaps in income and shrinking access to social services as well as persistent racial and gender imbalances. From a number of countries in South Asia, sub-Saharan Africa, Latin America and Central and Eastern Europe there are reports of growing morbidity and mortality among vulnerable sections of the population, including indigenous peoples. Traditional systems of knowledge and health, as well as well-established, social systems in the North, are under threat.

Our policy makers have failed to ask a more fundamental question: Should health insurance not be dealt with separately from the rest of the insurance market? Health insurance is a must, in some form or the other. The question is: should it be open to the private insurance companies? Answer to this question requires serious reflections on "what should constitute our societies core values". It is out of these considerations that we can decide how resources and authority are distributed in the health sector, what types of resources and how much are to be made available, and how are they to be organized. Answers to these questions flow directly from societies core values. For example, a close look at the 1999 Draft Health Policy (of the GOI) shows no sign of any such principle' guiding their approach. We need principles (showing our commitments) to These trends are to a large extent the result of the distorted guide our actions and programmes. structure of the world economy, which has been further Let me illustrate: We can make a commitment that all skewed by structural adjustment policies, the persistent maternal and child-care will be the responsibility of the state. indebtedness of the South, inequitable world trade The state then can work on appropriate mechanisms for arrangements and uncontrolled financial speculation- all part providing them and set realistic goals. We are very good at of the rapid movement toward globalisation. In many countries setting goals (alternatively called "targets"), disregarding the these problems are compounded by lack of coordination principles. It is not therefore difficult to understand why we between governments, bilateral and multilateral agencies as never come anywhere near our own goals. Setting goals is well as expensive duplication of work among these different from pronouncing policy principles. We need to institutions. Within the health sector, failure to implement define them and constitutionalize them. These are more Primary Health Care (PHC) polices as originally conceived, fundamental, very complex and painful tasks. Reforming has significantly aggravated the global health crisis. These health care system should not be reduced to a mere deficiencies include. "mechanical exercise that consists of implementing a rational • A retreat from the goal of national health and drug plan to improve the effective use of resources. More policies as part of an overall social policy. fundamental issues are at stake than simply financial retrenchment". But evidently our parliamentarians missed the • A lack of insight into the intersectoral nature of opportunity to discuss these issues, and they are not aware health problems and the failure to make health a yet of the blunder they have made. They believe and want priority in all sectors of society. others to believe that they have found solutions to problems • The failure to promote participation and genuine that are bound to remain and get worse soon. This is yet involvement of communities in their own health another instance of "putting the cart before the horse" in our development. public policy making process. The IRDA Act is yet to gain some flesh and blood. The man is on his way to the 50th floor. He has decided to fall to carry out his grand experiment. How can we stop him or slow him down, or reduce the damage he is sure to sustain?

•

•

Reduced state responsibility at all levels as a consequence of widespread and usually inequitable privatisation policies.

•

A narrow, top-down technology-oriented view of health.

The Need For a people's health assembly There is now an urgent need to place health at the top of

the policy agenda. Past policies and practices need to be scrutinised and new broad-based visions formulated. Every effort is needed to regain the imperative that health, and health for all, is one of the most important goals for everyone to strive for. Governments and international organisations have largely failed to reach this goal, despite much rhetoric. Genuine, people-centred initiative must therefore be strengthened, both to find innovative solutions and to put pressure on decision-makers, governments and the private sector. Responding to these needs, a People’s Health Assembly (PHA) will be organised in the year 2000 by a group of concerned civil society

organisations and networks. The Assembly will bring together the knowledge and experiences of different groups and communities around the world with the aim of analysing and assessing these. It will identify the main problems, trends and challenges in order to develop strategies to achieve health for all in the future. The PHA is a broad, new initiative seeking to involve as many people as possible in formulating their own health agenda and setting their own priorities. People's long and rich experiences will be presented, discussed and translated into clear, practical and democratic policy guidelines. Alternative analyses of the root causes of the global health crisis will be stimulated. Strategies and alternatives for achieving the goal of health for all will be developed. The PHA will be a unique event. It is an opportunity for you and for everyone else who believes that the current health situation is unacceptable and that communities and civil society organisations must playa more important role. Through the PHA, you can be part of a world-wide effort to improve the situation and point the direction for the future. Goal The goal of the People's Health Assembly is to re-establish health and equitable development as top priorities in local, national an international policy-making, with Primary Health Care as the strategy for achieving these priorities. The Assembly aims to draw on and support people's movements in their struggle to build long-term and sustainable solutions to health problems. Objectives The following objectives will guide the People's Health Assembly process: • To hear the unheard. The Assembly will present people's concerns and initiatives for better health, including traditional and indigenous approaches. Their direct experiences of ill-health, its causes and possible

solutions will also be presented, discussed and analysed. Action plans will be worked out and refined. • To reinforce the principle of health as a broad crosscutting issue. There will be emphasis on the intersectional dimensions of primary health care and focus on health development, rather than health services. The problematic aspects of vertical, non-integrated programmes will be highlighted. • To develop co-operation between concerned actors in the health field. The importance of strengthening the links-between the different institutions and actors in the health field will be emphasised. Such revived and / or new partnerships will be built on the principle of equity and accountability between the parties. • To formulate a People's Health Charter. Based on thorough analyses of world health problems as well as existing policies and programmes, a People's Health Charter will be formulated. Concrete recommendations regarding policy and practice will be made to governments, international organisations, the business sector, nongovernmental organisation and people's movements. • To improve the communication between concerned groups, institutions and actors. Communication and networking among individuals, groups, organisations and institutions will be developed during the Assembly and sustained and strengthened thereafter. • To share and increase knowledge, skills, motivation and advocacy for change. During and after the Assembly, opportunities will be provided for indepth exchange of experiences and development of skills. The People's Health Charter will provide a base for advocacy, policy formulation and campaigns at the local, national and international levels. Coordinating group: Asian Community Health Action Network (ACHAN) • Consumers International Regional Office for' Asia and the Pacific (CI ROAP) • Dag Hammarskjold Foundation (DHF) Gonoshasthaya Kendra (GK) • Health Action International (HAI) • International People's Health Council OPHC) • Third World Network (TWN) • Women's Global Network for Reproductive Rights (WGNRR)

For More information, kindly contact. Janet-Maychin PHA Secretariat Consumers International Regional Office for Asia and the Pacific (CI ROAP), 250-A, Jalan Air Itam, 10460 Penang, Malaysia Tel: 604-229 1396 • Fax: 604-228 6506 email: Phasec@pha2000.org Website: http://www.pha2000.org or http://www-sph.health.latrobe.edu.aulpha


