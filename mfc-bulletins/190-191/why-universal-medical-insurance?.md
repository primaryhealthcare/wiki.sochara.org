---
title: "Why Universal Medical Insurance?"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Why Universal Medical Insurance? from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC190-191.pdf](http://www.mfcindia.org/mfcpdfs/MFC190-191.pdf)*

Title: Why Universal Medical Insurance?

Authors: Phadke Anant

190-191 friend circle bulletin medico January-February 1993

HEALTH CARE IN TAMILNADU Millie Nihila There has been a growing impression that Tamilnadu (TN) is in the forefront of medicare and related facilities. But the level and standards of health care provided in TN is much below than the other states. The basic purpose of the paper is to test this contention, this is done by going through the into following facts, " 1) The health status in TN: This is discussed first in terms of output namely, mortality and morbidity. 2) Health care facilities in TN in terms of inputs such as beds, doctors, primary health centres etc. Both output and input, the indicators of health are to be compared with Kerala and all-India indicators. This comparison will bring out the contrast and similarities between Kerala - which serves as a model in terms of health, and TN which is an advanced state in many other aspects. 3) Imbalance and inadequacies in the health care system: The imbalance with respect to spatial spread, systems

of medicines, medical/para-medical personnel etc. and inadequacies are measured in relation to (certain required) norms. In the course of this presentation it has been found that, when compared with all-India status in terms of health inputs and outputs, TN's health status is above the National average, however the actual difference between TN and allIndia average is only marginal. But when compared with Kerala, TN has a long way to go. This is being discussed in the course or this paper. A cursory glance at the statistics, relating' to medical and health delivery system shows that TN has made a very significant progress. But the actual condition in terms of health is portrayed below. 4) Health status of the population in TN The general measure of Crude Death Rate (CDR) or the number of deaths per 1000 of population in a particular period.

Table: 1 Crude Death Rate

YEAR 1980 1985 1989

TAMILNADU RURAL URBAN COMB. 12.4 10.9 9.7

8.3 6.9 6.8

Source: Sample Registration System.

11:2 9.5 8.7

KERALA RURAL URBAN COMB. 7.1 6.5 6.0

6.5 6.6 6.1

7.0 6.5 6.1

ALL-INDIA RURAL URBAN COMB.

13.7 13.8 11.1

7.9 8.6 7.2

12.6 11.8 10.3

Of putting Doctors 'Where There Is No Doctor'! Among the various alternatives put forth in the MFC papers in this and the last two issues. One thing that intrigues me (and possibly others) is the (non) availability of enough doctors to man the rural medical institutions/ centres. It is not easy to imagine that by financial/ administrative mechanism we can ensure availability of doctors for alternative plans. The Current and projected socio-economic trends are so strongly against villages that the law of upward mobility can be rephrased as law of mobility towards cities. Leave alone doctors, even ANMS are not willing to stay' in our villages. Education of children among many other factors is a prime thing that weighs against staying in villages. So I believe that any assumption for reasonable redistribution of doctors favouring rural areas (or villages to be more precise) is wrought with fatal defects. Let us count our options in redistribution of medical manpower: 1. Compelling urban Doctors to move into villages by certain phasing mechanisms (What are these?) 2. Compelling fresh medical graduates to serve in villages, combined with a reasonable infrastructure in villages to reciprocate this arrangement. 3. Retraining a section of 'lesser doctors' to the right use of modern medicine. 4. Prepare a general practitioners' course required for rural medicare services infrastructure and train only those undertaking a stipulated period of rural service.

It could be seen from the table: 1 that the CDR in TN is much higher than Kerala; but lower than all-India. In the year 1985 (Annual year book 1986-87, Dept. of Health and Family Welfare, GOI) Kerala was ranked sixth first in terms of COR, whereas TN was ranked sixth. The ruralurban dichotomy can easily be explained by better living conditions, (which includes the availability of health services) in urban areas and the socio-economic difference that exists in mortality levels of different group in both rural and urban areas. The CDR in TN has declined from 14.4 in 1971 to 9.7 in 1989; the corresponding decline for the country as a whole (from 14.9 to 10.3) has been somewhat slow. The mortality decline was also reflected in the increased life expectancy at birth from 47.5 (1961-71) to 60 (in the year 1981-85) for males and from 46.5 (1961-71) to 59 (in 1981-85) for females in TN. ,The improvement in life expectancy at birth for TN has hence been less than the national average which rose from 46.40 (for the year 1961-71) to 63.1 for the year (1981-85) for males and from 50 (in 1961-71) to 61.8 (1981-85) for females in India. (Where-ever Health Profile of TN is used as source of information, the source is not mentioned).

Expectation of life at birth (e) for Kerala was 70 for females and 65 for males, whereas for TN it was 57.8 for females and 58.3 for males in the year 1985 (Year book, 1986-87). Kerala was ranked first and TN was ranked 8th We do not have a fifth alternative : except that we have to in the year 1985 regarding. wait for the right socio-economic equaliser conditions that High (Crude) Death Rate is largely due to high Infant would favour shift of doctors towards villages. Mortality Rate. The IMR for TN, Kerala and all-India is The first option among these is nearly out of plausible shown in table: 2. action in the democratic setup. The second, third and fourth are increasingly plausible and Even in the case of IMR Kerala is ranked first and TN in better alternatives in that order. ranked sixth in the year 1985. From the table: 2 it could be seen that IMR in TN is more than twice the IMR in Kerala, The social organisation of medicare services (both private but little less than all-India. It could be seen from table: 2 and government) is 'stratified and clustered' with the urban that IMR for the State is 68 in 1989 which is not above the stratum concentrating most of the modern medicare National figure of 71, it is still much lower than the Kerala doctors, the small town/bazar village level sustaining figure 21. It may be noted that IMR difference among majority of the rural doctors (mainly non-allopaths), rural-urban areas are striking. leaving a real average village without doctors. A reduction in the infant mortality rate can lead to a Unless we have a pragmatic solution towards reasonable reduction in the birth rate for a population, apart from its redistribution of doctors, all our suggestions, I am afraid intrinsic merit in saving the lives of infants. will be too futuristic; though I doubtless agree that we need Kerala is ranked first and TN second in the case of CBR for the year 1985. to think in terms of macro plans for medicare.

Table: 2 Infant Mortality Rate Kerala

Tamilnadu Year

Rural

Urban

Comb.

Rural

1980

103

64

93

1985 1989

95 80

53 43

81 68

41 32 23

All-India

Urban

Comb.

Rural

Urban

Comb.

34 30 15

40 31 21

124 107 98

65 59 58

114 97 71

Source: Sample Registration System. Morbidity Comprehensive and reliable data on morbidity is not available, for proper precept ion of morbidity one should have information on the frequency and duration of illness, its effect in terms of deviation from normal terms confinement to bed, hospitalisation activity, diet, socioeconomic background of the patients etc. With regard to

morbidity, information is available from National Sample Survey Organisation (NSSO) and on Institutional morbidity in rural areas in terms of new cases treated in Primary Health Centres (PHCs) and rural dispensaries. Here we have referred to the NSSO 28th round (1973-74).

Table: 3 Incidence And Prevalence Rate Of Morbidity By Sex

IR

PR

Rural

Urban

Rural

Tamil Nadu Male Female Total

20.18 21.10 20.65

21.08 20.93 21.01

34.62 33.34 33.99

Kerala Male Female Total

39.25 35.49 37.29

32.28 35.22 33.77

74.59 67.98 71.21

63.23 60.44 61.84

India Male Female Total

13.53 11.55 12.57

13.87 13.04 13.53

24.11 20.70 22.46

23.39 22.06 22.77

Notes for Table: 3

.

Incidence rate (IR) measures toe frequency of morbidity commencing during the reference period and Prevalence Rate Measure (PR) measures the morbidity already prevailing. Source : 'Sarvakeshan' July-October 1980. NSSO 28th round (1973-74)

Urban 31.62

32.35 31.98

From the table: 3 it could be seen that morbidity is higher in Kerala both in terms of IR and PR It is also found that IR and PR in rural areas are more compared to urban areas. Rural females and urban males are found to have higher rate of morbidity. TN, though it stands above in all-India average with respect to output indicators of health it is not much encouraging.

It is always below Kerala, our neighbouring state, the difference is too vast. With high levels of GDR, IMR, and lower in Tamilnadu, it is shown that it has to go a long way to reach health levels of Kerala and norms set by the World Health Organisation (WHO). Though it is not a 'medical' or technical issue, larger socio-economic/ developmental issues are related to it: purpose of this paper is not to go into these complexes issues; but an idea of the

the role played by some issues like water supply, sanitation etc. can be had if one goes into the 'causes of death' etc. Causes of infant deaths It is possible to analyse the causes of mortality among infants based on the information provided in the 'Survey on Infant and child Mortality, 1979'.

Table: 4 Causes of infant deaths, 1978. Broad Category

TN

of causes

Kerala

India

Rural

Urban

Rural

Urban

Rural

Urban

2334 (18.9) 2164 (17.6 ) 1411

361 (5.7) 1674 (26.6) 156

578 (13.1 ) 398 (9.01) 85

127 (4.6) 232 (8.4) -

1248 (8.95) 1622 (11.6) 1980

(11.9) 939 (12.0) 530

(11.5)

(2.5)

( 1.92)

( 14.2)

(6.77)

808 (6.56 )

433 (6.87)

704 ( 15.9)

654 (23.6 )

2707 (19.4)

966 (12.4)

401

389

109

500

233

6. Others

(3.26) 5201 (42.2)

(6.17) 3287 (52.2)

2653 (60.1 )

(3.94) 1646 (59.5)

(3.59) 5887 (42.2)

(2.98) 4223 (53.4)

7. All cases

12319

6300

4418

2768

13944

7825

(100)

(100)

(100)

(1 00)

(100)

(100)

1. Causes related to

to maternal ill-health 2. Waterborne infections 3. Tetanus

4. Respiratory infections 5. Mosquito-borne infections

/

Notes for Table: 4 I. Broad categorization of illness is as follows: Waterborne infections: dysentery, diarrhoea, gastroenteritis, Mosquito-borne infections: Malaria Respiratory infections: bronchitis, inl1uenza, pneumonia, infantile fever and other disorders of respiratory system. Causes related to maternal ill-health: Prematurity, congenital malformation. 2. Those without' entry do not figure in the top ten causes. 3. Figures in breakets 'give column percentage. Source: K. Nagraj, 1986 based on Registrar General, 1981.

Prematurity and congenital malnutrition constitute major

'

-

causes of death, particularly in rural TN where the IMR due to this cause in nearly four times as high as in rural Kerala and, nearly two times as high as in rural India. This may be due to the poor he...... of mother during pregnancy. Infant mortality, due to tetanus is also very high in rural Tamilnadu. Though it is lower than the rural India, it is 16 times the rate in rural Kerala. The waterborne diseases are major causes of infant deaths in rural and urban Tamilnadu, it is higher than the all-India rate and the contrast is striking when compared to Kerala. This type of classification helps us to know the causes of IMR and this could be rectified, for they are mostly exogenous environmental factors. Information on 'Causes of death' reflects the health status of a community and in turn provides rational basis for health planning. Information on causes of death is useful to determine priority intervention in health care.

'

Table: 5 Percentage distribution of death by major cause group 1981 and 1986 for Tamilnadu and India

1981

Major causes

TN 1. 2. 3. 4. 5. 6. 7. 8. 9.

Senility Coughs (Disorders of respiratory system) Causes peculiar to infancy Disorders of the circulation system

Fevers Other clear symptoms Digestive disorders Disorders of the central nervous system Child birth & Pregnancy

27.3 11.2 8.3 14.5 '8.8 6.9 7.7 6.8 1.5

1988 India

TN

Kerala

22.7 20.3. 9.8 10.1 7.9 8.8 6.5 4.6 0.8

20.0 11.7 7.1 20.0 10.7 7.9 6.7 7.1 0.8

13.8 17.2 1.7 17.0 2.3 15.5 1.7 21.7 -

Source: Health profile of Tamil Nadu P.31, 1981. For the year 1989, Survey of Causes of Death (Rural). Annual Report, 1988, Registrar General of India.

Table : 6 Medical Certification of Causes of Death in Tamilnadu (Institutional death as per ICD) (Percent)

Cause of death Section and parasitic disease Disease of the circulatory system Disease of digestic system Certain condition originating in the prenatal period Symptoms, signs and ill defined condition Accidents and poisoning

1981

1986

19.94 18.68

16.5

6.46 12.73 6.84 9.10

29.50 5.11 1.85 12.83 9.56

Source : Health profile of TN Based on the surveys conducted by SRS it could be seen that in TN the leading causes of death are due to Circulatory disorders and senility. A lower proportion under disorders of the respiratory system is noticed when compared to the National figure.

is more prevalent. Causes of this is yet to be investigated and this is beyond the scope of this paper.

Morbidity statistics are in fact replacing mortality statistics as an index of sickness and health as it throws light on the health status of the people. May be different indices of From tables 5 and 6 is seen that generally both in rural and morbidity are possible. urban area the death due to disorder of circulatory system

Table 7 : Morbidity By Type Of Aliments. (Prevalence Rate)

" Ailment 1. Waterborne infections Viral 2. Infections Respiratory 3. Infections 4. Accidents 5. Others 6. Not recorded 7. All ailments

Kerala Rural Urban 5.39 1.88 9.25 0.24 11.37 0.02 20.89

5.05 2.13 9.11 3.11 20.10 21.99 1.84

Tamilnadu Rural Urban 3.58 2.31 2.83 0.70 17.65 7.70 33.99

2.44 2.35 2.30 0.58 16.60 6.96 31.98

India Rural Urban 4.27 1.02 4.14 0.8 8.27 4.18 22.46

3.26 1.28 3.95 1.01 8.51 4.71 22.77

NOTE:

The broad classifications of ailments are: 1. Waterborne diseases : diarrhoea, dysentery, diphtheria, malaria 2. Viral infections: Measles, mumps, smallpox. 3. Respiratory infections: Influenza, whooping cough and pneumonia Source: 'Sarvakeshan' July-October 1980. (NSSO 28th round, 1973074) ‘Others and not recorded' account to 72 percent of ailments

in TN. Other than this, waterborne infection is less prevalent in TN compared to Kerala and all-India. The most widespread diseases' by human feces are the intestinal infection and cholera. These spread easily in areas without safe community water supply and good hygienic practice. Though meaningful and accurate information are not available regarding the incidence of morbidity, other than NSSO survey, one can know morbidity rates for individual diseases on the basis of data generated by various special programmes taken up by the state to eradicate communicable diseases. Notification of communicable diseases is primarily meant for preventive control and is source of morbidity data. Except for small pox, plague, hemorrhagic fever, cerebrospinal fever, typhus and scarlet fever, the other diseases like malaria, cholera, filaria, tuberculoses, sexually transmitted disease, trachoma and leprosy occur sporadically in TN. Some special problems of TN are cholera, endemic malaria, trachoma and leprosy. Among children diarrhoea, malnutrition and deficiency of vitamin A are principal cause of morbidity. Anemia affects more than half of TN's pregnant and nursing mother. To a very large extent, these and other infective and parasitic diseases have their origin in environmental factors and malnutrition’s. Their impact was highest on the poor who were most exposed to these conditions.

(Tamilnadu Economy, MIDS, 1988). The decline in the overall mortality is generally attributed to the control of major, epidemic 'Killer' diseases such as plague, cholera, smallpox and malaria and the improvement in the living conditions, including the availability of food and curative services. The increase in the life expectancy at birth is due to the reduction in the IMR and to various disease control programme implemented at the National and State level. To sum up, TN is not among the advanced states in India as far as its health status is concerned. Its de rate and IMR are not much lower than the nation’s averages. The health performance of TN measured in terms of output can, therefore, be described as only 'mediocre' (Guhan S, 1981). The attack on mortality in Tamilnadu, as in India, has to concentrate on reducing IMR, and more so in rural areas. At present, in TN, higher priority is given to maternal and child health services, Immunisation against childhood infection, preventive health care against the control of water and air borne diseases and communicable diseases (2). Health Care Facilities in Tamilnadu Having assessed the health status of Tamilnadu in terms of output indicators, we in this section will (a) study the availability of health care infrastructure in Tamilnadu and (b) study the development of infrastructure.

Distribution of Medical Institutions in Tamilnadu (3) Based on the statistics of number of hospitals and beds are available from published sources, it was found that Table : 8

Type of Medical Institutions

urban areas, as any where else, are better with regard to the services of health care. This is shown in the following table.

Distribution of Medical Institution TN 1970-71

No of Inst.

1980

Pop. Served Per Inst. In 000’s

No of Inst.

Pop. Served Per Inst. In 000’s

333.05 48.13 81.26 75.82 33.01

99 129 668 383 1279

327.84 56.97 72.47 84.74 30.41

Hospitals Rural Urban Dispensaries S’s Total

85 259 419 379 1142

Source: Compiled from Annual Statistical Abstract of Tamilnadu and Census of India, Population Reports. 1971 and 1981 In the table : 8 we have calculated average population served by the medical institutions in TN. There has been a fall in the average population served by dispensaries and rise in the PHC between "1970-71 and 1980. In 1990-91 there are 1357 PHC's in Tamilnadu each serving a

population of 27400 on an average. In the year 1991-92 there were 1505 PHCs in the State. Since similar data for Kerala and India is not available to us, from the census reports of 1981 we have obtained the following information presented in table: 9.

Table: 9 Availability of Services of Medical Institutions Average No. of Villages and Population served by medical institutions India Villages : Inst. Including hospitals Inst. Excluding hospitals 2. Population Inst. Including hospitals Inst. excluding hospitals Percentage distribution of villages having medical amenities : Average population of villages having medical amenities:

Kerala

Tamilnadu

11.1 13.1

0.2 0.2

4.9

8707 9990 6.28

2835 3021 89.18

8871 10464 16.87

2779

14958

3908

1.

5.7

Source: Occasional paper I of Census 1986. When one goes through on the basis of services rendered by medical institutions it is seen that a medical institute in Kerala serves 0.2 villages whereas in TN a medical institute serves nearly 5 villages compared to India which

serves 11 villages if hospital is included and 1 3 villages when hospital is excluded. The average population served by a medical institute is more in TN thrice the number of people served in Kerala.

When more population more population is served by the Even when data from local bodies are not included an average bed in Kerala serves only 631 persons which is 50 institute less efficient it lends to become. Less than onesixth of the villages in TN have access to medical amenities. percent more than India, twice the number of 4 persons served by a bed in TN. In part, this is due to the fact that Actually Bed Occupancy Ratio (BOR) would have been a TN has a smaller proportion of hospital beds under nongood index to measure the hospital services. Though the government Voluntary Organisation (VOs) typically 'statistics of number of hospitals are available, the other manage smaller and more dispersed institutions. The information like average number of days stayed by a proportion of beds under VOs is only 20 percent in TN as patient in the hospital etc. are not available. Hence BOR against 27 percent for all-India and 42 percent for Kerala. could not be calculated. But we have tried to estimate the Distribution of health service by medical personnel is brought out by an assessment of medical and paramedical population served by a bed in table: 10. personnel available for per lakh of population. Table : 10 Population served per bed No. of beds

Population Served

Kerala Tamilnadu India

43533 44263 536224

Note: Kerala excludes local bodies data

· ·Ratio has been worked out on the projected population to which the data relates.

631 1174 1368

Sources: Health Information India, 1986

Table : 11 Medical and Paramedical personnel in Tamilnadu - 1961 and 1981 1961

1. Medical personnel a. Others indigenous system : b. Physicians & surgeons

- Allopathic c. Physicians & surgeons - Ayurvedic 2. Paramedical personnel a. Others : b. Nurses c. Midwives and health visitors d. Vaccinators

1981'

Rural

Urban

Total

Rural

Urban

Total

7917

10664

18581

8933

20437

29370

(43) 6664 604 (15) 649 (47) 7000 (31 ) 2637 (33) 1003 (16) 2254 (51)

(57) 6491 3430 (85) 743 (53) 15755 (69) 5414 (67) 5094 (84) 2136 (49)

(100 ) 13155 4034 (100) 1392 (1 00 ) 22755 ( 100 ) 8051 (100 ) 6097 (100 ) 4390 (100)

(30) 5337 2224 (13) 1372 (51) 17840 (35) 9895 (36) 4229 (27) 3034 (54)

(70) 4806 14335 (87) 1296 (49) 32444 (65) 17547 (64) 11659 (63) 2581 (46)

(100) 10143 16559 (100) 266lS 50284 (100) 27442 (100) 15888 (100) 5615 (100)

1106

3111

4216

682

657

1339

(26)

(74)

(100 )

(51 )

(49)

(100)

Notes : Figures in parenthesis gives the percentage distribution between rural and urban areas. Source: Census of India, General Economic Tables, Madras (1961) and Series (20). 1981

(100)

Table : 11 is self explanatory. One could see from the table concentration of medical manpower in urban areas. We would like to highlight that in 1961 only 8 percent of the total medical manpower were allopaths and they were living and serving in rural areas; this has increased to 25 percent in 1981. Most of the 'doctors' in rural areas are thus indigenous medical practitioners. By 1981 the total number of medical practitioners had increased by some 70 percent in the state, the numbers of allopaths were fourfold; and Ayurvedics doubled but that of other indigenous practitioners declined. Out of the total number of doctors, only 30 percent were in rural areas; the extent of concentration in urban areas has drastically increased in the last 20 years. There is only one allopathic doctor for 40888 of rural population in the year 1961 and one allopathic doctor for every 14600 rural populations in the year 1981. Though there is a fall in the percentage of indigenous systems in rural areas between the years 1961 and 1981, they still account for 60 percent of rural medical practitioners. With the increase in allopaths in rural area, indigenous doctors are of less in demand. Table : 11 also reproduce the figures for the years 1961 arid 1981 paramedical personnel. Of the total paramedical sector, 70 percent were in urban areas in the years 1961. In 1981 it has fallen to 65 percent. In 1961,' nurses accounted to 27 percent of paramedical personnel, only 16 percent of the total nurse populations were found in the rural areas of the state. Midwives and health visitors who were mostly female contributed 19 percent to total paramedical population. An important thing to be noted here is that

51 percent of them were in rural areas and they constitute 32 percent of all the paramedical personnel in rural areas, in both categories the share of rural areas has increased, considerably, but is still much less below the proportion of population living in rural areas. In order to find out the relation between existing medical and paramedical personnel, we have calculated the coefficient of correlation. (Table: 12)

Table : 12 Co-efficient or correlation between medical and paramedical personnel in 1961 & 1981

Co-efficient or correlation

1961

1981

Urban Rural

0.23 0.75

0.69 0.79

There is a sharp increase in the urban co-efficient of correlation and marginal increase in rural coefficient of correlation also between medical and paramedical. This implies that the number of medical and paramedical manpower available for one lakh of population is closely related (for it is positive). Compared to urban areas, it is in rural areas the relation between medical and paramedical personnel is closer.

Table: 13 Population served by a doctor and a nurse in 1985 Nurses

Doctors

Kerala Tamilnadu India

No.

4248 6954 63084

Population Served*

No.

Population Served

6470 7470 9258

912 579 9598

14251 35644 297228

Note; *Ratio has been worked out on the projected Again it is Kerala which does a better service in terms of doctor and nurse for it serves fewer people, which implies population to which the data relates. that more medical and para-medical personnel is available. Source: Health Information India, 1986

Statistics of patients treated in hospitals of Tamilnadu

Trends in health expenditure and its composition

Statistics of patients treated in hospital would be useful 19' discuss the functioning of hospitals in terms of services rendered to the community (ie., availability of medical

Expenditure on health is one of the principal inputs which help us to assess the health care facilities in Tamilnadu. Our main source of data is from budget reports. The budget expenditure includes both plan and non-plan expenditure, this comprises of medical, family planning and public health (public health includes water supply and sanitation).

facility) and the incidence of morbidity.

.

Patients treated in a hospital 'include both outpatients and inpatients treated in the hospital. Based on the indices calculated from the patients treated in the hospital it was found that the total number of patients treated in government allopathic institutions has increased at compound growth rate of 8.03 percent, while the population has increased at a compounded rate of 1.51 %. That is to say, the increasing proportion of population has resorted to medical care institution under public sector. If we include also the patients treated in private institutions as well as the indigenous systems of medicine, the number would be much larger. This does not appear to reflect a decrease in the rate of morbidity in the state, despite an increasing proportion of its resources being expended on health sector.

Financing Health Care Financing health care of services has received little attention in this country as well as in our state - very little meaningful data is available on the subject. The main features of the existing situation regarding the functioning of health services in our country may be briefly stated : social services as a rule, receive step motherly treatment in our system. Among 'social service', health has a far lower priority and the total public expenditure on health services of all type were even less than that on education. The present total expenditure (private and public) about 2 percent of GNP only. Within health, the large bulk of our expenditure was incurred on curative services, in other words the largest chunk of beneficiaries were from the top 20 to 30 percent of the population. This part documents and analyses the financing of expenditure in the Government health sector. Although a complete picture of resource for health care would include both the private and public health revenue and expenditure, one is deterred from dealing with that aspect by the complex nature and by the inadequacy of data on the subject. Both these problems afflict the study of Governmental health finance as well, though to a lesser degree. Nevertheless, an effort is made here to use available information.

For our analysis we have taken into consideration the year 1970-71 to 1990-91, for figures earlier to 1970-71 are not available. In order to' get an accurate picture of government's 'commitment' to health, it is essential to examine the proportion of health to total (plan and non-plan

expenditure. The Table : 14 gives us the statistics VI revenue expenditure of our state on health .and its subsectors. It can be seen from this revenue and expenditure that health expenditure accounted to 8.4 percent of total revenue expenditure in the year 1970-71. The proportion of money allotted to this sector was maximum - (13.9 percent) in the year 1983-84. .These statistics reveals that health sector enjoys some priority in terms of development infrastructure of the state. It is felt that the regarding the low share of family welfare in health sector, may be due to the large assistance it' gets from the central government mostly in terms of centrally sponsored schemes and other allied schemes. Details about Tamilnadu expenditure can be had from the table: 14. As far financing of health sector is concerned, the states are actually dependent on the centre for financing best plan and non-plan schemes because their expenditure generally exceeds their resources. In health sector, to date, about three-fourths of government expenditure were from plan and non-plan state budgets. The remainder has been expended by the state from the central health budget on centrally sponsored schemes. These have covered the subjects of communicable diseases and family planning and, to varying degrees, programmes such as the community health worker, voluntary health guides schemes etc. The breakdown of expenditure by capital and revenue account approximate the relative proportion of health budgets being spent on 'infrastructure' and 'services' though the definition of these does not strictly adhere to the budgetary categories.

Table: 14 Expenditure on Medical, Family Welfare and Public Health and its Share as Percent of Revenue Expenditure in Tamilnadu Year

Medical

Family

Public

Total

Welfare

Health

Health Expenditure

Total

Total

Revenue Soc. Welfare Expenditure Expenditure

(2+3+4) (I)

(2)

(3)

(4)

(5)

(6)

(7)

1970-71

1855.36

—

850.08

2705.44

32148

9969

1971-72

2285.85

1144.69

3430.54

39384

12088

1972-73

2396.14

688.01

961. 91

4046.06

41704

13377

1973-74

2826.31

449.03

914.70

4190.04

47263

14557

1974-75

3610.00

555.00

599.72

4764.72

52836

22309

1975-76 1976-77

4112.66 4723.97

628.44 1211.06

774.85 1832.12

5515.95 7767.15

55792 62837

24246 26836

1977-78

5043.45

679.68

1382.33

7105.46

70612

29975

1978-79

5552.40

797.40

2020.22

8370.02

75351

32164

1979-80

6033.45

761.78

1814.89

8610.12

84955

35555

1980-81

7112.12

880.42

2428.16

10420.70

115225

43256

1981-82

8342.72

1038.36

5391.77

14772.85

135989

53171

1982-83

9400.42

1289.00

7595.11

18284.53

157608

71082

1983-84 19-84-85

10596.96 11867.96

2073.78 2535.72

13830.86 7492.88

26501.60 21896.56

191080 221083

83.866 88371

1985-86

14007.50

2622.03

9685.04

26314.57

244975

110486

1986-87

15396.98

3351.69

9554.21

27757:00

287931

117220

1988-89

21713.46

4354.56

9203.00

26068.02

376304

155660

1989-90

26994.10

4449.00

—

31443.10

47079

632845

1990-91

31593.00

2148.00

—

33741.00

508789

249191

—

Source : complied from the Appendicles to budget memorandum, Tamilnadu

In this paper we have concentrated only on the revenue expenditure of the state. This is so because, we have made an attempt in bringing out the amount spent on health services of various state government depts. like PMS, DME, ESI and !MH. In TN budgetary provisions were based on the past actual expenditure' and not on the basis of health facilities. Since the figure for a single year reflects the state's usual expenditure, analyses of expenditure on health sector from the year 1970-71 onwards was taken up for this report. Revenue expenditure of ESI, DME and DMS has been analysed individually. The total expenditure of each department for various years from the year 1970- 71 was grouped into three categories 5.

In all departments under the directorate of Health and family welfare, it could be seen from table : 15 that major chunk of expenditure has been allotted to establishment of manpower services. In ESI more than 45 percent was spent on manpower services; around

35 percent, is spent on provisions which is directly spent on patients. 40-60 percent total revenue expenditure was spent on establishment of manpower services in DME, so is the case with DMS. Just as it has to be, second major share of total revenue expenditure was distributed to the category of expenditure on provisions. It could be seen' from table: 16 that per capita expenditure (6) has doubled in real terms and per patient health expenditure has remained more or less constant. From table : 17 it is seen that per patient expenditure, which is calculated taking only the amount allotted to drugs, shows that it is almost constant over a' period of time. Expenditure's for PHC are available only from the ye~ 1980-81 onwards, Here PPE has increased from I' 2.52 in the year 1980-81 to Rs. 6.27 in the year 1985-86 (Table: 18). The PPE has fluctuated,

Table: 15 Percentage Shares of Major Heads of Expenditure in Health (Other (Sal & (Provisi Running Pay ons) exp.)

(Other Sal & (provisio Running Pay ns) exp.)

(Other Sal & (Provisio Running Pay ns) exp.)

(Other Sal & (Provisi Running Pay ons) exp.)

Year

DMS

ESI

DME.

IM&H

1970-71 51.8

21.0

27.2

1971-72 1972-73 1973-74 1974-75 1975-76 1976-77 1977-78 1978-79 1979-80 1980-81 1981-82 1982-83 1983-84 1984-85 1985-86 1987-88 1988-89 1990-91

26.5 27.5 27.5 24.4 31.6 26.6 33.1 28.8 29.5 31.3 30.0 40.7 35.7 36.4 34.3 31.2 31.2 25.0

17.9 26.9 26.0 25.6 25.7 35.7 21.4 23.0 19.4 20.5 20.5 8.5 8.3 7.4 7.4 8.4 7.6 5.5

55.6 45.6 53.8 50.00 42.7 37.7 45.4 48.1 51.1 48.3 49.5 50.8 56.0 56.2 58.3 60.4 61.2 69.5

41.7 41.7 50.2 43.1 38.6 37.1 53.6 49.0 48.2 52.1 46.9 50.9 53.8 55.6 61.1 57.8 67.6

35.6 35.6 40.9 42.5 38.0 50.0 31.4 43.0 42.9 40.2 44.1 39.9 38.7 38.1 31.2 34.9 25.7

22.7 22.7 8.9 14.4 23.4 12.9 15.0 8.1 8.9 7.6 9.0 9.2 7.5 6.3 7.7 7.3' 6.7

46.9

38.1

15.0

36.31

19.24

44.44

50.0 51.9 51.9 47.2 46.3 47.3 49.7 48.0 47.7 48.4 49.3 49.7 46.1 47.8 49.1 50.6 57.0 58.0

28.7 26.3 26.3 30.7 36.9 36.1 33.2 31.7 31.7 31.1 30.1 29.3 26.6 26.5 26.1 24.8 23.0 21.0

21.3 21.8 21.8 22.1 16.8 16.6 17.1 20.4 20.5 20.5 20.7 21.0 27.3 25.8 24.8 24.5 20.0 21.0

56.41 53.00 53.00 54.46 54.21

25.39 28.44 28.44 27.29 28.08 31.10 28.41 27.40 27.70 31.12 29.59 22.00 21.51 21.94 20.48 24.52 19.00 14.60

18.19 18.57 18.57 18.25 17.71

54.19

54.15 58.95 61.25 56.58 58.37 64.18 61.06 66.31 70.99 66.64 69.40 78.30

14.72 17.44 13.66 11.03 12.30 12.04 9.84 17.43 11.76 8.54 8.84 11.60 7.10

SOURCE COMPLIED FROM : Government OF TAMIL, REFORMANCE BUOOET OF Medical services and Family Welfare (OMS). Directorate of Medical Education (DME). Indian Medical and Homeopathy (IM&H).

Table: 16 Expenditure on Medical, Family Welfare and Public Health, its Share as Percent of Revenue Expenditure. in Tamilnadu and per Capita Expenditure.

Year

Per Patient Real Terms Share of Health as Share of Health Public Health Health per Capita Per Capita Percent or Total as Percent or as Percent or Public Health Health Total Expend. [Rs.] Social Expend. Expend. [Rs.] Expend. [Rs.] Expend. [Rs.] Expend. [Rs.] [Rs.]

1970 71

8.4

27.1

31.4

12.29

6.57

2.06

1971-72

8.7

28.4

33.4

11.26

0.00

2.73

1972-73

9.7

30.2

23.8

9.28

0.00

2.26

1973-74

8.9

28.8

21.8

5.66

0.00

2.12

1974-75

9.0

21.4

12.6

9.99

6.34

1.37

1975-76

9.9

22.7

14.0

9.67

7.42

1.74

1976-77 1977-78

12.4 10.1

28.9 23.7

23.6 19.5

7.42 .4.39

9.57 8.48

4.04 3.00

1978-79

11.1

26.0

24.1

3.14

8.96

14.31

1979-80

10.1

24.2

21.1

2.01

8.03

3.81

1980-81

9.0

24.1

23.3

12.57

8.16

5.02

1981-82

10.9

27.8

36.5

15.07

10.81

10.96

1982-83 1983-84

11.6 13.9

25.7 31.6

41.5 52.2

10.10 11.96

12.06

15.19

15.63

27.23

1984-85

9.9

24.8

34.2

21.19

12.25

14.51

1985-86

10.7

23.8

36.8

8.10

6.76

13.16

1986-87

9.7

24.1

33.8

8.4

6.50

6.38

1988-89

6.9

18.5

1990-91

4.1

15.1

— —

— —

— —

— —

Note: from, 1986-87 onwards expenditure on public health is combined with medical. ..

Source: compiled from the Appendices to budget memorandum, Tamilnadu

Table: 17 Current price P.P.EXP.

EXPI PAT

Constant price PPE

Exp I Pat

Per Capita Health Expenditure Current price

Constant price

P.P. EXP

Exp I Pat

PPE

Current price

Exp / Pat

P.P.EXP.

EXP/PAT

Constant price PPE

Exp/Pat

on Drugs

on Drugs

Dr VAL

(Rs.)

(Rs.)

(Rs.)

(Rs.)

(Rs.)

(Rs.)

(Rs.)

(Rs.)

(Rs.)

(Rs.)

(Rs.)

(Rs.)

(1)

(2)

(3)

(4)

(5)

(6)

(7)

(8)

(9)

(10)

(11)

(12)

Year

on Drugs

on Drugs

DMS

ESI

on Drugs

DME

1970-71

7.17

1..51

111.50

90.49

1971-72

5.42

1.43

137.57

78.92

1972-73

5.81

1.60

3.18

1.13

149.86

76.10

1973-74

5.39

1.48

3.25

1.32

148.16

78.25

1974-75

6.25

1.53

1.93

1.33

3.13

1.28

87.25

56.83

1975-76

7..55

2..39

1.44

1.03

3.48

1.48

1.20

1.56

7.59

6.04

4.89

2.99

1976-77

7.25

1.93

1.67

1.28

4.27

1.62

1.22

1.23

6.69

5.11

3.83

2.43

1977-78

6.82

2..26

1.23

0..91

4.44

2.22

1.64

1.51

6.96

4.65

3.44

2.10

1978-79

5.95

1.72

1.03

0.78

3.72

1.17

0.83

0.69

8.17

5.39

3.85

2.38

1979-80

5.09

1.50

1.58

1.18

4.36

1.87

1.29

1.19

8.40

5.59

3.84

2.56

1980-81

7.47

2..34

1..55

1.15

4.89

2.10

1.42

1.27

8.89

5.71

3.87

2.50

1981-82

8..83

2..52

1.67

1.29

5.95

2.39

1.48

1.35

9.31

5.68

3.51

2.09

1982-83

7.28

2..97

1.59

1.22

7.01

3.09

1.74

1.61

10.21

6.02

3.39

2.06

\

1983-84

7..51

2.69

1.72

1.33

7.92

3.16

1.87

1.71

11.72

6.75

4.01

2.45

1984-85

7.89

2.87

1.72

1.34

10.04

3.89

2.33

2.14

13.25

7.35

4.41

2.61

1985-86

9.52

3.26

11.38

4.33

2.29

2.09

17.46

9.28

4.90

2.86

1987 -88

22.77

7.11

14.88

4.65

21.45

10.52

1988-89

13.37

4.17

17.56

6.13

4.70

1.01

1990-91

13.14

3.28

5.94

1.24

SOURCE COMPILED FROM: GOVERNMENT OF TAMIL, PERFORMANCE BUDGET OF Medical Services and Family Welfare Medical education

PPE : Per Patient Expenditure (exp / pat) : Calw1ated by dividing the budgetary expenditure in various directorates by total No. of patients treated by that directorate hospital. &p I Pat on drugs Amount allocated to drugs divided by patients treated in The hospitals, under the directorate.

Table :18 Primary Health Centre

Rev Exp (Rs. 000's)

No. of Pat Treated

P.P. Exp

1980-81

37239

14781857

2.52

1981-82

41213

12208969

3.38

1982-83

47251

17704230

2.67

1983-84

62520

17107855

3.65

1984-85

60915

13989435

4.35

1985-86 1987-88

74738

11926526 11663014

6.27

Mobile Health Centre (MIIC) P.P. Exp Pat Treated

(Rs)

761

46377 9

1.64

1981-82

1436

547601

2.62

1982-83

1671

686538

2.43

1983-84

1533

720864

2.13

1984-85

2012

722074

2.79

1985-86

1327

729085

1.82

Rev. Exp

No. of (Rs)

1980-81

SOURCE: COMPILED FROM DIRECTOR & MEDICAL SERVICES

Inspite of increase in the share of money from total revenue expenditure due to fluctuations in the number of patients treated break up of expenditure is not available. Since the numbers of persons serving in PHC are less, it is assumed that establishment cost would be less than the expenditure on provisions. The Mobile Health Centres (MHC) do not have a significant share in health sector as such. The Table 18 gives us a rough picture of MHC's in terms of amount spent by this department and patients treated PPE was between Rs.l.60 and Rs. 2.80 The expenditure of budgets allotted to this sector had fallen from Rs. 2012 thousand in the year 1984-85 to Rs.1327 thousand in the year 1985- 86 In general, it could be said that the drastic increase in health expenditure was mostly due to inflation. State accord to health could be assessed from the importance given to health in their financial allocation. Most modern medicines and drugs cover only a small minority of the people in poor society. The PPE of various directorates of health services imply that mostly minimum drugs needs of the society go unmet. We conclude that -the state expenditure which has been increasing over the years had grown -in an unhealthy direction. It was not enough to meet the health needs at all. Our then Chief Minister in his speech at K.J. Hospital has said that even if the entire budget expenditure was spent on medicine, health needs of all poor people cannot be met. (Thinamani, 23.6.89) Even if the entire revenue expenditure is spent on health, per capita expenditure will be a negligible amount. Medical Infrastructure in Private Sector The National Sample Survey Organisation 34th round had survey non-7 and own account enterprises of unorganised services relating to medical and health services for the period 1979-80. For whatever little information is available from published sources on medical and health services covers only government organisation and few voluntary organisations. Since NSSO 34th round had surveyd non-governmental establishments. We tried to incorporate NSSO 34th rounds results with the information we have for the year 1979-80. Even here, the non-governmental directorate establishments were not available, for the CSD have done a 'survey for the period 1979-80, the non-governmental directory establishments, which has not published statewise data.

Table: 19

Number of Medical Institutions 1979-80. OAE

Types of Institution Hospitals Maternity Homes Nursing Homes Cons. Chamber / Clinics Dispensaries -1 PHC's Units Others

12135

Total Total no. of medical institution of all categories excluding directory establishments = 20771 Note for Table No. 19 1. Various type of institution under OAE is not available 2. Hospitals under non-directory include special and general hospitals. Hospital under government includes special, District head quarters hospital, Taluk hospital, nontaluk hospital, Teaching (Allopathy, Homeopathy and Indian Medicine), Municipal and corporation Hospitals and ESI Hospitals. 3. Dispensaries includes Government Dispensaries, Table : 20

Non-Directory

Government

409 74 220 5856

306

6559

4 1073 383 73 238 2077

subsidised rural dispensaries, Panchayat union and municipal dispensaries and Indian system medicine dispensaries (local bodies) 4. Units includes mobile medical units and leprosy control units. 5. Others under government includes private aided and nonaided medical institutions. Source: Performance budget 1979-80 of Medical Education, Medical Services and Family Welfare and, Public Health Services. Tamilnadu - An Economic Appraisal (1980).

Number of persons employed in Health Sector 1979-80

1. Own Account enterprises (OAE) 2. Non-directory establishments (non- governmental) 3. Government undertaking (DMS, DME, PHS)

Rural

Urban

9852 3416

6806 14102

Total

Total 16658 17518 50621 84797

Note : Persons employed in government undertaking Source: 1 & 2 N.S.S. 34th round, No. 327 and No. 321. includes medical officers, nurses, pharmacists, Health 3. Tamil Nadu- an economic appraisal and performance Inspectors, technicians, Health Assistants and Lab budget 81-82 DMS, DME, & PHS. Assistants employed in DME, DMS, and PHS.

Table 19- gives a picture of the state level medicare institutes. It shows that there are 20771 medical institutions, which includes all categories of institutes functioning in the state, excluding non-governmental directory establishments. 58.42 % are OAE's, 31.2 percent are non-directory establishments and only remaining

10 percent are government enterprises. Table 20- gives the number of persons actually serving health and medical facilities. The total number of persons comes to around 84797 only. Own account enterprises are one person enterprises, other non-directory establishment as

per definition were expected to have strength of 5 or more working staff. When Government institutions are seen, only the statistics of medical and paramedical personnel are published. These figures seem to be an underestimation. The estimated figure is far from earlier census reports, for medical and paramedical personnel comes to around 27751 medical personnel, and

48741 paramedical personnel serving in the state for the year 1979-80. Though in terms of number of medical institutions it could be seen that only 10 percent are government institutes, it is the government undertaking in which nearly 60 percent of the total manpower is here. The resources spent for medical health services were around Rs. 949529.64 thousand.

Table 21: Expenditure - Health Sectors 1979-80 (Rs.000)

Own account enterprises (OAE) Non-Directory (non -governmental) Government Indian Medicine DME DMS ESI Family Welfare PHS

Rural

Urban

Total

12164.80

9172.54

21337.34

7809.0

4075.23

48561.3 888365 8277 280878 325598 59357 77311 136944

Total

958263.64

Note: For OAE total expenditure was calculated from the per enterprise expenditure.

expenditure is not available.

OAE and non-directory establishment total expenditure was converted to thousands of Rs. For government undertaking rural urban break-up of

Source: NSSO- 34th round, No.32.7 and No.321, Performance Budget 1981-82 of Indian medicine, Medical Education, Medical Services and Family Welfare and Public Health and Precentive Medicines PHC.

Statistics reveals that 90 percent of medicare institutions i.e. (12135 OAE and 6559 non-directory establishment) come from non-governmental health sector only. In the Government establishments in which nearly 59 percent of the total persons serving in health sector are found in the government sector, further 93 percent of the total resources were spent by government departments only. Hence, while- the government also has a dominant presence in health care services, non-governmental cannot be ignored. The level and standard of health care provided in Tamil Nadu is far below than in other States. Somehow there is growing impression that Tamil Nadu was in forefront of medicare. These have been created by the few peaks of excellence of the most famous surgeons and prestigious medical schools and hospitals in the state.

be compared with those prevailing in some of the small states for instance in 1988, despite the best efforts the CDR in Tamil Nadu was 9.7 per 1000 as against 6.8 per 1000 in Kerala. Infant fertility in Tamil Nadu was 76 per 1000 as against 36 per 1000 in Kerala. And the ratio if bed population was 1039 in Tamil Nadu, where as Kerala had one bed for 591 persons. So much has to be done in Tamilnadu in terms of health care.

The average health care standard in Tamil Nadu could

IMBALANCE IN DIFFERENT HEALTH CARE INFRASTRUCTURE IN TAMILNADU : 1. Special imbalance a) Rural-Urban imbalance : Here rural-urban imbalance in terms of population served by medical institutions, medical and paramedical personnel is brought out. From Table: 22 we can see the imbalance

Table : 22

Population (000.S) Served By Medical Institutes -1970-71 & 1981 1970-71

s

m

1980

Hospitals Rural

Urban

Dispen.

PHC

Others

Total

Hospitals Rural

Urban

a

2

15

27

27

3

93

2

19

2

b

948.45

63.17

107.69

70.2

969.2

31.2

703.9

11.62

a

4

31

46

38

1

119

5

23

b

743.18

25.26

81.65

78.23

3755.3

31.56

679.76

a

8

17

39

34

102

b

338.09

30.18

92.76

91.32

904.43

a

3

6

18

9

1

b a

732.41 1

132.56 7

166.26 14

244.14 16

2992.62

4

,

Others

Total

27

14

94

113.02

52.14

38.47

43

38

17

44.15

102.66

89.44

35.03

8

15

36

34

15

35.47

442.78

43.97

116.72

104.72

38.91

37

6

17

32

35

9

99

80.88 38

407.67 3

58.56 8

107.55 14

69.89 16

382.4 8

34.74 49

44.15

603.3

23.39

142.65

113".12

24.96

40.94

145

11

24

91

45

13

183

83.31

56.35

69.54

394.5

27.97

32

2

,6

24

6

5 14.65

Dispens.

Pile

126

105

b

1533.63

20.56

119.84

95.86

a

9

27

64

45

b

312.99

57.64

68.33

62.6

a

2

7

16

b

125.39

34.75

30.81

62.7

15.41

16.12

51.31

24.24

53.72

a

6

25

56

35

121

9

32

55

35

18

49

b

435.67

. 52.97

70.32

51.36

90.72

82.63

321.37

51.37

90.72

82.63

252

30.44

a

16

40

43

37

1

127

27

50

110

75

25

287

163.36

7689.55

60.55

261.47

40.68

67.94

77.31

30.76

1

102

12

30

31

32

12

,

4

1

4373 30.15284.48 3

b

377.78

41.73

178.83

a

12

25

32

32

b

176.13

29.87

39.38

66.05

2860.2

25.04

149.54

31.37

107.59

74.83

25.53

a

11

26

46

31

1

116

10

23

64

31

12

b

197.36

39.6

69.58

70.03

3200.3

277 .56

233.6

58.4

55.4

73.35

25.53

a

3

6

18

9

1

37

4

8

14

9

3

b

339.38

34.07

67.92

113 .12

1222.55

33.04

294.47

30.69

101.67

130.87

37.46

25

107

a b

11

30.62

85

259

419

379

18

1248

99

280

668

383

162

b

333.05

48.13

81.26

75.82

2288.64

33.oI

327.84

56.97

72.47

84.74

30.41

units served (000's) by each unit.

d from Statistical Abstract of Tamilnadu, 1972.73 and 1.981-82 and from census of India, Tami1nadu, Population tables, Series 20, 1971 and 1981.

140 140 38 143

22.91

131.06

a

s of number of hospitals is available from 1970-71 onwards only. We have calculated the population (in 000’s) served by s of medical institutes, under the jurisdiction of various medical directorates of Govt. of Tamilnadu. pital and PHC’s hospital-population ratio is obtained by taking into account only the rural population. Similarly for urban hospital-population ratio is calculated by taking into account only the urban population. In the case of dispensaries and ere the area is not mentioned total population is taken into account. So is the case for total number of units

43

1592

in terms of population served by medical institutions in Tamilnadu for the year 1970-71 and 1980. Though there is increase in the absolute number of medical institutions, the population that is served by these institutes has fallen marginally. In terms of hospitals each rural hospital is expected to serve nearly eight times more than urban hospital in seventies and six times more Table : 23

in eighties. It cannot be denied that there are PHC's which cater to rural medical needs to an extent. The medical and para-medical personnel available shown in table: 23 also clearly brings out the concentration of medical and paramedical personnel in urban areas' and more in the Capital city Madras.

Index no. of Medical Personnel in Tamilnadu 1961

State/Dist.

1981

Urban

Rural

Total

Urban

Rural

Total

119 129

32 32

55 54

128 122

28

TN (-MDS)

28

61 54

PARAMEDICAL Tamilnadu

189

25

65

203

55

104

TN (-MDS)

168

25

53

201

55

96

MEDICAL Tamilnadu

Note: MDS - Madras, Index - No. of medical personnel/Lakh population Source : Compiled from Census Reports b) Imbalance across district' There is no proper distribution of health infra-structure across the state. Madras being a metropolitan area has more health facilities; Kanaykumari and Nilgiris districts are better covered in terms of health facilities. Because of these three districts the State average in terms of any

medical indices. Hence generally we see most of the districts are below state average. This is brought out in the tables 24 a and 24 There is imbalance in terms of rural urban ratio between districts. This is shown in table : 24.

Table : 24 Urban/rural ratios of medical and paramedical personnel for the population of one lakh in urban and rural areas of Tamilnadu during 1961 and 1981. Medical

Paramedical

State / Districts

1961

1981

1961

1981

Tamilnadu Chingleput North Arcot South Arcot Salem Coimbatore Madurai Trichy & Thanjavur Ramanathapuram Tirunelveli Kanyakumari The Nilgiris

3.7 2.8 5.6 4.7 5.5 3.7 4.1 3.9 3.9 2.2 2.0 2.8

4.6 3.4 6.2 4.9 4.1 4.8 4.7 5.8 4.1 3.6 2.3 2.0

7.6 4.1 7.2 8.0 7.6 6.5 7.5 7.9 5.3 5.6 4.3 2.9

3.7 2.1 4.7 4.8 4.0 3.7 3.6 5.0 3.8 4.0 2.2 1.8

Source: Computed from Census of India. Madras, Vol. IX, 1961 and Series 20, Tamilnadu 1981.

In the southern Districts the urban-rural ratio is not much the spatial distribution of medical Institutions seems to De 'more even. It is also noted that the urban-rural ratio with respect to medical personnel has increased between 1961 and 1981, whereas with respect to para-medical personnel

it has fallen. It implies that rural population though does not have enough medical personnel available, para-medical personnel services are spread over to cater their needs.

Table 24 A

Average Number of Villages and Population Served by Medical Institutions Percentage or Villages having Medical Amenities

Avg. or Villages Served

Avg. populations Served by a

by a Medical Institution

Medical Institution

Including Hospitals

Excluding Hospitals

Including Hospitals

Excluding Hospitals

India

6.28

11.4

13.1

8707

9990

Kerala

89.18

0.2

0.2

2835

3021

Tamilnadu

16.87

4.9

5.7

8871

10464

Chengalpattu

13.06

8.2

8.8

8701

9344

Northarcot Southarcot

14.05 10.87

6.1 9.5

7.8 11.4

10693 12621

12490 15145

Dharmaputi

11.63

5.6

6.4

8291

9468

Salem

20.68

3.9

4.2

9005

9679

Coimbatore

23.20

3.5

4.3

10550

13102

Nil Giris

50.00

1.2

1.4

7376

8648

Madurai

24.20

3.4

3.7

8656

9'471

Trichirapalli

20.60

4.7

5.5

10609

12519

Thanjavur

16.56

5.5

7.00

8456

10711

Ramanathapuram

16.38

5.1

5.8

7575

8662

Tirnelveli

23.44

3.1

3.9

6981

8649

Kanyakumari

74.19

0.3

0.4

4389

5919

Source: Census of India, 1981 Occassional Paper of 1986. Imbalance in personnel development : There is also districts and between rural and urban areas imbalance in medical manpower availability between

Table: 25a Index No. of Medical Personnel in Tamilnadu (No of medical personnel/lakh population) 1961 State/Dist.

Urban

Rural

Tamilnadu

119

Madras Chingleput North Arcot South Arcot Salem Coimbatore Madurai Trichy & Thanjur Ramanathapuram Tirul,1elveli Kanyakumari

7$ 93 141 117 120 105 111 137 105 89 166

The Nilgiris

1981 Total

Urban

Rural

Total

32

55

128

28

61

33 25 25 22 28 27 40 27 40 82

75 45 48 37 38 50 54 60 47 55 95

152 85 179 98 111 125 127 156 98 103 153

25 29 20 27 26 27 27 24 29 66

152 48 64 32 45 64 63 56 45 55 81

75

27

48

58

29

43

TN (-MDS)

129

32

54

122

28

54

TN (-NIL & KK)

119

30

54

129

26

60

Tab1e 2Sb Index No. of Paramedical Personnel in Tamilnadu (No. of paramedical personnel/lakh of population) 1961 State/Dist.

Urban

1981

Rural

Total"

Urban

Rural

Total

55

104 211 116 123 71

Tamilnadu Madras Chingleput North Arcot South Arcot Salem Coimbatore Madurai Trichy & Thanjur Ramanathapuram Tirunelveli Kanyakumari

189 276 150 210 192 122 150 173 182 159 151 191

25 37 29 24 16 23 23 23 30 27 44

65 276 57 61 43 31 55 65 52 58 61 62

203 211 171 313 211 193 204 191 205 173 167 211

81 66 44 48 55 53 41 46 42 95

The Nilgiris

61

21

36

212

116

162

TN (-MDS)

168

25

53

201

55

96

TN (-NIL & KK)

192

25

65

203

53

103

-

80 112 103 79 82 85 117

Source : Compiled from Census of India" : Series 20,

Series' 20, (1981) General Economic Table Part III,

Tamilnadu (1981), part III, General Population Tables.

A & B (ii).

Volume IX, Madras (1961), General Economic Tables

Even among districts it could be seen that it is the southern districts which have better access to hospitals. Districts like Nilgiris, Kanyakumari, Tirunelveli, Coimbatore, Madurai, Sales and Trichy are always above stare average in terms of villages served by medical institutions and regarding villages served by medical institutions (Table: 240). From table: 25 it is seen that northern belt of Tamilnadu has more number of medical and para medical personnel available for every lakh population. One observation that can be made is; the distributes which have better access to medical institutions are not the same districts which have more medical or para-medical personnel service available. Districts like Tanjavur & Trichirapalli, North Arcot are better in terms of availability of medical manpower.

treatment underwent by more than 95 percent of the case; was allopathy (Tables-29 and 27). Whether allopathy is good compared to other systems of medicine is debatable and this is outside the preview of this paper. It is clear from the NSSO survey, that public sector in health has a prominent share in rendering health services, especially in the category of hospitalised cases. In this paper though much emphasis is not given to private health services, it does throw a better picture of health services at the state level.

Distressing feature is the concentration of hospitals and beds in big cities and metropolitan towns. In Tamil Nadu on an average a hospital has 108 beds whereas the AllIndia average is only 70. In Kerala it is 57. The large concentration of hospital beds in TN is due to the urban3) Imbalance in the development of various types of oriented infrastructure. The tendency was to locate more hospitals in big cities and in district head-quarters, which infrastructure. provide them with modern facilities and help them to In the year 1986-87 National Sample Survey Organisation, specialise and make them institutions of excellence. And in its 42nd round conducted a enterprise survey, which the interest of the people living in rural and semi-urban included information on morbidity and utilisation of areas is not properly cared for. medical services. This survey has revealed that in rural areas both in India and Tamilnadu 55 percent of the Yet another distressing feature in TN is that 80 percent of hospitalised cases were treated in public hospitals only the doctors rendered services only in the cities and big (Table: 26). In urban areas 60 and 58 percent in India and towns. Some of the rural and semi-urban areas of Tamil are Tamilnadu respectively were treated in public hospitals. not as bad as they are in some' North Indian States. Of course, our rural medical services are also better though not While only 31 and 39 percent of hospitalised cases in rural the best! area of India and Tamilnadu were treated in private hospitals; only 30 and 34 percent of hospitalised cases in Statistics show that there is one doctor for 621 persons in urban of India and Tamilnadu were treated in private big cities and one doctor for 4820 persons in semi-urban and rural areas of Tamil Nadu. One of the reasons for the hospitals. concentration of a few big hospitals and doctors could have created the illusion that Tamil Nadu is the heaven for When we account for the patients who were not medicare and medical facilities. (The Hindu, 16 April hospitalised, it could be seen from the table: 27 that more 1989). This illusion should be exploded as that more than 50 percent, of the patients were treated by private voluntary efforts would be forthcoming in this field, doctors in India, but in Tamilnadu 33 percent in rural areas particularly in rural areas. The voluntary efforts in the area and 41 percent in urban areas were treated by private of medical aid arid health services in TN. were not as good doctors, Regarding the number of days by the patients as they should' be. Out of the total number of hospital beds stayed in the hospital it is clear that from the Table: 28 that in TN hardly 20 percent is funded by voluntary agencies. on an average more number of days were stayed in urban government hospitals than in rural hospitals; the Before concluding' an attempt is made to compare the position of Tamilnadu vis-vis India, and Srilanka, expenditure on an average was more in private hospitals. especially in terms of some important indicators. This is 4) 1mbalance in terms of different system of medicine. shown in table: 30 Whatever be the source (in or out patient) the system of

Table : 26 Distribution of Hospital Cases by Type of Hospital and Type of Ward.

classification

India

Tamilnadu

Kerala

Type or Ward

Type or Ward

Type or Ward

Sector Free

Paying Paying Gen Spec.

All

Free

Paying Gen.

Paying Spec.

All

Paying

Paying Spec.

All

Free Gen.

ESTIMATED NO. OF PERSONS

Public Hospital

Primary Health Centre

Private Hospital

Nursing Home

Charitable Inst. (Pub trust)

Others

ALL

RURAL

51.2

3.5

0.7

55.4

54.3

0.7

0.6

55.6

39.6

1.4

0.1

41.0

URBAN

50.1

6.9

2.5

59.5

54.7

2.0

1.0

57.7

42.1

7.6

5.1

54.8

TOTAL

51.1

4.0

1.0

56.0

54.4

0.9

0.7

56.0

42.2

2.0

0.6

42.5

RURAL

4.0

0.3

0.0

4.3

0.6

0.0

0.0

0.6

1.9

0.4

0.0

2.4

URBAN TOTAL

0.6 3.5

0.1 0.3

0.1 0.0

0.8 3.8

0.0 0.5

0.3 0.1

0.0 0.0

0.3 0.6

0.5 1.8

0.2 0.4

0.1 0.0

0.9 2.2

RURAL URBAN

3.6 2.8

23.6 18.9

4.8 7.8

32.0 29.5

2.9 1.5

29.7 24.3

6.5 8.4

39.1 34.1

3.6 2.0

39.0 29.1

10.S 10.6

53.4 41.8

TOTAL

3.5

23.0

5.2

31.6

2.6

28.6

6.9

38.1

3.4

38.0

10.8

52.2

RURAL

0.3

3.5

1.1

4.9

0.5

2.0

0.2

2.7

0.0

1.9

1.1

3.0

URBAN

0.5

4.2

2.3

7.0

0.2

4.8

0.6

5.6

0.0

1.1

0.8

1.9

TOTAL

0.3

3.6

1.3

5.2

0.4

2.6

0.3

3.3

1.8

1.0

1.0

2.9

RURAL

0.8

0.8

0.1

1.7

0.5

0.3

0.2

1.0

0.1

0.2

0.0

0.3

URBAN

0.6

1.1

0.2

1.9

0.2

0.3

0.0

0.4

0.3

0.3

'" 0.0

0.6

TOTAL

0.8

0.8

0.1

1.7

0.5

0.3

0.1

0.9

0.1

0.2

0.0

0.3

RURAL

0.8

0.8

0.1

1.7

0.7

0.4

0.0

1.1

0.0

0.0

0.0

0.0

URBAN

0.7

0.5

0.1

1.2

0.9

0.9

0.0

1.8

0.0

0.0

0.0

0.0

TOTAL

0.8

0.7

0.1

1.6

0.7

0.5

0.0

1.2

0.0

0.0

0.0

0.0

RURAL

60.7

32.5

6.8

59.4

33.1

7.5 100.0

45.2

42.9

11.9 100.0

URBAN

55.2

31.8

13.0

57.5

32.4

100.0

45.0

38.3

16.7 100.0

TOTAL

59.9

32.4

7.7

59.0

33.0

8.0 100.0

45.1

42.4

12.4 100.0

SOURCE: Compiled from Tables 2.00,2.09 and 2.15, Forty second round, No.364, 1986.87 Morbidity and Utilisation of Medical Services NSSO, Department of Statistics, New Delhi.

100. 0 100. 0 100. 0

10.1

Table : 27 Percentage Distribution or Treatment (Not as Impatient in a Hospital) By Source or Treatment and System or Medicine

Tamilnadu

India Rural

Urban

Rural

Urban

Kerala Rural

Urban

SOURCE OF TREATMENT ,

Public Hospital

17.67

22.60

30.41

29.94

27.50

32.83

Primary Health Centre

4.94

1.19

4.93

1.11

4.32

2.43

Public Dispensary

1:.59

1.75

.0.85

1.52

2.32

0.43

Private Hospital

15.23

16.18

20.32

17.28

41.64

40.21

Nursing Home Charitable Institutions ESI Doctor of AMA

0.75 0.35 0.38

1.15 0.81 1.61

3.04 1.63 0.85

3.94 0.49 2.50

1.04 0.11 0.38

0.66 0.12 0.63

Private Doctors Others

'53.01 5.18

51.83 2.86

33.13 4.84

40.91 2.31

20.57 2.12

19.87 2.82

Allopathic

95.91

96.31

95.95

97.93

93.27

92.61

Homeopathic

1.78

2.09

1.41

0.46

2.17

2.58

Ayurvedic

1.53

1.03

1.54

1.08

4.12

4.40

Unani/Hakmi

0.27

0.27

0.12

0.09

0.09

0.26

Combination of these

0.07

0.05

0.05

0.09

0.09

0.15

Others

0.44

0.25

0.93

0.35

0.14

-

100.00

100.00

100.00

100.00

100.00

100.00

System of medicine

All

Source: Statements 13 and 14, Forty second round, No: 364. 1986-87 Morbidity and Utilisation of Medical Services NSS0, Department of Statistics, New Delhi.

Table: 28 Average Numbers of Days Stayed in the Hospital and Average Total Expenditure per Hospitalised Case for Rural and Urban Sector

Average Number of Days (0.00) Stayed in Hospital Government Hospitals

Private Hospital

Free Ward

Paying General Ward

Paying Special Ward

Free Ward

Paying General Ward

Paying Special Ward

Rural

17.30

19.37

22.66

11.12

12.49

12.98

Urban

17.49

17.03

17.07

16.46

10.39

13.60

14.94 16.28

11.63 25.22

32.10 44.03

10.28 17.69

10.45 9.48

13.08 19.23

16.70 15.43

10.01 7.99

12.72 12.22

10.25 12.67

9.91 11.41

9.98 9.97

.

India

Tamilnadu Rural Urban

Kerala Rural Urban

Average Total Expenditure (Rs. 0.00) per Hospital Case India Rural

630.40

1040.21

1482.62

665.65

1031.15

1637.85

Urban

582.12

1421.11

1268.08

975.21

1393.26

2862.86

Rural

168.71

1070.63

765.60

470.78

868.90

1556.58

Urban

206.00

873.00

1139.38

640.70

1107.59

2891.52

392.40 229.21

265.86 450.55

692.84 568.69

243.99 242.17

464.64 598.46

704.26 982.49

Tamilnadu

Kerala Rural Urban

Source : Tables 7 and 8, Forty second round, No: 364, 1896-87 Morbidity and Utilisation of Medical

Services NSSO, Department of Statistics, New Delhi.

Table: 29 Distribution of Hospital Case by System of Medicine System of Medicine

India

Tamilnadu

Kerala

Rural

Urban

Total

Rural

Urban

Total

Rural Urban

98.50 0.30

98.52 0.25

98.50 0.29

98.49 0.63

98.78 035

98.55 0.57

96.80 0.52

0.51 0.22

0.42 0.28

0.50 0.23

035 0.13

0.25 0.84

033 0.07

1.73 0.73

0.11

0.11

0.03

0.00

0.02

0.36

0.10 0.43

037

037

0.51

0.40

0.11

0.10

100.00

100.00

100.00

100.00

100.00

100.00

100.00

100.00 100.00

Total

PERCENTAGE

Allopathy Homeopathy A Ayurvedic Unani/baksi Any comb. of these Others All

98.28 96.95 0.38 0.51 1.27

1.68

0.00

ESTIMATED NO. OF HOSPITAL CASES Allopathy

15690192 2687574 18377766 1129256

285567 1414823 1697748

200368 1898116

Homeopathy

47787 6820 54607 7223 81239 11457 92696 4013 35044 7638 42682 1491 344 17522 2728 20250 57345 11730 69075 4242 15929129 2727948 18657077 1146569

1012 723 318 0 1474 289094

9120 30342 14733 0 1929 1753872

775 9895 2589 32931 143 14875 0 0 0 1929 203875 1957747

Ayurvedic Unani/baksi Any comb. of these Others All

8235 4736 1809 344 5717 1435663

Source: Calculated from Table 2 and statement4, Forty second rounds of Statistics, New Delhi. No: 364 Morbidity and Utilisation of Medical Services MSSO, Department Table : 30 Health Indicators Indicator

Year

Srilanka

India

Tamilnadu

CBR

1990

20

30

22.4

COR IMR Population per: Physician Nurse Bed Percentage expenditure on health Per Capita expenditure

1990 1990

8 19

11 92

8.7 67

1985 1985 1985

250 1290 340

5800 297228 1368

7470 35644 1174

1989

6.2

1.7

Note : 1 For Srilanka the year referred under this Category is 1984.

1985-86

7.19

5.23

Source : World Development Report, .1992, and Health Information India, 1986.

It could be seen that Tamilnadu is better in terms of health status when compaired to India, but it has a long was to go when compared to Srilanka, which Js also a developing country like ours.

is dependent on culture, social and economic organisation, environment, attitude of life, patterns of behaviour, wealth ownership structure, level of education, nutrition, condition of work, and of course on the availability, accessibility and quality of medical services.

Concluding Observations The empirical analysis made in respect of health status in the state generally leads to the following conclusion. Death rate has declined over a period of time. Mortality is more in rural areas as compared to urban areas, which may be attributed to better health facilities. The incidence of disease has also come down. This is shown by the morbidity statistics. Though there has been a decline in the incidence of sickness, the level of morbidity has been quite high. The prevalence of water borne diseases and its mortality is basically due to lack of pure drinking water, poor environmental sanitation, poverty and ignorance. The analysis relating to health facilities show that the facilities have been quite inadequate. The ideal doctor population ratio is 1:3500, in 1985 it was 1:7470 in Tamilnadu; The bed population is 1: 1 000 is the ideal one, whereas in Tamilnadu it was 1: 1174. It has also been found that between the fears 1970- 71 and 1990-91, population has increased at a compound growth rate of 1.5 percent, but patients have increased at a rate of 8.03 percent (this includes only allopathy treatment of public services); budget expenditure (revenue) of Tamilnadu has increased by 6 percent during this period, while the amount allotted to Public health and medicine has increased by 8 percent. When compared with the increase in patients this allotment is not sufficient. . Though Tamilnadu has attained some goals relating to the 'Health for all by 2000 AD ' there seems to be some imbalance, especially when it is seen district wise. In India, Kerala is at one end of the spectrum in terms of the health; in Tamilnadu Kanyakumari is comparable with Kerala, while most of the districts are lagging behind. Efforts should be made to increase the number of hospitals and dispensaries, particularly in rural areas. The number of doctors seems to be quite limited in relation to population. Though statistics shown above is impressive (When compared to National averages), there is much to be done in this Held. Actual status will be obtained only when the statistics for different breakups are available (for

different groups within the same population have different health profile.) The health of the population

Non-health sector which has important role in the context of inter-sectoral action for promotion of health goals are food, housing, water supply, sanitation and hygiene. These sectors should be studied in relation to health; so that better health policies to prevent various diseases can be formulated. NOTES: 1. We depend on the Sample Registration System (SRS) survey on Causes of Death in rural areas and Medical Certification of Causes of Death (ICD) for urban areas. SRS information is based on the report by para-medical staffs who code the cause of death on the basis of symptoms and condition reported prior to the incidence of death from the household. Similarly ICD is based on the report from district and teaching hospitals located in urban areas; urban deaths which occur without treatment tend to be excluded. Hence both SRS and ICD are not much reliable and there is no other source of information providing cause of death. 2. The database are; they cover only statistical information recorded by Government Agencies.' In Tamilnadu Private Health sector also does enormous services mostly in urban and semi-urban areas. The statistics of the private sector if recorded at all, are not available to researchers. Government of India data covers only the statistical information from Central Government agencies, and the health profile of Tamilnadu covers only the information from State Government Agencies. In this part the discussion is on the Government infrastructure facilities for similar type of information for private sector in health sector is dealt with, with the available information at the end of the section. 3. Data Source As far as Tamilnadu is concerned Annual Statistical Abstract of Tamil Nadu gives the figures for hospitals dispensaries, PHC's and beds under the jurisdiction of Director of Medical Services (DMS)' and Family Planning and Director of Medical Education, (DME) by districts. This is the only source of information (NSSO) 34th round had done a survey on nongovernmental, non-directory medical and health institution in 1970-80 and for medical and health directory and establishments.

For Tamilnadu the database regarding health services are poor. for hospital services, there is no detailed information about approximate number of days a patient stays, in a hospital, time spent by medical personnel and, paramedical personnel on rendering services and details on beds, number of days stayed by the patients etc. It is a fact that the health infrastructure is very poor, especially so in rural areas. 4. Medical personnel are covered, under the occupational category of physicians, surgeons and dentists. It includes allopathic, ayurvedic, homeopathic, Unani doctor dental surgeons; veterinarians, pharmacists; dieticians and nutritionists; public health physician’s surgeons not elsewhere classified are excluded from our analysis to estimate the total medical personnel. Paramedical personnel are covered under the category 'Nurses and other medical technicians' census were considered for the purpose of this study. Here we have excluded the subgroup 'pharmacists and pharmaceutical technicians' in the year 1981. 5. The first group includes salaries, pay, travel allowances, dearness allowance, establishment etc. Second group includes the amount spent on provisions ie. on medicines, diet, linen, hospital accessories on provisions etc. The remaining heads of expenditure were put under another group. 6. Expenditure for per patient has been calculated. This has been done on the following basis; total number of patients treated (Which includes both impatient and outpatient treated in hospital and dispensaries under various directorate) was divided by expenditure on medicine and drugs was calculated. This was done both at current as well as constant prices (base 70-71 prices). To calculate the per patient expenditure on medicine and drugs at constant prices. This will help us to gauge out the actual spend on medicine for each patient.

References 1. Madras institute of Development Studies, II 988) Tamilnadu

Economy; Performance and Issues, Oxford & IBH, New Delhi. 2. Guhan G. II 981). Health in Tamilnadu Facts and Issues Working

Paper No. 21, Madras institute of Development Studies, Madras. 3. Nagraj K. (1986) Infant Mortality in Tamilnadu, bulletin Madras

Development Seminar Series XVI (I) Pp 27-68 4. Health Profile of Tamilnadu, 1981 (unpublished), Government of

Tamilnadu. 5. Annual Statistical Abstract of Tamilnadu, 1970- 70 and 1980,

Department of Statistics, Government of Tamilnadu. 6. Performance Budgets of Directorates of Medical Education Public

Health & Preventive Medicine, Primary Health, Medical Services and Indian Medicine and Homeopathy, For years 1970 to 1991-92, Government of Tamilnadu 7. Appendices to Budget Memorandum, Government of Tamilnadu, For the years 1970 to 1991-92, Government of Tamilnadu. 8. National Sample Survey Organisation 34th Round, II 973-73) Sarvakhshna July-Oct, 1980. 9. National Sample Survey Organisation 42nd Round, Tables with Notes on Non-Governmental medical and Health Non-directory Establishments, July 1979-Junc 1980, No. 321. 10. National Sample Survey Organisation 42nd Round, Morbidity and Utilisation of Medical Services, No. 364, 11. Study on Distribution of Infrastructural Facilities in Different Regions and Levels of Urbanisation, Occasional Paper I of 1986, Census of India, 1981. 12. Census of India, General Economic Tables, Madras, 1961 and Tamilnadu (Series 20) 1981. 13. Survey on Infant and Child Mortality (1979), Registrar General of India, Government of India. 14. Annual Year Book, (1986-87), Department of Health and Family Welfare, Government of India. 15. Health Information India, II 986) Registrar General of India, Government of India. 16. Survey of Causes of Death (Rural), Annual Report, 1988, Registrar General of India, Government of India. 17. The Hindu dated 16th April 1989. 18. World Development Report 1992.

Form IV - MFC BULLETIN DECLARATION

1. Place of Publication: Sanjeevani Hospital, Dindori, Dt. Nasik - 422202. 2. Periodicity of Publication: Monthly.

7. Own account enterprises (OAB) i.e. enterprises without any worker and; (ii) establishment: i.e. enterprises with atleast one hired worker. 8. Establishments having hired workers were further classified into two categories namely; (a) Non-directory establishments (NDE) i.e. establishments with total number of workers including household workers, if any, 5 or less and annual output/turnover/receipt less than Rs. 1,00,000 and (b) Directory establishments (DE) i.e. establishment with 6 or more workers and/or annual output/ turnover/receipt Rs. 1,00,000 or more.

3. Printer's Name and address: Dr. Sham Ashtekar, Sanjeevani Hospital, Dindori, Dt. Nasik - 422202.

4. Nationality: Indian 5. Editors Name: Dr. Sham Ashtekar 6. Owner's Name: MFC Trust, (Representing Member: Dr. Anant Phadke, 50, UC Colony, University Rd., Pune - 411016. I, Dr. Sham Ashtekar, declare that the particulars given above are true to the best of my knowledge. Sham Ashtekar (Publisher) 17/2/93.

WHY UNIVERSAL MEDICAL INSURANCE? Anant R. S. Phadke The anarchy, the irrationality, the malpractices prevalent in the medical profession needs no description. The crass commercialism, consumerism, expertism that has progressively invaded our social life has now also engulfed the medical profession. The point is- what is the solution? The solution cannot of course be in isolation of what happens in the rest of the society. But at the same time, there have to be specific measures which would be conducive to a rational, ethical, humanitarian medical practice. Nationalisation of medical services, as was done in the former State Socialist Societies like Soviet Union, is one standard solution. Nationalisation has also been done partially in the U.K. Nationalisation removes the commercialism in the medical services and hence eliminates the most important cause of irrational practice and malpractice. Secondly, in nationalised medical services, everybody gets medical service irrespective of his/her ability to pay. But nationalised services tend to be bureaucratic. Especially in a country like India where there is as yet, not enough of a democratic culture, marked bureaucratization may take place much more easily. Such bureaucratism is quite inimical to a highly personal, intimate service like the medical service. I, therefore, feel that in the coming few decades, nationalisation of medical service would not be an appropriate step. I feel that a system of Universal Medical Insurance (UMI) as is being practised in Canada for the last 30 years would be a much more appropriate system for us. In this system, every citizen would be insured by the local or central government so that nobody pays at the point of delivery of medical services. Doctors are reimbursed by the government on the basis of the bills submitted. It is thus a publicly financed but privately managed system. It would avoid the bureaucratism of a nationalised system (arising out of a weak democratic tradition) and the commercialism of the private medical care. What is equally important, such a system would be instrumental in regulating the medical care into a rational cannel. Given the very nature of doctor-patient relationship, patients are not in a position to regulate the

content of medical practice. Patients are not mere consumers. A patient is quite vulnerable physically and psychologically when s/he approaches the doctor. Secondly, s/he lacks the knowledge of medical science to critically analyse the doctor's advice to undergo, say an expensive investigation. In the system of medical insurance, the insurance agency is, however, free from both these vulnerabilities. Since it is going to pay the doctor, it is bound to see that the doctor is not indulging into unnecessary medical intervention. It can employ its own medical experts to analyse the doctor's advice. Individual patients cannot afford this. Similarly, doctors' charges would also be regulated through negotiations between doctor’s bodies and the insurance agencies. Individual patient cannot do this. Doctor's bodies and medical experts of insurance agencies would decide broad standard treatment guidelines in accordance with the local conditions of availability of drugs, equipment, funds etc. Doctor's intervention in a particular case would be evaluated for payment on the basis of these guidelines. Today no such guidelines exist because there is no powerful agency to formulate and enforce them. Standard textbooks or other Western literature is not sufficient in this respect because conditions in India differ a lot from those in the West and hence recommendations in Western literature cannot be directly implemented here. We have been talking of rational, ethical\medical care. All this talk has no practical relevance because in spite of whatever health education we do, patients as a social layer by its very nature can not acquire enough strength and expertise to regulate medical practice. Universal Medical Insurance will change this picture. In doctors' interest also Such a regulated medical practice is in the interests of doctors also. It will ensure a fair competition amongst them. Doctor's income would depend upon the quality of diagnostic and the therapeutic interventions, on the rapport with patients. Today, there is a lot of insecurity amongst doctors in the cities because of increasingly fierce competition amongst doctors. Some doctor’s employ unfair means and many others are forced to follow this trend to survive and grow in the increasingly fierce competition.

Insured medical practice would not in itself reduce urban concentration and competition. But it would prevent unscientific practices including unnecessary medical interventions. A large majority of doctors would like to keep away from malpractice and insured medical practice would help in this. Not by UMI alone Universal medical insurance would have its own problems. Payments to doctors may become plagued by bureaucratism and corruption. But any solution has its own problems. These problems would have to be solved. Since in any case I feel, there is no other effective measure available to control the content of medical practice. This measure has to be a part of a broader National Medical Care Policy in which other aspects of medical care like Medical Education, Drug-Industry are also reshaped on a rational, pro-people basis. Specific measures will have to be taken to reduce the congestion of doctors in the urban areas and the paucity of doctors in rural areas. It would be meaningless to have Universal Medical Insurance if capitation fee medical colleges, concentration of doctors in urban area, use of allopathic drugs by non-allopaths not trained in allopathy, production of irrational drugformulation ...etc... continue as is happening today. Universal Medical Insurance would not itself curb these unhealthy tendencies. For example, urban concentration would reduce because doctors choosing to practise in rural area would now earn a good living even there, since now the purchasing power of the patients would no more be a problem. But this in itself would not be sufficient to eliminate urban rural disparity in the availability of medical services. \

The experience of ESIs One doubt may be raised about UMI (Universal Medical Insurance) - "The E.S.I.S. for workers looks good on paper, but it has failed; similarly UMI is also likely to fail." I feel that ESIS has not been very successful because it is part of the mixed system in which both private an" insurance medical practice is allowed, "The private practice incentive" sabotages the insurance scheme like in all mixed schemes. In UMI, no patient would be paying directly to the doctor nor would there be two types of prescriptions-private' and insurance prescriptions.

So there would be no scope for doctors in discriminating amongst different types of patients. There will of course be some unhealthy tendencies and malpractices. But on the whole, under the present circumstances, UMI appears to be the best means to create conditions for rational, ethical medical care available to all. How to finance UMI? To implement UMI, the Government will have to spend many times more on medical care than what it is doing at the present. Today, it is spending a mere' 1.17% of GNP (Gross National Product) as compared to about 7 --11 % of GNP spent by developed countries and 5% recommended by WHO for achieving the goal of "Health for All by 2000 A.D." In fact the health expenditure of the Government of India has declined in relative terms from 3.3% of plan outlay in the First Plan to 2.9 % in the Sixth Plan. Even Sri Lanka is spending more than this. India is wasting so much money on so many uneconomic and wasteful projects and on so called defence expenditure. All this waste must be stopped and the resources be used for health care. The per capita private health care expenditure in India is four times that in the public sector. A large part of this is wasted in wrong kind of medication and other medical interventions. If the Government raises its health care expenditure upto 5% as recommended by WHO, and if this much of expenditure is not sufficient to finance UMI, people may be ready to pay for a special health-tax because now they would not be forced to waste money on irrational medical care. Medical profession is at cross-roads today. The medical system requires a complete overhaul. A large number of doctors are becoming more and more uneasy about the fierce competition amongst medical professionals. The lay people are of course looking for a change in the medical system. Now is the time to push forward reforms which will on the one hand solve the problem of increasingly costly exploitative medical care and at the same time win over the majority of doctors. I feel that UMI should be one of the central pillars of our demand for a complete overhaul of health care in India.

Discussion at Wardha meet On National Medical Care Policy [A Summary Account] The customary restriction of medical care to curative hea1thcare, places medical-care in a sharp contrast with the health-care per se. The paper opens up with a critique of this far stretched polarity. Medicine as a therapy or science and medicine as a social institution has come to "be inseparable Certain political and commercial pressures conditioned the colonial medical care policy.

The National government adopted it spontaneously without clearly enunciating the underlying policy principles or successfully implementing it. The expected commitment, that, the medical care is a right of every citizen, is no~ reflected in the NHP. Instead, what we have is a widespread but selective expansion of medical services. [Contd. on page 32]

LET US RESOLVE The December 6th events at Ayodhya followed' by wide spread carnage all over the country and the continued pillage in Bombay have nearly brought our frail Republic back to barbarism of the middle ages only with modern gadgets of killing and rampage. The fascist forces have been able to whip up the parochial instincts and sentiments driving voice of reason and sanity into hiding. And then the inevitable follows, subjugation of individuality, human rights, women, dalits, the poor and the reasoning minds, all in one stroke. Time and again, barbaric communal forces have sabotaged mass movements for socio-economic justice. We are being defeated again just like we beat a retreat into partition. Communalism is the easy and happy choice of barbaric elements trying to seize power and control over the people. The Ayodhya dispute which was at the most a mere archeological debate was 'resurrected' into a 'national issue', for those very ends.

Unfortunately all the major political parties have been a party to such ends by overt or covert means. Letting down Shahbanos in the tW91tieth century by a strong Govt. was no less barbaric than the ‘pre-historic’ killing of a penancing Shudra by 'Ram' who represented the state or his abandoning of Seeta. History of Modern India is replete with instances of the State indulging in crassly communal, casteist and parochial methods. No post-independence govt. can absolve itself pf this sin. The issue is of creating a due space and opportunity for secular options, of making individual freedom a reality. This is no easy task after the Ayodhya Kand; with a weakening state power and the elite sections switching on to the communal rhetoric, and minority community already in the ghettoizing process. Another major stumbling block is the severe rural backwardness which makes casteism

an endemic existence. . Let us not hide behind scenarios of global communalisation or seek solace in historical interpretations and arguments. The situation demands that all sane individuals and organisations galvanize into a secular front that drives religion and caste back into their holes and cleanse the atmosphere of medieval elements and slogans. Nothing is more urgent than this.

Those who in the name of Faith embrace illusion, Kill and are killed. Even the atheist gets God's blessings Does not boast of his religion; With reverence he lights the lamp of Reason And pays his homage not to scriptures. But to the good in man. The bigot insults his own religion When he slays a man of another Faith. Conduct he judges not in the light of Reason; In the temple he raises the blood-stained banner And worships the devil in the name of God... All that is shameful and barbarous through the Ages, Has found a shelter in their temples Those they turn into prisons; 0, I hear the trumpet call of Destruction! Time comes with her great broom Sweeping all refuse away. That which should make man free. They turn into fetters; That which should unite. They turn into a sword; That which should bring love From the fountain of the Eternal, They turn into prison And with its waves they flood the world. They try to cross the river In a bark riddled with holes; and yet. in their anguish, whom do they blame? o Lord, breaking false religion. Save the blind! Break! 0 break The altar that is drowned in blood. Let your thunder strike Into the prison of False religion, And bring to this unhappy land The light of Knowledge. 'Later Poems' (False Religion)

Ravindranath Tagore

The search for a rational medical care policy is grounded in the above considerations. The debate at Wardha meet was focused on 1) The rationale behind the proposed three-tier medical care policy 2) Technicalities of such a programme in the light of our experience with the public health care machinery. 3) National Medical Care Policy looked at, from a wider political and International perspective. The three-tier policy implies free medical services to the privileged poor. The proposal, firstly, of free medical services and secondly, that of privileging a section, is questioned. Most of the population (80%) is poor. Yet, despite of the wide-spread public health sector and irrespective of the class of the clientele, private sector has maximum utility. Identifying the poor with the public sector and rich with the private sector is therefore invalid. Public health services won't become more accessible to the poor by debarring the affluent from the public sector. Besides the private medical services, available and affordable to the affluent section, are not necessarily rational medical services. Only a comprehensive, universal and a rational medical-carepolicy would make the irrational and unregulated private practice redundant. The three-tier system purports to protect rights of the deserving population in the labyrinth of freely operating market economy. Recognition of the privileged should be incorporate at the perspective level rather than artificially including it on the 'programme. Exclusiving the poor and letting the market economy float freely may result in serious distortions. Even when the three-tier system is accepted at the programme level, difficulties are foreseen.

Editorial Office Dr. Sham Ashtekar, Dindori, Dist. Nasik 422202. Editors Dr. Sham Ashtekar, Anita Borkar Subscription / Circulation Enquiries Dr. Anant Phadke 50, LIC. Colony, University Road, Pune 411 016. India. Published by Dr. Sham Ashtekar for MFC Printed at Impressive Impressions, Nasik. Ph [0253] 73744 Typeset by. Paper Chase, Nasik. Ph [0253] 73564

Demarcating the income level is problematic, because of the perpetual destabilization of all economic predictions. Some years ago, above 300 Rs. income groups was not entitled to free services. Issuing poverty cards and entitlement certificates might involve tremendous local politicking and corruption. The rural-urban state discrimination is also not valid. Rural areas are not devoid of affluent sections, which, however, do not pay taxes. Private medical personnel and establishments are swarming the rural markets. The poor population cannot be content with a very general health care while the affluent get specialised medical treatment. PHCs are underutilized and the private sector seems inescapable. Regulation of the private practice in the context of CPA and in the broader context of NHP is therefore, the main contention. Apart from the corrective measures through a redressal forum, there are two ways of limiting medical malpractices. I) Universal Medical Insurance Scheme 2) Standardised but graded private/public medical care system. Some suggestions regarding the state medical care infrastructure came forward. I) Rather than gradation in health care facilities, gradation in health insurance system is more feasible. 2) Control over allocation of resources and implementation has to go to decentralised pubic bodies. 3) The poor shouldn't be taxed for medical-care-services. 4) Let the present, potentially non-curative, preventive health infrastructure continue, but with the 5-6% sanctions promised by the government in the WHO Forum, People will be prepared to pay after this initial incentive.

-Maya Nirmala

Views and opinions expressed in the bulletin -are those of the authors and not necessarily of the organisation.


