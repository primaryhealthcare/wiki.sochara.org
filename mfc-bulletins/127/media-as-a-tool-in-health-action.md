---
title: "Media As A Tool In Health Action"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Media As A Tool In Health Action from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC127.doc](http://www.mfcindia.org/mfcpdfs/MFC127.doc)*

Title: Media As A Tool In Health Action

Authors: Balasubrahmanyan Vimal

﻿ -­

127

medico friend circle bulletin

APRIL 1987


Media as a Tool in Health Action
	

Virnal Balasubrahmanyan

Can the general mass media be used by the rational health movement to influence people's attitude, under­standing and behaviour on health matters? As one who has been writing on health issues in various newspapers and journals, I feel this is a topic which needs to be thought about seriously by activists in the movement. People at large can understand the deeper aspects of the politics of health only if they have exposure to adequate information and analysis on the subject-the sort of information confined at present within the 'alternative media' of health activists and progressive social scientists, and circula­ting therefore mainly among the 'already converted'. Although in recent years there has been more coverage in the mass media on specific health issues, like harmful drugs, there has been little enlightenment of the public on what health-for-all really means, what people must do to change their own outlook and lifestyle, or what kind of economic and social measures are needed to be implemented as part of health policy. The public still equates health-for-all with sophisticated medical-care-for-all. which in turn is equated with costly diagnostic facilities, surgical procedures and 'technologised' medicine. In the public mind, the low health status of the people is a result of inadequate medical facilities and hence the clamour for more doctors, more hospitals and more CAT-Scan units.

The serious progressive journals which comment on these issues have an extremely limited, intellectual and academic readership. So, the question facing the health movement is: can a sustained consciousness raising campaign be directed towards the lay public, using the mass media as one of the tools?

One must begin by acknowledging that this kind of health education must necessarily explode a lot of myths and this will inevitably mean that powerful vested interests are not going to like it one bit. To be credible and convincing to the public, this task should not therefore be left to journalists alone, for two rea­sons.

(a) General articles on health issues (as opposed to reportage on, say, harmful drugs, which has both news value and sensation value) carry greater convic­tion when the information and advice comes from qualified health personnel. Readers are far more receptive to health advice from a column whose by­line is a 'Doctor Somebody'. (b) The medical profession is itself hostile when a non-medical person exposes malpractices in the area of health care. Health columns which seek to en­lighten the public on the politics of health wi1l nece­ssarily tread on Establishment's toes. And when such articles are attacked by qualified and senior medical personnel, often with fancy designations, the public doesn't know whom to believe, the doctor or the journalist. On the basis of some of my experiences while writing on health issues, I would like to offer some ideas on how the mass media can be used by the health movement and those sympathetic to it, to further the cause:

1. Health activists, especially those with medi­cal degrees and designations, must acquire media skills, establish contact with editors, and introduce regular health columns in the print media. The help and guidance of sympathetic media people can be enlisted to ensure that these columns are written in appropriate language and style (Dr. P. k. Sarkar’s column on Harmful Drugs in the Calcutta Telegraph and LOCOST's Education Service published in the Sunday Express magazine are examples of the kind of initiative needed). 2. Constant and tactical use of letters-to-the­-editor columns should be made to: initiate debates; get across information on crucial issues; and refute distorted information planted in the media by vested interests. (See example of ORT narrated later in this article). 3. Qualified medical people, especially those in high positions, who may not be 'activists' but are progressive in outlook, should come out on vocal support of any campaign launched by activist groups. This could be in the form of a formal statement issued as a press release or just a letter to the editor. Such endorsement greatly enhances the credibility of the campaign in public eyes. For example, media coverage on the rational drug policy campaign helped in creating some public awareness and even succeeded in getting government's pro­-industry proposals stalled for several months. But the movement would have been more successful if the 'eminent' members of the various government appointed committees, which have in the past called for an essential drugs policy, had realised the need to issue statements supporting the demands of the All India Drug Action Network. 4. Health activists must feed information more effectively to committed journalists so that timely coverage of important issues is ensured. The health movement should actively seek out and identify sym­pathetic media people and keep up a steady flow of information to these contracts so that consistent media coverage on health matters is maintained. 5. It is sometimes more useful to feed information to a news agency rather than to a staffer or a free­lancer whose report can appear in only one paper or journal. An agency report on the other hand has the potential to appear in a large number of papers all over the country. For example, when the MFC critique on painkillers was published, I realised that any article I might write on it would only appear in one of the weekly journals to which I contribute. So I passed it on to a friend in PTI, persuaded him to do a short item on it, and this subsequently appeared in several newspapers in different parts of the country. 6. Specific health actions should be planned and published with an eye on factors like news value and topicality-the two holy criteria for the media people. For example, a critique of cholera vaccina­tion programmes should be timed to coincide with the drought or flood situation when such programmes are usually conducted.

7. If an issue needs coverage in the press, a suitable and timely write-up should be released to the media and this statement should be short, straightaway men­tioning the highlights in the first two paragraphs. A long research type report, often polemical in langu­age, may be all right for publishing in the alternative media, but very difficult for use in the general media. It is my impression that health activists do not fully realise this and are often disappointed when reports they send to the press don't get published.

8. The media has to be consciously used by the movement in as dynamic a manner as it is currently being used by the Establishment and Industry. The PR departments of the drug firms work overtime to present a favourable picture of the industry and its products and the public swallows it all. When a harmful drug gets a great deal of adverse publicity, the industry manages to get spokesmen from the medical profession to defend its product and such statements are reported prominently with big head­ings-leaving the public thoroughly confused as to who is right. Unless these statements are swiftly challenged and refuted, the public will only get dis­torted information. And this kind of counteraction can only be effective if the health movement main­tains the same kind of contact with the media which the industry does, and demands the same kind of coverage and space which the industry is at present getting.

9. The potential of radio and television in serving the cause of the health movement remains almost totally untapped. Some recent programmes on television indicate that immense possibilities exist. We are so obsessed with the notion that radio and television, being government controlled, are not 'free' that we have overlooked the fact that it is preci­sely because they are government controlled that they have a good potential 10 serve our purpose. The government, at least on paper, is committed to all kinds of radical ideals, and so it should be possible for the health movement to demand time on these media for health education of the public. Talks, interviews, features, even serials could be planned, with the help of sympathetic and progressive producers on these media to get across information and messages. Since the initiative to do all this won't come from the government, it is up to the health movement to lobby for effective use of the government media to achieve al least those radical goals


2

­


which have been accepted in official policy.

The fact is that the different forms of media have been generally thought of as areas of expertise beyond the purview of those not specifically trained to handle them. But activists in progressive movements must realise that it is not difficult to learn to use the media to serve their cuase, and for this they have to begin by going to the media instead of waiting for the media people to come to them.

The ORT Example: Some points raised in this paper may be illustrated by a recent experience.

Since the early 80s, there has been a lot of UNI­CEF-inspired coverage on ORT, which has acquired the image of a miracle solution to Third World diarr­hoeal deaths. ORT as a phrase has become associat­ed with 'poverty' and 'children'. Nowhere does the ORT message clarify that this is a therapy for all rich and poor, adults as well as children. Nor is the ORT slogan accompanied by a statement to the effect that most of the commonly used anti-diarrhoeals are irrational and harmful. In the eyes of the middle ­class newspaper reading public, even today after a veritable media blitz on ORT, the salt-and-sugar remedy is seen as something meant for the very poor­ those who can't afford to buy anti-diarrhoeal drugs.

Although I have written about this aspect of the ORT mystique in various serious progressive forums, I felt it's important to focus on it in a typical Estab­lishment paper whose readers are unlikely to have been exposed to this understanding of the ORT issue and who are also likely to be the kind to consume irrational antidiarrhoeals, either self-prescribed or prescribed by doctors who are themselves unconvinced about OR T.

It has been my experience that both editors and readers are suspicious of controversial health infor­mation corning from non-medical writers. So, instead of an 'advice' piece, I did a short 'news report' based on Health Action International's Diarrhoea File, and this I sent to the Open Forum of the Hindu, which declined to use my item on that page (probably because I am not a doctor and therefore not compe­tent to write on matters medical!) and placed it instead in the Letters-to-the-Editor column. Anyway, my purpose was served.

My piece specially mentioned that many doctors themselves do not promote ORT but prescribe irra­tional drugs. Not surprisingly, there was an immediate response from a Madras doctor, asserting that ‘doctors know best’ and defending some of commonly consumed irrational preparations (which one assumes he was in the habit of prescribing).­ This was immediately followed by a letter from an eminent Madras doctor, who is wel1 known to the readers of the Hindu, defending the statement of his younger colleague, and condemning the misinformed sensa­tionalism indulged in by 'journalists and politicians'.

I now stood thoroughly discredited in the eyes of the Hindu readership, and all because I had tried to share with them the information I had regularly been receiving from health action groups and from no less a source than the WHO-supported journal, Diarrhoea Dialogue, published from London! I realised that my own rejoinder would not carry enough conviction unless my stand was also support­ed by doctors. I therefore sent photocopies of my original item and of the responses of the Madras doctors to doctors in health groups all over the country and requested them to respond to the Hindu.

Of the 16 letters sent by the health activists, the Hindu published about eight-all deploring the attitude of the Madras doctors, welcoming the critique which I had offered, quoting texts to support it, and endorsing my statement on irrational antidiarrhoeals. Some of the letters also used this as an occasion to explain why a rational drugs policy alone can prevent the consumption of harmful and unnecessary drugs, and why it is necessary for socially conscious journalists to inform the public on such issues. There was absolutely no answer to any of this from the Madras doctors, and one assumes that as far as the Hindu readership was concerned, the point about ORT had been adequately made in fact, more effectively pro­bably than if my original item had not raised the ire of the Madras doctors. Because, as a result of their intemperate response, the issue was kept alive in the Hindu's columns for almost two months afterwards. This little episode shows that: J) It is not easy to get radical health information into the Establishment media; 2) Even when one 'smuggles' it in, there is opposition from the medical profession itself; 3) When a doctor contradicts a journalist on a health issue, it is the former's words which carry weight, regardless of how well-informed and scientifically sound the latter's argument may be; 4) In spite of these handi­caps it is still possible to save the situation and turn it to the advantage of the movement by mounting an orchestrated attack on the views being propagated by the vested interests; 5) In such a strategy, the role of qualified medical people is crucial; 6) The letters pages of the Establishment papers are an ideal forum to introduce information which may not be readily accepted in the news columns or the features’ pages. This fact should be more consistently exploited by the movement; 7) The letters pages also need to be used swiftly to refute every kind of distorted information which 

3 aimed at misleading the public. For example, when a sophisticated and elitist diagnostic service displays a full page advertise­ment, using the slogan Health-For-All, and announces that the Health minister will inaugurate the building, a strongly worded letter-to-the editor from activist doctors should set right the picture in public eyes: that Health-For-All does not mean costly medical services for the affluent, and that it is scandalous for the Health minister to be a party to this perpetuation of falsehood. areas of Science and Technology for women and water resources development in rural areas. Projects pertaining to health risks in women's occupation will also be considered.

Write to: Dr Kamala Kumar, Dept. of Science & Technology, Technology Bhavan, New Mehrauli Road, New Delhi-l 10016.

Form IV

(See rule 8)

However, this type of alert action has so far not come naturally to the health movement, because they haven't really thought about it seriously. Taking the ORT episode for instance, many progressives who read the Hindu may have silently criticised the Madras doctors' statements but did not realise that in public interest they must sit down and write re­joinders. The activists who did write had to be contacted and briefed, and their subsequent me of the media to put across the viewpoint of the move­ment was part of an organised strategy. For, this issue of using the media more actively was something we had discussed earlier, during a conference in December 1985 on 'Pharmaceuticals and the Poor'. And so, when the Hindu incident cropped up, every­one was alive to the need for swinging into action.

(Adapted and condensed from a background paper, The Media, The Message and Health For All prepared for a seminar on 'Health for AIl: Concept and Reality', organised by the Foundation for Re­search in Community Health, Bombay, on November 15 and 16, 1986)

I. Place of Publication

2. Periodicity of its publication

3. Printer's Name (Whether citizen of India?) Address

4. Publisher's Name (Whether citizen of India?) Address

5. Editor's Name.

(Whether citizen of India?) Address

New Delhi 110029

Monthly

Sathyamala

Yes

B-7/88/l, Safdarjung Enclave New Delhi-I 10029

Sathyamala

Yes

B-7/88/1, Safdarjung En­clave New Delhi II 0029

Sathyamala

Yes

B-7/88/l, Safdarjung En­clave New Delhi II 0029.

Background papers of the XIII Annual Meet on "Family Planning: Theoretical Assumptions, Imple­mentation & Alternatives" available from the Convenor's Office, 1877, Joshi Galli, Nipani, 591237, at Rs 20/- per set.

6. Name and address of individuals who own the newspaper and partners or share holders holding more than one percent of the total capital.

Medico Friend Circle Bulletin Trust, 50 LIC Quarters, University Road, Pune 477073

The Department of Science & Technology IS inviting project proposals to support research in the 

4 Report of the proceedings of the VB Annual General Body Meeting of MFC held at KAY A OD 28th January 1987

The VII AGM of MFC was held immediately following the Annual Meet. Fifteen members were present. Despite lower than usual attendance, several key decisions were taken at this meeting. A brief report is presented here.

1. Bhopal Intervention: (i) The: Convenor reported on the discussion held at a meeting convened by Anil Sadgopal at Wardha to discuss the question of a fresh epide­miological study of gas victims in Bhopal. As things stand, the future of this study remains uncertain. Sagar Dhara and Nishith Vora from Bombay proposed to take initiative in this matter and have requested mfc to­

(a) make the raw data of the mfc study available	for a computer analysis,

is motivated and competent to utilize it for further study including a computer analysis. Such a person/s should go to Mangrol and may take Zerox copies of the data with them. The original data should not be handed over to anyone. Moreover, the data should not be passed onto a person not known to mfc.

(d) The person/s using this data for further study should discuss the plan of their study with mfc especially in case of converting it into a programmable format.

(ii) Pregnancy outcome survey: Sathyamala reported on the present status of the survey report. It is expected that the final report would be ready by March end.



(b) take up the technical responsibility for a long term follow up study in Bhopal.

There was a long discussion on the pros and cons of letting mfc data being used for computer analysis. Fears of the data falling into the hands of people other than Nishith and Sagar and of its misuse by them were expressed. After much deliberation, the following decision was taken.

(a) Though earlier it was decided that mfc would not go in for such a study if it did not have a direct bearing upon the compen­sation case (see minutes of the VI AGBM), it is now thought that in the light of the possible use such a study may have in future, against hazardous technologies in general and also because, the question of its accepta­bility as evidence in Indian courts is likely to remain open till such an attempt is actually made, mfc should participate in such a study. Other provisos decided earlier remain as they are.

2. Publications: Sales and Distribution: The con­venor reported the existing stock held by VHAI as well as mfc members. It was stressed that the sales of all the three anthologies need to be expedited to ensure speedy returns on investment so that enough money is made available for the IV anthology. To begin with, all the members should be requested to sell at least five copies each.

3. Medical Education Anthology: Dhruv Man­kad, the editor of the Anthology reported that except for 2-3 articles which have been returned to their authors for rewriting/adapting, all the others are ready. The manuscript will be ready to go to press by March end. Suggestion for quotes, cartoons and the title were called for.

Even after much discussions and juggling around with suggestions like making of a Doctor, Training Doctors, Diagnosis and Treatment of Medical Edu­cation, what is wrong with Medical Education: Critical Reflections, Towards an Appropriate Doc­tor's Training and so on, no decision could be reached on the title for the anthology.

(b) As of today, mfc is in no position to commit itself for five years. A short term commit­ment is not ruled out and may be made once the plan of such a study is known.

(c) The raw data of the mfc's March study should be made available to anyone who'

4. Rational Drug Policy Cell: The cell co-ordi­nator, Anant Phadke updated the GBM on the acti­vities of the cell. He drew the member's attention to the just announced Drug Policy. He pointed out several lacunaes and weaknesses in the policy which intentionally or otherwise have given a free hand to the pharmaceutical to

5 continue to produce irrational, costly and unnecessary drugs, while allowing for higher profits on all drugs including those used in the National Programmes. It was decided to stress on building public awareness by using means such as writing popular articles, contacting local branches of professional organisations to enlist their support and to launch signature compaign to make known our stand on the drug issue. One such compaign is already under way in Maharashtra.

5. Bulletin:

(i) The Editor reported on the decision taken at the Editorial Committee meeting on the 25th. It discussed the proposal of converting the monthly bulletin into a bimonthly one. Such a move would give the Editor more time to pursue other matters of choice and necessity and also give more space for long articles. But this proposal was not accepted because it was felt that if the bulletin is to serve as a medium of debate, a role that seems to have been overlooked by the rea­ders of late, it must be published frequently enough to maintain a sense of continuity in debate. Two month period is too a long a gap for that.

It was also decided to identify the potential reader­ship more clearly and to formulate a pointed Editorial policy to make the bulletin meaningful publication for most of the readers. Several commitments were given for writing in the bulletin. in time for them to study it. Follo­wing this decision certain priority areas were identi­fied: a) Child survival b) Medical Technology c) Toba­cco and Health d) Objectives of Primary Health care e) Sexual practices, sexual health in India. It was further agreed that unless some important topical theme comes up, one of these topics should be selected for each of the next five Annual Meets. The theme chosen for the XIV Annual Meet was Determinants of Child Survival: Issues and Alternatives. It will be held on the 27th, 28th January 1988 at Jaipur. This decision of holding it once again in Rajasthan was taken in the light of several opinions expressed at this meet regarding the positive impact of holding an Annual meet in an area where not much rethinking is being done in the field of health and medicine.

As an alternative, KSSP would be sounded out regarding its ability and willingness to organise the meet in Kerala.

7. Organisational Matters: (a) Election of the EC, Mira Shiva, Vlhas Jajoo and Ashvin Patel were elected in place of Daxa Patel, Thelma Narayanan and Sathya­mala who completed their two year term. Narendra Gupta was re-elected. Marie D Souza, Mira Sadgopal, and Ravi Duggal continue to be on the EC for the second year.

(ii) The Medico Friend Circle Bulletin Trust, formed exclusively to publish the mfc bulletin, has now been registered with the Charity Commissioner, Pune under the Bombay Public Trusts Act. (iii) It was decided to retain the existing editorial committee with the addition of Kamala Jaya Rao.

6. Mid Annual Meet: After much discussion it was decided to hold the next Mid Annual /CG/EC/ Meeting on 4,5,6 July 1987 at Community Health and Development Project, Pachod. This venue was selected in response to a long standing invitation from Dr. Ashok Dyalchand and the pachod team.

Annual Meet: After a long discussion centering on weaknesses in organising the meet identified during the past meet, it was dicided that in future, mfc should plan out Annual Meets well in advance-may be two to three years-so as to give adequate time for the members to prepare themselves on the topic as well as to ensure that the background material reaches the participants well in 

8. Summary of Org. Decisions: It was noticed that during the Annual Meets or during other meetings, time and again situations have arisen, where in past decision of mfc have been referred to. Those who have joined mfc recently are not aware of the conditions under which such a decision was taken. This has caused some confusion and even misunderstanding in the minds of new comers. To avoid such a situation it was agreed to prepare a summary of the important organisational decisions taken by mfc in the past clearly spelling out the logic behind such a decision. Mira Sadgopal agreed to prepare a note on decisions taken upto the Jamkhed. Meet and Anant Phadke took the responsibility of following it up with decisions taken thereafter.

Introduction to MFC at the Annual Meet Since introduction of mfc as well as that of the Meet has come to be a regular feature of Annual (Contd. on page 8)

6




