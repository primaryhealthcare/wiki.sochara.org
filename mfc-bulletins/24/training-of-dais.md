---
title: "Training Of Dais"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Training Of Dais from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC024.pdf](http://www.mfcindia.org/mfcpdfs/MFC024.pdf)*

Title: Training Of Dais

Authors: Sadgopal Mira

Medico Friend Circle Bulletin

DECEMBER 1977

KISSA KHESARI KA Kamala S. Jaya Rao* IF the nutritional diseases present in a country, some are seen throughout and may be considered to be universal in nature. Others are confined to only certain regions and are referred to as endemic nutritional disorders. Endemic diseases are defined as those which are present indefinitely or continuously in a localised geographic area. But another feature peculiar to endemic nutritional disorders in our country is that they are generally to be seen only among the poor. The question is why? Is that not a naive question if not downright stupid? Who does not know the answer? The people are ignorant, superstitious, have food taboos and hence they do not eat proper food. Of course, they also do not cave money to buy enough food. Hence, they suffer from all sorts of deficiency diseases. But you see, I am not talking of such diseases at all. I am referring to those which people, I mean the poor ones, get because they eat food. Does that sound strange? What I specifically have in mind today is what I would like to refer to as the story of neurolathyrism, or if you Want it in Hindi -Kissa Khesari Ka.

What is neurolathyrism? Neurolathyrism is a neurological disorder characterised by a progressive, spastic paralysis of the lower limbs. The most unfortunate part of the disease is that its peak incidence is between 11 and 35 year of age, thus turning young men into permanent cripples. I say young men because the disease affects male’s ten times more than the females.

The Clinical Picture In a large Dumber of patients, the disease has an acute onset. In others, it may progress through Subacute or insidious stages. The patient experiences sudden severe pain in his calf muscles and sometimes, in the posterior the thighs. The muscles go into

Spasmodic contractions and this is known as ‘lodka’. This is followed by stiffness, genuflexion and the patient finds it difficult to walk. Paralysis sets in either within a week or two of the attack (acute type) or after some months (Subacute type). The patient walks awkwardly with jerky movements and develops a sort of scissors pit. Later, the patient may need the help of a stick to walk and still later, two sticks to support himself. In the final stages, the stiffness of limbs and the bending of knees is so great that he can move only by crawling 1 or by dragging himself on the ground.

Where does neurolathyrism occur? The disease is seen in Bihar, south-east parts of U.P., and some northern districts of M.P. The first report of an outbreak of the disease in India in the Sagar district of M.P. was made by a British officer General Sheeman in 1884. Subsequent reports have come from various places in U.P., M.P. and Bengal. The ICMR carried out an extensive survey in Rewa 2 (M.P.) and the report remains a classic today (despite all the spelling mistakes it contains). I will therefore use this as the main source for my story. From 1884 till the present day the causes leading to the disease have remained practically unchanged. A proof that Bharat can maintain its tradition through the ages! Rewa and Patna in MP at, considered to be endemic areas in India, probably beer use they were brought into the limelight by the ICMR. I am sure that there continues to exist endemic foci even in U.P. and Bihar.

What causes the disease? The answer on the surface seems simple. Eating the seeds of Lathyrus Sativus is the cause. However, * National Institute of Nutrition, Hyderabad-500 007

the answer is not that simple because the question arises as to why the people eat the seeds. Before that, the question — WHAT IS LATHYRUS SATIVUS? Lathyrus sativus is a legume which is extensively cultivated in Madhya Pradesh, Bihar and to some extent in Uttar 1, 3 It is Pradesh; Bengal, Andhra Pradesh and Maharashtra. known locally as Khesari, Teora Metra, lakh, lankalu, etc. It has become notorious as Khesari. The seeds are gray in colour and are sometimes mottled. The dehusked seeds or the dal resembles chana (bengal gram) and arhar (red gram), and is also used for adultering besan (bengal gram flour). The local population which suffers from lathyrism is totally and painfully aware that eating khesari cripples them. Thus they, are not ignorant. Why then do they grow L. Sativus and why do they continue to eat its seeds? The Cultivation of Lathyrus Sativus I would like to mention here that henceforth I will talk only of Madhya Pradesh, for reasons mentioned earlier. The survey by the ICMR was carried out in 1959-1960, and some changes might possibly have occured subsequently. I therefore stand corrected if any statement I make or quote is in variance with the existing situation. But the difference, if any, will be of u minor nature and will not in any way alter the content of my story. Rewa and Satna have a good soil and a good rainfall, and grow rice and wheat in equal proportions. I hope the readers are aware of the mixed, rotation system of crops, in which the harvest of the main cereal crop is followed by a legume crop. Generally chana or arhar is taken in the rotation pulse crop. When the paddy crop is of a long duration and the season has advanced too far for chana, L. Sativus can be easily taken in 4 rotation. The people have however found other advantages with lathyrus: 1. 2.

3.

4. 5.

It is not affected by excess water at the time of sowing, It is not affected by shortage of water during the growing season, It can grow in soils which become hard after paddy harvest, it can be raised as a drought crop, Volume by volume, the seeds are heavier than wheat or barley.

Thus lathyrus is cultivated because it can grow under extremely adverse agricultural and climatic conditions. In Rewa, nearly 19000 acres of land were said to be under lathyrus cultivation. Yet, not all those who grow lathyrus, suffer from neurolathyrism.

On the other hand, those who do not grow it (or for that matter, do not grow anything) suffer from the disease. That my friends, is the main thread of my story…..because those who suffer, if you .have already guessed it, are the poorest of the poor.

The Story In 1960, more than 75% of the rural populations of Bhagelkhand, of which Rewa and Satna form a part, were 2 landless labourers. They were mostly Kols, Chamars and Kochis. Many families were in bonded labour. The wages of the labourers are usually paid in kind, in the form of food grains, which is known as 'birra’. This consists of a mixture of wheat, barley, chana and khesari. The birra is ground into flour and made into chapatties. Lathyrism is very common in this landless group, with many families baying more than one victim. The landowners are a minority, numerically, and needless to say that the disease is very rare among them. The middle group are either small landowners or do share cropping with the big landowners, Lathyrism is also seen in this group though not as commonly as in the landless class. As mentioned earlier, experience has taught the farmers that L. sativus can grow under hostile agricultural and climatic conditions. Apart from providing protection against natural calamities, lathyrus sativus comes in handy for the landowners to distribute the seeds amongst the labourers in lieu of their wages for more than one reason: (a) .... it does not require labour, irrigation and manuring (b) volume by volume, lathyrus sativus is heavier than wheat or bengal gram. Moreover, “Lathyrus sativus, due to its cheap money value, has become the chief means in the hands of the rich to feed the poor and to........extract work". All reports of outbreaks of neurolathyrism pointed out that the disease occured only among the landless labourers, that the payment was generally received in kind as birra (also known as bejhar) and that lathyrus formed anywhere from 25 6, 7 90% of the mixture of rood grains . The investigators of the Rewa outbreak concluded that diet providing more than 40% of lathyrus and consumed for atleast six weeks can lead to the overt manifestations of the disease. In Madhya Pradesh, the Kharif crop is harvested in October-November and people get rice and wheat for consumption. As time passes, they consume more and more of lathyrus as the availability of cereals becomes less. Lathyrus

sativus is a rabi crop sown along with wheat and chana, and

is harvested in February-March. Birra between May and September

was found to contain not less than 50% lathyrus sativus. The incidence of neurolathyrism was found to be highest between July and September. During a drought period, it was found that birra contained upto 75% lathyrus. “Only landowners and... middle class peasantry could save wheat and gram for their consumption. The 2 poor labourer class had to subsist on Lathyrus sativus only. " It is thus found that lathyrus sativus is a hardy crop, which can grow under circumstances which the more 'sophisticated' crops cannot withstand. Thus it is what is known as a life saving subsistence crop (Whether the life, as it remains after eating lathyrus, is worth saving being an entirely different question). India, one should remember, is the land of ahimsa, a word freely translated into English as non-killing). This advantage of lathyrus sativus being a drought crop has turned into a fortune for the landowners and a gross misfortune for the landless.

The Recommendations The investigating team of the ICMR survey' made some recommendations for prevention and preface these by the statement, "the disease is not only a public health problem, but that it also involves socio-economic and agricultural problems prevailing in the population. The problem of lathyrism is a challenge to all those who are interested in the promotion of health and welfare of the people of this region” (the emphasis is mine). In fact, but for the occasional off-key Dote where the authors imply that the people may prefer to eat lathyrus, I consider the ICMR monograph a brilliant essay. The recommendations include the following: 1. Banning of the crop - "An important practical difficulty would be to provide the people an alternative, suitable crop in place of Lathyrus sativus." "Lentil, bengal gram and jowar were also suggested. These crops are already in the region. 2. Lathyrus sativus be gradually withdrawn in exchange of wheat or other suitable cereals. 3. Improvement in agricultural methods - "lack of irrigation is the main problem and is the main cause of practice of lathyrus sativus cultivation. There are two great rivers, the Tons and the Son, which can be used to develop canal irrigation. Digging of wells and tube wells should' also be considered. 4. Lathyrus sativus should not form 'more than 25% of the total diet.

The Follow up Till now, the story was concerned with the discovery of the disease and its causes. The next important thing naturally is to know what has happened in the fifteen years following the ICMR survey in

Rewa; a simple answer would be—nothing. Yet that is not entirely true. Something has been done; but how much has it help? We will look into this issue in two parts —action by the scientists. At the outset, let me admit that do not know what efforts the government has undertaken to prevent neurolathyrism in the country or whether is has of has not accepted the recommendations of the ICMR. I do not know whether the incidence of the disease has come down or increased in these years. Discussions on this condition at scientific meeting or scientific and statement from any quarters that the disease has

disappeared or has been on the decline. Thus, one may conclude safely that the situation exist status quo. On the other hand, a fresh outbreak of the disease has been reported from Madhya Pradesh in 1974.

The Government's Action The government of India issued a ban under rule 44 - A of Prevention of Food Adulteration, in 1961. “No person in any state shall with effect from such date as the State Government concerned may by notification in the Official Gazette specify in this behalf, sell or offer or expose for sale, or have in his possession for the purpose of sale under all; description or for use as an ingredient in the preparation of any article of food intended for sale—wilt special reference to khesari (Lathyrus sativus) or it dal or a mixture of khesari with bengal gram or any other gram.” Have you read the above notification carefully' I do not know whether the government of M. P., U.P. and Bihar did notify in their Official Gazelle, the date on which the ban should come into effect. Assuming they have done so, if you have read carefully, the, rule prohibits the sale of khesari but not its cultivation nor it being given as wages. I do not know whether irrigation projects on the two rivers as recommended by the ICMR team, have been undertaken with a view to facilitate bring more area under better food crops. For reasons discussed earlier, one may take assume that whatever steps the government had taken if any were not effective.

The Scientists Contribution The scientists, on the other had, have been more active; or, being one of the flock, I am probably more aware of their achievements'. Some of these are classical examples of how intellectuals, can function from their ivory towers, totally ignoring the socio-economic realities of a situation. Many a time we even believe that the solutions worked out by us are the right ones.

1. At the outset, the scientists had taken the stand that banning the crop is not possible. The agricultural scientists may have had good reasons for stating, "it has been found difficult to 4 let hold of a satisfactory substitute crop." However, one does not know on what basis the nutritionists have stated, "Effective implementation of such measures is not easy. The cultivation of lathyrus has been deeply footed in the prevailing. Agroeconomy of the region, providing a food suitable to the dietary habits and economic level of the poor segments of population?" (emphasis mine). In making this statement, the fact that those who eat lathyrus sativus are not interested in growing it and that their foods habits have been forced upon them are totally forgotten. 2. The agricultural scientists have also not said whether 2 and why the recommendations of the ICMR, for bringing more area under lentil (masur) chana and jowar and for increasing irrigation facilities, are not feasible. They have indicated that masur has good possibilities of a substitute crop", How far this has been followed up, is not known. 3. A negative attitude has been taken also towards decreasing the quantity of lathyrus seeds in the 'birra', "It has also been suggested that the lathyrus seeds produced could be diluted with wheat or barley so that the effective intake of lathyrus would not exceed more than 30% of the total diet. This suggestion too may be expected to have many practical difficulties.”9 Such as what and why has not been specified. Having thus taken a stand, which though not spelt out - so clearly is very much implied, that there are no socio-economic solutions to a socio-economic, problem, the scientists had then proceeded.' .to seek and offer their own solutions. (a) Biochemists very eagerly sought to identify the toxic factor in the' seeds of Lathyrus sativus and to establish its 10 chemical nature. They succeeded in their venture. The work though significant, is largely academic. We may let it be, since it is not of direct relevance to the present discussion. The offshoots of this discovery, on the other hand, are of greater significance. (b) Agricultural scientists have tried to identify 'trains of lathyrus sativus, with low toxin con lent. They have claimed some success in this.11 If this is true and if this continues to work, this would probably be a good achievement. However those who have some knowledge of agricultural science will know that such solutions are companied by problems of their own. (c) Methods have been suggested by which the toxin can be removed from the seeds by simple cooking procedures. It is stated, “such a such a pro-

cedure would overcome most of the difficulties.... and at the same 9 time conserve lathyrus seeds as a useful food source." The local population easily understood this technique of detoxification. The method also did Dot alter the tell or texture of the chapatties. The method is simple and can be followed by even the most illiterate villager. “However, with all these benefit, the home scale method docs not appear to be practicable because of the system of daily payment of wages in the form of lathyrus and the agricultural labourers did not set sufficient time to detoxify 8 it.... Another problem was the cost -of fuel..... " Here then was a scientific achievement which failed to take into account the socioeconomic realities of the situation ... the fact that extra time and fuel would be needed even for a so called simple procedure. Yet, the method continues to and is not considered to have any practical difficulties. While working out solutions (b) and (c), the scientists have failed to realise that, they were placing in the hands of the landowners a power full tool. Now, the landowners can blame the distributing agencies for not providing them with low-toxin seeds 'and accuse tile labourers as ignorant and lazy, for not utilising a Simple solution offered for the relief of their own misery. This then is the sad story of neurolathyrism in Central India. In 1961 it was estimated that there might be 32,000 cripples in the Rewa and Satna districts alone, giving an incidence of 2.6%. Assuming that no change has occured, for better or for worse, a rough estimate would be that the numbers would have increased by another 10,000: that is, nearly 1000 fresh cripples would be added to the population every year. My main reason for discussing this problem is to emphasize once again that most health problems in our country, or for that matter in any other developing country, are basically socioeconomic 'problems. Neurolathyrism is just one example and not something unique. In other parts of the country, there are other nutritional problems: for example, pellagra and flourosis in Andhra Pradesh have a similar story. We should remember that whatever preventive measures we wish to offer for a problem should take into account the socio-economic factors of the population. The treatment should not be worse than the cure. The solutions that have so far been offered to the victims of neurolathyrism, in my opinion, will only aid the perpetuation of the solution. It is therefore important that scientists should not offer solutions which will only help the vested interests, (Turn to page 7)

TRAINING OF DAIS Mira Sadgopal* THIS is a sort of speculative appeal from one who is probing into the activity of dai-training and feels the acute need to communicate with others who have already faced, or are presently faced with, a similar challenge. As a female medical practitioner (MBBS) associated with a rural education and development agency (Kishore Bharati Group) I was recently approached by the local Block Primary Health authorities to assist in the impending programme of training traditional dais to meet the requirements of the Government's revised rural health services plan. My colleagues and I accepted the invitation with considerable interest, as we have done some thinking on the problems in this line over the last year or so. The activity is consistent with our wider exploration of non-formal' methods of education. We have also taken up a three-year Blockwide research and action study of the various categories of traditional indigenous health practitioners, including dais, and their relationship to 'primary health care' such as it is. In the latter context, we are interested in observing the type of responses which arise when traditional dais are put in situations of interaction with government health workers and 'the system '.

The scenario is as follows: There are 126 villages in Bankhedi Development Block, encompassing a total population of roughly 60,000. At crude estimate there are probably about 100 practising 'dais' in this area. Caste-wise, they are predominantly Basoards, an 'untouchable' caste in present society. Only in areas where there are no Basoards, chamar women perform this function. A few Maiter women also attend births. Practising Basoarins range between the ages of 20 and 70, the average age falling around 40. The younger ones are inexperienced and mostly attend calls of the poor. The oldest daises are frequently blinded by cataract, but are sometimes in demand among wealthy households on the strength of past reputation. The middle age range is most competitive about the profession and such dais frequently express shrewd concern about building up a prestigious reputation 10 the homes of the socially elite. Occasionally a woman of another caste will gain a reputation - a ‘khawasin' (barber's wife) has been observed to be called by the family for active diagnostic 'consultation' in cases of abnormal labour. She is able to do a per vaginum examination (with bare hands, of course) and detect the position (attitude) of the foetal head from the direction of the fontanelles. In another case, a socially ostracised Kotwal widow has a certain reputation in managing prolonged labour. At present

neither the traditional midwife caste nor be occasional self-made 'expert' has the lightest thread of a relationship with the organised health services, and when a case is shifted from the dai's care to the visiting nurse or the hospital, the dai ceases to matter. On her part, she suavely ‘washes her hands’ of the case, covering lip a feeling of inadequacy and public loss of face. From the health services point of view, the dai is never thought of as 3 referring agent, and hence is almost always ignored even when she is still present. If she is not ignored, she will still almost certainly be given a one-sided verbal thrashing in the presence of her villagers for her supposed ignorance and unhygienic handling. A wise dai would better be absent, with her independent reputation intact. The training of dais has been a mental challenge to the Directorate General of Health service of the Government of India for many yeans. A booklet entitled Training of Dais was published by the Directorate first in 1960, analysing the profession and proposing a concrete programme in minutest academic and administrative details. The booklet also contains the 'Regulations and Syllabus for the Training on Dais' sanctioned by the Indian Nursing Council. The programme was started in a few selected development blocks of some states. Apparently very little concrete and purposeful evaluation of this programme was ever done and government enthusiasm sputtered and nearly flickered out. A few ten-year-old dai -kits arc to be found in some subcentres of our block in the charge of the ANM. The programme itself never thrived. The state of use and maintenance of the kit is obviously poor, many parts simply rotting from disuse, or used for other things. Occasionally, one comes across a sort of relaxed symbiotic relationship between a good-natured ANM and an untrained dai, a situation which sometimes arises automatically under favourable circumstances. The dai kit does nor come in between the two but continues to collect dust in the deserted MCH clime rooms, along with the white-enamel UNICEF baby-weighing scale. Suddenly, the machine of state has roused out of its sleep, and sees the possibilities of the rural health services in a 'new light'. It realises with uneasy concern that time has been lust, and must be made up. The people are suffering – some might be getting restless. With the Honorable Shri Raj Narain at the banner head of the Health Ministry, a ‘Draft Plan’ has been launched. Almost all of the good-old ideas have been dug out and draped in new garb. Since time is to be made up, regulations are to be relaxed, time limits shortened, and monetary incentives raised.

* Kishore Bharati Group, P.O. Malhanwada Dt. Hoshangabad MP

The dai-training programme is a perfect example of this treatment (see table). A look into basic principles of the Government programme is in order: 1.

Additional village midwives are to be educated to adopt certain modern practices of hygiene and obstetric science, of which they are presently unaware and hence unknowingly promoting hazards to the health of mothers and babies during the birth process.

2.

The adoption and promotion of family planning/ birth control measures are desirable but dubious role which the Government has envisaged for these women, considering that their vested interest naturally lies in a high birth rate.

3.

Trained indigenous daises are envisaged as playing an active role in the nationwide MCH programme, including antenatal and postnatal care. This concept carries with it certain sensitive impracticalities in today's social set-up, considering the untouchable status of the dais as a class, who are usually called upon only at delivery time.

Despite the contradictions inherent in the second and third principles here given, a systematic and sustained attempt to weave the professional dai into the general tapestry of the national health services system is obviously a desirable thing from many viewpoints. However, in order to make any meaningful headway towards the goals, understanding and apprecia-

tion of the particular, situation of untrained dais as a class must actually be incorporated into the training programme and inculcated strongly into those responsible for this training. The latter will probably be the most difficult task of all. Firstly, it is important to distinguish between "students" and the typical nursing or midwifery student, or even medical students. Fundamental differences in social status, daily cultural I and physical environment, prior education and convictions must be measured up. Illiterate dais enter into training with almost half a life of work experience behind them, and no practical understanding of the benefits of modern method. The ordinary educational methods of lecture-demonstration cannot be foisted upon them with good result. Standard teaching aids are not going to be useful without careful testing and appropriate modification. The availability of the most practical teaching material women in advanced pregnancy must be ensured in sufficient quantity. On the other hand, chances to develop certain unorthodox but familiar learning situations, such as guided gossip-type story-telling sessions, should not be missed. For this, each dai will have a fund of past experiences in her memory to contribute from. Maximal association of the dais past experience with newly learned practices is one factor necessary to break old habits and foster the maintenance of newly set standards. Likewise, the course sequence for illiterate but experienced dais must be confidently rearranged to

SOME OUTSTANDING DIFFERENCES IN FORMER AND PRESENT DAI-TRAINING REGULATIONS Indian Nursing Council Regulations (l960†)

GOVT. of India and State Directorates of B.S. (I977† †)

1. Period of training

six months

one month

2. No. of teaching classes

twenty-four to forty-eight

eight

3. No. of ANC Clinic

twenty

(once weekly for four weeks)

4.Supervised deliveries during training

ten

(only two realistically possible)

5. Certified deliveries during training

twenty

(three to four realistically possible)

4.Regulations for training place

Any MCH Centre/PHC

Any PHC (most PHC's have

having Well-establish ANC

ANC Clinic only in name, only

programme and midwifery services.

few deliveries per year.)

Attendance required

† Training of Dais, Directorate Gen. of Health Services, G.O.I., New Delhi, 1964 (2nd Ed.), Appendix 1. † † Recent communications from District Health Officer and District Mass Information and Education Officer (FP), Hoshangabad and Block Extension Educator (FP), Bankhedi PHC.

maintain and develop her interest in learning. Thus, abnormal labour should be discussed such as to lead up to appreciation of the normal rather than the other way round. Late obstetric complications should be taken up before early ones, and anatomy and physiology should be explained appropriately at various points and not in otic indigestible lump in the beginning. Asepsis is' a concept which must be both subtly or overtly introduced and reintroduced in every session, not in a single lecture. Family 'welfare' can be discussed whenever relevant. Another problem to which attention needs to be given is the dai kit itself. For example, rather than including the suggested mercury thermometer, (for puerperal fever), wouldn't it be better to train the dai to recognize fever more simply. The measurement of pulse rate, an important index of well-being, can probably be taught with the use of a simple 1½ minute sand-glass or other simple standard time counter which" could be developed. Scissors could be replaced by a couple of good quality stainless blades, less likely to be snatched, lighter, more easily sterilized, cheaper and easily replaced, The expensive rubber mackintosh can be replaced by' cheap plastic sheeting locally available in half-slit bag form as ' barsatis ' at the beginning of the monsoon weather. Inclusion of rubber catheter is controversial for risk of urinary infection. On the other hand, a well trained dai might have the skill to use rubber gloves, currently not part of the list. Active research is needed to solve these questions. A major stumbling block likely to remain until and unless widespread changes come about in the priorities of medical and nursing education is the interprofessional relationship between the health team members. Mutual respect is the critical factor missing, constantly nipped in the bud by the gnawing temptations of growing capitalist society. Even the dai will sharpen as she enters into the system whore justice and quality has little place without a price. This, however; is beyond the realm of dai-training, Would readers with any further practical insight into these problems respond to my appeal in this Bulletin?

(Continued from page 4)

The role of the MFC 1. I hope there are MFC members is M.P, U.P. and Bihar. Could they meet the local population, who are victims of neurolathyrism, and let us know first hand their problems? 2. Has there been any attempt to improve the irrigation facilities in Rewa and Satna in M.P.? If not, are there real problems or is it just indifference on the part of those who should be responsible for this 3. Is it really difficult to grow any crop other than lathyrus in the area? 4. Has payment of lathyrus in wages been banned or has there been any attempt to do so? 5. Does the government provide drought relief work to the local population and open fair price shops to sell wheat? We hear so much about the surplus wheat that is produced. What prevent this being sent to those areas where the labour can buy it with cash received as payment rather than be forced to accept lathyrus seeds as payment—an outmoded, primitive system of barter. Lastly, whatever the area we may be working in, we should enquire from the so called beneficiaries their own reaction to the solutions that are offered to them and not take upon ourselves the role of all-knowing preachers. As I said earlier, the story of neurolathyrism is only a case in example. Other instances are no less poignant.

References 1. 2.

In Search of Diagnosis

3. 4.

Increasing demand of the back issues of the Bulletin

5.

compelled us to publish an anthology of select articles from back issues. The anthology titled In Search of Diagnosis, contain. 180 pages in pocket book size and is priced Rs. 8/-. Subscribers of the Bulletin and member of MFC can have

6. 7. 8.

Lathyrism –A preventable paralysis. ICMR Monograph published by the National Institute of Nutrition, Hyderabad K.T. Ganapathy and M.P. Dwivedi (1961). Studies on clinical epidemiology of lathyrism, Indian Council of Medical Research. Wealth of India - Raw Material (l962). Vol.6, p. 37, Councilor Scientific & Industrial Research, New Delhi. Pulse Crop. of India (1970). p. 314. p. 9. Ed. P. Kachrao, Indian Council of Agricultural Research, New Delhi. K.K. Govil. B.M. Gupta, S.D. Kapur. N.C. Chakravarty D.P. Bhatnagar and K.C. Pant (1959) J, Indian Med. Assn. 33:499 R.N. Chaudhuri M.K. Chhetri, T.K. Saha and P.P. Mitra (1963) J. Indian Med. Assn. 41”169. K.L. Shourie (1945). Indian J. Med. Res. 33: 239.

M.P. Dwivedi and S.S. Mishra (1975), Proc. Nutr. Soc. India, 19:23

this book at a subsidised price Rs. 6/- only which includes

9.

postage. Please Book your order immediately.

10. S.L.N. Rao, K. Malathi and P.S. Sarma (1969). Wld.

V. Nagarajan (1969). Indian J. Med. Res. Suppl. To Vol. 57, p. 92.

Rev. Nutr. Dist. 10:214. 11. P.L.N. Somayajulu, G.K. Barat, S. Prakash, D.R. Mishra and Y.C. Srivastava (1975). Proc. Nutr. Soc. India 19:35.


