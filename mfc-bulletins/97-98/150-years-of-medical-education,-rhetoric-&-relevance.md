---
title: "150 Years Of Medical Education, Rhetoric & Relevance"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled 150 Years Of Medical Education, Rhetoric & Relevance from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC097-98.pdf](http://www.mfcindia.org/mfcpdfs/MFC097-98.pdf)*

Title: 150 Years Of Medical Education, Rhetoric & Relevance

Authors: Narayan Ravi

medico friend circle bulletin January-February 1984

150 YEARS OF MEDICAL EDUCATION RHETORIC AND RELEVANCE RAVI NARAYAN The greatest challenge to medical education in our country is to design a system that is deeply rooted in the scientific method and yet is profoundly influenced by the local health problems and by the social, cultural and economic settings

I. THE BACKGROUND

Medical Education in India has a long history, dating back to the Vedic period. The development of Ayurveda, the training of ayurvedic doctors, the compilation of textbook (Samhitas) by the physician Charaka and the Surgeon Susruta, the seven year medical course of Taxila University, the contributions of teachers like Atreya and the development of the hospital systems for men and animals under patronage of Rahula son of Buddha are well known facets of this history. With the advent of the Mughals, the Graeco-arabic system of Medicine was also introduced into the country. Through free interaction with the indigenous systems it developed into the Unani system with its own chain of 'Hakim' practitioners and centres of learning.

The systems of medicine evolving in Europe (the Western system) was first introduced into the country by Portuguese in the 16th century with the establishment, of the Royal Hospital in Goa in 1510. Two hundred years later in 1703, a rudimentary form of medical teaching was also started in this hospital, Meanwhile the East India Company had begun its mercantile operations in India and in 1679, the Madras General Hospital W2S built, In 1740, the medical department of the

in which they arise ............. We need to train physicians in whom an interest is generated to work in the community and who have the qualities for functioning in the community in an effective manner.

— Srivastava Report 1975

company was created and the Indian Medical Service established. Physicians and Surgeons of the I.M.S. (all expatriates) trained local assistants, dressers and apothecaries to form a subordinate medical service. The first medical school to train local doctors was established in Calcutta (1922) followed by Bombay (1826) and Madras (1927), William Bentinck appointed a Committee in 1833 to work out the principles on which medical education was to be established in India. The first important recommendation of this Committee was that the "principles and practice of medical science" to be taught would be in "strict accordance with the mode adopted in Europe". This, therefore, set the foundation for the evolution of a system of education cut off from its own local cultural and historical roots and completely Westward-looking in its orientation and direction. The review that follows (Section II) will attempt to highlight chronologically the events, the developments, the experience, the attempts at reorientation and the concerns of medical educationists in the 150 years that have followed this Committee's recommendations. The quotes in boxes are from government reports and authoritative sources.

In Section III an overview of these trends will be attempted to highlight the factors that continue to maintain the status quo of irrelevance. The National Health Policy document of 1982 describing the existing situation candidly confirms that the almost wholesale adoption of health manpower development policies and the establishment of curative centres based on the Western models....are inappropriate and irrelevant to the real needs of our people and the socio-economic conditions obtaining in the country'. II. A CHRONOLOGICAL REVIEW (1833-1983) 1833:

Historical Foundations of Medical Education — "principles and practice of, medicine in strict accordance with the mode adopted in Europe. — instruction ... in English — course should be far 4-6 years — instructions in Anatomy, Surgery, Medicine and pharmacy.... — witness the practice in various hospitals and dispensaries — public services be supplied with doctors from these institutions.

— Crawford, 1916

The Committee appointed, by William Bentinck outlined six principles on which medical education should be established in India The first medical colleges established in Calcutta and Madras incorporated these recommendations. Today, a hundred and fifty years later, these six principles still form the sacrosanct pillars on which the edifice of medical education in India rests"

1833-1932 The century that followed this committee's recommendations saw the following important developments. Medical Colleges were established in the main centres of British India provinces The subjects of chemistry, physiology, ophthalmology. medical jurisprudence, dentistry and hygiene were introduced into the curriculum. Admission of women was introduced and University affiliations for colleges were obtained" Recognition of colleges by the Royal Colleges of London Dublin and Edinburgh was received: All these took place under the strict supervision of the General Medical Council of England" 1933-1945 The Medical Council of India Act of

1933 was promulgated, creating a council to provide uniform and improved standards of medical education in the country. Many provincial governments, missions and private' organisations established another category of institutions called 'medical schools' to train students for the degree of Licentiate Medical Practitioners. This LMP course was for 3-4 years, often in the vernacular and the training was geared to general practice in small towns and rural areas In 1943, the government of British India appointed a Health Survey and Development

Committee (popularly known as the Bhore Committee) to make a comprehensive survey of existing facilities for medical care and suggest the future course of developments" In its four-volume report it suggested that the licentiate course should be abolished and India should produce 'only one and that the most highly trained type of doctor' . Such a doctor was to be a 'basic doctor' combining both curative and preventive functions The committee made important recommendations for curriculum changes to produce such basic doctors. Two features of this report which are seldom highlighted but are very significant are:i.

the committee emphasised that "wide margins should be allowed to individual colleges and universities to experiment with the curriculum and' develop their own potentialities". This was never allowed ill the decades that followed!

ii. the decision to abolish the licentiate course was opposed by a group of dissenters on the committee who felt that 'medical education' in India should develop along the experiences of the 'feldshers' in Russia this being more relevant to the Indian situation. This dissent was recorded in the report but generally ignored.

It must be recorded to the credit of the Bhore Committee, however, that it made a very strong case for a radical, social and community orientation of medical education in India so that it could serve the masses who had been ignored by earlier policies in British India.

1946-1956 India attained independence and the Planning Commission was established. The Constitution of India declared the right of every citizen to public assistance in the case of Sickness…..’…. The first five-year plan \\'as launched in 1951 with the goal 'to provide scientific medical aid to all who needed it and to promote public health and preventive medicine’. Medical education began to develop along with the health services using the, Bhore Committee recommendations as' the blue print, Its social objectives were reiterated in all the policy documents and there was a quantitative growth in services – both in terms of hospital and health centres and medical colleges, heavily subsidised by the State.

The First All India Medical Education Conference was held in 1955 to outline the importance, the scope the aims. the physical and curricular requirements of the departments of Preventive and Social Medicine that were being established in all institutions In spite of all the radical declarations, however, no attempts were made to change the mentality that the senior members of the medical profession and teachers had inherited. nor were attempts made to open medical education to the poorer classes of Society (Banerji, 1977). The elite structure controlled by the educated, \\'estward looking upper classes of India both among the medical profession and the political leadership continued to develop a hospital-based system on British and later American lines which catered mainly to the privileged sections in the community and supported the growing industrial interests. 1957-1967 The Government of India appointed the Health Survey ann. Planning Committee (popularly known as the Mudaliar Committee) in 1959 to assess the progress of medical care and education after two plans and to sec whether the targets of Bhore Committee report were relevant The Committee stressed the need for more realistic targets (modified some of Bhore Committee recommendations) and emphasised the need to shift attention from opening new institutions to improving existing services. In the field of medical education it reiterated the importance of compulsory pre-registration internship with three months rural postings and the importance of field practice areas for the new departments of PSM (where staff of both the clinical and PSM departments should participate in joint teaching program-

mes). This was particularly important since most medical colleges till the late '50's had paid little attention to the PSM departments, its field practice areas or rural posting in internship.

Even the Medical Council of India which was established primarily to maintain uniform standards took time till 1964 when it, for the first time clearly outlined:-

i. a curriculum of social and preventive medicine extending through the course; ii. the administrative, preventive and clinical

objectives

of

rural

training during rotating houseman ship and prescribed it as mandatory

standards

for

medical

education all over the country.

It also stressed that the PSM curriculum was to be a joint programme with all departments to give students ‘a comprehensive picture of man, his health and illness'. Interestingly enough in the same meetings when the social orientation of medical education was being concretised the pressure to keep up with developments and standards in the \Vest were not ignored and the Committee had no hesitation in introducing courses in Genetics Biophysics, Electronics, Space Medicine, Concepts of molecular biology, Radioisotopes and Nuclear Medicine along with Psychology, Statistics, Elementary Social Sciences. Family Planning and all other components (if the teaching in PSM. In 1960, the Indian Association for the Advance of Medical Education (IAAME) was established as an independent forum for exchange of views by medical teachers, Its annual workshops and conference and its publication — The Indian Journal of Medical Education — was meant to create some interest in issues relevant to medical education.

1968-1973 As part of the populist upsurge of the late sixties another medical education committee was appointed to study and recommend the changes in training of doctors in the light of national needs and resources since earlier rhetoric and recommendations were not making much of a dent in the general trends of medical education. For the first time the Committee clearly defined the terms 'basic doctor'. This Committee like the previous one suggested some more changes in the curriculum in order to produce the basic doctors,

In 1972, the twenty-fifth anniversary of independence became an opportunity to review the growth of medical education in the country. Statistics showed that there had been a phenomenal quantitative growth, However, in terms of the people's and country's health needs the developments were far from satisfactory. Qualitatively the curriculum was still very urban-oriented with little emphasis on prevention and promotional aspects of health and, education was cut off from the national programmes resulting in 'rural communities being still deprived of doctors in spite of the increase in total stock'. The Fifth plan document stated that 'teaching in medical colleges still requires a radical change' and in parrot-like fashion it repeated the exhortation that 'the undergraduate medical education would have to be reoriented towards the needs of the country and emphasis would have to be placed on community care rather than hospital care'. 1974-1983 The last decade has seen certain changes and developments which for the first time indicate that -the government and the expert committees are seriously seized with the fact that the medical education developed in the last 150 years has failed to make any impact on the health needs of the masses in our country and that a radical revision in the curriculum - in spirit, methodology and content are required, A special group of expert medical and social scientists were constituted -to study the problem of 'Medical Education and Support Manpower". Taking a very comprehensive view of the matter they have critically diagnosed the existing problem (Box on pg, 5). The Committee made man v recommendations of its own and endorsed some made by previous committees It also sought to stop increase in medical colleges and admissions; to generate a manpower policy along scientific lines on a national basis; to evolve a national system of medicine by integrating modern and indigenous systems of medicine; to establish a medical and health education commission to implement the needed reform: and 'initiating and nursing the change process'. In 1979, the Sixth Plan document realistically stated that "the behaviour of a doctor as anyone else is determined largely by the socio-economic and political structure of the society and not merely the undergraduate medical education, Marked improvements can, therefore, be expected only if and when the society is restructured for social justice. But within the limits placed, we can achieve a considerable amount in changing attitudes, skills and knowledge if the medical colleges

Growth of Medical Education Medical Colleges Annual admissions Postgraduate institutions Hospitals Primary Health Centres Doctor Doctor-population ratio Nurses Nurse-population ratio Health outlay (crores) Outlay on education and training

30(50) 2500 (50) NA (50) 8600(50) Nil 56000(50) 1:6000(46) 15000(50) 1:43000(46) 1400(1) 216(1)

103(72)

13000 (72) 82(68)

14600(63)

5183(71) 115725 (71) 1: 4300(72) 66000(71) 1: 6400(72) 11555(1V) 982(1V)

(Year or plan in brackets) Reference: Rao, 1966 and India. 1972)

A Basic Doctor A Basic Doctor is one who is well conversant with the day-to-day health problems of the rural and urban communities and who is able to play an effective role in the curative and preventive aspects of the regional and the national health problems. Besides being fully well up in clinical methods, i.e. history taking, physical examination, diagnosis and treatment of common conditions, he should have the competence to judge which cases are required to be referred to a hospital or to a specialist. He should be able to give immediate life-saving aid in all acute emergencies. He should be capable of constant advancement in his knowledge by learning things for himself by having learned the proper spirit and having learned the proper techniques for this purpose during his medical course. — Patel, 1970

Diagnosis of the Problem

"The strangle hold of the inherited system of medical education, the exclusive orientation towards the teaching hospital, the irrelevance of the training to the health needs of the community, the increasing trend towards specialization' and acquisition of post-graduate degrees, the lack of incentives and adequate recognition for work within rural communities and the attractions of the export market for medical manpower are some of the factors which can be identified as being responsible for the present day aloofness of medicine from the basic health needs of our people. The relation of medical education to the social framework of the community is largely brought out towards the end of the student's period of formal training and medical education continues to postpone rather than prepare a doctor for the practice of medicine in the community". — Srivastava Report, 1975

restructure the educational programmes". The statement was significant because it was for the first time that the planners were accepting in an official document that socio-political change was prerequisite, before a more community/people oriented doctor or medical education process could be developed. In 1980, a joint study group constituted by the Indian Council of Social Sciences Research and the Indian Council of Medical Research outlined an Alternative Strategy for Health for All in India. Their report initiated a nationwide debate on the subject. The Alternative Health model had elements in it which clearly indicated the pre-requisite of an alternative socio-political structure that was 'democratic, decentralised and participatory'. This group endorsed all the recommendations of the Srivastava Report on the re-orientation of medical education and pleaded for the abandonment of the existing flexnerised model of education and ‘the adoption of an interdisciplinary holistic approach' which was 'severely more practical and field oriented'. It recommended some more additions to the curriculum and suggested that a deliberate

effort should be made to throw the net of selection of medical students wider so that more people whose social and cultural backgrounds were closer to the poor and under-privileged groups could be included. It also suggested that there should be a close collaboration of health services with the medical colleges. In 1982, the Government for the first time in 35 years made a comprehensive National Health Policy statement in which it emphatically stated that "the entire basis and approach towards medical and health education at all levels should be reviewed in terms of national needs and priorities and the curricular and training programmes restructured to produce personnel of various grades of skill and competence who are professionally equipped and socially motivated to effectively deal with day to day problems within the existing constraints". During this decade the IAAME also began to consider these new issues in their annual conferences and from 1972, they began to organise student intern’s seminars to elicit junior doctor’s views on many of these matters. However, these meetings were no better than the average 'paper-reading sessions' common in professional association meeting and lacked the punch, both in debate or recommendations, to make any impact. The Medical Council of India in the same decade has been as usual very cautious in its approach to change. While not removing or altering any part of the basic curriculum, it suggested some additions in response to the recommendations of A health care system which combines the best elements in the tradition and culture of the people with modern science and technology,

- integrating promotive, preventive and curative functions, - democratic, decentralised and participatory, - oriented to the people ……. - economical, and - firmly rooted in. the community and aiming at involving the people in the provision of services they need and increasing their capacity to solve their own problems. — ICMR/ICSSR, 1980

Srivastava Report and the growing dissatisfaction in government and public circles with the irrelevancies of our educational process. The curriculum as it stands today — 1977, 1978 and 1981 modifications tries to incorporate some new ideas towards a greater social relevance but its wording is both adhoc and full of jargon. The Plan itself is disjointed - contradictory. For example, while preamble 1.3 states that the graduate medical curriculum is oriented towards 'training a physician of first contact who is capable of looking after the preventive, promotive and curative and rehabilitative aspect of medicine' preamble 1.6 and 1.7 at once contradicts it by stating that 'graduate education per se cannot be tailored to service situation' and 'has to be flexible in nature to offer a wide range of employment opportunity. However, for the first time in decades the l\ICI recommendations include, since 1977, a paragraph on Teachers Training Workshops for teachers of medical colleges on:i)

'problems of community health and delivery of health care'; and

ii) 'pedagogy’ are recommended This marks the beginning of a new realisation that the major block in medical education is not 'curriculum content' or 'quality of students' but the 'disorientation of faculty' IlI. OVERVIEW In the last l50 years Medical Education in India hascontinued to develop within the context of its owns historical roots in the Western situation, There are many historical, socio-political, professional and other factors which have maintained this trend of inappropriateness in India. Bryant (1971) has said that 'in every corner of the world the products of such systems (i.e. doctors trained in large city hospitals modeled on the British and American pattern) have not only been unwilling to work where they are most needed, that is a familiar story but they have had limited capability for working there. They have not been prepared to do what needed to be done’.

Some of· the important factors are outlined (many of these have been further described in earlier MFC sources/bulletins). 1. Colonialism

Even though nearly four decades have passed since we achieved independence the colonial men-

tality of the medical profession, the elite bureaucracy and the political leadership have not disappeared. The 'brown sahebs' who rule India, have very deep roots in their hack ground and education which make them see the dictates of Western society as more important than the basic needs and aspirations of our own people. Hierarchical trends in administration of medical colleges, non-democratic spirit in curriculum planning and authoritarian methods in bringing about changes in medical colleges have prevented serious and meaningful change in the inherited structures 2. Pseudo-socialism

The political leadership and the leaders of the medical profession have found it important in the interest of the elite and privileged sections of the community, whom they represent to cry out from the social objectives of medical education in keeping with the democratic and socialistic spirit of our constitution. However, in actual practice they have built up hospital structures mainly in urban areas with highly sophisticated technology which serve their own class. All this has been done through heavy subsidy by the government in the name of the people, This pseudo- socialism is seen best in situations like:i)

months of profession, public and government debate had to take place before the CH\V scheme was introduced while hi2.h powered technology like cat-scan and line: accelerators are sanctioned in minutes during cabinet meetings for our elite institutions which in their own reports accept that they do not cater to the masses;

ii) the drug budgets of the PHCs which cater to the masses in the villages;

iii) iii) the indecision regarding the mushrooming

capitation fee medical colleges; iv) the hesitation in taking action on the drug policy

recommendations of the Hathi Committee and so on. The Medical Council and the medical profession have shown the same trends since the decision makers among them have even less personal experience of the social realities of rural India. 3. Dilution of Standards

The phenomenal growth of medical colleges in the post-independence period has led to a gross

dilution of standards in education In addition there has been what Banerji (l977) has called a culture of 'glorification of mediocrity'. In the absence of adequate number of well qualified teachers. all and sundry have been promoted to positions of leadership and important academic levels. These people not only lack the vision or the academic maturity but have tended to form 'mutual admiration societies' around them to pad up their own complexes and limitations. Even though the Medical Council has laid minimum standards for medical colleges including the qualification and experience of teachers it is a well-known fact that these standards have been followed in default rather than in acceptance. It would not be rash to state that if an objective evaluation were to be made of the l20-odd medical colleges using the so-called minimum requirements of the MCI more than 50 per cent of the colleges would have to be closed down immediately. This exercise would itself expose the complicity of MCI inspectors in the dilution of standards in the country. °

4. Non-teachers

Because of the professional vested interests in medicine, the medical profession has refused to accept the fact that all doctors cannot necessarily be good teachers. Educational science and pedagogy are important foundations on which medical curriculum should be organised, whatever the content and relevance of the course. Teachers in medical colleges in India seldom join because of a love for the 'vocation of teaching'. A base in the medical college is helpful in the cut-throat competition of private practice apart from being itself a channel of referral to one's own private clinics. This is in spite of the fact that MCI recommends full-time nonpracticing teachers. The remuneration offered to medical teachers further compounds this problem. It is only as late as 1977 MCI has stated the need for teachers to undergo course in pedagogy. However, even now this is not mandatory. What is worse is that all the recommendations for community and rural orientation have never included the single most relevant one for change, i.e., reorientation of the medical college faculty. When all of them have little knowledgeft5ensitivity or skill to work with people in the community, how is it ever possible to bring about a social orientation in the environment of medical colleges?

5. Schizophrenia in Educational objectives

The Todd report in U. K. commenting on undergraduate medical education clearly pointed out that 'every doctor who wishes to exercise a substantial measure of independent clinical judgement will be required to have a substantial post-graduate professional training and the aim of the undergraduate course should be to produce not a finished doctor, but a broadly educated man who can become a doctor by further training'. While continuing the same curriculum that Todd comments on the Medical Council of India and the Government of India expect that the graduate in India will be capable of independent decision making and effective health team functioning necessary in community health practice!! °

This schizophrenic trend in objectives spills over to the curriculum as well. There is a constant pressure within the profession to keep up with the latest and introduce it into the educational process. At the same time in response to all the rhetoric there is a pressure to make the course more socially relevant. Nuclear medicine is added along with rural sociology, concepts of molecular biology added along with health management principles and so on. This is primarily due to the fact that the medical profession and the students selected to enter the profession all belong to the upper elite sections of Indian society. While seeing the need to appear relevant and radical, the options of going abroad and working in situations for which one is better trained arc always kept open! 6. The Myths of 'PSM'

In the Bhore Committee Report, Dr Grant and other stalwarts had visualised the department of PS.M as initiating primarily:i)

an extension link for medical college hospitals with the communities rural, urban around them;

ii)

an association with clinical teachers to stimulate them to see the importance of socio-economic, cultural and ecological factors in management of health and diseases.

Preventive and Social Medicine was to be process of reorienting the existing faculty and the

inherited curriculum and not just another speciality or para-clinical subject. The history of the evolving PSM department has

iii)

Re-orientation of Medical Education towards social/rural/community relevance is the responsibility of every member of the medical college faculty.

been very different and there is no doubt today that there is a gross confusion between means and ends. On the one hand PSM departments have literally no contact with hospital teaching or service and on the other hand the rural and urban field practice areas under their supervision are so severely under their own management, with next to nil participation by faculty of the hospital departments that today PSM curriculum is becoming the producer of many myths.' The commonest being:-

Myth

Myth

Myth

I — that there are two specialties — clinical medicine and rural community medicine. The latter being a diluted substandard type of the former. II — community medicine, PSM is poor quality medicine dished out by disgruntled and disinterested staff in an adhoc fashion to an equally disorganized and divided community. III — Community Medicine, rural reorientation and reorientation of medical education is the thankless responsibility of the PSM faculty while other departments will continue to teach borrowed, inappropriate Western medicine,

A serious movement in this direction is necessary to prevent PSM departments from becoming even more counter-productive than they already are. 7. Nil Evaluation/Experimentation

In spite of clear exhortations from time-to-time in all the expert committee documents that experimentation in curriculum within the overall context of the social objectives of medical education must be encouraged and fully supported (in order to evolve our own relevant Indian curriculum) it is a sad fact that this has not taken place. Volumes of papers in conferences have been read but real hardcore planned and evaluated experimentation in medical curriculum has not taken place in the country. The Jamnagar experiment and the Kottayam experiment are the only two real attempts and even these have neither been well recorded or circulated to the teaching medical profession for debate, consideration or emulation. The Indian Journal of Medical Education is full of ideas within. the constraints of the existing curriculum — family health

These myths created by the PSM departments as they exist today need to be squarely confronted with the following maxims:-

advisory service, clinico social case conference, studentstaff health service, rural orientation camps and so on — but all these have been adhoc unevaluated interventions

i) There is only one relevant medicine and that is

within the existing curriculum mostly by PSM faculty. With the ethos of medical colleges being what they ale

'medicine' with a PSM orientation.

today, any hope that any of these would bring about ii) Community Medicine is the best quality of care within

the

available

resources

or

attitudinal changes in the students was unrealistic.

local

constraints (especially economic) rendered in an

It is also unfortunate that the Mel in 1981 has glibly

organised manner, sensitive to the people's

stated that 'no scientific evaluation of what has been in

needs and based on scientific principles.

vogue for more than two decades has been undertaken nor a study made of medical graduates from different colleges in the country'. It further adds that 'deficiencies which exist at present are attributable more to a failure of implementation of the council's recommendations and the absence of a system of continuous monitoring, and not due to defects in the course and curriculum prescribed.

Surprisingly in spite of this statement and in spite of all the rhetoric neither MCI nor ICMR has yet planned an evaluation or encouraged experimentation!

Further reading

1.

Crow ford. D.C.: "A History of the Indian Medical Service.: Thacker Spink & Co. Calcutta (1914).

8. Stress on Problem/Content not on Process

2.

“(Report of Health Survey and Development Committee (Bhore Committee)": Government of

Change can take place only when the change agents are sensitive to process. i.e., sensitive to an evolving scheme of things dependent on underlying .socio-political

India, (1946). 3. "Report of Health Survey and Planning Committee (Mudaliar Committee)": Government of India

economic, cultural, technical factors. All the expert

(1961).

committees since 1946 have outlined the problems and then

recommended

curricular

changes

—

never

4.

158 & 227 (1966).

mentioning process of change i.e., How to get to where we want to get from where we are? Who will do it? How

5.

"Reorientation of Medical Education for Community Health Services": (B. P. Patel): Government of India (1%8).

6.

"Ministry of Health and Family Welfare Annual Report 1971-72: Government of India (1972).

7.

"Report of Group on .Medical Education and Support Manpower (Srivastava Report)": Ministry of Health & Family Welfare, Government of India (1975).

8.

Banerji D: "Objectives of Medical Education": In Search of Diagnosis, MFC Anthology (1977).

9.

"Health for All - An Alternative Strategy": CMR/ICSSR Report, Indian Institute of Education (1981).

will those who have to do it themselves get the required experience or skill? This explains why there is a knowledge explosion in the curriculum but no change in attitudes, vision or relevance in the career aims of medicos today. IV. CONCLUSION

150 years have passed since the present Western oriented system of medical education was established in India. The first century saw the establishment of the educational system transplanted in toto from the British and European situation. With resurgent nationalism, the next forty years (both pre and post-independence) Saw lip-service paid to the reorientation of education to make it relevant to the people's need. The last ten years has been a new spirit of introspection which at least accepts that medical education has failed the people but still Jacks the political will to take any radical action. However, behind the continuing rhetoric, it must be emphasised that in the last decade there have been in government and other expert committee documents some critical analysis and recommendations for curricular change of far-reaching importance. Medico-Friends and others seriously concerned with issues in medical education would find these of some support to their own search.

Rao, K. N. Indian Journal of Medical Education. 5,

10. “Statement on National Health Policy," Ministry of Health & Family Welfare: Government of India (1982). 11. “Recommendations on Graduate Medical Education": Medical Council of India: 1964, 1971, 1977 & 1982. Note:

A summary of the main curricular changes and recommendations of the Bhore, Patel, Srivastava and ICMR/ICSSR reports and the 1981 modifications of the Medical Council of India curriculum are available from the author. Write to: Community Health Cell, 326, V main, 1st Block, Koramangala, Bangalore 560 034.

**

INNOVATIVE PROGRAMMES IN MEDICAL EDUCATION: THREE CASE S1UD1ES Compiled by: C. Sathyamala I. Institute of Medicine, Tribhuvan a) Rationale

Earlier to the establishment of the medical school, the students from Nepal were being sent to other countries for their undergraduate medical education. Many of the students thus trained never returned to Nepal after their graduation. Those who did return found it difficult to cope with the reality existing in Nepal, for which they had not been specifically prepared. Further, as these doctors had been trained in different countries under varying conditions, there was no uniformity in the content of the education they had received which affected their functioning as a team in the national health service. Hence, the training of students outside of Nepal was considered inadequate both in terms of knowledge and skills for the needs of the country. This combined with the fact that Nepal had insufficient number of doctors to meet the requirements of the health care system, made it necessary for the country to open its own school of Medicine Since this decision was taken only in the midseventies, the debate on medical education in the International scene influenced the shaping of the curriculum and made it easier to formulate a progressive educational system, designed to meet the social needs of Nepal. The Institute of Medicine admitted the first batch of students for undergraduate medical education in 1978. b) Role and Objectives

The objective is to produce a person with adequate skill, knowledge and attitude to function in all types of environment existing in Nepal. The highest hospital in Nepal has only 300 beds. Working conditions in the district hospitals are far below the standard of PHCs in India. Most of these health centres are expected to remain in poor standard for another 10-15 years. Health problems vary from malnutrition, communicable diseases and respiratory problems to high fertility rate. As the region is hilly, and transportation poor, patients cannot be transferred from one centre to another, in other words, referral system does not exist.

A doctor working in these circumstances has to have adequate knowledge to handle all these problems and skills to take appropriate measures to save life, be it obstetric or surgical emergencies. Most of all the doctor needs to have the necessary attitude to work under such difficult conditions. The basic role envisaged for the physician is to render secondary care at the 15- 50 bedded district hospital. Broadly, the doctor is expected to — institute life saving measures (e.g. relieve obstructed labour, perform Simple surgery, give anaesthesia, and deal with medical emergencies. — recognize his/her limitations and refer patients to appropriate authority — organize preventive health services through the district health officer — supervise the work in the health posts — educate and train the paraprofessionals and auxiliaries attached to the health teams within the district c) Criteria for the selection of the student

Entry into the medical college is open only to the middle level workers already functioning as part of the health care system i.e. Health Assistants, Staff Nurses, Lab. Technicians, Radiographers, Pharmacy Technicians, Ayurveds The only pre-requisite is that the applicant should have completed a minimum of three years service in health field as a trained worker d) Structure of the training

Medical education in Nepal forms part of a system in which the student enters the institute after school leaving certificate and undergoes a basic paramedical training. After graduation, the graduate is sent to a health post or hospital to work as middle level worker. From this group, anyone desirous of joining the medical college can do so after three years of service. This two-tier plan was seen to have an added advantage as it would attract the 'best' student’s thereby improving the quality of all levels of workers in the health care system.

University Certificate Course

School leaving

General medicine — 2½ years

certificate

Pharmacy Lab technician Radiography Ayurved Nursing*

— 2½ years — " — " — 3 years — 3 + (1)

3 years

4 years

service in health field

MBBS

* The basic nursing course if of three years duration but if a nurse intends to enter into medical undergraduate programme she has to complete a further one year of basic science course. The four year curriculum is divided into 3 phases: Phase I - (1½ years)

Phase II - (1½ years)

Phase III - (I year)

..

..

..

..

..

..

..

..

..

:

Community Medicine for 5 months

:

Study of basic science subjects coordinated on system basis

:

Theoretical and practical background in patient

: : :

management skills Community Medicine (epidemiology) Learning to manage a district hospital (5 months) Development of Clinical Skills

e) Methodology of teaching

The course begins wit]; the theoretical teaching of Community Medicine at the campus, At the end of this programme, a period of 5-6 weeks is spent by the students with their supervise- in rural areas for doing health related surveys, Problems thus identified form the basis of some of the corrective measures the student may undertake to implement during the course in future. The programme for the study of basic science subjects is coordinated on system basis. The course begins with intensive topographical study of anatomy for two weeks followed by systems, In each system, the student correlates the structure (anatomy), and the function (physiology), studies the pathophysiological on the patient by taking history and doing clinical examination, Emphasis is laid on the common problems prevalent in Nepal, In all areas, study of controversial or unproven theories are avoided as far as possible, At the end of each system the students try to solve some clinical problems presented on the basis of the study of anatomy, physiology, pathology and pharmacology. During the system study, time is spent both in the campus and in the hospital to correlate the basic sciences with clinical science. Students also visit

the homes of chronically ill-patients and study the family to identify the socio-economic-cultural- environmental factors which may have a bearing on the disease and its outcome. Hospital management is taught in district hospitals. f) Nature and set up of training centres

The Institute does not have a hospital of its 0\\"11 The clinical teaching is therefore done in the hospitals belonging to the Ministry of Health, The programme now in its final year is using all the available hospitals in Kathmandu valley for teaching purposes, 5-6 weeks of the initial part of the first phase is spent in rural health posts, 5 months of the third phase is spent in district hospitals (which have purposely not been upgraded into teaching hospitals as they are used for situational learning) outside Kathmandu valley. g) Faculty

Experienced and motivated teachers were not available in the field of Community Medicine, Since there was a strong emphasis on Community Medicine and as it was introduced right from the beginning, the programme had to be run with young and inexperienced faculty. 75% of the senior con-

sultants in the hospitals selected for teaching purposes are helping the Institute as part-time visiting professors. . h) Difficulties encountered

— Lack of the right kind of staff members especially in the field of basic sciences — Inability of the staff members to conceptualize the decompartmentalization of the different disciplines — Lack of innovative teaching methods and equipment, more suitable for this innovative education — Lack of teaching hospital facility II. The Institute of Health Sciences. Tacloban University of Philippines a) Rationale:

In the Philippines, it normally takes nine years for a standard medical school to produce a physician. In addition the products of the medical schools are generally disinclined to serve in the rural communities. Medical schools have rigid entrance requirements and the competition for places in the college is intense. The selection mechanism therefore tends to favour those who come from the privileged sector of the national community. Due to their dependence on sophisticated medical technology the standard medical training also makes the graduate unfit for rural practice. Therefore a need was felt to health personnel who would be competent and desirous of working with rural communities. The Institute of Health Sciences was set up in 1976 as an experimental school to train health personnel to fulfill this need. The three broad objectives of this Institute are; — to produce a broad range of health manpower to serve the depressed and underserved communities in an Eastern region of Philippines; — to design and test programme models for health manpower development that would be replicable in other parts of the country; — to monitor the development of rural communities in relation to the effectiveness of the health care system operating in these communities

b) Role

The student is expected to return to his/her community and function as a community health worker. He/ she is expected to plan, implement, evaluate health programmes with the active participation of the community provide leadership and organize the community towards better health. c) Criteria for Selection of Students

There is no entrance examination. The admission policy is based on the assumption that a. person recruited from the depressed and underserved area and trained for the necessary health and community organization skills would become a health worker to rural community service. The students are recruited primarily from communities which are badly in need of health workers. The first step is the identification of target barangays (communities) in the selection process. Indicators used are:— the availability of health personnel — mortality and morbidity rates — peace and order situation — economic status The students for these target barangays are nominated by their communities in an open village meetings. As regards the nominees the Institute requires that he/she should be a high school graduate no more than two years out of school, a resident of the barangay for at least one year, and willing to return to the barangay after training. Upon nomination, the student with the consent of his parents pledges to return to the community to render service as a health worker. This pledge operates as a type of social contract and is entered into by the student and the community. The community in turn pledges to provide a measure of support to the student while at the Institute. This support includes transportation, medical kits and participation in the health programme the student will set up upon his return to the community. This pledge or resolution is to he Signed by at least 70% of the households. The final criterion for admission is based on a weightage system in the following way: — Community of origin — 50% Barangay council endorsement — 20%

Training programme selected Midwifery Nursing

BSCH MD Motivation Aptitude

— 20% — 20% — 5% — 5% — 5% — 5%

d) Structure of the training

Unlike the traditional medical schools, the institute is not, concerned with the training of physicians alone. Its curriculum has a ladder type of a course, with various points of entry and exit, Students admitted to the programme initially go through a paramedical training known as the Barangay Health workers programme, This lasts for eleven weeks (or one quarter), After this. the students return to their barangays where they perform activities that require them to apply the knowledge and skills they have learned. ll1is service leave, as it is called, lasts for three months. The student is provided with a checklist of activities which includes assessment of, the health needs of community and planning. Nutrition, Environmental Sanitation, MCH, Control of Communicable Diseases. treatment of common illnesses. First Aid and Emergency care. If the student satisfactorily performs his tasks and if he is endorsed by his community again. he returns to the Institute to follow the flex programme, the community health workers programme, it lasts for 55 weeks (5 quarters) and the contents arc in part determined by the Philippine Governments rule pertaining to midwifery curriculum and also by the fact that passing the Government's board examination is a pre-requisite to the practice of midwifery . Students after completing the Community Health workers programme once again return to their respective communities for their second service leave Students who perform satisfactorily during the second service leave return to the Institute for their midwifery board examination. At this point some students are selected to enter the Community Health musing programme, while others are required to stay at the level of community health workers. The decision as to whether the student shall follow the Community Health nursing programme depends upon a number of factors, the most important of which is the need of the community for a health worker. Where the community is badly in need of a health worker, the student is required to remain in the community.

The ladder type curriculum of the Institute of Health Science (1 quarter = 11 weeks) Doctor of Medicine Doctor of Medicine Programme (12 quarters) with alternating service leave Service Leave Bachelor of Science in rural Medicine Programme (2 quarters) Service Leave Community Health Nursing Programme (4 quarters) 'Service Leave Community Health Worker Programme (3 quarters) Service Leave Barangay Health Worker Programme (1 quarter) The Community Health Nursing programme covers four additional quarters of study followed by two quarters of study towards the Bachelor of Science in rural medicine _ The person who comes out of this programme successfully is allowed to join the 2 years medical education (MD) programme. During the service leave, a faculty member visits the community where the student is placed to support and strengthen the link between him and the community. e) Methodology of teaching

The faculty uses lecture-cum-demonstration and group dynamics as basic teaching methods. Individual reporting is not used because of the 10\\reading comprehension of the students and their difficulty with the English language. Participative learning in taking care of patients is reinforced by case presentation and case study. Weekly ward classes are held after each clinical experience. Application of theory is learnt during their service leaves which are part of the field experience. There is a strong emphasis on the community organization and the student is given political knowledge and skills to understand and help the community. Faculty member; are required to indicate exactly in which aspects of the course the student is deficient The tutorial scheme is provided for individual tuition for students lagging behind. f) Assessment of the students

There is no standard grading system that awards numerical equivalents to the quality of students' performance. In the courses a student is

rated in terms of whether he has reached the minimum acceptable level or needs more instruction in certain branches. During the service leave the student is assessed by the rural health unit (RH U) the community and the Municipal Development Officer" Evaluation forms are provided for each student This includes checklist of activities, attitudinal evaluation scale, anecdotal report, and clinical performance rating scale" The' results of the evaluation are discussed in the barangay assembly meeting. the minutes of which are accompanied by the names and signatures of all registered members of the barangay assembly who attended the public meeting. The students are also asked to do a self-evaluation based on competency levels and commitment to their communities. The final evaluation is made on the basis of feed back from the RHU, Community, Municipal Development Officer and the students. g) Difficulties encountered — Students in the initial entry all had the impression that, provided that they maintained good academic records, they would eventually follow the MD course It therefore became difficult for the Institute to leave Some students out 01 the programme on the strength of their being needed by the community. — The age range of the first batch of students ran from 17 to 28 years and some of the older students had previous college experience that placed them ahead of the younger ones in academic competence and maturity of outlook. This heterogeneity made it very difficult for teachers to adjust their perspective and teaching strategy. — Even 'though the Institute wished to create a programme that was specially tailored to the needs of the rural communities, it could not disregard the existing governmental regulations (e.g. The student has to pass Government board examination prior to practising midwifery) — Since majority of the pioneer faculty came from the college of nursing. the nursing orientation was used in teaching of midwifery courses. III. The University of New Castle, New South Wales, Australia. a) Rationale The Australian health care system is based on compulsory health insurance scheme subsidized by

the Government. In recent years, with the escalating cost of health care, the Government has been forced to spend a larger proportion of its gross national product on medical care.

The decision to start a new medical school was taken in 1973 although it materialized only by 1978. In these 5 years, apart from the decline in Australian economy as a whole. two other factors led to the questioning of the wisdom of starting a new medical school The doctor-population ratio for a variety of reasons had increased more than was predicted and there was a growing awareness that the maintenance of doctors was the expensive part of the health care system. In this semi-hostile atmosphere, the decision to admit the first batch of students was made, are in order to justify the cost, the training had to be all the country's need. b) Objectives The faculty has developed 45 programme objectives. Certain commonly neglected aspects of medical education especially those related to attitude of the emerging physician has been given equal importance as that of specific clinical skills" c) Criteria for selection of students The formulation of the policy on student selection was jointly decided by the faculty and a group of community representatives" The representatives consisted of the mayor of the city, a senior trade unionist, a business executive a social worker and two medical practioners, three of three six community representatives were women. The £11' policy decision was that: — half of the available places will be filled on the basis of academic achievement alone — the remaining half on a number of personal and intellectual characteristics, one of which would certainly be a reasonable level of past academic performance. d) Methodology and structure of the training The entire educational programme is constructed, from the beginning around the process of clinical problem solving. From the first term of the first year, the student is involved with real, live clinical problems which he will be required to understand and manage, at an increasing level of complexity as the course progresses. The learning in 'basic sciences' will be derived from the study of these problems. The clinical problems themselves

have been selected after a close study of detailed Australian epidemiological data. An example of such a problem is pain in the chest. Learning will include the biochemistry of cardiac muscle, the physiology of pain, the anatomy of the blood and nerve supply to the heart, risk factors, epidemiology and prevention, physical examination, appropriate investigation. management of the patient, coping with the relatives, effects on the family, and community care and rehabilitation. Thus the basic sciences will not be learned as .separate disciplines but will be studied and applied in a clinical context from the first to the last day. (However, it has not been possible to integrate the learning of basic communication and clinical skills into this method of learning)

term and is to assess the students' fitness to proceed to next stage. The results are not made on pass/fail basis but on whether the student has reached the requisite standard or not. The students are given complete feedback on the assessment in order to help them undertake remedial work before reassessment. g) Faculty

An educator, a behavioral scientist and a professor of Community Medicine were appointed first. A professor of Clinical Pharmacology and Psychiatry were then recruited and at a comparatively early stage in advancement of the recruitment, in Paediatrics, Obstetrics and Gynaecology and Surgery. Conclusion

The five year curriculum is divided into four phases: Phase I — (10 weeks) introduction to curriculum and problem solving Phase II — (3 years) Problems of the adult, system by system at first and ending with multi system problems Phase III — (1 year) Problems of the newborn, children, adolescents and the aged Phase IV - (l year) A form of internship. The student will undertake increasing responsibility for the management of patients of all ages both in hospital and in the community. The initial emphasis is on practical experience and analysis of human behaviour in the student group, before moving on to interaction with strangers and then with patient is. Tutors for these small group activities include local practitioners and other health professionals. e) Nature of training set-up

"The students receives a substantial part of their education in the local community i.e. in general practitioners, OPDs, Community Health Centres, and other health and welfare agencies. f) Assessment of students

Two types of assessments are made. First informal assessment occurs during learning so that "Students can monitor their own progress. The second type is done towards the end of each 10 week

In all these three cases, the educational programmes are aiming at producing health personnel better equipped in terms of attitude, skills and knowledge to respond to the needs of the particular country They are innovative in terms of the criteria used for selection of students and in terms of content and methodology of training. Philippines and Nepal are attempting to make medical education an integral part of the overall education of health personnel. Philippines is one step ahead by making the community the "ultimate beneficiary" and not the individual student alone. However there are certain contradictions which need to he explored more. The career-oriented ladder-type of curriculum could become its own nemesis. The students joining this type of educational programmes generally are interested reaching the highest rung. It also seems contradictory to find a country like Philippines with its oppressive Government to have apparently the most politicised medical education and produce health workers whose main function is to mobilise the community. Finally, the students trained in these progressive schools would still have to go out into the world where "free" markets forces still dominate. Compiled from: — "The undergraduate medical education in Nepal: its concepts and problems". Dr. B. R. Prasai, Coordinator, MBBS Programme, Institute of Medicine, Kathmandu (Continued on page 16)

FROM THE EDITOR’S DESK The Medico Friend Circle has had, as one of its earliest objectives, the evolution of 'a medical curriculum and training tailored to the needs of the vast majority of the people of our country'. , Abhay Bang's background paper in 1974 on "The relevance of the present system of Medical Services in India" had raised many issues on the ethos of medical colleges and the orientation of the curriculum. In 1975, Banerji's article on "The Social and Educational objectives of Medical Education" was featured in the Bulletin and in our first anthology, in which he had brought out some of the historical and socio-political factors responsible for the state of the art in India. Apart from these papers this subject has cropped up from time to time in many of the other articles, letters and discussions in the Bulletin. This is, however, the first time that an issue of this Bulletin is being fully devoted to this theme. Even those of us who believe that the existing pattern cannot change basically without a concomitant fundamental change in the socio-economic and political situation realise that medical education as it exists today is so counter-productive and irrelevant to the needs of the people that discussion towards change within the existing constraints can no longer be avoided. Even the govern-

(Continued from Page 15) — "Personnel for health care: Case Studies of

educational programmes". Vol. II. Edited by F.M. Katz and T. Fulop, Public Health Papers No. 71. WHO, 1980.

ment accepts officially that the system in India is irrelevant and inappropriate and the Health Policy Statement in April 1982 talks about the need for 'a dynamic process of change and innovation'. The GK Project in Bangladesh is in the process of evolving an alternative medical curriculum for the Bangladesh situation. In this context we organised a special session in Anand, on 'Medical Curriculum' so that the MFC participants attending the Dhaka conference in April 1983 would have some background consensus support from the group. The decision to devote the next annual Meeting to a discussion on the 'Need for an Alternative Medical Education' requires that participants come well-informed and prepared to discuss this subject. In this special issue the first article on '150 years of Medical Education' gives readers the historical setting and developments in the context of which our discussions will take place. Sathyamala’s article puts our search in a broader perspective. For those absorbed in health work and medical practice, some time spent questioning the relevance of their own medical education in the light of the social reality in which they now function or the tasks which they are being called to perform would itself be a starting point for discussion.

Programme of the U, P. Institute of Health Sciences — 1977 to 1980 printed by the Ministry of Public Information Region VIII, Tacloban city.

— A medical school for the future: The New Castle experiment". D. Maddison, World Health Forum (1, 2): 133-138 (1980).

With effect from March 1984, all correspondence with Kamala Jaya Rao should be addressed to:

— "Towards alternative approaches to health development in underserved areas", A retrospective report on the Research and Development

Kamala Jaya Rao, C/o Mr. S.S. Jaya Rao, 3-6-515, Himayatnagar P.O., Hyderabad-500 029.

Editorial Committee: Anant Phadke Padma Prakash Ravi Narayan Shirish Datar Ulhas Jajoo

Editor Kamala Jayarao

Views and opinions expressed in the Bulletin are those of the authors and not necessarily of the organisation.


