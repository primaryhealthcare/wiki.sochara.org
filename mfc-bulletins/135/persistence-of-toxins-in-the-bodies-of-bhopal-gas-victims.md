---
title: "Persistence Of Toxins In The Bodies Of Bhopal Gas Victims"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Persistence Of Toxins In The Bodies Of Bhopal Gas Victims from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC135.doc](http://www.mfcindia.org/mfcpdfs/MFC135.doc)*

Title: Persistence Of Toxins In The Bodies Of Bhopal Gas Victims

Authors: Sadgopal Anil & Das Sujit K

﻿

	­

135

medico friend circle bulletin



DECEMBER 1987

Persistence of Toxins in the Bodies of Bhopal Gas Victims*

Anil Sadgopal & Sujit K Das




Soon after the gas leak, the Indian Council of Medical Research (ICMR) announced that the gas victims were afflicted with systemic and Persistent cyanide poisoning and recommended detoxification of symptomatic gas-exposed population by sodium thiosulphate. The Government of Madhya Pradesh, however, dragged its feet with respect to this recom­mendation. The State administration went to the extent of stopping Dr. Nisith Vohra of Jana Swasthya Kendra from pursuing this therapy. In July 1985, two gas victims and their pl1ysician Dr. Nisith Vohra approached the Supreme Court claiming the right of the gas victims for detoxification. In the course of hearings, the Supreme Court appointed a seven-member expert committee on November 4, 1985 with the following terms of refer­ence:

(i) Carrying out fair distribution of sodium	thiosulphate; (ii) Carrying out prop ~r epidemiological survey and also house to house survey of the gas affected victims for the purpose of determining the compensation payable to the gas affected victims and their families; draw up a scheme for carrying out survey; (iii) Ensuring proper medical facilities to the gas affected victims; give directions from time to time for carrying out survey and providing medical relief;

(iv) Draw up a scheme for provision of medical relief and for monitoring the implementation of the scheme for medical relief; (v) Give direction to produce the results of the	surveys which have already been done. The Committee, in its last held meeting on Decem­ber 13-14, 1986 decided to submit two separate in­terim reports to the Supreme Court-one by the majority constituted by five official members, and the other by the two undersigned non-official mem­bers constituting the minority. Besides its lackadai­sical functioning, the Committee could take up work only on the first term of reference, i.e. carrying out fair distribution of sodium thiosulphate. Ig­noring the proposal submitted by the Minority Members, the Majority Members decided not to take up the remaining tasks specified in the Supreme Court order.

Even on the issue of sodium thiosulphate therapy, the Majority Members declined to pursue or analyse all relevant scientific data. As a consequence of this enigmatic attitude towards the work of the Supreme Court Committee, the Majority Interim Report only perfunctorily went through the motions of recom­mending sodium thiosulphate therapy to unidentified subjects and failed to come to grips with the central questions viz. persistence of toxins, role of antidotal therapy and magnitude of the task. We, therefore, undertook it upon ourselves to collect and analyse all available scientific information in order to fulfill the task entrusted by the Supreme Court. 


* Excerpts from a preliminary report submitted to the Supreme Court on October 26, 1987 by the minority group.


(f) detection of carbamylated haemoglobin (hae­moglobin linked with MIC) in the blood of gas victims;

Our Minority Interim Report is almost ready and will be submitted in about a month's time. Despite refusal by various official research bodies and indivi­dual scientists engaged in research on Bhopal victims to provide us with essential information, we have been able to collect significant evidence pointing towards systemic and continuing presence of toxins in the bodies of the gas victims. Yet we were frust­rated all along this work because the data on this issue as presented before the Committee as well as those released in ICMR publications could not stand the test of objective scrutiny in scientific forums. It is in this light that the evidence obtained by us in August 1987 and analysed during the past few weeks, assumes special significance.

(g) 	finding of cherry-red colour of venous blood in the autopsy studies; (11)  evidence for the presence of anti-MIC anti­body in blood samples drawn from gas victims;

(i) observation of disturbance in several immune parameters and adverse impact on the res­ponsiveness of lymphocytes; and

(j)  evidence of MIC's ability to cross air-blood barrier in animal studies conducted by DRDE, Gwalior.



The Medical Context At quite an early stage, the Indian investigators detected significant pointers towards the possibility of systemic and persistent toxicity of the poisonous emission. These pointers are briefly enumerated below:

(a) presence of multisystemic ailments in a significant percentage of victims and a high frequency of symptoms relating to non­-respiratory systems in patients not reporting respiratory complaints, as revealed by Medico Friend Circle's epidemiological sutdy;

(b) presence of a large percentage of patients who suffered from respiratory symptoms but were free from any evidence of organic damage to lungs, as reported by ICMR;

(c) evidence of significantly increased rate of spontaneous abortion among women who conceived upto 10 months after the toxic exposure, compared to the rate prevailing before the exposure, as reported in Medico Friend Circle's Pregnancy Outcome study;

(d) early reports of the presence of two to three­fold higher levels of thiocyanates in the urine of gas victims than the levels found in un­exposed subjects;

All the above-mentioned pointers towards syste­mic and persistent poisoning of gas victims have been discussed in detail in the Minority Interim Report to be submitted later to the Supreme Court. What is of special concern presently is the pointers relating to elevated levels of urine thiocyanates and the ameliorative role of sodium thiosulphate. The early reports of increased excretion of thiocyanates in the urine of gas victims led ICMR to conclude that the gas victims could be suffering from toxic effects of persistent cyanide poisoning. This inference was based on the well-known cellular reaction for transforming cyanide into thiocyanates in the presence of an enzyme called rhodanase. Under normal conditions, the cyanide radicals, entering human body through certain foodstuffs or through consumption of tobacco, get converted into thiocyanates as a result of donation of sulphur by intrinsically present sul­phur-containing compounds, constituting the 'sul­phane pool'. In the case of acute cyanide poisoning, the victim dies unless a large extraneous source of sulphur is quickly made available. The antidotal role of sodium thiosulphate has its basis in this simple biochemical mechanism. Since there was no known record in medical literature of chronic persistence of cyanide after single exposure, the ICMR found it necessary to invoke the concept of an 'enlarged cy­anogen pool' in the bodies of the gas victims, which would become a continuing source of cyanide-like compounds through the chronic phase.

­

(e) observation of symptomatic relief and further elevation of urine thiocyanates levels on ad­ministration of sodium thiosulphate injec­tion, even several months after the toxic exposure; Scientific Evidence

What is being presented here is a part of an ICMR­funded research project (No. 5/7/28E/84-RB) dealing with analysis... of urine thiocyanates levels in child (0-15 yrs.) and adult (15 yrs. and above) population samples 

2

­ collected from control and gas-exposed areas of Bhopal. The study, led by Dr. M.G. Kar­markar of the Department of Endocrinology and Metabolism, All India Institute of Medical Sciences, New Delhi, was undertaken with the objective of finding out "whether thyroid function is affected in population which was exposed to MIC'. The urine thiocyanates level was determined in this study in view of the well-known anti-thyroid property of this chemi­cal and in the wake of reports from Bhopal that the gas victims were excreting two to three-fold higher levels of this chemical compared to the sub­jects from Delhi. These investigations were conducted in two distinct phases.

In the first phase, from July to November 1985, urine samples from 326 gas-exposed women admitted in Sultania Zenana Hospital in Bhopal for delivery, were analysed for thiocyanates levels. An estimate of the 'normal range' (baseline) of urine thiocyanates levels was obtained by studying a group of 35 Delhi residents from six different socio-economic categories in order to have a representative sample. Care was taken to ensure that the sample was composed of roughly equal number of individuals with respect to sex and age groups (i.e. children and adults). The report presented to ICMR in December, 1985 reveals that the control sample from Delhi had a sample mean value of 0.399 mg % of urine thiocyanates with a standard deviation of 0.06. Compared to this mean value of the control sample, the group of 326 'exposed mothers' had a sample mean value of 0.45 mg %, with a standard deviation of 0.27. It was found that 88 individuals out of 326 'exposed mothers', i.e. about 27 % of the sample, had urine thiocyanates levels which were above 'mean plus 2 standard de­viation units' of the control sample, a fact reported to an ICMR Project Advisory Committee meeting held on May I, 1986. This implied that a significant percentage of women in the sample were excreting a noticeably high level of thiocyanates in urine. The investigators further noted that 40 out of these 88 women had urine thiocyanates values in the 'smokers range' (i.e. 0.55-0.70 mg %)*. It may, therefore, be concluded that about 14.7% of women in this sample could be unambiguously categorised as 'high excretors' of urine thiocyanates.

In the second phase of this study, conducted in collaboration with the Department of Medical Bio­chemistry, Gandhi Medical College (Bhopal), popu­lation-based samples of children and adults were selected

from both the control and exposed areas of Bhopal, that were coded by ICMR. The sampling was conducted according to a randomised block design prepared by ICMR's Senior Statistical Officer associated with the Bhopal Gas Disaster Research Centre at Bhopal. The size of each sample was found to be adequate for applying statistical tests of significance for assessing the validity of observed differences, if any, in urine thiocyanates levels of the samples being compared. It may be worthwhile to note here that the control areas coded by ICMR may not be taken as equivalent of unexposed areas since Medico Friend Circle's epidemiological study in March, 1985 showed that Anna Nagar, one of ICMR's 'control' areas and also included in the present AIIM study, was actually a mildly exposed colony, though it was 8 kilometers from the Union Carbide plant.

An important feature of this study was a follow­ up 6 months later in March, 1987, of the samples ini­tially investigated in September, 1986. It is reported that the follow-up study investigated the change in urine thiocyanates output of exactly the same indivi­duals who constituted the original sample.

Table I compares the mean urine thiocyanates levels of children and adults from control and ex­posed areas in both September, 1986 and March, 1987. These data were subjected to a detailed sta­tistical analysis by applying the Test Statistic, standard normal deviate, for testing the statistical significant of observed differences.** This exercise revealed that the differences observed in the mean urine thiocyanates values of exposed and control samples are statistically significant, and not due to chance fluctuations. In other words, it means that the toxic exposure is most likely responsible for an increase in the excretion of thiocyanates in the urine of gas victims on a population basis. It is further evident that the raised levels of urine thiocyanates persisted m the gas-exposed population until at least March, 1987.

The data shows that the gas-exposed samples, irrespective of the age group and time of testing, have a larger proportion of high-excretors than the control samples. These observed differ­ences in the proportions of high-excretors s of urine thiocyanates in various samples were subjected to a rigorous statistical analysis. It turns out that the above-mentioned differences between various exposed and control samples being compared are

	*Consumption of tobacco, whether through chewing or smoking, is reported to approximately double the output of urine	thiocyanates. "The statistical analysis, referred to in Table I, was conducted by Shri Subhash Ganguly, a professional statistician and an activist of Calcutta's Scientific Workers Forum, at the request of the authors of this report.

3 statistically significant, implying thereby that the toxic exposure could reasonably be taken as the cause of the observ­ed increase in the proportion of high excretors of thiocyanates.

Analysis and Interference

The results presented above have established beyond doubt that exposure to toxic emission from the Union Carbide plant led to an increase in excre­tion of urine thiocyanate in gas victims and this in­crease persisted until at least March, 1987, if not even later. This indicates the possibility of a sys­temic and continuing presence of certain toxic chemi­cals in the bodies of the gas victims, which are biochemically transformed into thiocyanate. This find­ing obviously cannot be attributed to earlier reports of a rise in thiocyanate content of the water of Bhopal lakes because (a) the increased rise was temporary and the elevated levels returned to normal by August, 1985, and (b) intake of thiocyanate from water would have equally affected populations’ amples from control and exposed areas.

Elevated urine thiocyanate levels do not necessarily imply cyanide poisoning alone, as has been assumed so far because of the well-known enzyme-catalysed transformation of cyanide into thiocyanate. The Minority Interim Report to be submitted later, would present evidence pointing towards the possibility of MIC itself being the cause of systemic and persistent toxicity in gas victims. Attention has been drawn to animal studies conducted by DRDE (Gwalior) showing that sodium thiosulphate may have anti­dotal role in animals exposed to pure MIC. In light of this, it would be pertinent to explore hitherto unknown biochemical basis of persistence of MIC or its metabolites and pathways for their removal (ie. detoxification) from human body. In this context, antidotes other than sodium thiosulphate may also need to be investigated.

The systemic and continuing presence of toxins, be they cyanide or MIC or any other chemical, in the bodies of the gas victims points towards the possible long-term risks to themselves and to their progeny, as indicated earlier. This clearly calls for much larger allocations for medical relief, rehabilitation, surveys, monitoring and research, covering the entire gas-ex­posed population for much longer duration than what have been so far anticipated on the basis of informa­tion furnished by Union Carbide.

There is a visible declining trend in the urine thiocyanate levels from September 1986 to March 1987 in both children and adults alike (see Table I). However, the statistical significance of this obser­vation cannot be ascertained on the basis of compari­son of mean values alone. For this, it would be neces­sary to compare individual thiocyanate values at both points of time, which unfortunately was not possible because the original data, lying in possession of the Head, Department of Medical Biochemistry, Gandhi Medical College, Bhopal, were not accessible to us. The importance of studying the declining trend may not be overemphasized, since it is only through its detailed analysis that meaningful predictions can be made about the duration for which the toxics would persist. This information is required for making a reliable assessment of the current and future toxicolo­gical risks faced by the gas victims. Clearly, no de­toxification programme can be planned without such an assessment.

An assessment of the current toxicological status of the gas victims would have been possible even with­out statistical analysis of the declining trend suggested above, had the direct monitoring of urine thiocyanate levels been continued beyond March, 1987. This, however, did not happen since ICMR, for inexplicable reasons, could not see the significance of the findings by the AIIMS team and decided to terminate the project. It is peculiar that ICMR has not even thought it fit to list this research project in its list of studies on Bhopal, published in ICMR research update of December, 1985. The ICMR research update of December, 1986 makes a mere four-line reference to this study, without even mentioning its findings concerning urine thiocyanate levels.

The above-mentioned decision of ICMR to termi­nate the project is baffling, to say the least. This study, when compared to the pioneering and much bigger study conducted by the Medico-Legal Institute, Bhopal, is distinguished by its scientific approach which involved the following features :­ (i) use of a more reliable and specific method of	estimation of urine thiocyanate,

(ii) use of a statistically valid method of sampling population; in contrast, the Medico-Legal Institute's study had investigated only hospi­tal-bound patients, that too without taking a random sample, and thus it has never been able to present a representative picture of the toxicological status of Bhopal's gas-exposed population. (iii) use of comparable controls whose only limita­tion was due to ambiguity in ICMR's coding of areas with respect to degree of exposure.

4 (iv) taking into account the effect of tobacco consumption on the output of urine thiocya­nate by (a) random sampling which ensured that probability of including or excluding a tobacco-consumer in the selected sample was equal for both control and exposed areas, and (b) including a sample of children in the study who for all practical purposes can be considered as non-consumers of tobacco,

(v) doing a follow-up analysis six months later on the same individuals, which makes it the only study on Bhopal victims known to us that has yielded information on the trend of toxicological status as a function of time.

Another distinguishing characteristic of the AIIMS team of investigators engaged in this study has been its willingness to share and subject its data to open scientific scrutiny.

Table I

MEAN URINE THIOCY ANA TE LEVELS OF CONTROL AND GAS-EXPOSED POPULATION SAMPLES FROM BHOPAL*

Age Group

Time of Testing

Status of Exposure

Size of Sample*

Mean Urine Thiocyanate level (in mg %)

Whether Mean (ex­posed) turns out to be significantly grea­ter than Mean (Con­trol) on the basis of statistical test?**

Children Sept. 1986 Exposed 67 0.52 ± 0.145 (0-15 yrs.)

Control 113 0.41 ± 0.109

March 1987 Exposed 67 0.44 ± 0.134


Control 113 0.37 ± 0.105 Adults Sept. 1986 Exposed 83 0.54 ± 0.159 (15 yrs & above)

Control 111 0.41 ± 0.135

March 1987 Exposed 83 0.48 ± 0.154


Control 112 0.37 ± 0.113


Yes

Yes

Yes

Yes

* Basic data taken from the report of ICMR Research Project No. 5/7/28E/84-RB (see Annexure 6).

* Indicates the number of individuals tested in each sample. The sampling was done on the basis of a ran­domised block design from gas-exposed and control areas as coded by ICMR. The size of each sample was determined on the basis of statistical principles and found to be adequate for applying statistical tests significance.

** The Test Statistic, standard normal deviate, was used for testing the statistical significance of observed differences in the mean urine thiocyanate levels (see Annexure 8 for details). The statistical analysis was conducted by Shri Subhash Ganguly, a stastician and an activist of Calcutta's Scientific Workers Forum, at the request of the authors of this repol1.

5 The Chernobyl Disaster-Potential Hazards to the Third World

C. Gopalan

Source : NFI Bulletin, July, 1987

Following on the Chernobyl nuclear disaster, many countries of the world had taken precautionary steps to ensure that foods from Europe possibly con­taminated by the radioactive fall-out were not im­ported into their countries. EEC had set up limits to the levels of radiation that may be permitted in foods to be exported. EEC had also imposed a ban on fresh food imports from Eastern Europe and selec­tive bans on their own food production. Third World countries, however, generally lack the infrastructure, testing facilities, and import control measures which are needed to impose and enforce such restrictions. Under the circumstances, it is not surprising that the Asian Regional Office of the International Organisation of Consumers Unions (IOCU) warned: "There is a possibility of deliberate dumping by some Corporations of contaminated food products in countries whose governments do no have the in­formation, infrastructure or expertise to check the entry of such items". That this warning was justified has been shown by subsequent events.

Despite the EEC ban on export of food products beyond certain radiation limits, it has been reported that the Philippines discovered in Many 1986 that 39 containers of milk of one brand and 4,000 cartons of another brand which arrived with "safety certificates" from Holland had, in fact, dangerou1; levels of radio­activity. Subsequently six other brands of milk powder from Holland and one each imported from Britain and Ireland into the Philippines were also reported to have high levels of radioactivity. These incidents confirmed the fears of Third World countries that "because of the EEC safety checks, multinationals which could not sell their products in Europe were deliberately diverting them to the Third World".

Singapore which has a fairly good food testing facility had to ban several consignments of imported food. Malaysia had to ship back 45,000 kg of butter fat. Sri Lanka, Bangladesh and Nepal had also de­tected high levels of radioactivity in food products and milk products imported from Europe and had taken steps to ban their imports. Jordan had rejected several consignments of meat imported from Europe and Iran had to strengthen its food testing facility in the wake of the discovery that several consignments of food imported from Europe were contaminated.

It has even been reported that finding that milk products from Europe were suspect, some milk com­panies had started making false claims as to their products' origin. Thus, in the Philippines, one brand from Holland was claimed to be prepared from "100 percent Australian milk"!

Unfortunately, not all Third World countries have been equally vigilant. In India, which boasts of a very large scientific and technical manpower and a whole chain of high-powered scientific institutions, a strange silence seemed to have descended on this entire issue. The Scientific Adviser to the Prime Minister, the Scientific Advisory Committee to the Cabinet, the Food Department and health and health-related agencies, all seem to have been strange­ly complacent and silent over this issue-this, des­pite the fact that we import large quantities of milk from Europe to sustain our Operation Flood and considerable quantities of milk powder for use by religious missions and other voluntary organisations in their relief programmes. While there was evidence of considerable concern and anxiety in our neighbour­ing counties over possible radioactive contamination of imported food after the Chernobyl fallout, there was not a ripple in our waters.

In view of this situation, in May 1986, I ventured to draw the attention of the Prime Minister's office to this matter. Giving the entire background, I suggested in my letter: "It is very important that we make ourselves absolutely sure that the milk that we are importing is safe. It will not be enough if an assurance of safety is provided by the exporter. Our previous experience shows that quite often un­scrupulous exporters have dumped in developing countries foods and drugs which were considered unfit and unsafe for use in their own countries. Faci­lities must immediately be set up to ensure that every batch of dried milk which is being imported is sub­jected to test before it is accepted."

The Prime Minister's office responded promptly to this communication and directed the Food and Agriculture Ministry to institute suitable arrangements for testing imported milk. I was given to understand in July by the Ministry of Agriculture that following on my commu- nication in May, arrangements had been worked out

6 ....... whereby representative samples of milk powder being imported for the Operation Flood programme will be submitted by the National Dairy Development Board to the Atomic Energy Agency for testing. While welcoming this arrangement, I indicated that while I have every confidence in the National Dairy Development Board and the Atomic Energy Agency, the present procedure might be con­sidered inadequate by the consumers. I suggested that apart from the manufacturer, the consumer and the health agency should also be involved in the regulatory procedures. I felt that there should be no doubt in the minds of the public with respect to the number of samples selected for testing and the representative nature of these samples. There is no indication that this suggestion has been acted upon. According to information available, not a single batch of milk powder imported into this country from any European country, following on the institution of the testing arrangement referred to above, had been founded to have dangerous levels of radioactivity re­quiring the rejection and return of any consignment! This appears strange, especially in view of the fact that all our Asian neighbours have had problems in this regard. This could mean that European exporters have been particularly kind to India and have been far more careful about the quality of their exports to us than they have been with respect to their exports to our neighbours. On the other hand, it could also mean that the present testing arrangements are not adequate and sufficiently stringent. The question that obviously stands out is: "Is the present set-up for 'scientific advice' to the Prime Minister, adequate, alert and competent enough to identify and advise on major issues related to our public health?"

XIV Annual Meet of MFC



Medico Friend Circle will hold its XIV annual meet at Jaipur, Rajasthan, on the 20th and 21st of January 1988. The theme chosen for discussion' this time is "Determinants of Child survival and Child mortality and their impact". Child mortality has always been an area of serious concern in our health policy but since the past few years, it has become the central focus as the result of its importance in the acceptance of family planning by our people. What is really meant by child survival in terms of its impact on the future health of our people? Is the exclusive empha­sis on survival appropriate without talking about the nutritional status of the surviving child? What are the other important factors in child survival be­sides the control of diarrhoeal diseases, immunization and nutrition? These are some of the issues that could be discussed at the coming meet. Background papers are under preparation and they will be circulated before hand to facilitate discussions.

They include: Child survival and population control; malnutrition and child survival; immunisable respi­ratory diseases and child mortality; controversies in diarrhoeal diseases control; housing, household eco­nomics and child survival; gender and child mortality. We invite you and others whom you know would be interested, to attend the meet and share your views and experience. Participants are, as usual, expected to pay for their own travel. Simple boarding and lodging facilities will be available at the venue on a payment of Rs 25/ per person per day. We charge a small fee to cover the cost of cyclostyled back­ground papers. Return reservation facility is also available. If you wish to attend, please write to us at: Medico Friend Circle, 1877 Joshi Galli, Nipani, Karnataka, 591237. We shall then send you the venue details and the background papers.

Dhruv Mankad, Convenor,

MFC.

Dear Friend

This is in response to your letter requesting com­ments on specific EPI related questions. Explanation for the high morbidity and case fa­tality rates noted in your editorial comments on the Singanikuppam measles outbreak (mfcb 126): It is not clear from your article what were the age-specific attack rates from the village and what percent of all cases were hospitalized. The true case fatality rates and severe morbidity rates can only be determined after knowledge of the attack rates for the geographic area from which these cases and deaths originated. Since we are all aware that measles cases are often not brought to the attention of health authorities, we can assume that many more cases occurred which were not reported. Therefore we would reserve comments until a full epidemic report could be pro­vided.

7

Other populations who have not been exposed for a period to measles which show these features on subsequent exposures: Unimmunized populations will typically experience periodic measles outbreaks as sufficient numbers of susceptible accumulate and the measles virus is introduced. Populations that have experienced mass immunization programmes and have not continued to maintain high coverage may also build up large number of susceptibles. The joint WHO/UNICEF statement on Planning Principles for Accelerated Immunization Activities specifically notes: A spectacular immunization effort, if not well followed through, can produce 'an equally spectacular epidemic. High immunization levels achieved only briefly, temporarily suppress the transmission of disease. Newborn can then accumu­late as susceptible children until disease transmission again becomes easy. It is at this point that an epi­demic may explode." (1)

Measles immunity: Measles immunity is consi­dered to be durable in its protection and there does not seem to be the phenomenon of grades of resistance. The immunization Practices Advisory Com­mittee (ACIP) has noted that 'Asymptomatic measles reinfection can occur in persons who have previously developed antibodies, whether from vaccination or from natural disease. Symptomatic reinfections have been reported rarely."2

Consequences for EPI/DIP: The programmes of EPI/DIP in India are designed to progressively in­crease coverage for a1l EPI antigens. Government is well aware of the need for, and is committed to, achieving and sustaining universal child immunization. Recommended number of doses of OPV: Each country must establish recommended immunization schedules based on the epidemiology of the diseases and the resources available. The EPI Global Advi­sory Group (GAG) has recommended that "in coun­tries where poliomyelitis has not been controlled, use of TOPV in the newborn period may be particularly important in R.N. 27565/76/ providing early protection. In this situation, oral polio vaccine is given at birth or first contact, with subsequent doses at 6, 10 and 14 weeks of age." (3) Immunization and AIDS: The EPI GAG has specifically addressed the issue of immunization and the spread of AIDS and has noted that: "the dis­covery of (HIV) virus as the cause of acquired im­mune deficiency syndrome (AIDS), coupled with the increasing recognition that retroiruses are cir­culating in many counties, has raised the question whether unsterile immunization techniques might contribute to (HIV) transmission. Thus far, there has been no demonstrated transmission of (HIV) as a result of immunization. Since the possibility exists that unsterile needles and unsterile syringes can transmit not only (HIV), but other infectious agents including hepatitis viruses, immunization programmes have the obligation to ensure that a sterile needle and a sterile syringe are used with each injection."(3) Robert Kim-Farley Regional Advisor, Expanded Programme on Im­munization for Regional Director. WHO, Regional office for South East Asia.

References : I. Planning Principles for Accelerated Immunization Activities : A joint WHO/UNICEF statement. WHO, Geneva, 1985, pp 17-8. I. Measles Prevention : Recommendation of the Immunization Practices Advisory Committee (ACIP). Morbidity and Mortality Weekly Report 1982; 31(17): 218.

3. Expanded Programme on Immunization Global Advisory Group. Weekly Epidemiological Re­cord 1986; 3 : 13-6. 4 Expanded Programme on Immunization Global Advisory Group. Weekly Epidemiological Re­cord 1986; 3; 16.

Editorial Committee: Anil Patel Abhay Bang Dhruv Mankad Kamala S. Jayarao Padma Prakash Vimal Balasubrahmanyan Sathyamala, Editor

Views and opinions expressed in the bulletin are those of the authors and not necessarily of the organization. Annual Subscription - Inland Rs. 20.00 Foreign: Sea Mail US $ 4 for all countries Air Mail: Asia - US $ 6; Africa & Europe Canada &: USA - US $ II Edited by Sathyamala, B-7/88/1, Safdarjung Enclave, New Delhi 11002.9 Published by Sathyamala for Medico Friend Circle Bulletin Trust, SO UC quarter University Road, Pune 411016 Printed by Sathyamala at Kalpana Printing House, L-4, Green Park Extn., N. Delhi I~ Correspondence and subscriptions to be sent to-The Editor, F-20 (GF), JUD8PUra Extn., New Delhi-ll0014.

8 
