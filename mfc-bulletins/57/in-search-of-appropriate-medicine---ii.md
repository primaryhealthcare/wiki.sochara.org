---
title: "In Search Of Appropriate Medicine - II"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled In Search Of Appropriate Medicine - II from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC057.pdf](http://www.mfcindia.org/mfcpdfs/MFC057.pdf)*

Title: In Search Of Appropriate Medicine - II

Authors: Jajoo Ulhas

medico friend circle bulletin

57

SEPTEMBER 1980

The Paramedics of Savar: An Experiment in Community Health in Bangladesh Introduction The People's Health Centre (Gonoshasthaya Kendra) is situated at Savar, some 30 miles from Dacca. The centre at Savar bas gained an international reputation as an example of integrated development; none of the familiar problems (health, family planning, food, even poverty) are dealt with in isolation. Health, however, dominates the activities of the Centre. Most of the Centre's 44 paramedics ore women. One of the center’s founders was Dr. Zafrullah Chowdhury. Chowdhury's team has achieved remarkable success as well as deserved recognition. But it hag not been without its setbacks. In 1976, a key paramedic was murdered in one of the local villages --illustrating the kind of opposition to pioneering health that exists among the wealthy elite, Dot least among the quack doctors, in the villages. Here Dr. Zafrullah Chowdhury describes the work of the Centre.

The poor health in our rural areas is a consequence of underdevelopment. Malnutrition is a problem not for the physician, but for the agronomist the teacher and the community organizer. A strictly medical approach cannot produce a healthy community and without the involvement of the community, anything that is produced will have a questionable value. Origin ally, the Bangladesh Hospital came into being during the war of liberation in 1971. At the close of the 'war it moved into the rural area of Savar Thana, which had no health centre. Health services for the heavily populated rural areas are virtually nonexistent. It was on remedying this that the Bangladesh Hospital, now named Gonoshasthaya Kendra, set its sights.

When we came to Savar in 1972, we held numerous meetings both with villagers and students in the area, to try to determine the best methods for bringing service to the people. We decided upon a centre base, which would act as referral point for a number of subcentres. Initially we recruited 100 part-time volunteers from among the students, to carry out the vaccination and health education programmes. From the beginning, we made efforts to ensure that Gonoshasthaya Kendra would be a People's Health Centre, rather than a community death and disease centre. 'Preventive programmes were emphasized and integrated with other areas of life that had a bearing on health, such as nutrition, agriculture and family planning.

The paramedic; In time, we discovered that the part-time volunteer workers were not able to fulfill the demands that the project work was making on them. We came to realize that a full-time paid worker was needed.

It was at this time, in 1973, that we developed the concept of the paramedic. This has continued to evolve, although we still define the paramedic as ' a worker who brings community development services to his own village. ‘From the beginning, we realized that a majority of girls would be needed if we were to reach the women of the area. 1 he paramedics are drawn from the area which the project serves, so they are working in. a familiar locality where communications are at their best.

They range in age from 17 to 25 years. Their training is carried out in the field, where they take part in the delivery of services, carefully supervised and supported by the doctors.

Some theoretical classes are given in the evenings, but the greatest strength of the paramedic is his or her closeness to the village, its unspoken needs, its wisdom and its ways.

which would provide them with a marketable skill, they would eventually gain a certain economic independence and respect.

Paramedics must show understanding and sensitivity to the life of the village. They do not preach vitamin A capsules, but rather local green vegetables. They do not ask the mothers to go (usually some distance) to a tube well for bathing, but they are pleased if the tube well water is used for cooking and drinking. Unlike the doctor who doled out two to six large Piperazine tablets to be taken at home, the paramedic had the child take the required treatment in a front of her. She is aware that a mother would hesitate to give such a large dose of medicine to a child at one time.

When we started our project, we became aware that demand for family planning services existed in the villages. The source of supply was lacking. So we began to offer a family planning service, but always within an integrated programme. Without real efforts at assuring parents that their young children would reach adulthood, we felt we could not with justice deny them the right to sons and daughters of their own. The programme has therefore made efforts to provide the needed health care, educating the parents in birth control methods and family planning to motivate them properly. Once the method has been chosen clients are visited at home regularly.

It was also the paramedics who questioned the wisdom of the antenatal clinics. Among the people being served in one sub-centre area (15,000 to 20,000) there would be approximately 800 pregnancies in a year. Out of this number, no more than 15 to 20 percent would be 'risk' pregnancies. Gathering all the women and having them sit unnecessarily was neither an efficient use of their time nor of the clinic’s. An alternative was to have the paramedics

pay

regular

visits

to

those

pregnant

women who are most likely to have difficult labour or other pregnancy problems, and give them the necessary

examination and instruction. The result is that we have had no maternal deaths in the area. The selection of the paramedics involves the villagers, which leads to a greater responsibility for the programme on both sides. Members of the community chosen to interview the new recruits are older villagers, but from among the poorer class. In the delivery of the service distance is always a problem. We sought to overcome it by the use of bicycles. Though quite acceptable for boys, girls on bicycles were a revolutionary step. It, took little time to win over the villagers, but the more 'educated' and religious leaders balked at the idea. Nevertheless, the plan went ahead—and not only does it solve the problem of transportation but it is also a definite step forward in the liberation of the women. The degraded social position of the women in the villages was what first moved us into the field of education. We felt that if they could receive some training

Family planning; demand and obstruction

The dai have also been successfully incorporated into our programme. Remaining in the village, she works on a parttime basis, distributing pills, checking (or side effects, assisting where possible and referring to the centre or subcentre where needed. She is also taught to spot per-eclamptic patients and other possible labour and birth difficulties and to instruct the mothers in regard to child care. Because the dais are village based, their drop-out rate is lower than that of the paramedics. Since the beginning of the programme in 1972, we have notice a steady pattern of clients moving towards a more permanent method of contraception, once family planning has been accepted. In 1974 we began to offer female sterilization, performed by the paramedics, and found that a relatively large demand existed for this method. The sterilizations are performed under local anaesthesia. Paramedics, having been trained to perform these operations, have proved themselves to be quite skilled. The villagers prefer the female paramedic to the male physicians, and it has been noted that the infection rate for the paramedics is lower than that of the doctors. The reason for this may be that the doctor is usually an occasional operator, and there is doubtless a tendency for him to assume the more difficult cases. Advice on menstrual regulation and abortion are offered at the clinic. More advanced stages of abortion are performed by the doctors, but the government attitude to abortion is somewhat ambiguous. A survey (Cont. on page8)

The Worcester Ward: Violence Against Women By The Coalition to Stop Institutional violence (CSIV) 'The coalition to stop institutional violence, a coalition of

‘violent' women, who have by reason of severe mental illness a

rationale for the unit, according to the dept. of mental health, is the need to provide ‘effective clinics treatment to patients whom the system has failed to treat adequately. We can all agree with them that the system fails as well as oppresses women and children and men as well, and the two departments have contributed to that failure, The return rate to psychiatric Institutions and prisons, the number of people who never leave them and the conditions of the lives of those who do, are well-known realities of that oppression.

recent and repeated history of behaviors harmful to themselves or

The proposed Worcester unit is not an aberration or an abuse of

feminists in the Boston area who have major commitments to the Women's Movement or 10 the advocacy movements for prisoners and psychiatric inmates, has been successful to date in delaying the construction and opening of a proposed behavior modification unit for women in Worcester, Mass. (USA). This unit has been defined as a small maximum security 'treatment' facility for so called

others and for whom all attempts at treatment in existing facilities

psychiatry but an example of psychiatric ideology. Psychiatry

have failed.

claims to be apolitical, yet is highly political because it denies the

Not to heal but to police

reality of oppression and power relationships or the need for

This unit euphemistically called the Special Consultation and Treatment programme for Woman (SCTPW) or the Worcester unit is the joint effort of two departments.

societal change. Psychiatrists are specialists on hew we should think. They are in fact mind police.

The

place is clearly a prison. On the fifth floor of an old state

institution is a long ward of open rooms on one side and individual adjustment rooms on the other. The exit is guarded by three metal doors and steel security equipment. The isolation cells marked 'adjustment' rooms make clear that what is intended is for women to adjust and become the way staff say they should adjust to this grossly abnormal institutional setting in order to be called normal and well.

Much of psychiatry, is based on the concept of a ‘sick' person, and the healthy professional person. In the community, ‘sick' people are separated from I well' people so that even those not involved as workers in psychiatry see themselves as different from the ‘mentally ill'. These mentally ill are then further labeled as schizophrenic,

psychotic,

obsessive-com,

pulsive,

manic-

depressive, ‘border line "and on and on. All these terms isolate actions called symptoms from a woman's personhood and entire

Throughout l973-76, the attempts to create a unit for

life isolate her from other people because she is seen as a fright-

women labeled ‘violent’ were opposed by an ad hoc coalition of

ening object that is other than human. All future actions are seen

legislators and advocates representing the prisoners and psychiatric

only in relation to these labels, with other possible social causes

inmates' rights movement, and the Women's movement. In 1976

discounted.

when it was clear that the state was increasingly committed to a unit

False assumption

women from the three areas of advocacy came together to form the CSIV Through education and public mobilization petitions and demonstrations, legal and legislative work and support for alternative shelters and healing places, the Coalition and its

supporters have prevented the opening of this proposed unit. We don’t believe these departments can truly assist women without radically altering their approach. The

1

The proposed Worcester unit is based on false and sexist assumptions about normal behavior for women. Eighty-five per cent of psychiatrists in all psychiatric institutions are men and nearly all are white. Women, Third World people, poor and old people are found in larger proportions in state institutions than in the larger society. The administrator and psychiatrist of these

centers however, are not from the working class communities in which they are often placed, but are generally from white and privileged communities.

The centers once again try to channel individuals into dependence on the 'healthy' elite. They discourage awareness that the person is political and discourage self-help in the true sense of the word. Thus they actually discourage community action on many issues. They focus on individual sickness rather than, for example, unemployment and lack of child care.

are drugged, most often- with phenothiazines. No one claims these drugs cure anything. Their stated purpose is to make people more amenable to talk therapy. Their effect though is to numb and control all thoughts and feelings. The real purpose is to break down defenses built up in an effort to cope with one's environment. These defenses are defined as inappropriate by medical standards.

Just as all women are reacting to the tensions and brutalities of every day life, many women who have in the past been classified violent or psychotic were simply relating to the tensions and brutalities of the lives they led or to the conditions in the institutions in which they were incarcerated. The authoritarian environment of the institutions is only a reflection of the world outside. The power relationships are the same. Women still have little control over their lives or bodies and consequently have little power.

The side effects of these drugs are their main effects. Over one-half get tardive dyskinesia and this brain damage is entirely caused by doctors. These drugs are used in all closed institutions: reform schools, nursing homes and prisons. They are also used au outpatients, in public schools and community mental health centers. ‘Minor' tranquilizers such as valium and librium are used by millions, mostly women, through private therapy. The bulk: of the profits of the drug companies in the US are made on psychiatric drugs.

To focus on the actions of a few women labeled violent is to obscure the violence of our society that is capable of driving each of us to assaultive behavior. Bu.1ding a facility for security and control is the categorical opposite of solving societal problems and supporting women as they resolve their crises. Psychiatric Institutions are the most brutal side of psychiatry. The poor are housed in state institutions. The living conditions are intolerable bad-food, over and underheating, and generally old buildings in poor condition. Physical, psychic and sexual abuses are a constant in inmates' lives. There is an unusually high death rate in psychiatric institutions. People are treated by specialists in art therapy, music therapy; .dance therapy and talk: therapy. This alleviates boredom perhaps, but it often avoids the real reasons wily the person was seeking help or forced into the institution. Private institutions while looking different, nicer and maybe seeming less oppressive operate on the same principle and towards the same goals. Contrary to the stereotype perpetuated by the media the vast majority of people locked down in mental institutions have not committed or been accused of acts of violence. Rather they have been imprisoned in hospital on the judgement of a psychiatrist about possible future behavior. Or they have turned in desperation to psychiatry in an attempt to resolve their unhappiness. The most prevalent form of treatment in institutions is drugging. Ninety five to 98% of all inmates

Seclusion is often used as treatment although it is restricted by law to emergency situations (as determined by staff). The seclusion room is a cheerless and cold cubicle with a mattress thrown on the 4100r. When you are put into a seclusion room, you are typically thrown on the floor, stripped and given a shot of phenothiazines. Then the door is closed and you are left naked, shivering and helpless. It is a totally humiliating experience. Seclusion is used as punishment for infractions of even the most minor nature. Other overt brutal forms of treatment and behavior modification are psychosurgery restraint and shock. Shock often causes permanent memory Joss. One of two kinds of restraint is most often used straitjacket or four-point restraint in which an inmate is strapped down by bindings on all four limbs. Once restrained the woman is usually left in seclusion. One of the reasons that there are not many laws to protect the rights of inmates is the prevalent concept that what is being done in mental institutions is handling specific problems not unlike medical problems. The medical model holds that physical illness and mental illness are comparable, and that both are specific problems which has a specific cause and can be treated with a specific intervention, usually drugs. (Physical medicine as we know it suffers from the same problem-that of zeroing in one symptom rather than socio-economic causes and then attempting to remove the symptom, often by drugs, without nece-

ssarily removing the cause). To use the word ‘sick' in describing an individual whose behavior disturbs us is to perpetrate an abuse against that person's integrity The use of the medical model serves to mystify people's pain and to create the impression that only professionals are capable of understanding that pain. In these ways it has become difficult for anyone to share subjective experiences of an unusual or frightening nature for fear of being labeled sick. Many people do not understand the inherently oppressive nature of the medical model of mental illness. Some people who are committed to working for social change fail to see the basic connection between psychiatric oppression and other forms of oppression.

Sexist bias A large part of the professional attitude, that women are inherently less mentally healthy than men, can be traced back to Freud who legitimated the belief that women experience greater difficulty than men in resolving the issues of psychosexual development. Freudian theory, which continues to shape much of current psychiatric thought, maintains a traditional view of the ' normal' woman as wife and mother. Women are viewed exclusively in terms of our sexuality and reproductive function; other aspects of our lives and personhood are either ignored or seen as less significant. For a woman to attempt to assert her independence or autonomy, to reject marriage or motherhood or to begin to take control of the circumstances of her own life is seen in the Freudian view, as evidence of a ‘masculinity complex’. Though much of Freudian theory has fallen into disrepute, clinician's in general still view women as less ‘healthy' than men. Clinician's concept of a mature adult was identical to their concept of a mature man, but their view of a mature woman was quite different. She was more dependent and passive and less assertive and adventurous than her male counterpart. Professional views of women's mental health are bound by sexist bias. A normal, healthy woman is a ‘feminine woman', one who does not necessarily develop or use the full range of her talents, and one who is less than an autonomous, independent, strong individual. Women who deviate from acceptable standards of behaviour are ‘sick’. This is a prime example of science being used by the members of the dominant class to justify their biases.

It leads to subsequent oppression and punishment of those who do not conform to their limited notions of what constitutes' healthy' behavior. What these views mean for women in institutional settings is that in order to gain their freedom, they often must adopt the pretense, if Dot the reality, of ‘feminine' ‘ladylike ' and therefore ‘healthy' behavior. Given this framework it is not difficult to predict which women will be sent to the Worcester unit. They will be women in institutions who demand their rights and who fight back when those rights are violated; women who are justifiably angry at the condition of their lives. They will be women who direct their anger at others, striking out in pain and rage and selfdefense, and also women who turn their anger inward, hurting themselves because they have been belittled, brutalized and have led overwhelmingly impoverished Jives. They will be poor women. Black women, Hispanic women, any woman who lacks sufficient money and privilege to escape incarceration. Many theorize that women's violence is increasing as a result of the Women's Movement. As women become liberated, so the theory goes, they will begin to adopt ‘male' patterns of violence and criminality. Feminists recognize the notion of the ‘new violent women' for what it is. While increase of violence committed by women is a myth, the increase of violence against women is a stark reality. Physical violence against women, in the form of battering, rape and assault continues to rise. Psychological Violence, sexual harassment, violations of women's integrity, have always been our daily reality. Women have rarely been imprisoned for violent crimes. The overwhelming majority of women in prisons are imprisoned for acts directly related to economic survival. Prostitution, larceny, forgery, shoplifting, receiving stolen goods drugs. Our goal is to dismantle the violent system, Our vision demands a strategy that includes four aspects: antiinstitutional work, support work for women already incarcerated, support work for genuine alternatives for people in crisis or distress, and personal practice to overcome the violence that pervades our daily lives. (Extracted from – Science for the People, Nov/Dec. 1978).

IN SEARCH OF APPROPRIATE MEDICINE-II Critical Evaluation of Utility of Chest Radiology in Common Medical Disorders Bed occupancy in private hospitals in India is about 40%, whereas

of overnight specimen of sputum at monthly intervals approaches

in government hospitals, where free treatment is ensured, it goes

closely culture examination and drug sensitivity tests in assessing

much beyond the bed capacity. Obviously the major governing

the progress of patients receiving chemotherapy.

factor is the cost of treatment. Much of the cost in patient care is due to cost of treatment. Much of the cost in patient care is due to costly investigations done without considering their utility and sometimes to hide our clinical inefficiency. Here is an attempt to evaluate the utility of chest radiology without compromising the quality of medical care.

2) To judge the activity of the disease from the radiological picture is difficult m any times, especially if it is an extensive fibrotic lesion Positive sputum for AFB alone is the answer. In case of a negative AFB smear, chest radiography gives very little information,

Possible indications for chest radiography can be: 1.

Diagnosis of a disease, if clinical examination and simple bed-side investigations do not help to reach the conclusion.

2. 3.

Follow-up of the case and to keep a permanent record.

Research and to detect subclinical cases and unknown associations of the disease which can manifest radiologically.

3) Chest radiograph can indicate the pathology namely cavity, fibrosis, collapse, effusion etc., and the probable etiology but needs positive sputum smear for confirmation. Thus chest radiograph is helpful in following conditions: a) Diagnosis of tuberculosis with negative sputum g. military tuberculosis, earlier TB lesion suspected on strong clinical

Utility of the chest radiology can be judged by the following

suspicion and in children or debilitated old people where

criteria:

sputum collection is 110t possible.

1. Has it confirmed the clinical diagnosis?

b) Follow-up of those tuberculosis patients who had negative

2. Has it altered the clinical diagnosis?

sputum. Chest radiography should not be repeated before 6

3.Has it helped to rule out some illness? Differential diagnosis.

months of regular anti-tubercular treatment.

4. Has it altered the treatment? 5. What is the risk involved in practice if radiological investigation is avoided?

Please Note that 30% of tuberculosis is self-limiting. Thus to judge activity of the disease in a defaulter; positive sputum and strong clinical grounds (symptom wise) are the best criteria to

6. What ate the technical limitations of radio logical methods?

decide the fate of treatment. It is desirable to se n d back the

If the patient's interest is at heart 1 is not justified, 2, 3 are

patient to the tuberculosis clinic or the hospital where he was

very much justified, and 4, 5, 6 are of real consideration.

taking treatment earlier, than to try to judge activity of illness from chest radiograph and modify the treatment.

Let us now try to judge the utility of chest radiology, diseasewise by these criteria.

Doubtful cases of lung lesion with negative sputum justify a trial with bread spectrum antibiotics and repeat radiograph after a month to compare it with the old radiological picture.

Pulmonary Tuberculosis: The following facts must be kept in mind— 1) Direct smear examination of two-spots specimen gives 70.4% positive results and two overnight specimens give 83% in patients known to have symptoms of tuberculosis. Direct smear examination

Pneumonia Majority of the patients can be diagnosed clinically, the most important sign being positive whispering pectoriloque. Most of the occasions, simple bronchitis is mis-diagnosed as pneumonia. This can be avoided if patient is re-examined in post-tussive

phase when crepitations of bronchitis disappear or diminish

emphysema, especially the centri-lobular type, have had normal or

considerably. Clinical follow-up of these patients in the ward for

almost normal radiograph prior to death.

two or three days without hurrying for chest radiograph in the beginning (when crepitations disappear in bronchitis) can avoid this unnecessary radiological exposure. If cases where localised crepitations at the end of inspiration

persist even after coughing, it is better to clinically follow-up these patients in ward. This may reveal bronchial breathing after a day or two. Chest-radiograph is justified in a case of pneumonia if:-

a) Pneumonia is suspected (dyspnoea) but no clinical signs

are located e. g. Deep seated pneumonia, viral pneumonia. b) In children and debilitated old people where clinical signs are difficult to locate. c) Non-resolving pneumonia i.e. fever and other signs persist for more than seven days in spite of regular treatment (Fever

Clinical parameters for diagnosis of chronic obstructive airway diseases according to priority are prolonged expiration, upper border of liver dullness lowered, superficial cardiac dullness obliterated and presence of rhonichi Prolongation of expiration and rhonichi may disappear in advanced cases but marked dyspnoea, cyanosis, clubbing and right heart failure now appear. Radiograph is indicated when:-

1) Patients with status asthmaticus do not respond to drugs or suddenly develop chest pain and increasing dyspnoea i.e. possibility of pneumo-thorax is suspected which is difficult to locate clinically due to emphysematous chest. 2) To rule out other cause of dyspnoea i.e. left ventricular

usually settles within 48-72 hours of antibiotic therapy,

failure.

crepitations become coarser. Complete radiological resolution

Pneumothorax: - This is one clinical condition where chest

may take as long as 6 weeks),

X-ray is of considerable help in diagnosis, follow-up and

d) In case of recurrent pneumonitis to find out underlying primary cause. e) For follow-up of viral pneumonia (if clinically occult and diagnosed radiologically) or small deep -seated lesion, after minimum 4 weeks.

Pleural effusion: - Clinically evident cases of pleural effusion can be confirmed by pleural tapping which alone will indicate probable aetiology. Chest radiograph is indicated in1) Suspected loculated effusions. 2) Suspicion of malignancy e. g. pleural effusion without mediastinal shift, pleural effusion with clubbing, pulmonary osteo-arthropathy, gynaecomastia, pleural effusion with hard lymph glands in neck, or hemorrhagic pleural tap. 3) For follow-up, sometimes radiograph may be needed, though screening of costo-phrenic angle is better.

treatment. It gives early indications of tension Pneumothorax, thus a very useful tool. Small Pneumothorax may easily be missed clinically and a chest film at the end of expiration is most valuable.

Bronchiectasis- There is considerable difference of opinion concerning the accuracy of plain film diagnosis of Bronchiectasis. Early lesions are often missed and advanced lesions are more obvious clinically than radiographically. Plain radiograph is useful pre-operatively for localised Bronchiectasis.

A Case of Fever: - A case of fever, when clinical examination and routine laboratory tests fail to locate the aetiology, needs further investigations. Most fevers are viral infections, which are self limiting. In actual practice, chest radiograph is usually taken to rule out possibility of miliary tuberculosis or a deep seated lung lesion. For all practical purposes, if patient is not critically ill, radiograph is indicated only in fever of more than seven days duration which has not responded to antimalarial and other symptomatic measures.

Chronic obstructive airway disease

Early diagnosis of chronic obstructive airway disease is most unsatisfactory. In fact, it is not unusual to find the patients with morphologically extensive

Tropical eosinophilia: Peripheral smear examination for eosinophilia count is diagnostic. Radiological picture may show miliary lesion or patches of pneumonitis which is not specific for tropical eosinophilia.

There is no doubt that radiological investigations are a great

Yet there are numerous instances where four of five such wells are

asset for the proper management of many chest diseases and that

installed within the radius of a mile. Often, these results in too

not only early but even extensive lesion can be missed clinically.

rapid use of ground water and local hand pump wells go dry.

Though a useful tool, it must be utilised with clear perception of

UNICEF has also paid attention to the problem of water, and

what we wish to achieve. It should not replace proper bedside

admirably put in efforts into supplying handpump tubewells. But

examination and follow-up of the case in day to day ward rounds.

generally the pump has been situated close to or within, the

These restraints lire desirable if we keep in view the poverty of the

compound of the Wealthy man, the man with power and influence,

patient and unnecessary radiation hazards for which we are to be

UNICEF's aim has been to supply, one pump for every 200

blamed.

people. Our suggestion was to make an initial payment of 25

Dr. Ulhas Jajoo Kasturba Medical College

paisa for each person using the pump. This would ensure that it was placed in a position advantageous to all members of the village. UNICEF did alter its original scheme, and decided after three years to make a charge for the pumps…250 taka for each pump

(Cont. from page 2)

conducted in Bangladesh regarding attitudes towards legalization of abortion found that, with the exception of engineers, physicians were the most conservative. Sixty two per cent of the physicians surveyed opposed the legalizing of abortion. Being far from the village reality, they cannot or will not, accept it.

Water; The problem of pumps They' have no king, the rivers of Bangladesh. At whim they rise and fall, and carry the fate of 80 million people in their course. They bring destruction, drought, dehydrated bodies, disease in a myriad forms, to green fields, ponds, fish, fertile soil. Water is the first authority in the land to whom poor and rich alike

installed, with no further payment required. Anyone individual

could make this payment of 250 taka. So, now the rich man could establish his full rights over the water. Not only installation but also real availability of water and latrines for general use will contribute to the better health of the community, cutting down intestinal and diarrhoeal diseases and skin conditions, The incidence of disease is decreased by the provision of water, irrespective of the quality, and an uncared for latrine has no appreciable effect on community health. What we need is a simple construction that can be cared for as necessary and is convenient for use. (Courtesy-Development Dialogue 1978, No. 1)

make their appeal. For want of water, or because of flood, the lands lie idle, yielding nothing. However, this need not be. Bangladesh has a farm labour force of approximately 19 million men, but only 12 million of them ale

Seva Mandir, Udaipur, wants urgently a young dedicated

employed, If land was used to its maximum advantage, rather than

doctor who has concern for the rural community and is interested

only producing just over one crop a year, there would be shortage

in training of village health workers. For details write to Kishore

of labour. Sixty seven per cent of the land in Bangladesh,

Saint, Gen. Secretary, Seva Mandir, Udaipur, 313001.

however, requires irrigation. One deep tube well irrigates an area of atleast 100 acres.

Editorial Committee: Anil Patel Anant Phadke Abhay Bang Luis Barreto Narendra Mehrotra Rishikesh Maru Kamala Jaya Rao (Editor)


