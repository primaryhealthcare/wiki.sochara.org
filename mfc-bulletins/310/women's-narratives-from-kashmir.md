---
title: "Women's Narratives from Kashmir"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Women's Narratives from Kashmir from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC310.pdf](http://www.mfcindia.org/mfcpdfs/MFC310.pdf)*

Title: Women's Narratives from Kashmir

Authors: Zamrooda Khandey

medico friend 310 circle bulletin April- May 2005

CHW: Survivals and Revivals 1 -Shyam Ashtekar

CHW Debacle in India The CHW scheme, an important 1978 option for reaching primary care/first contact care to villages of India was a dying scheme by mid-eighties; and now it is no more in 2002. The vacuum has been filled partially by somewhat similar variants from governments, but in the main by private doctors of various kinds in different states of India. As the already belated option of CHW failed to answer an important social need, markets have provided some options; for which the State and its policy makers and bureaucrats have to share the blame. Recent evidence suggests that there is very little that is learnt from this nationwide failure. To set the record straight, Government must publish the formal postmortem report of the CHW scheme, and if anything what was done to salvage the scheme through two decades. It concerned five lakh CHWs and their villages. In absence of such an official statement on the issue, I am sharing my thoughts and concerns.

Gandhi had started preparations for village level healers (see Home and Village Doctor by Dr. Dasgupta) but the initiative was lost in oblivion after Independence. ☼ Even though it was a belated effort to make

amends, especially after the Alma Ata mandate, the scheme lacked steam in several departments: conceptualization, technical design, content, training, political management, finance, monitoring, linkages, etc. Subsequent handing over to Family Welfare Department put the last nail on the coffin. The Janata regime lasted only 30 months and the later Congress Government was not impressed with the scheme. Medicines were withdrawn in eighties and then people forgot the CHW. Honoraria continued as litigations dragged and finally that too stopped. ☼ There was not much light at the other end of the tunnel either-the global experience of CHWs. A 1992 WHO review (TRS 780) was seized of this and so were other

☼First of all, the Indian health system planned under the Bhore Committee Report had little to offer on village level services, based as it was on a doctor-hospital centric state model of health care. The CHW appendix did not stick to the system. In contrast China's effort was much better, but our policy community is unwilling to admit this fact. It is noteworthy that Mahatma

1Email: < ashtekar_ nsk@Sancharnet.in>

Contact: Dr Shyam Ashtekar. Bharat vaidyaka Sanstha. Contact: Sriramwadi. MG Road, Nasik. 422 001. Tele fax 0253580147

experts (Frankel, 1992). In a personal conversation in 2000, David Werner, author of the famous Where There is No Doctor, also could not find any large-scale country CHW programme worth mentioning. China was a solid exception and others countries who had the Abbreviations: CHW: (Community Health Workers). COPC:

Community Oriented Primary Care, JSR: (Jana Swasthya}'a Rakshak-the new CHW programme in Madhya Pradesh) FCC: (First Contact Care). FRU (First referral Unit- the rural 30100 bedded

hospital), PHC: Primary Health Care. PMP: Private Medical Practitioner ("Quacks '"), PRMP: Private RMP. NHP: National Health Programme.

mfc bulletin/April-May 2005 CHW scheme offered attenuated versions (Philippines for instance). Brazil had introduced a CHW programme as late as in 1995. Was there then something genetically wrong with the CHW programme? Was it unfit for any national system and only good for NGO islands? .:. A hard look at various states in India shows that villagedoctors that nobody made or dreamt of in Nirman Bhavan or Lodhi Road have been spreading in every state of Indiaeast and west, south and north. Although some states like MP and Chhattisgarh are trying CHW variants in the states, at least the MP JSR scheme is headed the same way as the old CHW scheme (our own unpublished study, 200 I). This makes it imperative that we take a closer look at the issues and problems.

Understanding the Jigsaw Puzzle The social engineering of CHW scheme is a complex matter and we see very different models in various countries and NGO areas. To make the issue more explicit, all the three words in the classical nomenclature 'Community Health Worker' are rather vague concepts, especially in practice. Our questions are summarized in the table below. Are CHWs Necessary for Primary Care in India? In the new century, primary care is surely still necessary and valid; but is a CHW necessary for primary care? CHWs (or some of its variant) are surely necessary

primary care in villages (also parts of urban areas) in many states, if not all. However situation varies. as it may not be necessary like, say in Kerala. Conversely, in Bihar it may be frustrating to reintroduce the CHW as most villages have a home grown local medical practitioner.2 Come to states like Maharashtra and Madhya Pradesh, there are large numbers of private medical practitioners in rural areas, but they are clustered in some rural centers. In one study in Maharashtra (Ashtekar and Mankad, 2001), we found that in 737 villages there were 555 doctors of all types, but they were all practicing in just 16% of the villages. On a roadside village in MP, we found over 30 clinics of doctors (in addition to the mini-PHC) and obviously they depended upon surrounding villages for their clientele. In such situations, it is still possible to introduce CHWs in peripheral villages but they will have to prove themselves against the 'doctors'. I am aware that many experts may not like this connection but the reality is that even good CHWs find they lose out when an injection-doctor even starts visiting their village. First goes the curative and later go other health components. In short, a CHW may be theoretically necessary in all villages, but in the vicinity of doctors', the CHW option melts away. This is difficult to understand from a planner's and activist's end but very easy to grasp from the community's perspective. This leaves us with two tasks, an easy one and a difficult one: of choosing villages for CHW programmes and of improving the content of the programmes in several ways. There are some fresh initiatives on CHWs in some states: MP, Chhattisgarh, AP and in Maharashtra. The National Health Policy also makes some not very

for This is an impression from Janani, an organization working in Bihar and adjoining states. Their understanding is that each village has more than one private medical practitioner. 2

Community?

Health?

Worker?

Village?

'Development'?

Liberator, change agent?

Panchayat?

Health-especially primary health care/prevention?

Worker (lackey)?

Government health dept?

National health programmes?

Guide (only IEC?)?

NOO?

Narrow or vertical programmes?

Volunteer (little or no pay)?

SHG?

Primary medicare?

'A doctor' where there is no doctor?

Only left to individual?

Little of each above?

mfc bulletin/April-May 2005 nuanced and/or wooly references about options. To make

of private PMPs and share the agenda with them on

things clearer for new initiatives on the CHW, I am

institutional basis.)

making a matrix to underline the various issues and layers

I am giving a schematic sketch (see Figure I) here of three perspectives on the CHW: the planners, the community and the candidates/care providers. Unless we combine the prime concerns of each perspective, there cannot be a 'successful model'.

involved. Most of the early and current Indian CHW programmes tried to steer clear of the complexities and give an innocently simple 'minimal version' CHW programme and we know that they have faltered or failed.

Expanding the Search for Primary Care 'Model' Since the main objective is and must be primary care and CHW only a means to it, I have included village doctors as a pragmatic and existing option, and I am not alone on this

At the end I am presenting a table depicting what is likely to happen to all the issues of concern when we deal with different models of primary care: Government, private or combined.

since there are experts like Jon Rohde who wrote a book on rural doctors (Rohde and Viswanathan, 1995). On this wider canvass of primary care by CHW

3

I had a brief interview with Dr Gerald Bloom, an

schemes, NGO experiments and private village doctors,

expert of the Bristol University, who has an intimate

the complexities deepen. The approaches, inputs, processes, and outputs change according to the vehicle of primary care. My argument is that even if a classical State CHW model is not feasible in some states/districts, it is still feasible to make use

understanding of the China health scenario. As I was describing the ubiquitous PMP-quacks in India, he shrugged and said that it sounds much like China s rural doctors in the village health stations, many of whom have little training and are rarely monitored. I was taken aback by the comparison; can products of two entirely different systems be alike?

Larger Matrix of Factors Related to Primary Care by CHWs Place in

Who

Technical

Personnel

Health System

Owns it?

Content

Policies

Supports Stability

Access

Political

and Trends

Factors

Situation

Formal

State

Task list:

Entry/

Program

Distance

Demo-

Mal-

External MainStream

Expected and/or Actual

Selection

Life, Attrition

Factors

cratic, Centralized

practice

ContriCommbution to/ Unity share of health services

Training Systems, Books, software

Mobility/ Transfers

Financial Expanding Cost or Factors

Democratic and Decentrali zed

Survival

Political

Shrinking?

Provider Orien-

Upward

(private) tation: tiveor Preven-

Mobility

Legal

Oligarchic/

Factors: Authori(Caste and tarian Gender

Curative Use of Different Healing Systems

Social

in India) Age/Sex

Institutional

One Party Rule

Problems?

PLANNERS'CONCERNS . Selection: gender, age, education . System links-does CHW fit well in the health system? . Not clashing with other HWs gets along well with them . Preventive tasks, national health programmes . payment to be linked to performance/more from users . Relations with Private/ISM doctors . Training /system/books/CDs/TV channels/specialty training. Monitoring and controls, fine tuning . Supplies/logistics/multi-sourcing/ non-drug healing . Works within rational therapeutic framewor1c/CME . Not to be part of salaried staff, . Low attrition rate . Programme durability, gets rooted in the health system . Service authorization/ CPA immunity . Universal or Village selection on criteria? . 'True attitudes' of Govt health system about JSR . Institution for CHW?

Programme Content: Skills and tasks, Attitudes of

providers, Knowledge,

Relations

COMMUNITY CONCERNS

PROVIDER

CONCERNS

. Basic needs-survival . Monetary gains/incentives . Self worth

. . Security- professional . Motivation for action . Learning opportunities Sense of belonging

. Upward mobility

.

Does CHW answer usual

medical needs?

. Linkages . Better than other available healers?

. Friendly? Accessible? . Economical? . Lasting/dependable?

Figure 1: First Contact Health Care, Perspective Mapping

mfc bulletin/April-May 2005

Possibilities with 'Primary care' Models

(A) Planner's Concerns Major Selection

Sub-issues Gender: men or women

Usual Possibilities4 with staff model Men or women depending upon policy. Women tend to take even small pay jobs.

A Combination model

Village doctor model

Mixed-alternate Village/both man and women in each village/couple Post-twenty five year candidates, other health cadres

Men mostly

Age

Late teens/early twenty candidates Govt. jobs

Education

Age strata will decide entries

Negotiable

Caste

Any

Any is possible on criteria

Attitudes of

Work

Declines with tenure,

Candidates

motivation

upward mobility if any.

Depends upon candidates/returns/ work satisfaction

Learning Communication Candidate Locality

Distribution

Training Monitoring

Initial course

Generally programme Combining self-interest plus related. Little motivation programme interests of their own. More with administration, Possible to ensure with both less with people/users administration and users From anywhere, unless Generally from locality salaries limit choice to locality Not so evenly spread-small hamlets can be attached All, even hamlets, through. Sustainability is depending upon available prime concern. May not funds and pattern survive in less than 2000 without contract payments. Can begin small, Qualifying necessary, stepladder CME can be done

Generally post 25 Need to be 20 yr+ for respectable earning Generally upper and middle. Monetary gains are the deciding factors for attitudes. Self-learning, limited to skills that can sell Only client-oriented. Usually from outside

Only big villages, clustercenters. Cannot survive on small population-below 2000. Overcrowding of PMPs can pose serious problems. Initial crash course, little CME later

Social aspects

Poor control

Feasible-

Poor control

Technical

Theoretically possible

Possible- programme wise

Poor control

Govt PHC/CHC

Govt for NHPs, from market for other needs. Can increase choices with better training and public

aspects Medicine supply Healing systems Preventives

Generally allopathic

Overall

Programme-specific

Education Programme-specific, but expandable

NHPs

NHPs on priority

NHP on contract

Market, Medical Reps Generally allopathic

No interest (actually sickness-interest) Poor compliance for NHPs

Rational therapeutics Program

4

Protocol-driven/ narrow Yes and No

Possible with standard list and rates.

Weak

Can be stable

Generally stable, though with some flux. Increasing competition can destabilize PRMPs

Assumes appointment of one primary care worker in each village.

System linkages Costs

To the Govt. To the consumers

Payment modes Financial sources Venue for work

Can be kept moderate, Need to be designed and administered Medium

Negligible/inbuilt

High Low or nil (except in case Medium to both of bribes) Combined: user paid at Salaries/honoraria/ prescribed rates + contract pensions payment for NHP/State programmes Taxation/grants to local User-fees or insurance plus bodies programme grants Formal center desired, but Formal center necessary interim arrangements possible

Low to nil Highest

User fees Fees or may be insurance at later stage. Private room in bazaar lanes essential.

"Do not care", generally some cover is available. Unlikely, except when Possible, as payment is on both husband and wife are contract for tasks/services practising. Janani programme in Omnibus-GLY scheme in Bihar th the 10 FYP of Maharashtra, Community Nurse in AP

Legal status for providers

Easy-with Govt notification Possible to work out

'Couple'

Not possible

Examples

Negligible, Difficult and tenuous always abhorred

ANMIMPW

(B) Provider (Candidate) Concerns 5

Usual Possibilities with staff model

Planners can manage' with a What happens with PMP combined model model

Incentives

Mandatory-housing/ food/transport/security Felt as 'always meager'

Income

Fixed-effort or no effort

Some costs are less thanks to local High, ever increasing. residence. Always eager Variable with services and Adjusted to services and tasks opportunity

Self worth/public image

Unduly low, tormented

Can live respectfully and socially useful career.

Unduly high

Learning

Limited to directives

Can be woven into the programme.

Limited to sales promotion

Both

To professional guild and user community

In between, banks somewhat or Govt Policy

Ever searching for better position

Major Basic needs

Belonging To Govt. system (sense of) Professional Fair, because of unionization security/stabilit y Generally fail to keep pace with Supplies needs Upward Limited, (a neglected issue in Mobility India)

5

Self-procured, so usually ensured. More equipment facility Limited, but skills can be improved. upgrading.

Assumes appointment of one primary care worker I each vii/age

6

Assumes provision of facility on village showing some preparedness, proper candidate, etc.

(C) Community Concerns Major Healing: (Medical needs)

Usual Possibilities? with staff model

Planners can manage' with a What happens with PMP combined model model

Only limited, may not satisfy, Good healing + satisfaction may or may not heal. mandatory for survival

Time bound, programmelinked, not dependable May be free, if not doing Economical? private practice Friendly? Depends upon the person transfers, and visiting nature Lasting? makes it look less like lasting Not really-because of various Dependable factors Poor, works through long User control Politico-administrative links. Access

Satisfying it must be (but may or may not heal)

Ensuring good access is precondition Can save access costs and needless medication Professional requirement.

Time: elastic, but often distant. So access is limited High costs. and also hidden costs Professional requirement.

Can be

Generally

Can be fairly controlled.

Omnibus Scheme on Gramin Lokswasthya Yojna (GLY) in 10th in Maharashtra Even as the CHW scheme vanishes in thin air and pada health workers scheme is equally evanescent, the primary care group of Maharashtra was arguing for a comprehensive and realistic alternative scheme for needy villages. Thanks to various circumstances and forces, five years of efforts resulted in inclusion of GLY in the 10th FYP of Maharashtra. I call it 'omnibus' for various strategic and conceptual reasons. The scheme will be tried on a pilot basis in 1000 villages first, with help of NGOs to start with. Later it will be expanded. We are trying to mainstream it with help of the Open University, legal status under MMC, Panchayat & SHGs.

Generally dependable and accountable Poor control on quality of care

References I. Dasgupta. Home Village Doctor. Khadi Pratisthan, Calcutta, 1942. 2 WHO: Strengthening the Performance of Community Health Workers in Primary Health Care. TRS 780, Geneva, 1989. 3. Frankel, Stephen. Overview, The Community Health Worker. OUP, 1992, pp 1- 62. 4. PHA Calcutta. "Document Resolutions", p3. PHA Secretariat, Bangalore, Dee 2000. 5. Ashtekar, Shyam and Dhruv Mankad. "Who Cares? Rural Health Practitioners in India". Economic and Political Weekly, Feb 3-9, 200 I.

7

Assumes appointment of one primary care worker in each village.

'Assumes provision of facility on village showing some preparedness, proper candidates, etc.

6. Rohde, Jon and Hema Viswanathan. The Rural Private Practitioner. OUP, Delhi 1995, pp 37-57.

Available Revised Edition

Impoverishing the Poor: Pharmaceuticals and Drug Pricing in India Publishers: LOCOST, Vadodara and JSS Bilaspur, Dee 1004 Pages 180, Price Rs 100/- For copies, write to <locost@satyam.net.in> or <jss_ganiyari@rediffinail.com>

National Rural Health Mission1 : Comments by the National Coordination Committee of Jan Swasthya Abhiyan 1. 2.

3.

4. 5.

The Common Minimum Programme adopted by the UPA Govt. was a conscious effort to respond to the mandate given by the electorate in the recent elections that brought the UPA to power. The National (Rural) Health Mission, as a response to this mandate is welcome by the Jan Swasthya Abhiyan. There was an initial apprehension that the national Health Mission would be reduced to a targeted contraceptive programme that is neither desirable nor effective. Recent assurances that this would not be so were accepted by the national health action networks and this has led to our engaging in this process of responding to the Health Mission agenda. On the 18th of December 2004, the National Coordination Committee of the Jan Swasthya Abhiyan met and formulated its response to the proposed Mission agenda and action plan. Dr. Anita who had been following up the developments with the Prime Minister’s office, with the Health Ministry and the Planning Commission, briefed the members of coordination committee on the developments in the formation of the mission and the initiatives he had taken to shape the agenda of the Mission. After discussions the national Coordination Committee of Jan Swasthya Abhiyan decided to put forth a broad consensus on basic concerns regarding the Mission and its suggestions for strengthening the Mission for Strengthening primary health care.

The broad consensus that has emerged is articulated below as five guiding principles that are essential to implement the mandate of the Common Minimum Programme:

5.1 The Mission should be based on a sustained increased budgetary allocation to health care and not be based on donor funds or ad hoc reallocation offends from the very limited existing scanty resources available in the health sector. The need for an immediate and substantially increased budgetary allocation to health, at least doubling of this by the year 2009, has a very broad based consensus and is one of the cornerstones of the CMP section on health; the Mission should be seen to be responding to this consensus. 5.2 The Mission should can for and coordinate with increased investments and better design and implementation in a number of

sectors related to health - especially food security, water and sanitation, elementary education, shelter, urban development, environmental safety, poverty alleviation programmes and livelihood issues. The universalisation of the ICDS programme must

be part of the same mission or must proceed in parallel and close synergy with this effort. 5.3 The Mission should ensure that investments in health not only increase universal access but also address issues of gender and socio-

economic inequities in health status and health care services. Health and access to quality health care services are basic inalienable human rights and should not be dependent on the ability of the citizen to pay for health care.

For example basing the proposed Community Health Worker programme on the charging of user fees would run 5.4

completely counter to this perspective. The Mission should be a programme to strengthen comprehensive Primary Health Care within which RCH must be positioned as one of the components. The rural health care mission should not become yet another vertical "fragmented" program. While recognising the need to engaging rationalise the use of resources, improve access and quality of services in the public health system, the Mission needs to ensure that the planned interventions strengthen public health systems and build upon the basic framework of the facilities built up over the

decades. The Mission would have to have strong links with the Panchayati Raj System and move towards devolution of greater funds and responsibilities in health care to the Panchayati Raj System. In terms of strengthening public health systems the Mission has proposals in four areas to which we submit the following response: 5.5

a. b. c. d.

Community Health Worker Programme The Subcenter and the ANM The Primary Health Center and Community Health Center Role of the Private Sector

a) Community Health Worker Programme The move for a nationwide community health worker programme is a most welcome development We refer here to the proposed ASHA, which we refer to by the generic term community health worker (CHW). However the lessons of the past programmes, the evidence from detailed evaluation reports and our shared theoretical understandings of the potentialities of the community heath worker programme lead us to draw your attention to some major concerns and suggestions: 1. Not a substitute but a supplement to ANM: The CHW is part of an approach to strengthening the public health system: The CHW should not be seen as a substitute to the ANM and the Subcenter, but as a supplement to these. These two different functionaries are both needed and have distinct functions. The introduction of the CHW must therefore be accompanied by parallel measures to strengthen the Subcenter. Any move to replace an adequately paid, systematically trained and supported cadre by a much less paid and less trained and supported CHW would be a big step backward and would rightly be seen as using the CHW as an excuse to dismantle the health system. The CHW/ ASHA programme must become an idiom of strengthening public health systems - not an element of weakening it. 2. CHW’s important role in securing - health services as _________________________

1 The official power point presentation on the Rural Health Mission is available at http://mohfw.nic.in/nrhm.html.

basic rights: One essential corrective is to strongly introduce accountability of the health system and securing of health rights of the people into the work definition of the CHW. This requires strong community support and mobilisation. It also requires that though the CHW works with the ANM and anganwadi-worker - she does not work under them. 3. No to user fees for prescriptions and services - rejection of the RMP model: The CHW's remuneration should NOT be based on her charging fees for prescription as a private practitioner prescribing drugs in the open market- similar to what was attempted under the Jan Swasthya Rakshak programme. Glorifying this as a social entrepreneur should not blind us to the evidence accumulating that in programmes like the earlier Jan Swasthya Rakshak programme, the rise of such a cadre of ill qualified private practitioners raises the cost of medical care to the poor without any corresponding increase in health benefits.

but is also facilitated by trained, sensitised facilitators through a monitored process and that the elected Panchayat formally ratifies the selection. 5. Hamlet as unit or village as the unit? There is a strong case for insisting that the habitation and not the hamlet should be the unit of selection. The one per 1000 population would mean that the main hamlet-largely of upper caste would get adequately covered and the smaller outlying hamlets usually where the needs are

greater and the population more marginalised would again get left out. This has been the experience of the entire anganwadi programme and even of earlier CHW programmes. Geographical distances make village as the unit untenable in places like Chhattisgarh. Jharkhand and Rajasthan while caste tension make it non functional in places like Bihar and Uttar Pradesh. Situating the CHW in each habitation instead of each village cuts across these problems. This of course increases the number of CHWs The evaluation report of the JSR scheme in Madhya proposed substantially, and it increases costs Pradesh points out that 90% of the JSRs had dropped out proportionately - but it would dramatically increase outreach and within a year of being trained! Most of those who were diffusion of messages and services. functional were not practicing rational medical care and were 6. Qualifications and certification: How essential/feasible are indulging in irrational, exploitative "injection practice". The they? Educational qualifications should be a desirable criticism we raise of not accrediting or building a health care qualification and not a mandatory rule. Otherwise the system around the illegal and unqualified rural practitioners is based on an objective recognition of what such care poorest habitations will find no suitable candidate or would means in medical and social terms. Studies that have have to agree to candidates from a different socio-economic commented positively on their service have typically failed to class. include the rationality of care within their scope of the study. Similarly certifying them all through one syllabus fixed Hence they have failed to recognise that most care given by centrally is neither feasible nor desirable. Local processes the RMP and similar informal practitioners of modem should be built around a core syllabus, both in terms of medicine is wasteful and redundant care for trivial content and method of training. Assessment should be self-limiting illness, or inappropriate and often made at the local levels (with the block as the unit) to hazardous care for serious illness. The vulnerability of Jan ascertain whether they have reached the desired Swasthya Rakshak and similar health care providers to knowledge and skill levels, based on a standardized regional curriculum. Certification would be for functioning as unethical influence from pharmaceutical companies is also underestimated. We strongly feel that if the CHW in their habitation and within the programme Rural Health Mission leads to addition of Lakhs of ill trained guidelines only. RMPs to the existing menace of RMPs, the whole purpose of 7. Build on - do not replace - existing CHW programmes: ensuring Primary Health Care to the rural poor would be lost We note that with the spread of such "RMPs" health care expenditure has now emerged as an important contributor to rural indebtedness. Most of such expenditure is totally unnecessary and senseless; this wastage should be condemned rather than encouraged. Irrationally practicing RMPs are an index of the failure of the system, a part of the problem and not a part of its solution. Introduction of an RMPlike CHW scheme is our single greatest area of disagreement with the Mission draft as it is currently articulated. 4. Selection of women candidates by the community and what it implies - To enhance the process of empowerment of women, this skilled work should be entrusted only to women. The process of CHW selection should be carried out by the community, through a process that ensures that this choice is made after the community has been well informed of the programme and after due care is taken that the voices of weaker sections have been facilitated and have been given space during the selection process. This requires that the decision is always made in the general body of the habitation,

Existing CHW programmes run by NGOs or run by state governments would continue- with necessary strengthening rather than be replaced by the Mission initiated CHW programme. 8. Training and support should be a continuous effort and not one time- the programme should therefore be built and budgeted to continue for at least a five year period. The training programme should be in short spurts of three to five days at a time once in two to three months till the necessary competence is built up. This may take as long as 18 months to build up. Following the foundation training, continuing inputs such as follow up training, skill up gradation, revision classes and annual assessment should continue for a period of at least five years from the initiation of the programme. 9. Curative Care is essential though not central. Curative care would be an essential part of the training programme and CHW function- but it would be supplementary and not central to the definition of the CHW and her functioning. 10. .Financial Compensation to the CHW: The compensation, for the services carried out by the community health worker may take the form of enhanced incentives paid for loss of livelihood on the

days

of

training

(about

Rs

100

per

day

of

training). With 30 days of training in the first year, 15 days of meetings and training in subsequent years, the training compensation would amount to Rs 3000 in the first year and Rs 1500 per year in subsequent four years. To this may be added from the second year onwards a performancelinked honorarium / incentive, which should be commensurate with the average number of hours, spent by the CHW per day in various types of health related work, and may be calculated at least at the level of the rate of minimum wages for skilled work. This performance-linked honorarium would be paid by the Gram Panchayat, based on an annual grant received by it from the Government. Payment of this performance linked honorarium could be based on two types of annual assessment: a) The general body of the habitation (equivalent to Gram Sabha), assisted by the hamlet health committee, would annually assess the social performance of the CHW on the basis of certain specific parameters (actions taken by CHW to help the community to assert their health rights; monitoring of village level health services expected to be delivered by ANM / MPW; whether nutritional advice is given to mothers; monitoring and support to the Anganwadi by CHW on behalf of the community; assistance given by the CHW to patients by enabling them to obtain care at the PHC, etc.) b) The ANM / MPW, assisted by Hamlet health committee, would annually assess the technical performance of the CHW in terms of knowledge of a sample group of mothers about basic nutritional practices; number of patients who have received appropriate First Contact Care; attendance of eligible children/pregnant women during village immunization sessions; attendance by pregnant women during village antenatal checkup sessions; number of suspected serious cases referred to the PHC by the CHW etc. II. Process sensitive selection of programme leadership: There is a need for full time staff recruited from motivated NGOs, for monitoring and training support at the level of a cluster of villages, at the block, district and state level. In some of these levels, in some of the districts we would find

NGOs to whom this role can be contracted out. In many districts and blocks these full time programme leaders would have to emerge from a process of social mobilisation that precedes and continues through the programme. We note that very often the issue is raised that ''we cannot get an Arole or an Anita each block to provide the sort of motivated leadership Jamkhed had". Unfortunately such an assertion becomes grounds for therefore dumping all the basic process elements of the CHW programme in favour of some well tested and failed short cut approaches like the user fee. The true answer to this assertion is a question" yes - there is an Arole or an Anita in every district, indeed there are many but what processes should we follow so that they would emerge?" We reply to this question saying that there are many possible answers in each district. But to find them one needs to ensure that in project implementation there is a civil society partnership provided at the stat~ and national level, which can physically visit the various districts and catalyse the emergence of project teams in a flexible and principled manner. This is one of the key lessons of the Mitanin programme that are worth emulating.

12. We note that Mitanin programme has been one programme that has scaled up CHW interventions to the state level and the problems and design changes that scaling up create should be studied here and used for designing the national programme. 13. State specific names for CHWs: Finally, though we are not making an issue of it, we suggest that instead of ASHA we allow each state to choose its own name- like Mitanin, Sakhi, Swasthya Karmi etc., and in official papers use CHW or CHV as the generic name. This is not only on theoretical grounds (should she be accredited, what does social health mean and should we not use activist if in practice she is going to focus on) but also keeping in mind what would be attractive in the popular idiom and what term would create better ownership amongst the states.

b) Strengthening of the Subcenter I. Jan Swasthya Abhiyan notes that one of the most effective components of the existing public health system is the subcenter and within this the Multipurpose Worker Female, otherwise referred to as the ANM. We also note that even this component needs far more investment and design

improvements to reach minimum desired levels of effectiveness. 2. The single most important suggestion we put forth is to have two ANMs per subcenter. The budgetary burden of this would work out to a sum of Rs 80 crores per year, which is extremely affordable (note, for purposes of comparison, that the Govt. was committed to raising) 800 crores for six AIIMS). A second ANM is every subcenter halves the work burden of the ANM- and would be a welcome step. In tribal areas it would become one per) 500 population or even less almost the norms proposed for the CHW. The capacity to generate so many ANMs is relatively easier to build up. Also by halving her work burden one finds the space to make her work more comprehensive and therefore more responsive to the community's felt needs. Further if there are two ANMs per subcenter the subcenter can be kept open during all days allowing it to undertake functions like institutional delivery. 3. The location of subcenter buildings may be rationalised so that optimum coverage is attained. Since most buildings are not built it is an opportunity for the Mission to complete the gap in this infrastructure through a one time all out effort and simultaneously improve the efficiency of these services by better location. The estimated budgetary support for infrastructure gaps at the subcenter level is in the range of Rs 5000 crores. Spread out over five years at Rs I 000 crores per year this goal of an adequate subcenter as per norms along with accommodation for the ANM can be attained. 4. Expanding the ANM drug kit to 25 drugs and a better level of training on curative skills and home and herbal remedies based on standard treatment guidelines evolved for paramedical would also enable the ANM to respond much better to felt health care needs and therefore give her much more credibility and respect in the local community. We note that it is incongruous to express so much interest in training

RMPs and a new cadre of CHWs without ensuring that the current functionary most available in the periphery is not yet trained on such curative care. Nor is this perceived as increased work burden. Indeed there is a request from ANMs to receive such training as it gains her better local acceptance. (At /0 patients per day per subcenter. 250 patients per month, Rs. 5/- average drug cost per patient. the medicine cost works out to about Rs. /5.000 per year per subcenter. ) 5. The male worker may continued as a state government funded cadre but retrained to provide all the support services needed as a paramedical- the functions of

dresser, compounder, basic laboratory technician, communicable disease control interventions with the community as well as to provide support to the ANMs. 6. The subcenter needs to be strengthened with communication facilities, road access and transport access

so that it can be a site from which referral transport can be called for and referral to higher levels undertaken where needed. 7. The subcenter and the ANMs need the CHW programme for effectively reaching the core health system goals. One is not a replacement of the other. The CHWs a semi-honorary worker should not be burdened with routine service delivery tasks of the ANM, and it may be kept in mind that she does not have infrastructure and much equipment with her. Her role is more in

increasing

awareness,

mobilisation

and

organisation of women and the community for health, and facilitating village level service delivery including provision of First Contact Care. The subcenter is on the other hand aims to provide all the services that paramedical can be trained to provide. And therefore its functionaries are therefore given a full day's wage. (We note that the Ministry's presentation on the inadequacy of the subcenter had located the problem exclusively at its

being made to follow a population norm instead of a village or geographical norm. This is at best a very simplistic reduction and whimsical understanding of the sub center’s limitations and perhaps accounts for the lack of sufficient discussion of the means of improving subcenters) 8. Problems of accountability and decentralisation are best met by making the ANMs and MPWs a block level cadre recruited and paid by the block and village Panchayats to whom the entire funds of the subcenters are devolved. (Existing ANMs are deputed to the block level cadre.) c) Primary Health Center and Community Health Center I) There has been a mistaken trend to recommend doing away with or diluting the Primary Health Center. This is not explicit but the situational analysis is indicative. More worrying there seems to be a large informal consensus building up against this facility amongst some "experts" which since it is not placed before formal public scrutiny is not being refuted. Even if these suggestions are well meaning - they are nevertheless seriously mistaken. 2) The case for the primary health center rests on two criteria

-

one epidemiological and one social. The epidemiological

include short periods of inpatient care, e.g., care in cases of diarrhea with dehydration, institutional delivery, epileptic attack, first aid/first contact curative care prior to transfer, for example in snake bite or poisoning. However inpatient care is peripheral to its function. The sociological consideration is that there is a general correlation between the distance people travel to seek treatment and the nature of illness that they have. Patients could not bear the expense and time loss of seeking health care at too much distance in most circumstances-unless there is a serious prolonged illness which gives them enough time and compulsion to raise the funds and make the journey. Nearer the facility, the earlier the patient would seek care and therefore the greater the reduction in mortality and long-term sequelae. 3) The very poor utilisation and therefore cost-effectiveness of the PHC at present is mainly due to its very poor functionality. At such poor levels of utilisation of all three facilities, only a subcenter and but that is conceding to a CHC approach may appear to suffice very poor levels of care provision which would be sub-critical for major reductions in mortality and morbidity. The poor functionality of the PHC owes to three design weaknesses of the PHC: a. A very selective package of care provided by the PHC which is completely mismatched with people's felt and urgent health care needs- thereby destroying the credibility of the PHC as a

-

health

services

provider

and

also

seriously

underutilising the doctor and support staff. b. Building up all care around the presence of a doctor - all the other six to eight staff seen as adjuvant to the doctor. This in a context where the medical doctor is often not available at any time of the day and almost universally not available on a 24-hour basis. Thus on one hand we almost invariably have number of underutilized staff available in parallel with vacant staff positions-and all of them rendered wasted when the doctor is absent. c. A failure to differentiate the nature of in-patient care provided in the PHC, from that at the secondary care level (leading to the curious statement in the Mission introduction that the problem with the PHC is the failure to differentiate outpatient work from in-patient work. 4) To strengthen the PHC, there are five key suggestions: a. Firstly to ensure that all sectors (a health administrative area corresponding to a PHC) have a PHC. Currently about one third of sectors in the weaker states do not have one. b. Secondly to close various infrastructure gaps in the PHC. c. Thirdly to build the services on a 24 hour basis around trained multiskilled paramedical operating in shifts providing preventive, promotive services plus a wide variety of basic curative services built around a paramedical standard treatment guideline. (This includes basic curative work, institutional delivery, first aid for injuries, immediate treatment before referral for more serious illness, management of diarrhoeal epidemics, etc. d. Fourthly, especially in difficult and medically underserved areas, it has been seen that non-availability of the doctor reduces the functionality of the PHC, which may be the only

facility capable of giving the services of a qualified doctor and short indoor care in the remote locality. Keeping this in mind, assurance of proper living and transport facilities, backup electricity arrangements. a 'possible schooling / family allowance (to support the schooling of children in a nearby town) and time-bound short postings (ceiling of two years) for doctors working in remote, 'difficult' PHCs may be considered, as measures to help ensure the attendance of

doctors. e. Placing the sector PHCs, including all the paramedical staff under the district and block p3nchayats as a district cadre with recruitment and services linked to the district situations, may be considered. The funds for this may be given to the district Panchayats and block Panchayats. Where the area is medically underserved the Panchayats can be somewhat flexible with pay packets and perks as well as with other options, while observing certain basic norms. 5) The role of the CHC has been recognised in all the Mission's background documents. The objective of reaching about 7000 well-equipped secondary care centers approximately at one per 100,000 population, is excellent But the situational analysis does not seem to recognise all the reasons why this has not happened so far- despite this need being well recognised over the last thirty years at least. Other than the well-recognised constraints of funds and equipment the other major constraints relate to governance issues linked to workforce management policies as well as the approach to getting the necessary specialist skills in place. These latter constraints are not impossible to overcome, but they require both political will and astute administrative action. 6) The danger lies in the readiness to accept the accrediting of private sector as the main -even sometimes as only alternative- to break the deadlock in the creation of secondary care centers in the public health system. The Jan Swasthya Abhiyan cautions that given the problems of governance and the completely unregulated environment in which the commercial private sector works, the hasty switch to public -private partnerships as the main means of closing this gap in secondary centers is bound to be disastrous. Not only will the private sector fail to fill this gap. In the process the thrust to build up the secondary centers network in the public health system linked to the primary care network seriously undermined. For would be considerations of cost effectiveness, governance issues and of feasibility of providing an adequate dispersal of these centers, the bulk of secondary care provision would have to remain as public provided and public funded facilities. 7) The Mission's suggestions for strengthening urban health care, referral transport services etc. are all welcome. We however need to make the same cautions that we made for private provisioning of secondary health care regarding too readily accept the weaknesses of the public sector as inevitable given the nature of governance available today and jumping into untested notions of public private partnership which would suffer even more from these same problems of governance. 8) The National Human Rights Commission has adopted a

set of recommendations for moving towards the attainment of health as a basic human right. These recommendations look also at policy frameworks that are needed and monitoring and regulatory mechanisms that are needed to make the health system function better. These also need to be incorporated into the Mission objectives d) Role of the Private Sector I. The need to involve the private sector in the provision of the affordable services for the poor and to contribute to national health goals is appreciated. But there are some important concerns to be addressed before this becomes policy. 2. The flourishing unregulated and illegal sector (often called the informal sector or RMPs), is symptomatic of the break down of the health care system and this must not be projected as the solution to rural health problems- much less equated with the private sector in health and as a structure that needs only training and accrediting for integrating into the health system. Recognising that this informal sector exists because people have no where else to go and also noting that it reduces sharply in extent once qualified service of even moderate quality becomes available, there is no need to actively proscribe and punish the informal practitioner (as demanded by the IMA). However, we note that their continuation depends so closely on the mystified doctor-disease-drug perception of health care that they have become one of the main means of propagation of a wrong perception of health in the public and actively contribute to poverty by a wasteful consumption of unnecessary health goods. In the context of a large CHW programme, a strengthened subcenter and a PHC and CHC structure made functional, there would be no need to invest in this informal structure, which by its very nature would be impossible to regulate and is now a pivot of all wasteful and wrong notions of health care. 3. The formal "qualified" private sector is also almost completely unregulated in pricing of services, in quality of services and in ethics. A regulatory mechanism in place would be mandatory before public private partnerships can be forged. We note that the National Human Rights Commission has also called for such regulatory mechanisms to come into force. 4. The experience of private provisioning and public funding of services is limited and problematic. Most of the deals have had poor transparency and no public scrutiny. They have required considerable public investments in private hands with poor access to the poor in return. The cost benefits and cost effectiveness of such options have not been worked out. There are no social insurance programmes reaching out to the poor to which private provisioning and public funding of the programme may be linked and in its absence reimbursement alone would prove

-

too costly. A national policy framework for engaging the private sector, incorporating regulation of the private health sector, and with safeguards against such initiatives becoming avenues for expanding misgoverance, must be drawn up and scrutinised by thorough public debate before it is fil1alised.

Public Hearings for the Right to Health: An Analysis of Different Approaches Public hearings have emerged as an important mechanism for sharing violations of economic, social and cultural rights in India. The campaign on the Right to Information has pioneered the use of public hearings and now it is being increasingly applied to health related rights. A series of regional public hearings organised by the National Human Rights Commission and the Jan Swasthya Abhiyan concluded with the National Public Hearing on the Right to Health Care on December 1617,2004. One of the successes of this process was the adoption of a National Action Plan by the NHRC that suggests further actions for realising the right to health care. The success of this series of public hearings, coming as it does on the heels of the earlier successes of the Right to Information campaign, highlight the importance of this mechanism in asserting and claiming social, economic and cultural rights which are otherwise non-justifiable.

Health in the Context of Human Rights Though many human rights experts claim that the division of human rights into civil-political on one side and social-economic-cultural on the other is an artificial one, the acknowledgement of this division is of great importance where the lives of the poor and marginalised groups are concerned. Civil and political rights are those human rights which not only enjoy legal protection, but also much greater recognition and visibility. Arbitrary detention, torture and extra-judicial executions are some instances where individual civil and political rights are applicable. Social, economic and cultural rights, on the other hand, do not enjoy specific legal protection. This is unfortunate, because most of the human rights, which are necessary for the dignified survival of marginalised communities, fall into this domain, including, among others, the right to health (Article 12 of the ICESCR). The right to health is also recognised and reinforced by sections in the CEDAW (Convention on the Elimination of All forms of Discrimination Against Women), CRC (Convention on the Rights of the Child), and ICERD (International I Email :< abhijitdas@Softhome.net>

-Abhijit Das1 Convention on the Elimination of All forms of Racial Discrimination), as well as regional human rights treaties. Despite much official recognition, these rights are not protected by law, and the international consensus is that the principle of progressive realisation should apply. In the case of India the Constitution takes note of these rights in Part IV, or the Directive Principles of State Policy. Although these rights are supposed to be fundamental in the governance of the country, and although it is the duty of the state to apply them while making law, they cannot be enforced in a court. Progressive realisation has proceeded at a very slow pace (if at all) and the Indian state often fails in its duties as described in the constitution. Thus the distinction between civil and political rights and economic, social and cultural rights is important because it reminds us of areas where commitments have been made but justice mechanisms are lacking, and that we still have a long way to go to before we can ensure a dignified life to all citizens of the country. Neo-liberal globalisation is a well-known threat to the lives of the economically vulnerable and often leads them to further destitution. Neo-liberal globalisation also champions the rights of individuals (and even that of corporate entities) to trade without any restrictions based upon the protection of states for their right to do so. However, neo-liberal economic reforms pay little or no heed to economic, social and cultural rights, including the right to health care. In the realm of public health, neo-liberal reforms are manifested in increased privatisation and fee-for-service policies which make it even more difficult for the poor to access health care services. While the National Action Plan adopted by the NHRC (after the Public Hearing on the Right to Healthcare) has many provisions and activities for realising the right to health care, given the realities of our country, this process will continue to remain an uphill task. The poor and marginalised will need to continually assert and demand that their rights and entitlements be respected, protected and fulfilled. If public hearings are emerging as one of the mechanisms through which this assertion may be made, it is necessary to examine the strengths and limitations of this process as well.

As Mechanism for Asserting and Claiming Human doctors or drugs, and the equipment and supplies lie rusting or rotting while the poor are forced to take Rights recourse to potentially dangerous, unqualified The articulation of a human right is several steps practitioners. Tens of millions remain without safe away from either protecting or claiming a human drinking water and sanitation is a distant reality for right. The United Nations Conventions and an equal number of people. It is clear that the right to Committees engage in a process of clarification and health has tremendous implications for the life and interpretation by preparing General Comments and dignity of a large proportion of the citizens of India, General Recommendations. In the case of the right to even though it enjoys little protection. health, or specifically "The right of everyone to the enjoyment of the highest attainable standard of Public Hearings and Truth and Reconciliation physical and mental health", this has been through Non-state and international interventions for the General Comment 14 of the ICESCR. While protecting human rights include identification of international health experts note that the right to rights violations, fact-finding of such cases, health does receive legal protection through some instituting monitoring mechanisms, the preparation regional human rights mechanisms, and that of reports, in some cases setting up tribunals, as well domestic mechanisms exist in some countries as lobbying and advocacy to ensure redress and (Report of the Special Rapporteur on Right to Health compensations to victims and punishment to 2003 E/CNA/2003/58), specific international perpetrators. Truth and reconciliation commissions mechanisms to ensure and protect this right have not have also emerged as mechanisms for addressing been developed. In the absence of such mechanisms large-scale human rights violations which take place there is a need to develop alternative methods for in repressive political regimes. These are usually claiming and protecting this right, and public instituted after a new regime has come to power and hearings can be seen as one such method. State action for ensuring and protecting human rights are meant to bring some form of closure to the includes the process of framing specific laws, humiliation faced by the repressed groups. In India, developing monitoring mechanisms and ensuring public interest litigation has emerged as a legal justice. While this is often adequate for civil and mechanism to expand the interpretation of existing political rights, for economic social and cultural law for the protection of human rights, where rights governments need to institute special specific provisions do not exist. It is necessary to programmes for providing services — as in the case find a place for public hearings within this broad set of health or education. In India, there are few of mechanisms. constitutional protections for the right to health. Among the interventions mentioned, above public Aside from interpretations of the right to life, there hearings can perhaps be compared to truth and are some laws, which relate to health, such as the reconciliation commissions, however there are some National Maternal Benefit Act, Child Marriage differences as well. Truth and reconciliation Restraint Act, ESI Act, MTP Act, Consumer commissions (TRCs) have been constituted in a Protection Act, and others. Some sections of the number of countries, though the one in South Africa Indian Penal Code, notably those relating to consent, is the most renowned. These are extra-judicial homicide, negligence and hurt can be applied to the commissions where survivors and perpetrators of an realm of irresponsible service provision. However, acknowledged event in which human rights these mechanisms are grossly inadequate to protect violations took place are expected to provide the right to life of over one and quarter lakh women testimonies. For the survivors it is a public who die during pregnancy and childbirth, even acknowledgement of their experiences, and for the though mother and child health have been areas of perpetrators it is an acknowledgement of their role in special attention for health programs since the violation of rights. It is expected to bring closure independence. Tuberculosis continues to affect over to the suffering and sense of violation of the two million new people every year and kill survivors and simultaneously make the perpetrators approximately 500,000, even though it has been part publicly acknowledge their culpability. of a separate mfc bulletin/April-May 2005 national thrust for over 30 years. Many government hospitals and health centres remain unoperational, without

i .1

~

.

TRCs do not typically include punishment, but the public acknowledgement of the suffering of survivors helps those groups come to terms with their past and face their future with a new sense of purpose. There are a number of differences between public hearings in the current Indian context and a TRC. First, the public hearings that are under consideration take place in more or less the same political and administrative setting that in which the human rights violation took place. Second, the perpetrators (who are administrative and governance systems) are also reluctant either to recognise their violations or assume culpability. Third, TRCs have usually been organised to address gross civil and political rights violations and not address social issues like livelihood, food, women’s rights or health.

Issues and Challenges In India, as has already been mentioned, public hearings became popular mechanisms for communities to express their dissatisfaction and expose human rights violations as part of the Right to Information campaign. Public hearings were organised to expose the huge corruption that had taken place in spending development funds in Rajasthan and elsewhere. Subsequently the National Women’s Commission and different state commission's have started using hearings to address women's issues. There is one significant difference between these hearings, organised by statutory commissions and the ones organised by civil society campaigns. The women's commissions are parajudicial bodies of the state and are empowered to take action and give instruction to different agencies of the state. The hearings organised by the women's commissions include representatives of different government departments and instant justice is delivered by instructing these officials to redress the grievances of individual claims and complaints. In this regard these hearings do not represent the collective expression of a single claim, but are a collection of individual claims, most of which are of a similar nature. Public hearings organised by the Right to Information campaign, on the other hand, represent the collective expression of a single claim or violation. This distinction is important to understand in order 1

Information on the Right to Information campaign was

obtained from <wwwfreedominfo.org> and < www.transparency.org>

To examine the use of public hearings in cases of the right to health. As mentioned at the outset the notion of a jan sunwai, or public hearing, has been used by the Right to Information campaign as a mechanism of non-violent protest. However, it goes beyond mere protest by adding evidence and documentation to substantiate the common violations faced by the poor in a community and by arguing that development-related corruption and deprivation are in fact violations of the right to life. Apart from being a practical weapon for exposing corruption, these hearings also provide an opportunity for deprived communities to assert their citizenship rights in a democracy.1 The violation of the right to health care affects a marginalised community uniformly inasmuch as the whole community is either deprived of health care services or has inefficient, callous and corrupt services. However, this lack of services has a much greater impact on those with a perceived health problem than on those without. In this regard, a public hearing can appear to be a set of grievances of individuals rather than an entire community's claim for health. This difference has a significant impact on how the claim is perceived and expressed. If the public hearing emerges as a platform to voice individual complaints, then the purpose of establishing a common community claim may be diluted. Individual Grievances or Collective Claim? The distinction between a "collection of individual grievances" and a "single collective claim" can be understood by considering public health in the context of a human-rights framework.. Classically, human rights were applied only in the context of an individual's right to remain free and autonomous and to receive the support of the state in order to do so. It has even been argued that the concept of individual rights is applicable to the developed world of America and Western Europe but conflicts with the sense of traditional community identity that is prevalent in much of the developing world. Over the years, however, human rights scholars and practitioners have applied an individual rights concept to make claims for the rights of marginalised groups. Since most traditional communities are deeply hierarchical, they allege that the human rights of a marginalised group are made up of the individual and

common claims of each member of the marginalised group, and in this way, the group can be seen as a col1ection of individuals. The right to health care can be understood within this framework. Thus public hearings on the right to health ought to address individual violations but at the same time weave these individual testimonies into a more complete and unified expression of a community claim. The sense of unified community assertiveness is essential for the struggle to claim the right to make demands beyond the life of the public hearing. Quick fulfillment of individual claims may be detrimental to this process. Addressing individual 'cases' may also deflect attention from the systemic failures and inadequacies that lead to violations of rights in the first place. Another issue that needs careful consideration during the public hearing is the role and presence of the perpetrator group (health service providers and managers). In the case of a TRC, the perpetrators obviously do not enjoy the same privileges that they did earlier and so are forced to come face-to-face with their abusive acts. But in public hearings, especial1y those organised by civil society movements, it can be difficult to ensure the presence of government parties, and even more difficult to make them accept culpability, even when they are present. In para-judicial public hearings (those organised by legally constituted commissions) where the presence of government agencies can be ensured, case by case orders for grievance redressal can be detrimental to the overall purpose of establishing a common community rights violation, for the reasons described above. The government parties can also challenge individual testimonies, seriously undermining the fragile self esteem of vulnerable groups. And finally, individuals who share their testimonies may later be victimised by the agencies that are indicted. This can reduce the sense of assertion, which had enabled the marginalised groups to come forward with their claim or violation in the first place. All of this is not to argue that the redressal of individual rights violations or claims for compensation should not be expressed through public hearings. What perhaps needs to be addressed is how this demand is made and followed-up. This leads to the question of who takes the initiative for organising public hearings and what should be the mfc bulletin/April-May 2005 essential follow-up

after it has been organised. Public hearings are and should be seen as essentially political events of assertion of a claim and expression of a violation. They are significantly different from a public consultation or dialogue where individual's testimonies are presented so that common elements can be derived from the various stories and then the powers-that-be can decide on their course of action. The greater the distance between the community whose rights have been violated and the location of the hearing, the greater the chances that the public hearing will become an academic exercise rather than a political one, because distance determines who organises the event. The ownership of the public hearing should ideally be with the community who is expressing its claim/ violation. It should be a process initiated and controlled by the community. This precludes individual grievance redressal taking precedence over the expression of a collective claim. Even the individual claims for redressal and compensation can then be followed up through a collective process. When claims are settled, the victory would not simply belong to one individual but rather to the whole community. Similarly, as an assertion of its collective strength, the community can engage in other interventions such as filing cases in criminal or consumer courts, raising public interest litigation, or other forms of collective action. The role of an intermediary organisation has to be seen in this context and should not supercede that of the community.

Conclusion The imperatives of economic reforms often lead governments to abandon basic social sector reform. In this age of neo-liberal reforms, the Indian health sector seems to be condemned to die a natural, slow death. However, this is against the core interests of hundreds of millions of citizens in our country. It not only leads to poorer health outcomes at the individual level, it also stands against the fundamental tenets of the constitution. In such a situation, public hearings can become both a powerful mechanism for communities to demand health related rights and also a political tool to strengthen the process of participatory democracy. But they must be designed, carried out and followed up in such a way that it assists individual redressal and strengthen community processes at the same time.

Women's Narratives from Kashmir-l -Zamrooda Khandey1 Dear readers, this is a series of narratives of women who have been living in far flung villages in Kashmir not known to many people outside the state. The villages are remote and underdeveloped with minimal contact with people outside the boundaries of their villages. These villages as is the story of most villages in Kashmir have in the last sixteen to seventeen years of conflict survived almost without a trained or a qualified doctor. This series has been written to bring forth the women who are living in these villages silently bearing the brunt of a war that they never understood or wanted. I confess that in terms of the state of medical condition this series has nothing new to put forth. But there is just one difference; and that is the ranging conflict in the state for the last sixteen years. The state of Kashmir has a medical infrastructure that is as defunct as the medical system in any part of India plus it has a ranging battle to negotiate. It is this conflict that makes life as been termed by the research participants "unbearable". The second reason that has bothered me enough to put this series together is the fact that prominent scholars wanting to know why the right of reproductive health is such a big concern during the years of conflict when there are other major issues to be looked into? The only thing that I want to say is that women like other people in conflict have rights and when the reproductive rights of the rest of the population are not taken away then why are women denied this right.

Badila Badila is a forty-year-old woman mother of two living in a small village. She is supporting her family single handed, as her husband due to his leg disability is not able to work for a living. Badila considers it her luck that in days when women's education was not heard of, her parents educated her till the tenth grade. Even now she is one of the few women in her village who is educated and the only one who is working. She describes the various bans imposed on women as a way of subjugating them. She is a devoted follower of her faith yet does not understand the bans that were imposed on people in general and women in particular. She describes them as a method to exercise control especially on the women of the valley. She says that even if sor.1e of the bans were justified by the holy book (Koran), yet one has to see them in the light of the period in which they are being imposed. Citing her own family as an example she explains why the ban on movement of women was irrational. She says that Koran had divided work for men and women by which men 1

Email: <zamrooda@hotmail.com>

were to go outside the house in search of work while women were to look after the house. This system worked very well in those days. But today, in her own house if she was not to go outside and work who would feed her family. "We would all die of starvation. They wanted to spread their terror all around that is all. . . Today when I look at it I think they really wanted to send us women back to dark ages." Women and especially women's health was the most affected by the militancy in the state. She says that hospitals were allowed to function because the militants needed them for their own treatment as well. Yet the circumstances made the hospitals dysfunctional. Destruction of infrastructure, lack of staff and supplies, unavailability of safe transport for staff to be able to report on duty or for that matter for the patients to be able to reach the hospitals rendered the available services ineffective. People in the villages were left with no choices but to rely on untrained medical practitioners posing to be doctors. Badila is very open about saying that these untrained doctors were the lifelines of the villages. If it were not for them many people would have died without any medication. She is honest in saying that though she is aware that these doctors can harm a patient yet in times of need she would definitely consult them. Talking of bans, Badila says that she could rationalise all bans but can never understand or justify the ban on family planning methods imposed by the militants. This was a ban that bought in hardships like women had never seen earlier. "Hardships that really affected the health of the women, it became almost impossible for women to get any kind of safe treatment in the hospitals." When asked what she meant by safe treatment she said that, "the hospitals were watched, so women could not avail the facilities offered by them. Even if a woman would gather enough courage to go to a hospital the doctors would refuse her. Everyone was scared." Narrating her own experience in organising a "safe" ligation operation for herself this is what she had to say: (She smiles): "Yes, I did, but only I know what I went through for that operation. No hospital, no doctor was willing and I was sure that I did not want it done by a local doctor. There was this one doctor from our village, who was very good. He was in the hospital in Sri Nagar. I knew his family very well. Finally I requested him to do something for me. At first even he was not ready. I literally had to fall on his feet to make him agree. If it had not been for him I don't think I would have ever managed the operation. He took pity on my state in life and arranged for the operation.

He arranged for an ambulance to come and collect me Report in the middle of the night. I was taken to some place I did not know. I was operated upon and before morning Adivasis Speak out for I was back home. No one knew of it except for my Implementation of Supreme Court husband and the doctor. For a very long time I did not tell my in-laws. They would have been very upset, and Orders they were when they got to know about it, (smiles) but by then it was too late for them to do anything. I was not the only woman who must have hidden it like this. The adivasi belt of Panchmahaals and Dahod has often We had no choice. The militants would have gunned been referred to as a drought-prone or flood-hit region. Scratch the surface a bit harder and the stark reality of the us down if they had come to know about it. Now when issues of food insecurity stare at the people dwelling in this things are better we have started talking about it. But region unblinkingly. even now one is careful about whom one talks to." As part of the Anna Suraksha Adhikar Abhiyan, Gujarat. She goes on to say that there were times when even ANANDI (Area Networking and Development Initiatives) along parents or husbands were not informed. with Devgadh Mahila Sangathan and Panam Mahila According to Badila, what made things worse was the Sangathan organized a two-day convention on Dec 6-7. 2004 fact that the ban was not followed in totality. She says to create awareness and expose the irregularities that exist in that people always found ways to get around them. "It the system. is similar to my case. No doctor was willing to operate The public distribution system (PDS) lies in shambles as yet I got my operation done, similarly if one knew the "unfair shops" are running without a bother. The manner in right people one could get an abortion done as well. which APL (Above Poverty Line) and (Below Poverty Line) But yes it was not easy. Hearing me talk about it you cards are distributed raises doubts. Widows, jobless adivasis, may feel that it was all very easy but believe me it was old and impoverished ones have been "awarded" with APL not. We had to face a lot of problems and these cards, thereby depriving them of basic rights to food security. operations were done in conditions, which were not Issues pertaining to lack of proper implementation in various government schemes were discussed on Dec 6 in group safe for even an animal. Actually women were treated meetings to create awareness among the women who like animals in those days. Why else will otherwise all gathered in droves at Sagar Mahal in Devgadh Baria. these restrictions be placed only on us? There was Nutritional food insecurity could be one of the contributing never any ban on the men. They were free to do factors for high prevalence of anemia and chronic energy whatever they wanted to. deficiency among the women participants. "Treatments were carried on in utmost secrecy and On the second day, a play by the tribal artists on issues of would not be done unless the "doctor" knew you food insecurity created room for Jan Sunwai. Over a fifteen personally or you were well referred. It was not easy hundred affected people from Panchmahaals. Dahod, to gain access to these doctors. The conditions under Sabarkanta, Bharuch, Surat, Narmata, Valsad and Baroda which the treatments were carried out were so bad. I district besides people from Saurashtra and Central Gujarat once accompanied a relative of mine. When I saw the and Chhattisgarh shared their grievances at the Jan Sunwai. room in which it was done I thanked my God that I did Bajiben Rathwa of Diviya said no road ever reached their not have to go through what I was e witness to. It was village, which lies in the midst of forest, inaccessible to the a small shanty room in back lane of a poor outside world, as there were no transport facilities. There is no neighborhood of the city. There was just the doctor fair price shop (FPS) in Diviya. Their condition worsened and me. All through the operation I kept wondering during flood and there was a case of death in the area where in the victim Bodiben starved to death, as she had no money what if something goes wrong what will happen to her. to buy from regular shops. In Gajapara 49 cardholders (BPL) The closest hospital was about 4 kilometers away. have been given only 5 kg wheat 2 kgs of rice while the More than anything else the fear that someone may entries are made for 9 kg and 3 and half kgs. come in was traumatising not only for the patient, but As per a recent Government of Gujarat GR. per card for the doctor as well as me who was accompanying allocation of food grain has been increased from 12 and half the patient. We all knew that if some one came to to 25 kgs only as against Supreme Court order of 35kg. know we would be killed within a second. I am still However, the additional food grain is available at higher rates amazed that the doctor actually managed to do the thereby violating SC orders. Five women in Ankli were not operation successfully. To be honest if I was in his given edible oil in the month of September, but the entries for the same were made in their ration cards. position I would never have had the heart to go According to Budhabhai Titiyabhai of Lavaria (Devgadh Baria through so much pressure and tension. That was the taluka), "I get only 5 kgs of wheat and 3 kgs of rice every only time I agreed to accompany a person for an month in the Antyodaya Yojana:' operation. After that one experience I simply refused I did not have the courage to go though it again." (To be contd.)

As per the Yojana he is entitled to 35 kg of grains. Gangaben Baria, a widow from Ranjitnagar (Ghoghamba taluka) has no ration card. Her son is disabled and has no other source of income and is deprived of government schemes. When the panelists asked how many people there are facing similar problem, barring a few, all hands went up. Sayeedaben from Boru (Kalol) 'spoke on behalf of other 176 Muslim families of her village. Their ration cards and other important documents were reduced to ashes during the riots of2002. "The Sarpanch of the village also fled and we are left in the middle of nowhere," she said, Navliben Mura of Gajapara (Ghoghamba taluka) holds a BPL card, which was later, stamped as APL. Similarly, several others like Mangiben Ratan of village Bor (Ghoghamba) and Dayliben Shankar Gajapara (Ghoghamba taluka) faced the same problem. During the discussion it was found that most of the people present there had the same confusion whether they were APL or BPL. All panelists agreed that there is no marked difference in determining "how poor is a poor person" and is based on individual perception. Shri Prashant Joshi, DSO (Panchmahaals), responded by maintaining that as their department lacked manpower, the task of preparing the cards was given to a "private company" which led to the confusion. He has, however, committed to taking a look at these cards and revising them if necessary. The panel was chaired by noted journalist and activist Shri Indukumar Jani and included senior officials, activists and researchers. Kailashben Baria from Ruparel village (Ghoghamba) and Veenaben Parmar of Chandranagar (Ghoghamba) complained about irregularities in midday meals. The stock also finds a way to the cocordinator’s house. Santokben Baria of Bara (Baria), a cook in a primary school for mid-day meal scheme said, there was no kitchen shed, which is a necessity as per the SC order, Dinesh Baria from Baria falia in Panchmahaals district contributed his land for building a school. When he applied for the post of co-ordinator (mid-day meal) his applications was rejected under the pretext that he was not "old enough" and rudely asked to get out of the office. Kokilaben from Ruparel said there was no supplementary nutrition (mamara) for the past six months. For three months the helper fed from her own income. In response to this Mr. Lalubhai Desai said "I spoke to the person who runs this factory in Mehsana. He told me that the state of Gujarat has not paid him for six months due to which he. had stopped the supply. But rot runs even deeper. Mr. Amitabh Mukhopadhyay made a startling revelation that no audit report has been tabled for discussion in the Vidhan Sabha in Gujarat from the past ten years. The Collector, Mr. Solanki, assured that action would be taken to ensure food security in the region. He urged the tribal people to come forward and submit their grievances along with proof following which the shutters of unfair shops will be pulled down.

The DSO (Panchmahaals) stated that they will follow up the cases and respond to them in writing as soon as possible. Mr.Samir Garg shared his experiences of Right to Food Campaign in Chattisgarh where in strong grassr090ts movement along with state level advocacy led to cancellation of 4,000 licensees of Fair Price Shops across six tribal districts.

Other Highlights ø Panchmahaals and Dahod districts (combined) have been identified as one of the too most impoverished districts of India by the government. Ø Despite the fact that Gujarat counts as one of the most prosperous states in the country today, 20.4% population gets less than the required 1890 calories which is higher than the entire nation's 13.4% as per a research carried out by the M.S. Swaminathan Research Foundation (Chennai). Ø The Supreme Court has passed II interim orders pertaining to enhancement and strengthening of the provisions and monitoring mechanisms for various government schemes affecting to food security particularly the marginalized groups. This is in light of me PIL for Right to Food, filed by PUCL (People's Union for Civil Liberties), Rajasthan. Ø For more details refer to www.righttofoodindia.org Ø As per a recent survey in 1,435 families in Panchmahaals and Dahod, 80% families face food insecurity for over six months. Only 10% families have proper meals throughout the year for all members. Panchmahaals (undivided) is the most food insecure district in Gujarat. Some other events during this two-day convention: Ø An audio cassette release of "Andhare Atwaiye" (Caught in darkness) was held on Monday. The II folk songs in the cassette have been written and sung by the tribal artists of Devgadh Mahila Sangathan. The songs mainly deal with issues related to food insecurity like irregularities in mid-day meal scheme. Antoyoday Yojana, Annapurna Yojana and Targeted Public Distribution Scheme. Ø A mashaal rally wherein over 600 adivasi women and men participated was carried out in Devgadh Baria town in the evening. As part of Anna Suraksha Adhikar Abhiyan conventions would be held in Rajkot, Jamnagar and Bhavnagar in the next three months. -ANANDI (Area Networking and Development Initiatives) For more details please contact Neeta Hardikar - (02678) 220226, 22 I 097, Email: <neetahardikar@redijfinail.com>

Economical Tools Available for Health Work

Rs. V.SS Rest of Indv. Inst. Asia world Annual 100 200 10 15 Life 1000 2000 100 200 The Medico Friend Circle bulletin is the official publication of the MFC. Both the organisation and the Bulletin are funded solely through membership/subscription fees and individual donations. Cheque/money orders/DDs payable at Pune, to be sent infavour of Medico Friend Circle, addressed to Manisha Gupte, II Archana Apartments, /63 Solapur Road, Hadapsar, Pune 4/1028. (Please add Rs. /5/- for outstation cheque). Email: <masum@vsnl.com>

For tile village health workers

··

· Thermometers

··

Teaching stethoscopes Breath counter Pictorial formulary

Growth monitoring booklets

For communities

· A paper strip test for detecting contaminated

·· · ··

drinking water and disinfection system

· Mosquito repellent oil Safe delivery kit Amylase rich flour

ORS packets

For rural laboratories and mobile clinics Anemia detection kits Electrophoresis kit for sickle cell anemia · Tests for urinary tract infection Concentration test for detection of TB bacteria Low cost carbon dioxide incubator for cultivation of TB bacteria · Cleaning system for glassware · Vaginal infection diagnostic kit

For the clinic and pharmacy

· Portable Stadiometer

·

Subscription Rates

· Tablet breaking device Paracetamol in gel form

Come and learn more about these tools and kits at

MFC Convener Ritu Priya, 1312, Poorvanchal, JNU Campus. New Delhi -110067. Email: <ritupriya@vsnl.com> MFC website:<http://www.mfcindia.org> a workshop, on March 25-26, 2005 at Jeevan Darshan, Fatehgunj, Baroda. For details contact: Jan Swasthya Sahyog, /-4, Parijat Colony, Nehru Nagar. Bilaspur495001, Chhattisgarh. Email: <jss . f5aniyari@rediffmail.com> Phone and fax: 07752-27096, or LOCOST at 0265-283009. Email:

<locost@Satyam.net.in>

Contents CHW: Survivals and Revivals National Rural Health Mission: Comments Public Hearings for the Right to Health: An Analysis of Different Approaches Women's Narratives from Kashmir-I

-Shyam Ashtekar JSA NCC

I 8

-Abhijit Das -Zamrooda Khandey

13 17 18

Adivasis Speak out for Implementation of Supreme Court Orders -ANAND!

Editorial Committee: Anant Bhan, Neha Madhiwalla, Dhruv Mankad, Amita Pitre, C. Sathyamala. Veena

Shalrugna. Chinu Srinivasan. Editorial Office: c/o, LOCOST, 1st Floor. Premananda Sahitya Bhavan. Dandia Bazar. Vadodara 390001 Email: chinumfc@icenet.net. Ph:02652340223/2333438.Edited&Publishedby:S.Srinivasan for Medico Friend Circle. II Archana Apartments. 163 Solapur Road. Hadapsar. Pune 411 028.

Views and opinions expressed in the bulletin are those of the authors and not necessarily of the MFC. Manuscripts may be sent by email or by post to the Editor at the Editorial Office address.


