---
title: "Principles of A National Health Policy"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Principles of A National Health Policy from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC176.pdf](http://www.mfcindia.org/mfcpdfs/MFC176.pdf)*

Title: Principles of A National Health Policy

Authors: Jana Smarjit

176 medico friend circle (Background papers for annual meet, Jan 23, 24, 25, 1991) CRITICAL REFLECTIONS ON THE STRATEGY OF HEALTH FOR ALL BY 2000 A.D. (Dr. Anant R. S. Phadke) It is well over a decade after the famous AlmaAta conference in 1978 in which Governments from different countries all over the world decided upon the goal of "Health for all by 2000 A. D." The approach to attain this goal 'Was to be primary Healthcare. Since the Alma-Ata declaration, the slogan of "Health for all by 2000 A. D." and the "Primary Health Care approach" have almost become catchwords in public health circles. Every body seems to have accepted and glorified these two things uncritically. In MFC also, Primary Healthcare approach has been the accepted approach. After the experience of more than a decade, it is high time that these concepts, strategies, programmes as evaluated to identify the genuine progress, the blindspots, deficiencies and distortions in the primary health care approach and the programme of "Health for all by 2000 A. D." In this note, an attempt would be made to do the same in the spirit of taking stock of the situation. The aim here is not to establish the viewpoint expressed with empirical support but to put forward a framework, a view point, for discussion. Perhaps this viewpoint summarizes the consensus that exists in M.F.C. on this issue.

1) Health for all and Primary Health Care: Let us first be clear as to what is the 'Health for All by 2000 'A. D.' strategy. The Alma Ata conference declared "Governments have a responsibility for the health of their people which can be fulfilled only by the provision of adequate health and social measures. A main social target of governments, international organisation and the whole world community in the coming decades should be the attainment by all peoples of the world by the year 2000 of a level health that will permit them to lead a socially and economically productive life. Primary health care is the key to attaining this target as part of development in the spirit of social justice. Primary health care is essential health care based on practical, scientifically sound and socially acceptable methods and technology made universally accessible to individuals and families in the community through their full participation and at a cost that the community and country can afford to maintain that every stage of their development in the spirit of self –reliance and self - determination. It forms an

integral part both of the country's health system, of which it is the central function and main focus, and of the overall social and economic development of the community. It is the first level of contact of individuals, the family and community with the national health system bringing health care as close as possible to where people live and work, and constitutes the first element of a continuing health care process. Primary Health Care includes at least: education concerning prevailing health problems and the methods preventing and controlling them, promotion of food supply and proper nutrition; an adequate supply of safe water and basic sanitation; maternal and child health care, including family planning; immunization against the major infectious diseases; prevention and control of locally endemic diseases; appropriate treatment of common diseases and injuries; and provision of essential drugs."

2) Positive Features: These declarations are impressive enough; and there is some genuine conceptual advantage in these declarations compared to the dominant pattern of privatized, commercial health-care that existed then and even continues today. The dominant pattern of health - care was described by the Alma Ata Conference as follows: "Health resources are allocated mainly to sophisticated medical institutions in urban areas. Quite apart from the dubious social premise on which this is based, the concentration of complex and costly technology on limited segments of the population does not even have the advantage of improving health. Indeed, the improvement of health is being equated with the provision of medical care dispensed by growing numbers of specialists, using narrow medical technologies for the benefit of the privileged few. People have become cases without personalities and contact has been lost between those providing medical care and those receiving it." The 'Primary Health Care Approach' is radically better than this dominant approach. There were certain features in the PHC - approach which were new and positive

The commitment to 'socially acceptable methods and technology,' to community participation, to the principle of 'cost community and country can afford', to self-reliance and selfdetermination these features imply a health care delivery model quite different from the one dominated by blind acceptance of: 'high-technology' from Western countries. Secondly, the approach of Community Diagnosis is of course a rational one. The Bhore - Committee had recommended the formation of Citizens' Committees of 5-7 voluntary workers in each village. Each member was to be trained in health - activity - initiation of voluntary preventive activities, dispensing of simple drugs, collection of vital statistics ... But such committees became defunct. Later on, the evolution of voluntary Health Workers in innovative health projects in the NGO-sector and the adoption of this concept by Alma-Ata Declaration is one of the most important positive steps in the organization of health-care. Though the experience during last decade has removed the romanticism about CHW's role, on the one had, and on the other hand, the vulgarized from in which the scheme was implemented in the Government sector has much maligned it, there is a golden mean that can and has been achieved in different places. A much better trained, better equipped, better supported CHW has an important role to play in providing good quality medical care and health-education and in fostering preventive, promotive activities in rural areas. But perhaps the powers that be are not interested in its full-potential and look upon the CHW-Scheme as a cheap device of providing a token of health-care to rural areas. Induction of provision of nutrition, water, sanitation in Primary Health Care is also a step ahead of a purely medical model of provision of medical services. Unfortunately, this demedicalitation inclusion of the non-medical aspects is the most neglected and failed part of the PHC as has been practised in India.

3) Blindspots, deficiencies, distortions: While analysing the negative side of the HFA-strategy and the PHC-approach, it would be better to distinguish between the blindspots, deficiencies and distractions in this strategy. By blindspots I mean those deficiencies which occur because of the very nature of the limited conceptual framework of the strategy, which prevents it from looking at important problems. For example, as would be argued later, there is no

National Programme for women's Health in spite of. This is because women are looked upon only as mothers and there is thus only Maternal and Child Health Program. By deficiency is meant the inadequate performance of an essentially correct programme, for example, inadequate coverage of the population in need of a healthcare service. When the deficiency is so gross or the quality of implementation is so bad that it hardly reflects the original aim of the programme, we would call it as distortion. Foe example, in the Government set up, the extremely low quality of training to Community Health Workers and the extreme neglect of this programme of VHW has resulted in a picture that hardly reflects the original aim and potential of this programme. By way of example, let us take a few important aspects of the HFA strategy to analyse the blindspots, deficiencies and distortions in them. Let us first critically look at the goal of HFA itself.

3.1 The Goal of HFA by 2000 A. D. 3.10 Who has defined health as "not merely the absence of disease", but as "a state of complete physical, mental and social well-being", and has regarded "The enjoyment of the highest attainable standard of health", as "one of the fundamental rights of every human-being". But the Alma-Ata Declaration aims at: "a level of Health that will permit people to lead socially and economically productive life." This subtle shift to a limited goal should be noted. There is nothing wrong in setting up limited goals in a time-bound programme, so long as we are aware that the goal is in fact a limited one. It is important to point out the limitations when an aura has been created around HFA. It is true that even this limited goal was a difficult one to achieve when it was announced in 1978. Moreover, now it looks impossible to achieve, given the performance so far. Yet the conceptual shift as to be noted. Secondly, in today's market economy, what is 'productive' is decided by its value in the market place. That is why the work and hence the health of house-wives, marginalised workers, old people have been neglected. The nature of the limited goal for the time-bound programme of HFA-200 A D has thus been decided by the blinds pot of the socio-economic system and not directly by the health needs of the people. 3.11: As for deficiencies, in the fulfillment of the goal of HFA-2oo AD, the accompanying table gives the grossly deficient progress made in India, in the first ten years after Alma-Ata. With this pace of progress, even the limited targets of HFA-200 AD seem impossible to achieve in time. It will be seen from the table, that the People's Republic of China and Shri Lanka have already fulfilled some of these targets.

Table NO.2 Progress towards some HFA-Targets"

Sr. No.

1) 2) 3) 4)

5) 6) 7)

Indicator

(1 ) Infant Mortality rate Crude Death-rate Crude Birthrate Material Mortality rate per lac births Life expectancy at birth for males Life expectancy at birth for females Babies with low birthweight

(1) Deliveries by trained birth 8) attendant

Status in 1978

1987

(2) 125 11 35

(3) 99 11 32

500

China by 1987 (4) 32 7 21

Progress in Sri-Lanka by 1987 (5) 33 6 23

44

90

(1980)

(1980)

Progress till Progress in

N.A.

Target for 2000 A.D. (6) blow 60 9 below 21 below 200

52.6

58

68

64

64

51.6

58

71

73

64

30%

6%

28%

10%

(2)

30% (1985) (3)

(4)

(5)

(6)

30%

30%

N.A

87%

100%

-

-

100% 85% 85%

(1985) 9) Immunization: a) T.T. for pregnant women. b) DPT for children c) Polio

20% 25% 5%

3.1.3: The WHO has evolved a set of indicators for monitoring progress towards HFA-by the year 2000AD. (3) The Government of India, has however, deleted the non-medical indicators from its reports of progress towards HFA by 2000 AD. In their view, HFA is thus a purely medical enterprise. The nonmedical determinants of health such as extent of provision of safe drinking water, adequate sanitary facilities, integrated development etc. have been mentioned in the “National Health Policy” declaration of the Government of India. But since they are not included in the monitoring of progress towards HFA, these declarations can not be taken as genuine. This absence of integration of medical and non-medical aspects of health amounts to a distortion of the declared strategy of HFA by 2000AD. The actual performance on the economic social front is abysmal. In the context of rising prices, unemployment, pollution, the talk of HFA by 2000 A. D. has become an empty rhetoric. 3.1.4: Even amongst the health-indicators, one sensitive, important indicator has been deleted in the WHO has specified that by the year 2000 A. D.

30% 64% 58%

"Atleast 90 % of children have a weight for age that corresponds to the reference-values." (4) This is a difficult task to be achieved compared to the task of say reduction in infant mortality rate. It is precisely this indicator which is missing in the Government of India's evaluation-parameters. 3.1.5: Even if we take only health-interventions, they are grossly inadequate; an important reason being lack of adequate funds. The WHO has specified that 5 % of the Gross National Product be spent on Health-care. But in India, however, it had increased to only 1.17 % by 1988 (5). Moreover, the share of Health and Family- Welfare (Read population-control) as percentage of total plan outlay has declined from 3.3 % in the first plan to 2.9 % in the sixth plan (6).

3. 2: Primary Health Care approach According to the Alma Ata declaration, the method of attaining the goal of HFA is that of tackling the health-problems with the Primary Health-care approach. As remarked earlier, this approach is much better than the dominant

privatized, commercialized approach. But the PHCapproach has its blindspots, limitations.

3.2.1: Neglect of Curative Services: The minimum list of activities quoted above which must be included in Primary Health Care are heavily biased in favour of preventive and promotive measures. . Appropriate treatment of common diseases and injuries; and provision of essential drugs comes at the end of this list. In practice also, medical treatment is the last priority of the governmental health-system. It is true that diseases can be eradicated primarily through preventive and promotive measures. But that does not mean that curative/ symptomatic care be neglected. The need for such care is acute and many times crucial in alleviating intense suffering or preventing death or presenting a disability. But it is precisely this need that has been by the large neglected by the Government set up. People therefore, suffer or die silently or go to private practitioners, many of whom in rural area are simply quacks. There is tremendous exploitation of the rural people by these third rate practitioners. Since the Government service is grossly deficient in curative care, in the eyes of the people the credibility and popularity of this service is also low. The emphasis on preventive, promotive measures is a convenient alibi to neglect curative healthservices and thereby to reduce Government’s expenditure on Health Services. What is needed is adequate emphasis on both prevention and cure. One need not be emphasized at the expense of the other.

3.2.2.: Narrow Coverage: In terms of coverage of population for health care, the PHC- approach is a step backward compared to the recommendation of the Bhore Committee in 1946. The Bhore Committee's recommendations were to be the basis of the restructuring of health - services after Independence. The Bhore-Committee had recommended “Comprehensive Health - Service" having the following criteria: (a) Provide adequate preventive, curative and promotive health service. (b) Be as close to the beneficiaries as possible. (c) Has the widest cooperation between the people, the service and the profession. (D) is available to all irrespective of their ability to pay for it. (e) look after more specifically the vulnerable and weaker section of the community (f) create and maintain a healthy environment both in homes as well as working places.... (7)

"To the individual patient, comprehensive health care means that he can get whatever kind of care is required by the diseases to which he may be subject, Primary Health Care is however, selective. It includes: 'maternal & child health,’ ‘locally endemic diseases, common diseases & injuries,’ as problems to be tackled. (Incidentally there is no mention of control of communicable disease - probably a slip of pen?) Secondly, PHC does not include occupational health - hazards, which are now becoming increasingly common even in rural areas. Material and child Health is hailed as an important example of community - approach to health problems. What is forgotten is that the real reason for choosing this programme is the interest the state has, in having a healthy adult population which can contribute to "National Wealth". Children are the future 'pillars of the nation' and hence the interest of the State in their health and thereby in the health of the mothers. Women are thus looked upon only as mothers and their other health-problems are being neglected. The 'scientific rationale advanced in support of the MCH programme is the biological vulnerability of mothers and children, the universal nature of health problems associated with motherhood and childhood, and its preventability. There is of course some truth in this argument. But this rationale is only a half truth. Old people are also biologically vulnerable. But there is no National Geriatric Health Programme. If prevalence is a criterion, gynaecological problems in women would require a National Programme. A community based survey found that as much as 92 % of women in a rural area had some gynaecological disease or the other, a very high proportion of these were due to vaginitis, cervicitis. (3) This finding is probably representative. In spite of such a high degree of prevalence of gynaecological disorders, there is no national health programme to tackle them. If neglected, many of these gynaecological problems turn into serious crippling illness, or even death. Chronic cervicitis which remains untreated in Indian women for years can predispose women to cancer of the cervix of the uterus, a killer disease. Many of these diseases are easily preventable and easily treatable in the early stages. It is therefore clear that epidemiological characteristics are not enough to make any health problem worthy of a national programme in PHC. The interests of the ruling elite decide the final selection. Many people who glorify the PHCapproach' gloss over this fact of the step-down from Comprehensive Health services (recommended by the Shore-Committee) to Primary Health Care and are unaware of the politics of selection of health problems in PHC.

Why this step-down? This step-down was necessitated by the fact that most of the recommendations of the Shore-committee remained largely unfulfilled; and with increasing financial constraints due to economic crisis, it was pretty clear that there was no chance of making 'Comprehensive Health Care' a reality. Table No. 1 gives the performance of the health - care system in India, as compared to the recommendations by the Shore - Committee.

THE COMPARISON OF HEALTH MAN - POWER AND INFRASTRUCTURE IN 1981 WITH BHORE - COMMITTEE RECOMMENDATIONS 1971

Population

1981 685 Million....(2)

1971 Recommended...(1) Projection as required by Bhore – committees

Primary Health Centres

1 : 20,000

Doctors

Table No.2

Actuals

34,250

5,740 (2)

1 : 2,000

3,42,500

Nurses Health Visitors

1 : 300 1 : 5,000

22,83,333

Midwives

1/100 births:

Dentists

1 : 4,000

Shortfalls

Shortfall in

…

percentage

28,510

83 %

2,68,712 (3)

73,788

21.5 %

1,50,399 (2)

21,32,934

93 %

1,37,000

@ 19,033 (2)

1,17,967

86 %

2,31,000 1,70,500

@ 23,200 (2) 8,648

2,08,330 1,61,852

90 % 94 %

@ Trained upto 1981 SOURCE: (1) Shore Committee Recommendation report (2) Health Atlas of India, 1986 (3) Health Information of India, 1987

This abysmal performance is an a way the source of the new selective approach. Some kind of cheaper health-service had to be offered to the rural areas to avoid discontent. PHC-approach was seen to be such a solution.

3.2.3: Poor-training: Poor quality of training of VHWs means a marginal ability to treat even minor aliments, to establish credibility to do health-education.

There is also no continuing education of VHW. Perhaps the doctors do not believe in the philosophy of the important role of VHWs; nor are they themselves trained to appropriately train this cadre of health workers. The doctors themselves are not appropriately trained to work in a rural, peripheral set up, and to act as team - leaders.

3.2.4: Assumed resource-constraint The powers that be have in fact reduced the already subminimal percentage of plan-expenditure on health (family planning excluded.) The medical experts have meekly adjusted themselves to this neglect of the health sector and under reassumed resource-constraint, devised suboptimal strategies. Thus in TB-control programme, sputum negative, xray positive patients are given an inferior drugregiment. Though they are epidemiologically not a threat to the community that does not mean that their disease or suffering is less dangerous or less troublesome to them than those who are sputum positive. (10) The step-motherly treatment to these patients is based on the assumption that we do not have enough resources to treat all TB-patients equally well. The same assumption is responsible for inadequate dose of Iron to pregnant women in the MCH-programme n. (11) or lack of calciumsupplementation to pregnant, lactating women from the poorer community.

3.2.5: Domination of Family Planning Programme This point needs some elaboration: 3.2.5.1: To begin with, Family Planning is a misnomer, a euphemism for population control since it is aimed at only planning for a small family. Other aspects of family-education, rearing-up, employment etc. are not planned. Similarly, Family Welfare is also a misnomer. The Maternal and Child Health are only a miniscule part of the F. W. Programme. In the 5th plan, expenditure on MCH services constituted only 1.4 % of the Family Welfare expenditure, and was under spent by 15 %. In the 6th plan, this expenditure was raised to 13 % (that too because of the incorporation of the Expanded programme of Immunization) but it was again grossly under spent by 27 % (12) Thus F. W. Programme is in fact, Primarily the F. P. Programme. 3.2.5.2: When other programmes are starved of financial support, funds for family planning programmes increased astronomically. For example, in Gujarat, F. P. expenses on per eligible couple increased from Rs. 0.68 during 1966-69 to Rs. 309.93 during 1985-90; whereas the per capita expenses on all other health-programmes together increased from Rs. 3.51 to Rs. 39.91 during the same period. (13) Allocation for Health Plan excluding F. P. declined during the same period from 4.1 % to 3.5 % of the total plan outlay. (14) 3.2.5.3 : Even within the F. P. Programme, those undergoing tubectomy are increasing as compared to those undergoing vasectomy, even though the latter

is of course, for convenient, simple, safer, cheaper method. The medical establishment thus has meekly joined hands with the patriarchal system which is the root cause of the preference for tubectomies. What is indeed shocking is that more than 50 % of the sterilization operations are done on individuals who are beyond thirty years of age or have four or more living children. (15) Thus more than half the operations are done after the couples have completed their family; and hence are by and large wasteful. These trends of excessive emphasis on population control, on tubectomies, and late-sterilizations are not simple deficiencies, but constitute distortions of the PHCapproach.

3.2.6: Lack of community participation By and large, the community is even unaware of the various health-programmes. The Village Health Workers are recommended by the Gram Panchayat, but alter that the Gram-Panchayat does not have any control over the VHW, nor over any other health functionary. The VHW or the health-bureaucracy is not able to involve the community in the planning and implementation of Primary Health Care. Nor are the social political leaders interested in Primary Health Care; they are by and large unaware of its existence. Much innovative thinking and work is needed to foster community participation. The experiments in the voluntary sector need to be evaluated and integrated in the national planning.

3.27 Neglect of non-allopathic systems In the Alma-Ata Declaration there is no mention of the role of non-allopathic systems of medicine. Role of indigenous systems of medicine is important in the context of "Self- reliance" as one of the features of the PHC-approach. In the National Health Policy statement of the Govt. of India, this aspect has been covered. But it has neither been included in monitoring of the progress nor in the section on medical research. Grass root level operational research, as well as laboratory research, is crucial in defining and fostering the role of indigenous systems of medicine. In absence of such research mere inclusion of a few non-allopathic drugs in the VHW-kit hardly does justice to the subject.

3.28: Selective Primary Health Care and Vertical Programmes: Since the Government is not willing to spend enough money and other resources on even the limited programme of Primary Health Care, there is now a talk about selective Primary Health Care. Human beings and their health-Problems in totality are going more and more into the background, and a limited number of targets are being chased with the help of technocratic managerial interventions; whatever may be the scientific jargon that may be used to camouflage the real motive. This new upcoming strategy of selective

PHC with the help of vertical programmes is at variance with the basic tenets of Primary Health Care - essential health care, community participation, selfreliance, demedicalitation of health. That is why it has been very severely criticized by those who steadfastedly adhere to the basic philosophy of PHC, for example, ACHAN. (16)

3.29 Privatization In recent years, in different places, aspects of public health services are being handed over to private sector and there is a trend towards "fee for service" approach in the public health sector. This trend vitiates the basic tenets of Primary Health Care Approach, which includes universal accessibility of Primary Health Care services. "Fee for service" means a denial of health services for those who can not afford these. It is one thing to collect token contribution in a collective health insurance scheme, as a measure to enhance people's participation and control. But the upcoming trend is not aimed at this, but is aimed at essentially reducing the Governmental

expenditure on health care. Like selective PHC, this trend also constitutes a distortion of the PHC approach. We would not go into all the problems of the PHC-approach, as it is practised in India in the government sector. (The NGO sector also shares some of these problems.) It would be clear from the discussion so far that the PHC-approach in theory and in practice is far too short of what neded is and what is possible. It is, therefore, necessary to conceptually broaden and deepen the scope of PHC, and to take up steps to avoid the mistakes in planning and implementation for a strategy for health For All. The role of the NGO sector could be important to show how this can be done. (This is a revised version of my earlier note: "Critical reflections on the Strategy of Health For All by 2000 A.D." submitted for the Seminar on "Science and Technology Policy in India" at Kolhapur, Maharashtra in February, 1990)

REFERENCES 1) Alma Ata, 1978, WHO, UNICEF. 2) Health and Related Statistics, FRCH News Letter (Special number), Oct-Dec, 1989, table IX, National Health Policy, Lok-Sabha Secretariat, 1985, pp-49-50. 3) Development of Indicators for Monitoring Progress Towards HFA by the year 2000 AD' WHO, 1981, pp/3940. 4) "Development of Indicators op. cit. P/40. 5) Health and Related Statistics, Op. cit. table-Via. 6) Health Statistics of India, 1983)-71. 7) Textbook of Preventive and Social Medicine, Parks, 1970, p612. 8) High Prevalence of Gynaecological Diseases in Rural Indian Women, R. A. Bang, A. T. Bang et al, Lancet, 14.1.1989. 9) Community Health in India, Health Action, July'89, p-6. 10) Confusion-A way to knowledge, U. N. Jajoo, MFC-Bulletin, No. 125, Feb. 1987. 11) "Insufficient dose of iron in MCH programme: Anant R. S. MFC Bulletin, No. 151, May, 1989. 12) Financing Family Planning, Ravi Duggal, Background paper for MFC-Annual Meet-1987, P-7. 13) Primary Health care and F. P. Planning Programme in Rural Gujrath, some issues; Sudarshan Ayengar & Ashok Bhargav, Background paper for MFC-Annual Meet, 1987, table-10. 14) Ayengar - Bhargav, op cit, table - 3. 15) Ayengar - Bhargav, op cit tables - 8 & 9. 16) LINK, Vol - 7, NO.2 Aug Sep, 1988.

The Targets: ‘HEALTH FOR ALL’ Primary Health Care is to be the approach and Health For All is to be the strategy of achieving quantified targets by 200 AD. An impression is created that the health-ministry through this very well formulated programme is going to improve the health of the people. In reality, health-status can not be 'delivered: nor can health-services be the primary determinant of improving health-status of the people. In the absence of proper socioeconomic development, today health-services cannot play even the catalytic, supplementary role, they ought to play. There is no wonder, therefore, that in the period of burgeoning price-rise, unemployment, drought, socio-political instability HFA-strategy is a mere cry in the wilderness. It is bound to fail even in the limited goals it has set for itself. A word about the limited goal, HFA aims at: 1a level of Health that will permit people to lead a socially and economically productive life.' In today's market economy, what is productive is decided by its value in the market-place. That's why the work and hence the health of the old people, house-wives, marginalized workers have been neglected. Secondly, though the Alma Ata Declaration gives a set of twelve health and non-health indicators to evaluate the progress of HFAstrategy, the Government of India has deleted the non-health-indicators from its reports of progress towards HFA by 200 A.D. Health has become a purely medicalized enterprise. Thirdly, though Alma Ata Declaration specified that 5 % of the GNP (Gross National Product) be spent on health-care, in India, however, it had increased to only 1.17 % by 1988. (5) Moreover, the share of Health and Family-Welfare (read population-control) as percentage of total plan outlay has declined from 3.3 % in the First Plan to 2.9 % in the Sixth plan. (6)

Fourthly, even amongst the health indicators, one important, sensitive indicator has been deleted in the Indian evaluation parameters. The Alma Ata Declaration specifies that by the year 200 A. D. "Atleast 90 % of children have a weight for age that corresponds to the referencevalues... This is a difficult task to be achieved, compared to the task of achieving reduction in maternal mortality or immunizing all children. It is precisely this' indicator which is missing in the Govt. of India's evaluation parameters. The progress towards achieving even these limited goals is extremely tardy. This is reflected in table NO.2 With only ten years to go it is quite clear that except for a couple of indicators, even the limited targets of Health for All by 2000 A.D. would not be achieved in India. The table shows that China and Sri Lanka have already surpassed some of these targets. Thus these targets are definitely achievable in backward countries. India has however failed miserably. To conclude, health-status in primarily a function of socioeconomic development, healthinterventions can considerably accelerate this process, but can not substitute it. Health for All is, therefore, bound to fail in today's crisis-ridden socio-economic set up. Primary Health Care as an approach to health - problems has some positive features, but its blindspots have to be brought into focus, and the retreat form the aim of providing Comprehensive Health Care has to be criticized. By overcoming there blindspots and by consistently applying the principle of community diagnosis and Primary Health-Care, a programme of "HealthCare For All" can be successfully launched. An honest and consistently scientific approach would, however, require a different alignment of socio-political forces.

References. . . (1) Alma Ata, 1978 WHO UNICEF; (2) Textbook of Preventive and Social Medicine, Parks; (3) High Prevalence of Gynaecological Diseases in Rural Indian Women, R.A Bang, AT Bang et al, Lancet, 14.01.1989; (4) Community Health in India, Health Action, July 1989, p,6; (5) Health and Related Statistics, (Special Number), FRCH News letter, Oct-Dec. 1989, Table VI-a; (6) Health Statistics of India, 1983, p. 71; (7) Health Related Statistics, Op cit. Table IX, National Health Policy, Lok Sabha Secretariat, 1985 pp 49-50.

Principles of a National Health Policy Smarjit Jana MFC, West Bengal 1. Definition of Health Health is not merely absence of disease but is a state of complete physical, mental and social well being. This definition of WHO has gained ground in both official and progressive circles and has served the purpose of obscuring the popular concept in such a way as to undermine the importance of medical care responsibilities of the state. Further, though this abstract definition is a step forward in the way of intellectual sophistication, it is beyond peoples' comprehension: as well being is not measurable. It is therefore, suggested that we should define health merely as absence of diseases, since it is comprehensible, clear and points towards a concrete objective; presence or absence of diseases is measurable.

2. Medical Health Care Vs Non-medical Health Care Medical Health Care (MHC) involves measures which require medical intervention ego treatment of illness, immunization, medical rehabilitation etc. Non Medical Health care (NMHC) comprises those basic non-medical needs which also act as determinant elements of health ego food, shelter, drinking water, sanitation, clean environment, education, employment etc. The present (National Health Policy) declares that our failure on the health front is due to injudicious grafting of a western model of health care system which is curativeoriented, hospital centred and urban biased, and thus only provided benefits to the upper crusts of society. The health care service, therefore, should be drastically revised to put emphasis on universal provision of primary health care and public health services, a time-bound programme be adopted to ensure adequate nutrition for all, potable water care supply and basic sanitation facilities, protection of environment etc. This is also the policy direction so long advocated by the WHO. Since then it has been adopted almost as a truism among those concerned that priority should be given to NMHC and emphasis on MHC should be reduced. The above principle, it appears, has developed out of ignorance or political motivation. There is no denying the fact that NMHC lays and maintains the proper foundation of health but there can not be any question of differential priority between curative

and preventive care; nor there is any conflict between the application of these two. Curative care is meant for ill persons whereas the recipients of preventive care are those who are not afflicted with illness. In other words, for the ailing person the curative care is a must, no amount of preventive care can relieve or cure him. Hence, it ought to be clear that we need both preventive and curative care and say, both NMHC and MHC, of necessary standard and adequate measure, and there cannot be any question of emphasising one at the expense of the other.

3. Programmatic exercise implementation agencies

and

From the foregoing, it is obvious that in order to protect and maintain the health of an individual as well as the community, we should have medical care for all, food for all, shelter for all, i.e. water, sanitation, education, employment etc for all. Can the health administration be entrusted to achieve all these objectives? The answer obviously is, no. The job of health administration of the country should, therefore, be to prepare a scientific, people -oriented MHC policy and its implementation; NMHC should be left to other appropriate agencies to deal with.

4. Tasks of the policy on MHC It should deal with all aspects of medical care in public and private sectors; production, marketing and consumption of drugs and equipment, medical and paramedical personnel and their education and training, immunization and control of communicable and other controllable diseases, health education, health legislation, health research and other related matters.

5. Basics of the policy on medical care The objectives should be universal coverage by an acceptable and feasible standard of curative care and its equitable distribution. At present, the poor people get the inferior kind of Free State medicare and buy, under compulsion, a cheap low quality market medicare; the affluent get the superior kind of free state medicare and costly high quality market medicare. Employed people in the organised sector additionally get another bounty of institutional medicare reserved for themselves. In order to proceed towards and achieve equity, it is imperative that the entire state free medicare

service be exclusively reserved for the poor people so that the affluent people may be confined to the market sector and the employed people to their own reserved institutional medicare. This should be the point of departure.

6. Task of the people's health movement From the foregoing, it is quite apparent that the issues pertaining to the NMHC belong to the political forces to deal with. Activists and organisations of people's health movement should therefore, concentrate on the issues pertaining to MHC.

Guidelines for state health care delivery system Suggestions from field experience of voluntary sector Dr. U. N. JAJOO Incharge, Health Insurance M. G. I. M. S., Sewagram.442 102 Dear Medico Friends, While preparing the note entitled 'Guidelines for State Health Care Delivery System suggestions from field experiences of voluntary sector' the following assumptions were in my mind and those need to be considered.

.

The distance of primary health care hospital should be such where emergencies requiring immediate attention (e. g. neuroparalytic snake bite, obstructed or complicated labour, organ-phosphorus poisoning, convulsing child, acute left ventricle failure etc.) can be transported within 1/2 hour (maximum).

.

. To provide at least primary health care services

The distance of referral hospital should be such where a patient needing caesarian section can be transported from the cottage hospital within 1/2 hour (Maximum).

.

To reduce unpredictable maternal & child mortality (disorders like uterine inertia with foetal distress, retained placenta with post-partum haemorrhage), institutional delivery should be accessible to every mother.

accessible to poor is an obligation that the state should fulfil. The tendency of the state to shrug off this responsibility by encouraging privatisation must be opposed by pro-people voluntary sector. The pro-people voluntary sector should offer its constructive suggestions based on their field observations, to the state. While doing so, voluntary sector must put itself in the position of the 'Last man' and question it selves to spell out optimum minimum that he/she would like to have.

.

Eg. Will I like to get my delivery conducted by a trained Dai/ ANM in the village home?

. Will I like my child to be treated for pneumonia

at the

mercy of a village health worker?

.

In a situation where 2I3rd microscopically detectable cases of pulmonary tuberculosis are missed and are sent off with a prescription of cough mixtures, will I insist for radiological support to be available at reachable primary care hospital?

.

Will I like a well-equipped referral hospital to stand by for catering to common emergencies (e.g. obstructed labour, acute appendicitis, intestinal obstruction, bladder neck obstruction due to benign prostatic hypertrophy, head injury needing burr hole, fracture neck femur, mastoiditis, lens induced glaucoma, Gullianbarre with respiratory paralysis etc.)? .In matters of justice to the poor (providing minimum emergency care) the cost factor should not deter us. The onus of redistribution of misallocated funds rests with the state. The field observations that heavily weighed in my mind while writing this note are

.

.The cottage hospital (PHC) should be so equipped

that it caters to all emergencies which either can not be transported due to gravity of illness or do not offer enough time to transport patient to referral hospital. The quality of skilled manpower availability at the cottage hospital should be such that the above said emergencies can be dealt with confidence. Barring sophisticated and superspeciality services, all other demands (e.g. Intensive care unit, neonatology, general medicine, general surgery, ophthalmology, ENT, radiology, including ultrasonography, clinical pathology, microbiology and clinical biochemistry) be met by the referral hospital. The cottage hospital (PHC) which provides curative services should extend preventive & promotive outreach services. The responsibility of assuring availability of skilled man power at peripheral hospital must rest with the state. For primary health care services, a common man should not be required to pay from his pocket at the time of need. By controlling finances and evaluating performance, the people can be empowered to command an efficient and qualitative health care delivery system. Since all demands of people (tonics/pricks) may not be professionally justified,

. .

. . . .

there is a limit to which the control can be decentralised. An obligatory golden mean will have to be worked out from time to time. The village is not a homogeneous community. There may be strong party affiliations which do reflect in Gram Panchayat. The people's mandate must be sought in forum no less than Gram Sabha. Realising the culture-that majority is silent, decisions in Gram-Sabha should be made by overwhelming majority (say 75%). For a genuine protest to be lodged (e.g. performance evaluation) with higher health system hierarchy, less than majority (say 35%) should suffice. The financial control by the people should be routed through voluntary prepayment (insurance contribution) The private sector should be allowed to compete with the state services. If private sector is ready to provide services through prepayment schemes, the state should offer equal support (financial) to them as is given to state run hospitals.

.

. .

Guidelines 1. Aim: To provide health care services equitably

accessible to poor. 2. Structure: For each village/ward of a city - Door-step services through village/community health worker and Dai. For a cluster of Villages/City -Cottage hospital services of primary health care. At district level - Referral hospital preferably attached to the Medical College (If available) The state level- Superspeciality hospital accessible

strictly use referral basis.

3. How- far the cottage hospital should be?

Nearly about 1/2 hour's reachable distance from the village uses fastest available transport (on foot, by bullock cart, by ambulance. The pliable approach roads and quick communication system -----------e) is requisite minimum if ambulance of cottage hospital ---to opposed to cater to emergencies. With ready ambulance service ------- good roads one expects to cater to 25 k. m. radius area around. 4. To How far should the referal hospital be? Preferably within 1/2 an hour's distance by ambulance of cottage hospital. The referral hospital is thus expected to cater to 50 kms. radius area.

5. How much the cottage hospital be equipped?

The minimum that cottage hospital should cater to is 5.1 Curative care 5.1.1 Indoor emergency care for the problems where patient is not transportable to referral hospital e.g. Forceps/Ventouse delivery, breech delivery, PPH, incomplete abortion (OB & Gyn). Resuscitation of newborn, dehydration, lower respiratory tract infections, convulsing child, PEM with complications, Kerosene/Datura poisoning, Diphtheria (Paed), Snake bite, Tetanus, insecticide poisoning, severe hypertension, acute LVF, COAD (General Medicine), Epistaxis, tracheostomy, colic (Surgery)

5.1.2 Curative services for illness which need not come to referral hospital. e.g. Common Ailments attending Gyn/Paed/Medicine O.P.D. (including T.B. & Leprosy), Extraction of loose tooth, Acute otitis media, wax in ear, otomycosis, safe CSOM, Maggots, Minor burns, abscesses, suturing, dressing, splints, Conjunctivitis, Normal delivery 5.2 Preventive and promotive services at door-step by out reach programme organised from cottage hospital 5.3 What should be minimum human power requirement of a cottage hospital? – A postgraduate in paediatrics A postgraduate in OB & GYN A postgraduate in community medicine (A special in service training of all these doctors in elementary anaesthesia, orthopedics, dentistry, ENT, ophthalmology will have to be undertaken to equip them for good medicine efficiently.)

.. . .. . .. ..

Two ANMS for hospital Two ANMS for outreach health services Two social workers for outreach programme. A dresser cum dispenser cum registration clerk. Two attendants for hospital Two attendants for outreach programme A driver for ambulance A dhobi on contract basis for the hospital,

5.4 Learning’s from Sevagram experiment: Expected indoor load - 1 per 11 people Expected average hospital stay - 7.5 days Expected bed requirement - 2 beds per 1000 population with 100% bed occupancy Expected indoor recurring cost (1989 figures) (Salary, drug, food, Maintenance all inclusive) 68 Rs. /day/bed, 510 Rs. /admission, 46 Rs. /per capita/per year Expected out-reach recurring expenditure (1989 figures)\3.5 Rs. /Capital Year for village drug kit and for hiring mobile health team 2 Rs./capita/year for VHW + Dai Remuneration Expected outdoor attendance - 1 per/ person/per year

. .

5.5) Blind lanes What can be the average outdoor expenditure of running O.P.D. Service? What should be the numerical relationship between total O.P.D. attendance/indoor admissions and number of doctors/ paramedical staff needed to run the cottage hospital? The abovementioned information will guide in deciding population that can be catered to and the catchments area for a cottage hospital in more concrete terms. The cost economic feasibility of skilled manpower at the cottage hospital can also then be calculated.

5.6 The strategy for outreach services Annual cluster (pulse) immunization strategy (4 visits/village/year) MCH services (3 visits/village/year) Health education with Slide shows/community meetings Supervisory role to be shouldered by postgraduate doctor in Community Medicine 5.7 How to assure availability of skilled manpower? If a medical college is available nearby, the district referal hospital be obligatorily run by medical college staff. 6. The role of referral hospital: To provide all specialties (Care General surgery, General Medicine, General Paediatrics, Orthopedics, ENT, Ophthalmology, OB & Gyn, Dentistry & Community medicine) strictly on referal basis either from private sector or from cottage hospital. Documentation of local events Appropriate research Teaching students, if the referral is attached to a medical college:

.. .

7. How can privatisation be checked? Free and good quality of accessible health service from government sector can check privatisation. All government employees must join state health insurance scheme and no reimbursement be allowed for hospital care obtained from private sector. 8. How to incorporate people's participation and their control? For state health services to be responsible to people's need, empowering people by promoting their participation and by controlling at least a part of financial resources is obligatory. The decentralisation process must be initiated. In the present situation, the control cannot be totally decentralised as professional wisdom is not necessarily identical to felt demands of the people e.g. the demand of tonic bottles and pricks can not be justified by professional wisdom. There must be scope in the system for professional wisdom to be assertive enough when required. As it is important that health professionals are not required to dance to the tune of people's minds, it is also imperative that people be empowered to the extent that they can command an efficient and quality health care delivery system. The following strategy is suggested - The state should extend health services through health Insurance scheme. The smallest participatory unit would be a village or a ward of a town (depending on population). The population size of the participating community can be around 1000. The area to be catered by district level referal hospital and cottage hospitals would be predefined. The decision to participate in the state health insurance scheme will be taken by Gram Sabha with at least 75% majority that means health insurance scheme would be voluntary. The people's participation in management services would occur by co-opting a representative from each participating unit. The management body so formed with the hospital/state co-ordinators would enjoy maximum autonomy in local planning, budgeting, and implementation. Out of expected recurring expenditure, state

would handover, say, 75% amount in advance with district hospital and 25% balance amount would be distributed to participating unit according to per capita calculations. This earmarked amount will thus be deposited with GramPanchayat/Municipality/Corporation and would go back to hospital in form of health insurance contribution, provided beneficiaries by atleast 75 % majority approve it. In the event of non-participation in state health insurance scheme, the money would go back to the state. The financial control will serve three objectives: a. The professional and management body of the health system would be responsive to people's demand in terms of efficiency, outreach and quality since they seek 25 % budgetary allocation from the people. b. Since 75% of the funds are assured, the professional and management staff would not be that insecure so as to compromise on misdirected people's demand. c. The autonomy in planning, budgetary allocation and implementation would pave way for creative involvement of managerial staff and the people. The superspeciality health care will be provided by state run hospital strictly on referal basis. For a purposeful link between people and health service delivery staff, it is essential that village based and village level staff is controlled more intensely by the people. I suggest the following strategy The village based staff (VHW & Dai) would be paid honorarium by Gram Sabha. The earmarked amount would be made available with GramPanchayat by the state (2 Rs. per capita per year). The powers to hire (by 75% majority) or fire (by atleast 35% beneficiary) village based functionary will rest with Gram-Sabha. To get satisfactory performance report from Gram-Sabha would be requisite minimum for doctors/ANM/Social workers before they entitle for increments or promotions. The obligatory minimum for unsatisfactory performance would be protest lodged by at least, 35% of beneficiary population. It is believed that direct control on village based staff and indirect control on village level staff will pay its dividends in increasing reopensiveness to the people. 9. What is the proposed financial layout? (Based on Sevagram experience) Money with the State for Health delivery system expenditure - 80 Rs. per capita year (1989 figure) Allocation of the District Hospital System - 36 Rs. per capita (75 % of 48 Rs) Allocation to Gram Panchayat for VHW remuneration Rs. / per capita Allocation to Gram Panchayat for health insurance contribution 12 Rs. / per capita (25 % of 48 Rs) Balance money with state government for: running superspeciality hospital - 30 Rs. /Capita To meet non-recurring expenses of cottage & district hospitals - 30 Rs. Capita For state sponsored scheme - 30 Rs. Capita 10. The role of Voluntary Sector:- Appropriate training of paramedics (Supportive role)

. . . .

- Operational Research. - An obligatory participant in evaluation of health system performance (Watch dog) - Search for alternatives (Reformist/revolutionary role) 11. Village based workers: David Werner in "VHW, lackey or liberator" has brought out comparison of doctor and village health worker. The appropriate future role of a doctor, according to the author is on a tap (not on top), as an auxiliary to the VHW, helping to teach him/her more medical skills and of attending referrals at the VHW's request for 2-3% of cases that are beyond VHW's limit. VHW has been recognised as the key member of the health team, is the doctor's equal and one who assumes leadership of health care activities in the village, but relies on advice, support and referal assistance from the doctor when he/she need it. Sevagram Field experience distillate the following:11.1 The Role: VHW can not be doctor's equal in curative services (The felt need of the people) because VHW can not command faith of an expert healer. Though people do approach VHW for symptom relief of common self limiting illnesses because of easy accessibility, the credibility that VHW enjoys depends on efficiency and quality of the supporting hospital system. VHW is regarded as a" link between system and the people. The power equation-VHW for poor villagers and superspeciality hospitals for urban elites-is seen by rural poor as double standards and glaring discrimination. To be instrumental in development activities and conscientisation process, VHW has wide scope. In pro people milieu their leadership skills can be nurtured. One does not expect government system to provide such milieu, the hopes lie with voluntary sector. Looking at the hard realities, in the present government health system, VHW can be a lackey and not the liberator. 11.2 The Selection:-

For VHW to be answerable to the community, it is often quoted that VHW should be selected by the community. However, precise structure of 'community' is never realised. In a Gram Sabha, few local affluent dominate the platform whose opinion can not be considered vox populi. All members of a village (community) never turn up for meetings. The partisan nature of selection can not be considered to be the consent of the silent majority. Apart from answerability, to the community,

VHW has to meet the minimum quality expected by the health system from him/ her to function as an efficient link. The directions of where and how to go is known better to the professional wisdom in technical matters. To meet both requirements, Sevagram experience suggests the following strategy Let the maximum number of candidates be suggested by Gram Sabha by at least 75 % majority. Suitable person would be selected from among them by the mobile health team members on the basis of following guide-lines a) The VHW should preferably come from a family which is not desperate in meeting two ends. The poor class remains engrossed in earning one's bread and hardly can think beyond. b) The VHW be preferably acceptable to all fractions of the community. c) The scope for developing leadership qualities in VHW should always be kept in the mind. d) It is obligatory that VHW can read and write in local language. e) The selected candidate should be likely to stay permanently in the village. In the Sevagram experience, it was not easy to get voluntary offers for VHW's responsibility in sufficient numbers. Quite often the health team had to persuade a person in whom leadership qualities been observed. The occasions where nonperforming VHW was to be dropped, were not infrequent. 11.3 Male or Female:The choice depends on the kind of job expected and the availability. A lady VHW is preferable if she meets the minimum quality expectations. We found it difficult to get appropriate lady candidates who fulfilled technical expectations and also perform socially with competence. 11.4) The incentive:The incentive of putting one's soul in an endeavour can be money, material, prestige, power and enjoyment of creativity. The first two become the major concern of a poor & low caste VHW who is struggling to find out his/her identity. The prestige and power attract those whose minimal basic necessities are satisfied. The creativity factor satisfies only few already conscientious individuals. By selecting VHW from middle class one tries to tap a candidate who may not be attracted merely by monetary consideration. By effective implementation of health programme the credibility of the health system can be transmitted to this vital link. By nurturing leadership skills through informal participatory education one hopes that the creativity incentive will supervene. 11.5 The Control:VHW has to be responsive to people's need at the same time be guided by village health team. The twin control can be established by providing financial support through Gram Sabha and performance evaluation by the health team.

The Bazar doctor - A harsh reality! Sham Ashtekar

Rural India - that is Bharat - presents a strange and pathetic health services scenario. It is a peculiar mix of unpardonably poor state health infrastructure and a thriving 'underworld' of bazar medical services. The ills of this heritage are manifold and multifaceted; and given the existing situation and its projections; the 21st century also shall be no different despite all that is said and done about HFA. A stunning peculiarity about rural health services is that there is a convenient division of preventive/promotive from the curative. The state services padalling a caricature of preventive programs (largly F. P.) and the private medical sector cashing up on the curative. There is enough said and discussed on what and how the state services should be (even in this issue); but hardly anything on the private sector. There is a tendency to ignore it as a nonessential (?) exploitative (yes!) sector that has no place in the scheme of things to come. No clear cut policy perspective or even pointers are available on how to deal with this large and politically strong section of the rural elite; serving the rural masses-whatever the quality - much more than what the state seems to offer as a curatitve component. (3/4th of the rural sick seek private medical services). Even if there is a full scale nationalisation of health services, there has to be some way of dealing with this sector except when one is proposing to do away with this sector with some method not known today. Till then, the rural people - poor as well as rich or not so rich - are going to suffer at the hands of the private medical practitioner. True, there are a lot of undesirable things that come with his being a 'private' practioners; but there are many ills attached to his 'medical' status also; and it is to this latter aspect that I am addressing to in this article. First of all let us examine the fibre of these 'medical' services' in rural areas - or 'periphery' to be more true to professional medical jargon - most doctors are non-allopathic. It is surely so in Maharashtra and should not be far from the truth for other states also. The numbers of medical practitioners in the rural areas are directly in proportion to the general cash-flow level in the area, the population, proximity to big cities etc. In Junnar block near Pune - this is what I have heard - there are about 150 medical practitioners; while in my own block there are about fifty. These include about 5 % of allopaths (graduate, post graduates) and then Ayurvedics, homeopaths, electropaths (?) R. M. P. s etc. Their knowledge about allopathic science is obviously very poor and largly comes from observation of senior private practitioners with whom they had chanced to work after graduation and from the medical representatives. This, plus their own perceptions in practice make a strange 'masala' of general practice offered to the hapless rural masses. Large and easy everyday earnings make this masala all the more fixed. There could be some exception to this kind of practice but this is what generally prevails here. Absence of clinical diagnosis is made good by using aggressive medication/prescriptions without any heed to science or ethics of medicine. In Maharashtra atleast, use of remedies from the respective science of healing (that the practitioner belongs to) is hardly ever seen. Thus the situation can be summed up as - most of them non-allopaths; but all of them using modern medicine without any rationale.

Can we imagine the result of this revolting situation? I see that simple malarias continue to get antibiotics for weeks & weeks, fungal infections of groin branded as STDs and patients bled for money, uterine irrilability in pregnancy given methergin injections, children with bladder-stones given large infusions to expel the 'obstruction'; acute meningitis cases being given fast infusions causing deaths, meningitis cases being treated without any diagnosis, diarrhoeas getting all kinds of antibiotics, gastro cases with renal shutdown not getting so much as a lassix injection (Rs 2) to save the kidneys, PEM babies being given all kinds of things except correct nutritional advice, Tuberculosis cases being treated with cough mixtures and so on and so on. Add to this the woes of our village women who don't even see anyone around to tell about their Gyneac complaints. There is little if anything by way of clinical diagnosis and the management of illnesses is often less than what a better trained ANMNHW should be able to offer. So is it not only exploitation of poor rural but injury too, and it is not profit-motive alone that operates this but a mighty ignorance of medical science on the part of the rural medical practitioners coupled with an abysmal illiteracy in the rural community about matters related to health and medicine. For those who choose to ignore this sector under the cover of 'private enterprise' and its profit motive, the answer is a simple ban on all this. But having worked both as a medical officer staffing a govt. health center myself and as a medical practitioner in the 'periphery'; I see a simple - if partial and limited - measure of orientation & training of this sector in the basics of modern medicine, its strengths & limitations, as a necessary step. I am aware of the criticism this attracts that this rationalises malpractices etc. - but insist that all the ills of rural medical sector are not because of privatisation alone. Ignorance is also an important factor coupled with inactive & apathetic state medical apparatus that has little to do with anything more than F.P. and 'vertical programmes'. In fact such courses are no doubt essential for many allopaths is another point. But a short term crash course a compulsory one - with a well worked out detailed programme shall serve as an important step to evolve mechanisms to regulate this anarchic sector. This will benefit rural patients much more than the practitioners themselves. Further, there can (and has to be) a consensus list of drugs & procedures that these practitioners can employ. Prescriptions/purchases beyond this list should be treated as transgressions. If these two measures - training courses coupled with regulating use of drugs etc. - are effectively employed, the rural community shall be at least partially relieved of the prevailing malpractices. I do feel that not all 'other' practitioners are tendentiously reckless or compulsive anti socials, and many of them shall welcome such trainings and facilities; at least the new-comers shall surely do. Effective legislation & regulation should iron out many things. What remains is the privatisation factor which needs discussion that is beyond this article. A tangent, not wholly out of the context, is the retraining of allopathic practitioners. In the day to day general practice, some of us increasingly feel the limitations of modem medical science in various situations and the need to resort to alternative therapies. Without elaborating on this, I would like to add that allopaths - general practitioners - also need some measure of 'alternatives' training. It is possible to institutionalise this in the same fashion as above. Lastly, in conclusion, I would like to restate that in the absence of any long term policy perspective on the rural private medical sector (nationalisation etc.) it would be a folly to ignore and wish away this sector when we think of rural health services. It is necessary to

take a pragmatic - if short term - position on a sector that is far bigger and important as compared to the

state sector in the eyes of the rural community.

Dear Friends, This is infact the last minute version & the bulletin issues that were due for sometimes. From Jan 91, the publication printing and all the business of the bulletin is with us at Nasik. Pending some formalities the regular bulletin issue could not be finalised. This mail carries the background papers prepared for the Annual Meet (Jan 91). From the next month-- i. e. Feb. 91, you shall be receiving the bulletin regularly. Wishing you all a Happy Year. (Editors)

Sham Ashtekar At. Post Dindori, Nasik - 422 202 Jan 7th, 91

Anita Borkar P. O. Box 6, College Road, Nasik - 422005


