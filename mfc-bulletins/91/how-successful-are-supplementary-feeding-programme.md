---
title: "How Successful Are Supplementary Feeding Programme"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled How Successful Are Supplementary Feeding Programme from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC091.pdf](http://www.mfcindia.org/mfcpdfs/MFC091.pdf)*

Title: How Successful Are Supplementary Feeding Programme

Authors: Jaya Rao Kamala S

medico friend circle bulletin JULY, 1983

PART - II

HEALTH FOR ALL BY THE YEAR 200 A GREAT POLEMIC DISSOLVES INTO PLATITUDES? DAVID NABERRO

Community participation: a practical possibility or more wishful thinking?

A central feature of the PHC approach is the involment of people in the delivery of the health services they receive. Inevitably conflicts — between factions in the community, between professionals and community representatives, between funders and professionals, for example — are bound to arise whenever participation is initiated. These conflicts are faced by PHC workers, not only in South Asia, but throughout the world. We can all conjure up the ideal scenario which is presented by some proponents of the PHC philosophy. A group of people who live in a village or street meet together, discuss their health problems, decide that they need more knowledge and skills with which to tackle these problems, and then nominate (and if necessary, elect) someone who will be sent for training as a health worker. The government health services set up a special health worker training programme. The people then evolve a system for paying the worker for his/her services and for covering costs of medicines. As a result of the information they receive from the newly trained worker, the people all change their life styles, the health risks they face are reduced then child mortality rates and other health indices change for the better. It sounds good and we would all like it to happen. What PHC workers actually find is that the people of a particular village, street or district - the community - belong to different groups. Members of anyone group may be characterised by, for example, family ties, caste status, ethnic origin or religion. Groups may be further distinguished by their allegiances to different leader’s political ideologies of established political parties. Few communities have evolved ways of making

decisions by mutual agreement. In practice, the decisions that carry the day are made by the most powerful, and they are not always the group that is in the majority. If power is shared between different groups they may attempt to reach a decision by compromise. But in the end the compromise could still be elusive because opposing groups take different positions on principle. If there is active dissent over a particular issue inside a community, professionals who automatically side with the majority viewpoint may well be met with active resistance from members of an outspoken and powerful minority. The latter may cause trouble for the elected health worker interfere with the career of the local government doctor, and even damage property used by the health services. These political realities may influence even the most carefully conceived, fair sounding and humanitarian Primary Health Care project. Conflicts can be anticipated. even if they cannot be avoided, if those involved n designing projects analyse the political and economic processes that affect the people to be served as well undertaking a more usual study of the diseases they experience. But it is not only the "community" that has to "participate" if people are to be actively involved in health care systems. Medical professionals will also be expected to provide support and training to nominated health workers to heed the criticisms or to respond to the demands of community representatives, and to provide an effective backup curative service for people who are seriously ill. Inevitably medical professionals will have their own priorities — and their own interests (be they the discovery of rare diseases. providing a service to people who request it or simply earning enough money to survive). They will also have knowledge and skills with which to identify health problems

in the community.

methods for tackling them. However, the professionals' specialised view of health problems may well be different. They

will suggest what they see to be the most suitable from the views of different community groups. Certainl the professional will want to adopt the approach that he - on the basis of training and experience - considers appropriate.

y

.

Consider a situation where community group, after discussing their health care problems, reach some kind of a compromise about the kinds of services they want. The "most powerful group (in a minority) has won the day with a request for a curative service which tackles problems faced by middle aged men. A less powerful group (representing a majority) has failed in its attempts to get improved curative facilities for women. The professionals responsible for providing services for the community take several different viewpoints. Some favour a preventative and educational programme. Others propose a specialist curative programme that will meet the needs of the most powerful. Inevitably there will be conflict — both in the profession and between some members of the profession and some groups in the community. "Health for all" or "Medical Care for all”? I wonder whether "Health for all" really is an appropriate slogan for activities that are undertaken primarily by the medical and allied professions. "Health for all" is a vital goal for all development workers as ill-health is inevitably a matter of poverty and deprivation. Health for all will any be achieved through collective action from technicians and administrators involved in all sectors of development, and they can only do this if they have the necessary political backing. Important sectors of government which can contribute to this process include those concerned with agricultural development and industrialisation, with the provision of amenities (like water supplies and sanitation), with the redistribution of resources (particularly land reform) and with the development of the machinery of government, financial institutions and communications. Doctors, nurses, nutritionists and the like are well placed to identify "unhealthy" population groups needing priority help. At the same time, everyone gets ill. Whether they are rich or poor, people wish to keep disability to a minimum, make it last for as short a time as possible and to avoid death. In many instances distress can be alleviated by effective counseling, preventive health actions, skilled use of medicines, first aid, operative treatment and so on. Those of us working in the medical profession need to ensure that these medical and nursing skills are as widely accessible as possible. We can debate and try to define levels of medical

care to be

provided for a population, the quality of care and the accessibility of services. To do this well will need access to good epidemiological information. Our goal, for example, may be to reduce the distance between people's homes and different kinds of services, relating availability of services to the needs of the population and minimising the time people have to wait for attention when they seek help. We may also want to ensure that the services provided are of high quality and the costs that people are charged are within their means. When we are considering these kinds of levels and patterns of medical care we are considering activities that can be put into practice by the medical profession. These are very different from the activities which might one day lead to health for all, and in which the majority of medical professionals can only play a very limited role. If we are serious about improving the provision of medical care, we have to consider manpower issues, too. Given the limited number of medical and nurse professionals available to provide medical care in disadvantaged communities, we need to set out clearly the tasks for which medical auxiliaries should be trained. Their requirements for in-service training and supervision need to be spelt out too. All of us have a role to play in providing good quality, relevant task-orientated training to people working at a number of different levels in medical care services. During the planning process the medical services proposed for each community need to be debated with its representatives. Professionals involved in programme planning and implementation need to be prepared for the inevitable conflicts between different interests involved, wherever they are working. These conflicts will increase the more people are involved in - and participate in — their own "development". Medical and nursing professionals working in Third World countries need to work to spread the availability and accessibility of appropriate medical care services among all population groups — particularly the groups who are traditionally disadvantaged and denied services. This will not be easy. There are vested professional interests attempting to keep control over the availability of medical care; to profit from the illness of others; to encourage the over prescription of unnecessary drugs and to concentrate on the rare, dramatic and interesting illnesses in society. Rather than concerning themselves, almost uniquely with complex issues that have implications outside the medical professional sphere, international medical organisations would do better to concentrate their efforts on promoting improved provision of curative medical care and public health services throughout the world. They need to provide maximum support to those who are trying to counter the vested

interests inside the medical professions that work so effectively against this objective.

Perhaps a new operational objective for activities undertaken by the World Health Organisation, Government Health Ministries and the Medical Professionals is “Medical care for all by the year 2000". The aim should

be to provide good, appropriate, accessible effective care and public health services and supervise doctors, nurses and auxiliaries to provide them. This is within the context: development goal, of "Health for important polemic that must never be in a sea of platitudes.

HOW SUCCESSFUL ARE SUPPLEMENTARY FEEDING PROGRAMMES? KAMALA S. JAYA RAO It is very well known that undernutrition among children is a major public health problem in India as well as in other developing countries. With a view to mitigate this problem, many governments sponsor large scale supplementary feeding programmes. Imrana Qadeer had discussed in one of our earliest issues the relevancy or irrelevancy of these 1 programmes . The programmes have been going on and supplementary feeding is presently a major component of the Integrated Child Development Services (ICDS) of the government of India. A question often raised is, how successful are these programmes? The impression gained from listening to seminars or through personal discussions with various people is that the programmes do not really improve the nutritional status of beneficiaries in any substantial way. However, when one goes through the relevant literature, the position is not as clear. As Gopalan said, "With no built-in machinery for scientific direction or evaluation, the position with regard to 2 many feeding programmes is truly chaotic" . \

Recently the UNICEE had commissioned Beaton and Ghassemi to write a report on this subject. The report says. "while food distribution programmes of many types have been introduced in many countries relatively few have been examined or evaluated in a manner that produces usable 3 information for the present assessment of past experience" .

1)

the young, preschool child has to be brought the centre by an older sibling or parent;

2)

the centre must be reasonably close to the house to ensure regular attendance;

3)

the centre needs personnel to cook and serve the food;

4)

the child has to eat the' entire amount of food a', one sitting;

5)

Since no organisation can run throughout the yea without break or holidays, the feeding in effect generally takes place only for 250 - 300 days in a year.

The second type of feeding programme is the TakeHome distribution programme. Food, adequate for a week or more, is distributed at the centre and can be carried home.

1)

This needs less frequent visits to the centre and proximity of the centre to the house becomes less important.

Types of Feeding Programmes:

2)

The presence of the beneficiary child at the centre is not essential.

Before evaluating the programmes, let us consider the two main types of feeding programmes. The first is the on-thespot feeding or supervised feeding programme. The children are "assembled at a Balwadi or some such central location and are fed once a day. This entails that.

3)

The food can be given to the child whenever it is hungry, and in divided amounts, if so desired.

4)

The child will receive the food for all the days in the year.

5)

The centre does not have to keep utensils for cooking and serving the food.

In the take-home system, there is the risk of sharing of food by other members of the family. However in most studies where this was noted, it was observed that the sharing occured with siblings under 10 years of age and not with adult members. This partly depends on the nature of food distributed. Any food identified by the community as children's food was generally not consumed by the adults. To overcome the problem of sharing in one study conducted in Colombia. South America, enough food was given to take care of the energy gap among all members of the family'. The targets were however pregnant and nursing mothers, and young children. The food ingredients are those commonly used by the local population such as bread, oil etc. It was however found that the total calorie intake of beneficiaries had not increased. It was concluded that either part of the, food was being sold in neighbouring, control villages or the money spent otherwise on food by the family was now being used for buying other necessities.

The Problem of Substitution: Thus in the above study, the intended supplement was being used as a substitute. This problem of substitution is seen in both supervised feeding and in take home programmes. In the former the time of feeding is fixed according to the convenience of the organisers. It was felt that if the gap between this and the normal meal time at home was not much, the child may not eat its usual quota. However the process of substitution even in the take-home programme belied this assumption. "The overall impact of these programmes, of either type in filling the apparent 'energy gap' of young children is disappointing". "For most programmes the supplement was designed to meet about 40-70% of established energy gap. In point of fact only 1025% of the gap was closed". "On the basis of these data alone, one would have to question the effectiveness of food distribution programmes in effecting satisfactory net increases in energy intake 3 among target population” . However, the results of some individual studies have been more encouraging. Thus, according to the Project Poshak, the food was used as a complete supplement in children between 1-2 years and mostly as a supplement, and only to a small extent as a substitute in those between 2-4 years. The supplement was hardly ever given to infants 5 below 1 year . A strong component of health and nutrition education is necessary for the parents, particularly where the take-home system operates. This may ensure, to some extent, that the supplement is not utilised as a substi-

tute. The extent of substitution will also depend on the economic pressures and financial liabilities of the family. In the take-home system, if all the children of the family are not included sharing will become inevitable unless the supplemental food is the same as the staple consumed in the family. In on-the-spot programmes too, sharing cannot be avoided in the poorer families. The food from the family pot which would have normally been the share of the beneficiary Child may now be offered to the older siblings. In many instances the person accompanying the child to the feeding centre happens to be older sibling and she has to be included in the programme both for psychological and ethical reasons. A strict beaurocratic attitude of including only preschoolers, whatever their vulnerability from the nutritionist's point of view, will not help in the long run. Improvement in Nutritional Status: The above conclusions were drawn mostly from diet survey data. Results of nutrition surveys, on the other hand, are more encouraging. It is well recognized that individual diet surveys, particularly of young children is a difficult task. The errors can therefore be great. In this respect nutrition survey data are more accurate.

Evaluation of a Special Nutrition Programme organised for tribal children showed that in villages where the programme was on, there was no grade III malnutrition. Incidence of grades II and I was less compared to that in unsupplemented control villages. It is not known how long after the implementation of the scheme, these changes could be seen. The 'evaluators attributed the success of the programme, which was on-the-spot type, to the fact that the villages were compact, locals were employed as helpers 3 and food material was directly supplied to the organisers . Other studies also showed that when planned and operated properly, supplementary feeding can result in improvement of the growth status of the malnourished child. By' and large it appears that success depends as in any such programme, on the dedicated involvement of the organisers and in the continuous operation of the programme. Such of those Voluntary agencies or research projects have been successful because a single agency operates the programme continuously in the same villages, An important observation made in one of the studies was that although all malnourished children benefited, those severely malnourished benefited most the increments in 7 heights and weights being more in these children . In any large scale feeding programme almost all children of the

community are included, irrespective nutritional status. If a striking im-

of

their

provement is seen only in the severely malnourished, then these) will be missed in .the calculations of means. averages etc .of the whole group. It is therefore necessary that children with different grades of malnutrition are classified separately before the start of the programme and the effects on both food intake and body growth are' calculated separately for each group. Such an analysis may provide a more accurate picture. If supplementary feeding programmes are then found to help reduce grade ill malnutrition and prevent others from sliding into grade III the programmes cannot be deemed unsuccessful.

I will not go into the cost-benefit effectiveness of the programmes. Firstly, there are not many, or perhaps any such evaluations. Secondly, I am not competent to make such assessments. The question that however, needs to be asked is even if these programmes do mitigate malnutrition to a some extent how long could they be continued?

whole programme will be a colossal burden and the country will become more and more dependent on foreign aid whether that comes directly from donor countries or through international agencies. Assuming that we save young lives through ICDS, what is the next help the child will get in terms of food in late childhood, education, vocational training, etc? One finds no answers. As Dr. Gopalan said, "In the long run, we can hope to improve the nutritional status of our preschool children only through improvement in the economic conditions of the community to a level at which families can afford balanced diets. Organised state-sponsored feeding programmes cannot be the permanent answer to the problem". "The need for supplementary feeding programmes for preschool children will be in inverse proportion to our success in the matter of removal of socio-economic: disparities and improvement of economic and living 2 standards of our people" .

The ICDS Programme: Every nutrition programme, when taken up is referred to as a short-term measure the implication being that such measures is needed till socio-economic conditions improve. However neither administrators and planners nor politicians define this word, 'short-term'. One wants to know how long will the short term be? In view of the programmes being" termed short-term, they are started as ad-hoc programmes; the definition of short-term is restricted to this and unfortunately no attempt is made to link it with a long term goal. Or, may be the truth of the matter is that our developmental plans have no specific 8, goals. The mammoth ICDS programme is a case in point 10 . Nowhere is it mentioned as to how long the supplementary feeding will be a part of the comprehensive programme. The whole ICDS project is actually termed an experiment and yet an experiment whose duration is not mentioned. A strange way indeed of starting an experiment. Although the ICDS was conceived by the Fifth Planning Commission it does not seem to have beel1 linked with any economic programme. The latest evaluation report of the ICDS attributes the improvement in the nutritional status of the children solely to the programme since there were absolutely no changes in the socioeconomic conditions in those study areas. This may be very well as far as the success of the ICDS is concerned. The ICDS covers large parts of the country and more areas are coming under its purview.' In view of this that statement needs serious consideration. How long will a country be able to feed children, who may number 100 million or more, without any socioeconomic improvements? Was the ICDS programme started in areas where the Planning Commission also launched some new economic programme? If not, the

The question is not whether supplementary feeding programmes improve nutritional status of the children. The point is unless they are a part and parcel of an overall economic programme they should not be taken up. The success of the programme should be measured in the context of the success of the overall economic programme. If not they should be restricted to only two situations. Firstly, in emergencies like acute natural and man-made calamities. Secondly, in extremely poor and backward areas where the administrators and planners' are able to state categorically that significant improvement in socio-economic conditions will not be possible in a specified period of time, for whatever stated reasons.

REFERENCES: 1. Imrana Qadeer. MFC Bull. 14. 2. C. Gopalan. Proc. Nutr. Soc. India. 15 (1973).

3. G. H. Beaton and H. Ghassemi Amer. J. Clin. Nutr. (Suppl) April 1982. 4. J. O. Mora et. al. Nutr. Rep. lnternt'l. 17:217 (1978). 5. Project Poshak, Vols. 1 & 2. CARE INDIA; New Delhi; 1975. 6. D. Hanumantha Rao et. al. Indian J. Med. Res. 63: 652 (1975). 7. D. Hanumantha Rao and A. N. Naidu. Amer. J. Clin. Nutr 30: 1612 (1977).

8. ICDS: A co-ordinated approach to children's health in India. Lancet 1; 650 (1981). 9. B. N. Tandon et. al. Indian J. Med. Res. 73: 374 & 385; 1981. 10. ICDS. Progress Report after Five Years. lancet 1; 109 (1983)

CONCLUSIONS REACHED BY THE DRUG SUB-GROURP AT THE NATIONAL HELTH POLICY SEMINAR 23rd APR I L 1983 Towards a Rational Drug Policy: 3.

Organizations involved in this kind of drug work should be kept informed about hazardous drug being banned and withdrawn and the new drugs which may be cheaper and better alternative’s.

4.

Information about non-drug therapies specie: locally available common home remedies, herbal medicines etc. should be made available. Cent Council for Research in Ayurveda would help.

5.

Consumer caution on commonly used drugs v. potential and serious side effect should be writ, on the package e.g. on all anti-diarrhoea is, it should mandatorily be put. "Oral Rehydration therapy the main" treatment of diarrhoea and this is he you can prepare the ORT solution - to be shown pictorially.

6.

Voluntary organizations should play an important part in giving counter it information and the "drug information campaigns" should be widely launched and well coordinated. For example, the concept essential Vs unessential drugs vs brand, hazard drugs combination drugs, misuse of antidiarrhoeal hormonal pregnancy tests, etc.

(1) Recommendations for a National Drug Policy as an essential ingredient of a National Health Policy should be widely publicised (in accordance with the National Health Policy document). (2) Voluntary organisations involved in the drug issue should be involved in decision making about certain drug policies for ensuring collaboration in implementing of these policies, too. Essential Drugs: (1) Adequate amounts of essential and life saving drugs should be produced according to peoples health needs and should at least fulfill the Drug Companies installed capacity. (2) A certain fixed proportion of drugs produced by each drug company should constitute of essential and life saving drugs. This should be made mandatory, specially when large amounts of profitable, unessential drugs are being produced. (3) On anti-TB, anti-Leprosy, anti-Malaria, Vitamin-A drugs and fortified (iodized) salt there should be no sales tax and excise duty. Costs should be substantially decreased for another essential and life saving drugs, too.

Drug Control and Drug Ban 1. Drugs recommended for being weeded out by the Drug Consultative Committee should be withdrawn immediately. 2.

Banned drugs should be seized and destroyed aft, a fixed date since such drugs are produced and dumped in the market arid continued to be so even after the last date.

(4) Quality controlled essential and life saving drugs should be available under Generic names as mentioned in the National Health Policy. . (5) *Installed capacity and drug production should be related to the population.

3.

Better quality and drug control mechanism is demanded/needed. For this, the drug manufacturers and ultimately the government drug centre authorities should be held responsible. Before going into the market each batch should be quality controlled.

Drug Information;

4.

The detailed information about the number' c cases of spurious drugs detected and action take should be made public. (Specific case of a ma caught for producing and selling spurious drug three times and being released on bail was mentioned.)

1. Unbiased information and publicity of banned and hazardous drugs should be disseminated by the Government drug control authorities in national dailies, medical journals and magazines and it is their responsibility. 2.

Drug companies withdrawing the certain batches of drugs should mandatory inform doctors, chemists and consumers about the withdrawal of these specific drugs, the batch number and the reason for withdrawal?

* (During the summing up session, a few more recommendations were made and are being included.)

5. Drugs in the market should be constantly reviewed and 'irrational drugs based out. An effective mechanism for this should be developed and monitoring should be on ongoing basis.

forward by the company alone. A matter which needs to be dealt by the Bureau of Industries pricing.

Research:

Drug Marketing For Research and Documentation amount more than

.

-

1. Where unethical marketing practices, are concerned, e.g. Sablok Clinic, anabolic steroids, tonics, tranquilizers, evens some indigenous drugs, etc. it should be the responsibility of the drug control authorities to deal with the defaulters and not that of citizens and consumer groups. 2. Authentic trial reports and studies in India under government supervision should be required before allowing a new drug in the market this was nut for example in the case of Sulphin pyrazone, (according to a participant).

the present 1.1 % of sales turn over should be utilized

specially as for the common tropical diseases.

Consumer Action: 1.

Concerned organisations and individuals should make it a point to inform the Drug Control Authorities about sales of spurious drugs and banned drugs or drugs without medical literature, drug past expiry date etc.

2.

Whenever Government authorities are involved in legal proceeding against the drug industry an making effort geared to promoting and ensuring people’s health, we should. support such efforts.

Pricing: 1.

2.

Quality drugs should -be obtained at competitive prices through open tenders, whether it is at district, state or national level. There should be greater restriction on prices of category 3 and 4 drugs instead of category 1 drugs. Drug prices on essential drugs should be fixed on most economic packaging and not on what is put

Coordinator: Dr. MIRA SHIVA, VHAI Rapporteur: Mr. J. S. MAZUMDAR. Federation of Medical Representatives Association of India.

.

MYTHS ABOUT DIARRHOEAS (Extract from a pamphlet by Medical News Group, London) Myth: In infective diarrhoea the infecting organisms irritate the mucous membranes in the bowel .and this results in diarrhoea. False Therapy: Antibiotics or bacteriostatic like streptomycin, neomycin and sulphonamides will sterilise the gut and thus cure diarrhoea. Myth: Diarrhoea, whatever its cause, is due to excess fluid in the gut. To cure diarrhoea it is necessary to give medicines that will absorb the fluid. False Therapy: Kaolin mixtures. Myth: Diarrhoea is due to increased motility within

the gut. Compounds that will dampen down the peristaltic rush will -cure diarrhoea. False Therapy: Morphine, codeine, diphenoxylate and loperamide are useful. Dogma Disputed: Codeine although partially effective may cause drowsiness and dizziness. Diphenoxylate and atropine (lomotil) is less effective than loperamide and its use is associated wit toxicity' in children. Even in adults correct: constituted glucose — electrolyte mixture: should be first line treatment.

FROM THE EDITOR'S DESK… THE BAN ON PRIVATE PRACTICE The Telugu Desam government an Andhra Pradesh has banned private practice by .government doctors from 1st May, 1983. On the face of it, it is ridiculous, that government doctors were allowed private practice in the first place. No other civil servant in the country is allowed to take up any private employ men or business. Perhaps in the days gone by, when there was a paucity of doctors, this was useful to the public and the government, too.

As expected a few specialists and super specialists, who have adequate government service to opt for voluntary retirement have done so and a few more have resigned from government service. The rest, also as expected, are agitating. They have started what they learn "Work to rule" tactics, are taking a longer time than usual and perhaps even necessary to examine each patient are threatening to go on "Mass casual leave". The last is an euphemism for remaining away from work. The whole energy is not new to Andhra Pradesh. Sometime around 1968, the Congress government then in power: also attempted the ban. The reaction from the doctors was similar as now. With the resignations of professors and specialists the government lot heart and revoked their decision. The cause for the revocation might or might not have the ensuing dearth of hands to man the hospitals and dispensaries. Most high officials and ministers are used to utilise the free facilities and services of the specialists in hospital and they feel threatened by the loss of this group. The senior doctors and specialists are thus fully aware of the vulnerability of those in power and their own 'indispensability'. One only hopes that the present

government will withstand these pressures and stand by its decision to ban private practice. More than the question of whether a doctor, unlike other government servants, should be allowed addition means of income, it is the uncontrolled malpractices, for the ban. It is not necessary to discuss them here, which have pervaded the government hospitals that call Since MGC had discussed this on many occasions. This has been one of its major planks of debate. The malpractices in the hospitals, the present threat of "Mass casual leave", "Work to rule" threat etc. once again brings to light the callousness of the Indian elite towards the poor. It is the poor and the lower middle classes that suffer when government doctoes are allowed private practice and also when they resort to the "trade union style" tactics. It is foolish to think that the miserable condition of in the hospitals will change because of a mere ban on private practice. Moreover, some of the doctors have decided to change the name boards on their private clinics either in favour of another private physician for consideration of some fee or to a 'benami' doctor. As in any other such instance, the basic character of an individual cannot be changed by legislation and government orders.' However, the decision of the AP Government is in the right direction. It has public support and possibly the medical students also support it (the have made no statement on this; perhaps it is examination time). The ban may wear away the coming gene-rations of doctors from simultaneous private practice one hopes that other states which now permit practice by government doctors, will soon follow the example of the Andhra Pradesh Government.

Editorial Committee: Anant Phadke Padma Prakash Ravi Narayan Shirish Datar Ulhas Jajoo Kamala Jayarao - EDITOR

Views & opinions expressed in the bulletin are those of the authors and not necessarily of the organisation.


