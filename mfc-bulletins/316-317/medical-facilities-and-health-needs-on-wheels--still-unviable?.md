---
title: "Medical Facilities and Health Needs on Wheels: Still Unviable?"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Medical Facilities and Health Needs on Wheels: Still Unviable? from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC316-317.pdf](http://www.mfcindia.org/mfcpdfs/MFC316-317.pdf)*

Title: Medical Facilities and Health Needs on Wheels: Still Unviable?

Authors: Dr. N. Kannan

medico 316- friend 317 circle bulletin April-July 2006

Impressions from a Rural Laboratory -Jan Swasthya Sahyog1 Jan Swasthya Sahyog is a voluntary, non-profit organisation that has been providing primary and secondary level healthcare mainly to rural patients in Bilaspur, Chhattisgarh, over the past six years through a three-tier programme that includes: i) A referral centre with a ward and an outpatient clinic that is accessed regularly by people from a radius of over 50 kilometres. The ward has 15 beds for patients recovering from general, gynaecological and ENT surgery and for adults and children with medical problems. The outpatient clinic runs thrice a week for patients with all kinds of problems and is staffed by specialists in Internal Medicine, Paediatrics, Surgery, Gynaecology and ENT. There is also a tuberculosis clinic that runs once a week. The ward, the OPD and the two operation theatres are supported by x-ray and ultrasonography facilities and a diagnostic laboratory. The referral centre may therefore be considered roughly equivalent to a government CHC except for the smaller number of beds. ii) Three outreach clinics every week in forest-fringe villages staffed by single doctors of any speciality functioning as GPs. The doctor is assisted by a pharmacy assistant carrying more than 50 essential drugs, a laboratory technician carrying a microscope, a centrifuge and a colorimeter for basic investigations and a registration clerk. Together, the three outreach clinics cater to patients from over 150 villages and correspond to a PHC OPD in terms of the range of services offered. iii) An intensive village health programme in 42 villages based on 85 female village health volunteers trained by Jan Swasthya Sahyog. 1

<jss_ganiyari@rediffmail.com>,Janswasthya@gmail.com>

We feel that some of our experience in the laboratory might be of interest to others in similar situations and we are summarising the key points in the following paragraphs. 1) The single most cost-effective investment in a small laboratory is a well-maintained microscope of good quality. It is equally important to invest in the training of the person who sits behind the microscope. It is surprising how often a microscope comes in handy to differentiate the causes of the various symptoms commonly encountered in a rural hospital. For example, Cough with or without Fever • Z-N stained sputum smear for acid-fast bacilli • Gram-stained sputum smear (pneumococcal pneumonia has a very characteristic microscopic appearance; so do aspiration pneumonia and bronchiectasis) • Absolute eosinophil count • Absolute lymphocyte count (Very high counts in infants and children with prolonged paroxysmal cough suggests whooping cough in communities with poor vaccination coverage) Fever without Localising Symptoms • Peripheral blood smear for malaria parasites. (Finding out the species of malaria parasite allows one to choose the right dose of Primaquine and anticipate the severity of the illness. Counting the density of Plasmodium falciparum in blood allows one to decide on early referral, the need for exchange transfusion and the choice of antimalarials, e.g. Artesunate for severe malaria

2 § § •

•

mfc bulletin/April-July 2006 Total and differential leukocyte counts Microscopic examination of urine sediment Slit skin smear for AFB (Multibacillary leprosy is common enough in our area for ENL to be an important cause of pyrexia of unknown origin. Since some of these patients only have diffuse infiltration without discrete skin lesions they are invariably missed by the current criteria for leprosy diagnosis that are entirely dependent on visible lesions or anaesthetic areas on the skin. We diagnose six to eight cases of ENL yearly) People in eastern India could also look at smears of splenic or bone marrow aspirates for LD bodies

Diarrhoea • Pus cells and ova, parasites and cysts in stool wet mount • Hanging drop for Vibrio cholerae • Kinyoun stained stool smear for coccidian oocysts, particularly important in • patients with persistent diarrhoea including AIDS patients Infertility Semen analysis, which we always do before investigating the wife

Joint Pain and Swelling • Total and differential cell count of synovial fluid • Gram stained smear of synovial fluid • Eosinophil count in peripheral blood • (Examination for crystals in synovial fluid is another thing that should be done but at the moment we do not have the accessories for polarised light microscopy)

Hand Rash Both hand eczema and tinea manuum are common complaints in agricultural communities and can often be difficult to differentiate. A positive KOH mount for fungal hyphae often clinches the diagnosis in favour of the latter.

Dysuria • Urine wet mount for pus cells and trichomonas • Gram stained urethral smear for gonococci • Vaginal fluid for trichomonas and yeast cells

A lot of recent technical developments focus on one parameter or analyte, e.g., malaria antigen in the case of dipstick assays, at an enormous cost. A microscope does quite the opposite: it uses one versatile piece of equipment to diagnose dozens of illnesses at a very low cost and has the potential to identify new problems as they arise.

Vaginal Discharge • Saline mount for Trichomonas vaginalis • Gram-stained smear for clue cells and for the semiquantitative assessment of bacterial morphotypes to diagnose bacterial vaginosis, for yeast cells to diagnose vaginal candidiasis, and for elongated pus cells trapped in strands of cervical mucus to diagnose endocervicitis Anaemia § Microcytosis and hypochromasia with pencil cells, teardrop cells and target cells in iron-deficiency anaemia § Macrocytosis and hypersegmented polymorphs in folate or B12 deficiency anaemia § Extreme aniso-poikilocytosis in thalassaemia § Sodium dithionite mount for sickle cell anaemia or sickle cell trait § More or less normal RBC morphology with leukopenia and thrombocytopenia in aplastic anaemia § Elevated leukocytes with blasts in leukaemia Meningitis Elevated cell counts support the diagnosis and the classic appearance of pneumococci or meningococci under the microscope permit presumptive identification allowing one to choose antibiotics accordingly

Microscopy vs. Newer Technology

2) It saves the patient’s money if you use test panels for specific symptoms or syndromes while being flexible about the sequence of testing The exact tests done and the sequence in which they are done depend on i) the cost of the tests, ii) the local prevalence of different diseases, and iii) the urgency of the situation. For example, in a patient with fever but no localising symptoms or signs who comes to our centre, the initial panel of tests would include: § Peripheral blood smear for malaria parasites, § Total and differential leukocyte counts, § Microscopic examination of urine sediment, and § ESR, (a raised ESR with normal TLC would heighten the suspicion of tuberculosis) There is merit in doing these tests in sequence and do the second one only if the first one is negative; since, in our experience, malaria and UTI are among the commonest causes of undifferentiated fever here and also because all of these tests cost so little to perform. For example, in a person with fever of three days’ duration, if the blood smear shows malarial parasites the laboratory may not spend time in doing TLC and

mfc bulletin/April-July 2006 DC. When these initial tests are non-contributory, we go ahead with the Widal test and Chest X-ray, while if the urine shows pyuria, we go ahead with a urine culture. However, if the patient is very ill, we would do all of these tests at the same time and also take two blood cultures, start antibiotics and antimalarials and then send for a Chest X-ray. 3) Doctors are fallible too. Test panels work best when they are linked to a panel of ‘must ask’ points in the history and ‘must see’, must auscultate’ and ‘must palpate’ points in the clinical examination for particular symptoms. For example, reliable laboratory reports on urine sediment microscopy and urine culture and antimicrobial sensitivity cannot ensure quality care on their own if the treating doctor forgets to look for prostatic hypertrophy in elderly gentlemen with laboratory-proven UTI. 4) To save the patient’s money and the laboratory’s time, do not get a test done if the result is not going to affect your treatment strategy. For example, we would not measure bilirubin or transaminase levels in a visibly jaundiced patient whose illness is clinically compatible with viral hepatitis. Similarly, we would not bother to ask the laboratory to look for microfilariae in the blood if we have already made up our mind to give DEC to a young patient with swelling of one foot and minimal pain of sudden onset four days ago. This is because the epidemiological probability of filaria in this situation is so high, the test takes so much time and effort to perform and the results are not always positive in patients with filarial lymphangitis. 5) If the patient cannot come to the laboratory, the laboratory can go to patient, at least, sometimes. There are several ways we carry out this concept in practice: a) In the 42 villages where we work intensively: • • • • • •

Village Health Workers (VHWs) make thick blood smears for patients with fever Slides are labelled and wrapped after drying Slides are given to children going to school Slides are dropped by children at designated ‘Paan shop’ at the nearest bus stand Slides are handed over to bus conductor, who drops them at our health centre Slides are processed and reported at our laboratory

Reports are sent back to the VHW by the same route, i.e.,from the laboratory via bus conductor via ‘Paanwala’ via schoolchildren to the VHW. This system has been functioning without major hitches

3 for the past 50 months and has transported more than 6,000 smears and their reports between the village health workers and our health centre with an average slide positivity rate of nearly 20%. b) In the last one year, we have taught some of our VHWs to inoculate stool samples from patients with dysentery and watery diarrhoea on XLD agar and TCBS agar respectively. The plates have been brought back to the laboratory at our referral centre and have yielded isolates of Shigella spp. and Vibrio cholerae after incubation with good recovery rates. In the case of Shigella spp., it has allowed us to document very high rates of resistance to co-trimoxazole, amoxycillin and nalidixic acid in the villages; in the case of Vibrio cholerae it has allowed us to notify the government health authorities early on in the course of an outbreak for prompt initiation of control efforts. c) Similarly, we now inoculate urine samples on CLED agar in the outreach clinics and bring them back to the laboratory for further processing. This has again helped us avoid the trouble of transporting urine samples in the fluid form over bumpy roads and allowed us to find out differences in antimicrobial resistance patterns between uropathogens isolated from patients belonging to primitive tribes inside forests and those belonging to more mainstream tribes living in forestfringe and other villages, the latter having resistance rates as high as seen in city patients. 6) In an integrated set-up, many lab tests pay for themselves through the avoidance of shotgun therapy or by allowing the use of less expensive drugs. For example, a) At our referral centre we always do a microscopic examination of vaginal discharge unless the woman refuses internal examination. Bacterial vaginosis is the commonest cause of vaginal discharge in our population (and anywhere else in the world, for that matter) accounting for more than one-half of all cases of discharge. Therefore more than half of our patients with vaginal discharge with documented bacterial vaginosis go home with Rs. 10 worth of metronidazole as the only treatment. If these same women were to be treated syndromically, it would cost them Rs. 10 for metronidazole, Rs. 17.50 for doxycycline, Rs. 20 for ceftriaxone and Rs. 6 for fluconazole or a total of Rs. 53.50 apart from being exposed to the unjustifiable hazard of treatment with avoidable antimicrobials. b) We now routinely culture urine samples from all patients with suspected UTI. We have to do this because in our area, resistance against co-trimoxazole and tetracyclines is seen in two-thirds, against nalidixic acid in one-half and against fluoroquinolones,

4 aminopenicillins and 1st generation cephalosporins in around one-third of all isolates even from previously untreated women with uncomplicated, communityacquired UTIs. Roughly one-sixth of isolates from this group of women are simultaneously resistant to aminopenicillins, 1st generation cephalosporins, cotrimoxazole, tetracyclines, all quinolones and gentamicin, the drugs that are used most often in this setting. Resistance rates among patients with recurrent or complicated or nosocomial UTI are, of course, much higher. In a situation like this, the cheapest drug that we can empirically give to a previously untreated patient of pyelonephritis with a lower than 20% risk of treatment failure is gentamicin, which costs of Rs. 180 for a ten-day course including the price of disposable syringes. On the other hand, when we do a urine culture that costs Rs. 80 in our centre (price required to break even) we actually save money if the culture report allows us to use co-trimoxazole or ciprofloxacin after culture results become available. If we use co-trimoxazole from the third day onwards, we spend only Rs. 136 for the entire course of treatment (Rs. 80 for culture + Rs. 36 for two days of gentamicin + Rs. 20 for ten days of cotrimoxazole). For ciprofloxacin with a higher cost of Rs. 50 for a ten-day course, we still spend only Rs. 166 (Rs. 80 for culture + Rs. 36 for two days of gentamicin + Rs. 50 for ten days of ciprofloxacin). c) Routine follow up of our chloroquine-treated falciparum malaria patients with repeat smears on the 4th and 14th days after treatment has shown us that resistance rates have consistently remained below 5% over the past five years, at least in the area that we work in. This has allowed us to continue to use chloroquine for the majority of our patients with falciparum malaria. At our centre, a course of chloroquine for a 60-kg adult costs only seven rupees. A course of quinine and doxycycline, on the other hand, costs Rs. 220. The cost of monitoring chloroquine resistance is clearly offset by reduced drug costs in the village health programme in this case. d) In our laboratory, we subject all sputum samples that are negative for acid-fast bacilli (AFB) to a concentration process that was described in 1988 in the Indian Journal of Tuberculosis by Dr. R. Vasanthakumari, then of the Institute of Thoracic Medicine at Chennai. The technique is inexpensive and user-friendly and is the only concentration process for sputum that does not require the use of a centrifuge. In a series of 1966 sputum samples in our centre, 129 samples were positive for AFB in direct smears while another 115 samples were positive for AFB after concentration by this technique. The cost of concentration, which was Rs. 10 in our hands, was

mfc bulletin/April-July 2006 adequately compensated by the fact that we could avoid a much more expensive chest X-ray in those patients whose sputum samples turned out to be positive after concentration. 7) Technology adapted ‘in house’ and modified technology often works just as well as the commercial or standard versions: use these whenever possible to reduce costs. a) Dr. Pramod Upadhyaya, an active member of Jan Swasthya Sahyog while continuing to work at the National Institute of Immunology, New Delhi, has developed a capillary centrifuge and an electrophoresis apparatus that work perfectly well and cost 25% and 10% as much respectively as their commercially available counterparts. b) At Bilaspur, our chief technician has steadily reduced the size of the agarose gel for haemoglobin electrophoresis, with an accompanying decrease in the requirement for fixer and stain, to the point that we can offer haemoglobin electrophoresis for sickle cell anaemia at the rate of Rs. 16 per sample, including the cost of running appropriate controls. c) At Bilaspur, we have constructed a carbon dioxide incubator for enhancing the growth of Mycobacterium tuberculosis at a cost of Rs. 30, 000 in contrast to the Rs. 4,00,000 that we would have shelled out for a commercially available model. Our incubator works just as well as commercial models, increasing the yield of positive cultures on egg-based media by 30%, a rate that is equal to that quoted in a study published by the National Tuberculosis Institute, Bangalore in the Indian Journal of Tuberculosis. d) We use methanol instead of the recommended ethanol for making carbol fuchsin and 3% acid-alcohol for use in the Ziehl-Neelsen technique. It costs significantly less and works just as well. A point of caution about ‘in-house’ technology: take it through a ruthless process of validation before introducing it for daily use. Not all the technology that we tried to adapt or innovate has worked quite as well. 8) Any person possessed of a reasonable level of intelligence, dexterity and enthusiasm can be trained to become a laboratory technician. It takes years of daily interaction at the laboratory bench for this process of lateral diffusion to occur but it finally happens.

mfc bulletin/April-July 2006

5

Surgical Care for Rural India – A Perspective - George Mathew1 The industry, media, government and even academic surgical professionals highlight and crusade the advantage of adopting high-tech innovations, intervention and instrumentation in surgery. These can be inferred from the high profile given by the media for transplants, prosthetic replacements and high precision operative instrumentation. Although these are laudable aspects of the practice of surgery, it is debatable how many of this high-tech, high cost and highly publicized interventions reflect the true level of surgical care and its cost effectiveness in rural India. Approximately only 20% of the surgical workforce live and work in rural India where there is 70% of our population whose surgical problems need to be addressed. I do not need to reiterate that we still do not have the basic infrastructure of power, sanitation, roads and protected water supply in many of our villages. Although, I am an academic surgeon with more than 20 years of involvement in an academic atmosphere, I am acutely aware of the problems that face surgeons who work in remote and rural India. I have been privileged to start my career in a rural setting and continue to have association with surgical colleagues who are in many instances, the last bastion of the poor and marginalized, working in rural India. There is merit in doing these tests in sequence and do the second one only if the first one is negative. The challenge for the rural surgeon today is to adapt, improvise and to innovate technologies available to make them relevant to the majority of the Indian population. The answer to the above problem is a much larger issue involving medical education, government policy and the mindset of the medical community at large. I feel the answer lies in adopting appropriate technologies rather than copying the industry driven technologies and interventions blindly. The debate should continue, however it is beyond my brief of this presentation. I wish to highlight three interventions in surgery as an example of adapting appropriate technology in the care of our population in the rural areas. In the first instance, I would like to highlight the area of hernia repair. Hernia is one of the commonest problems encountered in any surgical practice, be it rural or urban. It is now widely accepted that hernia repair using prosthetic material (Vicryl mesh, Prolene mesh, etc.) is superior to any other method of hernia repair. Industry and the academic surgical community has embraced this technology without counting the cost. A branded hernia mesh costs anywhere between Rs.800/- and Rs.1000/- which many of our rural population cannot afford. However, rural surgeons have used mosquito net cloth which is made up of similar synthetic material as compared to what is marketed for the hernia by the industry. Initial studies with the use of this net cloth, which costs approximately 45 paise, only a fraction of the mesh marketed by the industry, has shown that it is equally 1

George Mathew, MS, MD, FCAMS, Professor & Head, Department of Gastrointestinal Surgery, Christian Medical College, Vellore 632 004. <sur3@cmcvellore.ac.in>

effective. The academic community still looks at this low cost appropriate technology with suspicion in spite of the results which so far have shown that the complications or recurrences following this low cost repair is no inferior to the repair using branded prosthetic mesh. Laparoscopy is here to stay. Laparoscopy offers the advantage of short duration of hospital stay and early return of patients to normal activities. However, the use of carbon dioxide has made it expensive due to the transport and difficulty of supplying gas in rural areas. The use of atmospheric air and performing laparoscopy without gas (using instruments to lift the abdominal wall) has brought down the cost of this procedure considerably. Again the academic community looks at this with suspicion although many surgeons have demonstrated that this is eminently feasible with considerable cost saving. I know of many surgeons in the rural India who have innovatively used cystoscope, sigmoidoscope and instruments which are locally made for laparoscopic procedures with considerable cost benefit to patients in our rural India. Nutrition is a crucial factor in the recovery and healing of wounds following surgery. The industry has been in the forefront of popularising and driving the surgical community to institute parenteral nutrition in surgical practice. It is now well demonstrated that feeding in the postoperative or preoperative period can be instituted enterally through nasogastric tube, nasojejunal tube or a feeding jejunostomy whenever indicated. The institution of enteral feeding has been found to be of immense benefit to the patients immunologically, nutritionally and financially. It costs approximately Rs.2500/- to Rs.3000/- to maintain adequate nutrition parenterally for a patient per day. However, this is associated with complications of sepsis and requires insertion of specialised catheters as well as dedicated nursing care. Enteral nutrition preparations can be even made at home and for a patient to be adequately supported nutritionally through enteral feeding would cost approximately Rs.150/- to Rs.200/ - per day. This is an appropriate technology which can be adopted in any of the rural centers. I could continue to give information and examples of adopting such appropriate technologies as compared to what is sensationalised by the industry and the media. Lack of Continuing Medical Education is a factor in the feeling of isolation among surgeons working in remote and rural areas. With the advent of internet and widespread availability of telecommunications, it is now made possible for surgeons in rural India to keep abreast of the recent advances in surgery and instrumentation with cost effective investments in computers and internet access. I would like to close by emphasizing that it is not the blind copying of industry driven technology that would be the answer to the quality care of our fellow Indians living in rural India rather it would be innovation, improvisation and adaptation of appropriate technology. We should endeavour to ensure the availability of high standard of health care rather than high-tech, high cost to every Indian which is his/her fundamental right.

6

mfc bulletin/April-July 2006

Excessive Use of Screening and Diagnostic Tests - Anant Phadke1 Screening test is used to screen a population-group to detect a particular disease in its asymptomatic stage; for example diabetes, Ischaemic 2 Heart Disease (IHD), etc. (Screening test need not necessarily be a laboratory test. Simple blood pressure measurement in forty-plus population is also a screening test.) The purpose of the screening test is quite different from that of the diagnostic test. The screening test is done in a group of apparently well people to identify those at increased risk of disease whereas so that any corrective intervention can be done before symptoms appear. The diagnostic test is done in an individual patient who on clinical examination is suspected to have some disease. I would however point out that some of the principles governing the interpretation of the test-results are the same for both these types of tests. Screening test is a valuable tool in Public Health. However, if it is applied to a group in which the prevalence of the suspected disease is low, the screening test tends to become wasteful and misleading. Similarly diagnostic tests used for a particular patient can also be wasteful and misleading if the pre-test possibility of the disease in that patient is low. I would illustrate this well-known phenomenon with some concrete examples and would argue that if these principles are to be consistently applied in practice, this issue needs to be discussed widely by the People’s Health Movement amongst doctors and lay people. This will help to oppose the increasing wasteful expenses on unnecessary investigations due 1

Background Paper for the 29th Annual MFC Meet, 27th-28th January 2006, Vellore. Author’s email: <amol_p@vsnl.com>. 2 Ischaemic = deficient supply of blood to a body part (as the heart or brain) that is due to obstruction of the inflow of arterial blood (as by the narrowing of arteries by spasm or disease). 3 The probability of an event A conditional on another event B is generally different from the probability of B conditional on A. However, there is a definite relationship between the two, and Bayes’ theorem is the statement of that relationship. In particular, Bayes’ theorem helps us to calculate the probability that, given the test was positive, that it is a false positive.

to commercial or consumerists’ pressure. We are all for screening tests and diagnostic tests, provided they are used scientifically, sensibly. Screening Tests An ideal screening test would have 100% sensitivity (ability to detect the abnormality, see the accompanying box on the next page) and 100% specificity (ability to recognize/spare the nondiseased), it would be cheap and would be able to pick up the disease early enough so that effective intervention can be launched in time. Not all laboratory investigations and not all diseases are appropriate for screening. Even in case of a very good screening test (the one with high degree of sensitivity and specificity) the Positive Predictive Value (PPV) of the test (the chance that those with a positive test result actually have the disease) is low if the prevalence of the disease in that population is low. (Bayes’ theorem 3 ). This is seen from the example in the accompanying table. In this table, I have taken the example of stress test, which is now commonly used by some physicians in India as a screening test even amongst low-risk groups. According to various textbooks of medicine, the sensitivity and specificity of stress-ECG for detection of Ischaemic Heart Disease (IHD) is between 55 to 70% and 75 to 95 % respectively. We will take an average figure of 70% sensitivity and 85% specificity. The API (Association of Physicians of India) Textbook of Medicine mentions the pre-test possibility of IHD in the following subsets of the population as follows: § § § §

Asymptomatic adults – 3 to 5% will have this disease Persons with non-anginal pain - 10% will have this disease Persons with atypical angina– 60% will have this disease Persons with typical anginal pain – 95% will have this disease

mfc bulletin/April-July 2006

7

For non-medical readers Sensitivity, Specificity and Positive Predictive Value of a Test For the non-medical readers, the terms – sensitivity, specificity and Positive Predictive Value of a test need to be briefly explained. Any test has two basic attributes – sensitivity and specificity. Sensitivity of a test is its ability to detect the abnormality. For example, x-ray chest is a highly sensitive test, i.e., out of 100 persons who have developed some abnormality in the lungs, x-ray chest can detect this abnormality in most of these patients whereas simple, ‘resting ECG’ (ECG taken while the person is at rest) is said to have a low sensitivity to detect Ischaemic Heart Disease because out of 100 patients who have the Ischaemic Heart Disease, only about 30% would be detected by simple, ECG. Specificity of a test is its ability to distinguish between the affliction with a particular disease and the nondiseased. Thus for example, x-ray chest is said to have a low specificity because its ability to specify whether the lesion is due to tuberculosis or bronchitis or something else, is low; whereas the specificity of peripheral blood smear for malarial parasite is high as a positive test result would certainly mean presence of malaria. The Positive Predictive Value (PPV) of the test is the chance that those with a positive test result actually have the disease. In clinical practice, the doctor is not interested in the mere sensitivity of a test, as s/he does not know beforehand whether the patient has a disease or not (sensitivity being the percentage of people with the disease who have an abnormal test result). Rather the clinician is concerned about the interpretation of positive and negative test results, and therefore about positive or negative predictive values. With this scenario, the Positive Predictive Value of the Stress-ECG in asymptomatic population, as seen from the accompanying table is a mere 12.6 to 19.7%. If the prevalence of IHD in the community is 3%, out of 1000 persons undergoing the test: § § §

21 will be true positives 9 will be false negatives (i.e., labelled as normal though they have the disease) 145 will be false-positives (i.e., labelled as having IHD though they do not have the disease!)

Subsequently, all those with the positive test result will have to undergo a process of ruling out of IHD. This ‘ruling out’ may include the invasive, costly angiography. About 87% of these angiograms would be normal! Alternatively, if the physician does not advise an angiogram and relies on this test result alone, s/he may advise a life long treatment for IHD when in fact the person does not have IHD! This example illustrates the well-known Bayes’ theorem mentioned above, that the Positive Predictive Value of the screening test is low if the prevalence of the disease in the population, i.e., the pre-test possibility is low. It

is thus not enough to know whether a screening test is good (highly sensitive and specific) or otherwise. It is equally important to see which population group it is applied to groups without any symptoms, or those who are symptomatic (typical or otherwise) etc. Given the fact that when the ‘pre-test probability’ of IHD in the general population is low, the utility of stressECG is also low, it should be employed as a screening test only in high-risk groups or in the subset of persons who have suggestive symptoms etc. It may be done for aeroplane pilots, railway drivers etc. provided we are ready to do the follow-up of all the positive results with an angiogram, as 87% of these stress-ECG positive results (obtained in the asymptomatic group) would turn out to be false positive after the angiogram. The Bayesian theorem is given in the standard textbooks of medicine. But in practice these scientific principles are violated under commercial pressures and pressures of consumerism. It may be noted that some physicians follow the sensible tradition (though mostly unconsciously) of ordering diagnostic tests only in individuals in whom the pretest probability of the disease is high. For example, young adults without risk factors are not asked to undergo

8

mfc bulletin/April-July 2006 Positive Predictive Value (PPV) of Stress-ECT with Varying Degree of Prevalence in Subsets of Populations of 1000 each SENSITIVITY 70%

SPECIFICITY 85 %

Prevalence of the Disease

No. of Diseased

True Positives

False Negatives

No. of NonDiseased

True Negatives

False Positives

Positives Predictive Value

a

b

c

d

e

f

g

h

(b x 0.7)

( b-c )

(1000-b)

( e x 0.85)

(e-f)

c/c+g X 100

1%

10

7

3

990

842

149

4.5

3%

30

21

9

970

825

146

12.6

5%

50

35

15

950

808

143

19.7

10%

100

70

30

900

765

135

34.1

20%

200

140

60

800

680

120

53.8

60%

600

420

180

400

340

60

87.5

periodic blood glucose or cholesterol/lipid profile, whereas those with family history or other risk factors are given this advice. But this principle of using screening tests only in case high-risk groups has to be applied consistently. It’s no good to order un-indicated screening test, just because the patient can afford it.

Some other examples of such virtually useless screening tests are - pre-operative ECG and chest x-ray in low risk individuals, serum PSA as a screening test to detect prostate malignancy, mammogram as a screening test in women even below 50 years of age, etc. Diagnostic Test

Ready reckoner tables are necessary which will give sensitivity; specificity and Positive Predictive Value of commonly used screening tests in populations with different levels of prevalence. Doctors and lay People should be educated especially about the increase possibility of false positive test in low-risk population groups. Some of the ‘screening tests’ are basically meaningless. For example, bone densitometry in post-menopausal women though being aggressively pushed, (even in absence of appropriate standardization for Asian, Indian population) has no value. If a 50-year-old woman has marked osteoporosis due to oestrogen deficiency, as seen on bone-densitometry, no safe and effective medication is available. Hormone Replacement Therapy (HRT), which is still being advised by some doctors to such women, is hazardous. Adequate dietary calcium and regular exercise are any way to be advised to all ageing people. Bone densitometry as a screening test has been used to terrify a womn into believing that she would develop fractures and thereby make her ready to take the hazardous HRT. Since HRT itself has been questioned by different studies, bone-densitometry as a screening test should disappear.

Diagnostic test is applied to an individual patient as a supplement to the clinical examination. Diagnostic tests are of course extremely valuable and have contributed so much to revolutionise clinical medicine. However it may not be out of place to make a basic point about the limitations of diagnostic tests. To quote from Harrison’s Principles of Internal Medicine, the globally renowned book on clinical medicine: If the diagnostic test simply duplicates information that has already been obtained by the clinical examination, it will not have any additional benefit for predicting whether or not the disease is present. For example, in trying to determine whether or not a patient with carcinoma of the colon has hepatic metastases, the finding of jaundice on physical examination should be a strong predictor. The degree of hyper bilirubinemia also can be measured, but the bilirubin level in a patient with jaundice does not add substantial independent information to that obtained by a careful physical examination. When integrating a diagnostic test with clinical information, the test in helpful only when it adds incremental information to what can be inferred based on history and physical examination and on prior, less costly or less risky diagnostic test. (14th edition, page 9).

mfc bulletin/April-July 2006

9

Diagnostic tests are done to:

anginal pain this sensitivity increases substantially. Thus whether ECG is a ‘good’ test or not depends greatly upon when it is used. A test results by itself contributes little to the diagnosis but is useful only when it is integrated with the pre test probability, which is to be assessed with history and physical examination.

• • • • • •

Establish diagnosis Rule out dangerous disease Decide the choice of drugs/therapy (culture sensitivity tests, biopsy, etc.) Assess prognosis Assess the impact of therapy (follow-up sputum microscopy in TB or liver function test / WBC count in case of certain medicines) Monitor the long term treatment (periodic blood sugar in diabetics)

But some clinicians tend to order much beyond these classical indications for commercial reasons. Some do so to play it safe. I would argue that the same principle of pre-test probability should be applied to clinical situations also, consistently. There is a healthy tradition of using this principle, though this is generally done somewhat unconsciously. Thus Blood Glucose is advised not in every case of fungal infection, but when it is not responding to routine anti-fungal agents. In such non-responsive conditions, the pre-test possibility of detecting diabetes is reasonably good and hence blood-sugar to ‘rule out’ diabetes in such situation is justified. What is needed is the consistent application of this consideration of pre-test probability and in evidence based manner. Evidence based approach is especially needed when lab tests are used to rule out various rare causes when clinical situation does not warrant this. For example, is it scientifically justified to advise HIV test in every case of TB, just because some cases (about 12%) of HIV disease present as TB-cases? Some non-commercial clinicians tend to use ‘non-invasive’ tests more liberally to rule out ‘dangerous causes’, irrespective of the pre-test probability, and unmindful of the problems created by false positive results. (Some other examples of such unnecessary testing are - serological tests for diagnosing TB, rheumatoid factor and uric acid levels in all joint pains, CT scan in lowrisk headache, MRI scan in every aching back, etc.) It should be emphasized that what matters for the clinician is not the sensitivity or specificity of the test alone but it’s the predictive value. Predictive values depend a great deal on prevalence. Unfortunately many clinicians do not realize this point. Even the sensitivity of the test may vary according to the clinical situation. For example, the sensitivity of the ECG depends upon in which clinical situation it is applied. Resting ECG amongst asymptomatics has a very low sensitivity to detect Ischaemic Heart Disease. But in presence of

No Value in Clinical Practice? Some clinicians feel that “all this statistics has no value in clinical practice as we are dealing with a individual patient.” The argument outlined above shows that even in case of individual patient, clinicians do unconsciously use pre-test probability while ordering investigations. Let us see what Harrison’s Principles of Internal Medicine (15th edition) has to say about these quantitative aspects of clinical assessment. It has a section, “Quantitative Aspects of Clinical Reasoning”, in which the author has argued for a more rigorous and consistent quantitative approach in clinical situations: Several quantitative tools may be invaluable in synthesizing the available information, including diagnostic tests, Bayes’ theorem, and multivariate statistical models … More complex clinical problems can be approached with multivariate statistical models, which generate highly accurate information even when multiple factors are acting individually or together to affect disease risk, progression, or response to treatment. Studies comparing the performance of statistical models with that of expert clinicians have documented equivalent accuracy, although the models tend to be more consistent. These multivariate statistical models may be particularly helpful to less experienced clinicians …. For the foreseeable future, excellent clinical reasoning skills and experience supplemented by well-designed quantitative tools and a keen appreciation for individual patient preferences will continue to be of paramount importance in the professional life of medical practitioners. This recommendation by this renowned textbook should take care of the argument that “in clinical practice, these statistics are of no value.” If we continue to waste so much money on unnecessary investigations, it would not be possible to achieve the goal of accessibility of secondary and tertiary care also to all the citizens. Hence the People’s Health Movement should question this overuse on scientific grounds. (I am indebted to Anurag Bhargava and S P Kalantri for their comments and suggestions on the draft of this article. The usual disclaimer remains.)

10

mfc bulletin/April-July 2006

Are Glass Syringes Inferior? - Jan Swasthya Sahyog, Bilaspur1 Introduction Injections are an important tool at all levels of healthcare. One of the reasons for their popularity is the quick relief that they provide compared to other drug delivery methods. However injections not only increase the cost of care but are also capable of transmitting infections bypassing the cutaneous and mucosal defence mechanisms. They also undermine the benefits that other forms of drug delivery may provide. Traditional glass syringes, which can be sterilised at a low cost, have been given up in many healthcare facilities because of concerns that include the ‘unreliability’ of ‘in-house’ sterilisation practices with the attendant risk of transmission of blood-borne infections. In addition, the breakage of glass syringes as well as the time and labour involved in cleaning, drying, packaging and sterilising them possibly discourage healthcare managers. It has even been claimed that the overall cost of using glass syringes is higher than that of using pre-packaged, sterile, disposable plastic syringes. The increasing acceptability of “disposables” in all other walks of life may have facilitated this trend. Unfortunately, all of this has happened without documented proof of the superiority of disposable plastic syringes either of the regular or of the ‘autodestruct’ kind. Plastic syringes, however, are not the answer to all problems. Practitioners who were using glass syringes in an unsafe way now reuse disposable plastic syringes in the same unsafe way. There have been reports of used ‘disposable’ syringes masquerading as ‘sterile’ after being recycled, washed with water and repacked. PVC syringes release carcinogenic dioxins on burning.

Physically intact plastic syringes are also liable to be pilfered from landfills for unauthorised recycling by unscrupulous elements. This makes it difficult to dispose them off satisfactorily without using shredding machines. Shredding machines, because they are expensive and energy-intensive, are not viable options for small healthcare set-ups. At Jan Swasthya Sahyog,2 we have looked again at these concerns. We present our experience of using glass syringes over the past four years in a busy community health programme and a brief comparison of the cost of glass vs. plastic syringes. Syringe Use at Jan Swasthya Sahyog As an ideological stand we use injectables only when essential. Syringes are used in the ward, outpatient clinic, operation theatres and laboratory for giving parenteral drugs and for drawing samples of blood and other body fluids. Blood samples for blood culture and platelet count are drawn with disposable plastic syringes. Glass syringes with disposable needles are used for all other diagnostic and therapeutic purposes. We maintain a stock of disposable syringes for emergencies such as prolonged power cuts, breakdown of hot air oven, washing staff going on leave, etc. On an average we use 800 plastic syringes a year. This amounts to 2 - 3 syringes per working day and accounts for about 2% of all syringes used. Flow Chart of Glass Syringe use at Jan Swasthya Sahyog Used syringes discarded in 3% lysol

1

<Janswasthya@gmail.com> We are a voluntary organisation of health professionals running a low-cost, people-oriented, community-based health programme in rural Bilaspur in Chhattisgarh. To this end, we run an outpatient service at our referral centre with a general clinic catering to 225 patients thrice every week and a tuberculosis clinic once a week. The mobile outreach service goes to three different villages every week. These villages are between 25 to 60 km away from our referral centre and 40 to 80 patients attend each of the clinics that are held there. The inpatient service at JSS consists of a 15-bed general ward and some observation beds. Inpatient admissions include a large number of post-operative patients along with patients with medical problems. The OPD clinic, the ward and the outreach service are supported by a laboratory that performs 40 different kinds of tests including culture and antimicrobial sensitivity testing for common aerobic bacteria. 2

Syringes removed from lysol after 12 - 24 hours

Syringes rinsed thrice in tap water

Syringes soaked in enzyme-containing detergent (e.g., Henko, Surf Excel) for one hour

Syringes scrubbed with brush and washed in tap water by blowing tap water through the barrel

Syringes rinsed six times in tap water and once in

mfc bulletin/April-July 2006

11

distilled water (made in solar distillation plant)

Syringes dried in hot air oven at 70ºC. Barrels and pistons are fitted together to ensure matching, then taken apart and wrapped in pairs in old newspaper.

Syringes sterilised in a hot air oven with circulating fan at 170ºC for two hours. Load allowed to come down to room temperature before opening oven door to reduce breakage. (Newsprint acquires a faint brown colour and becomes stiff at the end of the sterilisation process. These changes constitute visible proof of the fact that the syringe inside has gone through a high temperature for a significant length of time and provides an additional level of assurance to the user. If the paper becomes too dark and brittle or if it actually cracks, the syringe inside is taken out, repackaged and sterilised the next day)

One syringe taken every week and put in 100 ml of Brain Heart Infusion Broth and incubated at 37ºC for 24 hours for sterility check. The number of different kinds of glass syringes used in all work areas in the 12-month period from August 2004 to July 2005 were as follows: Syringe size

Total number used per year

Average per week

% of total

2 ml

20147

387

45%

5 ml

17909

344

40%

10 ml

6716

129

15%

Total

44772

860

Syringes are used in the OPD clinic for outpatient procedures such as pleural and peritoneal taps, as well as for benzathine penicillin prophylaxis for patients with rheumatic heart disease and for injecting antibiotics such as aminoglycosides and cCeftriaxone that can be given once daily on an outpatient basis. In this kind of a situation, the syringe use in the outpatient service was 5.25 per 100 patient-days. Since admissions are made only for very sick patients, the number of syringes used per 100 patient-days in the inpatient ward was much higher at 500 per 100 patient-days. The number of syringes used in the operation theatre was lower than the average for the ward because the majority of operations are of only 1 to 3 hours’ duration. The average number of syringes used in the laboratory per 100 patient-days is less than 100 because some patients have tests done only on samples such as urine, stool, sputum or vaginal fluid that do not require a syringe for collection. The breakage rate of glass syringes was about two per day or about 1.6%. 707 glass syringes were broken during the 12-month study period, mainly in the wards during disposal into the 3% lysol-filled container after use. Breakage was negligible in the laboratory. Roughly, 2-ml syringes accounted for 50%, 5-ml syringes for 35% and 10-ml syringes for 15% of all syringes broken. Cost Analysis We compared the recurring cost of using disposable syringes and needles with that of glass syringes and disposable needles, the latter being routine practice in our organisation for the past four years. In this analysis, the cost of disposable syringes has been calculated according to wholesale rates prevailing in Bilaspur town and the cost of pre-disposal autoclaving has been

Number of Syringes of all Kinds used in Different Work Areas Work area

Average number of syringes consumed per week

Patient-days per week

Number of syringes used per 100 patient-days

Outpatient clinic

42

800

5.25

Inpatient ward

420

84

500

Laboratory

294

375

Operation theatre

105

50

78.4 200

Total patients in all work areas in one year: 33895 (14427 new and 19468 old)

Total syringes used in one year: 44772

Average number of syringes used per patient in the entire programme: 1.34

12

mfc bulletin/April-July 2006

factored in. For glass syringes, the analysis has taken into consideration the cost of the following components 1. 2.

Replacement of broken syringes Lysol to render the syringes before washing

3. 4. 5. 6. 7.

Water and detergent for washing Old newspaper for packaging Electricity for heat sterilisation Manpower Disposable needles

Annual Recurring Expenditure if We Depended Exclusively on Disposable Syringes Price of disposable syringes: Syringe size

Price in wholesale market (INR)

No. of syringes required per year

Cost per year (INR)

2 ml

1.5 per piece

20,147

30,221

5 ml

1.9 per piece

17,909

34,027

10 ml

3.5 per piece

6,716

23,506

Total Cost of autoclaving used syringes prior to disposal: Running a 1kVA autoclave consuming 1 unit per hour @ Rs. 4.50 per unit for one hour thrice a week, i.e. 156 days a year would cost INR 702

87,754 Conclusion We wish to say that properly sterilised glass syringes are preferable to plastic syringes in healthcare institutions on the following grounds: a.

Total annual recurring cost of disposable syringe strategy at our centre : The total cost of using plastic syringes in our set-up would therefore have been INR 87,754 + INR 702 = INR 88,456. In addition, there would have been the risk of pilferage from the landfill for recycling by unscrupulous elements. Shredding prior to disposal would have eliminated the risk of pilferage but brought in the cost of electricity to run the shredder and the cost of periodic maintenance. If these syringes were to be incinerated, it would have eliminated the cost of pre-disposal autoclaving and shredding but the environmental price of dioxin emission would not have been quantifiable in purely monetary terms. It is clear that in spite of the high rate of breakage there is a net saving of Rupees 16980 per year, even in our small clinical facility. This amounts to about 24 % of the total anticipated expenditure had we depended on disposable syringes alone. If we used autodestruct syringes instead of regular disposable syringes, the difference of cost would have been even higher.

Their sterility is more reliable since sterilisation is done by an accountable organ of the same institute, b. They are clearly cheaper if the volumes are significant, and c. Using glass syringes prevents the generation of avoidable waste. We question the wisdom of the government to go for the more expensive option of autodestruct syringes in the presence of the cheaper and equally reliable option of glass syringes sterilised “in house”. All government healthcare facilities are supposed to be prepared for following universal precautions and all of them offer services such as tubal ligation and vasectomy that require sterilisation procedures to be carried out within the premises. Washing and sterilisation of syringes can easily be incorporated in such settings if desired. Playing down the ability of healthcare facilities to wash and sterilise glass syringes with complete reliance on disposable syringes reflects an attitude prevalent amongst planners that workers in small places or in developing countries can not be trusted to carry out technically demanding procedures. Such

mfc bulletin/April-July 2006

13

Total Annual Recurring Costs if We Depended Exclusively on Disposable Syringes Item

Unit cost

Cost of replacing broken syringes

2 ml syringes, 338 pieces @ Rs. 20 / piece = Rs. 6,760 5 ml syringes, 267 pieces @ Rs. 27 / piece = Rs. 6,399 10 ml syringes, 102 pieces @ Rs. 37 / piece = Rs. 3,774

16,933

Lysol for discarding syringes after use

14 litres of 3% lysol every week for discarding syringes. 14 litres x 52 weeks = 1144 litres of 3% lysol used every year, consuming 35 litres of neat lysol. Thirty-five litres of lysol costs Rs. 6,300 @ Rs. 180 / litre

4,680

Electricity for pumping water for washing syringes

300 litres a day x 6 days every week x 52 weeks a year = 93600 litres a year 1.5 x 93.6 = 140 units of electricity consumed per year @ 1.5 kWh / 1000 litres Cost of 140 units of electricity @ Rs. 4.5 / unit = Rs. 630

630

Cost of detergent

One sachet of Surf Excel/day for 6 days every week for 52 weeks @ Rs. 2 / sachet = Rs. 624

624

Distilled water for rinsing syringes

Made in solar distillation plant with no recurring cost

Nil

Old newspapers for wrapping

3 kg of old newspapers every week x 52 weeks kg

@ Rs. 7 /

1,092

Electricity for sterilisation in hot air oven

0.46 kWh x 2.5 hours a day x 6 days a week x 52 weeks = 359 units of electricity per year Cost of 359 units of electricity @ Rs. 4.5 / unit = Rs. 1,615

1,615

Manpower

Rs 1400 per month for 12 months

16,800

Disposable needles

Rs 0.65 per needle

29,102

Total Cost Rs.

Cost per year (INR)

71,476

an attitude is not conducive to capacity development and empowerment. Lastly, even if the country were to be flooded with disposable syringes, it would not solve the problems of sciatic nerve injury, injection abscesses and inadequate dosing at the hands of poorly trained medical practitioners on whom the poor and the under-privileged have to depend most of the time.

technique of administration of the injection and the disposal of the syringe (and preventing reuse without sterilizing, after one use) are all equally important steps that determine that an injection is safe. Thus in order to ensure safe injections for all, we need to ensure the training, supervision and the accountability of the system. If these can be ensured, then these would also be adequate to ensure proper sterilization of the glass syringes too.

Merely hoping that the replacement of a glass syringe by a disposable “hopefully sterilized” plastic syringe will ensure a safe injection is incorrect. The handling of the sterile syringe, the cleaning process and the

We wish to reaffirm the faith that so many small and medium sized health care set ups that work for the disadvantaged have in the use of glass syringes by this small analysis.

14

mfc bulletin/April-July 2006

The Quality and Cost of Health Care: Some Notes on the Context This meeting marks a conjuncture that is personally very important for me. To have an MFC meeting in Vellore seems to me like a dream coming true, one that I dreamed along time ago, and I am particularly grateful to Anand, Sara and Ritu for making it happen. But at the same time both the national and indeed, the international context of our meeting, as well as the topic of our coming deliberations are such that they subject both these institutions to major scrutiny. India is, today an important member of the imperialist world system under the political, economic and military leadership of the United States of America. The health care system, both in their public and private sectors, constitutes one of the most important theatres of operation of this imperialist world system. At the same time the poor and working people throughout the country are being subjected to a process of expropriation of their human rights and common property resources of unprecedented and ferocious proportions. More than 75,000 families have had their homes destroyed in Bombay,12 people have died in police firing in kalinga nagar in Orissa; 15,000 people have been rendered homeless in Bastar at gunpoint, in the process called ‘Salwa judum’, and these are only a few events of which we have a personal knowledge. The working people of Chattisgarh are being persistently denied their industrial rights in the local courts, and, indeed, in a recent ruling the Supreme Court has told us that daily wageworkers do not qualify as workmen under the Workmen’s Compensation Act. Amartya Sen informs us, in his speech before the India Science Congress that India is half California and half Sub-Saharan Africa – a useful comparison, but the proportions are wrong. Meanwhile the National Nutrition Monitoring Bureau tells us that 34% of adults in India have a BMI of below 18.5 (including 50% of STs and 60%of SCs), to a blithe counterpoint from the conomists that poverty rates have come down to 26%. What does all this have to do with us in this meeting? We are grateful to George W Bush for putting the matter simply and clearly when he said that if you are not with us then you are against us. Turning that argument around on its head, we say that if you are not against them then you are with them. The first thing we health professionals need to realise is that questions regarding our political posture can no longer be pushed away to the periphery of our field of vision, but have now willynilly come to occupy centre stage. In particular, 1

Email: <binayaksen@gmail.com>

-Binayak Sen1 considerations of equity and justice can no longer be sequestered safely with in boundaries of the Community Health department, but instead, have to be brought into research protocols, departmental work plans and financial investment decisions. It is especially important, when we talk of quality and cost of care, that these discussions do not take place in a moral and ideological vacuum, but that the equity and access and entitlement parameters of the discussion should be made transparent, specific and plain. The justifying ideologies of past empires appear to us, on looking back, to be ludicrous and obscene. However, the present ideologies, in which we are all complicit, appear to us to be self evidently true. The unchallenged legitimacy of a commodified technology is one of the unifying principles of the present imperium, and the violence of the global market helps keeps this in its place. It is important to stress that every time one operates a protocol or makes a research decision without reference to issues of equity and access, we further contribute to this violence and to the movement towards an amoral ethos for the health care industry. In this situation, as we proceed to deliberate on the issues of cost and quality of health care, a few questions that, in my opinion, need to be examined are as follows: l

How do IPR regimes impact on the quality and cost of health care? What practical alternative health care regimes are possible today?

l

What are the possible juridical and institutional mechanisms that would allow alternative quality and cost of care norms to achieve mandatory as opposed to recommendatory status?

l

How could one construct diagnostic and therapeutic algorithms that would impact favorably on equity and access in quality as well as cost of care?

l

How could one democratise social and technical relations within the healthcare profession? Is it possible to ensure equity and improve access without such a democratisation? Could such a democratisation be extended to the doctorpatient (or health care system-user) relationship? Is litigation under the Consumer Protection Act the only or even the most desirable way forward?

mfc bulletin/April-July 2006

15

Quality and Costs of Health Care in the Context of the Goal of Universal Access Protocols and Appropriate Referrals This paper seeks to describe the experience of the Low Cost Effective Care Unit of the Christian Medical College (LCECU) to set up a low cost but good quality obstetric service for the town of Vellore. The LCECU was set up in the 1980’s as a general practice unit to serve the needs of the town of Vellore. It did not however provide obstetric services. In the 2004 it was decided that this need should be addressed. Pregnancy and delivery as far as possible should be considered a normal event. In the middle of the last century and even late into the 1980’s over 80% of deliveries were conducted at home in the rural areas of India and close to 50% in the urban areas. However it is well documented that complications do occur during pregnancy, delivery and the postnatal period that lead to high mortality and morbidity. Moreover, many of these complications are preventable. With the availability of technology and more sophisticated methods of care and high tech medicine, a number of options are available to a pregnant mother for care. The high tech medicine had added cost and a normal delivery is expensive, especially in private care. There have been questions raised about the unnecessary use of these technologies and the overuse of caesarean section in obstetric care. Even today 80% of all pregnancies have a normal outcome. The physiological process of pregnancy and delivery has however moved into the care of the professionals and the focus is on pathology. In an attempt to tackle the problems related to the high morbidity and mortality associated with the process, institutional deliveries are encouraged. The process that was normal and took place within the home environment has today moved into the sphere of the medical institution. Issues that arise include: An unacceptably high morbidity and mortality associated with the process of pregnancy and birth, most of the causes being preventable. In addressing this issue, the programmes have tended to be professional driven and top down. Technology is used without discrimination, making the process expensive when used unnecessarily. This occurs during the antenatal period as well as during labour. The high cost means that services may not be available to those who do need it and cannot afford to pay. 1

Low Cost Effective Care Unit, Christian Medical College, Vellore. Email: <s_bhattacharji@cmcvellore.ac.in>

-Sara Bhattacharji1 While the process of pregnancy and delivery if normal should be something that the person and the family are involved in, with the positive cultural and family support mechanisms present, the overuse of technology makes this frightening and unacceptable for many women. This is especially true during the process of labour and delivery. In this context, how does a secondary centre that is working to be cost effective, plan and provide a good quality, but low cost service? In the LCECU, we have followed the principles of using protocols and appropriate referral, use of less trained personnel, use of education and exercise. The antenatal clinic and the labour room in the LCECU are run by the nurses using well-defined protocols. The doctors provide referral cover. This means that the costs for providing care come down. As patients are seen, they are encouraged to feel they are in charge of the process and a part of the team. The stress in the clinic, is for the women to feel that this is a normal process. Health education helps to provide information that helps them to feel in charge of the process. It helps them to take simple measures at home with regard to their diet, activity, preparation for labour and the care of the newborn that are most appropriate for them. During the antenatal clinic, exercises are taught and the women are encouraged to do them regularly at home. These measures help to reduce complications and keep the costs down. Screening using protocols allows the team to categorise each mother according to their ‘risk’ status and refer as needed to the tertiary care centre. By picking up the problems early, complications can be prevented, thus keeping costs low. At each stage, every effort is made to explain to the person what is being done and why. The labour room in the LCECU currently takes only normal deliveries that are conducted by the nurses. The staff requirements are kept to a minimum. The labour room itself is simple and has only the necessary basic equipment. This ensures that overhead expenses are kept as low as possible. How is quality assured under these circumstances? The protocols ensure the basic levels of quality. For obstetric care in the LCECU, there are well written protocols for each anticipated event. Audit of the deliveries against the protocols is done on a regular basis. The consultant in the tertiary care centre audits referrals. This will help in modifying the protocols as needed. Since this system is still in its infancy, it is too early to make an evaluation. The patient feedback has so far been positive.

16

mfc bulletin/April-July 2006

Cost Containment in Trauma Care Services- Limited Experience - Jacob John1 I am working as a neurosurgeon in a community hospital in Kollam. The main bulk of my work is trauma including neurotrauma, soft tissue injuries and critical care. Protocol based practice and restricted use of antibiotics and drugs have brought down the cost drastically. Ours is a small unit and hence our findings and observations need further validation. But definitely these are pointers. The following policies have made the impact. 1. Soft Tissue Injuries For traumatic soft tissue injuries including contaminated wounds, no antibiotics are used as a policy. Wounds are cleaned with soap and water. Contaminated wounds may be kept for delayed primary repair. Suture material used is autoclaved black silk. Dressings are avoided if possible. After 12 hrs anyway, wounds are kept open and patients are given bath. Paracetamol is the only drug that is usually given. Not even a single case of wound infection has occurred during the last 4 yrs. 2. Surgical Procedures (craniotomy included) For craniotomies, tube thoracostomies (surgical creation of an opening, stoma, into the chest cavity for drainage), spinal surgery, compound depressed fractures, and skull base fractures – no antibiotics are given. No case of infection has been encountered. 3. Critical Care Patients on mechanical ventilatory support including post operative cases are off antibiotics initially. Invariably, they develop ventilator associated pneumonia in 4 to 5 days. Protocol CxR (chest x-ray) and culture of ETT/tracheostomy secretions. Pending culture report, if CxR shows pneumonia or patient develops hypoxia (clinical manifestation of respiratory distress consisting of a relatively complete absence of oxygen), ciprofloxacin 500mg 12 hourly is started. In the beginning, we were treating with third generation cephalosporins and aminoglycosides. C/S revealed the organisms to be sensitive to ciprofloxacin. A peculiar finding was its resistance to cephalosporins and aminoglycosides supposed to be the drugs of choice in ventilator associatedp. Isolates are klebsiella or pseudomonas. They are all sensitive to ciprofloxacin. We have successfully treated all ventilator associated pneumonia with this cheap single drug regime for 7 days. No failures have occurred. The number of cases treated so far is about 70. 4. Training All our nurses have been trained in the following procedures. l 1

Early assessment and management of the traumatized

Dr.Jacob John MS,M.Ch,Dpt Of Neurosurgery & Trauma Care Services,Bishop Benziger Hospital,Kollam. < jacobjns@yahoo.com>

patient. l Central venous canulation. (the insertion of a cannula or tube into a hollow body organ) l Endotracheal intubation. (the placement of a tube into the trachea (windpipe) in order to maintain an open airway in patients who are unconscious or unable to breathe on their own. Oxygen, anesthetics, or other gaseous medications can be delivered through the tube.) l External Ventricular drainage using Jamshidi Bone Marrow Biopsy Needle. (The brain and spinal cord are surrounded by cerebro-spinal fluid (CSF), which helps to protect them from damage. The areas in the brain which contain this fluid are called ventricles. Sometimes CSF needs to be drained away from the ventricles. External ventricular drainage (EVD) is a temporary method of doing this.) This has saved many a life. Nurses can be trained to do many life saving procedures. This becomes very useful in small units like ours where we don’t have a full time resident. 5. Training of Junior Doctors Junior Doctors have been trained to do bed side tracheotomy, cricothyroidotomy, tube thoracostomy, emergency burr hole in addition to the above procedures. [Cricothyroidotomy is a surgical procedure in which a surgeon or other trained person cuts a hole through a membrane in the patient’s neck into the windpipe in order to allow air into the lungs. Cricothyroidotomy is a subtype of surgical procedure known as a tracheotomy; a surgical procedure that opens up the windpipe (trachea). Tube thoracostomy, is a procedure by which a tube is inserted into the chest cavity in order to evacuate air or fluid. A burr hole is a hole that is produced in a surgical procedure: a special drill, with a rounded tip is used to carefully drill through the skull, until the brain is exposed.] 6. Use of Protocols Use of Mannitol, CT Scans, other drugs, dressings, hospital stay, etc., are all restricted and guided by protocols. Extradural haematoma (buildup of blood occurring between the dura mater, the brain’s tough outer membrane, and the skull), chronic subdural haematoma, depressed fractures, spine, are all discharged after 48 hrs if they are conscious. This has helped us to cut the costs at the same time enhancing quality and outcomes. We have also worked out that cost can be reduced by about 50% if protocols are adhered to. Hence volume can be increased. This is applicable in all specialties. Now there are many drs sharing these experiences and we have all come together to put this into practice in a collective way. We are working this out. Comments are solicited. Dated 22 January 2006.

mfc bulletin/April-July 2006

17

Access to Health Care for All: What can we learn from the AIDS Movement? -Anand Zachariah1 Introduction Last year one million people across the world in low and middle income countries received free ART drugs through government programmes as part of the WHO’s “3 x 5 initiative”. This programme aims to provide universal access to HIV care. In India about 30,000 people are receiving HIV treatment through the government ART programme and it is estimated that the government has spent $ 85 million over the last year. The development of this programme is largely due to movements of people across the world that has made AIDS a political issue, which governments, international agencies and pharmaceuticals cannot ignore. The question is not whether AIDS has more priority than other disease problems and public health issues, such as water, food, housing and sanitation. It is obvious that AIDS cannot have greater priority in India. However if good quality and expensive treatment can be made available for one disease, then surely the same can happen for other common diseases which are less expensive. What can we learn from history of AIDS, about how to make a disease important enough politically, to ensure access for all to health care. Development of Anti-Retroviral Treatment and HAART The development of highly active anti-retroviral treatment (HAART) is the story of how understanding of basic biology of the virus has enabled drug development and trials towards successful treatment regimens. The knowledge of critical enzymes in the virus life cycle, reverse transcriptase and protease has enabled the development of drugs that target these enzymes. The first anti-retroviral drug developed in 1987, zidovudine, a nucleoside reverse transcriptase inhibitor (NRTI), was found to temporarily reduce morbidity and mortality. In 1994 it was a shown that a combination of 2 NRTIs was superior to zidovudine, but still of short-lived benefit. In 1995, David Ho 1

Medicine Unit 1 and Infectious Disease, Christian Medical College, Vellore, <zachariah@cmcvellore.ac.in>

suggested that intensive treatment if started early, could eradicate the virus in a few years, with the slogan, “hit early hit hard”. In 1997, trials of a combination of two NRTIs with one protease inhibitors (PI) achieved maximal and durable suppression of viral replication. This 3 drug combination became referred to as highly active anti-retroviral therapy (HAART) and became the standard of therapy. In 2001 it was shown that combination therapy of one non-nucleoside reverse transcriptase inhibitors (NNRTI), Nevirapine and Efavirenz with 2 NRTIs was equally efficacious as PI based HAART regimens. Following the advent of HAART therapy in US in 1996, hospital admissions and AIDS deaths immediately declined and AIDS wards were closed down. We now know that for these drugs to be effective, 95% adherence is required and a range of drug toxicities are recognized. These drugs are not curative because of a reservoir of inactive cells which are latently infected with the virus. Resistance to anti-retroviral drugs occurs over time, resulting in treatment failure and necessitating change in regimens. People’s Initiatives in Development of ART Gay community activism in US were crucial in defining research policy, ensuring that trials addressed concerns of the gay community, facilitating drug licensing and also in promoting access of drugs to affected people. Committees which defined research policy required representatives of affected groups. Research guidelines required that positive peoples groups were involved in trial planning, execution and dissemination. Concepts such as compassionate use, expanded access, accelerated access and parallel track came into use in drug trials research as a result of AIDS activism. Peoples’ groups negotiated directly with companies to promote their research priorities and with FDA to facilitate drug licensing. Scientific writings today do not acknowledge the crucial role of gay community activism in ensuring the development of effective treatment and enabling access of anti-retroviral treatment for all who needed it in the

18 United states. From the development of HAART till about 2000, the cost of drug treatment was about Rs. 40,000 per month. Outside the western world, only the wealthiest had access to such treatment. The governments and people of Brazil and Thailand and Indian pharmaceuticals were crucial in changing this status quo. Brazilian Experience Brazil’s freedom struggle from dictator rule, the formation of the democratic constitution in 1988 and the first election in 1990 were closely linked to the AIDS issue. The first cases of AIDS were detected in 1983, and health officials and affected people in Sao Paulo (which was the epicenter of the epidemic) demanded for health rights of affected people and to make treatments available. One of the groups involved in the political struggle against dictatorship was “sanitary reform movement”, a loose affiliation of health workers, political parties, trade unions, academics and churches. They demanded a health system that was responsive to the people and this same group was actively involved in the early response to AIDS epidemic in Sao Paulo. When the opposition won the election in 1990, members of the sanitary reform movement became senior health officials and were involved in the formation of the “National Unitary health system” based on the Sao Paulo model, with AIDS care as an important component. The Brazilian National AIDS programme emphasized linkage of care and prevention. Just as they had struggled together for democracy, they emphasized the need to openly address the stigma of AIDS and ensure the rights of affected people. A multifaceted care programme, one aspect of which was anti-retroviral treatment, was put into place. AZT became available from 1991 and HAART therapy in the late 90’s. Antiretroviral treatment was provided not just through the government but at all points where people accessed the health care system. Training of health personnel and development of appropriate laboratory monitoring infrastructure were simultaneously addressed. In 2004 it is estimated that 154,000 people were receiving AIDS care at a health cost of US $ 426 million of which 80% is spent on treatment. This cost has not increased over the 6 years of its functioning. The efficacy of the AIDS programme has been shown by the falling incidence of

mfc bulletin/April-July 2006 new infections, 50% reduction in AIDS mortality and 70% reduction in in-hospital admission days Crucial to these have been the legal demand of access to health care as a basic right under the new constitution. The constitutional provision for health care as a basic right for each citizen, was used by the affected people, NGOs and health department to fight legal cases for better provision of ART. Brazil has focused on development of its local pharmaceutical industry which manufactures seven anti-retrorvirals and provides about 18% of the country’s requirement. In 1971 the country enacted a law which provided the right for manufacture of patented drugs. When the law had to be revised to recognize the international patent regimes, they introduced a legal provision to enable local generic drug manufacture, if the patented drug was not manufactured within Brazil within 3 years of patent issue. Brazil later issued compulsory licenses for nelfinavir (produced by Roche), lopinavir/ritonavir (Abott) and efavirenz (Merk) and then gave notice to these companies to reduce costs of these drugs. Thereby they forced prize reductions. Today Brazil is cited to have as a model ART programme, for other countries to follow. Experience of Thailand in Providing Anti-Retroviral Therapy The other non-western country which set up an early ART programmes was Thailand. The Royal Thailand government in response to the rapidly evolving HIV epidemic set in place model prevention programmes which reduced the incidence of the disease in the 1990’s. After the early published reports of the effectiveness of AZT in preventing mother to child transmission (PMTCT), a public donation campaign was started in 1996 which cut mother to child transmission to 5%. This was the first demonstration of a successful PMTCT intervention in a non-trial setting. In 1999, Thailand published the effectiveness of a shortened regimen of AZT in the last trimester of pregnancy. Soon after, the Thailand government started implementing operational trials with this regimen and universal access to PMTCT became available from 2002. Based on the Thailand model, PMTCT is recognized as an essential and costeffective component of national HIV prevention programmes. Thailand universities along with Thai Red Cross, university research groups in Australia and

mfc bulletin/April-July 2006

19

Netherlands (NAT collaboration), have been doing research trials on implementation of ART programmes from the late 90’s, developing local expertise and models of relevant practice. In response to the public demonstrations from affected people and NGOs, in 2001, a policy of universal access to ART was announced. This has been gradually scaled up from treatment available to 3000 people in 2002, to treatment provision to 50,000 in 2004. The Thai programme is largely funded by the Royal Thai government. Some of the factors that have appeared critical to this support were: (a) the international recognition of effective prevention and PMTCT programmes; (b) development of local expertise, infrastructure and relevant models of care; (c) affected people and NGOs lobbying with the government; (d) drastic price cuts from international companies and local manufacture of drugs; (e) more recent contributory support from global fund; (f) holding the AIDS conference in 2004. Durban Conference 2000 Academics, government representatives, affected people, NGOs and international agencies met together in Durban at the international AIDS conference, in the

context of the looming epidemic in Africa. The conference occurred in the background of the experiences of countries such as Brazil and Thailand which had shown that large scale successful and selffunded ART programmes could be implemented. In an act which indicated the mood of that moment, South African High Court Justice Edwin Cameroon described his own story of developing AIDS at the conference: “Amidst the poverty of Africa I stand before you because I am able to purchase health and vigour. I am here because I can pay for life itself.” The speech crystallized sentiments in favour of providing treatment to countries who could not then access care, and the need to cut drug prices. A consensus formed at the Durban conference, that if prevention was to succeed it must be integrated with care, and treatment must become available to all who need it. There was rejection of the thinking at that time, that treatment should be available only to people and countries who could afford it, and that the poor and the developing world must only focus on prevention. International funding must be found to support drug treatment and companies should be forced to reduce the cost of drugs. Role of Indian Pharmaceutical Industry Soon after this Brazil offered to export medicines to

Figure 1: The Effects of Generic Competition - Sample AIDS Triple Combination: Lowest World Prices per Patient per Year (Stavudine (d4T) + Lamivudine (3TC) + Nevirapine)

12000 Proprietary $10439

10000

Proprietary Generic

6000 Brazil $2767 Proprietary $931

2000 Cipla $800

Jun e Jul y Au gu st Se pt. Otc .

M ay 20 00

0

Source: Perez-Casas et al 2001

Proprietary $727

Cipla $350

Hetero

Fe b. Ma rch Ap ril Au gu st

4000

No v. D Jan ec. .2 001

US $

8000

Ranbaxy $295

20 Africa and transfer technology for local drug manufacture. In response to this five major pharmaceutical industries offered reduced prices to sub-Saharan African countries. In March 2001, the Indian firm Cipla offered to sell three antiretroviral drugs at US$350 per patient per year. The companies responded by further prize cuts (see figure 1 below). This 40 fold reduction of drug costs has been because of wars between between proprietary manufacturers in western countries and generic manufacturers in Brazil and India who have used patent laws strategically to their advantage. United National General Assembly, Global Fund and 3 by 5 Initiative The Durban conference was a turning moment. In response to this, Kofi Annan provided a vision for taking these ideas forward. At the United Nations General Assembly Special Session (UNGASS) on HIV/ AIDS in 2001, he suggested that a global effort which linked care and prevention must be initiated and that we should work towards providing ART to all people who needed. A global fund should be created to provide universal access to HIV treatment to 3 million people by 2005. All WHO countries supported this proposal at the UNGASS. In December 2003 the WHO and UNAIDS came out with the programme “3 by 5 initiative” which envisaged, use of common and simplified guidelines, reliable supply of medications, better diagnostics, training and national advocacy. This strategy was endorsed by 192 member countries. The WHO soon after, sent missions to individual country’s to lobby for and assess their readiness in setting up local ART programmes. Subsequent to this the WHO has been working with individual countries to set up local ART programmes. As a result of this strategy by June 2005, the number of people on ART has doubled increasing from 400,000 to 1 million. About 14 countries are providing ART to 50% of persons who are in need of it. In Africa about 500,000 people are receiving treatment (3 fold increase), 155,000 in Asia, 20,000 in central Europe and 290,000 in South America. In many areas the demand outstrips that which is being provided. 34 countries are implementing national treatment programmes for ART. Therefore although the programme falls short of its target, it has shown that such a large scale treatment

mfc bulletin/April-July 2006 initiative is achievable, effective and affordable. Indian AIDS Programme On World AIDS Day 2003, the Government of India announced its decision to provide Anti Retroviral Treatment (ART), free of cost to people living with HIV/AIDS in the six HIV high prevalence states of Tamil Nadu, Andhra Pradesh, Karnataka, Maharashtra, Manipur, and Nagaland and the state of Delhi. Since then several other state governments are providing ART from their own resources. 25 larger hospitals are currently involved in ART treatment and many other district level hospitals have been identified. Large scale training is being conducted for personnel involved in this programme. Currently it is estimated that about 30,000 persons are receiving ART on the government programme. The government programme aims to provide ART to 100,000 persons by 2007. Conclusion What can we learn from the history AIDS treatment? In order for governments to have legitimacy, they have to respond to the perceived welfare of the people. In US, Brazil, Thailand and South Africa, the decision to provide ART were a result of concerted peoples action, putting pressure on governments to respond. Conferences at South Africa and Bangkok, and information sharing of local experiences, have been critical in enabling in generating such a response. Lobbying with WHO and UN to develop international initiatives and the impact these have on local governments has been important. Pharmaceuticals are not immune to the pressure of peoples groups, governments and international agencies. Concerted action can reduce costs of drug treatment. As Justice Cameroon said in an interview in 2002, “the question is not whether treatment is affordable. It is a question of will to provide treatment. It really is. US $ 7-9 billion is not a great amount by any metric”. Reference Perez-Casas, C., Macé, C., Berman, D., and Double, J. (2001). Accessing ARVs: Untangling the Web of Price Reductions for Developing Countries. Médicins Sans Frontières, Paris. Also at <www.accessmed-msf.org>.

mfc bulletin/April-July 2006

21

Quality of Medical Care: Public versus Private -Alpana Sagar1 The present debates on quality of care in the medical sector seem to be increasingly accepting that the private sector delivers better quality care than the public sector. This has important implications. It is necessary therefore to examine more deeply the concept of quality of care as it can be applied in the public as well as the private sector.

services. Subsequently I apply these measurements to the private health sector to assess their quality of care. I conclude by pointing out an important precondition for understanding ‘quality of services’.

The idea of assessing ‘quality of care’ in medicine is not a new one but has been present from times of doctors as ancient as Galen and Hippocrates.2 Then, and later in mediaeval times, medical care whether delivered by doctors or by wise women was on a ‘one to one’ basis. It was the individual healer’s knowledge as well as his/her relationship with the patient that was important. However, gradually, as medical schools became more common, medical knowledge was to some extent ‘standardized’ (for whatever its value!) and it was the doctors with greater bedside skills or some extra knowledge (like the Chamberlen’s and their forceps) who were considered as those who delivered better care. It was only after the 19th century that attempts were made to improve quality of care institutionally – either by assessing outcomes as done by Florence Nightingale in 1854, or by standardizing medical curricula as done by Flexner in 1910.

Traditionally quality of medical care is measured by an examination of inputs, processes and output or outcomes. Inputs are tangible resources that can be financial or physical such as infrastructure, personnel, and equipment. Processes can be either tangible or intangible. Among tangible processes we consider diagnostic or therapeutic procedures, leading to the intangibles of patient care. Similarly outcomes are either tangible or intangible. Mortality occurring or prevented would be a tangible event, morbidity could be either and patient satisfaction (a very important outcome) is often an intangible. However we find a limitation of the use of traditional managerial techniques when measuring medical inputs, processes and outcomes. We see that though we have used the terms ‘tangible’ and ‘intangible’ they are not really as separate as it seems.

Today much emphasis is being paid to assessment of quality of medical care and this is being done using techniques of Total Quality Management (TQM). These came into existence after World War II as an attempt by the Japanese to improve their industrial productivity. Today the same principles of input, processes and output are being used to assess quality of medical care – usually institutionally. However, in this paper I do not use the concept of ‘quality of care’ to assess performance of any one institution or compare it to the performance of another institution. Rather I use it in a macro public health perspective where I deal with the stated ‘poor’ quality of services in the public health sector and the stated ‘good’ quality of services in the private health sector, and in the process show that managerial techniques are not suitable for assessing ‘quality’ in medical care. In order to do this I divide my paper into four sections First I discuss some of the measurements of quality. I then utilize these measurements to understand some of the reasons for the stated ‘poor’ quality of public health 1

<alpanasagar@yahoo.com> This paper is limited to the concept of quality of care as examined in the West and in allopathic care. 2

Limitations of Traditional Managerial Measurements of Quality in Assessing Quality of Medical Care

For example while the ‘inputs’ i.e., actual physical and financial resources may be considered ‘tangibles’ their allocation belongs to the realm of ‘intangibles’ viz., policies for health and the political economy that determines these resources. Similarly while in ‘processes’ a machine is a tangible, the reasons why it may be used in one institution and not in another may vary and depend on many ‘intangible’ factors. Again outcomes can be both tangible and /or intangible. For example while the malaria parasite may be removed from a patient’s body (tangible) his/her subsequent weakness may persist (intangible). Similarly patient satisfaction may be measured but the assessments depend on intangibles – ones socialization, expectations etc. These are in turn linked to multiple factors –age, sex, class occupation relationship with doctor etc!! Care that is considered by many patients to be satisfactory may be assessed clinically as ‘medically irrational’! This ‘patient satisfaction’ depends on what the patient has been socialized into expecting. For some people care that may be considered most satisfactory may consist of short times spent waiting for the doctor, multiple visits (or even a private nurse!) many high tech diagnostic tests, and the latest and most expensive drugs. This however may not be therapeutically the most

22

mfc bulletin/April-July 2006

efficient treatment. Also care that satisfies a poor person may not be considered adequate or satisfactory by a better off person. However I will deal with an assessment of inputs and outcomes using only tangible measurements and without entering into this debate. An examination of processes would however touch upon this debate. The first part of the following section therefore deals with inputs and outcomes in the public and private sector, and examines some issues that are thrown up while the latter deals with the processes. I would conclude by pointing out an important precondition for understanding ‘quality of services’. Examining Inputs and Outputs for the Public and Private Medical Care Sector in India The Public Sector

Similarly if we examine the distribution of medical and paramedical personnel in the rural institutions, we find that specialists are not even posted at the CHCs more than 90% of the time. In the primary health centres we find only one doctor per PHC and many of the ancillary staff - 50% Male MPW 21% ANM, 34% lab technicians, 34% of pharmacists - are not posted (HII 2000-2001). However, at this point it is necessary to caution that we are looking at aggregate all India data, which means that often the situation is even worse in states with poor services or conversely better in those with good services. Another point we need to keep in mind that even if the manpower is present, equipment may not be present. For most doctors who have been trained in tertiary hospitals and learn to diagnose by depending primarily on investigations, to be put in a situation where they need to diagnose and treat disease burdens without sufficient and appropriate diagnostic tools is a major issue. While some doctors learn to

As mentioned above, tangible inputs into medical care can be considered in terms of infrastructure, personnel Table 1: Establishment of Rural Infrastructure in India since First Five Year Plan Plan Period

Community Health Centres

Primary Health Centres

Sub Centres

First (1951-56)

725

Second (1956-61)

565

Third (1961-66)

4631

Fourth (1969-74)

5283

33509

Fifth (1974-79)

214

5484

47112

Sixth (1980-85)

761

9115

84376

Seventh (1985-90)

1910

18671

130165

Eighth (1992-97)

2633

22149

136258

Ninth (1997-2002)

3043

22842

137311

and financial allocations. An examination of rural infrastructure over time (Table 1) reveals irregular growth as well as stagnation in infrastructure since the Eighth Plan.

compromise and develop their clinical acumen, others feel they are lost without the technology they have been trained in.

When we link this data to the infrastructure at the grass roots we find that the present infrastructure shows a major shortfall of 41% First Referral Units ie Community Health Centres (CHCs). For primary health centres and subcentres the shortfall is much less only about 7% (compiled from HII 2000-2001).

However this does not mean that the services in the rural areas are of no benefit. If we examine trends in health indicators we find that Infant Mortality Rates, life expectancies, and burden of communicable diseases like malaria, cholera, etc have shown the impact of medical care. It is however noteworthy that the changes in these health indicators do fluctuate over time.

This is significant since unless these shortfalls are addressed primary level care and not primary health care is what we are delivering to the people in the rural areas.

It is important to keep in mind the fact that medical

mfc bulletin/April-July 2006

23

care is not the only input that has improved these indicators. It is well known that health is affected by a complex of social and economic factors of which medical care is a part. This has been pointed out by people like Villerme and Virchow in the eighteenth century, Chadwick in the nineteenth century and most recently McKeown in the twentieth century. However though their work has shown that medical care has a limited part to play in improving health indicators, we do need to keep in mind that for a large proportion of people in our country access to proper sanitation and clean

indicates that while cases of malaria seem to be decreasing over time, case fatality seems to be increasing – this is indicative of the validity of the argument that the decreasing investment in malaria is proving to be detrimental to the program for malaria.

drinking water, employment, wages, nutrition etc has remained suboptimal. Given this situation medical care continues to play an important role in averting mortality to some extent as well as the episodes of morbidity for which care was sought.

called poor quality of the public sector it is useful in improving overall indicators.

Returning to the issue of inputs into the public sector we now look at trends in financial allocation. An examination of allocation of financial resources to communicable diseases over time also reveals decreasing investment in some diseases and increasing investment in others (Graph 1).

The first point to note here is that a large proportion of the improvements in health indices occurred pre-eighties before the private sector expanded to its present level.

What is the impact of insufficient manpower and infrastructure and decreasing financial resources on outputs/outcomes? To see the implications of this we examine some data on trends in malaria (Table 2) as well as in overall health indicators (Table 3). Table 2

Aggregate data on health indicators (Table 3) reveals that despite a slight slowing down in the last two decades, overall indicators of health (which are some reflection at least of health services) have shown improvements. Thus it seems that even despite the so-

Could it be possible that this improvement was due to the private sector and not the public sector?

Secondly, we need to keep in mind the fact that the public health infrastructure extends all over our country, especially in the rural areas. The presence of this infrastructure, even if it is inadequate, ensures that it is to a certain extent accessible and available to many of our people. The use of public health facilities seems to be a function of the availability of such facilities (Srinivasan and Mohanty <www.sasnet.lu.se.> Data

Table 2: Trends in Malaria 1986

1992

1994

1997

1999

2003

Malaria Total Cases

1.79 million

2.13 million

2.93 million

2.66 million

2.28 million

1.9 million

Total Deaths

323

422

1122

879

1048 (926 verified)

1006

Compiled from Annual Health Reports 1986-2005

24

mfc bulletin/April-July 2006 Table 3: Trends in Some Health Indicators Year

1947

1961

1971

1980

1991

2004

Life Expectancy M /F

32.5 / 31.7

41.9 / 40.6

46.4 / 44.7

54.1 / 54.7

59.7 / 60.9

64.1 / 65.6

IMR

162

146

129

110

80

63

MMR (estimates)

20

10

———

5.7

4.07

Malaria cases (estimates)

750 lakhs

49,151

13 lakhs

28 lakhs

21 lakhs

18.6 lakhs

% Deaths in 1st Year life

25%

———

20.8%

18.7%

15.1%

15%

from the 52nd round of NSSO also reveals that of the hospitalized cases the poorest are more in the public sector (Qadeer 2003). We see therefore that despite its weaknesses, due to the extent of its coverage, the public sector still has an overall epidemiological impact on life expectancy, Infant Mortality Rate etc. This is despite the fact that many of its facets leave a lot to be desired. The Private Sector Any assessment of the private sector also has to consider the issue of physical and financial allocation over time. However we have to consider the fact that the private sector is extremely diverse and includes not only the tertiary care hospitals like Apollo, Escorts etc, but a multitude of small and large nursing homes, individual MBBS (GPs) as well as the ‘quacks’(non MBBS doctors). We do know that the ‘quacks’ get no inputs financially or physically from the Government. However, oftentimes, these doctors do receive some degree of training from the MBBS doctors with whom they work with for some time before opening up their own practice. The GPs and small and even most large nursing homes also do not receive any direct subsidies from the Government. Some subsidy however is available for those who import equipment. Nevertheless we need to keep in mind the issue of indirect subsidies as most of these doctors have been trained in the public sector institutions (Baru 1998). However the greatest concessions are to the large corporate hospitals not only in terms of grants for land, but in terms of concessions for import of equipment and the indirect subsidy due to training in the major public institutions of the doctors in this sector. In terms of outcomes, the ‘quacks’ serve the greatest number of persons, followed by the GPs. The nursing homes do treat the poor but the majority of their patient load is from those who can pay – from the lower middle to the upper middle class. The large tertiary private hospitals serve only the better off section of society that can afford their care (about 5%) as well as those whose care would be reimbursed. Whether the care offered by these ‘quacks’ is irrational

or not, we have to admit that a great number of people access them. We have to accept therefore that they must be alleviating suffering and probably mortality.1 Also people continue to access them so they must be meeting some criteria of quality of care!!! (If we take continued use as patient satisfaction then we have to accept this statement. If we take it as people’s lack of choice then we have to question a system that forces people to these doctors). The smaller MBBS doctors (GPs), also serve the poorer sections and the lower and middle class who often cannot afford to use the nursing homes for common problems of ill health. Their consultation charges can vary from Rs 30 to 80 per visit and would include drugs as well. The nursing homes serve those who can afford the consultation charges (independent of drugs or investigations) that can range from Rs 100 to Rs 300 per visit or those who can afford the in patient care offered by these institutions. And the elite – a very small fraction of the country’s population - uses the corporate hospitals like Apollo. Earlier we have seen that an important indicator in quality of outcomes seems to be population coverage – and this needs to be considered in such discussions on ‘quacks’ as well! It is difficult to quantitatively measure outcomes for the various sections of the private sector, as data on this is just not available. But considering the coverage offered we can assume that impacts would be inverse to the hierarchy in which this sections stand. Processes for the Public and Private Sector As mentioned earlier the whole idea of tangible and intangible as regards quality measurement is problematic. In an examination of processes this can be seen extremely clearly especially when we talk of diagnostic and therapeutic procedures involved in patient care. Assessing ‘Good Quality’ Diagnostic and Therapeutic Care What a patient considers ‘quality’ therapeutic or diagnostic care may not be medically rational. For example, often in its attempt to give ‘better quality

mfc bulletin/April-July 2006

25

care’ the private sector sets the example for irrational diagnostics. Is an investigation by MRI for headache the best investigation possible? What is the importance of ‘hands on’ diagnosis and care? Is rational diagnostics necessarily the most expensive or the highest tech?

to move into a macro perspective and needs a framework beyond the purely managerial. The concept of quality of care provided must function within this understanding. A precondition therefore that is important for measures of quality is that we take context and larger structures into account.

As an example I would point out that both the slide and the dip strip can be used to diagnose malaria. While the latter may be more accurate it costs about Rs 150/ per strip while the slide costs about 10 paise. Which is then more efficient? Similarly for chloroquine vs artemesin the cost of the entire treatment is Rs 10 vs about Rs 700. Artemesin has come into the market because of the fear of drug resistance of the malarial parasite to chloroquine – but resistance is occurring due to incorrect prescription and most doctors are to be blamed for this. Again will prescription of let us say cefduroxime be better for a tonsillitis that erythromycin? Are the latest drugs or high tech tests necessarily indicative of good care? How will we regulate either the diagnostics or therapeutics or the costs of care in the private sector? It would be difficult enough in the public sector as rational therapeutics is not considered to be an important issue in medical training.

(I gratefully acknowledge Drs Sathyamala and Ritu Priya for their valuable comments on an earlier draft of this paper).

Can we disassociate rational low cost care from discussions on quality of care? Another extremely important issue is that the philosophy behind the private and public sector care is extremely different. For example the public sector works on principles of universality, comprehensiveness and equity while the private sector in general works on a profit basis. One works on the principles of social justice and the other on principles of the market. One has to work on the public health principle of caring for as many people as possible with optimum care within the allocated resources, while the other works on the principle of ‘designer’ care where resources are used according to the patient’s paying capacity.

§

Then how do we expect standards for processes to be the same? The discussion on ‘accreditation’ today seems to be leaning towards more impressive infrastructure, more personnel, more high-tech equipment etc as available in corporate hospitals. Can a Primary Health Centre or a GP or a Community Health Centre (the First Referral Unit) or even a Government tertiary hospital, meet these standards? In fact does it need to meet this ‘accreditation’ to deliver good care? These are some of the questions we need to examine and debate when we talk of issues of quality of care. They signify that a consideration of quality of care has 3

I have been unable to find data to support or disagree with this so I am throwing it open to debate.

References § § § § § §

§

§

§

§

§ § § §

§

Baru R, 1998: Private Health Care in India: Social Characteristics and Trends Sage New Delhi. Government of India (1946), Ministry of Health, Health Survey and Development (Bhore) Committee Report, New Delhi. Government of India (1961) “Report of the Health Survey and Planning (Mudaliar) Committee, Vol. 1,”, Ministry of Health, New Delhi. Government of India (2000) Survey of Causes of Death Rural 1998, Office of the Registrar General, Vital Statistics Division, New Delhi. Government of India (2004) Sample Registration Survey Bulletin, Office of the Registrar General of India, New Delhi. Government of India 1989- 2005, Annual Health Reports, Ministry of Health and Family Welfare, New Delhi. Government of India 1990-91-2005-2006, Expenditure Budgets Vol II. Government of India, (1977) Pocket Book of Health Statistics of India, Central Bureau of Health Intelligence, Directorate General of Health Services, Ministry of Health and Family Welfare, New Delhi. Government of India, (1981) Health Statistics of India 1981, Central Bureau of Health Intelligence, Directorate General of Health Services, Ministry of Health and Family Welfare, New Delhi. Government of India, (1984) Health Statistics of India 1984, Central Bureau of Health Intelligence, Directorate General of Health Services, Ministry of Health and Family Welfare, New Delhi. Government of India, (2003) Health Information of India 2000 & 2001, Central Bureau of Health Intelligence, Directorate General of Health Services, Ministry of Health and Family Welfare, New Delhi. Government of India, (2005) Annual Report 2004-05, Ministry of Health and Family Welfare, New Delhi. International Institute for Population Sciences (IIPS) and ORC Macro (2000) National Family Health Survey (NFHS2),1998-99;India, IIPS, Mumbai National Planning Committee (1938), Sub-Committee on National Health (Sokhey), Report, Vora, Bombay. Srinivasan K and Mohanty SK Health Care Utilization by Source and Levels of Deprivation in Major States of India: Findings from NFHS 2 <http://www.sasnet.lu.se/ EASASpapers/11Mohanty_Srinivasan.pdf> Qadeer I 2003: “Health Sector Reforms in India” in International Conference on ‘Monitoring Shifts in Health Sector’ organized by Centre of Social Medicine and Community Health JNU, Delhi.

26

mfc bulletin/April-July 2006

Some Strategies to Cut Health Care Costs -Vinod Shah1 1. Medical Training Issues

1.2 Suggested Solutions

1.1 Lack of multi competency or family medicine specialists

a) Policy l

The family medicine to specialist ratio in the Indian subcontinent is extremely low. Lack of multi-competency training leads to excessive referrals to specialists. l

Specialists generally stay in district headquarters or bigger towns and they are expensive. For instance, 15% of outpatient care in a primary care centre has depression/ anxiety/phobia/obsessions all of which can be better treated by family medicine practitioners; however the latter are not available and the average GP cannot handle these. Giddiness is among the five most common symptoms among adult males but GPs are not competent enough to manage this. A World Bank-funded study in 1999 showed that the private sector is an important cause of submerging many below the poverty line. According to the study: l l l l l

55% of all hospital admissions are in private sector 40% of all anti and postnatal care are in the private sector 25% of all hospitalized Indian slip below the poverty line because of hospital expenses alone Hospitalized Indian spend more than half of their annual expenditure on health care More than 40% of those hospitalized borrow money or sell assets to cover expenses

b) Distance Education in Family Medicine There are between 250,000-500,000 registered doctors in India and only 25% of them have postgraduate training. Make distance education in family medicine accessible to the GPs in the country by starting courses in many centres in India. Family medicine reduces costs by: l One window approach to health care in 80% of cases l Closer geographical access l Less referral to specialists who are more prone to over-investigation. 2. Health Insurance Issues l

l l

The National Health Policy 2002 has this to say on the need for specialists in Public Health and Family Medicine: In any developing country with inadequate availability of Health services, the requirement of expertise in the areas of Public health’ and ‘family medicine’ is markedly more than the expertise required for other clinical specialties….NHP-2002 examines the possible means for ensuring adequate availability of personnel with specialization in the ‘public health’ and ‘family medicine’ disciplines….. 1

Distance Education Unit, Christian Medical College, Vellore. <vshah47@cmcvellore.ac.in>

Mandate every medical college involved in post graduate training to have a Family medicine department able to offer post graduate family medicine training in large members. Incentives to family medicine trained doctors to start work in taluk Headquarters.

l

An average Indian including those below the poverty line spends Rs 48/year/person on health care (this includes gifts to the religious practitioners). No insurance company can sustain health insurance for the poor at this rate Have to look at any community based insurance program. Here the TPA (third party administrators) are eliminated and where the community runs its own insurance program in partnership with a NGO/ charitable hospital There are several models which are sustainable, for e.g., the SEWA microinsurance program and the Catholic Social Insurance Program in Chattisgarh.

There are problems with the current mega health insurance companies: l l l

They do not register families but individuals They reimburse; they do not pay up They do not provide for outpatient treatment

mfc bulletin/April-July 2006 l

They do not pay for deliveries, for TB / AIDS and several other illnesses common among the poor.

27

l

3. Drug-Related Issues l l l

Training in rational drug therapy mandatory for relicensing Make drug inspectors accountable to the community – to reduce spurious drug use m Fight “Injection culture” as it not only escalates health care costs but can spread HIV / Hepatitis B&C

l l l l

4. Corruption Issues l

According to the 2003 Transparency International Corruption Perceptions Index: l Finland is the least corrupt country with the score 9.7 l Bangladesh is the most corrupt country with the score 1.3 and l India with the score 2.8 is among the 50 most corrupt countries in the world out of 133. 4.1. Three Most Corrupt Practices l l l

Commission giving and taking for referrals Over investigating Over treating (Drugs and procedures)

4.2. Corruption in Government Hospitals l l l

The total value of monetary corruption is Rs 2017 crores per annum This is apart from commission give and take, over and above prescribing and over-investigating. States like Kerala, Karnataka, Tamil Nadu and Gujarat with low levels of corruption have better health indicators.

l

l

l

l l l

l

l

4.3. Government Hospital Usage l l l l

Average of 55% (Bribe average 27%) Rural average 59% (Bribe average 19%) Urban average 51% (Bribe average 51%) 45% of the time bribe was demanded

l

l

4.4. Corruption and Health Care l

Education of SCs and STs and slum dwellers neglected because teachers concentrate on children whose parents pay them, so another generation grows up ignorant of basic health

l

measures. Spurious and adulterated drugs proliferate because high level politicians protect the killers who sell them. Students bribe college staff resulting in underexperienced/ qualified staff taking up posts. Buildings fall down in earthquakes because of building violations. Doctors don’t attend place of work despite drawing a wage from the Government. Failure of drugs and other consumables to reach the intended point of use. Misappropriation of vehicles (and much more) by officials and doctors. Staff not paid or reimbursed for outreach work because of embezzlement of their wages or babus demanding favours to move their files. Inattention to ensuring clean water and hygienic environments resulting in filthy hospitals - because officials know there is no accountability Official government and externally funded programs are neglected as senior District health officials are more interested in recouping the cost of buying their post. Unnecessary duplication and irrational drug prescribing by doctors, due to lack of accountability and incentives from unscrupulous pharmaceutical companies. Under the desk payments demanded from patients by everyone from the watchman to the doctor. Lack of accountability of staff and contractors due to corrupt supervisors/ masters Theft and sabotage of instruments by staff wanting to make their private practices look more efficient than the public sector. The wrong staff is sent on training courses (even abroad) because of patronage, similarly incompetent consultants and advisers are appointed to ‘assist’ projects. Politicians and public servants badger development agencies or private sector banks for loans which have little or no technical justification. Theft and sabotage of instruments by staff wanting to make their private practices look more efficient than the public sector. The wrong staff are sent on training courses (even abroad) because of patronage, similarly incompetent consultants and advisers are appointed to ‘assist’ projects. Politicians and public servants badger development agencies or private sector banks for loans which have little or no technical justification.

28

mfc bulletin/April-July 2006

5. What can be done? l l l l

l

Users’ committee Diagnostic facilities should be outsourced to private players Display of drugs received and stock position Community participation in hospital management: Get the local communities to form local registered bodies and empower them to manage as well as levy small user charges. (Example: Rogi Kalyan Samiti in MP). Hippocratic type oath to be promulgated by IMA which would make it a matter of professional selfdiscipline to: m Refuse to give, receive or countenance bribery in their sphere of influence; m To anonymously report for collective action to highlight where and when outside interference occurs (there is safety in numbers);

To commit themselves to rational drug use, referral and evidence based interventions. To shun errant colleagues For professional associations to support the Medical Council of India in making violations of the above a ‘striking-off’ the register offence, and to set up their own self-disciplinary machinery if the official machinery won’t do it. Participate actively in designing and pressurizing for governance systems that are transparent and fool proof. Refusing incentives of pharmaceutical companies and others Refusing to buy posts or co-operate with those who buy post. Refusing to tolerate bribery among junior staff. m

l l

l

l l l

Medical Facilities and Health Needs on Wheels: Still Unviable? -Dr. N. Kannan1 There is a coastal village - Periyathalai - in Thoothukudi district, Tamil Nadu, which has more than 200 people with different types of (physical) disabilities representing all the age groups (a few them are young children also). The village has 1150 households with a population of 4700. The village is not frequented by public transport. For everything - be it obtaining an identifcation card needed for getting government assistance or for medical help - they need to go to nearby towns such as Tiruchendur and Thoothukudi located at about 20 and 50 km, respectively, with great difficulty. Most of the children need to be lifted physically by their parents to get out of home. The pathetic condition of girls and women more than boys and men in such burdensome situations would shake the consciousness of any human being. But not the government machinery! Consider another village - Vadikottai in Tirunelveli district situated by the side of stone quarries, with a population of 2500. More than a dozen children frequenting nearby town - Sankarankoil at a distance of 10 km - even for one or the other common ailments at any part of a day. The PHC, under which Vadikottai village falls, is about 9 km away, and not connected by public transport facility with the village. It is a painful sight to see mothers carrying the sick young ones to the clinic, spending a whole day for simple treatment and going back with a heavy heart. 1

Dept. of Sociology, M. S. University, Tirunelveli - 627 012. <kannanmsu@rediffmail.com>

Yet another village is Thulukkarpatti in the dry tract of Virdhunagar district - accounting for more than a hundred elderly, aged population, also offers a similar agonizing sight while they go to the nearby town Srivilliputhur at a distance of 23 km - for medical help. The nearby PHC, though located only at a distance of 7 km, is not accessible by road. Needless to say, such difficult situations and circumstances are seen all over our country - be it rural or urban area, disabled or children or old age. It suggests that it is not enough merely to create medical facilities, which are not useful for a large section of our populace in one way or the other. It may be recalled that Article 12.1 of the International Covenant on Economic, Social and Cultural Rights which was adopted in 1966, also implies that medical and health care services be made physically accessible for all sections of the population, including vulnerable and marginalized. Is it really not feasible to make the necessary medical services and health needs available for our people at their doorstep by a “medical team” visiting them everyday at an appointed time - of course, at a low cost? Surely costbenefit analysis alone can convince us such services are worth it keeping the present as well as future needs of the kind of quality of life envisaged? The time is ripe to think and act towards services at door step before the ‘private sector’ steps in with mobile “mediclaim insurance” packages - albeit for the poor?

mfc bulletin/April-July 2006

29

Rational Drug Therapy: Principles, Realities and Road Ahead - Sujith J Chandy1 Introduction Ineffective, inappropriate and economically nonviable use of medicines is often observed in health care throughout the world. This is more so in the developing countries. The need for achieving quality use of medicines in the health care system is not only because of the financial reasons with which policy makers and administrators are usually most concerned. Appropriate use of drugs is also one essential element in achieving quality of health and medical care for patients and the community.

does not conform to these criteria and can be termed as inappropriate or irrational prescribing. Common examples of irrational prescribing are: l l

l

Defining Rational Drug Use What is rational use of drugs? The Conference of Experts on the Rational Use of Drugs, convened by the World Health Organization in Nairobi in 1985 defined it as: Rational use of drugs requires that patients receive medications appropriate to their clinical needs, in doses that meet their own individual requirements for an adequate period of time, and the lowest cost to them and their community. These requirements will be fulfilled if the process of prescribing is appropriately followed. This will include steps in defining patient’s problems (or diagnosis); in defining effective and safe treatments (drugs and nondrugs); in selecting appropriate drugs, dosage and duration; in writing a prescription; in giving patients adequate information; and in planning to evaluate treatment responses. The definition implies that rational use of drugs, especially rational prescribing should meet certain criteria: Appropriate indication: The decision to prescribe drug(s) is entirely based on medical rationale and that drug therapy is an effective and safe treatment Appropriate drug: The selection of drugs is based on efficacy, safety, suitability and cost considerations. Appropriate patient: No contra-indications exist and the likelihood of adverse reactions is minimal, and the drug is acceptable to the patient. Appropriate information: Patients should be provided with relevant, accurate, important and clear information regarding his or her condition and the medication(s) that are prescribed. Appropriate monitoring: The anticipated and unexpected effects of medications should be appropriately monitored. Unfortunately, as all of us are well aware, reality as regards rational prescribing is otherwise. Prescribing most often 1

Clinical Pharmacology Unit, CMC, Vellore, <sjchandy@cmcvellore.ac.in>

l l

l

l

The use of drugs with doubtful/unproven efficacy, e.g., the use of antimotility agents in acute diarrhoea. The use of unnecessarily expensive drugs, e.g., the use of a third generation cephalosprin, when a first line agent is indicated or a broad spectrum antimicrobial when a narrow spectrum drug would do. The use of drugs when no drug therapy is indicated, e.g., antibiotics for upper respiratory infections, diarrhoea and viral fevers. The use of drugs of uncertain safety status,e.g., use of dipyrone/analgin. The use of the wrong drug for a specific condition requiring drug therapy, e.g., tetracycline in childhood diarrhoea requiring ORS. Failure to provide available, safe, and effective drugs, e.g., failure to vaccinate against measles or tetanus, failure to prescribe ORS for acute diarrhoea. The use of correct drugs with incorrect administration, dosages, and duration, e.g., the use of oral steroids in asthma when inhaled steroids would be much more efficient and safe.

Other common and widespread irrational prescribing practices include: l l l l l

Overuse of antibiotics and antidiarrhoeals for non-specific childhood diarrhoea Indiscriminate use of injections, e.g., in malaria treatment Multiple drug prescriptions, fixed dose combinations Use of antibiotics for treating minor ARI Minerals and tonics for malnutrition

Of course, rational prescribing is not as easy as it sounds. The drug use system is complex and varies from country wise. Drugs may be imported or manufactured locally. The drugs may be used in hospitals or health centers, by private practitioners and often in a pharmacy or drug shop where OTC preparations are sold. In some countries all drugs are available over the counter! In India, there are a lot of alternate systems of medicines and many practitioners prescribe allopathic medicines. There are a large number of quacks without any knowledge of rational prescribing. Last but definitely not the least the public includes a very wide range of people with differing knowledge, beliefs and attitudes about medicines. Many a time, the patient expects a particular drug and hints or directly asks for that to the doctor or pharmacist.

30

mfc bulletin/April-July 2006

Factors Underlying Irrational Use of Drugs There are many different factors which affect the irrational use of drugs. In addition, different cultures view drugs in different ways, and this can affect the way drugs are used. In India, this can therefore be a complex maze with multiple cultures, religions, dialects and castes. If one were to broadly classify the factors, they could be divided into: those deriving from patients, chemists shops, prescribers, the workplace, the supply system, industry influences, regulation, drug information and misinformation. In each group, there can be various ways contributing to irrational use of drugs: l l

l l l l

l

l

Patients - drug misinformation, misleading beliefs, patient demands/expectations Prescribers - lack of education and training, inappropriate role models, patient pressures, lack of objective drug information, company incentives, limited experience, Misleading beliefs about drug efficacy, competition Chemist shops – patient pressures, profit motives, competition Workplace - heavy patient load, pressure to prescribe, lack of adequate lab capacity, insufficient staffing Drug Supply System - unreliable suppliers, drug shortages, limited budgets necessitating fixed choices, expired drugs supplied Drug Regulation - non-essential drugs available, inefficient audit system, inadequate legal implementation, non-formal prescribers Industry - promotional activities, misleading claims, incentives

Impact of Irrational Drug Use Irrational drug use can have various consequences, for the patient, the public, the health system and even the economy. A few important consequences are mentioned below: l l l l

Reduction in the quality of drug therapy - this can lead to increased morbidity and mortality Waste of resources – this can lead to reduced availability of other vital drugs and increased costs Increased risk of unwanted affects - adverse drug reactions and the emergence of drug resistance Psychosocial impacts - patients may believe that there is “a pill for every ill”

Antimicrobial Misuse Problem: A Focused Case of Irrational Drug Use and Its Consequence Irrational prescribing and dispensing issues come to the

fore when it comes to antibiotic use. I would like to therefore focus on this issue and how it has impacted society. The problem of antimicrobial resistance was one of the important issues brought up at the World Health Assembly (WHA) in 2005. The WHO says that antimicrobial resistance is one of the world’s most serious public health problems. A major reason is the irrational use of medicines. According to WHO, worldwide, more than 50% of all medicines are prescribed, dispensed or sold inappropriately, and 50% of patients fail to take them correctly. The consequence of this is seen directly with the misuse of antibiotics. There is increasing antimicrobial resistance, with resistance of up to 70-90 percent to original first-line antibiotics for dysentery (shigella), pneumonia (pneumococcal), gonorrhoea, and hospital infections (Staphylococcus aureus). A WHO policy paper on “Containing antimicrobial resistance” says that many of the microbes that cause infectious disease no longer respond to common antimicrobial drugs such as antibiotics, antiviral and antiprotozoal drugs. The problem has reached unprecedented proportions that unless concerted action is taken worldwide, we run the risk of returning to the pre-antibiotic era when many more children than now died of infectious diseases and major surgery was impossible due to the risk of infection. WHO’s data show the following antimicrobial resistance global prevalence rates: malaria (chloroquine resistance in 81 out of 92 countries); tuberculosis (0-17% primary multi-drug resistance); HIV/AIDS (0-25% primary resistance to at least one antiretroviral drug); gonorrhoea (5-98% penicillin resistance); pneumonia and bacterial meningitis (0-70% penincillin resistance in streptococcus pneumonia); diarrhoea: shigellosis (10-90% ampicillin resistance, 5-95% cotrimoxazole resistance); hospital infections (0-70% resistance of staphylococcus aureus to all penicillins and cephalosporins). Another WHO paper says that irrational medicines use includes use of more medicines than are clinically necessary, inappropriate use of antimicrobial agents for non-bacterial infections; inappropriate selection or dosing of antibiotics for bacterial infections; over-use of injections when oral formulations are more appropriate; failure to prescribe in accordance with clinical guidelines; and inappropriate self medication often of prescriptionsonly medicines. Referring to the HIV/AIDS, TB and malaria epidemics, the paper says “concerns are growing about accelerating rates of anti-microbial resistance and rising prices for alternative anti-microbial agents to treat infections due to resistant pathogens.”

mfc bulletin/April-July 2006 Antimicrobial resistance in the Indian context is also on an ever increasing rise. This is mainly attributed to antibiotic misuse at three levels, human misuse, animal misuse and environmental misuse. It remains to be seen how each is having an impact on resistance levels. Human misuse is the most widely documented of the three. In a study done to measure the misuse of antimicrobials in predominantly viral conditions such as diarrhoea, URI and fever with myalgia, it was seen that a high percentage of patients received antibiotics. In Uttar Pradesh, it was as high as 80%, in Tamil Nadu, 70%, whereas in Kerala it was only 40%. This brings us to the fundamental question whether other factors besides drug promotions and profit such as socioeconomic conditions, literacy etc are factors which influence use of drugs. In another study supported by WHO in Vellore district, an attempt was made to establish a model to compare antimicrobial resistance and usage patterns. It was found that approximately 42% of all outpatients were being given antibiotics. It was also noticed that various stakeholders were responsible for overusing various types of antibiotics. Private practitioners preferred antibiotics such as ciprofloxacin, whereas pharmacists dispensed both amoxicillin and ciprofloxacin. Due to limited availability of antibiotics in government facilities, cotrimoxazole was highly used. These observations hold valuable lessons in pointing to the factors that drive irrational drug use. Accessibility and availability were key issues in the governmental facilities whereas peer competition, pressure to cure and industry incentives contributed in the private health facilities. In both areas, patient expectations was a key contributing factor What can be done? The WHO lists measures that governments can take. One of the interventions suggested relate to drug sales promotion. “Pharmaceutical promotion often has negative effects on prescribing and consumer choice, but regulation of promotional activities has been proven to be one of the few effective interventions … Countries should therefore consider regulating and monitoring the quality of drug advertising and of the pharmaceutical industry’s promotional practices, and enforcing sanctions for violations.” Strategies to target the stakeholders such as doctors and pharmacists need to be thought about. Proper training on the approach to therapeutics, minimizing the impact of industry incentives, a peer process to reduce irrational prescriptions are just some of the ways that can be adopted. Last but not the least, public awareness and health education are key issues to be dealt with. If a layman is made to understand the difference between viral and bacterial illness, it makes it easier to understand that

31 antibiotics are not needed in viral conditions. Although there have been previous WHA resolutions, and a WHO programme on rational drug use, not much has been done in countries. Very little is being spent to promote rational use of medicines. The global sales of prescription drugs in 2000 were $282.5 billion and drug promotion costs in the US were $15.7 billion the same year. In 2002-03, global WHO expenditure was $2.3 billion, of which the WHO expenditure on promoting rational drug use was only 0.2%. The WHO is tackling the issue through advocacy, the essential medicines list, training programmes and a WHO global strategy on anti-microbial resistance. There was inadequate implementation of rational medicines use in countries, with only 26% of countries having a national strategy and only 50% of countries having public education in the past two years. Irrational drug use is a very serious global public health problem and much more policy implementation is needed at national level. Rational use could be greatly improved if a fraction of the resources spent on medicines were spent on improving use. Examples of successful national programmes for rational drug use are found in Indonesia, and the Swedish Strategic Programme for Rational Use of Anti-microbial Agents. In conclusion, the need of the hour is to develop a coherent, comprehensive and integrated national approach to implement the strategy for irrational use of drugs; to enhance the qualtiy use of medicines through using national standard-practice guidelines for common diseases; and to strengthen legislation; and to mobilize resources to promote sustainable, practical and cost – effective interventions for rational use of medicines by providers and consumers. The question is - do we have the courage and can we develop an optimal strategy to confront the factors promoting irrational use and move towards rational drug use? References <http://www.who.int/medicines/areas/rational_use/en/index.html> <http://dcc2.bumc.bu.edu/prdu/> <www.who.int/gb/ebwha/pdf_files/EB115/B115_40-en.pdf> <www.who.int/entity/csr/resources/ publications/drugresist/ WHO_CDS_CSR_DRS_99_2/en> <www.who.int/medicines/areas/rational_use/en/index.html> <http://www.twnside.org.sg/title2 twninfohealth009.htm> APP Project Report: “A Survey on Antibiotic Prescribing Pattern and Related Factors in Primary and Secondary Health Care Settings”, 2002-2004, INCLEN-USAID Consolidated Technical Report, INDIACLEN Infectious Disease Initiative. See also <http:// indiaclen.org/app_report_july12_04.doc>

32

mfc bulletin/April-July 2006

Quality of Care: The Ayurvedic Perspective -P. Ram Manohar1 Even the resurgent traditional modalities of healing, which were once upon a time, controlled by a humanistic value system have become extremely opportunistic and exploitative. Seeking alternatives in the wake of the failures of biomedicine, vulnerable people find themselves falling into the fire from the frying pan. Advanced high tech curative modalities have undermined the importance of providing high quality personalized care in medicine. We have to search for the vision of a more complete healing environment in which caring and curing will go hand in hand. Traditional medicine, which has innately been care oriented will be able to better position itself by drawing from its rich inheritance rather than by mimicking biomedicine. An anecdotal account in the tradition of Ayurveda gives incisive insight into the overriding importance of care over cure. Nagarjuna was an accomplished alchemist and a great physician. He possessed rare skills in formulating the most potent medicines that could cure grave illnesses. He was very concerned that he should discover a worthy disciple to transfer his wisdom for the benefit of humanity. Two young men approached Nagarjuna once in the hope that they would be chosen as his disciples. Nagarjuna said that he needed to test them before initiation. He instructed them to go home and prepare a sophisticated formulation as per the directions given by him. He told them that he would decide whether they were worthy of his discipleship based on the results of this test. The two of them quietly went home and returned the next day. One of them had prepared the medicine as instructed by Nagarjuna. But the other person came empty handed. Nagarjuna first examined the medicine prepared by the young man and nodded his head in approval. This person had done a good job, indeed. He then turned to the young man who had come empty handed and asked, “What happened to you? Why did you not make the medicine as I had instructed?” He replied, “Sire, as I was walking towards my home, I found an old and sick man on the side of the road pleading for help and assistance. Seeing his pitiable 1

Director of Research, Arya Vaidya Pharmacy, Coimbatore, <rammanoharp@gmail.com>

state, I could not ignore him. I stayed back to help him and nurse him. Hence I could not make the medicine.” Nagarjuna realized that the other young man who had also seen this old man went past him in his hurry to make the medicine and please Nagarjuna. In a moment, Nagarjuna remarked, “You are my disciple, the one who helped the old man; because healing is primarily compassion and only secondarily medicine. It is not as challenging to impart the skill of curing as it is to nurture the virtue of caring.”

Compassion is synonymous with healing in Ayurveda. The very knowledge of Ayurveda is said to have arisen out of compassion for not only suffering humanity but also all living creatures. The story goes that the Sages assembled at the foot of the Himalayas deeply moved by the sight of living beings suffering from grave diseases. Sage Atreya is characterized as one who sees other beings as his own self (maitrIparaH) and overflowing with compassion for all life forms (sarvabhUtAnukaMpA). He taught six of his disciples in order to spread the knowledge of Ayurveda and propagated the branch of general medicine in the world. Criteria for Quality Care: the four limbs of treatment Medical treatment is a process that unfolds through a delicate interaction between the physician, the drug, the nurse and the patient. Ayurveda considers these four factors as essential components of the healing process. These are in fact, known as the four limbs of treatment. Ayurveda attempts to set forth high standards for health care by defining desirable qualities of the four limbs of treatment. Healing does not happen by powerful medicines alone. When the four limbs of treatment are endowed with four desirable qualities, then the healing potential of medical intervention is maximized. The physician, the drug, the nurse and the patient are all participators in the process of healing and make a share of the contribution necessary for healing to become complete. l

The physician has to be dexterous, should have learnt medicine from a qualified preceptor, and

mfc bulletin/April-July 2006 must have rich clinical experience and a value for cleanliness. l

The medicine should be presentable in variable dosage forms, should have a multifaceted pharmacological profile, and should be potent and capable of being formulated for individual needs.

l

The nurse should be caring, have a value for cleanliness, and be dexterous and intelligent.

l

The patient has to be resourceful, compliant, communicative and endowed with mental strength.

Treatment outcomes are maximized when the four limbs are endowed with the stipulated qualities. And of course, when it is erected on the fertile ground of compassion! Ayurveda forewarns that powerful tools of great curative potential are likely to be used to exercise control over sick people and to exploit them rather than to serve them. A weapon, water and knowledge depend on the vessel for benefic or malefic effects. Likewise, medical knowledge will be used for good or bad depending on the character of the person who wields it. Qualities of a Physician Compassion is the foremost quality that sets the gold standard for a humanistic health care delivery system. This quality is variously referred to as bhUtadayA or kAruNyam in the classical Ayurvedic texts. Four qualities make a physician capable of delivering high quality health care. The first quality is maitrI (the ability to put oneself in another’s position). The second quality is kAruNyam (the inner urge to remedy another’s suffering). The third quality is Cakye prItiH (enthusiasm to treat diseases that will respond well to medical interventions). The fourth quality is prakRtistheSu bhUteSu upekSaNam (wisdom to refrain from adding to the agony of a dying person by administering useless medicines). The celebrated Ayurvedic text Caraka Samhita minces no words to condemn the physician who torments a patient by irrational use of medicines. The text says that even speaking to such a physician will take one to the gateway of the worst hell. There is no greater sin than administering an unknown medicine or a

33 medicine of doubtful efficacy to a sick and unhappy bedridden person. It is better to drink poison or molten copper or red-hot iron balls rather than demanding food or money from a person in distress without being able to help that person in a definite way. On the other hand, the physician is obliged to treat hapless patients without proper means of livelihood as one’s own child. Compassion (attitude), service (action), appropriate use of medicines and avoiding irrational medical interventions are four qualities to be cultivated by a physician. Curtailing the activities of unqualified persons in the field of health care is another measure through which quality of heath care can be ensured. When quackery is rampant, the health of the public is exposed to dangerous medical practices. It is equally important to prevent qualified physicians from misusing their skills by violating the rights of the patients they treat. The power to enforce such regulations was vested with the King in ancient times. Every physician had to be certified by the preceptor and the King. Ayurveda has given some attention to the economics of health care. It admits that people who are resourceful have access to better medical care. Therefore the physician is sensitized to become aware of the disparity that is very likely to arise in providing high quality medical care for the poor. The Ayurvedic texts distinguish between what is called as royal treatment that only the higher strata in society can afford and ordinary treatment that is accessible to the poor. The physician has to be intelligent enough to make innovations in the treatments, so that poor patients can also get medical care without compromise in quality. Quality of Medicines Quality health care is very much centered on judicial and safe use of medicines. Quality of care cannot therefore be defined without considering the quality of medicines. Ayurveda lays great emphasis on the use of safe medicines and the need to avoid injuries inflicted by dangerous medicines. The need to monitor safety of medicines and treatment as well as physician induced illnesses and deaths have been pointed out. The ideal medicine is that which cures a disease without giving rise to other problems. It is that which not only cures, but also brings balance in the internal environment of the body. Ayurveda has laid down

34 quality control guidelines to make medicines that begin at the stage of raw material acquisition right up to the finished product. Body, Mind and Spirit What perhaps gives an added dimension to the quality of health care in Ayurveda is the notion of the three dimensional human personality. The human person is not merely a body, but a self with a mind that inhabits the body. Healing cannot be complete if it ignores the psychological and spiritual dimensions of health and disease. The Ayurvedic texts declare that the physician who does not succeed in entering into the self of his patient fails to treat him completely. Psycho-spiritual elements should supplement the empirico-rational approach to healing for a truly holistic medicine. Ayurveda believes that true healing works by creating a value for self care. Nature’s Healing Powers In ideal circumstances, the body cares for itself. It is this innate ability for self-care that has to be nurtured for true healing to take place. One of the fundamental principles of Ayurvedic healing is to kindle the ability of the body and the individual to care for itself. Medical intervention is only a helping hand – an extra help for someone trying to help himself. High quality medical care recognizes the needs of not only the body, but also the mind and self. It empowers the individual to actively participate in the healing process. Caring is not creating dependence on another individual. It is a liberating influence that creates independence. Healing is a judicious combination of caring and curing. Cure has to be circumscribed within the larger ambit of care. The skilled physician is always caring but attempts to cure only when it is relevant. An Ayurvedic text reminds the physician that diseases get cured even without treatment and don’t get cured even with treatment and so the scope of curing diseases is limited, while the scope of caring for the sick is vast and unlimited. One may fail to care while attempting to cure, but may succeed in curing while engaged in care! Caring is the fundamental principle of healing.

mfc bulletin/April-July 2006 Social Control In classical Ayurvedic practice, the phenomenon of a pharmaceutical industry remotely controlling the physician and patient was virtually unknown. Medicines were prepared in households and home pharmacies of physicians and both patient and physician had great control over the medicines used in treatment. In the most typical settings and for routine ailments, the physician would fish out a prescription listing the herbs to be collected from the wild and from raw drug shops. The prescription would also contain detailed instructions on how to make the medicine, which in most cases would be a decoction that could be cooked in the kitchen of the patient’s home. Traditional homes in Kerala have even today preserved the remnants of the paraphernalia that was once part of an active home pharmacy. In these circumstances, the patient would have complete control in the procurement of the raw drug and it’s processing. In situations where the preparation of the medicine involved more sophisticated procedures, the physician would make the medicine and hand it over to the patient. In both cases, the patient or physician had direct control in the preparation of medicines. Ayurvedic traditions also experimented with the concept of community medicine with people’s participation to nurture a model of health care that was controlled by the patients. Apart from state support and patronage, the public also supported traditional physicians. In return, these physicians offered free medical services to the needy. Remnants of such traditions can still be seen in Kerala, especially in the context of the tradition of snake poison healing. Even today, there is community participation in the healing rituals and patients are not charged for the services rendered. Kerala also developed “a physician for each village” model of health care that gave rise to the tradition of the aSTa vaidyas, who were vested with the responsibility of looking after the health care needs of a specific village. Complete healing takes place in optimized space and time matrix. A favorable spatio-temporal locale has to be discovered for the process of healing to unfold itself. Good health care should be able to enhance expectations and belief and promote awareness in the individual. It should be able to bring a sense of wholeness and completeness to the individual by integrating the physical, psychic and spiritual levels of experience. It should facilitate the cultivation of

mfc bulletin/April-July 2006 personal and social relationships that can enhance the healing mechanisms. It should promote healthy life styles with emphasis on health education. Not to speak of the least, it should provide high quality curative medical services drawing upon the rich and diverse experience codified in different medical systems. Ayurveda recognizes medical pluralism and advises the physician to be open-minded and not restrict himself to the limitations of one system. Though Ayurveda recognizes the role of hospitalized care, it has given more importance to home treatment. Some of the Ayurvedic texts have listed the guidelines for establishing an ideal hospital. But, overdependence on hospitalized care indicates the failure of the health care system in protecting the health of the people. Hospital care is crisis management and not the mainstay of high quality health care. Conclusion A careful study of the Ayurvedic tradition can help us to understand how this ancient medical tradition visualized the quality of health care in a socio-cultural milieu that is quite different from the one in which we live today. However, it would hopefully be possible to glean some universal principles and concepts of quality care that can be reformulated and applied in the modern health care scenario. At a global level, we see an emerging trend that is moving towards a pluralistic health care set up. The role of traditional and alternative systems of medicine will have to be properly defined to develop comprehensive guidelines for ensuring quality of health care. In this context, it becomes necessary to study and understand the vision of health care from the viewpoints of different medical systems so that we can weave and synthesize a truly humanistic approach to health care. To summarize, the following points emerge as the key concepts underlying the Ayurvedic approach to quality of care. 1. The foundation of true healing is compassion. Compassion has to be nurtured as a value that will be the driving force in the health care field. Health service providers must develop empathy and responsiveness to the needs of the sick. 2. Health care becomes service oriented only through

35 patronage of not only the state but also the people. It tends to become a business and exploitative in private hands. 3. The skill to cure must be circumscribed within the larger goal of caring for the sick. While the physician must promptly attempt to cure diseases for which effective remedies are available, he should not indulge in irrelevant medical interventions in obstinate diseases but must continue to care for the patient by creating an environment that is conducive to healing. 4. While quackery should be controlled, misuse of the skills of qualified doctors should also be prevented and the rights of the patients as human beings protected. 5. The control in medical care should be equally distributed amongst the physicians, pharma, nurse and the patient to maximize the treatment outcomes. 6. When cost is a prohibitive factor in accessing high quality medical care, attempt should be made to make available cheaper alternatives by pharmaceutical innovations. 7. Community based health care delivery models should be developed to encourage people’s participation in the health care delivery set up. 8. Apart from state support, community oriented health care delivery mechanisms should be developed to pool in resources that will enable poorer sections of society to access high quality health care. 9. A holistic approach to healing should be developed that recognizes the psychic and spiritual dimensions of the human personality and goes beyond the physical. 10. Quality of care must be ensured by aiming at the goal of establishing healing environments that are homely and enhance belief and expectation, nurture a sense of wholeness, cultivate healthy personal and social relationships, encourage healthy life styles and integrates diverse approaches to curing diseases from different medical systems. 11. High quality hospital based health care needs to be established for crisis management, but this should not constitute the primary mode of health care delivery.

36

mfc bulletin/April-July 2006

Quality Assurance in the National Rural Health Mission (NRHM): Provisions and Debates - Rajib Dasgupta1 The stated goal of the National Rural Health Mission (NRHM) is to “improve the availability of and access to quality health care”. It has also done an elaborate exercise to draw up the Indian Public Health Standards (IPHS) for the Community Health Centres (CHC). Recognising that “75% of the health services are being currently provided by the private sector,” the Mission will also regulate the private practitioners including informal rural practitioners to ensure availability of quality service to citizens at a reasonable cost. The principal thrust areas of the IPHS, as defined by the Mission Document are as follows: l

l

l

All “Assured Services” as envisaged in the CHC should be available, which includes routine and emergency care in Surgery, Medicine, Obstetrics and Gynaecology and Paediatrics in addition to all the National Health programmes. Appropriate guidelines for each national programme for management of routine and emergency cases are being provided to the CHC. All support services to fulfil the above objectives will be strengthened at the CHC level.

l l l

l l l

Every CHC has to provide the following services Assured Services: l

The requirements have been calculated for average bed occupancy of 60%. The standards shall be upgraded if the utilisation pattern improves. The following additional manpower has been sanctioned: l l l

Anaesthetist and a Public Health Programme Manager A Public Health Nurse (PHN) and an ANM An Ophthalmic Assistants where there are no sanctioned posts at present

Other quality assurance measures include: l

1

“re-modelling

or

re-arranging

physical

Centre of Social Medicine & Community Health, Jawaharlal Nehru University, New Delhi 110067. E-mail:<dasgupta_jnu@yahoo.com>, <rdasgupta@mail.jnu.ac.in>. I am grateful to Dr. Ritu Priya, Associate Professor, JNU, for her comments and suggestions.

infrastructure” and undertake new construction as per specifications provide drugs as per the list strengthen support services including laboratory services have Standard Treatment Protocols and Standard Operating Procedures for common ailments and national programmes capacity building of existing manpower have a ‘Rogi Kalyan Samiti (RKS)’ for each CHC to ensure accountability internal monitoring through the programmes, external monitoring through panchayats and social audits through Rogi Kalyan Samitis

l l l l l l

Care of routine and emergency cases in surgery; this includes: l Incision and drainage, and surgery for hernia, hydrocele l Appendicitis, haemorrhoids, fistula, etc. l Handling of emergencies like intestinal obstruction, haemorrhage, etc. l Care of routine and emergency cases in medicine: l 24-hour delivery services including normal and assisted deliveries l Provide emergency care for diseases covered under the national health programmes as per standard treatment guidelines l Essential and Emergency Obstetric Care including surgical interventions like Caesarean Sections and other medical interventions Full range of family planning services including laparoscopic services Safe Abortion Services New-born Care Routine and Emergency Care of sick children Other management including nasal packing, tracheostomy, foreign body removal etc. All the National Health Programmes (NHP) should be delivered through the CHCs. m

RNTCP: CHCs are expected to provide diagnostic services through the microscopy

mfc bulletin/April-July 2006

m

m

m

m

m

l

centres which are already established in the CHCs and treatment services as per the Technical Guidelines and Operational guidelines for Tuberculosis Control HIV/AIDS Control programme: The expected services at the CHC level are being provided with this document which may be suitably implemented.(Annexure National Vector Borne Disease Control Programme: The CHCs are to provide diagnostic and treatment facilities for routine and complicated cases of Malaria, Filaria, Dengue, Japanese Encephalitis and KalaAzar in the respective endemic zones. National Leprosy Eradication Programme: The minimum services that are to be available at the CHCs are for diagnosis and treatment of cases and reactions of leprosy along with advice to patient on prevention of deformity. National Programme for Control of Blindness: The eye care services that should be available at the CHC are diagnosis and treatment of common eye diseases, refraction services and surgical services including cataract by IOL implantation at selected CHCs optionally. 1 eye surgeon is being envisaged for every 5 lakh population. Under Integrated Disease Surveillance Project, the services include diagnosis for malaria, Tuberculosis, typhoid and tests for detection of faecal contamination of water and chlorination level. CHC will function as peripheral surveillance unit and collate, analyse and report information to District Surveillance Unit. In outbreak situations, appropriate action will be initiated.

37 Internal Monitoring § Social Audit: through Rogi Kalyan Samitis/ Panchayati Raj Institutions § Medical audit § Others like technical audit, economic audit, disaster preparedness audit, etc. § Patient care including: Access to patients, Registration and admission procedures, Examination, Information exchange, Treatment §Other facilities: waiting, toilets, drinking water §For Indoor patients: Linen/ beds, Staying facilities for relatives, Diet and drinking water, Toilets External Monitoring § Gradation by PRI (Zilla Parishad)/Rogi Kalyan Samitis Monitoring of laboratory: · Internal Quality Assessment scheme and External Quality Assessment scheme Computerisation “is a must” and has been envisaged for proper maintenance of records and also as a monitoring tool. There are several issues regarding these standards that merit debate and discussion: l

While quality assurance measures are welcome improvements, the IPHS currently focuses only on CHCs. In a significant departure from the previous position CHCs have been identified as providing secondary level care. Nothing has been done about assuring standard for the primary level – most importantly, PHCs and to a less extent SCs. Further, the national health programmes are now to be delivered through the CHC. These changes would substantially weaken the PHC and is against the basic theories and practice of the primary health care approach. The PHC as an institution requires similar standards that should be implemented stringently, given the fact that this is the first refuge of the rural population for both outpatient and inpatient needs – particularly, in pregnancy and childbirth. The Union Ministry is expected to come out shortly with the IPHS for PHCs. .

l

While the NRHM needs to be complimented on attempting to achieve inter-sectoral coordination,

Others: Blood Storage Facility, Essential Laboratory Services, Referral (transport) Services

The following services have been identified for outsourcing: l

Ambulance, Food for patients, Laundry, Water, Electricity, Telephone, Civil Engineering, Waste disposal

The detailed quality monitoring mechanisms are as follows:

38

mfc bulletin/April-July 2006 the IPHS is stuck in a hospital model and has not moved beyond to ensure public health standards in terms of water, environmental sanitation and sewerage.

l

Though the policy document emphasises availability of medicines and laboratory supplies, it does not spell out quality assurance mechanisms for these services.

l

While training and capacity building has been rightly emphasised, training will need to be systematic and standardised in order to be effective; the operationalisation of this key component needs to be spelt out clearly. Improving service conditions have been a genuine demand by doctors for decades. Certain incentives have been offered to the doctors but the 700 odd PHCs that lack doctors generally do so since they are located in backward and underdeveloped areas. Despite the incentives it may well be difficult to attract doctors to these locations. The District Health Missions have been allowed greater “flexibility” to recruit doctors locally on contractual basis, in tune with the contractualisation and privatisation of the services.

l

A large range of essential and critical services like water, sanitation, waste disposal, laundry and other basic services have been proposed to be outsourced. Unfortunately, the responsibility seems to have ended with this recommendation. Experiences with privatisation of various services across the states have highlighted the pitfalls and consequently the need for the state to play the role of a responsible regulator if at all these services merit privatisation.

l

l

The proposal to mainstream AYUSH has been met with cautious optimism. They will be inducted essentially at the primary level. Concerns have been raised about AYUSH practitioners indulging in ‘cross-system practice’. Though a detailed monitoring mechanism has been outlined, its operationalisation is yet to be defined. To what extent can, realistically, the Rogi Kalyan Samitis exercise quality controls? While they can be effective for certain ‘lay’ services, they will not be in a position to intervene in technical matters. Further cautions have been

l

expressed that control by Panchayati Raj Institutions are not to be misconstrued as community controls. The criteria for laboratory quality controls have not been specified. Will they be accredited as per the norms of the commercial pathological and microbiological laboratories, or, will they have separate norms of their own?

The NRHM is being rolled out in the respective states. These provisions are being operationalised in the states. The issues outlined above needs to be debated in order to shape a NRHM that delivers what it is purported to deliver. A key area that needs to be rigorously implemented is the regularity and adequacy of drugs and other essential consumables. Another ‘silent’ area has been the motivation and attitude of health personnel. A common refrain, and often legitimately so, by the personnel is about lack of adequate supplies and equipment; the implementation of the IPHS shall hopefully take care of that and therefore simultaneously the personnel should also rise to the occasion to complement these measure. The regulation of the private sector is completely unaddressed. That needs to be taken up seriously given the heterogeneity of providers and a large range of rational/irrational and ethical/unethical practices. The measures outlined in the IPHS are largely technocentric pertaining only to the CHC (purported to function as a ‘hospital’); standards for the two critical levels - PHCs and SCs are conspicuous by absence. References 1.

<http://mohfw.nic.in/nrhm.html>. National Rural Health

2.

Mission, Ministry of Health and Family Welfare. Government of India (1997). Evaluation of Community Health Centres, Programme Evaluation Organisation,

3. 4.

Planning Commission, New Delhi. Singh S (2005). “National Rural Health Mission”. Yojana, July 2005, pp. 2-6. Dasgupta R and Qadeer I (2005). “The National Rural Health Mission (NRHM) – A Critical Overview.” Indian Journal of Public Health, Special Issue on the NRHM, Vol. XXXXIX, No. 3, July-Sep, 2005.

5.

Shukla A (2005). “National Rural Health Mission-Hope or Disappointment?” Indian Journal of Public Health, op.cit.

6.

Satpathy S K (2005). “Indian Public Health Standards (IPHS) for Community Health Centres”. Indian Journal of Public Health, op.cit.

mfc bulletin/April-July 2006

39

Financing the NRHM -Ravi Duggal1 The NRHM (National Rural Health Mission) makes a faulty start. The mission begins with the statement, “The NRHM seeks to provide effective healthcare to the poor, the vulnerable and to marginalized sections of society throughout the country” (Chapter 1, section 2, page 3). Further, in rest of the document it keeps referring to 18 States as the focus area. One acknowledges that these groups need special support form the public health system but the goal of the program cannot be selective because in doing so it distorts the design. It is well established today that anything designed specifically for the poor or marginalized does not work in practice. If universal access is not at the core of the mission then it will never be able to achieve its goals. Since universal access to comprehensive primary healthcare and referral services, which the 1982 National Health Policy committed, is not stated clearly as a goal, the financing strategy for NRHM also falls into the trap of selective strategy for targeted populations. Hence separate schemes like Rs. 10,000 for untied funds for the subcentres, Rs. 100,000 for rural hospital maintenance if Rogi Kalyan Samitis are formed, Rs. 750,000 per block for training ASHA’s etc., have been worked out, instead of determining what resources would the proposed package of services require in order to implement it. For 2005-06, the mission document states that Rs. 6713 crores have been allocated for NRHM. If we look at the 2005-06 Central government budget we do not see NRHM figuring as a separate budget item. In fact, NRHM is going to use funds of existing programs like RCH-2, NDCP, Integrated Disease Surveillance Project and the AYUSH program (Annex 5, page 3). NRHM is being seen as an omnibus for the above programs (Chapter 3, page 12). Thus in effect NRHM is only a label for selected activities from amongst existing programs. The only “new” component is the ASHA scheme, which is actually a revival of the erstwhile CHV scheme of 1978, which became defunct in the nineties in most states. At the national level today the Central and State 1

Email: <raviduggal@vsnl.com>, Phone: 91-22-26673571

governments spend about Rs.25,000 crores annually on healthcare (excluding water supply and sanitation), which is just about one percent of GDP. If these resources were to be distributed on a per capita basis equitably, then rural healthcare should get Rs. 17,500 crores in contrast to about Rs. 10,000 crores it receives today. Of course, this does not happen because the more expensive hospital services and the elaborate health bureaucracy are located in urban areas. There was great expectation that the Budget 2005-06 will make a marked deviation using the NRHM as the peg for at least launching a process for changing the political economy of healthcare in India. Unfortunately the only mention of the NRHM within the budget is in the Finance Minister’s speech, “The National Rural Health Mission (NRHM) will be launched in the next fiscal. Its focus will be strengthening primary health care through grass root level public health interventions based on community ownership. The total allocation for the Department of Health and the Department of Family Welfare will increase from Rs.8,420 crores in the current year to Rs.10,280 crores in the next year. The increase will finance the NRHM and its components like training of health volunteers, providing more medicines and strengthening the primary and community health centre system.”2 When we look at the expenditure budgets and demand for grants of Budget 2005-06 we find that there is no mention of NRHM as an item of expenditure. The Finance Minister says that the increase (Rs. 1860 crores) over the previous budget will finance the NRHM component. This overall increase of 24% in the budget appears substantial and if it were to be divided equally among all PHCs then each PHC would get additionally about Rs. 8 lakhs. However the budgetary allocations belie this fact when we see that the increase for the HIV/AIDS program is 105% from Rs. 232 crores in 200405 to Rs. 476.5 crores in 2005-6. Similarly for the RCH program the increase is a whopping 94% from Rs. 710.51 crores to Rs. 1380.68 crores, for medical education also a high of 50% from Rs. 912.82 crores to Rs. 1360.78 crores and as much as 80% for Indian Systems of Medicine and Homoeopathy (AYUSH) from Rs. 225.73 crores to Rs. 405.98 crores. Just these four programs account for Rs. 1543 crores (or 83%) of the increased amount of Rs. 1860 crores.

40

mfc bulletin/April-July 2006

Thus the FM’s statement in the budget speech is clearly a populist pronouncement and like all such pronouncements of past budgets similar to the various versions of health insurance packages of different

finance ministers, sickness assistance funds etc.. is pure gas and will disappear as soon as the budget euphoria dies down. The overall budget of the Ministry of Health and Family Welfare is outlined below:

Demand for Grants of Ministry of Health and Family Welfare (Rs. Crores) Category

Budget 2004-05

Budget 2005-06

Medical and Public Health

3103.12

4253.84

AYUSH

225.73

405.98

Family Welfare

6696.37

7769.01

Gross Total Health

10025.22

12428.83

Grants to States and UTs

4663.00

5158.00

Total Health Central govt.

5362.22

7270.83

Less recoveries

(-)1587.10

(-)1741.72

Net Health Central govt.

3775.12

5529.11

Source: Budget 2005-06, Demand for Grants, Demand Nos. 47, 48, 49, Ministry of Finance, GOI, New Delhi, 2005 Those in decision making positions at the Centre and State levels feel that increased resources in the rural areas will not help because there is limited “absorption capacity”. Hence, unwillingness on behalf of these decision makers for fiscal devolution to the district and panchayat levels. This business of absorption capacity is a façade. While governments have created the infrastructure, like hospitals, primary health centres, subcentres, etc., they have not endeavored to assure that the complete inputs for the efficient functioning of these are provided. The government’s own RCH Facility surveys highlights the pathetic conditions of public healthcare facilities, which is largely due to inadequate resources being allocated, but very little has been done to use this most valuable information to improve the public healthcare facilities. Thus this absorption capacity pretense has no meaning; it is sheer indolence that drives this belief leading to curtailment and/or non-allocation of resources for peripheral health institutions. If autonomy 2

Budget Speech 2005, <http://indiabudget.nic.in/ub2005-06/ bs/speecha.htm> 3 This calculation at 2003 prices was done for a Jan Swasthya Abhiyan discussion on the National Rural Health Mission

is given to districts and panchayats to use resources as per their local needs and demands within a defined framework that is open to social audit then one will see a wide range of innovations in setting up local healthcare delivery systems and provision of healthcare for the people. Thus the overall NRHM strategy needs a drastic makeover and reoriented into a universal access framework for which financial resources need to be determined on the basis of needs and demands of people, and this would be best met if resource allocations are based on an assessments of such needs and demands and given to local governments to plan their use autonomously. To conclude the NRHM should be used as an opportunity to work out a new health financing strategy, which devolves financial resources to local governments and uses a social audit framework to monitor its implementation. In Annexure 1 an estimate of financial resource requirements for a universal access comprehensive healthcare system has been made and this can be used as a starting point to redefine the financing strategy.

mfc bulletin/April-July 2006

41

Annexure 1

1 clerk/stat asst., 1 office assistant, 1 lab technician, 1 driver, 1 sweeper – this adds up to salaries and benefits/capitation of Rs. 3,064,000 (salary structures across states may be different and hence this could vary). Doctors and nurses may either be salaried or contracted in on a capitation basis as in the NHS of UK. The curative care component should work as a family medical practice with families (1000 - 2000 depending on density) being assigned to each such provider.

Calculation for Comprehensive Healthcare in India3 1. Primary healthcare (FMP+ES) with following features: l

Staff composition for each PHC-FMP unit to include 4 doctors, 1 PHN, 2 nurse midwives, 8 ANMs (females), 4 MPWs (males), 1 pharmacist, Line item

Rate

Rural (20000 popn. per unit)

Urban (50000 popn. per unit)

Medicine and other clinical consumables

Rs. 25 per capita per year

Rs. 500,000

Rs. 1,250,000

Travel, POL etc.

Rs. 6000 pm rural; and Rs. 3000 pm urban

Rs. 72,000

Rs. 36,000

Office expenses, electricity, water etc..

Rs.10 and 12 thousand for rural and urban, respectively

Rs. 120,000

Rs. 144,000

Maintenance of building and equipment etc.

Rs. 100,000

Rs. 200,000

Rent and/or amortisation

Rs. 144,000

Rs. 240,000

Rs. 480,000

Rs. 495,000

Total Non-salary

Rs.1,416,000

Rs.2,365,000

Total Primary care Cost per unit

Rs. 4,480,000 (Rs. 224 per capita)

Rs.5,429,000 (Rs. 109 per capita)

Rs. 156 billion

Rs. 32 billion

CHW honorarium

Total Primary care cost for country

l l

4

Rural 1 CHW per 500 population @Rs. 1000 pm per CHW; Urban 1 CHW per 1500 population @ Rs. 1250 pm per CHW

Rural: 700 million population needing 35,000 PHCs; and urban 300 million population needing 6000 PHCs

10 beds per PHC Average rural unit to cover 20,000 population (range 10-30 thousand depending on density); average urban unit to cover 50,000 population (range 30-70 thousand population depending on density)

All higher level care would be on basis of referral from primary care providers, except in the case of emergencies. This should help rationalize higher levels of care as well as make it more efficient and cost-effective

l

Non-salary costs separately for rural and urban units per unit cost as per table below:

2. First level Referral Care4 In rural areas for every 5 PHCs there would be one 50 bedded hospital and this would cost Rs. 200,000 per bed per annum or Rs. 10 million per such hospital. As per this ratio we would need 7000 rural hospitals

42

mfc bulletin/April-July 2006

and this would translate into Rs. 70 billion for the country as a whole.

Primary + First Referral + Secondary/Tertiary = Rs. 359.50 billion

In urban areas for each 10 PHCs one 200 bedded hospital would be needed and this would cost Rs. 250,000 per bed per year or Rs. 50 million per hospital. As per this ratio 600 such hospitals would be needed and this would translate into Rs. 30 billion for the country as a whole.

4. Other Costs

3. Secondary and Tertiary care / Teaching Hospitals

Grand Total would be Rs. 431.40 billion or Rs. 431 per capita and this works out to 1.7% of GDP. This calculation excludes medical education and medical research, which would be 15% and 10% of the total healthcare cost, respectively, amounting to an additional Rs.108 billion.

One such hospital per 2.5 million population, that is 400 hospitals of 500 bed each at a cost of Rs. 350,000 per bed per year translating into Rs. 175 million per hospital or Rs. 70 billion for the country as a whole.

Capital @ 10% or Rs. 35.95 billion Research and Data systems @ 4% or Rs. 14.38 billion Admin costs @ 4% or Rs 14.38 billion Audit costs @ 2% or Rs 7.19 billion

Summary Table Type of Cost

Amount in Rupees billion

1. Primary care

189.50

2. First Referral Rural

70.00

3. First Referral Urban

30.00

4. Secondary/Tertiary care

70.00

SUBTOTAL

359.50

5. Capital @ 10%

35.95

6. Research and data systems @ 4%

14.38

7. Admin @ 4%

14.38

8. Audit @ 2%

7.19

TOTAL Healthcare Cost Medical Education and Research Grand Total

431.40 or 1.7% of GDP 107.82 539.22 or 2.1% of GDP

mfc bulletin/April-July 2006

43

Declaration of Women Healers and CHWs Background The context of this Declaration was a four-day meeting of health and development workers and women healers (‘Mahila Chikitsak Samagam’) organised by Omon Mahila Sangathan at the Tribal Research and Training Centre (TRTC) in Chaibasa, Jharkhand (India) from 16th through 19th February 2006. Among the 92 participants, most were women health workers and indigenous traditional healers. They came from six States (Andhra Pradesh, Chattisgarh, Gujarat, Jharkhand, Maharashtra, Rajasthan). Among them they spoke around 20 languages, including Adivasi and local languages like Bhili, Ho, Konkani, Khortha, Santhali and Maghadi, Mewari, in addition to the regional languages Bengali, Chattisgarhi, Oriya, Gujarati, Marathi, and Telugu. Hindi was the main ‘link’ language. Hindi-to-English-to-Telugu translation was done for the Andhra group. Well known herbal medicine experts Dr. P. P. Hembrom and Fr. Sevanand Meloo participated as specially invited resource persons. Various organisations involved in health issues and women’s development were represented (see signatories below). The participants shared their diverse experiences, discussed the traditional healing systems and women’s roles, and focused on women’s health problems and the ways they are dealing with them. One day was devoted to a trip to the Juggidaru forest 20 kms from Chaibasa and, on return, to discussing the many plant specimens collected. A session was devoted to the experience and issues of dais (traditional midwives) and the challenges they face as women healers. Throughout the four days, the issues that came to the fore included destruction of the forests, indiscriminate mining and industrial growth, pollution of land, water and air, over-exploitation of natural resources, agricultural mono-culture and cashcropping, ‘commodification’ of health (vastukaran) for the market, rise in violence against women, etc. On the last day all these changes were seen together in the context of the worldwide economic and political changes called ‘globalisation’ (bhumandalikaran). In order to assert and record their collective views on all these issues, the participants decided to issue the following declaration. Here they also identify the ideal role of a woman traditional healer as ‘guni’. Declaration by Women Healers and Health Workers Chaibasa, 19th February 2006 As women working in the field of health we have come together at this national-level meeting to affirm our healing vocation and to assert our common concerns for the status of health in our communities, and also our concern for the status of women. In this declaration, we

express our view of people’s healing traditions and put forth our thoughts on today’s sober realities and on what is to be done. 1. The ancient healing systems that have evolved among the ordinary people are distinct from the formal medical systems of vaidyas and hakims, although they are reflected in these systems. 2. In our tradition, a genuine healer or ‘guni’ is one among the people who has acquired knowledge and skills passed orally down the generations from older healers. Real gunis do not see their knowledge, skills and medicines as saleable “intellectual property” or as services and goods for the market place. Rather they see this as a sacred gift held in trust to relieve suffering and keep our communities healthy. 3. Our traditions include the following healing skills: l l l l l l l l

use of herbal medicines removal of poisons from the body bone-setting and realignment of joints massage and pressure of certain points re-establishing balance in mentally disturbed states care relating to pregnancy, birth, and breastfeeding an infant prevention of pregnancy and occasionally induction of abortion treatment of women’s problems like anaemia, womb prolapse, troublesome vaginal secretions, and not having children.

In the people’s healing traditions, a ‘guni’ acts as the medium for the healing process, and thus respect is paid both towards the ailing person and to the medicine to be used. For example, when a sick person comes to a guni for help, a ‘mantra’ to gain strength to heal may be said, and likewise the spirit of a medicinal plant may be invoked for the use of its power in healing. 4. Those of us who are dais are glad to have gained new skills through various organizations. On the other hand, we are very disturbed by some widespread abuses of allopathic methods today, such as the use of injections by quack ‘doctors’ to increase labour pains and the high rate of caesarean section operations in private hospitals, etc. It is the Government’s policy to see that all childbirths occur in institutions by the year 2020. This means that the Government does not recognise our skill and role. This is not right and we feel that it will not reduce maternal deaths. 5. It is of extreme concern to us that the nutrition and health in most of our communities is getting worse, and

44 the worst effects fall upon women and children. Women carry more than their share of society’s burden of work while they get less food to eat. 6. Also, violence against women is on the rise. Our experience tells us that those women are targeted whose lives go against the local social and political interests – widows, those whose right to property is coveted, and those who stand up for the rights and health of women. In some parts such women are accused of being ‘witches’ and are even killed for it. We condemn such violent assaults on women. 7. Destruction of the environment is increasing. Forest wealth is depleting from the uncontrolled exploitation by mining, industry and smuggling, and our land, water and air has become polluted. Because of this, not only is our health becoming worse but the very resource base of our healing traditions and medicines is being destroyed. 8. Now we see that the root cause of these widespread, damaging changes is an economic and political transformation going on throughout the world. It is called “Globalisation”. Accordingly, privatization is increasing on a large scale and government controls on trade are being relaxed. This is affecting our communities in many negative ways and pressurizing all those engaged in health service towards narrow, self-seeking and socially destructive behaviour. In view of the present insecure social, political, economic and cultural conditions, we feel the need to engage ourselves in the process of positive change towards a healthy and sustainable society. Along with our attempts to positively strengthen people’s healing traditions, we strongly oppose the negative Government policies that are being shaped undemocratically under the influence of globalisation. A. We stand strongly opposed to traditions that are violent, exploitative and discriminative against women, advasis or dalits. Likewise, we oppose all religious fundamentalism and prejudices against people on the basis of caste, gender, etc. B. While the World Health Organisation has recognised both formal traditional medical systems and folk health traditions, the Government of India only recognizes the formal systems like the Ayurveda, Unani and Siddha. Our Government needs to see the great importance of the widespread “people’s healing traditions” (lok swasthya parampara) in both provision for health as well as conservation of the environment. C. The special issues of ‘dais’ require attention,

mfc bulletin/April-July 2006

l l l l l

namely, government recognition, and ID cards, reasonable payment for pregnancy and childbirth care, better roads and transport provision for referral, better-equipped and staffed hospitals in emergencies, and support for improving the health of mothers. Safe childbirth care should be accepted as a basic right of women. In addition to the new modern skills the dai training module should include traditional skills that can be life-saving. As dais, we refuse to become only “agents” for Government programmes. We will push towards progress on all these issues through organizations of dais in our respective States.

D. We oppose those agriculture-related policies of the Government by which life-giving food substances are today being filled with poisonous chemicals. Policies like these also destroy the bio-diversity of nature, and for this reason we oppose ‘monoculture’ and ‘cashcropping’. We stand along with those groups and lines of thought that are working for the conservation of natural resources, for sustainable agriculture, for proliferation of non-harmful industries, and for democratic distribution of the benefits of wealth. E. We oppose forest and industrial policies that take the control of resources out of the hands of people. In this regard, we appeal to the Government to fully implement “PESA” (Panchayat Extension to Scheduled Areas Act) so that mineral and forest resources stay in the hands of local communities, especially those who have been deprived, and environmental destruction is curbed. Tradition is not static. It is people’s right and duty to build and modify traditions to suit the changing needs and conditions. In this process, we accept the guidance of those who have traveled on this path before us. This is what we believe, and in this we have faith. [SIGNATURES, ORGANISATIONS] BAIF (Dhurwa), CUTS (Chittorgarh), Jan Chetna Manch (Bokaro), Jan Swasthya Sahyog (Bilaspur), Jangal Bachao Andolan (Latehar), LAYA (Vishakhapatnam), Asha Sewa Sadan (Gomia), MASUM & Tathapi (Pune), Omon Mahila Sangathan (Noamundi), Sahaj (Baroda), SARTHI (Godhar), Puleen (Madhupur), Prerna Bharti (Ranchi), Jilla Mahila Samiti (Chaibasa), Chotanagpur Adivasi Sewa Samiti (Hazaribagh), Siddhu Kanu Kisan Sanghatan (Charhi), Asha Vihar (Bokaro), Jharna Adivasi Mahila Sanghatan (Charhi), Jan Vikas Kendra (Maluka) and Jharkhandis Organisation for Human Rights (Chaibasa).

mfc bulletin/April-July 2006

45

Terrorism, Pfizer Style 1 - James Love Pfizer is a big company. It has a market capitalization just over $183 billion, and a workforce of more than 100,000 persons around the world. Pfizer is also pretty aggressive. Recently Pfizer sued the head of the drug regulatory agency in the Philippines, personally, asking for 1.4 million pesos in damages. Pfizer also sued another government official, the regulatory agency itself, and a government owned trading company. Pfizer has also threatened Philippine news outlets, includingtelevision stations, with pull-outs of advertisements if they reporton the dispute, and Pfizer has enlisted the US Department of State to pressure the Philippines government. What is Pfizer up to? Well, they sell a drug, amlodipine besylate, that is marketed by Pfizer in the United States under the trade name Norvasc. It is used to treat hypertension, angina and myocardial ischemia. The drug is sold in two dosage formats: 5 mg. and 10 mg. tablets, and typically taken once a day.

In other words, the Philippines government is allowing Pfizer to price Norvasc out of reach for 95 percent of the population of the country for the entire term of the patent, but they want the cheaper prices foreigners pay, when the patent expires. But this isn’t good enough for Pfizer. Pfizer is suing the government, and government officials personally, so it can stop the process of testing the imports. Pfizer figures this might delay the imports of the cheaper drugs for 18 months. And Pfizer also hopes they can stop the Philippines government from reducing the prices of other Pfizer products, including Lipitor, Zithromax and Unasyn, which are in a similar situation. The legal issues are described briefly in this blog (http://secondview.blogspot.com/2006/03/pfizer-issuing-philippines.html) by my colleague Judit Rius. Pfizer seems to be succeeding in bullying the Philippine government. Apparently the Philippine government has stopped efforts to register the cheaper imported products.

In the Philippines, Pfizer charges from $.88 to $1.46 per day for Norvasc (more for the larger dose). In 2004, the average per capita income in the Philippines was $3.20 per day. Eighty percent of the population lives on less than $2 per day. Pfizer knows this. They have calculated that they can make greater profits selling Norvasc at a high price to a small number of the wealthiest Filipinos (less than 5 percent of the population can afford the drug), than a larger number of people with lower incomes.

This is only the latest installment in a long history of pressure on the Philippines government. For example, check out this astonishing (http://www.cptech.org/ ip/health/c/phil/philtimeline.html) report by Jennifer Ellen Mattson on a 1999 Collaboration between the US government and pharmaceutical industry to oppose Philippine government effort to promote expanded use of generics for off-patented drugs.

The Philippine government is trying to undertake some extremely modest measures to lower the price of this drug. They want to import versions of the drug that Pfizer sells in other countries. Pfizer charges much lower prices for the same drug in Thailand, Indonesia, India and other countries in the region. And, the Philippines government says it won’t even do this until June 2007, when the Pfizer patent on Norvasc expires.

It would be nice if the US news media would report on disputes like this, so US citizens would know what our government is up around the world (note the role of Clinton’s Secretary of State Madeline Albright in the deplorable 1999 dispute), and it would be important also for the public to understand what type of company Pfizer is, and to appreciate why the Pfizer CEO Hank McKinnell is considered somewhat out of control even by other pharmaceutical company executives.

1

< http://www.huffingtonpost.com/james-love/terrorismpfizer-style_b_18290.html>, April 2006

46

mfc bulletin/April-July 2006

Document

Vadodara: The Context of the Dargah Demolition1 The people of Vadodara (also known as Baroda) proudly tell its visitors that their city is a “Sanskar Nagari”, that is, a “city of culture”. It is also referred to as ‘the cultural capital’ of Gujarat. The city does indeed have rich traditions of composite culture. But if the post-1969 history of the communal violence in the city, the 2002 carnage (the notorious Best Bakery incident was in Vadodara) and the recent May 1, 2006 demolition of the Dargah and its aftermath are any indication, the cultural capital of Gujarat has been on a serious decline for sometime…

taken by the civic administration and the police at the instance of the state to signal, once again, to the Muslim community of its second-class citizenship. It was also clearly meant to intimidate and terrorize the minority community. It was a veiled warning to the minority community that another post-Godhra scenario may follow in its wake…

…At the root of the recent demolition of the Sayid Rashidduin Chisti Rahematulla Alay Dargah near the Champaner Darwaja is the skewed idea of what constitutes development: for instance widening of roads, but not housing, nor garbage collection nor drinking water services for everybody. Parts of the city has always seen a boom whereas there is a considerable underbelly of Muslims and Hindus especially in the old walled city and in the Eastern part of Vadodara that is ghettoized, underemployed, thrown out of jobs in organized sector as industry reorganizes post-liberalization, living in nasty, brutish conditions and sitting on a powder keg ready to explode, and this thanks to communal minded politicians of the Sangh Parivar as well as of the Congress.

Who encroaches what is always a question? Is the Dargah, masjid or the temple encroaching the road or the road encroaching the temple/mosque/Dargah? Are hutments encroaching the idea of development, or the idea of development transgressing hutments?

The VMC’s demolition squad, egged on by a nexus of some greedy builder-corporators, the Mayor and the Municipal Commissioner, regularly demolish hutments without giving alternative accommodation - inspite of considerable ULC land being acquired or otherwise allocated for this purpose. It is as if the VMC has a monthly quota of demolitions to be fulfilled come what may. Instead, it is the poor and the minorities who are routinely terrorized in the process. The demolition of the Dargah, directed by Mayor Sunil Solanki and his elected cohorts, served twin purposes: it painted the Mayor in “pro-development” colors among the Hindu majority and it won him brownie points in the Sangh Parivar’s competitive fratricidal, and genocidal, politics: witnesses report the indecent thumping foot stamping and frenetic dancing on the freshly carpeted demolished Dargah site by the Sangh Parivar followers, and distribution of sweets thereafter in nearby Hindu majority areas. Events before and after the demolition of the Dargah clearly indicate that the entire exercise was under1

Vadodara: Violence on Gujarat’s “Gaurav” Day - A PUCL Interim Report, May 1-13, 2006. Full report available at <www.pucl.org>.

…Sure enough they destroyed a lot of minor temples that have recently sprouted – but major temples on the path of the city’s traffic have been untouched.

Adding to this cauldron is the role of Police who is hostile to the poor and to Muslims, and an unsympathetic and largely communalized language press. Most women continue to be upset about the manner in which the police behaved with them – using sexually abusive language and gestures - during curfew and as the violence was raging all over the city. That apart, across communities, women have confronted police authorities with respect to arrests of youth in their areas. Muslim women were extremely agitated at the random police firing and this sentiment which rose in one voice that it would have been far easier for everybody concerned, if they were lined up and shot at one go. Demolition of Dargah: What Happened at Yakutpura The incidents occurred on the main road from Mandvi gate to what is called the Champaner darwaza. The Dargah is close to the Champaner darwaza. As one comes from Mandvi gate, the Muslim-dominated area of Yakutpura and the Hindu-dominated Mehta Pol is to the right of the Dargah, whereas Jagmal ni Pol and Bajwada are to the left. The Dargah is on the main road - almost to the left of the road. The people of Yakutpura told us that on 1 st May they had gathered around the Dargah to protect it from the demolition squad. The people milled around the Dargah and they also said that Nalin Bhatt, ex-BJP Minister, and others had gathered near the Dargah well before the operation began.2 According to the people in Yakutpura they had gathered around the Dargah peacefully. They said stone throwing began from Mehta Pol - a Hindu area near the Champaner

mfc bulletin/April-July 2006 Darwaza. Hindus on the contrary allege that the stone throwing began from the Muslim-dominated Yakutpura. As a result, people ran helter-skelter and the police opened fire. Chaos reigned for more than two hours. The warning fire was hardly noticed by the people. ...Two boys died on the spot. Some women who stay in houses right next to the spot where the firing took place told us that “we were all huddled on the first floor and saw everything.” They were also very agitated. Some women told us that “if they want us to die they should kill us straightaway,” “what if these bullets had hit some young kids in houses, who is accountable for this?” The police fired 36 rounds according to the people. They have also collected used and unused tear gas shells. We saw bullet marks on the laris and the walls and it is very clear that it was indiscriminate firing although the Police Commissioner later claimed that the bullets probably ricocheted…. Farce of Talks and Betrayal thereof ….It is very clear from multiple evidences gathered that demolition of the Dargah was slated anyway for 1st May and probably the decision to demolish was taken at least three weeks before.

47 demolition. According to a member of the Muslim delegation having talks with the VMC over the Dargah, during the mid-night of Sunday, and morning of Monday April 30-May 1, 2006 (around 1.00 am!) the Mayor rang up the Muslim leaders and told them to come for yet another meeting on Monday 1 st May at 9 am. Many of the Muslim leaders did attend the meeting at such short notice – a meeting in which the Mayor Sunil Solanki and Municipal Commissioner Rohit Pathak and Standing Committee Chairman Dinesh Choksi were present among others. Also in the same meeting, the Municipal Commissioner told the Muslim leaders that their proposal for trimming the Dargah structure was put up before the elected body but was not accepted by the Mayor. The talks apparently failed to evolve any mutually acceptable solution. And the meeting dispersed at about 10 am with an assurance that they will meet again to discuss the issue. In the meanwhile, preparations for demolition of the Dargah had already begun at the site – which means that the demolition machinery, the mechanized road carpeting equipment, and dumpers, had already proceeded with an intention to destroy the Dargah, before 10 am, even as the meeting was on to find a constructive alternative.

Talks were on during the previous 10 days (that is 10 days before 1st May) with a select group of Muslim leaders on finding alternatives to the demolition of the Dargah. The Muslim leaders while making clear that the Dargah cannot be demolished in toto o r shifted (for reasons enumerated above by Sultan Miyan Malek), they offered to trim the dimensions of the Dargah. This more practical alternative was first proposed by the Municipal Commissioner; it was discussed by the select Muslim leaders with their larger community and accepted in principle. This alternative proposal was once again discussed on the morning of 1st May although the Mayor denied that they had discussed any such proposal.

The Mayor, on being asked by the PUCL team, why he did order the demolition even when the Muslim leaders were given the impression that they would meet again and discuss, said that the demolition was anyway scheduled for that day, that is Monday May 1, 2006. “And if we did not do it immediately, they may obtain a stay from the Courts.”

The Police Commissioner Deepak Swaroop had warned the Mayor not to go ahead with the demolition specifically on 1st May as several other events which needed Police bandobast were scheduled for that day. The Police Commissioner made it public that he had written a letter to the effect “all the way to the top.” Nevertheless the Police prepared for the job by requisitioning additional reinforcements on April 29/ 30. Some Police troops were stationed at the Dargah site from the morning (5 am) of 1st May – this happened despite differences of opinion between the Mayor and the Police Commissioner on the issue of

What indeed was the extreme urgency in sticking to this devilish schedule of demolition?

2 A fact affirmed by Nalin Bhatt himself on cable TV channels and the local print media.

Note the chicanery and the cynicism of Mayor Sunil Solanki and Commissioner Rohit Pathak in the entire process. They promised the Muslim leaders further discussions even as they were planning to demolish the Dargah and had no intention of meeting again, as if the act of demolition was an inevitability, an act foretold.

Would the skies have parted and the gods hurled their wrath at the Mayor and his team if demolition was not carried out on 1 st May? Indeed which of the BJP/Sangh Parivar gods would have hurled their wrath on Mayor Sunil Solanki if they did not demolish on 1 st May? What does it say of an administration that cannot exercise restraint and cares two hoots about what happens to the trampled religious feelings of a people?

48

mfc bulletin/April-July 2006

And what it can do to vitiate the atmosphere of the city? Why could not the Mayor and VMC resort to routine democratic norms when the Muslim leaders were more than willing to accommodate the needs of development “so-called”? Why is it that the VMC ignored the firm and clear warning of the Police? Were the discussions then a smokescreen and a deliberate sham? When we asked the Mayor why he went to the trouble of having discussions if he was sure the Dargah had to be demolished anyway for the “development” of the city, he said, “discussions have to be held, otherwise some human rights group will pop up and say I have violated human rights.” Obviously, Mayor Sunil Solanki is of the opinion that he has not violated human or any other rights if he had had discussions, even if it were for appearances. Such wisdom.

Subscription Rates

Annual Life

Rs. Indv.

Inst.

U.S$ Asia

Rest of world

100 1000

200 2000

10 100

15 200

The Medico Friend Circle bulletin is the official publication of the MFC. Both the organisation and the Bulletin are funded solely through membership/ subscription fees and individual donations. Cheques/ money orders/DDs payable at Pune, to be sent in favour of Medico Friend Circle, addressed to Manisha Gupte, 11 Archana Apartments, 163 Solapur Road, Hadapsar, Pune - 411028. (Please add Rs. 15/ - for outstation cheques). email: masum@vsnl.com MFC Convener Ritu Priya, 147-A, Uttarakhand, JNU, JNU Campus, New Delhi -110 067. Email: <convenor.mfc@vsnl.net>.Ph: 011 26185102 MFC website:<http://www.mfcindia.org>

Contents Impressions from a Rural Laboratory Surgical Care for Rural India – A Perspective Excessive Use of Screening and Diagnostic Tests Are Glass Syringes Inferior? The Quality and Cost of Health Care: Some Notes on the Context Quality and Costs of Health Care in the Context of the Goal of Universal Access Cost Containment in Trauma Care Services What can we learn from the AIDS Movement? Quality of Medical Care: Public versus Private Some Strategies to Cut Health Care Costs Medical Facilities and Health Needs on Wheels: Still Unviable? Rational Drug Therapy: Principles, Realities and Road Ahead Quality of Care: The Ayurvedic Perspective Quality Assurance in the National Rural Health Mission (NRHM): Provisions and Debates Financing the NRHM Declaration of Women Healers and CHWs Terrorism, Pfizer Style Vadodara: The Context of the Dargah Demolition

- Jan Swasthya Sahyog - George Mathew - Anant Phadke - Jan Swasthya Sahyog, Bilaspur - Binayak Sen

1 5 6 10 14

- Sara Bhattacharji - Jacob John - Anand Zachariah - Alpana Sagar - Vinod Shah - Dr. N. Kannan - Sujith J Chandy - P. Ram Manohar

15 16 17 21 26 28 29 32

- Rajib Dasgupta - Ravi Duggal

36 39 43 45 46

- James Love

Editorial Committee: Anant Bhan, Neha Madhiwalla, Dhruv Mankad, Amita Pitre, C. Sathyamala, Veena Shatrugna, Chinu Srinivasan. Editorial Office: c/o, LOCOST, 1st Floor, Premananda Sahitya Bhavan, Dandia Bazar, Vadodara 390 001 email: chinumfc@icenet.net. Ph: 0265 234 0223/233 3438. Edited & Published by: S.Srinivasan for Medico Friend Circle, 11 Archana Apartments, 163 Solapur Road, Hadapsar, Pune 411 028. Views and opinions expressed in the bulletin are those of the authors and not necessarily of the MFC. Manuscripts may be sent by email or by post to the Editor at the Editorial Office address. MEDICO FRIEND CIRCLE BULLETIN PRINTED MATTER - PERIODICAL Registration Number: R.N. 27565/76

If Undelivered, Return to Editor, c/o, LOCOST, 1st Floor, Premananda Sahitya Bhavan Dandia Bazar, Vadodara 390 001


