---
title: "Memorandum on Polio Eradication Initiative in India"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Memorandum on Polio Eradication Initiative in India from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC311.pdf](http://www.mfcindia.org/mfcpdfs/MFC311.pdf)*

Title: Memorandum on Polio Eradication Initiative in India

Authors: 


medico 311 friend circle bulletin June-July 2005

How the Two-Child Norm in Himachal was Withdrawn -Subhash Mendhapurkar1 (Even as we write this the Governments of Gujarat and Maharashtra have opted for the two-child norm, with the latter even intending to not supply irrigation water for those having more than two children. It is in this context the decision of the Himachal Government to withdraw the two-child norm comes as a rare act of sanity.) During a casual meeting with the Secretary, Rural Development & Panchayat, in early 2000, I learned he was planning to introduce the two-child norm. This officer, essentially a good human being, was concerned with population growth and wanted to do something, so he said, “I wish I can impose this ban on all elected representatives, but I cannot do this, so I am going to do what I can and that is to make these ayarams to get out of Panchayat elections if they had more than two children.” I tried to explain to him that his intended move was anti-woman and it would force large number of women to either forget contesting elections or go in for MTPs. A junior woman IAS officer sitting next to him said, “A woman who cannot decide when to become mother, should not be allowed to contest any elections, not only this, but such women should be barred from all public affairs. A woman who cannot decide about her fertility is no good role model for other women nor she would be capable of taking independent decisions in Panchayats, such women who would act as proxy, bring bad name to all womanhood.” I was taken aback by this unaccepted support my Rural Development Secretary was getting. Even today, I am not able to decide whether this woman officer was Email:<sutra@sril.com>. The author is Director, SUTRA, Jagjitnagar, Dt. Solan, H.P.

1

putting her argument more to please her senior or she sincerely believed this. I again tried to explain my friend that such coercive methods are of no use and I reminded him of Emergency days and methods used and results of the same. I also asked him, how much his government is willing to spend resources on empowering women so that they are equipped with an ability to take decisions regarding their fertility. I quoted him NHFS results and also asked him, if the TFR of HP is much below the national average, why should he initiate such move. Once the NHFS results were quoted, he started ‘re-thinking’ and asked me to submit a note. I did that within the next 4 days, but by this time, he also moved and submitted his note to the Cabinet. After going through my note, he regretted his ‘rapid move’ on this front and tried to withdraw the note to Cabinet, but by this time, the politicians fancied the idea and made a public commitment. The ball was out of court. Our handicaps during this period was non-availability of authentic data on girl child sex ratio - as census results were not out nor did we have knowledge about collecting data from State Registrar of Births and Deaths. Anecdotal data on female feticide was of no use. The State Government went ahead and imposed the two-child norm for Panchayats. One thing that we had learned from the experiences of Rajasthan group was not to approach the courts - as they had already lost the case and we were sure that the HP government would quote the Rajasthan High court orders and we would lose the battle. Instead of this, we decided to approach people as well

mfc bulletin/June-July 2005 as leaders of the Opposition. The Congress Party, which was in opposition in those days, initially refused to even debate the issue in the House. But then we got some ‘good’ Congress party activists, through whom we were able to get our note to the Leader of Opposition. Surprisingly, this leader (who is now the Chief Minister, Shri Vir Bhadra Singh) decided to debate the issue in the House and he gave a spirited speech opposing the two-child norm, promising on the floor of the House that if his party returns to power, the first thing, he would do is to remove this norm. I think extracting a public commitment from Shri Vir Bhadra Singh helped us a lot.

2 During this year (2004) we were offered by the National Commission for Women to undertake preparation for a study on Situational Analysis of Himachal Women. When the Commission held a public hearing on our Report, we again used this opportunity to make our views known regarding the declining girl child sex ratio and two-child norm. Towards the end of the Public Hearing, the State Officials had to give a promise to remove the two-child norm. Fortunately, we had a sensitive officer heading this department and we started sharing information with him on this issue.

Once the Census data were available, we again started raising the issue but the Government was not ready to listen.

It took about 12 months to convince the Secretary Rural Development and Panchayat. He had spent this time in collecting data for the argument that he would have to put before the Cabinet. We were all aware that almost all his senior officers either were pro-two child norm or ignorant of the issue.

When the elections were declared in 2003 for the State Assembly, we took two issues for campaign – one an issue at national level (Gujarat genocide) and second, the anti-woman policies of State Government (twochild norm and declining expenditure on women’s empowerment program).

The Health Secretary was convinced that declining girl child sex ratio is a bane on the State and he was convinced that removal of two-child norm might act symbolically. This helped us a lot as the Health Department stopped making noise on ‘increasing population’.

We failed to convince people that the two-child norm is a violation of human rights, but nevertheless people perceived this as a discriminatory approach of the Government (people wanted the same norm for MLAs and MPs). Nevertheless, it helped us in taking a step ahead.

We also consider that the resolutions send by various CBOs helped to create enabling environment amongst the politicians – especially the Chief Minister and Minister for Panchayats – when the Secretary Rural Development and Panchayat moved the file for cabinet approval.

After the elections, the job was to keep on reminding the Chief Minister of his promise.

Two other things helped us:

To do this, we undertook advocacy at two levels: one at people’s level and another through media.

A presentation prepared by Abhijit Das and the documentation of People’s Tribunal held in Delhi on the issue – in which Panchayat representives of HP participated.

Almost all our CBOs (Panchayat Mandalis and SHGs) kept on sending resolutions to the Hon. Chief Minister reminding his promise (of removing the two-child norm). With support from Population Foundation, we organized a workshop with MLAs on declining sex ratio. We had a State-level Press conference on this issue and we raised the issue of two-child norm. We received a good coverage from the press barring a few who were very ‘loyal’ to the party in power and they felt publishing our views may attract ‘anger’ from Raja Sahib (as Shri Vir Bhadra Singh is known in the State). During this workshop, we understood that instead of working with Health Department, we need to work with Rural Development and Panchayati Raj Department if we want to get this norm removed.

Other data were also used – we found that the areas where the girl child sex ratio was best in the State, had attracted maximum dis-qualifications of Panchayat Representatives under two-child norm and none from the areas where the girl child sex ratio was lowest. We used this data (Abhijit calls this as proxy data) to further advance our views. But another unfortunate thing happened – Secretary Rural Development and Panchayat who was successful in getting Cabinet approval for the removal of twochild norm left the State and the present Secretary Rural Development and Panchayat refused to talk with us on this issue. We had practically lost our hope that the current Secretary, Rural Development and Panchayati Raj, a lady, would not take any initiatives for expediting the presentation of bill on Amendments to Panchayati Raj Act. The standard response was that the Cabinet decision has been sent to Law

mfc bulletin/June-July 2005

3

Department and after getting their approval the same shall be tabled in the house – when, no one knew.

wife who is a woman activist and a friend of many of us - she is an ex-McArthur fellow.

We were also worried that the recent Supreme Court directives on two-child norm, which referred to all the six States having two-child norm for Panchayats, may be used as an excuse by the Law Department to postpone the introduction of the Bill. Fortunately this did not happen.

We prepared ‘talking points’ in support of the Bill and distributed to the MLAs and we are now sure that the Bill would get the Governor’s nod before the next Panchayat elections. The most difficult officers to convince at the State level are the women officers from State or All India cadre – how do we go about it? We need to evolve strategies for this. May be special workshops should be organized.

We started chasing the previous Secretary (Rural Development and Panchayati Raj) and tried to make the issue as ‘an issue of prestige’ for him – of course this was done very delicately and mainly through his

Please do not approach the Courts – we have not been able to sensitize the Judges as yet.

Withdraw the Ban on People with More than Two-Children from Contesting for Panchayat Elections: A Memorandum1 We are deeply disturbed by State Governments banning persons with more than two-children from contesting for Panchayat elections. Enforcing a two-child norm, using coercion to lower fertility rates, imposing penalties and now banning people with more than two children from contesting Panchayat elections are unnecessary and unwarranted. Any policy or legislation that bans people with more than two children from contesting for elections is discriminatory, disempowering, and discouraging of democracy. One, it negates the spirit of the 73rd Constitutional Amendments by preventing women, younger people and those belonging to the weaker sections of society from participating in democratic elections. In many states the ban on people with more than two children from contesting Panchayat elections has emerged as the main reason for removal of elected members of Panchayats. Such a condition has no nexus with the duties and responsibilities of Panchayat members. Two, imposing penalties is clearly biased against the poor, the non-literate and socially disadvantaged groups in society – the same groups that have historically faced discrimination and neglect and whom the 73rd Amendment seeks to include in the grassroot democratic institutions of Panchayats and empower. Three, women, whether as elected representatives or their wives are severely affected due to the continuing lack of autonomy of women in taking fertility decisions. Further, given the strong son preference in our society, any enforcement of the two-child norm on Panchayat representatives will increase discrimination against the 1 Submitted to Shri Mani Shankar Aiyar, Union Minister of Panchayati Raj, Government of India, New Delhi, by health activists and health groups.

girl child and worsen the already declining child sex ratio. Studies have shown adoption of practices like deserting wives, neglected female children, sex selective abortions, denying paternity of children, adopted by elected representatives to avoid disqualification which are adverse to women. Four, imposing penalties is unnecessary when most people, particularly women, even the poorest, and those living in rural areas and belonging to minority groups, want to have fewer children. According to the National Family Health Survey-2 of 1998-99 almost half (47 percent) of ever-married women consider two to be the ideal number of children and 72 percent women consider two or three to be ideal. The same survey also revealed that 72 percent with two living children and 86 percent of women with four or more children do not want to have any more children. Why then do women have more children than they would prefer to? The fact is that the state has failed to provide to a vast majority of women adequate access to safe and appropriate reproductive health service, improved services for child survival, and the freedom to make fertility choices. Five, such state legislation is inconsistent with the National Population Policy 2000 (NPP 2000) that does not support or recommend any coercive actions or penalties. On the contrary, the NPP 2000 emphasizes the importance of empowering women for improved health and nutrition, promoting child health and survival, and meeting the unmet needs for family planning services – proven measures for achieving population stabilization proposed at the International Conference on Population and Development (ICPD 1995) and endorsed by India.

mfc bulletin/June-July 2005 Six, banning persons with more than two children from contesting for Panchayat elections is likely to have an insignificant impact on population stabilization. According to projections, 75% of the projected increase in India’s population of 392 million between 2001-2026 will occur due to the ‘population momentum’ (given the age structure with a large proportion of women in the reproductive age group). The impact therefore of banning people with more than two-children from contesting elections is likely to be insignificant as the remaining 25% of the population increase will be accounted for by those living longer and having more than two children – only a small proportion of whom would contest Panchayat elections. Seven, misconceptions of a population explosion, rather than ground truths and empirical evidence has been shaping public perceptions and influencing decisions. For instance, both the statements: (1) “… the fact remains that the rate of population growth has not moved one bit from the level of 33 per thousand reached in 1979” and (2) “The torrential increase in the population of the country is one of the major hindrances in the pace of India’s socio-economic progress” are not correct. Fertility transition in India is well underway. Population growth rates continue to fall across India and in all states. Birth rates are steadily declining. Replacement level, or close to replacement level fertility, has been reached in Kerala, Tamil Nadu, Karnataka, Goa, Andhra Pradesh, Himachal Pradesh, Delhi and Punjab.

4 Annexure Population Fact Sheet 1. Good news on the population front 2. Negative impact of imposing two-child election norms 3. Limited impact of two-child norm on India’s population growth 4. Arguments against penalties 5. Arguments against China’s one-child policy 6. Economic prosperity and population size: common misconceptions 7. Population stabilization: six policy lessons 1. Good News on the Population Front •

Fertility transition in India is well underway. Birth rates have been falling – and continue to fall.

•

India’s annual population growth rate that was 2.14 percent between 1981-91 fell, for the first time, to below 2 percent (1.93 percent) between 1991-2001.

•

There is all-round slowing down of the rate of population growth in all states.

•

India’s birth rate has fallen steadily from 45.5 in 1951 to 28.2 in 1991-2001.

•

Total fertility has declined in all states since the early 1970s. Between 1992-93 and 1998-99, when the National Family Health Surveys I and II were conducted, India’s Total Fertility Rate (TFR) declined by almost half a child.

•

Replacement level, or close to replacement level fertility, has been reached in Kerala, Tamil Nadu, Karnataka, Goa, Andhra Pradesh, Himachal Pradesh, Delhi and Punjab.

•

It is true that the TFR is high in Uttar Pradesh, Bihar, Madhya Pradesh and Rajasthan. But even in these states, the TFR has been declining though not as fast. Between 1992-93 and 1998-99, for instance, TFR fell in Uttar Pradesh from 4.82 to 3.99, from 4 to 3.49 in Bihar, and from 3.90 to 3.31 in Madhya Pradesh.

•

According to NFHS-II for 1998-1999, there was no state where women in the age group 20-24 years gave more than 3 as the mean ideal number of children. Thus women’s views are fast approaching the ‘two-child’ norm, although this may take a little longer in the states of Uttar Pradesh, Bihar, Madhya Pradesh and Rajasthan.

We therefore urge and request the Hon. Minister to immediately 1. Advise State Governments and write to State Chief Ministers to withdraw such legislation as it has no nexus with roles and responsibilities of Panchayat members. It is inconsistent with the National Population Policy 2000 and adversely affects women, young people, dalits, and weaker sections of society without necessarily serving the purported intent of rapidly achieving population stabilization. 2. Request the Minister of Health and Family Welfare to write to the State Governments re-emphasizing the approach of the National Population Policy 2000 3. Move the Supreme Court for a review of the Apex Court’s Judgment dated 30 July 2003 on Haryana Panchayati Raj Act 1994 on the above grounds and that Government of India could not present its point of view.

mfc bulletin/June-July 2005

5

Population Growth Trend from 1951-2001 Years

Total Population (in crores)

Absolute Increase (in crores)

Decadal Growth Rate (%)

Average Annual Exponential Growth Rate (%)

1901-51

23-26

13

1951-61

36-44

8

21.6

1.96

1961-71

44-55

11

24.8

2.22

1971-81

55-68

13

24.6

2.2

1981-91

68-84

16

23.9

2.14

1991-2001

84-102

18

21.3

1.93

Source: Census of India

2. Negative Impact of Imposing Two-Child Election Norms Disqualification on Account of Two-Child Norm (%)

Disqualification on all Other Grounds (%)

Madhya Pradesh Haryana

54 87

46 13

Rajasthan

63

37

Chhattisgarh

68

32

Source: Mahila Chetna Manch, Bhopal (Nirmala Buch 2005)

Disqualified for Violation of this Norm after 2000 Elections Disqualification for Violation of the Norm After 2000 Elections (numbers) Andhra Pradesh Chhattisgarh Haryana Madhya Pradesh Orissa Rajasthan Total

No data 766 1350 1140 No data 508 3764

Source: Mahila Chetna Manch, Bhopal (Nirmala Buch 2005)

mfc bulletin/June-July 2005

6

3. Limited Impact of Two-Child Norm on India’s Population Growth

Pradesh, and of Rajasthan are projected to rise by 45-50 per cent.

According to population projections by Dyson, Cassen and Visaria -

¾ The projected increase for Uttar Pradesh (former) between 2001-2026 is 55 per cent.

For India

¾ Nearly a quarter of the total population increase of 392 million is projected to occur in Uttar Pradesh (former).

¾ India’s population will increase by 392 million – from 1021 to 1419 million during 2001-2026 – a rise of 38 per cent. ¾ The crude birth rate will decline appreciably (from 24.8 per 1000 population in 2001-6 to 16.1 by 2021-260 because of falling total fertility from 2.84 (over 2001-06) to 1.94 (by 2021-2026). ¾ The proportion of population aged 0-14 years is set to decline considerably. During 2001-26, it falls from 34.4 per cent to 23.2 per cent. ¾ The absolute size of the population 0-14 years will also fall from about 353 to 329 million. For Major States ¾ For the already relatively low TFR states – Andhra Pradesh, Karnataka, Kerala, Maharshtra, Orissa, Punjab, and West Bengal – projected growth during 2001-2026 is in the range of 20-30 per cent. ¾ Populations of former states of Bihar and Madhya

¾ 55 per cent of the total population growth between 20012026 is expected to happen in four states – Uttar Pradesh, Bihar, Madhya Pradesh and Rajasthan. One way of finding out the degree to which future growth will be due to population momentum is to compare the present state-level populations for 2026 with those arising from projections in which the TFR drops immediately, that is by 2001-6 to 2.1, and remains there. The results are presented in the following table. The main results are as follows:  Roughly 75 per cent of the projected increase in population of 392 million between 2001-2026 will be due to population momentum.  Population will increase even in the 8 states where the TFR has already fallen to near replacement levels due to the population momentum. These states are Andhra Pradesh, Karnataka, Kerala, Maharashtra, Orissa, Punjab, Tamil Nadu and West Bengal.

Analysis of Projected Population Growth During 2001-2026 States

Projected Population 2026

Increase during 2001-2026

Projected Population 2026 (TFR=2.1)

Increase due to Momentum

1

Andhra Pradesh

91.7

15.9

91.7

100

2.

Assam

36.0

9.4

35.7

96

3.

Bihar (Fmr)

166.2

56.4

143.6

60

4.

Gujarat

68.1

17.5

67.8

98

5.

Haryana

29.4

8.3

29.0

95

6.

Karnataka

65.5

12.8

65.5

100

7.

Kerala

38.4

6.6

38.4

100

8.

Madhya Pradesh (Fmr)

117.9

36.8

105.9

67

9.

Maharashtra

123.9

27.1

128.4

100

10.

Orissa

45.5

8.7

46.5

100

11.

Punjab

31.0

6.7

31.9

100

12.

Rajasthan

83.3

26.9

75.3

70

13.

Tamil Nadu

71.7

9.6

71.7

100

14.

Uttar pradesh (Fmr)

271.0

96.5

223.3

50

15.

West Bengal

104.3

24.1

104.2

100

ALL INDIA

1419.2

392.2

Source:

75

Excerpted from Tim Dyson, ‘India’s Population - The Future’, Table 5.7, page 96, in Tim Dyson, Robbert Cassen and Leela Visaria (ed). Twenty-first Century India: Population, Economy, Human Development and the Environment. Oxford University Press, New Delhi, 2004.

mfc bulletin/June-July 2005

7

4. Arguments against Penalties

them a bad image with serious negative consequences.





Imposing penalties makes little sense when people indeed want to have fewer children. Most people, even the poorest, even those living in rural areas and belonging to minority groups, want to have fewer children. For example, the National Family Health Survey-2 of 199899 asked each woman the number of children she would like to have if she could start all over again. Almost half (47 percent) of ever-married women consider two to be the ideal number of children and 72 percent consider two or three to be ideal. The same survey also revealed that 72 percent with two living children and 86 percent of women with four or more children do not want to have any more children. People’s knowledge of contraceptive methods is nearly universal. The real problem is that a vast majority of women lack adequate access to safe and appropriate reproductive health services and the freedom to make choices. Imposing penalties on those having more than two or three children has little moral or ethical justification. Penalties tend to be unfair and inequitable in terms of whom they affect in society. According to NFHS-2, The Total Fertility Rate among women in the reproductive age group 15-49 years is higher in rural areas (3.07) than in urban areas (2.27). The TFR is higher among Scheduled Caste and Scheduled Tribe communities than among the rest of the population. The TFR is higher among nonliterate women than among those who have been educated beyond Class X. Clearly, any measure that imposes penalties is clearly biased against the poor, the non-literate and socially disadvantaged groups in society – the same groups that have historically faced discrimination and neglect.



Implementing any system of penalties can quickly get abused and reduced to tokenism. Moreover, in many instances, they can become impractical. People will tend to find many loopholes to get out of paying the penalty. What if a family were to give over two of the ‘extra’ children for adoption?



Imposing penalties in many instances seems to go against the spirit of the Constitution that assures every child equal rights. For instance, to deny the third child either free education or even subsidized food seems utterly unjust and unfair.



Finally, it is not true that politicians who support penalties and restrictions on family size acquire a positive image. This is not going to get votes. As the Indian electorate has signaled time and again, politicians get reelected only if they are genuinely pro-poor, not because they feel people should have fewer children. On the contrary, support for imposing penalties is likely to give

5.

•

Arguments against China’s One-Child Policy The main arguments against the Chinese experience are summed up below: Limiting the number of children a family can have is not necessary when almost all countries in the world (including Bangladesh and Indonesia) have been able to lower birth rates without limiting family size.

•

There is little evidence to support the argument that limiting family size like in China yields quicker results. Kerala and China had similar fertility rates in 1979 when the one-child policy was introduced. Today, Kerala reports lower fertility rates than China. On the other hand, many other countries like Bangladesh have shown that it is possible to dramatically reduce fertility rates over a short period of time without having to limit family size.

•

Enforcing a one-child policy may be possible in an authoritarian country like China. But enforcement of such a measure is likely to have disastrous political consequences in any democracy like India.

•

Even in China, the one-child policy was accompanied by a broad and equitable expansion of social and economic opportunities for women – the proven way to reduce fertility rates.

•

Imposing restrictions on family size will promote further discrimination against the girl child. In China, for instance, the sex ratio at birth is highly skewed in favour of boys and against girls. As against a ratio of 105 boys to 100 girls at birth that is reported by most countries, the sex ratio in China is as high as 112 boys to 100 girls – reflecting the serious consequences of son-preference and anti-female biases. It is now abundantly clear that given the ideology of son-preference in India, particularly marked in the high fertility areas of the country, a vigorous pursuit of even a two-child norm is an invitation to sex-selective female abortion.

•

China’s one-child policy has created several other problems. Adoptions, for instance, rose sharply from around 200,000 before the one-child policy was introduced to around 500,000 a year in 1987. A significantly higher proportion of girls are put up for adoption than boys. Abortion rates have also gone up considerably in China causing considerable damage to women’s health.

•

Finally, imposing restrictions on the number of children violates people’s freedoms and individual rights. As Dr. Manmohan Singh writes: “A sensible approach to the regulation of fertility must

mfc bulletin/June-July 2005 respect the fundamental rights of parents to make informed choices about the number of children they wish to have and the types of spacing they would prefer in deciding about their family size.” (Dr. Manmohan Singh, “Population, Poverty and Sustainable Development,” February 3, 2003). 6. Economic Prosperity and Population Size: Common Misconceptions 1. There is no automatic association between population size and economic well-being. China, the only country with a larger population than India, reports a per capita income that is almost 70 per cent higher than India’s. Nepal and Malaysia have the same population – 24 million – and yet Malaysia’s per capita income is more than six times higher than Nepal’s. Zambia and Belgium have the same population size – 10 million. Yet, Zambia’s per capita income is barely 3 per cent of Belgium’s. 2. The lack of any obvious association between population size and per capita is evident even within India. Andhra Pradesh (76 million) and Madhya Pradesh (80 million) reported similar levels of population in 2001. Yet, most recent estimates reveal that, in 1997-98, the per capita Net State Domestic Product in Madhya Pradesh was only Rs.8,114 – almost 30% lower than the per capita Net State Domestic Product (Rs. 10,590) in Andhra Pradesh. Karnataka and Rajasthan report similar levels of population – 53 million and 56 million respectively in 2001. Yet in 1997-98, per capita Net State Domestic Product in Rajasthan was only Rs.9,356 – almost 20% lower than the per capita Net State Domestic Product (Rs. 11,693) in Karnataka. 3. The association between population growth and economic growth is weak. Levels of per capita income in the ten most populous countries vary enormously from a low of PPP US$ 780 in Nigeria to a high of PPP US$ 35,060 in the USA. China and India, two of the world’s most populous countries, grew at a much faster rate during the decade of the 1990s than the other eight countries with smaller populations. As a matter of fact, the Indian economy grew much more rapidly than most other countries with a lower fertility rate. 4. The association between population growth and economic expansion is weak within India as well.

8 Kerala, the state with the lowest growth rate of population between 1981-91 recorded the lowest growth rate. On the other hand, Rajasthan’s Gross State Domestic product grew the fastest in the 1980s despite the State recording the highest rate of population growth as well. In Haryana, despite ranking second in terms of income expansion, the annual rate of population growth in the state was next only to Rajasthan. Data also reveal that, between 1991-2 and 1997-8, per capita income (State Domestic Product) grew by 7.6 per cent every year in Gujarat – a state with a population of 50 million in 2001. On the other hand, per capita income (State Domestic Product) grew by only 2.8 per cent in Punjab – a state with a population of only 24 million in 2001. 5. The belief that rapid fertility decline will solve the problems of poverty is misplaced. In 2000, Gujarat reported a birth rate of 25.2 per 1,000 population – similar to that of Orissa’s 24.3. Still twice the proportion of population in Orissa lives below the poverty line than in Gujarat. Similarly, Kerala and Haryana report very similar proportions of population living below the poverty line – around 24-25 per cent. Yet Kerala’s birth rate is 18 whereas it is 27 per 1,000 population in Haryana. Even states like Goa, Kerala and Tamil Nadu that have lowered fertility to replacement levels have not done away with problems of human poverty. 7. Population Stabilization: Six Policy Lessons 1. Enforcing a one-child norm like China did, or even a twochild norm, is impractical, unnecessary and undesirable. Neither does it result in quicker outcomes. 2. Global experience points to the strong inter-connectedness between lowering fertility rates on the one hand and empowering women, reducing poverty and improving child survival on the other. 3. Use of penalties, disincentives and coercion to achieve population stabilization is counter-productive. 4. Improving access to high quality public health and reproductive health services is essential for lowering fertility. 5. Offering monetary incentives to acceptors of family planning, motivators or providers has often resulted in misuse, abuse and limited benefits. The success of the southern states within India in reducing fertility point to the importance of simultaneous actions along multiple fronts – quality health care services, education, women’s empowerment, child survival and reducing human poverty.

mfc bulletin/June-July 2005

9

Memorandum on Polio Eradication Initiative in India Submitted to the World Health Organization, UNICEF, and the Government of India, on 7 April 2004, World Health Day. We are a group of public health professionals from India who wish to register our concern at the manner in which the entire polio eradication ‘initiative’ has been thrust on our country by the World Health Organization (WHO), the UNICEF, the American Centers for Disease Control and Prevention, Atlanta, and the Rotary International. The Polio Eradication Initiative in India launched in 1995, as part of the Global Polio Eradication Initiative (GPEI), promised eradication by the year 2000 and the ‘certification’ of eradication by 2005. The target is now postponed to 2006-07. It is time to take stock and see what has been the achievement of GPEI in our country in the nine years of intense effort beginning from 1995. It is our contention that the goal of GPEI was flawed from the time of its conception and is unlikely to achieve its stated objectives this year or in the coming years.

•

The polio eradication campaign has failed to achieve its goal. In fact, in 2002, there was a serious upsurge in the number of paralytic polio cases and the cases were spread in 16 states. Not just that, in 2003, four states (Karnataka, Assam, Andhra Pradesh and Tamil Nadu) which had been previously polio free also had cases of polio. The states of Andaman & Nicobar, Arunachal Pradesh, Lakshadeep, Manipur, Mizoram, and Sikkim are the only six states that have not reported polio cases since 1997. We note with concern: •

The scientific organizations spearheading the initiative are aware of the flaws but are misleading our government and the world that the strategy adopted is scientifically sound and all that would be required to achieve it is ‘political will’. Instead of admitting to the programme’s failure, on January 15 this year, a ‘clarion’ call, “2004 –Now More than Ever: End Polio Forever”, was issued from the WHO Head Quarters at Geneva. On the same day, a ‘Declaration1’ was signed on behalf of the Health Ministers representing the Government of Afghanistan, Egypt, India, Niger, Nigeria, and Pakistan, allegedly “…the six countries of highest priority for stopping the transmission of poliovirus globally”. These governments, were made to note “…with grave concern that the poliovirus is being exported from endemic countries and is infecting children in previously polio-free areas” thereby implicating themselves in an international ‘crime’ which they are being accused.

•

• •

•

•

• It is our contention that: • •

•

• •

The WHO inflated the numbers of paralytic polio cases for the pre-programme period to justify GPEI; Repeated changes in ‘case definition’ have led to claims of a far greater impact of the programme than what it is in reality; An ‘Elimination’ strategy is being passed off as an ‘Eradication’ strategy when it is clear that poliovirus cannot be eradicated in India by relying on vaccination alone; Significant proportion of the polio cases in the country, including the epidemic in 2002 in UP, is iatrogenic 2; Significant proportion of the children with confirmed wild polio paralysis in the post GPEI are immunized children 3; and

A number of children have developed vaccine-associated paralytic poliomyelitis (VAPP) in the last nine years of GPEI 4.

•

•

•

•

The excessive dosing of our children with OPV, at times exceeding more than 25 doses in the first five years of age which is unheard of in the history of polio eradication in the West 5; The potential for future outbreaks due both to wild and vaccine-derived virus as a consequence of the impact on the natural history of polio. The arbitrary decision to do away with all universally held contraindications of OPV for our children 6; The resurgence of other vaccine-preventable diseases, due to the adverse impact of pulse polio on routine immunization programme; That a disease of lower public health importance in the 7 country has been justified on the grounds of some small savings for the developed nations 8; The over-loading of an already weak health care system of our country by the meaningless and expensive pulse polio initiative; That an impossible and unnecessary case is being made for the eradication of wild poliovirus as the objective, thereby trapping the country in the unrealizable success criteria imposed by WHO9 ; That the stage is now being set for an exorbitantly expensive IPV for routine immunisation in the coming years10 ; That more than Rs 2500 crores have already been spent on this misadventure and that more than Rs 400 crores/year has been allocated in the Tenth plan11 ; That the promises made by the international fraternity that the country will not have to borrow, has been belied by the government quietly borrowing US $ 180 million from the World Bank for the pulse polio programme thereby adding to our debt12 ; That UNICEF has changed its role from being only a technical advisor to also a procurer of vaccines for the

mfc bulletin/June-July 2005 •

Government of India leading to conflict of interests13 ; That three recent events — the discovery of resistant strain of vaccine to ‘wild’ virus, the discovery of longterm carriers of vaccine virus and conflict situations in several parts of the world, make worldwide cessation of polio vaccinations in future an empty claim.

We Demand: •

•

•

An independent inquiry be held on the polio eradication ‘initiative’ in India with due consideration to the relevant epidemiology as well as health-care priorities in the country. A white paper on why this ‘initiative’ was advised when the ‘experts’ knew that even after a successful campaign of full coverage was achieved, polio vaccination cannot be discontinued; a full database on the entire programme; the vaccine procurement policy; and the consequences of GPEI for public health including that for polio, be released. The independent enquiry committee should also charter the way forward. The right of compensation and rehabilitation for children affected by both wild polio and vaccine induced paralytic polio since 1995; and The right of rehabilitation of those suffering from nonpolio AFP since 1995.

The GPEI is yet another negative exercise in mismanaging the health priorities and programs in developing countries in the time of globalization. We recognize the efforts of thousands of well-meaning health personnel in reaching out to lakhs of children under difficult circumstances. However, it is unfortunate that the enormous energy and goodwill that the programme generated was misguided and wasted. The failure of the eradication campaign is not because of lack of proper implementation but because of a basic flaw in the strategy itself. Instead of placing the blame where it rightfully belongs, the parents are being blamed with continuous bombardment of the propaganda machinery. We view the UN institutions, their corporate philanthropic partners and the gullible health bureaucracies, technocracies and political leaderships of the developing countries, as equal partners in this questionable venture. The WHO and UNICEF were set up with a mandate of providing scientifically sound technical advice to their member States. We are disappointed at their abdication of this responsibility. We write this memorandum with the hope that it will initiate a national level debate and allow the Government of India to take corrective measures. (This statement is endorsed by several health professionals, academics, health activists and health groups. Readers are welcome to sign and endorse at the mfc website <www.mfcindia.org>.) “Geneva Declaration for the Eradication of Poliomyelitis” dated 15 January, 2004, Geneva, Switzerland. 1

10 Provocative paralysis due to intramuscular injection, ineffectiveness of cold chain and reduction of circulating wild poliovirus due to partial and incomplete vaccination which paradoxically increases probability of paralytic poliomyelitis. We note with concern that none of the IEC material issued in the pulse polio programme in India indicts intramuscular injections as a key risk factor for paralysis with poliovirus. 3 Personal communication from the WHO dated 5.3.2004 in response to a letter written jointly by Drs Onkar Mittal and C Sathyamala dated Jan 31, 2004, addressed to Dr Brent Burkholder, Regional Advisor, Immunization and Vaccine Development, WHO/SEARO, New Delhi. 4 http:// npspindia.org/t5.htm 5 For instance, a child born in Jan 1999 in some of the “high risk” states and districts would have received more than 25 doses of OPV during the many NIDs, SNIDs, and “Mop-up” operations by Jan 2004. 2

The risk of VAPP (Vaccine Associated Paralytic Poliomyelitis) among immunodeficient infants is 3,200-fold to 6,800-fold higher than among immunocompetent infants (Sutter RW, Prevots DR, Vaccine-associated paralytic poliomyelitis among immunodeficient persons. Infect Med 1994;11:426,429-30,435-8, as quoted in Centers for Disease Control and Prevention. Poliomyelitis Prevention in the United States: updated recommendations of the Advisory Committee on Immunization Practices (ACIP). MMWR 2000;49(No. RR-5):7. The Operational Guide 2003-04 on Pulse Polio Immunization in India (GOI, June 2003) does not mention even a single contraindication for OPV. 7 Paralytic polio is only a small fraction of all childhood paralysis and a polio eradication strategy will impact only marginally on childhood paralysis. 8 WHO (1997). Polio: The Beginning of the End. p84. 9 That even if one case is reported from a population of one billion, the entire programme would be treated as a failure. 10 Planning Commission, (undated) Tenth Five Year Plan (2002-2007) Volume II: Sectoral Policies and Programmes Family Welfare, GOI, New Delhi p196. 11 Planning Commission, (undated) Tenth Five Year Plan (2002-2007) Volume II: Sectoral Policies and Programmes Family Welfare, GOI, New Delhi, p213. 12 World Bank (2000) Project Appraisal Document on a proposed IDA credit in the Amount of SDR 106.5 million (US$ 142.6 million equivalent) to India for an Immunization strengthening project, March 30,2000, Health, Nutrition and Population Sector Unit, South Asia Region, p32. World Bank (2003) Supplemental Credit Document, International Development Association, Proposed Supplemental credit of SDR 59.5 Million (US$83.4 Million equivalent) to India for the Immunization Strengthening Project, Nov 18, 2003, Human Development Unit, South Asia Region. 13 One of the conditionalities of the World Bank loan is that the GOI will purchase polio vaccines only through the UNICEF. 6

mfc bulletin/June-July 2005

11

Advocacy for Post-Infancy Immunization Policy: Some quick observations from field notes - Rajan R Patil1 The impact of pulse polio immunization, elaborating especially the adverse effect on the routine immunization, has been well documented. At the same time, it cannot be denied that the high intensity polio awareness campaign may have some beneficial, collateral effects, e.g., change in attitude, i.e., the reduction in fear associated with immunization/reaction which seems to be reflecting in the field. There appears to be a spillover effect: while earlier, parents seemed to be concerned only when the child was ill and would take time off from their work to take the sick child to health care facility/provider, now in the pulse polio era, the parents are seen to be increasingly willing to take off time for their children for immunization, which is basically a preventive/prophylaxis exercise. This qualitative change cannot be missed in the community, in terms of enthusiasm and importance attached in taking child out for immunization, a good sign for those of us emphasizing on qualitative paradigm shift in importance from ‘disease’ to ‘health’. This overall enthusiasm and shift has its operational implications: 1. Volunteers from the community and NGOs very often mobilize older children who have never received any vaccination but are beyond one year. 2. Increased awareness vis-à-vis importance of immunization: Parents are now anxious for their older children who have missed out completely on immunization. Hence many often children aged between 1 to 5 years are being brought for routine immunization, just by practice, as in pulse polio immunization. When faced with above situations ANM/health workers in the field are exercising following options: 1. Play it safe: Only immunize children in their infancy and avoid older children. 2. Refuse to immunize any body beyond one year as they are not sure of suitability, dosage and schedule and some times administrative clearance from the health system for the same. 3. Convincing the attendants that immunization is meant only for children less than one year and the immunization is useless after first year of birth 4. Intimidate patients with possible severe side effects including deaths due to immunization after one year. Some of the health workers who choose to ‘act’ are said to be exercising the following options: 1. Each health worker seems to have his or her own The author is an epidemiologist, presently based in Orissa. Email: <rajanpatil@yahoo.com> 1

opinion about age of eligibility for immunization beyond infancy: ranging from 2-3 months to 2 years. 2. Only OPV and booster dose of DPT are given to the children beyond one year even if such child has missed completely the primary doses of routine immunization. 3. By policy measles vaccine is to be given after 9 months after birth. This practice seems to be internalized so much, that even if children are brought at the age of 10 months and above for the first dose of immunization, our workers think, just by sheer practice for measles vaccination, the child should be brought after 9 months of first DPT dose! 4. Some health workers out of confusion refer such unvaccinated children to PHC, where the medical officer also prefer to it play safe in the absence of any official guidelines or policy. 5. Some other possible consequences: a) With high blitz health education campaign and intense social mobilization we are capturing those children who would not have come in contact with health system for vaccination. b) The health staff is either confused or resorting irrational action when faced with immunizing children beyond the infancy. c) The perplexity of medical and paramedical staff is understandable from the fact that their training programmes always inform only about the immunization schedules for children under the age of one and there is mention of only booster doses beyond one year of age. The confusion of our vaccinators (and also PHC doctors) arising in immunizing children beyond their infancy is summarized below: 1. Whether complete immunization justified beyond infancy? 2. If yes, till what age can the complete immunization be given beyond infancy? 3. Will the order of the vaccination be same as that in infancy? 4. What is the schedule to be followed? 5. Will the dosage remain same? Conclusion 1. Urgent need for policy and guidelines for post infancy immunization. This would be especially useful in BIMAROU region, i.e., belt covering Bihar, MP, Rajasthan, Orissa and UP, where the immunization coverage is poor. 2. In developing countries, with high malnutrition levels and high measles incidence no opportunity to immunize children should be lost, even if it is beyond one year.

mfc bulletin/June-July 2005

12

The Myth of the Mitanin: Political Constraints on Structural Reforms in Health Care in Chhattisgarh Binayak Sen1 Since the middle of 2002, the newly formed state of Chhattisgarh has been the locus of a statewide programme of structural reforms in the Government health care system. This programme was designed to have had two broad components; one was the articulation and popularization of the right to health care through a process of selection, training, and activation of community based women health workers called mitanins. The other broad component was the implementation of a series of fundamental changes in the health care infrastructure of the state. Despite promising achievements in the area of infrastructure development, due to structural problems in the way in which the entire programme has been implemented, the political core of the programme remains unrealized. This major programme illustrates the structural constraints contingent upon state based interventions in health care in the absence of major political initiatives. Chhattisgarh is one of the three newly formed states of the Indian union, having been established on the first of November 2000. Apart from the sex ratio, all demographic indicators impacting on the health of the people are to the disadvantage of the people of the State. Hunger related deaths have been occurring in recent years with increasing frequency. Investigations of such events carried out by the Chhattisgarh PUCL, in which we participated, have shown: (a) a large section of the affected communities do not have the purchasing power to access the targeted public distribution system. (b) supplies of safe drinking water are simply not available many such communities. (c) the government health care facilities remain inaccessible to such communities. (d) BMI studies carried out in such communities have shown that the median BMI in such communities is below 18.5. Of all these factors we have deliberately concentrated our attention only on aspects relating to health care in the analysis hitherto. Within this context the Government of Chhattisgarh (GOC) in January of 2002 initiated a series of consultations to design a comprehensive programme of directed change to bring about specific changes in the health care system in the State. It was agreed that funding assistance for this ambitious programme would be generated through assistance from the European Commission under their sector reform programme. Apart from senior members of the health care Rupantar, A-26, Surya Apartments, Katora Talab, Raipur, C.G. Phone: 91-771-2422875/2424669. E mail: <binayak@senonline.com> 1

functionaries in the State, senior bureaucrats and civil society administrators, health activists participated in this intensive colloquium. What came out of this consultation was a two pronged strategy: on the one hand a programme for the state wide selection training and deployment of community health workers and the other hand there was a 15 point programme for bringing about fundamental changes in the health care system in the State. We will now consider each of the two aspects in some of greater details. Community Health Worker Programme The programme envisaged the selection, training and deployment of a statewide system of community health workers. These workers were supposed to have certain special characteristic, as follows a. All the workers were to be women. They would be known as “mitanins”. The use of these terms, which denotes a culturally hallowed relationship, generated considerable opposition; what was finally agreed upon was a compromise. b. All the women would be selected from within the communities that they would be serving through a process of consensus. c. The communities in question would be self-defined hamlets rather then villages. In other words any group of families regarding themselves as a community would define themselves as such and proceed to select a health worker for themselves. This provision was specifically design to tie in with the concept of “Gram Sabha” in the PESA. d. Educational attainment would not be a constraint on selection. e. None of the mitanins would receive any govt. salary or allowance in lieu of their services. This provision generated prolong debate, but was agreed to on the grounds that the mitanin should be a community representative rather then a government functionary. This provision did not preclude the payment of compensatory allowances during training or the provision of community based interventions at a later stage of the programme. f. The mitanin would perform certain technical functions by way of first contact care within the community, for which she would be specifically trained over a period of time. However, her main

mfc bulletin/June-July 2005 function would be to articulate the demand for health care within the community in the form of the right to health care. A slogan was coined, “Swasthya Hamar Adhikar Hawe” (Health is Our Right). Health Sector Reform Agenda A national level consultation in January 2002 identified a number of areas of the current health services provision which need structural changes in state policy and practice, in laws, in programmes and institutions. The focus was mainly on strengthening community health systems, primary and district level health delivery systems, health surveillance and epidemic control. The areas of reform, which would go along with the programme, were detailed as shown below. Agreed reform agenda with role of civil society partners was specified: 1. Community-Based Health Services • Assisting in the finalization of the communitybased health programme of the GOC (Mitanin Scheme).

13 3. Strengthening Health Intelligence, Surveillance, Epidemiology and Planning • Review of current systems of health intelligence and surveillance, and proposing reforms in integrating the mitanin scheme. • Developing systems of village and district health plans, with community participation. • Improving the quality, reliability and analysis of statistics. 4. Control of Epidemics Improving community and primary health care systems for (a) prevention (b) early detection (c) early intervention (d) early prevention of morbidity and mortality because of epidemics. 5. Health Problems of Poor People • Participatory studies of major health problems of rural and urban poor people. • Participatory plans at local, district and state levels to overcome these health problems of poor people. 6. Capacity Building

• Assisting in designing a social mobilization campaign for popularizing the idea of “people’s health in people’s hands’ and creating effective demand for the programme. • Assisting in designing the media and communications strategy and package for the programme. • Assisting in developing operationalisation details and implementation schedules for the mitanin programme. • Assisting in developing all training modules and pedagogy for the Mitanin Programme (see annexure). • Assisting in monitoring and evaluation of the programme. • Assisting in the co-ordination and logistics for the training programmes.

• Assisting in identifying capacity building needs and training packages for the DHA officials and Hospitals managers to enable them to perform their new role effectively. • Assisting identifying capacity building needs and designing training packages for the PRIs, starting from the Gram Sabha level as well ULBs to make devolution of powers to control government health institutions and services effective. • Assisting in building capacities to utilize existing funds, draw budgets and plan interventions. • Assisting in building capacities to develop accountable community mechanisms, like social audits to effectively manage and monitor the local health department.

2. Delegation and Decentralization

• Develop a rational drug use policy for the state. • Monitor the implementation of the rational drug use policy. • Establish transparent systems for community monitoring of the implementation of this rational drug use policy.

• Assist GOC in developing an autonomy package for (a) integrated District Health & Family Welfare Agency (DHA), (b) Hospitals, (c) Programme managers at district and facility levels and (d) PRIs and ULBs. • Planning of devolution of financial powers and other resources, specifically financial resources to PRIs and ULBs. • Strengthening system of transparency and the right to information and social audits.

7. Rational Drug Use Policy

8. Improving Internal Systems of the Department of Public Health • Identifying internal systems that need reform. • Proposing changes for identified areas of reform.

mfc bulletin/June-July 2005

14

9. Workforce Management and Transfer Policy

15. Drug Resistance in Malaria

•

• Studying extent of drug resistance (to chloroquine) in selected areas of Chhattisgarh. • Focussing on the incidence and prevalence of forest-fringe malaria in Chhattisgarh and recommend comprehensive treatment protocols for malaria.

Assisting in the development of a workforce management policy that is clear and transparent.

10. Drug Distribution and Logistics • Assisting in identifying bottlenecks in the distribution and supply of drugs. • Conduct a feasibility study to set up a parastatal organization for the distribution and logistics of drug supply across the state. • Based on the recommendations of the study, suggest policy norms and guidelines to further extend the reach of the state drug distribution network. • Monitor the implementation of the new drug distribution norms. 11. Uniform Treatment Clinical Protocols • Recommend standardized clinical protocols across the state at the primary, secondary and tertiary level. 12. Management Information System • Assisting GOC in designing comprehensive computerized management information system for the health department from the mitanin up o the district level. • Assisting GOC in the user need analysis as well define outputs expected from the MIS across all levels. • Assisting GOC in the process flow analysis of the Department. • Assisting GOC in feasibility of hardware platforms and software across all levels of users within the health department. • Assisting GOC in the development of atleast two websites: one for the mitanin programme and one for the health department of the GOC. 13. Decentralized Laboratory Services • Assisting in developing low cost diagnostic tools and systems for decentralizing laboratory services to the primary care level. • Assisting in developing training packages for “barefoot laboratory assistants” across the state. 14. Mainstreaming of Indian Systems of Medicine esp. tribal medicines into the state health system • Studying feasibility of integrating some aspects of traditionally practiced tribal medicine in Chhattisgarh.

Formation of State Advisory Committee Taking on board these suggestions, the Department of Health & family Welfare, Government of Chhattisgarh decided to formally formulate collaboration with the leading NGOs of the state who were involved in health action and who had been part of this process. These partners included Rupantar, Jan Swasthya Sahayog, Zilla Saksharta Samiti (Durg) and Bharat Gyan Vigyan Samithi, Raigarh and Ambikapur Health Society and Ramakrishna Mission. A high-powered State Advisory Committee (SAC) was formed by a government order. This state advisory committee was constituted with representatives of these NGOs, of senior state health officials and of representatives of funding agencies contributing to health sector development to monitor the progress of the reform process as well as provide inputs for the community health worker programme. Formation of the State Health Resource Centre To provide on-going support to the health sector reform and development process and to facilitate this massive community health worker programme Action-Aid was requested to set up a State Health Resource Centre (SHRC), fully supported by the Government of Chhattisgarh in order to make available high quality human resource support for health services in Chhattisgarh. The terms of reference for this state health resource centre was agreed upon the crystallized in the form of a memorandum of Understanding signed between the country director Action Aid and the secretary health. The memorandum of understanding defines the State Health Resource Centre as “an additional technical capacity to the Department of Health & Family Welfare in designing the reform agenda for the transition from existing health services to community based health services, developing operational guidelines for implementation of reform programme, and arranging/ providing on-going technical supporting to the District Health Administration and other programme managers in implementing this reform programme. We can briefly take stock of the achievements of the programme upto the present moment, before turning to an analysis of the various ways in which the programme has fallen short of its original objectives.

mfc bulletin/June-July 2005

15 sub-center.

Achievements of the Programme Almost 50,000 mitanins have now been selected and brought into the training process. They have been attending the training programmes held across the state. Many of them have been participating in village level n family health surveys. Their help and participation is also being sought in monitoring some of the functions of the ANM and other paramedical staff. Large numbers of them have been issued with dawa petis (medicine kits). We do not at the moment have any feedback about their ability to use the kits. They have also been issued referral slips with which to send patients to the peripheral health care units An essential drugs list has been evolved, and doctors trained in its rationale and use. An entirely new procedure for the purchase and stocking and distributing drugs of assured quality has been evolved and is slowly being put in place. A series of standard treatment protocols have been developed both at the medical and paramedical levels, and relevant in service training have been organized. A major workforce study has been undertaken and its implications are currently under consideration. A series of publications have been put out by the SHRC, drawing upon in house resources as well as the services of external consultants. These include a series of six mitanin training booklets, a publication on the Conceptual and Operational aspects of the mitanin programme. The Chhattisgarh State Drug Formulatory, Standard Treatment Guidelines for Medical Officers and para medical workers separately, A study of Workforce Management and HRD in the public health care system, and the Proceedings of a Malaria Operations research Workshop, The EQUIP (Enhancing Quality of Primary Health Care) programme has also been taken up by the SHRC.The central purpose of the programme is to strengthen service delivery in the public health system-both in terms of utilization and in terms of quality. The Government of Chhattisgarh has begun by strengthening health services in 32 blocks so that then over the next 3 to 5 years the process could be repeated till all 146 blocks are covered. After discussions, a participatory goal setting was abandoned in favour of a focus on reducing maternal mortality by: a. Ensuring 24 hour Emergency Obstetric Care capability in the CHC, b. Ensuring 24-hour institutional delivery capability in every sector PHC. c. Ensuring good quality sub-centre services at the

It is reasoned that if the gaps are closed in infrastructure, equipment, manpower, skills sufficient to provide the above as well as in parallel organizational and motivational processes are addressed then not only care at delivery but service delivery in the public system as a whole-in that block-would be strengthened. In parallel many other bottlenecks identified like better location of facilities, a working referral system, and the integration with the mitanin system, the multi-skilling of workers etc would be addressed. Many of these achievements have been obtained in the face of determined opposition from the many vested interests threatened by these measures. However, as we shall show, despite these changes, the possibilities created thereby for and equitable, accessible, effective, and humane health care systems remain unaltered in the main. While, a detailed analysis would yield many factors responsible for this outcome, the three major factors that we have been identified are a. The total destruction of the political characters of the community health programme. b. The already highly privatized nature of the health service infrastructure in Chhattisgarh. c. The failure to make any attempt at administrative and financial devolution within this programme. The critical assessment that follows discusses these issues in greater detail and elaborates on some examples of fundamental confusion that have bedeviled the programme. Critical Assessment In its original formulation the programme was to have a woman community health worker who would work in her community to articulate the right to health care and a slogan to this affect had been developed as has been mentioned. It was not mere naivety that persuaded us to accept the possibility of such a demand be articulated from within a state sponsored and implemented process. In the initial consultation, the state protagonists had taken care to involve civil society partners who had been working in Chhattisgarh and elsewhere to articulate peoples’ rights to health through a variety of approaches. The list of participants included Shaheed hospital which had done pioneering work in developing a curative health progrmme in the context of the Chhattisgarh Mukti Morcha, Rupantar, which had extended the ideas developed through the Shaheed Hospital experience to a tribal land movement context and had considerable experience in a community health worker based health programme, Raigarh Ambikapur Health Association which combined

mfc bulletin/June-July 2005 community based health insurance and service delivery through a chain of primary , secondary and tertiary health facilities, CEHAT which worked in health advocacy and community health worker training in Maharashtra, BGVS, which had experience of social mobilization and demand articulation for health and literacy, and many others. All the participating civil society organization had been given a categorical assurance that their would be a period of one year during which pilot phase they would have an opportunity to put this new concept of the community health worker on the ground, free of govt. interference and that these pooled experiences would be the basis for the further elaboration of the concept of the community health worker in Chhattisgarh. The civil society partnership in the programme was also sought to be formalizesd through the establishment of the SAC. However the SAC was quickly marginalized in the decision making process, and in fact, SAC meetings have not been held at all for the last 12 months. However, this assurance was breached early in the programme. Once the State Health Resource Centre (SHRC) was properly set up and the programme got properly into swing, performance indicators took over under the aegis of an agency that considered itself to be a “Para-statal Body”. Moreover, once the power elite in the government and outside it realized that the mitanin was a handy new source of patronage within the village, they quickly took over and occupied all the vacant spaces in the implementation of the programme. Within a year of the establishment of the new state, a very large number of new NGOs crawled out of the wall to serve as vehicles for the hegemonic aspirations of the existing elites. This nexus exerted great pressure to rapidly expand the programme. As a result of this expansion, the focus shifted away from the rights based approach to one that concentrated on technical milestones. This destroyed whatever possibilities were left in the development of an approach based on a realization of the right to health care. The para-statal Body, which quickly became a “quasi-statal body” implemented this total perversion of the original concept of the mitanin. The current government health care infrastructure in Chhattisgarh is already heavily privatized. All salaried doctors are allowed private practice, and, with rare exceptions, devote a large part of the working day to this activity. Moreover with the widespread application of user fee for service, government hospitals and health centres are almost universally places where effective treatment only becomes available after money changes hands. BPL cardholders are eligible for free treatment, but in a state were 40 % of the people are calorie deficient, only 25 % of the people have BPL cards by World Bank fiat. Moreover, in a situation where most

16 people pay, free often means inferior. Such norms worked themselves down the line. ANMs charge Rs. 500/- for a normal delivery, MPWs charge Rs. 100/- for a glucose drip in malaria and so on. Public investments in such a structure can only increase the lack of equity and accessibility. This is not a plea for less public investment, but only a realization of the fact that unless the back of this corrupt system is broken, piecemeal approches will not work. No such attempt has been made within the parameters of the present programme. On the other hand while the total budget of the entire health sector reform programme over three years is Rs. 16 crores, of which up till now only Rs. 8 crores have been spent. In contrast, in the overall health budget of the state, Rs. 12 crores, out of an annual budget of Rs. 400 crores have already been invested in a public private partnership with Escorts Heart Hospital. The latter facility is housed in one wing of the Raipur Medical College, an institution built totally our of public funds By devolution, we do not mean leaving impoverished communities with the impossible task of generating their own resources but to the transfer of real resources and decision-making power to communities and their federations. The entire legal and institutional apparatus for such a process already exist in Chhattisgarh, a state over large areas of which PESA is in operation, and where PRI institutions are almost a decade old. The structural reform programme nevertheless remains highly centralized, with real power being exercised by a tiny group of people -ministers, bureaucrats, senior doctor -administrators and a “para-statal body”. The entire Panchayati Raj system has been bypassed in this process. The long-term political results of such an approach will be interesting indeed to watch. The confusion about the role of the mitanin and whom she represents mirrors many of the issues we have been discussing above. In the original conceptualization, the mitanin was envisaged as a community representative, and the argument for not paying her a salary was based on this notion. As a matter of fact, the PRI interface with the mitanin was supposed to look into the issue of the compensating her for the service. Yet, as the programme grew more and more target driven, the onus for monitoring was placed firmly in the hands of the health and SHRC bureaucracy; the SHRC annual report for 2003-04 articulates the monitoring structure as “a monitoring cascade where every functionary above the mitanin has one or two formats to fill” (emphasis added). This was expected to “track every mitanin performance and send up only such information as can be acted upon at that level.” It adds that all necessary formats for this were to be printed centrally, and that the SHRC together

mfc bulletin/June-July 2005

17

with the District Collectors and CMOs was to tighten monitoring. The two key concepts of public-private partnership and state-civil society partnership that form a large part of the sector reform discourse are called into question through the experience of health sector structural reforms in Chhattisgarh. In many circles the programme in Chhattisgarh has been hailed as a success story, particularly for its operationalization of these two aspects. Yet, our discussion above has

shown that important political prerequisites for the creation of a just, equitable, accessible, and effective health care system have not been met in this process. A widespread articulation of the right to health care is not a mere utopian dream. It is a necessary part of any process of genuine structural reforms in the health care system. In its current formulation, a liberatory role for the community health worker remains a myth-the myth of the mitanin.

Living Will is the Best Revenge -Robert Friedman, Perspective Editor Like many of you, I have been compelled by recent events to prepare a more detailed advance directive dealing with end-of-life issues. Here’s what mine says: * In the event I lapse into a persistent vegetative state, I want medical authorities to resort to extraordinary means to prolong my hellish semi existence. Fifteen years wouldn’t be long enough for me. * I want my wife and my parents to compound their misery by engaging in a bitter and protracted feud that depletes their emotions and their bank accounts. * I want my wife to ruin the rest of her life by maintaining an interminable vigil at my bedside. I’d be really jealous if she waited less than a decade to start dating again or otherwise rebuilding a semblance of a normal life. * I want my case to be turned into a circus by losers and crackpots from around the country who hope to bring meaning to their empty lives by investing the same transient emotion in me that they once reserved for Laci Peterson, Chandra Levy and that little girl who got stuck in a well. * I want those crackpots to spread vicious lies about my wife. * I want to be placed in a hospice where protesters can gather to bring further grief and disruption to the lives of dozens of dying patients and families whose stories are sadder than my own. * I want the people who attach themselves to my case because of their deep devotion to the sanctity of life to make death threats against any judges, elected officials or health care professionals who disagree with them. * I want the medical geniuses and philosopher kings who populate the Florida Legislature to ignore me for more than a decade and then turn my case into a forum for weeks of politically calculated obliviation. * I want total strangers - oily politicians, maudlin news anchors, ersatz friars and all other hangers-on - to start calling me “Bobby,” as if they had known me since childhood. * I’m not insisting on this as part of my directive, but it would be nice if Congress passed a “Bobby’s Law” that applied only to me and ignored the medical needs of tens

of millions of other Americans without adequate health coverage. * Even if the “Bobby’s Law” idea doesn’t work out, I want Congress - especially all those self-described conservatives who claim to believe in “less government and more freedom” - to trample on the decisions of doctors, judges and other experts who actually know something about my case. And I want members of Congress to launch into an extended debate that gives them another excuse to avoid pesky issues such as national security and the economy. * In particular, I want House Majority Leader Tom DeLay to use my case as an opportunity to divert the country’s attention from the mounting political and legal troubles stemming from his slimy misbehavior. * And I want Senate Majority Leader Bill Frist to make a mockery of his Harvard medical degree by misrepresenting the details of my case in ways that might give a boost to his 2008 presidential campaign. * I want Frist and the rest of the world to judge my medical condition on the basis of a snippet of dated and demeaning videotape that should have remained private. * Because I think I would retain my sense of humor even in a persistent vegetative state, I’d want President Bush the same guy who publicly mocked Karla Faye Tucker when signing off on her death warrant as governor of Texas - to claim he was intervening in my case because it is always best “to err on the side of life.” * I want the state Department of Children and Families to step in at the last moment to take responsibility for my well-being, because nothing bad could ever happen to anyone under DCF’s care. * And because Gov. Jeb Bush is the smartest and most righteous human being on the face of the Earth, I want any and all of the aforementioned directives to be disregarded if the governor happens to disagree with them. If he says he knows what’s best for me, I won’t be in any position to argue. Robert Friedman is editor of Perspective. He can be reached at <friedman@sptimes.com> [Last modified March 27, 2005, 00:34:19]

mfc bulletin/June-July 2005

18

Right to Health Action by Mitanins in Koriya District - Sulakshana Nandi1 The following note is an attempt to capture the issues taken up, fights fought and risks taken by the mitanins, trainers and other tribal women of the villages in their efforts to ensure health and food entitlements to the tribals. The note refers to the Mitanin Prrogramme in Manendragarh block of Koriya district, one of the least developed and most impoverished areas of Chhattisgarh state where the Government is conducting the Mitanin Programme. Though there are blocks where the programme is being conducted by NGOs, this is not one of them. However there has been a conscious effort to shape this process from ‘outside’ by civil society through the Adivasi Adhikar Samiti. Though this is not typical of the programme in every block in the state it gives us an idea of some of the possibilities of empowerment that such a programme opens up. There are however examples like this from almost every part of the state, but given the pressures that State Health Resource Center faces just to keep the programme going, it is unlikely that the thousands of such events would ever be documented. 1. Ensuring Immunisation 1.1 Data collection, providing information and demanding action on immunization A campaign to assess the status of immunisation and people’s perceptions regarding it was carried out in about 50 villages over 14 Panchayats in Manendragarh block. As a part of the campaign, village meetings were held and the mitanins and trainers recorded status of immunization of each child and pregnant women in the Gram Swasthya Register. It was found that there were a large number of children and pregnant women who were not vaccinated. The reason for this was mainly because in each Panchayat, there were a few villages and hamlets where vaccination was not taking place, especially in those without an Anganwadi. This information was promptly shared with the BMO who directed his workers to cover these areas. 1.2 Spreading awareness and providing support for immunization The mitanins and trainers identified children and pregnant women who had not been vaccinated. They spread awareness about the importance of vaccination and tried to do away with people’s negative perceptions. When the ANM came to the village, the mitanin informed and gathered all the children and 1

The author is one of the main organisers of the Jan Swasthya Abhiyan in Chhattisgarh and is a member of the SHRC governing body and executive committee. Her email id is <sulakshana.nandi@gmail.com>

pregnant women. This led to a steep rise in vaccination especially in the villages where there is no Anganwadi centre. The mitanin’s efforts in increasing the use of ICDS also helped as she could get large number of pregnant women and children below 3 years to the centre for Take Home Rations, who therefore got vaccinated too. In January 2004, a series of Health and ICDS awareness camps were held in 45 ICDS centres jointly with the Women and Child Development Department, Health Department and NGOs in Manendragarh block. The mitanins mobilised large number of village people, especially women, to attend these camps. As a result of the camps, a large amount of backlog of immunization in those villages was cleared. The ANMs were initially very skeptical about the mitanins’ work, and a few were antagonistic too. But they have realized the mitanin’s ability in mobilising the community on health issues and now take them seriously. 1.3 Community monitoring of immunization Presently, led by the mitanin, the women of each hamlet monitor the ANM and the vaccination status. The date of monthly immunisation is recorded in a monitoring register. If one month goes without the ANM coming to the village, then the mitanin reports it to the trainer who then reports it to the BMO. The community also puts pressure on the ANM to visit. 1.4 Challenges However inspite of all the efforts there are still more challenges to overcome. For example, in the case of Kachod Panchayat, not much difference has been made regarding vaccination due to extreme negligence by the ANM. In the summer months, from March to June, the ANM did not carry out any immunization camps inspite of repeated warnings by the BMO. Her excuse was that as there is no refrigerator in the sub-center and neither in the PHC (Biharpur), she has to collect vaccines from CHC Manendragarh, 35 km away from the sub-center, and then go to villages 5-12 km away. So in the summer months it is not possible to maintain potency of the vaccines. Now the situation is that both the ANM and the MPW have been transferred without the Government placing new people there. So now the fight is on to pressurise the Government to place a Health worker there.

mfc bulletin/June-July 2005 In case of measles vaccines, there is still a lot of backlog to be covered. The vaccine has not been available for the whole of last year (2003). Efforts have been on to cover this backlog, but repeated requests to hold camps have been ignored. But things became very critical in case of Lalpur village. In the village, there is a hamlet of SCs (Basors), which is only 3 km away from Manendragarh town. In this hamlet, three children died of measles in May 2004 but even after that, measles vaccination had not been done for three months. In July when this was reported to the BMO, he sent a team who surveyed and found that in a hamlet of 70 households, about 30 children had not been vaccinated. A three-day vaccination camp was immediately carried out. Villagers Introduced to Immunisation Services Kharla and Kailashpur are twin tribal hamlets consisting of Cherwa, Gond, Oraon and Pando tribals. They are at a distance of 6 km from the main road and are connected to it only by a kuchcha road, that is not motorable, during the rainy season. In these hamlets, in the last one year, no kind of vaccination (either of the children or of the pregnant women) had taken place. The trainer’s cluster meeting was held in the PHC (Kelhari) during which the MPW was confronted in front of the PHC doctor. After giving some lame excuses he agreed to go there as soon as possible for immunisation. Now the MPW carries out vaccination camps every month and the mitanin helps him wholeheartedly to ensure that all the children and pregnant women are vaccinated. The mitanin has earned the respect of both the community and of the Government worker. In another case, in the Garudol Panchayat, the ANM would go only to the two roadside villages with Anganwadi centers, Garudol and Dulku, but rarely went to the other more remote villages (Bala, Balshiv and Chharcha). The mitanins reported this to their trainer who reported it to the BMO. Now the ANM has been forced to start going to these villages too. In this case too the mitanins offer every help in informing and gathering the children and pregnant women.

2.

Preventing and Controlling Gastroenteritis Epidemic: Prevention, Speedy Information Flow, Pressure on Health Department and Administration to Act

In order to prevent the outbreak of gastroenteritis, an intensive campaign was carried out in April 2004 in 60 villages of Manendragarh block. The mitanins were first given refresher trainings on the cause, prevention and treatment of gastroenteritis. Then meetings were conducted in 60 villages by the trainers and the mitanins in which the cause, prevention and treatment of gastroenteritis was explained. Discussions were held regarding irrational use of intravenous fluids and injections and emphasis was put on the utility of ORS

19 (home-made). Also, a system for prompt information flow in case of an epidemic was set in place through the mitanin. Along with that, a list of hamlets with drinking water problems was given to the SDM out of which, in 10 hamlets, handpumps were installed and another 15 were repaired. During the gastroenteritis season, the mitanins along with the community monitored each houses for disease and ensured that ORS (home-made) is given to the patients. In many cases the mitanin would make the ORS herself and feed it to the patient at intervals. Serious patients were taken to the PHC and the CHC. The mitanin was able to cure a large number of people through ORS and prevent further expenditure. Gastroenteritis Epidemic Control in Bairagi Village Bairagi is an extremely remote tribal village that is reachable only by an 8 km trek through hilly and forested tracts. There was an outbreak of gastroenteritis in the village and the mitanin immediately informed the government worker through the trainer, while ensuring that all the patients keep taking ORS (home-made). The health worker reached the village and treated the patients and as a result there were no deaths. Later the mitanin, along with other women of the village also put forward their demand for handpumps.

3. Preventing and Controlling Malaria Epidemic Work on malaria was done on two levels: 1. By ensuring that chloroquine is available in all the hamlets of Manendragarh block mostly through the mitanin. 2. Intensive campaign on larva control in 30 hamlets. 3.1 Ensuring chloroquine availability at each hamlet level A survey done by the mitanin trainers in Manendragarh regarding the effectivity of Depot holders showed that nearly 80 % of the Depot holders were inactive. The Health workers would occasionally supply them with medicines, but either they were not distributing them or distributing them with incorrect dosage. This was the case even with chloroquine. A dialogue was held with the Collector and at the Block level for the health administration to recognise all the mitanins as depot holders. This has been carried out in about 300 hamlets. Initially they were given 20-50 chloroquine tablets, which was insufficient for an average hamlet with 30 households. After further dialogue with the Collector, it was ensured that the mitanins are given at least 200 chloroquine tablets at a time. But the real challenge lies in ensuring regular replenishment of the medicine. The trainers give fortnightly reports on availability of medicines with each mitanin, which is forwarded to the BMO. But the mitanins and the trainers who constantly pressurize the ANMs and MPWs to give them more

mfc bulletin/June-July 2005 medicines and even go to the PHC to replenish their stock are fighting the real battle. Mitanin Goli The mitanins have been instrumental in containing and treating cases of malaria in a number of villages of Manendragarh block. Once armed with chloroquine tablets, they have been extremely diligent in giving the correct dosage and ensuring that the patient takes it for three days. When their stock runs out, they go to the PHC or to the ANM to demand more supplies. Along with this they have campaigned against the indiscriminate use of injections and intravenous fluids. Their work has won them the respect of the community as it has cured a number of people and prevented a lot of useless expenditure. People today know chloroquine as the ‘mitanin goli’. They believe that it is this ‘mitanin goli’ that will cure them and not the medicines given by the ANM/MPW or quacks.

3.2 Campaign on malaria and larva control Village meetings were held in 70 villages, to spread awareness regarding malaria, especially the utility of chloroquine tablets. Malaria committees were formed in 10 villages covering 30 hamlets. Volunteers in the malaria committees were first given training on cause, prevention and treatment of malaria. They were then trained to recognize anopheles larva around their village and how to kill them by pouring burnt lubricant/ kerosene oil. A register is being maintained recording the number of fever cases in the village. The mitanins provide hamlet-wise information to the trainers regarding the number of fever cases, who are keeping a lookout for serious outbreaks. Outbreak of Malaria Epidemic: Speedy Information Flow, Pressure on Health Department and Administration to Act In the month of July, there was an outbreak of malaria, in the Basor para (SCs) of Lalpur village only 3 km away from Manendragarh. The people were not being provided any services by the government. The ANM who lived in the same village, used to only come to the Anganwadi center, which was in corner of the hamlet, and most of the hamlet was ignored. This was reported to the BMO who sent a team there. The team did a house-to-house survey and found that in a hamlet of 70 households, there were around 50 cases of malaria.

4. Fighting Child Malnutrition 4.1 Forcing ICDS centres to provide supplementary nutrition In Manendragarh block, there are 103 ICDS centers while there are more than 400 scattered hamlets. Hence the ICDS center of a village is not even able to cover the entire village, but only the hamlet where it was situated. A campaign around ICDS was carried out from

20 August 2003 in 45 centers covering 80 villages. The mitanins had just undergone training on Child health and malnutrition. They were given additional information about the services provided by the ICDS centers and its utility and told to mobilize people to utilize those services. Hamlet-level meetings were held to spread awareness about ICDS services and to see it as a right, which they should demand. This led to a phenomenal increase in utilisation of the ICDS services, especially of Take Home Rations (THR). Mitanins from far-off hamlets would gather all the pregnant and lactating mothers and mothers of children below 3 years of age on the stipulated day and go to the center to demand THR. In some cases like Kharla, Kerabehera, Balshiv, Phuljhar, the women even go to another village (2-5 km away) to collect THR. Demanding these services has not been easy. The mitanin along with the women from the hamlet had to fight with the ICDS worker who was not willing to take the extra burden. Today the community, led by the mitanin, monitors the ICDS centers in 45 centres covering 80 villages. Any irregularities are immediately reported and they also fight it out at the local level. 4.2 Forcing ICDS centres to measure malnutrition One of the first tasks for the mitanins and trainers was to weigh children aged below 5 years and measure their malnutrition status. Weighing machines had to be borrowed from the ICDS centers. This exposed the shocking fact that in nearly 70% of the centers, weighing machines were not in working condition and had been in this state for the last one year to six months. This issue was taken up at the District level and in some of the centers, the machines got repaired. 5. Opposing Irrational Practices of Private Practitioners/Government Workers Doing Private Practice – Campaign Against Unnecessary Use of Intravenous Fluids and Injections The mitanin has been instrumental in campaigning against the indiscriminate use of injections and intravenous fluids and private practice. Village meetings were held in 60 villages in which discussions were held regarding how people prefer injections and intravenous fluids to tablets and how private practitioners and government workers just charge indiscriminately. The people were made to taste bottled saline water, which they recognized as salt water. The actual cost was also shared and the people were aghast that the private practitioners and government workers charge Rs.150 for a saline water bottle, which comes for Rs.12. The actual role of injections and intravenous fluids was explained. chloroquine injection was shown to the people and it was explained that both the tablet and the injection have the same medicine.

mfc bulletin/June-July 2005

21

It was also revealed that in the villages no one ever receives the correct dose through injections and this only causes them much harm and expenditure.

then only paid them. In some Panchayats like Garudol, the MPW has reduced the amount for administering injections from Rs.20 to Rs.10.

Discussions were held regarding the big financial advantage the private practitioners and government workers get in propagating indiscriminate use of injections and intravenous fluids. It was made clear that the mitanin is going to propagate use of home remedies and tablets.

‘Righting’ a PHC Biharpur PHC was literally a non-functioning PHC that used to open only on the local bazaar day. There is only one doctor who lives 59 km away and a peon who would only do private practice. Till March 2004, there was an average of only two patients per day. Also, whoever goes to the PHC has to pay up Rs.10 and this money is not even deposited in Rogi Kalyan Samiti. People had just stopped going to the PHC. These issues were discussed in various village and cluster meetings and people were encouraged to go to the PHC to demand their rights. A complaint was also made to the SDM and BMO regarding the doctor. As a result, the doctor now comes on most days of the week and there has been a steady increase of people going to the PHC and demanding services. Recently, mitanin and women leaders of Chharcha village took about 10 children who were ill to the PHC on a bazaar day and they refused to pay any fees. The peon has stopped doing private practice. Immunisation in the PHC area has also improved drastically. In one case, the MPW spent the night in the village to do immunization the next day, as the mitanin was not available the previous day.

There have been innumerable success stories where mitanins have treated people by administering homemade ORS and chloroquine tablets and this has lent lot of legitimacy to the mitanins. Most importantly, this had reduced useless health expenditure with people going more to the government system and demanding free services. 6. Sending Referrals to PHCs and CHC – Demanding Services Without ‘Extra’ Payments Initially, in many cases, a patient used to take along the mitanin when going to the PHC or CHC in order to ensure proper services. Today the mitanin proactively refers patients to the CHC and PHC and most of the time accompanies them. They persistently demand free services from the doctor and refuse to go to their private clinics. Demanding Government Services Sunita of Chharcha village fell ill with high fever and cough during February 2004, which continued for more than four months. During that time she went to a quack who charged her Rs. 40, got injections and medicines from a MPW for Rs. 80, went to the PHC in Biharpur where the doctor gave her two injections and iron folic acid tablets every week for four weeks, charging her Rs. 10 every time as fees. By this time she had become very weak and had stopped doing any economic or household work. The mitanin of her village, Sonkuwar, suspected that she might have TB and took her to the CHC Manendragarh. The CHC doctor told them to come to his house to show the X-Ray and blood test results, but they would have to pay up Rs. 100 as fees. The mitanin told the doctor that they would show to him only if he comes to the CHC in the second half. TB was confirmed and today Sunita is recovering as she is taking TB drugs regularly and her DOTS provider is Sonkuwar mitanin herself.

The various campaigns initiated have made the mitanins and villagers aware of their rights and given them knowledge about the services they should demand from the government. In case of ANMs/MPWs doing private practice, there have been instances in which the mitanins and people from their community first grilled them about whether a particular medicine is government supply or has been privately bought and

7. Ensuring Distribution of Iron Folic Acid Tablets to Pregnant and Lactating Women The utilisation of iron folic acid tablets was very low in the area and the ANMs would claim that women refuse to take the tablets. After the training on women’s health, the mitanins spread awareness regarding anemia and the utility of the iron tablets. They started demanding iron tablets for the pregnant and anemic women in their hamlets. It was then discovered that there had been no supply for more than six months; hence the ANMs did not have the tablets to distribute. The mitanins continued to pressurise the ANMs and pressure was put on the BMO who said it was not in his hands. The case was then taken up with the Health Secretary, which led to prompt supply of the iron folic acid tablets. 8. Demanding Free TB Examination and Free TB Drugs In Manendragarh block, there was no RNTCP programme for the last few years. Nevertheless, the mitanins were encouraged to bring suspected TB patients to the CHC and demand free services from the government. This was not being provided and as a result, a lot of resentment was generated. This put pressure on the health system and the RNTCP programme was finally started on 15th August 2004. Since then the mitanins have been bringing more suspected TB patients to the PHC and CHC for testing. And in a number of villages like Gundru, Chharcha

mfc bulletin/June-July 2005 etc., mitanins have been made DOTS providers. 9. Active and Open Opposition to Domestic Violence In quite a few villages, the mitanins have played an important role in taking up the issue of domestic violence. The mitanins were first sensitized on the issue of domestic violence. Then in all the village meetings, this issue was discussed. The awareness has spread to some extent that wife beating is not an acceptable thing and women have been encouraged to take action against it with the help of the mitanins and the trainers. There have been cases of women/mitanins going to the Police to complain about violence after being encouraged by the mitanin/trainer. Samudri, mitanin of Parasgarhi, was mercilessly beaten by her husband who then ran away with their two children. Samudri was then driven out of the house by her brother-in-law and sister-in-law. Samudri informed the trainer and then she was taken to meet a lawyer for advice. The husband got to know and he promptly returned and apologized to her. He has behaved himself since then. In another village, Rojhi, the mitanin Manmati’s brother-in-law beat her over a land dispute. She went to the Police and filed a FIR against him after a village meeting. In another case, a couple Dugla village used to drink alcohol and mercilessly beat their children, including their daughter aged 15 years. The mitanin of the village encouraged her to report to the Police. The Police threatened the parents and now they no longer beat the children. There are more cases in which the mitanin/trainer has counseled the woman and threatened the man beating his wife, like in Tarabehra and Vishrampur. In some cases, mitanins, like Jaimatiya in Kariabehera, have sheltered women in the middle of the night and also called village meetings to put pressure on the husband. In Dulku village, after discussions on domestic violence in the village meeting, the villagers got together to put pressure on a man to stop beating his wife. When he did not listen, the villagers socially ostracised him. 10. Leading the Campaign on Right to Food in Koriya The Right to Food campaign was started in Manendragarh block in 80 villages with the mitanins leading the campaign. Special trainings and meetings were held in order to familiarize the mitanins with the issues in Right to Food. Midday Meal and ICDS The mitanins and trainers clashed with schoolteachers on the issue of midday meal, ICDS workers on the issue of distribution of daliya, oil and gur. The community led by the mitanins/trainers wrote affidavits against 7 teachers who were themselves irregular or were not

22 serving midday meal regularly. Out of these, three got transferred and the rest were punished monetarily. As a result, those schools are running regularly. As a result of public action led by the mitanins, erring ICDS centers have been corrected and its utilisation has increased many folds, even though in most villages, mitanins and Anganwadi workers remain at loggerheads. Courage Shown by the Mitanin in Correcting the Anganwadi and School Rambai is the mitanin of Rokda village. She is a widow and stays alone with her 10-year old daughter. The Anganwadi in her village had not been functional for over six months as the Worker was from another village and she would never come to open the center. When in January 2004, the Health and ICDS awareness was held in her village, Rambai mobilised the women of her village who came to the camp in large numbers and complained against the Worker to the Supervisor. As a result, disciplinary action was taken against the Worker and now daliya is cooked in the Anganwadi daily and THR distributed weekly. In the same village, it came to the notice of the villagers that the schoolmaster had sold one quintal of the midday meal grain and there was one master who used to be perpetually drunk. In a meeting organized by Rambai, the women of the village wrote a complaint against the masters. Before the enquiry, the masters and one Panch served alcohol and chicken to 5 men continuously for two days. At the time of the enquiry, none of the women were called and these 5 men gave a written statement saying that the complaints were false. Later when the women got to know about this, they got very angry and complained again. When the Panch got to know about this, he came, drunk, to Rambai’s house at midnight and verbally abused her and troubled her for 2-3 hours. This time the SDM was brought to the village for the enquiry. All the men and few women of the village had fled to avoid any confrontation, but ten women, led by Rambai went to the school and openly spoke against the teachers and the Panch. The SDM threatened the Panch that if he ever tries to trouble the women again, he will have to pay dearly. The drunken teacher was transferred and disciplinary action was taken against the teacher who had sold the grain.

Public Distribution System State-level impact was made in the campaign to secure food entitlements for the BPL and Antodaya families. The community led by the mitanins wrote affidavits against PDS shops in 12 Panchayats and these were forwarded to the Supreme Court Commissioners. As a result, investigations were undertaken in these Panchayats. Consequently, all PDS shops in the initial 12 Panchayats were deprivatised immediately. During

mfc bulletin/June-July 2005 the investigation, mitanins and trainers were the ones to mobilize the community, especially women, to speak against the PDS salesman. They took on very strong vested interests that were also very strong politically. They were given death threats and three FIRs had to be filed to ensure their safety. One salesman even made a false case against a mitanin’s husband in order to trouble them. As a result of such action, all the PDS shops in Koriya and Surguja districts were investigated, where 1100

23 out of 1200 shops were found to be flouting Supreme Court norms. Hence the Government ordered that all the PDS shops be deprivatised in eight ‘tribal’ districts of the State. Currently, monitoring committees have been set up in 80 villages in which, the community led by the mitanins and trainers, monitors the working of all the food schemes and records it daily in a register and immediately takes action if there is deviance of any kind.

Mitanin Takes on Strong Vested Interests to Ensure that Food Entitlements Reach the Poor (1) In Tarabehera Panchayat, the District Vice-President of the ruling party ran the PDS shop and not a single BPL family had received grain in the last three years and the Panchayat Secretary had deposited most of the cards. The trainer Mankuwar and mitanin Jaimatiya conducted meetings in all the villages and got affidavits from the people, which were then sent to the Supreme Court Commissioners. The night before the enquiry, the Panchayat Secretary distributed BPL cards to few people. When Government Officials came for enquiry, Mankuwar and Jaimatiya mobilised a large number of people from all the villages, in front of the shop and they all (especially the women) spoke against the salesman. The Food Inspector later came to Mankuwar’s house and scolded her for causing so much trouble. The salesman sent death threats to her and the mitanin. Mankuwar’s husband got scared and told her that if she continues such work then he will leave her. But Mankuwar and Jaimatiya continued to travel alone from village to village through the forest and mobilize people. Later they filed an FIR against the salesman. Today the Panchayat runs the PDS shop and BPL families have started getting their rations for the first time in three years. (2) Sunita is the mitanin of Phuljhar village. The PDS shop salesman in their Panchayat had deposited most of the BPL/ Antodaya cards since last year. Whenever someone would go to him for rice, he would say that that BPL/Antodaya rice has finished and so for about a year hardly any of the BPL/Antodaya cardholders received any grain from the PDS shop. Sunita mobilized the people of her village, especially the women, to write an affidavit against the salesman. When Government officials went to investigate, she gathered all the women to the PDS shop and all of them spoke against the salesman. The salesman had meanwhile fled the village. His mother threatened to get Sunita killed, in front of the whole village. The villagers in turn warned her that if anything happened to Sunita, then they would drive the salesman’s family out of the village. Meanwhile, the salesman and Sarpanch pati filed a case of disturbing peace against Sunita’s husband, who then had to travel 70 km regularly to appear for his hearings. This case was later dismissed after a dialogue with the SDM. The Government has filed a FIR against the salesman for discrepancies found in the running of the PDS shop and the shop has been transferred to the Panchayat. (3) Rahawati, mitanin and trainer in Biharpur Panchayat led the fight against the PDS salesman who used to refuse to give BPL grain and the Panchayat Secretary who had deposited most of the BPL cards. One night, when Rahawati was out for mitanin training, the Panchayat Secretary and the Sarpanch pati came to her house, drunk, and threatened her husband that they will kill her in the forest. Her husband did not eat for three days, till she returned. But Rahawati was not worried and she continued to do her daily work that entailed traveling alone in very heavily forested hilly tracts. An FIR was filed against the two men. They asked for forgiveness when the Police started troubling them. They were first made to apologise in a big cluster meeting and then only was the case withdrawn.

MFC Resolution

Resolution on Judgement: Shankar Guha Niyogi Murder Case Medico Friends Circle deplores and condemns the Supreme Court judgement given in the Shankar Guha Niyogi murder case on 19/01/05. Shankar Guha Niyogi had led a major working class initiative for health and health care, based in a working peoples’ organisation. He had welcomed the MFC at its annual meet in Dalli Rajhara in 1986. In its judgment, the Supreme Court has overturned

the judgement of the trial court, based on evidence led in the trial. Only the hired assassin who fired the gun has been held guilty, and the rich and powerful people who organised the murder have been let off. At one violent stroke, the judgement exonerates this tragic crime, from all reasonable content and motive, and from its background in the quest of the working people of Chhattisgrah, for a more just society for all.

mfc bulletin/June-July 2005

24

Indian Journal of Medical Ethics NATIONAL BIOETHICS CONFERENCE

Subscription Rates

Ethical challenges in health care: global context, Indian reality

Rs. U.S$ Rest of Indv. Inst. Asia world Annual 100 200 10 15 Life 1000 2000 100 200 The Medico Friend Circle bulletin is the official publication of the MFC. Both the organisation and the Bulletin are funded solely through membership/ subscription fees and individual donations. Cheques/money orders/DDs payable at Pune, to be sent in favour of Medico Friend Circle, addressed to Manisha Gupte, 11 Archana Apartments, 163 Solapur Road, Hadapsar, Pune - 411028. (Please add Rs. 15/- for outstation cheques). email: masum@vsnl.com

November 25, 26 and 27, 2005 YMCA International, Mumbai Central, Mumbai, INDIA Conference sub-themes • Ethical challenges in HIV/AIDS • Ethics of life and death in the era of hi-tech health care • Ethical responsibilities in violence, conflict and religious strife • Ethics and equity in clinical trials and other issues While the conference is planned to cover these sub-themes, submissions will be accepted on other subjects as well. Last date for submission of abstracts: June 30, 2005. Conference details, application forms and updates available at <http://www.issuesinmedicalethics.org/ nbc2005.html> For questions and clarifications e-mail: <bioethics2005@yahoo.co.in>

MFC Convener Ritu Priya, 1312, Poorvanchal, JNU Campus, New Delhi -110 067. Email: <ritupriya@vsnl.com> MFC website:<http://www.mfcindia.org>

Mid-Annual MFC Meet To be held on July 9-10, 2005, at Sewagram. Discussion on next annual meeting theme “Social Regulation of Costs and Quality of Care in the Context of Universal Access to Health Care”.

Contents How the Two-Child Norm in Himachal was Withdrawn Withdraw the Ban on People with More than Two-Children Memorandum on Polio Eradication Initiative in India Advocacy for Post-Infancy Immunization Policy The Myth of the Mitanin Right to Health Action by Mitanins in Koriya District

-Subhash Mendhapurkar

-Rajan R Patil -Binayak Sen -Sulakshana Nandi

1 3 9 11 12 18

Editorial Committee: Anant Bhan, Neha Madhiwalla, Dhruv Mankad, Amita Pitre, C. Sathyamala, Veena Shatrugna, Chinu Srinivasan. Editorial Office: c/o, LOCOST, 1st Floor, Premananda Sahitya Bhavan, Dandia Bazar, Vadodara 390 001 email: chinumfc@icenet.net. Ph: 0265 234 0223/233 3438. Edited & Published by: S.Srinivasan for Medico Friend Circle, 11 Archana Apartments, 163 Solapur Road, Hadapsar, Pune 411 028. Views and opinions expressed in the bulletin are those of the authors and not necessarily of the MFC. Manuscripts may be sent by email or by post to the Editor at the Editorial Office address. MEDICO FRIEND CIRCLE BULLETIN PRINTED MATTER - PERIODICAL Registration Number: R.N. 27565/76

If Undelivered, Return to Editor, c/o, LOCOST, 1st Floor, Premananda Sahitya Bhavan Dandia Bazar, Vadodara 390 001


