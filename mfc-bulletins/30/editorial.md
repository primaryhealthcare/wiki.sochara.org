---
title: "Editorial"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Editorial from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC030.pdf](http://www.mfcindia.org/mfcpdfs/MFC030.pdf)*

Title: Editorial

Authors: Bang Abhay

medico friend circle bulletin

JUNE 1978

MAKING THE COMMUNITY DIAGNOSIS HELEN GIDEON* Community diagnosis is a technique which may be specially useful to those who plan to start community health projects. Medical education teaches the techniques of clinical diagnosis but does not tell how to approach a community and its problems. This article gives the preliminary idea.

Community diagnosis is a comprehensive assessment of the state of an entire community in relation to its social, physical and biological, environment. The purpose of this diagnosis is to determine problem and to set priorities for planning and developing programmes of care for the community. There are certain segments of community diagnosis that are hard measurable facts, such as age and sex distribution, occupation and literacy status, prevalence of diseases, land holding irrigational facilities, sanitary condition, etc. Equally important are those aspect which can’t be measured, but need to be determined and borne in mind, such as the customs, beliefs. Taboos, attitudes and values toward various situations. Furthermore, one needs to be aware of the overall organizational pattern of the community within which there are several suborganizations (e.g., castes) within their own values and patterns of conduct. Each sub-organization is intricately interrelated to the other and to the overall community, thereby affecting its entire behaviour. The diagnostic steps are more or less similar to those used in the identification of a disease in a patient. However, some fundamental differences exist. In the case of a patient, he is aware of a problem. He takes the initiative for advice and submits to whatever he is subjected to. The community, on the other hand,

may or may not be aware of a need/problem. Rarely does the community as a whole take the initiative to seek advice for collective care. Nor can remedical measures be imposed. In fact, active collaboration is an essential requirement for diagnosis and programmes of care for communities. Yet another difference is that the disease of a patient is invariably a pathological condition treated as a

Malady of the patient alone, often unrelated to his environment. The conditions identified in a community cannot be treated as isolated occurrences, as each one is linked to a host of their interrelated factors. For example, prevalence of malnutrition in a community not only call for treatment of the people suffering from malnutrition, but also calls for attention to other matters such as food habit s, inequality of land ownership, feeding practices taboos, economic statue, large or poorly spaced families, inadequate agricultural practices, etc. These are some of the areas which would need attention it comprehensive cares for malnutrition were to be undertaken.

Suggested steps Diagnosis†

towards

Community

I. Library reconnaissance: This is the study of what is known about the area. Information is usually available for a wider area than selected. Facts about birth and death rates, type of land, type of crops grown, occupational patterns, illiteracy rates, etc. may be easily obtained. Other than government 06"icea, local voluntary agencies active in the field may have information that is useful. Occasionally, special programmes or research studies have been conducted in the area or similar areas. The results of these needs to be studied for possible relevance and application.

* Ex Consultant, Department of Community Health, VHAI Present address – Shimla, 170005 India

† The selection of a population is a topic on its own it self and is not considered in this paper. The suggested steps for community diagnosis are for a selected population willing and keen for active collaborating.

.

II Field Reconnaissance: This involves field visits to the selected area to determine the specific local situation. There may be facts known but not recorded: e. g. an obvious prevalent medical condition, a local irrigational problem a social problem. On the other hand, there may be strengths in a community or a richness in the environment which should be noted. Field

basic demographic survey to determines age, sex, occupation, literacy of community. Such information is an excellent indicator for leads to community diagnosis, but, unfortunately, not every programme makes use of such data. The following few points are made to illustrate value of such data.

reconnaissance calls for;

a) The information about age and sex indicates to a

— Informal meeting and discussions to allow the community to: i) voice areas of interest/concern; ii) determine the ways in which the community it dealing with the areas of concern/ plans to deal with them/may be indifferent to them; An attempt should be made to establish what is possible with internal (local) resources and the extent to which outside intervention is Deeded. — Discussions with community to explain the need for accurate data, such as the extent and severity of conditions "of concern" before planning and development of programmes. — A search for ways in which such data can be obtained, seeking active community participation and the formation of working committees. III. Surveys: 1. Basic demographic survey: Most directors of community care programmes undertake a

diagnostician probable line of treatment. For example, if a married woman aged 30 has irregular bleeding, the physician considers the possibility of a threatened abortion. But if woman aged 70 comes with the same symptoms the physician will suspect malignancy. Similarly, in a community, the age-sex distribution aids community diagnosis and indicates the programmes likely to be required. As an example, consider the Population of Sri Lank in 1955. *

Age group Percentage of total population 1) 0-14 years 40.65% 2)15-59 years 55.84% 3) 60 years and above 3.51% The distribution brings out the following facts. (Table 1) Another example (If age and sex distribution gives leads to community diagnosis und programmes of quite another nature. Attention is drawn to population of Berlin by age and sex in 1946. (Cont. on page 6)

Table No. 1 Observations 1) Population under 5 is the largest group. 2) Population 0-14 years of age is 40.65%

Programme indication

Diagnosis Population with high birth rate.

Family planning. Maternal care. Problem associated with frequent child birth.

High number of dependent population,

Consideration of ways and means to increase economic situation (improving technology of existing occupation and trade to increase productivity, introducing other crafts training that may be feasible. Need to determine causes of morbidity and mortality.

resulting in drain on earning members and indicating poor economic status.

3. Steep steps from one age group to the next,

4. Population 15-59 years is 55.84% (Information for 15-44 years not available)

Need for primary medical care and referral care. If in a 100 population the biological state of women in age group 15-44 is as follows: 22 % pregnant at one time, 27 % in postpartum amenorrhoea

High death rates. Large fertile age group.

53% menstruating,

5. Population over 60 years is only 3.21%

Small numbers of people over 60.

then it becomes possible to plan for numbers that will need antenatal and postnatal care and also those who will need to be contacted for family planning advice. Programmes for generic problem are not priority. (Cont. on page 6)

* This age distribution is common in most developing countries.

EDITORIAL As I take over the editorial responsibility from Ashwin Patel. I am a bit bewildered, like Tagore’s famous Kabuliwallah. Kabuliwallah, who used to play with little Mini, goes to the jail for some offence. When he returns back after many years, he experts Mini to be the same little child and hence can not even recognise the fully grown up youthful Mini. Poor Kabuliwallah stands dumbfounded. When Medico friend Circle Bulletin was started about 5 years ago, for some time I used to look after it. The Bulletin was in a primitive stage then, in the form of some cyclostyled pages. The contents also were not of very high standard, quite often single handed written by me alone in one night. Then Ashwin took over the responsibility of bringing out the Bulletin. An able paediatrician as he is, he and Ashok Bhargava helped the Bulletin to grow to the new heights of standard of contents and of production. Imrana Qadeer, Kamala Jayarao, Anant Phadke, Anil Patel and so many others by frequency contributing their studied and thought provocative articles, gave a definite image to be Bulletin. Recently, when I spoke to Ivan Illich about MFC Bulletin, he said. "Yes, I know it well. It is the best periodical in the Third World which analysis health structure and its problems." This was a well deserved tribute to the efforts of the Editor and other contributors. And so here I am, in Kabuliwallah’s situation, unable to understand how am I accustomed to bring out the Bulletin in its childhood form, going to keep up this new standard and reputation. It will be possible only if all the contributors, subscribers and readers of the Bullet in accept the responsibility of running the Bulletin as their own and not of the Editor alone. This Bulletin should become a medium of expression, dialogue and communication as well as a source of conceptual and informative inputs for all those who are trying to think differently and fall out of the routine and established pattern of 'Medicine'. Hence, participation in such a dialogue through the Bulletin is invited. Three points specially deserve attention in this regard. For the last six months Bulletin is heavily depending for its contents on the sources other than its subscribers of readers. This borrowing business can be reduced only if we all who constitute this Medico Friend 'Circle’ start contributing original articles for the Bulletin. Secondly, Medico friend Circle is not an organisation of medicos alone, but or all "thou who are involved in health an. health related activities." It also aims at “improving the non-medical aspects to society for a better life.” Hence, material from other walks of life should also appear in the Bulletin Material on Education, Sociology, Psychology, Economics, Agriculture and so many other vital field is awaited by the Bulletin.

The third point is, Medical Students should feel free to write their ideas, experiences and problems in the Bulletin. They, I am afraid, quite often unnecessarily get subdued by their sector colleagues in the MFC conferences, Bulletin, or in the medical profession in general. Their new, raw, enthusiastic ideas are most welcome. It was Tagore again, who invited them saying “Come on my new, Come on my raw.” — Abhay Bang (Continued from page 5)

ion. M FC has been fortunate to receive help from agencies like OXFAM and FREA and warm cooperation form persons like Dr. Dwivedi, Dr. Gupta and Thakur Prabhakar Singh for this Camp. Sudhendu Patel and Ajay Khare toiled days and nights for the arrangements. Following the field survey, the camps assembled again in Rewa to share and discuss their observations and experiences. It was obvious to every one that the roots of ‘medical' problem were in social structure. Though the participants criticised the organizers for having failed to provide proper accommodation and food arrangements in the villages, it seemed that they had accepted the hardships very supportively. Walking 15-20 kill every day in the hot sunny Bummer of central India, sleeping under the trees, starving for the whole day tolerating insults by landlords, threats from the police, all these formed the memories which they were describing with beaming faces, faces which were severely tanned in 5 days. It should suffice the readers to understand the spirit of the camp that the participants from Jabalpur themselves contacted their friends in Medical college Rewa for cooperation on this problem and declared "We shall come back with our friends to carry this work still further..." — Luis Barreto, Sevagram

New Arrangement Now, as the MFC Bulletin has shifted to Gopuri, WARDHA, 442001, subscription money should be lent to this office, while the MFC membership money (Rs. 40 or 20 or 12, as the cue may be) or donations should be sent to The Convener, MFC, 21. Nirman Society, VADODARA, 390005, INDIA. Correspondence regarding the matters about the MFC organisation should be addressed to the Convener.

“We shall come back….”

Kissa Khesari Camp Ka What has the Government Done in Past 17 Years? It was Kamala Jayarao who wrote the article ‘Kissa Khesari Camp Ka’ in Bulletin (Dec. 77) and brought some new aspects of the problem of Lathyrism to the notice of MFC members. She also raised certain questions about this problem, the answers of which were not available in the contemporary medical literature. This stimulated lot of interest amongst the members of MFC (letter to editor on ‘Kissa Khesari Camp Ka’ Feb. 78) and it was therefore decided by MFC to hold a study camp at Rewa. This was a health development as for the first time MFC members were undertaking a programme born out of the study published in the Bulletin.

Jayarao and Abhay Bang with view to find out answers to some specific questions. The real issue was has the Govt. done anything on the lines suggested by the ICMR expert committee (Ganapathi and Dwivedi in 1961) to end this human tragedy and has the picture of Lathyrism changed in past 17 years?

The main recommendations of the ICMR terms were: 1) Banning the cultivation of Lathyrus sativus 2) Lathyrus be gradually withdrawn in exchange wheat or other suitable cereals 3) Improvement in agricultural methods, specially irrigation. 4) Lathyrus should not form more than 25% of the total diet

The objectives of the camp were: 1) To involve medicos through the issue of Lathyrism in to a process of understanding and analysing socio-economic, conditions which perpetuate the medical problems. 2) To educate medicos in the very process of study

survey and camp participation. 3) To find out answers of some questions about this problem and on completion of the study, to raise the issue of lathyrism before the public, medical experts and the Government and to create public opinion conducive to positive action. About 30 participants (form Varanasi, Bhopal, Jabalpur, Indore, Hoshangabad, Nagpur, Wardha, Bombay and Hyderabad) arrived at Rewa (M. P.) on 3rd June. The gathering was quite heterogeneous and included Nutrition Scientists, doctors, health project workers, Post Graduate and Under Graduate medical students, interns, Agriculture, Law and Science students and journalists. This variety contributed to the greatness of experience the participants had. This Camp was fortunate to have DR. M. P. Dwivedi himself available in Rewa. He is undoubtedly the authority on lathyrism as his life mission. He oriented the participants about the problem of Lathyrism in Rewa area, its causes and the work done so far. After a day’s orientation programme at Rewa, the participants went in groups to their base camps at 3 remote villages – Parasi, Chandni and Sonbarsa, and surveyed, the villages around for 5 days. The camp provided the participants an opportunity to get exposed to a live medical problem, so closely influenced by the socio-economic conditions prevailing in the feudal society of rural India. It also gave the campers an Opportunity to meet the masses and the landlords, and understand the intricacies of the tools of exploitation by the later. The survey performs was prepared by Kamala

Later on, scientist discovered the detoxification methods and advocated that these methods should be popularised in the area. In view of these recommendations, Kamala Jayarao had raised questions like 1) Has the incidence of Lathyrism changed in past 17 years in endemic areas of Central and Northern India? 2)

Have attempts been made to decrease the cultivation of Khesari? Any improvement is irrigation facilities?

3)

Why do poor people eat Khesari? Have they been told about the ill effects of Khesari and have they been persuaded to stop eating this pulse?

4)

Has the payment of Lathyrus in wages been banned?

5)

What are the dominant factors clue to which Khesari continues to be cultivated and given in Birri?

What are 'the levels of awareness, attitudes and reactions of the labourers, farmers and landlords to this problem? We will have to wait for few more months to get the scientific answers to these questions before MFC Publishes the report of its survey and study. But the impressions that one gathered during the survey are as follows:

6)

The area is very much backward in general with low productivity of agriculture, gross unequal distribution of land, strong feudal structure and culture prevalent. The transport and communication facilities are very meagre. There are no alternative opportunities for employment available and hence every landless is forced to become an agricultural labourer with no bargaining power, with the result that the wages are extremely low about Rs. 1.25 per day. There are virtually no irrigation facilities in the region, in spite of Tons river flowing through the middle. No

modern methods of farming are available.

Only one crop is taken in the year. These factors govern the choice of crop to be grown. Farmers see no other choice but hardy, easy to grow Khesari continues almost unchanged. An abortive effort was made by M. P. Government to ban the cultivation in 1963. Out it immediately retreated on slightly resistance from the landlord in only one area of M.

P. Majority of the population, including the affected Ones has some idea that Khesari is responsible for ‘Langad' (Paraplegia) but also attributes the calamity to Rains, Wind and the Heaven. No one knows about the possible methods of detoxification. This peaks a lot about the health education taken up by the Government In put 17 years. Some of the landlords who seemed to have an idea of the real nature of the problem invariably pleaded ignorance when faced with the question “why do you give Khesari in Birri?” This 'ignorance’ some times turned in to a vehement opposition to the interview, manifesting in refusing to be interviewed, refusing the accommodation to stay or even informing the police to put the participants in the custody. It was interesting to note that a landlord who refunds accommodation to the participants had 6 victims of lathyrism amongst his bonded labourers. The majority of the victims are not only landless labourers of untouchable castes (Kol, Chamar) but are also handed labourers Khesari still forms a major portion of their diet, specially in rainy season. They get the Birri at the end of the day's hard work and have nothing else in store to eat. Hence there is no time for detoxification by parboiling. Big farmers, obviously, don't eat Khesari. The acceptance of Khesari by the labourers is sheerly out of compulsion and they would be very glad to receive wheat or rice or any other cereal instead of Khesari. Many labourers, in clear words expressed, "we are being slow-poisoned by the landlords to that we

should always remain weak and be dominated.” So, the area is still a fertile source of victims of Lathyrism. The socio-economic structure which breeds this disease is still the same (who expected it to lave changed) and the Government has done nothing for this problem in the past 17 years. Interestingly, there are about 3 to 4 patents cases for each case of clinical Lathyrism. One more famine and these will be thousand of new victims. The exact incidence and prevalence in the area could not be divided in this survey because the random sampling of villages could not be strictly adhered to. It will need another survey planned in a different way. Similarly further studies need be done to explore the agricultural, legal and marketing aspects of this problem. Efforts are being made in this direct (Cont. on page. 3)

Report

One "Sir" every two minutes On the pleasant morning of 18th June, in a drizzling weather MFC Convener Ashok Bhargava, sad Ashwin Patel marched to the village Nagapur, about 2½ miles away from Sevagram, to see and get in touch with the work done over there by the MFC group of Sevagram Medical College. While traveling the slippery path and crossing the overflowing Nallah with much efforts, they got the muddy taste of what difficulties the local MFC members must be facing is accomplishing their task. At the end of the journey, it was heartening to see five MFC members, all medical students, running a dispensary with bubbling enthusiasm. This weekly dispensary was started about a months ago y the Sevagram MFC group with the initiative of Ulhas Jajoo and cooperation of the villagers. Apart from clinical work, the group also provides immunisation facilities to the villagers. On returning from Nagapur and after devouring a heavy lunch in the hostel mess, the guests and the MFC members of Sevagram, about 20, gathered together for informal discussion. Members were eager to know more about the aims and objectives of MFC, which Ashok tried to explain. After observing the trend of calling senior persons as "Sir" or "Madam", he repeatedly stressed that in MFC there is an understanding that everybody should be treated equally and in the organizational forums no one should put "Dr" before his I her name and also should not put suffix of degrees. In the same spirit, the Convener pointed out, it would be very opportune to abandon the practice of calling some one in the group “Sir” or “Madam.” The lively discussion went on for abut an hour. The members were probed regarding the utility of their running a dispensary in a village. Students explained that it was just a beginning. They wanted to get access to the people and found the dispensary a good medium for that purpose. They wanted to take the wed ahead by trying to understand the socio-economic problem of the village and by undertaking some activity in that dimension. (They have recently carried out a preliminary socio-economic survey or the village). It also came out during the discussion that the field work should be accompanied by a study circle. The observations of students in the village, (like malnutrition, milk-drain from the village to the urban areas) or limitations of their own work (like depending on physicians samples for running the dispensary) were pointed out as good topics for further study, on some of which MFC Bulletin had already published the material. After seeing the posh hospital and hostel buildings of the Medical college, Sevagram, one was naturally forced to think whether that could be acceptable in the (Cont. on page 8)

(Continued form page 2)

Age group

1) 0-14 years 2) 15-59 years.

Percent of total population

18.29% 83.67% (Female more than males)

3) 60 yean and above

18.05%

Table No- 2

Observations 1.Low proportion of population under 5.

Diagnosis

Programme indication

Low birth rate.

Less need for family planning programmes with less emphasis maternal and child care programmes.

2. Population 0-14 years of age is 18.29%

Lower proportion of dependent population at compared to Sri Lanka, possibly higher economic status;

Problems likely to be those of affluent societies.

3. Population 15-49 years of age is 63.67% Imbalance of sex ratio, especially between 20-35 (result of loss of males in World War II).

Community with single parent families; problems of teenage and young children. Problems of lonely adults. Fewer births.

Clubs for recreation and occupational facilities for adults. Child Creches, Youth clubs. Teenage activities. .

4.Populations 60 yean of age and over are 18%

High proportion of older people

Programmes needed for geriatric problems.

Though this age-sex distribution of Berlin is a typical, yet in specific areas of the developing world, the men of the family leave home for all many as six to eight months of the year, grazing cattle, working in factories or harvesting other people's' crops, resulting in problem of single parent families similar to Berlin population in 1949. It .s recognized that not every programme coordinator can do such a detailed analysis of large populations, but it is possible to do sample surveys and simplify the recording and analysis of data to get relevant information. Naturally, each project coordinator will design record forms most suitable for his own programme. From such a record, it is simple to determine the total number of children under five, the numbers in the child-bearing age and the older age groups. b) Information on the occupational pattern of a community indicates economic status and occupational hazards to be expected. Moreover, one be comes aware of the community’s pattern

The objectives of a specific survey: 1.

To ascertain the extent to which a problem affect the people.

of life which indicates the hours of work and thereby the leisure time during which one can make contact with the people, c) Determination of literacy status will indicate to the project coordinator the type and content media required for communication in the development process. 2. Specific Survey: It is necessary to establish whether specific conditions observed, mentioned or indicated by surveys are problems in reality. To this end, special surveys are required for final diagnosis. The surveys can be on a sample basis if the population ill too large and/or if resources are limited. The objectives of a specific survey (1) can be illustrated by the following example. During field reconnaissance, malnutrition in children was observed. After discussion with the community, a survey was undertaken.

or

Possible community diagnosis (not exhaustive): 1. One third of all children under five are malnourished. Prevalence rate: 35% of children under five are malnourished.

2.

To access resources available for remedical action.

2. Milking animals in community. Vegetables sugarcane and peanuts grown. Poultry farms. Space available for kitchen gardens in most homes. Active youth dubs.

3.

To locate persons or groups interested or opposed to the solution the problem.

3. Labourers interested, specially for supplementary food. Landowners opposed: they are well so do and believe the children are well fed.

4. To examine difficulties to the overcome.

4. Inadequate diet habits of low and high calories. Vegetables, milk, eggs, sold not eaten. Peanuts and Jagari, (cane sngar) considered harmful. Belief that a child can only given breast milk until teeth have erupted Poor economic state. Closely spaced children. Malnutrition not recognized 5.

5. To find problem, feasible remedies.

Health education: demonstration of value of peanuts, and other available food.

Advising certain items of food permissible before teething. Education for recognition o malnutrition. Determine ways of greater income by improving technology of occupation—introducing crafts and feasible training 6. Local govt. health centre for where supplementary food

can be obtained.

.

6 To determine adequacy of other agencies in area to deal with the condition.

Primary health care can be given at centre. Local practitioner, non-allopathic, could collaborate. Untrained midwife could be trained.

These observations lead to community diagnoses which, in turn, call for a programme of community care. Some steps for implementing such a programme are listed below.

1.

Discuss, suggest, plan steps for a method of work by which a feasible solution can be applied to identified problems. 2. Set intermediate and long-range plans. 3.

Develop operational procedures involving the community

4.

Consider a time period when effectiveness can be measured.

5.

Determine an educational process for the workers and the community.

IV Attempts to comprehend community behaviour:

Certain areas community diagnoses, such as community organization, attitudes, beliefs and values, are difficult to measure, but they are of vital importance, and the task is essential, because the behavioural pattern of the community depends upon them. Information concerning attitudes beliefs and practices is required for calls should be planned to representative groups. For example, if in the selected population 50% of the people belong to farming families, 25% to schedule castes and 25% to other castes, then the plan should be, if possible, to pay social calls in

the same proportion, i.e., if 20 calls in all are to be made, then 10 should be farmers, 5 to schedule castes and 5 to other castes. Such information cannot be obtained by regular surveys; it is best done by social calls paid to members of the community at their leisure. The conversation should be listened to with patience and gently girded so areas of concern. The following questions are example of how information may be obtained in order to determine beliefs, attitudes and practices.

a) Weaning diet: At what age do mothers in this village give babies foods other than breast milk? What do they start with? Do landowners/labourers do the same? Do you think this is the best? Did you do the same? Do you know some mothers give babies mashed vegetables as early as four month? What do you

think of that? b) Attitude to family size: what is considered a small family – too few children? Why? What be considered a large family – too many children? Why? What would you consider the “right” size? c) Attitude to a specific medical condition detected, e.g. TB/leprosy: In this village some families have cases of TB/leprosy. How is it that these families got this disease? Could they have avoided getting

it? Do you think they need attention? Where? Have thy sought /accepted it? Why not? Would you feel that other members of the family are likely to get this disease too? Is there any way to prevent this?

Attention is drawn to a few points: —

—

—

—

—

the above are simple, preplanned guidelines for direction of conversation. Interview should be conducted by a responsible person who does not attempt to interpret what is said, and yet has a sensitive ear to note relevant information given but not asked for. Record keeping or taking notes during a visit often mars the atmosphere of a social call, and genuine responses are not obtained. Diligent recording after each call is therefore recommended. If more than one person is conducting such surveys, some standardization needs to be established of comparison. Accumulation of irrelevant data serves no useful purpose. The only information required is that which can be used for concrete action.

As one can see, the steps suggested for community diagnosis are similar to those required for patient diagnosis: 1. Library reconnaissance; 2. Field reconnaissance; 3. Survey (a) basic demographic survey; (b) specific survey; 4. COMMUNITY behaviour; 5. Diagnosis. Comparable steps towards patient diagnosis:

1. 2. 3. 4. 5.

History taking; Symptoms and probing for greater detail; (a) Basic data for diagnosis leads; (b) Examination and investigation; Other factors affecting patients; Diagnosis.

field work it is the people, not the worker or his agencies that is in control” Community diagnosis is a much greater challenge than patient diagnosis. For those that accept the challenge of learning and developing themselves as well as the community, the job satisfaction is immeasurable.

Reference: (1)

Fact Finding with Rural People—A Guide in Effective Social Survey, prepared by Hain Pao Yang, FAO Agricultural Development Paper No. 52, 4th ed, 1962.

(2)

Batten T.R., Batten M, The Human Factor is community Work, Oxford Universal Press, Amen House, London EC4, 1965. (Courtesy— CONTACT)

(Cont. from page No.5) institution which carries name of Mahatma Gandhi and which was so much publicised as 'Rural Medical College.' The MFC members were questioned on this point and almost all said that it was a tragic contradiction in the name and the performance. It was realized that the phenomenon of irrelevance of standard & culture in medical colleges in India to the realities of rural area was a generalised problem of medical education. There was a strong feeling among the students that this discrepancy became very obvious when they were working in the actual rural set up of Nagapur. This it self could become an excellent topic for study. In the end it was announced by Ashok Bhargava amongst a loud laughter that inspire of repeated warnings, the word “Sir” was uttered 30 times in one hour, thus revealing the deep roots of the culture of domination and hierarchy in our minds. The gathering dispersed after enjoying a lovely song. — Rani Bang

PLEASE NOTE From this issue onwards, MFC Bulletin will be edited, printed and dispatched from Gopuri. All the correspondence

Batten and Batten (2) quite rightly say; "A community development worker may need to acquire technical skill, but his primary and basic skill is working with the people, This is a particularly difficult and complex skill to acquire - no one community or group is quite the same as the other. One lesson is that in

Editorial committee Imrana Qadeer, Ulhas Jajoo, Binayak Sen, Anant Phadke, Ashwin Patel, Abhay Bang (Editor)

regarding the Bulletin should be directed to the Editor, Medico Friend Circle Bulletin, PO. - Gopuri, WARDHA. (M.S.)

442001, INDIA.

Views & opinion expressed in the bulletin are those of the authors and not necessarily of the organisation.


