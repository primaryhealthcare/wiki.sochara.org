---
title: "Medical Education in Malayalam"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Medical Education in Malayalam from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC033.pdf](http://www.mfcindia.org/mfcpdfs/MFC033.pdf)*

Title: Medical Education in Malayalam

Authors: Dhaddha Sidhdaraj, Kumar Suresh R, George John T,

medico friend circle bulletin

3.

SEPTEMBER 1978

MODERN MEDICINE AND AYURVEDA: A synthesis for people’s medicine Ashok B. Vaidya* “Many medicinal plants used in systems of ‘primitive medicine’ are mow recognised to have specific beneficial pharmacological effects. Indeed, much of the basis armamentarium of pharmacology

Free Thinking

today has been built up by investigation the properties of traditional herbal remedies. Traditional use of such remedies evolved through countless trails and errors—in short, through human experimentation.

We think so

— Feed L. Dunn

1

Because all other people think so; Or because—or because—as all, we do think so; Or because we were told so, and think we still think so. Or because, having thought so, and think we still think so. Or because, having thought so, We think we will think so. Henry Sidgwich

2

The current confusion about various ‘Pathies’ in india can be ascribed, besides to the professional vested interests, to an untrustworthy perception of the historical forces which shaped the growth of the modern scientific medicine. Modern medicine, despite its dramatic achievements for the relief of human suffering, is attacked vehemently both by a venerated old guard of Ayurveda like Shri Bapalal Vaidya3 and the young Illichians of modern anatomy 4 like Manu Kothari and Lopa Mehta . Criticism, and that too scathing and uncompromising, sells these days. A pen comes easy and may prove mightier than a test-tube; polemics for diverse “Pathies” abound. Is there a way out and how? Firstly, we must drop our prejudices and pet world-views and freshly recapitulate the historical development of the scientific medicine. Ayurvedic medicine had greatly influenced 5, 6 and directly contributes d to the development of Greek mddicine . Subsequently, Arabic 7 medicine borrowed heavily from Greek medicine and eventually came to be recognized as Unani (Etym, Ionian) medicine. During the dark ages in Europe, Galen was worshipped as the final authority just as Atreya Punarvasu, Sushruta and Agastya are deified by “Siddha Ayurveda” protagonists in India of today. So it is obvious that the three ‘Pathies’ in India, viz. Ayurveda, Siddha and Unani medicine actually represent the ontogeny—the past stages, though partial, in the evolution of the modern medicine. Homeopathy was a reaction to allopathy—a past stage of modern medicine, which involved purging, bleeding and leeching as dominant therapeutic modalities. Once such an unbiased and historically valid view of various ‘Pathies’ is obtained, we will quit the sterile endless haranguing and evolve new rational and helpful strategies. A coherent synthesis of the valid elements of the different “systems of medicine” into a modern scientific health science is needed for our people. This requires work and not words, experiments and experience and not ego-reinforcing antiquities. Scientific Basis of Ayurvedic Drugs

The medicinal plants and herbs have been studied by many phytochemists and pharmacologists for the isolation and phramacodynamic activity of their active ingredients. Many popular and widespread practices of herbal therapy have

* Head, Department of Clinical Research, CIBA-GEIGY Research Centre, Goregaon (East)

drugs and therapy which have been studied or have a scientific validity of usage. Such lists can be prepared for different geographical regions. Depending on the plants easily available in the particular areas, the existing therapeutic practices can be reinforced and new treatment modalities can be evolved for the common diseases. The field demands active research and service work from the local groups. Herbal and medicinal plant farms must be made a compulsory responsibility of each and every Panchayat ad primary health centre. New useful plants and herbs can be grown in an area where a particular disease is prevalent. If such a movement can motivate and involve students and children, the results can prove to be very beneficial both to the environment and the health of the community. But it requires some essential human ingredients of the “total revolution” proposed by Shri Jayprakash Narayan.

been validated by scientific experiments. But the information stays captive in the speciality journals or the confidential laboratory logbooks of scientists. It is desirable that we educate the practising doctors and lay people about he scientific knowledge on the mechanism of action of Ayurvedic drugs commonly used by them. Such an educational programme will serve the following objectives: (1) Scientific work-view can affect other activities beneficially. (2) People may come forward to share their experiences with scientists, which may lead to research and new drugs and (3) A re-enforcement of the existing therapeutic practices with scientific data may popularize the use of such plants on a wide scale. (4) Scientific use of herbs and plants would tend to reduce the widespread quackery and witchcraft prevalent in out rural and backward areas. Table shows a brief list of certain Ayurvedic

SCIENTIFICALLY VALID USES OF AYURVEDIC THERAPY Drugs or therapy

Disease Eye infections

…….

Argemone inexicana

…….

Berberia aristata, Butea frondosa ……. Common cold11, Hyperacidity ……. Amoebiasis

……. …….

Inflammation-sprains, arthritic, etc Cough

…….

Menstrual disorders

…….

Ring worm

Antibacterial tannins

9

14

12

…….

Decreased HCL

Holarrhoena antidysenterika

…….

Amoebicidal alkaloids

Cephaelis ipecacuanha

…….

Psoralea corylifolia

…….

Ultraviolet - sensitive psoralens13

…….

Contains L-Dopa

…….

Anticholinergic

and chloride output

Ficus glomerata

Parkinson’s disease

Antibacterial alkalodis

Salt free diet

…….

Leucoderma

Mechanisms

14

Mucuna pruriens Atropa belladonna Curcuma longa

…….

Connessine, emetine

15

Non-steroidal anti-inflammatory curcumin Adhatoda vasica

…….

Saraea indica

…….

Casaia alata

…….

Rauwolfia serpentina

…….

14

Antitussives alkaloid

Steroidal activity

17

…….

Hypertension

…….

Antifungal agent in leaves Catecholamine depletion

New Research Approaches The major research effort of last fifty years was primarily concentrated on the pharmacological screening of Indian plants for biological activity. 19 Late R.N. Chopra was a pioneer in this field . The approach is still 20

being actively pursued . The practical returns, however, have been meagre. We have been following a new approach. We initially concentrate on the clinical screening of Ayurvedic drugs. If early studies are encouraging, place be controlled and more sophisticated clinical pharmacology is then employed. Recently Vaidya Antarkar and myself found good therapeutic activity of Arogyawardhini in viral hepatitis in a preliminary trail. Currently double-blind trials are in progress. Once clinical evidence is convincing, the efforts in pharmacology and photochemistry can be started. Even if no active ingredient may be identified

the contribution of the clinical research stands as a guide to future investigators. Another interesting approach is to use clinically a plant, whose certain constituents have shown an interesting biochemical activity in animals or in Invitro systems. Certain wheat-germ lectins have shown insulin-like activity in isolated tissues. It may be interesting to investigate the clinical possibilities of such a novel effect. Berberine has shown good local anesthetic activity in animals. Plants containing Berberine in significant quantities can be studied in crude for local anesthetic property. This approach can be very fruitful because many experimental observations on plants and herbs are awaiting clinical explorations. Only plants which are known to be safe can be studied clinically. (Continued on page 7)

EDITORIAL This issue in your hands opens three new topics for further probing and discussion – Indigenous medicines, medical education and the nursing profession. After the ‘not that successful effort of MFC to discuss the synthesis of various pathies at its second conference at Sevagram, there has not been much thought given to this importance issue. The indigenous medicines are important but just because they are ‘indigenous’ but because they have the potentialities to help to liberate the masses from the economic exploitation by the drug industry and the cultural slavery of medical professionals. But unless more organised efforts are made, this vast treasury will also be looted by the established shrewd health industry — drug companies and doctors — and then the same medicines will be supplied at very high cost in more attractive forms, using all the salesmanship to befool the masses, who will be made to abandon and forget the use of their ‘crude’, ‘unscientific’ remedies and accept these ‘new’ medicines by the process called ‘health education’. We critics the drug industries for the exploitation they do. The positive action which can be added to this criticism is to build the social pressure and control over these corporations and secondly to find, try, use and educate people to use the alternative technique of health care where the dependence on the drug companies and the doctors is reduced.

I was hungry and ….. I was hungry and you blamed it on the communists I was hungry and you circled the moon I was hungry and you told me to wait I was hungry and you set up a commission I was hungry arid you said so were my ancestors I was hungry and you said "we don't hire over 35" I was

Medical education is another very vital, but almost untouched field. It really needs a ‘cultural revolution’. The problems touched in this

hungry and you said God helps

issue are real but only peripheral. There are many important areas in

those….

medical education like aspirations and cultural of the medical education, its social accountability, social control over the planning and priority

I was hungry and you told me I

decisions in the medical education, its costly and elite nature and so on.

shouldn’t be

These need very thorough rethinking.

Every medical student reads and listens at least hundred times during his education the aphorism. The history must be recorded in the ‘patients own worlds,’ in the ‘patient’s own words’, but translate into the foreign language! How many patients give history in English? But still we all continue with this contradictory situation! How will question and when? Some of the letters on the issue of Medical education in Malayalam reveal what considerations guide the stand of doctors on this issue. There is a fear of getting alienated and deprived from the employment opportunities outside the country but no botheration about getting alienated form our own country men, their language, their culture and their problems by the dependence on a language unknown to majority of them. There are not many nurses in MFC. But the discussion on the problems of doctor’s nurse’s relationship will help us understand our won class and sex bias. — Abhay Bang

I was hungry and you told me machines do that work now I was hungry and you had napalm bills to pay I was hungry and you said “the poor are always with us” Lord, "When did we sec you hungry?"

Matthew

NEWS CLIPPINGS The Need, The international conference on primary health care now being held at Alma Ata, USSR, under the auspices of WHO and UNICEF and called for a global plan to raise the health of all people of the world to all acceptable level by the year 2,000. Both the organisations have promised their support to the effort each country is making in the respect to that the overall target may be reached. According to World Bank million people, do not get the minimum health care. Almost all of them are in the developing countries or in regions too backward even to plan or organise for economic and social development, WHO reports that fewer than10 per cent of the children born in a year in poor countries are immunized against the five most common serious childhood diseases. The adoption of the Western model in this countries for instance, has led to an over-centralised, over- professionalised and over-urbanised system of health services oriented to the needs of the well-to-do classes. This system has led to the neglect of the preventive aspects of medicine and failed to provide elementary medical care to the community. The order of priority needs to be changed. Indian Express, September, 9

The Words In the developing country like India, only sophisticated curative health care is not sufficient, and it needs social and economic change to overcome the various diseases on increase due to poverty and insanitation—said governor of Maharashtra Mr. Sadiq Ali, while inaugurating an International Seminar on Heart Diseases. Bombay, Sept. 10. Hindustan Samachat

In the Deeds….. The National Kidney Foundation (India) has been let up, with initial funds of about Rupees three lakhs, to prevent and control kidney diseases which were affecting increasing numbers of people in the country, the Chairman or the fund. Mr. Mathuradas Assomull, announced here today. Describing the major objectives of the foundation, at a press conference, Mr. Assomull said public education in kidney diseases would be a high priority. The foundation would also encourage research into the prevention of kidney diseases, which affect 50 persons per million per year in the country. Bombay, September, 11 P.T.I Recently, a new organisation has taken shape in the city, called the pacemaker club of India. Its members are users of the sophisticated electronic gadget called the ‘Pacemaker”. Bombay, September, 9 Indian Express

MEDICAL EDUCATION Physiology and Frogs In medical education it has always been noticed that the overall character is unsatisfactory. I feel that it is high time how that we scrutinise in details the various branches of medical education, the drawbacks therein and the radical changes which are emergent. As a teaching member in the department of physiology I feel that the experimental physiology as is taught in the practical in full with the following drawbacks. 1) Students are taught to dissect the frog. 2) Students are expected to master the operation of revolving smoky drums. 3) Students are supposed to get the graphic records of muscle contractions etc. and keep all this material in the form of journal. If We analyse further, we realise that students never come across the frog dissention in their future career as doctors, and never need it even for theoretical understanding because a theoretical undemanding can certainly be accomplished by deportation alone. In that case only the demonstrator has to demonstrate. In short then number of frogs, 2n number of hours and lot of energy of students can be saved from the wastage and can be utilised for more useful, fundamental and applied work in the subject. I feel it is emergent that the experts in the other branches of medicine also put forth their views on the present status of medical education in their fields. The medical education in its entirety needs to be evaluated scientifically with social revolutionary perspective in mind.

Shrinivas J. Kashikar Dept, of Physiology, G.S. Medical College, Bombay

CALL, CALL, CALL………… 1) The Health Project in NEFA is being restarted by Shanti Sena Mandal. (For those who don't know — ex-editor of our Bulletin, Ashvin Patel was incharge of this project for a1most 2 years in its earlier period.) Sanghmitra Gadekar is preparing to go there and writes to MFC that one more doctor and one paramedic with experience in, T.B. is urgently required. Please write to - Dr. Sanghmitra. Gadekar, c/o Shri Narayan Desai, Sarva Seva Sangh, Rajghat, Varanasi, 221 001. 2) Kishore Saint from Seva Mandir, the famous educational institution writes: "We are ill need for a Project Director for our new Mobile Health Unit-cum-Community Health Programme. He should be a fully qualified doctor with a strong orientation towards Community Health, especially for the rural poor." Contact- Shri, Kishore Saint, Seva Mandir, Udaipur 313 001. 3) A rural development project in a semi tribal area in Chandrapur was running a health project with help of a homoeopathy doctor. It needs a doctor from any ‘pathy’. Write to Shri Ram, Rural Development Project, Rajura (Manikgarh) Dist. Chandrapur, Maharashtra.

Dear Friends I

Data not readable……….

it would render the Malayalee doctors unable to compete with doctor’s form other parts of India on an all India basis. As present a lot of Malayalee doctors and employed in foreign countries. And, in view of our unemployment problem, this is indeed a great blessing. In case there is a change over to Malayalam, no doctor would be able to get employment in foreign countries and year after year, the ranks of the unemployed doctors in Kerala will swell. This “reform” will not benefit anybody in anyway. On the other hand, it will only cause difficulties for the State Government as well as for the doctors. T. John George Alleppey MFC News MFC Group in Bombay 34 MFC members of Bombay, who were scattered up till now, gathered together in the month of August to give some definite shape to the group. Happily for them, Navneet Foujdar form Gujrat and Raj Arole from Jamkhed were also present in the meeting. The group decided to undertake following activities. 1) Developing a library of non-technical books and literature on health care in the premises of Yusuf Meher Ali Centre. 2) Visits to the experimental projects in the rural areas of Maharashtra. 3) To associate with Yusuf Meher Ali Centre and its village project near Panvel for trying to implement new ideas in alternative models of health care. 4) Arranging lecture, debates and essay competitions in the Medical Colleges. 5) To undertake some educative activities on TB and leprosy.

II

Sir, — Does the Kerala Government think that to get all the medical books especially Grey’s Anatomy and Bailey & Love’s Surgical Procedures, translated into Malayalam is an easy task? Is it practical to find out equivalents for all the medical terms? In Malayalam is made the medium of instruction

A voluntary coordinating commi8ttee was formed which includes Ramesh Awasthi, P.S. Tathed, G. G. Parikh, Rashmi Kapadia, Uma Ladiwala, S.S. Pandya, Indumati Parikh, J. K. Shah. (The prefix ‘Dr.’ omitted according to MFC Tradition.) Rashmi Kapadiya Bombay * * * For MFC members visiting Calcutta An member of MFC who is well versed with the philosophy, organisation and activities of MFC when plans to visit to Calcutta should give presentation so that a get-together can be arranged there. Inform Lalit Khanra, Tamralipra Health Home, Po. Tamaluk, Dist-Midnapore (W. B.) 721636. Or, Shri Sauman Guha, Editor, Vijnan-Sanskriti Block, L/D Flat-5, L.I.G Housing Estate, Kusthia Road, Calcutta. 700039.

NURSES: THE CURSED NIGHTINGALES

“How to motivates doctors to go to the rural area?” “Post beautiful nurses at PHC’s” was the reply.

Rani Bang Recently, while I was working in the ward, one staff sister approached me and, to my surprise, requested me to take the responsibility of having broken one syringe. Realising that I was puzzled, she explained to me that a 20 ml syringe was broken by her while working. The hospital rule is that if a sister, even by mistake, breaks a syringe she has to pay the cost of it while if the same mistake is made by a doctor he or she is totally excused for that. This was the reason behind her request to me. The funny thing is that the most of the injections are given by the sisters and hence naturally they are the persons, who happen to break the syringes. Then why this double standard? But the discrepancy is not limited only to such minor incidences and can be seen in all realms of the doctor – nurse relationship. The nursing profession, which plays so vital role in the health care is riddled with the problems – economic, social, cultural, and the doctor – nurse relationship problem is only one, but probably the most pricking, of those. If one aggress that the prestige and recognition to a particular processional group should be in proportion to its usefulness to the society then doctors and nurses deserve equal prestige and respect. But unfortunately what one observes is that the services of the nurses are very poorly recognised by the society and there is a vast difference in the status of doctors and nurses. The doctor’s treats the nurse like a second helper to him or her in the patient care, and not like a colleague, even though the nurse is better prepared than the doctor in certain areas during her training. But as the nurses are not trained to do diagnosis and decide treatment they don’t get their due credit – either form the doctors or form the patients. The creative satisfaction goes to the doctors and what remains in the nurses’ lot is the laborious monotony. Why? The answer to this “why?” is not simple and is deep rooted in the values of our social system. In our morbid society the intellectual work fetches more respect than the manual labour. A white collar job of officer is always superior to the fitly, sweating job of a labourer. And as lot of physical and ‘non-intellectual’ work is involved in the nursing, the status of the nurses becomes inferior to those God figures of health care – the doctors. But there is an another cultural dimension added to ditch the nurses further down. Ours is a male dominated society and hence a profession like nursing, overwhelmingly occupied by females, can hardly get equal status, however unique its contribution might be. The doctor-nurse relationship also reflects the male-female relationship in our society. A doctor, even if she is a female, becomes the husband figure – ordering, scolding, and dominating the nurse. The point becomes very clear when one observes that the male nurses, brother, receive very different treatment. Brothers

command wore respect by the doctors, patients and even by the sitters. Doctors admit that they don’t feel that free to order or shout at brothers while the sisters are, at times, even physically assaulted by the male doctors. Few months ago there was a news in ‘Dinman' that one doctor slapped a sister in Rewa Medical college. In protest the sisters went on strike, asking for the appropriate action on the concerned doctor. But their strike failed miserably as they received constant threats from the college authorities who probably felt it insulting to see the nurses protecting and challenging the authority of the doctor. In no other profession the chastity of the woman is less secure than in nursing, except of course in prostitution. The exploitation of nurses is all pervading – economic, social, cultural and sexual. Many nurses are often at the mercy of everyone in the hospitals – the superintendents, doctors, patients, relatives of the patients and even the ward-boys. The relatives of the patients in private wards very frequently harass the nurses, specially during the night duty. And since they are influential people, they threaten the nurses. The timid ones submit while those who don’t have to face complaints, suspensions and remarks in their records as ‘disobeying, negligent in the duty,’ as if to please every male is also a part of their duty. When this is the state of affairs with relatives of the patients, one should only imagine what must be happening between doctors and nurses. The outlook of many doctors towards nurses is of sexual exploitation. This was very poignantly expressed in the answer of a doctor to the question. “How to motivate doctors to go to the rural area?” “Post beautiful nurses at the PHC’s” Specially in the remote villages where nurses and ANMs are posted they are very insecure. A nurse is looked at as a catchy prey by all the village ‘Dadas.’ There was a tragic case of Miss Vaidya who was murdered in Vada village in Maharashtra because she refused to give in to the sexual overtures of the local leaders. But the sex is not the only form of exploitation. Financially they are quite low paid. The work load in the ward is crushingly heavy. To add to it they are made to do certain chores which are not part of the nursing, like, to count linens, keep records or go to the hospital stores for supplies and all sorts of clerical jobs like fill-in up forms, diet sheets, indent requests and so on. And inspite of all this, what is the independence and authority that the nurses have? Even a VHW with three months training is authorized to dispense drugs like sulfa or Chloroquin but a sister after having had a training of three and half years has to go to a doctor for a signature even if she wants only aspirin from the hospital for her won use. ***

(Cont. from page 2) One productive field of research can be the study of the molecular basis of Ayurvedic drug action. Active plant constituents can be studied form their effects on enzymes, RNA and DNA synthesis, membrane-transport, etc. Molecular biologists, pharmacologists and Ayurvedic scientists have to work as a dedicated team for such research.

Economic and Industrial Aspects In India, we must concentrate on economical but safe and effective substitutes of many expensive and rare modern drugs. The widespread love of infectious has reached quite a pandemic proportion in our country People have to be re-educated into the habits of good dietary and health regimens. Crude plant products can be presented in more palatable form. New formulations can be developed which are adaptable to the level of small-scale village-industry. All this requires a deep motivation and creative effort by our best minds. But it can be done. Home-remedies can be emphasised and the preparation taught to housewives and teacher. The herbal and plant products are quite available for labour-intensive and decentralized small-scale industries. Complete exemption from all taxes, licenses, etc. But making expert supervision mandatory may attract many idealistic entrepreneurs to such an industry. Forestation and herbal farms must become a national obsession. All media of communication should be effectively employed to achieve this.

Ayurveda undergoes a basic transformation. Ayurveda has to become Ayur-Vigyan. Scriptural authenticity has to willingly be transfigured into heuristic scientific corpus by dedicated and original research by hundreds of scientists. Ayurveda had a glorious past. Ayurvigyan will have a more glorious future in the service of the suffering humanity. REFERENCES 1. Dunn, F.L: Traditional Asian medicine and

cosmopolitan medicine as adaptive systems in Asian Medical systems: A comparative Study, pp. 133, Ed. Charles Leslie, University of California Press. Berkeley, 1976. 2. 3. 4. 5. 6. 7.

People’s Medicine: The lay people often get confused when doctors of diverse 'Pathies' fight and blame each other. To a sick person what matters is a relief from suffering. He dots not care about what 'Pathy" procures it. As a consequence, our people with their robust common sense have developed strategies for health-care, which though far from perfect, are quite functional. People’s medicine, in India, is a strange admixture of home remedies, faith-healing, witch-craft and diverse 'Pathies’. This is the reality we have to face. How do we transform such a pot-boiler health system into a scientific yet culturally acceptable and organisationally effective new system? We have to deeply explore the anthropology, sociology and economics of diverse medical systems and evolve workable adaptations, compatible with the human and humane aspects of patient care. Placebo does have a place in therapeutics when at least the physician knowingly and purposely uses it. But an entire system based on mere placebo cannot have any place in modern times. Some decisions will be painful. But in the interest of the patient, we will have to scientifically compare the efficacy and safety of drugs of diverse ‘Pathies’. Much useless junk persist in all the systems of medicine. Ruthless and objective clinical research can only assist in the development of a respectable people’s medicine in India. In conclusion, a coherent synthesis of the diverse systems is possible by promoting further interactions among open-minded and motivated expert of diverse systems of medicine. Ayurveda can continue to provide valuable ideas for research in basic and applied biomedical research. But this would be possible when

Khanra, L: Alternative Approach: Various 'Pathies' M.F.C. Bull. 1-2: G, 1976.

8.

Vaidya, B.G.: Ayurveda and allopathy M.F.C. Bull. 10: 6, 1976. Kothari, M.L. and Mehta, C.: Knowledge Is confusion. M.F.C. Bull. 17: 5, 1977. Vaidya, A.B. and Pandya, S.K.: The golden age of Indian medicine, Bombay Hosp. J. 3: 95, 1961. Vaidya, A.B. and Pandya, S.K.: Greeka-Roman medicine. Bombay Hosp. J. 5: 95, 1963. Vaidya, A.B. and Pandya. S.K.: Arabic medicine. Bombay Hosp, J. 4: 52, 1962. Vaidya, A.H.: Modern medicine, Science Today. Oct.

1974, p.26. 9.

Foley, G.E. et al; A Comparative study of the use of microorganisms in the screening of potential anti-tumour agents. Ann. N.Y. Acad. Sci. 76:413, 1958. 10. Nadkarni, K. M.: Indian Plants and Drugs, Norton & Co. Madras, 1908. 11. Vaidya, A.B., Kothari, M.L., Kapadia, J. and Sheth. U.K.: Common Salt and common cold. Lancet, i: 1329, 1960. 12. Kothari. M.L., Vaidya., A.B., Doshi, J. C. et al: Raison d’etre of hydrochloric acid secretion by stomach. Ind. J. Med. Sci. 21:1, 1967. 13. Pathak, M.A. et al: Effect of-structural alteration on the photo-sensitizing potency of furcoumarins (psoralens) and related compounds. J. Invest, Derm. 48: 103, 1967. 14. Vaidya, A.B. et al, Treatment of Parkinson’s disease with the cowhange plant M. pruriens. Neurology (India). Accepted. 15. Damodaran, M. and Ramaswamy, R.: Isolation of L-dopa from the seeds of Mucuna pruriens, Biochem. 31: 2149, 1937. 16. Srimal, R.C. and Dhawan, B.N.: Pharmacology of diferaoyl methane (curcumin), a non-steroidal antiinflammatory agent. J. Pharma. Pharmacol. 25: 447, 1973. 17. Satyavati, G.V.: Personal communication. 18. Iggo, A. and Vogt, M.: Preganglionic activity in normal and in reserpine-treated cats. J. Physiol. Lond. 150: 114, 1960. 19. Chopra, R.N.: Indigenous drugs of India. U.N. Dhar and Sons, Calcutta, 1958. 20. Dhar, M. L. Dhawan, B.N., et al: Screening of Indian plants for biological activity: Part V, Ind. J. Exp. Biol. 12:512, 1974.

Clinical Trials with some Ayurvedic Preparations When the various systems so of Medicine are taken into consideration, Allopathy is quite an expensive line of treatment. And since it is beyond the reach of the poor masses, they are almost always compelled to seek the so called “medical assistance” from the easily available and easily approachable quacks or unqualified medical practitioners. The increased cost of medicines and the incidence of more untoward reactions are the two major pitfalls of modern medicine. In this instance it is quite smoothening and enlightening to know of certain preparations that are found to be more effective in combating certain diseases and can be prescribed with much confidence and self satisfaction. It is true that the complete pharmacology of the ingredients of these Ayurvedic preparations in not known and the great reluctance on the part of Allopathic doctors in prescribing them can the well attributed to this fact. But if the absolute clinical efficacy of a drug is proved beyond doubt, it having very little or no side effects at all and if the cost of treatment is minimum, there is no justification for ignoring these valuable remedies, provided our aim is to treat a patient and not simply to cure a disease. I would like to share with the Medico Friends,

the clinical experience I had during the last one year, with a few of these useful preparations. More than two hundred cases were studied in each. 1) CYSTONE (HIMALAYA DRUG CO.) in the treatment of urinary infection and urolitiasis:- Cystone has proved to be highly efficacious in the treatment of urinary infections and urinary stones. 2) R. COMPOUND (ALARCIN) AND RUMALAYA (HIMALAYA DRUG CO.):- These are two other herbal preparations found to be quite effective in the treatment of all forms of Rheumatic disorders. The reckless treatment with the most dangerous group of drugs viz. steroids is a growing tendency among today’s practitioners and that too without much success. Where the steroids failed, in many cases, R. Compound or Rumalaya had been found successful. There are many more preparations worth mentioning. Liv 52 (Himalaya Drug Co.) used in the treatment of jaundice and other liver conditions, has undergone clinical and experimental trials even abroad and its therapeutic utility is proved. It will be highly enlightening to hear from the Medico Friends, similar experiences they had so that those interested will be able to utilise nature's healing power for a better health and vitality.

N. P. Kanchana Mala Kerala

21.

The Joke Of The Year OPPI (Organisation of Pharmaceutical Producers of India) is repeatedly advertising following, and expects you to believe it.

—

Drug prices India are among the lowest in world.

—

Allegation of high price of drugs and excessive profits are irrelevant in Indian

context.

—

The poor can not buy medicines because they are too poor and not because the

—

Editorial committee: Imrana Qadeer, Ulhas Jajoo, Binayak Sen, Anant Phadke, Ashwin Patel, Abhay Bang (Editor)

drugs arc costly Expenditure on the medicines is less than 10% of the total cost of medical care. Hence the cost of the drugs is a negligible factor. Neglect it. Make noise about the cost of other components of medical care.

Views & opinions expressed in the bulletin are those of the authors and not necessarily of the organisation.


