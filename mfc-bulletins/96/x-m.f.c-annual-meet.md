---
title: "X M.F.C Annual Meet"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled X M.F.C Annual Meet from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC096.pdf](http://www.mfcindia.org/mfcpdfs/MFC096.pdf)*

Title: X M.F.C Annual Meet

Authors: Phadke Anant

medico friend circle bulletin

DECEMBER 1983

MISUSE OF CORTICOSTEROIDS Ulhas Jaju Sewagram In a majority of illnesses beyond cure, the pest that the allopaths can do is to suppress the severity of the disease manifestation and to provide relief from agonising symptoms to make life more comfortable. The bulk of the share is credited to the, corticosteroid group of drugs. They are the most powerful, antiinflammatory and immuno-suppressor drugs — a boon for non-curable inflammatory and immune disorders. Though very potent and life saving drugs, steroids can cost life — if not properly utilised. Over-use and misuse of steroids has posed life threatening complications like reactivation of dormant infection notorious being tuberculosis; bleeding peptic ulcer, bone rarifaction and fractures; precipitation of coma in diabetes etc. Although it is a drug of last resort in most of the illnesses, somehow it is used very frequently by the doctors. The reason for the popularity is the dramatic relief that these, drugs offer which helps the doctor to earn credibility. Steroids bring down temperature, relieve joint swelling and pain, relieve asthma, suppress allergic reactions etc. just to mention a few effects. Quite often and wrongly, steroids are prescribed as the drug of their choice. There are different ways in which- steroids are misused/overused. (i) Prescribing steroids where the drug has no value: The common examples ate viral hepatitis, viral infection (except tare examples where antiinflammatory property is utilised), fungal skin lesions and parasitic infections (with exception where it is used to suppress allergic manifestations).

Disorders like acute rheumatic carditis, shock, myeloblastic leukemia, and alcoholic hepatitis are situations where the role of these drugs is still debated. (ii) Overshoot by doctors for early relief of symptoms: Steroids are never the first line drugs in diseases like bronchial asthma and rheumatoid 'arthritis. In bronchial asthma, topical therapy or inhalation should be the preferred route of administration. Decision for systematic therapy must be taken with great care since the majority of patients once put on corticosteroids remain indefinitely on such maintenance therapy. Its use to suppress fever of unknown cause is unscientific. Fever is a' useful 'body response against the offending agent. (iii) Wrong route of administration: Systemic therapy has more complications than local use. Hence, in disorders like bronchial asthma (inhalation, skin application) and ulcerative colitis (enema), systemic therapy is better postponed as long as possible. Topical steroid application or subconjunctival injection of steroids builds up adequate levels of the drug in the anterior segment of the eye. Systemic therapy thus, is unwarranted, (iv) Selection of wrong drug: Among corticosteroids, some are immediate acting (hydrocortisone) while others take hours (dexamethasone). Injection of a drug like dexamethasone, In ran urgent situation like anaphylactic shock is merely to satisfy oneself that immediate action has been taken. The drug of choice should be intravenous hydrocortisone.

The usual practice of adding steroids: to blood to (v) Prolonged use of drug even after, remission is suppress transfusion reactions needs to be de- achieved: In diseases like nephritic syndrome, nounced. It is always better to know the blood multiple sclerosis, ulcerative colitis, the drug has no role in maintaining remission. The side effects of transfusion reactions early, than to suppress them. prolonged use overweigh the utility.

(vi) Guns fired by the drug industry: Many fixed Combinations containing antihistaminics and steroids dose combinations of steroids exist in market. Fixed lead to the hazard of steroid dependence. drug combinations do not allow alteration of any of Reasons for misuse: their components, nor allow for varying the dosage the Ignorance of the prescribe, over-reliance schedule of individual medication. For any disease, in claims of drug companies and their representatives, any patient, the appropriate dose of corticosteroids to anxiety to earn a good reputation by providing early achieve a specific therapeutic effect must be relief of symptoms (directly proportional to the determined by trial and error and must be re- income), absence of cross-checks on' irrational evaluated from time to time as the stage and the prescribing habits, aggressive marketing' strategy of activity of the disease alter. One of the fixed-dose the pharmaceutical firms, failure of government to combinations recommended by the subcommittee of restrict irrational drug combination's in the market the Drug Consultative Committee for being weeded and above all, lack of consumer education, are the out, are steroid combinations. reasons for these unethical practices.

on

The following facts need to be kept in mind while studying the accompanying table showing some available fixed-dose combinations. a) Pain-killers should not contain steroids. b) Drug combinations containing corticosteroids and anabolic steroids have no scientific basis. c) Steroids are useful in treatment of filarial infection only when immediate sensitivity reactions are observed on administration of di ethylcarbamazine citrate. Routine advocation of steroid + DEC combination unnecessarily exposes the patient to the toxicity of steroids. Steroids do not have anti-microfilarial action. d) For allergic disorders of chronic nature, steroids are used only as a desperate remedy.

Remedy: Education of the uneducated (doctors) may pay minor dividends. The drug can be made available only for specific indications when signed by a qualified doctor. A people-oriented government can ban fixed-dose combinations. The last but not the least, consumer education and their voice against irrational prescriptions alone can, in the long run, help in curbing such bad practices.

Bibliography: 1. Med. Clin. N.A. Sept. 1973. 2. Ann. Int. Med. 81: 505, 1974. 3. J. App. Med. 7: 1007, 1981. 4. MIMS India Vol. I, No. 12, 1981.

DRUGS CONTAINING CORTICOSTEROIDS Brand Name

Pharmaceutical firm

Ingredients

PAIN KILLERS Deltaflamar (Tab.)

INDOCO

— Dexamethasone 0.25 mg — Oxyphenbutazone 75 mg — Dried aluminium hydroxide 150 mg — Mg. trisilicate 100 mg

ingapred (Tab.)

INGA

— Phenylbutazone 50 mg — Prednisolone 1.25 mg

Rumatin (Tab.)

NOEL

— Dexamethasone 0.25 mg — Paracetamol 150 mg — Phenylbutazone 100 mg — Diazepam 2 mg — Dried alum-hydroxide gel 60 mg.

Rumatisone (Tab.)

SIRIS

— Prednisolone 2.5 mg — Phenylbutazone 100 mg — Atropine methonitrate 0.15 mg

Thilazone-P (Tab.)

UNIQUE

— Phenylbutazone 125 mg — Dexamethasone 0.37 mg — Mg. trisilicate 150 mg

Triactin-D (Tab.)

PHARMED

— Dexamethasone 0.25 mg — Phenylbutazone 100 mg — Mag. Trisilicate 150 mg

Brand Name

Pharmaceutical firm

Betaflam (Tab.)

VILCO

Ingredients — Betamethasone 0.25 mg — Oxyphenbutazone 100 mg — Analgin 250 mg — Diazepam 2.5 mg

STEROID ANABOLIC DRUG Dexabolin (Tab.)

ORGANON

— Dexamethasone 0.5 mg — Ethylo-estrenol 0.5

ASTHMA PREPARATIONS Asmaplon with KHANDELWAL prednisolone (lab.)

— Theophylline 100 mg — Ephedrine HCI16 mg — Phenobarbitone 16 mg — Papaverine HCI 8 mg — Prednisolone 2.5 mg — Betamethasone 0.25 mg — Chlorpheniramine malleate 2 mg — Ephedrine HCI 15 mg — Theophylline 120 mg — Prednisone 15 mg — Ephedrine HCI 10 mg — Theophylline 80 mg — Phenobarbitone 10 mg

Betasma (Tab.)

VILCO

Cortasyml (Tab.)

ROUSSEL

ANTIFILARIALS Neusonil Forte (Tab.)

PCI

— Dexamethasone 0.33 mg

UNICHEM

— Diethylcarbamazine citrate 100 mg — Thophylline 90 mg — Dried AI. hydroxide 60 mg — Prednisolone 3.75 mg

Unicarbazan Forte (Tab.)

— Chlorpheniramine malleate 5 mg

STEROIDS + ANTIHISTAMINIC Perideca (Tab.) MSD

— Dexamethasone 0.25 mg — Cyproheptidine HCI 4 mg.

Ref.: MIMS India, Vol. 1 No. 12, 1981.

IS ANTITUBERCULARS TREATMENT REALLY VERY EXPENSIVE? Dr. Nagendranath Nagar*

Generally, it is said that treatment of tuberculosis is quite long and very expensive and hence patients stop treatment as soon as they fed better. I submit that though treatment is quite long, it is not as costly as is generally thought, by doctors and social workers. Injection Streptomycin (SM) + Isonex (INH) = Rs. 3.00 + 0.10= Rs. 3.10 per day. (Injection charges not included) If SM + INH regimen is followed, it will cost Rs. 3.10 per day, that too only for the firs month. In the second month the patient has to take the injections on alternate days thereby reducing the to average of Rs. 1.60 per day. After another 30 injection i.e. by 4th month 'the expense is further reduced, as the patient has to -take the injections only twice a week for another 4½ months (30

injections). Thus having completed a total of 90 injections, the patient has to take only INH 300 or he may take INH + Thiacetazone tablet, costing paisa 10 and 20 respectively, per day, for another one year. Inj S M = 1 per day X 30 = …………………. 1 month + INH 300 Inj. S M = 1 Alt. day X 30 = ……………..

2 months + INH 300 Inj. S M = 1 twice weekly X 30 …………….. 34 months + INH 300 Then only INH 300 or INH 300 + Th 150 per day for 12 months. But, this regimen is useful in places where free injection facilities (like Govt. PHC) are available,

* Anjuman Hospital, Dahod.

or where a compounder or nurse is available who will charge minimum for injection (Rs, 1 or paisa 50 only.). In my opinion, another, cheaper regimen would be:Ethambutol (800 - 1200) + INH 300 or INH + Th per day =

(Rs. 1. 30 - 1. 90) + 0. 10 or 0. 20 = Rs. 1. 40 -- 1. 50 to Rs. 2.00 - 2. 10 per day. In this regimen, the patient not only saves the injection charges but also saves his ,time and energy, which he has to spend in going to the place of injection. A lot of inconvenience can thus be avoided. Though many people doubt the efficacy of Ethambutol, in my opinion, this is the ideal combination for our rural population. The medicines are to be continued for at least 1 year, to be followed by INH or INH + Th for another 6 months. Thus it is evident that for an uncomplicated case treatment does not cost .more than Rs. 4 per. day. It is another matter that there are many people, who can not afford even this. If the treatment doctor is really interested in the-patient, he can well utilise his intelligence in tailoring the treatment plan, according to the financial condition of his patients. Truly speaking treatment is made costly by vested interests and ignorant doctors. How? 1.

By addition of: (useless) malt and other tonic preparations to the prescription. The clever pharmaceutical people, so strongly hammer down the importance of various malts and ferrols, that the moment the young doctors see a case of T. B. they would first write the ferrol and then 5M or INH

2.

Many doctors think that giving injectable calcium to TB patients helps to calcify the lesions early. Many doctors use various calcium preparations to dissolve the SM powder for injection. This practice definitely helps the doctor and the pharmaceutical companies but the patient the least. It increases the cost by another Rs. 3 or 4 per

injection 3.

Addition of various other calcium iron multivitamin, protein and anabolic reparations to the prescription.

Thus a treatment which should originally cost Rs. 3 or 4 is unnecessarily being made expensive,

costing Rs, 8 lip to 10 per day. I would ask had a person been able to afford Rs. 8-10 per day on his drug bills, why should he have tuberculosis? I would like to avail this opportunity to request my sensible colleagues to ponder over their own prescriptions and modify their habits for the benefit of poor patients. I t has been proved beyond doubt that most of the TB patients would improve gradually, if they take anti-TB drugs regularly, without help of iron, calcium or multivitamin preparations. The addition of Thiacetazone Ito INH is quite beneficial. It affects the cost factor only by 10 paisa per day. The shorter regimens using Rifampicin and pyrazinamide are no doubt very effective and promising but these drugs are still beyond the reach of our poor masses. Most of the private practioners do not disclose the diagnosis of tuberculosis-to the patient because of the false fear of losing the patient. I would suggest that the patient should be well informed about the disease and he must be well convinced to continue the treatment for it years. He should also be taken into confidence about the cost of the treatment. All these measures will definitely reduce the number of defaulters and would improve compliance. It would not be out of place to mention that ambulatory treatment is preferred these days and sanatorium regimen is .reserved only for complicated or gravely ill patients. In my opinion M. M. R. is the best method of detection of Pulmonary TB, though the equipment is very costly (around Rs. 7, 00000/-, 50% of which is custom and excise duties and other taxes). But one chest X-ray would cost only Rs. 1.00, as against a full size X-ray costing Rs. 30 LO Rs. 60. A large net work of Tuberculosis associations and clubs should be spread throughout the country, where i) the drugs should be sold at subsidised prices. ii) the injections can be administered to the patients, free. These centres need not have a doctor in attendance but one or two experienced paramedical staff who can administer injection and keep records. The voluntary health organisations must press the Government to exempt excise duty and other taxes on antituberculars drugs, and M. M. R. apparatus. **

FANCY, FALLACY AND FACTS ABOUT FIXED.DOSE FORMULATIONS — S. K. Kulkarni* With the changing trend in prescription writing polypharmacy of prescribing more than one drug for a particular ailment has become very common in medical practice. Whether the physician prescribes multiple drugs because fixed combination dosage forms are available or vice-versa i.e. pharmaceutical manufacturers make these dosage forms as physician prescribes more than one drug at a time is a highly debatable issue. But in either way 'the patient is exploited. More than one third of all the new drug products introduced world-wide during 1978 were fixed combination preparations. The trend varied from country to country. In Japan only 10% of the new products were fixed-ratio combinations whereas in European countries it was up to 56% as in Spain. However, such statistical data are lacking for the developing countries although the trend is for the production and prescription of fixed combination drugs. The WHO TRS 641 (1979) while listing nearly 650 "essential drugs" included only 7 such drug combinations. There are many preparations of fixed dose combinations available for the treatment of various ailments ranging from nutritional deficiency to cardio-vascular diseases It is interesting to note that highest number of such preparations included vitamins (110), cough suppressants (71), antidiarrhoeals (64), iron preparations (66), antacids (,0), analgesics (67), and tonics (65). The enormous difference in the cost of these preparations is worth studying. In none of the above categories, however, really as any need for multiple drug treatment. The availability of such a high number of combination products clearly shows (i) public ignorance (ii) lack of pharmacological knowledge of doctors and (iii) exploitation by the industry. The protagonists and antagonists of fixed dose combinations may have their own arguments but the drug regulatory authorities should look into the therapeutic rationale of these combination products strictly on their merits and help in the prevention of public exploitation. It is essential that public awareness towards unnecessary medication through fixed combination should be increased through media.

Do Fixed-Dosage Advantage:

Combinations

have

any

1. Improved compliance: In situations such as hypertension, mixed bacterial infection, where more than one therapeutic compound "is indicated fixed combination has better patient compliance. 2. Synergism: The widely accepted synergistic: combination is Co-trimoxazole (Sulphamethoxazoie and Trimethoprim). This combination is official in certain pharmacopoeas. By sequential blockade this combination shows enhanced therapeutic action. But recent evidence tends to question such efficacy at least in the treatment of urinary tract infection. Trimethoprirn alone has been demonstrated to be more effective, with low incidence of resistance and side effects. In the light of this the future role of Co-trimoxazole remains to be determined. It is unwise to use this combination in minor infections. The other classical example of synergism is the use of thiazide diuretics with other antihypertensive agents in the management of essential hypertension. During 1979 in Germany alone more than 16 million prescriptions were written for such combinations.

3. Enhanced efficacy: The discovery of peripheral decardoxylase inhibitors has not 01111y enhanced the therapeutic efficacy of levodopa in Parkinsonism but has reduced the dose of levodopa and hence its side effects. Similar example of estrogen and progestogen combination in oral contraceptives may he given. Treatment of tuberculosis calls for more than one drug at a time. This not only enhances the efficacy of combined treatment but also reduces the risks of bacterial resistance. However, fixeddose combinations of Rifampicin and Isoniazid pose problems as Rifampicin dose is calculated on the basis of body weight. An added risk of inflexibility in dosage regimen of Rifampicin is over weighed as against the beneficial action. The Government has recently decided to stop the production of this combination.

* of P.G.I., Chandigarh

4.

Reduced

side

effects:

Some

argument is given for the co-administration of

second drug along, with the primary drug to, overcomes the side effects of the primary drug. This is particularly in case of antibiotic treatment. For example administration of Pyridoxine along with Isoniazid to prevent peripheral neuropathy seen with Isoniazid. Such fixed-dose combinations tend to 'offer better compliance but any combination genuinely shown to reduce the side effects (increased safety) while maintaining the efficacy must be judiciously considered as the treatment of choice. Unfortunately, many of such fixed-dose combinations have not been subjected to appropriate tests of therapeutic efficacy and safety, and, therefore, it cannot be assumed that all combinations are therapeutically rational and safe. a

What are the Combinations?

Disadvantages

of

sulphonamides content. In such individuals (sulpha-censitive) Trimethoprim alone can be used instead of depriving its use by prescribing Co-trimoxazole. Such problems may be more when 3 or more drugs are present in one preparation. It is always desired that increased benefit expected from a combination must out-weigh additional risks of adverse reactions 4. Physician's ignorance of the contents: In majority of the instances when the physician prescribes a fixed-dose combination he is less likely to know the exact ingredients of the combination. This is particularly so when the combination contains more than 3 drugs., If doctors are unaware of the ingredients of multiple drug combinations, there is hardly any justification in prescribing such preparations, say , which contain 3 or more drugs. There is also a tendency of trying multiple drug combinations where the diagnosis is doubtful or soppy.

Fixed-Dose

1. Fixed-dose ratio: While prescribing a fixed-dose preparation the physician loses flexibility in dosage. The effective dose of a drug varies among patients and in disease states in certain diseases such as insulin-dependent diabetes, mental depression, hepatic and renal failure the dose has to be tailor-made. Drugs like Rifampicin have to be administered on the basis of body weight. Though fixed-dose preparations are convenient dosage forms, they definitely encourage bad prescribing by doctors. The inflexibility of the ratio of combination of drugs is a definite disadvantage in most cases. 2. Irrational combination: Some combinations are simply irrational. For example, barbiturates or benzodiazepines plus analgesics, anti-inflammatory steroids. Some such combinations contained vitamins also. The British Committee on the Review of Medicines (1979) felt that almost all barbiturate combinations, even the ones with coronary dilators, should be withdrawn 0111 the grounds of doubtful efficacy and hazards of dependence. \VHO TRS 641 (1979) on 'Essential Drugs' has deleted multivitamin combination from its list suggesting that fixed-dose vitamin combinations should not be used indiscriminately. Vitamin needs should be worked out of a particular problem. 3. Increased toxicity: The use of two drugs will always increase the risks of idiosyncratic reactions. This can be avoided if individual drugs are used. For example some patients show hypersensitive reactions to Co-trimoxazole which could be due to

There are situations where fixed-dose combination may have clear cut advantage such as in hypertension. Similarly, some sound combinations may be available, as we have in Co-trimoxazole. But even in these situations if the prescribe is not aware of the ingredients it may lead to unwanted reactions. For example, prescribing a beta-blocker combination to a hypertensive patient who is asthmatic may precipitate asthma. Similarly Co-trimoxazole though scientifically sounds and widely used, opinions seem to be moving in favour of using Trimethoprim alone. A series of adverse reactions to Co-trimoxazole are appearing in 1iterature. Therefore, it is important to weigh the therapeutic advantages over potential toxicity of a preparation. In the developed countries the trend is already away from the fixeddose combinations. Some European drug regulatory agencies are thinking of restricting the number of available combinations. The recent decision of our Government to ban the manufacture of some fixed-dose formulations is a welcome step in this direction. Encouraging generic names against brand names may be another step towards discouraging branded combination preparations. But the most effective way would be through intelligent and selective prescribing. If doctors prescribed fewer drugs patients are less inclined to buy them. If doctors are selective and discriminating in their use of combination products the manufacturers might be more restrained.

** (Source: Drugs Bulletin, April 1983)

X M. F. C. ANNUAL MEET As announced in the October issue, the X M _ F. C. Annual Meet will take place in January end 1984, 27th to 29th January, 1984. Dr. Samir Choudhury, Director of Child In Need Institute (CINI) near Calcutta has kindly agreed to the convening of this meet at CINI. The first two days of the Annual Meet would be devoted to the discussion on the theme "Why alternative medical education is necessary?" and the third day will be reserved for the Annual General Body Meeting of MFC. Why discuss medical education? There has been a strong feeling among the members of Medico Friend Circle and like minded people that the exiting medical education is inappropriate. In fact one of the founding inspirations of MFC has been the realization of the irrelevance of the medical education to the needs of the rural poor. But it is only last year at Anand that we salt down together to systematically discussing the issue of medical education. In the mid-Annual Executive Committee Meeting at Kishore-Bharati in mid-July. 1983, it was decided that we should take up the above mentioned theme for discussion at the coming Annual Meet. In a sense, it would be a continuation of the discussion at Anand. But the emphasis would be quite different. The discussion at Anand focused on "Alternative Medical Education". That discussion was mainly (though not exclusively) aimed at the conference in Dhaka on alternative medical education for which some MFC member> were invited. This time the aim would be Ito build a strong case as to how the existing medical education is not appropriate. Though a criticism of the existing medical education presupposes some general notion of the alternative, those who were not present at the Anand-discussion would in no way feel handicapped at the coming CINI Meet. What shall we discuss?: During 'our prior discussion at Anand at Kishore-Bharati, we identified 'about 10 broad sub-topics which more or less cover medical education. These were- (1) Selection of students, (2) their social background, (3) Social background in which the student will work; (4) The basic role of the doctor: (5) Nature of training set-up; (6) Orientation of 'teachers; (7) Structure and content of course; (8) Methodology of training; (9) Method of evaluation: (10) Method of continuing education. All these -points cannot be properly discussed in a matter of two days during .the coming Annual Meet. At Anand and at Kishore-Bharati, we could discuss

first five points in some detail and could come to a broad consensus. (On other points, we could get time only to broadly delineate further sub-topics & issues). Based on this consensus, an article will be calculated well in advance, arguing how the existing system of medical education is at fault on these first five aspects of medical education. It is hoped that all the participants would broadly agree with this article. If there are any fundamental objections, these will be discussed at the beginning of the discussion at CINI. Otherwise we would directly go for the discussion on the remaining issues. The first five issues constitute the fundamental precondition of medical education. Thus if the medical education is to become relevant and useful for the cause of the people of India, the policy about these preconditions of medical education has to change fundamentally. In absence of this, mere changes in content of the curriculum will hardly be of any use. But it is not sufficient to discuss only the pre-conditions. Methodology of training, structure and content of curriculum is equally important. We have therefore decided to focus the discussion on these issues which have so far not been discussed in MFC in a systematic manner. How shall we discuss?: We will divide ourselves into three groups — The first group will discuss about what is wrong with the structure and content of the preclinical, clinical and paraclinical subjects as taught in today's medical education. The Second group will discuss about what is wrong with the structure and content of community medicine as taught in. today's medical education in India. This would involve discussion on (a) Preventive medicine (b) Sociology of Medicine. The Third group will discuss about what is wrong with the methodology of training in today's medical education. After these group discussions, there will be a plenary session wherein the discussion in each group will be reported and discussed upon one after another Background Material:

These discussions cannot be done properly unless we do some systematic home-work before the meeting. The following background material will be circulated before the meeting:A special joint January-February issue of MFC Bulletin would be published in the first week of January, 1984. It would contain: (i) An article reviewing the attempts made in different countries

to develop an alternative curriculum, (ii) An article reviewing the development of medical curriculum in India during last 150 years, (iii) A note shows the divergence between the hearth-needs of the people of India and the structure of the health delivery system in India. It would then broadly outline the type of doctors we need given the health needs of our people. These articles would create a general background for the discussions. Besides these articles in the Bulletin, following cyclostyled article will be sent to all participants:a) A Position Paper arguing how the existing medical education is a: fault as regards the fundamental prerequisites mentioned above. The discussion would proceed assuming this critique unless there are any fundamental objections from participants; b) A Position Paper on each of the three topics mentioned above for group discussions. These position papers will not be taken up for discussion as such but will be at the back of our mind when we start "the discussion; c) A Discussion Paper on each of these three topics for group discussion. It will be in the form of only a series of questions drawn up to delineate various sub issues that need to be discussed during the groupdiscussion. These questions will be taken up for discussion one by one. All these cyclostyled papers will be sent to

participants one month in advance \)0 that participants can do some more home-work-on their own. Those who want to send any written comments are welcome to do so. We will try our best to circulate such comments in advance to all participants. But there will be no "reading" of any papers during the Meet. Travel Arrangements and Registration:

As usual, participants will have to pay for their own travel. For return reservations, the hosts will have to be informed at the earliest. Those of you who want to come to the Meet arc requested to write to me immediately; if you have not done so earlier. Late-informers may not get Return-reservations. CINI is at the other end of Calcutta City, about 15 Kms, from the station, Return reservations will be made through an agency. Those who have not informed me so far may now directly write to the Director, CINI, (and inform me also) Village Daulatpur, P.O. Amgachi, Via Joka, 24 Parganas, West Bengal along with all details of train number, date ... .etc. and the fare and Rs. 15/- for the commission of the agency. I will send all the participants a detailed note about arrangements at CINI. As reported earlier, food will be subsidized; but the participants will have to pay something towards foodcharges. For details write to me. Anant Phadke,

Convenor, MFG.

CAMPAIGN AGAINST THE IRRATIONAL PRODUCTION AND MARKETING OF DRUGS This month marks the launching of a nationwide campaign against the irrational patterns of drug production in the country and unethical marketing strategies used by them to promote non-essential drugs, and against drug misinformation. Several consumer groups, voluntary organisations, people’s science

movement groups and pharmaceutical trade unions are participating in the campaign. The campaign has taken the form of a network of intensive action programme at the local levels which educates people and involve them in demanding a change in

the drug picture of the country; The Lok Vaidyan Maharashtra has published an appeal to 'the doctors In the October issue of the Pune Journal of Continuing Health Education. The letter lists nine common misuses of drugs because of misprescription on the part of the doctors. Quoting from standard textbooks it explains why these practices are irrational and appeals to the doctors to refrain from such practices, Dr. Zafrullah's Chowdhury's current visit to India gives the campaign an added fillip.

Editorial Committee: Anant Phadke Padma Prakash Ravi Narayan Shirish Datar Ulhas Jajoo

Editor Kamala Jayarao

Views and opinions expressed in the Bulletin are those of the authors and not necessarily of the organisation,


