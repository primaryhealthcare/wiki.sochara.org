---
title: "Dear Friend..."
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Dear Friend... from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC103.pdf](http://www.mfcindia.org/mfcpdfs/MFC103.pdf)*

Title: Dear Friend...

Authors: Nanavati Kartik

103 medico friend circle bulletin JULY 1984

RATIONAL DRUG POLICY A Drug Action Network Memorandum 3) WE, the health personnel and citizens of India recognize health as a fundamental right of the people in this, our welfare state. We recognize and strongly believe that the health status of our people is more depended on their access to adequate food. Safe and adequate water, proper sanitation and clean environment. WHILE we support the overall perspective and approach of the new National Health Policy Statement and demand its proper implementation, we believe that, a Rational Drug Policy is an integral part of a good National Health Policy. WE, following: 1)

2)

therefore,

demand

Further the national drug formulary should be revised and complied by an expert multidisciplinary committee keeping the followEssentiality Efficacy Safety Cost Ease of administration Availability Potential for misuse

Such evaluation of the drugs in the market and revision of the lists should be done periodically.

the

We have a right to safe, essential, quality drugs which are in keeping with the health needs of the people, at costs which the majority can afford. We urge our government to accept and implement the Hathi Committee Recommendations which are also in keeping with the WHO Guidelines for a Rational Drug Policy.

4)

The Essential Drugs Policy should be adopted for all health services, government and private, and priority in production, distribution and dispensing should be given to these essential drugs.

5)

The public sector should produce essential and life saving drugs on a priority basis at the national level.

6)

Drug production by multinationals and private manufactures in India should also be aligned with national health priorities.

7)

Bulk procurement of essential and needed drugs should be though world-wide competitive tenders and rationalization of drug purchases should govern both the public sector as well as private health sector.

8)

Imports and production of non essential, specially hazardous drugs, should be strictly curtailed.

9)

Drugs which have been banned from sale after being marketed for some time in one country may not be submitted for clinical trial or marketing in India. The onus of proving why a nonessential drug should be introduced or allowed to continue on the market should be with the manufacturer and such introduction should be preceded by adequate trials and evaluation by Drug Control Authorities.

10)

Comprehensive drug legislation which covers areas such as price control at different levels, patents and marketing

practices should be incorporated to serve the objectives to the national drug policy and there should be levies, sales tax or excise duty on any pharmaceutical product in the essential drugs list by the Central or State governments.

11)

No technology transfer agreement shall be legal and binding which contains restrictive practices, disproportionate and unnecessary use of imported intermediaries of obsolete technologies or unfair arrangements with respect to prices, payments or repatriation of profits.

12)

The National Drug Policy, should state clearly the steps towards a complete abolition of brand names and as a first step use of generic names should be made compulsory in medical education, prescribing and labeling of drugs. Generic names should appear more prominently on all packaging.

13)

It shall be the primary responsibility of the manufacturer to ensure the quality of drug products. However, it should be the statutory responsibility of the Drug

Control Authorities to monitor the standards and ensure a minimum uniform level of government control. Consequently, the government shall take all necessary measures to enable the Drug Control Authorities to function in an effective manner and discharge the statutory duties thrust upon them.

14)

It shall be the statutory duty of the drug control authorities to inform health personnel and consumers, of the essential drugs lists, policies, categories or brands of drugs banned for manufacture or sale, through publication in the national news-papers, magazines, and medical journals with adequate explanations and details.

15)

Availability of drugs required in the Government’s National Programmes should be ensured on a priority basis to the government as well as voluntary and private health institutions. Quotas for anti tuberculosis, ant- leprosy, anti malarial drugs, iodized salt etc., should be made easily available with regularity of supply to the voluntary health institutions where ever possible, specially when

their performance health care delivery is known to be effective.

16)

In all review committees, statutory bodies and other such bodies, there should be adequate representation of consumer groups and the voluntary health sector.

17)

Drug companies should follow ethical marketing practices, and this should be ensure by their own organizations like Organization of Pharmaceutical Producers of India (OPPI), India Drug Manufactures Association (IDMA), International Federation of Pharmaceutical Manufactures Association (IFPMA). We deplore the tendency of these companies and associations to get round every progressive measure of the government through recourse to technicalities of the law and through the courts.

18)

The marketing code drawn up by Health Action International (HAI) should form the basis for a National Code for Marketing Practices. This should be accepted by our government and should be suitably

implemented legislation.

though 

19)

The government of India should take a lead and endeavour to influence the WHO and the WHA to adopt the Code in the interests of the other developing countries and their people ----

     

 

Voluntary Health Association of India Centre for Social Medicine and community

Health, Jawaharlal Nehru University Kerala Sastra Sahitya Parishad Medico friend circle Arogya Dakshata Mandal Lok Vigyan Sanghatana Consumer Guidance Health Services Consumer Education Research Centre Federation of Medical Representatives Association of India.

MEDICAL CARE – A CRITIQUE AND BEYOND Satchidanandan, Calicut

A WORKER SPEAKS TO A DOCTOR We know what is it that makes us sick. They say you are to treat us when we are ill. They say you have learnt medicine for ten years in excellent institutions built on the sweat of the people And you have spent large sums to learn your art. Then you should be able to cure us, but can you? When we visit you, you take off our rags and probe our naked little bodies What is it that you are looking for? -- the cause of our illness? You would know better by looking at our rags. It is the same disease that consumes our bodies and our clothes.

You say our shoulders ache because of moisture, the same has stained the walls of our huts. Now tell us, where does this moisture come from? A little food and a lot of work make us pale and weak. Your prescription says: put on more weight It would be better to tell a blade of grass not to get wet in the rain. How much time will you give us? We see that a single carpet in your mansion costs the fees you get from five thousand patients. You will plead innocence, sure. The stain on the walls of our hut has the same tale to tell too. -- Bertolt Brecht

The worker in this poem is pointing to a serious disease that has come to affict the physicians in our country too: their absolute alienation from the common people whose toil have made them what they are. Careerism, callousness, corruption and greed are fast turning may of our doctors into a class of dubious integrity. But I refuse to consider the situation absolutely irredeemable. I do not mean to suggest that every doctor should transform himself into a Che Guevara or Norman Bethune, but there is no reason why young idealist, that may of you certainly are, cannot switch on a programme of housecleaning, a revolution of the scalpel and the stethoscope. What will be the nature of this medical revolution that that I wish you would set your minds to? What are its theoretical premises and its understanding of itself? What realistic goals and practical programmes can such a movement for socialist medicine have? I am presenting before you a very sketchy outline for a total critique of modern medical practice from a socialist perspective and the programmatic promotion of a counter-medical culture.

I. We shall begin with an examination of the basic tenets of the political-economic critique of modern medical and health care projects. The political-economic critics do not dispute the benefits of modern medicine; only they want the weaker sections of the society to have easier access to its unalloyed blessings. Poor or unequal geographic distribution of doctors and hospitals, lack of proper service in the rural areas, low technical quality of

service, discriminating treatment based on race, caste or sex, insufficient allocation of funds for medical care in government budgets, non-availability of necessary medicine, the general urban orientation of medical services, the general urban orientation of medical services, the exploitation of underdeveloped countries, especially of the third world by transnational corporations, chemical and technical experiments performed on colonial people by imperial masters, the unethical use of medicine in war and in the promotion of imperial interests – these are the recurring issues raised by these critics. The solutions to these problems are also political-economic: creation of an organized health care system, government sponsored mechanisms ot promote a more equitable distribution of and better quality in health care, greater centralization and careful bureaucratization. The more radical of these critics also find fault with all kinds of private ownership and control of medical and paramedical institutions. They demand complete control of even the proscription of the profit oriented private clinics and medical supply companies – in short a socialization of medicine on the models of Soviet Union or the countries of Eastern Europe. The political-economic critique, we should admit, is still not entirely irrelevant in the semi-colonial and underdeveloped situation obtaining in India. The enviable prestige that doctors enjoy in a generally illiterate country, their high status and money making capacity are enough temptation for any parent to wish to secure an admission for his child in a medical college even at the cost of honesty. The primitive power of a patriarchal society continue to drive even the disinclined to

the profession resulting in technical incompetence. The unwholesome alliance between pharmaceutical distributors and physicians and the callous export of useless or even harmful drugs by their global producers and corruption rampant at all levels of health services make the situation still worse. There is also a disproportionate emphasis placed on hospital oriented curative medicine compared to preventive medicine and environmental hygiene. It is wise to remember that the majority of the diseases that attack and kill our people are of an infectious nature like tuberculosis, diarrhea, dysentery, typhoid, leprosy and cholera or are mental disorders resulting in psychosomatic dysfunctions that spring from the tensions natural to our social milieu. But we have modeled our medical development on western lines concentration on non infectious diseases like diabetes, cancer, hypertension and cardiovascular diseases, more prevalent in the developed countries. Most of the communicable diseases are bred and nourished in India by condition of underdevelopment and can be considerably controlled by improvement and can be considerably controlled by improvements in the living standards of the poor. Better food, facilities for better housing and clothing, regular supply of disinfected water, more leisure and facilities for recreational activities, cleaner surroundings and a peaceful environment alone will put an end to the contagion that make our hospitals overcrowded hells. The struggle for the prevention of diseases has thus become an increasingly political question related to the removal of exploitation and the establishment of a genuinely socialist society. Modern statistical of genuinely

socialist society. Modern statistical studies by Rene Dubos, Thomas McKeown, John Prowles and A. L. Cochrane have proved beyond and doubt that the fall in the death rates and the decline in contagious diseases recently observed in the developed countries have little to do with curative medicine. They are products first of better nutrition and housing facilities, second of improvements in control of the environment and only third of personal medical attention. I know that this has been formally recognized by our national health programmes and that social and preventive medicine has been formally included in our medical syllabi. But the question is how far does this awareness inform the real activities of our doctors and health forums. What is their cumulative contribution to the study and dissemination of epidemiology, medical sociology and environmental education? I think this is precisely a field where informal and non-bureaucratic medical movements can make great strides. II. The socio-cultural critique of medical practice is a more recent development. Ivan Illich, the Viennese thinker who had developed a series of institutional alternatives for technological societies was perhaps the first to develop a consistent and radical cultural evaluation of western medical care. Besides Illich, feminists like Linda Gordon and Pauline Bart, black radicals like Frantz Panon and medical sociologist like Irvin Zola and John Ehrenreich have helped to create a profounder understanding f this cultural crisis. Illich opens his book, ‘Limits to Medicine’ with the words “The medical establishment has become a major threat

to health. The disabling impact of professional control over medicine has reached the proportions of an epidemic”. He calls this epidemic, ‘Iatrogenesis’ meaning a disease born of physicians. Illich holds that it is the laymen and not the physician who has the potential perspective and effective power to stop this epidemic (though I see no reason why earnest and intelligent physicians cannot join hands with the laymen in an attempt to demystify medicine). The increasing ‘medicalization of life’, Illich argues, is a form of the colonization of the body. By holding the exclusive right to determine what constitutes health and sickness and what shall be done to them, medicine has become a major means of social control. The unlimited growth of medical care also threatens to destroy the environmental and cultural conditions needed by people to live a life of autonomous healing. Medical technology has been helping industrial growth rather than personal growth. He admits that chemotherapy has played a significant role in the control of pneumonia, yaws, malaria, tetanus, diphtheria, scarlet fever and sexually transmitted disease but it has contributed little, he argues to the decline of mortality or morbidity form these diseases. Even further the pain, dysfunction, disability and anguish resulting from technical medical intervention have contributed to the increasing morbidity of modern life. The unwanted side-effects of medicines have also increased with their power. May drugs are addictive, mutilation or mutagenic; antibiotics can at times alter the normal bacterial flora and induce a superinfection; other drugs help breed drug-resistant strains of bacteria. The overuse of dangerous diagnostic procedures, the administration of synthetic hormones, chemical

stimulation of labour, ultrasonic fetal monitoring, the routine use of anesthesia for delivery, overdose of powerful drugs like chloramphenicol, using drugs in dangerous combinations, medical treatment of non-existent diseases and unnecessary surgery have a disabling effect on their victims. Professional callousness and negligence are increasingly being attributed to a break down or absence of equipment; thus moral faults come to be justified as technical errors. This aspect of the problem Illich calls ‘clinical iatrogenesis’. Medical practice also sponsors sickness by encouraging people to consume curative, preventive, industrial or even environmental medicine. Defectives are prevented from work and promptly removed from the scene of political struggle to reshape the society that has made them sick. This invalidating process is what Illich calls ‘social iatrogenesis’. Health professions also destroy people’s capacity to deal with their human weakness in a personal and autonomous way. Birth and death are equally controlled and culturally mediated by medicine. Death is turned into a profitable commodity with endless potential. Health management is designed on the engineering model. This has been termed ‘cultural iatrogenesis’. I would like to add two more modes of iatrogenesis; one is philosophical. Modern western medicine suffers from an overdose of scientism, and extreme form of mechanical materialism. First, it follows the doctrine of specific etiology, where the existence of cause is mechanically connected to the disease. Second it conceives the human body as a machine, the functioning of whose parts in

considered independent of the mind of the organize. It does not take into account the interactions of body, mind, and physical and social environment. Thus there is a disassociation of mind and body in this medical technologism. Both theses can be traced to the capitalistic formal logic that governs medical science. Only a dialectical approach can make medicine a genuine science. The medical technology itself is capitalistic; that is why a mere socialization of medicine cannot make it socialist. The other mode is moral iatrogenesis. Professionalism in medicine has unfortunately come to mean a defense of occupational and class privilege rather than high standards. The profession thus has created a highly specialized language meant to mystify the laymen. The cruelest examples of moral iatrogenesis may be found in psychiatry which openly practices the social control of deviant behavior. It is concerned not with clinically measurable somatic dysfunctions but with what is socially defined as abnormal or unacceptable behaviour. R.D. Laing’s classic example may prove my point. A man gibbering away on his knees, talking to some one who is not there should normally be considered mad; but society has come to define this activity as ‘prayer’ so that we consider it perfectly sane. Psychiatrists have the power to label several states ranging from rare creativity to revolutionary activity as forms of insanity. They help the preservation of the statusquo hope here lies in the development of antipsychiatric movements trying to discover the social roots of abnormal states and discouraging monstrous methods of treatment such as ECT, replacing them with love, understanding and patient persuasion.

III. The political-economic critique and the socio-cultural critique of modern medicine are not as contradictory as they may appear to be. If we can unleash the imagination of the people and lay the foundations of a popular movement it must be possible to bring about a synthesis of the perspectives of ‘more’ and ‘different’. What is required in the Indian situation of scarcity and corruption is an equal emphasis on the need for more services altogether. The seed of such an ambivalent popular movement for socialist medical care can be sown now, in the form of a forum of radical medical student, teachers, practicing doctors, psychiatrists, nonprofessional health workers and active sympathizers. The primary task of this forum will be constituted by campaigns of demystification and conscientization. The aim of these campaigns will be the demedicalization of society by spreading the concepts of self-help. But we should take care to see that it does not promote superficial fads. Some medical technology is useful but inappropriate for use by untrained people. Rejecting this would be a self-destructive form of ‘autonomy’. What is required is more a reorientation of dependency. , Rather than a complete abandonment of dependency. Consicentization can be done through the publication of books, pamphlets and periodicals and by camps and classes meant for weaker sections based on the dialogical and problemposing modes of socialist pedagogy as outlined by Paulo Freire. The whole campaign should enable the people to ask questions to the doctors on terms of equality and judge the kind of care and treatment offered to them secondly the new movement can see the enforcement of the oft-broken medical code of

conduct. This should be realized as far as possible, by persuasion, moral authority and honest example rather than direct coercion. Thirdly, the forum can conduct or guide revealing studies in the geography, history, sociology and politics of medical care in India, particularly in the States. These studies can expose the various forums of institutionalized corruption and expose the sexist, casteist and class prejudices now inhibiting medical study and practice and the effects of colonialism on our attitudes to illness and cure, thus evolving a total critique of medical practice in the country. Fourthly it can propose alternatives to the present medical syllabi so that they may place greater emphasis on a realistic study of our environment and study of the doctorpatient relationship. Healing relationships are as much social as they are physiological; so chemistry, biology and physics alone cannot form an adequate basis for scientific medicine. it should find place for subjectivity and consciousness in the study of man so that it becomes a dialectical science. Traditional medical systems like tribal cures, indigenous and holistic systems like Ayurveda and non-allopathic system like nature cure, homeopathy, Unani medicine and acupuncture should form a part of the syllabi. The forum can also encourage researches in mixed medicine that integrates the various approaches. Fifthly it can assist the deprofessionalisation of medicine, by teaching the patients to conduct their own laboratory test and offering compressed courses in environmental hygiene and preventive medicine to volunteering youth as is done in the case of the ‘bare foot’ doctors in China. (Books like ‘Where there is not doctor by David Werner can be of use in such courses). It should simultaneously

‘reprofessionalise’ medical care by invoking the idea of health care as a calling and a selfless mission. Sixthly and lastly, it can also build up a parallels system of clinics and nursing homes where the foundation is laid for a new type of doctor-patient relationship and a novel approach to the problems of medicine including psychiatry. Courtesy: Calicut Magazine 1982-83

Medical

Collage

Health for all-depends on three things: Reduction poverty and inequality and spreading education; organizing the poor and underprivileged to fight for their basic rights; replacing counterproductive consumerist western model of health care by alternative model based in the community. ---- ICMR/ICSSR

DEAR FRIEND....... Tuberculosis – Annual meet theme; 1985

1) What is the purpose of this exercise? -- Is the purpose to do a critical analysis alone and leave it at that? OR -- Discuss the various dimensions of TB problem for our own understanding and for education of others in MFC. OR -- Discuss the TB issue from the point of view of the workers in the field of health with the purpose of ensuring some improvement i.e., action plans

being an important aspect of our work -- This would obviously be based on the above two but would mean our going beyond that. In CINI this issue has come up, as to the changing role of mfc and I strongly feel, unless we are to some extent involved in coordinated action over selected priority issues, we will stay this big and have very marginal impact. I feel extremely strongly about the TB issue and I’d hade to see the whole thing limited to an intellectual exercise, no matter how fantastic. Mira Shiva VHAI, New Delhi

2) Actual discussions should focus of the following issues: a) Where does TB fall in our priorities? b) What priority in exact terms has been accorded to it in our national, state level, hospital level and day to day practice as well as in medical education? c) Assessment, remarks, criticism in the current direction of approach to the problem of TB. d) What could be the rational approach at each of the above mentioned levels?

We should avoid discussing details of chemotherapy etc., if we are to encourage participation of non-medical people in mfc. Current concepts and WHO recommendations about diagnosis and chemotherapy and the evolution of the care of TB at our national level may be circulated through background papers and the bulletin. KARTIK NANAVATI Ahmebadad

Self-sufficiency in health care 3) The main article in June’s issue “Towards an Appropriate strategy’ was very interesting for us as this is the sort of work we are starting to try to do. I wish it had been more detailed, in fact. Please keep up the immensely good work that mfc does. It is one bright patch in an otherwise very gloomy picture. Keith & Caroline Walker Gangavarpatti, T.N.

DRUG ALERT! DRUGS FOR ARTHRITIS IN THE DOCK On 17th May 1984, local newspaper announced that two popular drugs used for arthritis (Tanderil and

Tendacot) – both oxyphenbutazone derivatives – were ordered to be immediately withdrawn, from the market

in UK by a government order1. The action was taken on the recommendations of the Committee of Safety of Medicine (CSM). Though the manufacturer Ciba Geigy had exercised its right of appeal under the Medicines. Act to stall the government’s decision, which actually had been taken sometime ago, the Medicines commission had upheld the decision to revoke the license. 400 deaths are reported to have taken place in Britain in the last two years due to these drugs2. The committee found them twice as dangerous as three other drugs belonging to the phenylbutazone group (Butazone, Butacodine and Butacote) which were withdrawn in March this year. The CSM had continued to receive reports of adverse reactions including fatal ones due to blood disorders, gastro-intestinal intolerance and bleeding3. Sidney Wolfe, Director of the Health Research group (sponsored by Ralph Nader) has estimated that world wide probably more than 10,000 patients had died as a result of taking these drugs. In his letter to the Department of Health and Human Services, he gave anaemia, agranulocytosis, leukemia, gastrointestinal bleeding and peptic ulcerations as the leading causes of drug induced deaths. Other deaths were also attributed to hepatitis, thrombocytopenia and renal failure4. Interestingly in the last two years, three other non-steroidal antiinflammatory drugs benoxaprofen, indoprofen and zomepirac and a formulation of indomethacin (osmosin) were also withdrawn. A review of a current CIMS5 shows 20 formulations of oxyphenbutazone (Algesin-0,

Aristopyrin cream, Butacortindon, Butadex, Butaproxyvon, Disiflam, Flamarp., Ganrilon, Inflavan, Kilpane, Maxigesic, Oxalgin, Oxyrin, Oxytriactin, Reducin-A, Reparil, Rumatin, Suganril, Tendon, Tromagesic) and 8 formulations of phenylbutazone ( Actimol, Algesin, Aristopyrin, Butapred, Ebeflam, Parazolandin, Zolandin, Zolandin-Alka) recommended for use by doctors in India. How many patients must die before something is done about this in India as well? An mfc annual meet background paper in 1982 concluded that the ideal anti-inflammatory drug was yet to be discovered and Aspirin Remained the agent of choice when cost-factor and benefit to risk consideration were taken into account6. Have events in UK endorsed this? With such a large number of antiinflammatory drugs in the docks, will homeopathy7, ayurveda and non-drug therapies have a role to play in the treatment of arthritis? -- Community Health Cell, Bangalore

WARDHA MEETING The mid-annual EC/Core group meeting of mfc will be held at Wardha from 27-29th July 1984 at Gauri Bhavan, Sevagram Ashram, Sevagram (Maharashtra). At this meeting discussions will be held on organizational issues and plans for the annual meet on ‘TB problem and control’.

mic bulletin: JULY 1984

EDITORIAL The ICMR/ICSSR report on ‘Health for All’ has warned that “eternal vigilance is required to ensure that the health care system does not get medicalised, that the doctor-drug producer axis does not exploit the people and that the abundance of drugs does not become a vested interest in ill-health1”. The Drug Action Network which has come together in the last two years in symbolic of this vigilance, which is growing in India. The memorandum drawn up by the participating organizations, which is featured in this issue highlights the diverse aspects of drug policy towards which this vigilance has to be directed. THE banning of wide range of commonly used drugs for arthritis in U.K., in recent weeks (article on Drugs alerts) raises questions about the complexities of this vigilance. In countries like U.K. and U.S.A. in spite of drug safety committees, comprehensive drug laws, efficient drug control authorities, active consumer groups and socially sensitive elements in the profession – drugs continue to slip through and get used for years before their dangers get known and bans are instituted2. How much more difficult will it be in our country where all these elements of ‘vigilance’ are still only in the process of evolving? William Osler’s exhortation that one of the first duties of the physician is to educate the massed not to take medicine3 is particularly relevant in today’s drug situation. The role of doctors in acting as watchdogs is primary ---- laws, controls and

authorities notwithstanding. Are doctors prepared adequately for this role in India? Medical education stresses the minutiae in pharmacology and medicine without stressing the factors of cost, safety and social relevance. It also does not consciously immunize the doctors against the half-truths of persuasive medical advertising4. In the absence of programmes of continuing education in the country, practicing doctors continue to be informed only by the profit oriented pharmaceutical industry, thus worsening the situation. UNLESS there is a growing realization among medical students, young doctors, teachers, health workers, professional associations, consumer education groups and science movements that this problem movement very little change can be expected in the present situation. Satchidanandan’s critique presents an analytical framework and background against which such a movement would have to evolve. His suggestions for a multidimensional campaign of demystification, conscientization, study, curriculum change and deprofessionalization could well be initiated taking drug issue as the focal point. It would, however, be important to keep in mind that over seventy five percent of the people in India have little or no access to health care. Hence an action programme only on drug matters would be cut off from the needs and aspirations of the majouity5. However, if this became part of a wider people’s movement for socio-political change, the drugs problem would be tackled at its very roots.

Reference 1.

HEALTH FOR ALL – AN ALTERNATIVE STRATEGY: ICMR REPORT, 1984.

2.

NON-STEROIDAL

NATI-INFLAMMATORY

DRUGS:

Lancet

Editorial, 21st January, 1984. 3.

FEED BACK ON PRESCRIBING: Lance Editorial, 11th February 1984.

4.

WHAT IS RATIONAL DRUG THERAPY?: Health for the Millions, April-June 1981.

5.

CONSUMER ALERT—CONSUMER ACTION: Bulletin of Science, Vol.1, No.2, December 1983.

Editorial Committee: Kamal Jayaroa Anant Phadke Padma Prakash Ulhas Jaju Dhruv Mankad Editor: Ravi Narayan View and opinions expressed in the bulletin are those of the authors and not necessarily of the organization. Annual subscription – Inland Rs. 15.00 Foreign: Sea Mail – US$ 4 for all countries Air Mail: Asia – US $ 6; Africa & Europe – US $ 9; Canada & USA -- $ 11 Edited by Ravi Narayan, 326, 5th Main, 1st Block, Koramangala, Bangalore-650034 Printed by Thelma Narayan at Pauline Printing Press, 44, Ulsoor Road, Bangalore-560042 Published by Thelma Narayan for Medico Friend Circle, 326, 5th Main, 1st Block, Koramangala, Bangalore-650034


