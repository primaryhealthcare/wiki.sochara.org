---
title: "The politics of food: keeping the other half hungry"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled The politics of food: keeping the other half hungry from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC290-291.pdf](http://www.mfcindia.org/mfcpdfs/MFC290-291.pdf)*

Title: The politics of food: keeping the other half hungry

Authors: Devinder Sharma

290-291 medico friend circle bulletin Nov-Dec 2001

Body Weights — Role of Nutrition Veena Shatrugna1

The scope of this paper (Body Weights Role of Nutrition) ranges from the very simple understanding of the role of food adequacy and its’ impact on body weights, to the complex that weights and heights of populations may be used as a proxy for development, and equity. But one may ask, for surely eating well has always been associated with good health in India. What is new about this? ‘Have you eaten? And why are you looking so thin?’ are perhaps the most frequently asked questions form loved ones and those who are sick or recuperating form an illness. Love and affection are usually expressed through food, and unhappiness through self-denial (refusing to eat when you are angry). Surely common sense about the centrality of food cannot be so far removed from nutritional science? Despite this the questions about food still baffle most of us and always unsure we grapple with the scientific basis our dispersed knowledge. This is evidenced by the extreme interest in popular discussions on food in newspaper columns, and a fascination with nature cure regimes, water therapies and foodbased cures. The middle classes also worry about the questions of safety, quality and quantity of foods eaten (obesity). Slimming centers have mushroomed as professionals have taken over and strict regimes of dieting appear to help some shed excess body fat. In short, loosing weight requires your to either eat less or burn up more energy (though work or exercise), or preferable a combination of both. This new knowledge of the “scientific” middle classes that decreased food intake leads to loss of weight is rarely extended to understanding the plight of the poor visible everywhere. It is assumed that they (servants, coolies, rickshaw pullers, construction workers, head loaders etc.) are thin and emaciated because that is “normal” for them. Rarely does someone reflect on the possibility that they are thin because they do not get enough to eat, or they overwork and therefore their intakes do not get enough to eat, or they overwork and therefore their intakes do not meet their requirement. Instead the middle class are surprised when they come in contact with someone from the low socio economic group who looks healthy or obese. It is a universal truth (for the rich and the poor) that when the eat well, they have ‘normal’ weights, and may even become obese, and when children eat enough during their growth phase they not only weigh just right for their ages, they are active, stay healthy, and grow as tall as the children form the well off groups. Obviously the standards of ‘Normal’ weights and heights are not different for the different for the poor and the well off Indians. Infact, the standards of weight and height used for comparison in India are the American NCHS standards (National center for Health Statistics), because surveys of Indian children form the upper socio economic classes (and well fed kids form the low socio economic group) show that our children grow as well as the children from the western world till the ages of 12 – 16 yrs, suggesting that our kids have a potential to grow tall and reach the desired weight and height. (Gopalan et al (1989) and Vijayraghavan et al (1971) in the case of adults apart from the NCHS standard for comparison an index called the BMI (Body mass index) may be used which measures weight for height. Because adults can only lose or put on weight when their food intakes change, their heights do not change with malnutrition or obesity. Shorter heights represent malnutrition during the growth phase (adolescent and childhood period.) It is interesting to remind ourselves that it was suggested in the 60s and the 70s that because most of us were shorter and lighter than the westerners, the normal standards of weights and heights for Indians in this country must be lower. Weights This is the most important indicator of the nutritional status of an individual and also of populations. It is also the simplest reproducible measurement for the evaluation of the nutritional status of adults and young 1

Veena Shatrugna is Deputy Director, National Institute of Nutrition, Hyderabad. She is also associated with Anveshi Resource Centre.

children. In the underdeveloped world when large-scale surveys of weights and heights of populations show improvements over time, it may be presumed to indicated the national’s commitment to growth with equity. A 20 yrs trend based on a large number of studies and the NNMB (National Nutrition Monitoring Bureau) surveys of Indians (1977 - 1996) show that there have been minimal improvements in the weights of populations (of the same age of course) in India (Table-1). The mean weights of children at 5 yrs of age in 1977 were 13.7 kg and 14.1 kg (girls and boys) when compared to the NCHS median weights of 17.7 and 18.7 kg. This deficit of about 4kg compared to the NCHS standard of 56.6 and 68.9 kg. There was a small improvement in the weight of Indians as they reached the age of 25, (42.8 and 49.9 kg.) but it was still way below their desirable weight. At the age of 60+ they slipped back to mean weights of 39.7 and 47.6 kg. By the year 1996 the nutritional status of a large number of people had not changed, or perhaps had improved only marginally (an improvement of 1.5 to 2.5 kg. at each age group) It is disturbing to find that an ICMR study of 1955-65 and a survey carried out in Tamilnadu in 1954 (Table 1 and 2) provide similar figures of low weights of girls and adult women from the low socio economic group. Though the adolescents appear to have massive weight deficits in the 50s, the final weights of adult women are not different in the 50 yrs between the 2 surveys, (42 kg in 1955, and 42.k kg in 1996). It is disturbing that adult women and men have been surviving with mean weights of around 43 kg and 50 kg respectively since the last 50 years, far below the desirable weights of 56 and 69 kg. To make things worse it is important to remember that because these are mean weights, it is to be expected that approximately half the population in India have even lower weights than these, (weights as low as 38 kg) a condition very close to chronic energy deficiency or starvation. It may be stated that one of the important reasons for weight deficits is the shorter heights of Indian, but as the data for BMI reveals (see below) that, despite stunning, the weights of the majority of Indians is a cause for concern… Most of the data presented in large surveys represents the status of the poor. Interestingly when this data was disaggregated according to class (Table 3) it was clear that children from the higher income groups (HIG) had better weights when compared to children form the poor socio economic group (11.9 and 11.4 kg, in the HIG vs 9 and 9.5 kg in the slums at the age of +2 yrs, girls and boys). Children from the low socio economic group and specially those living in the slums and the rural areas were the worst off. Studies carried out by Dr. Gopalan et al (1989) on women from the affluent groups in Delhi show that the mean weights at the age of 18 yrs was 52.5 kg, almost 10 kg more than the national mean weight for women (42.5 kg). This trend of better weights in the well off women was also reported from Mumbai, Calcutta and Coimbatore. Other studies from different parts of the country confirm the fact that weights of the well off sections of society are as good as the NCHS median weights. Now that does this mean? It is well known that when the poor are forced to survive on inadequate food I results in very low body weights, (mean weight of 43 kg in the case of women and 50 kg in the case of men over the last 50 yrs), when compared to the 20% middle classes who have caught up with the NCHS weights. The reasons for this are obvious. For the large majority it is a question of poor incomes and/or overwork. Some of the other reasons for this state of affairs may be illnesses, but inadequate food intake is by far the most important. With better food intakes the poor are capable of putting on weight, but till such time for years in near famine like conditions. They make do with a smaller frames and the minimum weight necessary for survival, because they must continue to work. Weights and Famine All the great famine studies show that there is a loss of 25 to 30% of body weight during the period of the famine before it results in death. This weight loss is obvious and even visible because people start with normal weights. A weight deficit of 10 to 15 kg in our adults also amounts to a 25% inadequacy and may be said to explain the famine like condition. However the low weights seen in poor Indians is not a loss but it is so because they never reach their desirable weight. It also means that they do eat a little, but use all their energy from food for working, and nothing else. When there is a further crises (an official famine) they might lose another kilo or two, but the most obvious thing is that they start to cut down unnecessary movements, their walking is slower, and most of the time they lie down, just keeping body and soul together (even Medha Patkars and others on a hunger strike are found sprawled out on mats by the 3rd• day of the strike) With continued famine there is no further visible deterioration of their physical condition, the skin and bones appearance was always there, but mental changes are said to occur. Medical doctors have never seriously learnt Nutrition, and therefore they might

even treat the starving people with glucose drips and B-complex injections, without taking a detailed history of foods eaten, or foods available or prevalence of starvation. An OP chit in a famine area, will rarely have information on foods eaten, or the body weights of those who are starving, because doctors are used to the emaciated looks, in the rural areas of course. Death after that is not dramatic. (Famine studies were done on "Normal" healthy people, who had rapid weight losses, and therefore dramatic changes in weights were recorded ... but famine in a semi-starved population evolves differently, and to make things worse there is no information on this in the text-books.

Malnutrition- Children Children's weights are sensitive to very minute changes in food intake or illnesses like diarrhea and fevers etc. A large proportion of the IMR (Infant Mortality Rate) in this country is due to malnutrition. Therefore childhood weight deficits in a country represent not just food inadequacy but also an indifferent or unaffordable health care system in relation to the children's health needs. When these weight deficits in children and populations persist over decades it represents a state of underdevelopment and a disinterest in the questions of equity in that country. The nutritional status of children is usually assessed by using weights, height and mid arm circumference. The Gomez classification, which is the most commonly used procedures, is based on weight for age (Table 4). The child's weight is expressed as a percentage of the NCHS median weight classified into grades of malnutrition. The results of the NNMB surveys of 1977 and 1996 (Table 5) show that there is little improvement in the nutritional status of children over the 20-year period. Only about 10% of children can be classified as normal, and there are no differences between the boys and girls. It means that only 10% of children have a weight, which is more than 90% of the NCHS median weight, and can be called NORMAL. Over 90% of children had low weights for age and 45-50% of the children were classified into the moderate to severe malnutrition category. The 1984 NNMB survey data from the urban areas was disaggregated to study the role of incomes in the percentage prevalence of malnutrition in children (Table 6). In the HIG and MIG (High and Middle Income) groups 85-90% of the children were either normal or had only mild malnutrition, and only 0.5% of children had severe malnutrition. However in the case of LIG, IL, the slum and the rural population the percentage of normal children were only between 12-20% and 40-47% could be classified into mild malnutrition. A very large number of children (31-47%) could be classified into the moderates to severe malnutrition category when compared to 1015% in the well off. The reason for such a high prevalence of malnutrition in the poor income households is obvious, shortage of foods appropriate for the child, mother's who have to work 10 to 12 hours without any holidays, inability to hire caretakers because of low incomes, and absence of other kinds of support may be some of the important reasons for malnutrition .. The slum population continues to be at a disadvantage despite their nearness to the health services (both the Government and the private sector) and other employment opportunities in the urban areas. These are not advantages, which the poor can use, because they are not supportive to mothers of young children, bit with adequate purchasing power, the middle and upper classes can use these services… The above findings assume importance for women because malnourished children are known to fall ill easily, and repeated children's illnesses result in increased work and stress for women. This is compounded by the fact that in these poor households, childcare services are unsure and the curative health care system is not structured to respond to the child's need of nutrition, care and stimulation. Very often the repeated infections are either unattended or the treatment by the ill equipped private sector provides symptomatic treatment resulting in infant and under five morbidity. In the absence of money and a support system the consequences of child malnutrition are serious for women, who may have to opt out of paid work, to take care of the sick child, or resume home-based work. Heights Healthy children, who have no economic constraints, and abundant food availability, increase their weights and heights with age. When children do not get enough to eat, they stop growing tall and if the food deficits persist they may even lose weight. Therefore heights and weights are useful measurements for the understanding of childhood malnutrition (also has public health significance).

However after the age of 16 or 18 years weights are a better indicator of current malnutrition, because heights represents past malnutrition (during the period of childhood and adolescence). Surveys of adult height measurements of populations may be used to study improvements of nutritional status over generations. In fact heights have archeological value. (When the mean adult height of the younger generation is better than the height of the older age group, it signifies better socio economic and health conditions during the growth phase of the new generation, compared to the conditions present earlier. It thus suggests an improvement in the socioeconomic conditions over time, also called a secular increase in height). A 20 year profile (1977 and 1996 NNMB data) of heights of population based on the NNMB data shows that stunting continues to be a problem among boys and girls, and there is no secular increase in heights in 20 years, or a minimal increase of 1 cm. At the age of 5 years (in 1977 and 1996) there is a height deficit of about 8-9cm, both in the case of girls and boys when compared to the NCHS standards. This deficit appears to deteriorate at the age of 20 years (Table 7) when the height deficits range between 12 and l3cms in the case of women and men when compared to the NCHS standards, (mean adult height, 151 cms. and 163cms women and men in the NNMB surveys compared to 163.7 and 176.8cms in the NCHS standards) The only significant finding is that girls and boys have an earlier growth spurt, i.e. 15 years for girls and 16-17 years for boys in 1996, which is 1-3 years earlier when compared to the 1977 findings, though there is no difference in the final height in the 20-year period. But women had a loss of height of about 2cms due to aging and possibly osteoporosis by the age of 50 years in the 1977 survey whereas it was 5 years later in the 1996 data. These changes were minimal in adult men (Table 7). Height-Secular changes: Increase in adult heights of the younger generation called secular increase is an index of the long-term improved nutritional status of the population during the period of growth. Studies on the heights of parents and adult children from the rural areas around Hyderabad shows an absence of a secular change in the heights of younger generation of women from the low socioeconomic class when compared to their mothers (Table-8). When the data for the rural areas was disaggregated according to castes, the absolute heights of the adult women from the scheduled castes was only 147 cms when compared the adult heights of the Backward Castes (BC) and the Forward Castes (FC) and there were no secular increase in the heights over the generation in Sc. In the case of the GBC also there was no secular increase in the heights between the mothers and adult daughters, however, their adult heights were between the values of the FC and SC. Interestingly the adult heights of the Fe was greater (mother 150.6cms. and daughter l52.6cms ) and there was a secular increase in the heights of the new generation (Table-8). Studies on the urban poor population residing in the slums confirm the absence of secular increase in heights of the younger generation. However there was an increase in the heights of the younger men and women by as much as 4cm in urban HIG groups (Table 8). These studies confirm the results of the large scale NNMB surveys and highlight the nutritional disadvantages of the rural SC and BC and the urban poor. It is obvious by now that an increase in adult heights of the younger generation when compared to the heights of the parent may be used as a proxy for long-term improvement in the nutritional status of the population, and perhaps even of development of the region or country. It represents better access to food, safe water, and health care during the growth phase of the younger generation in contrast to the disadvantages suffered by the parent. The western world and also Japan and China have recorded a secular increase in heights, and in the west adult heights reached a plateau in the 1940s itself. Large scale Indian data on heights of the poor people show no such improvement in heights or a small increase of 1 to 2 cm. (Age difference between the parent and son or daughter was 18 to 35 years). If the conditions had been beneficial to the poor, height difference of 4 to 6cms should have been recorded by these surveys (Table- 7). However using better heights as an indicator of access to incomes, better nutrition, health care, and other benefits of development, it maybe said that possibly the middle and upper classes and the upper castes have benefited from 50 years of development.

Body Mass Index (BMI) After an adult has reached the final height, weight for height is a good indicator of muscle and fat stores in the body, in fact Body Mass Index is considered a reliable index of chronic energy deficiency or chronic hunger in the adults. BMI is a ratio of Weight (in kg)/Height 2(m) and denotes fatness. For example a 45Kg adult would have a normal BMI of 18.5 if her/his height were 156cms, but would be classified as severe energy deficient (BMI 16.1) if the height was 167cms.It is important to use this index for adults and not children. Table9 gives the cut off levels for classifying adults into different grades of Energy deficiency, (which really means Food deficiency). It is known that low BMIs are related to increase in mortality, increase in morbidity, and risk of low birth weight. Of course high BMIs of more than 28 are also associated with increased death rates in .those who have a heart conditions or some chronic diseases, Based on a large number of NNMB surveys and studies, the distribution of men and women according to their BMIs is given in Table-l 0, Over 50% of the population in India had BMIs less than 18,5 in 1979, and this has improved only marginally over the 20year period, The number of obese is only 2-3%, However Table II provides a grim picture of starvation especially of the SC and ST groups. (About 61 % of ST and 53% of SCs have BMIs less than 18.5, compared to an equally disturbing figure of 47% for the others in the rural areas)

The number of people classified as normal (BMI >18.5) are also fewer in the SC and ST when compared to the others. The same reports show that land holdings appear to protect the rural population from chronic energy deficiency, as it possibly assures them food security. Based on the WHO data base of healthy adults (WHO 1995) only about 3-5% of a healthy adult population should have a BMI below 18.5; the Expert Committee suggested the following classification of the public health problem of low BMI, based on BMI distribution in adult population worldwide. Very high prevalence (critical situation): = 40% of population with BMK 18.5 Low prevalence (warning sign, monitoring required): 5-9% of population with BMK 18.5 Medium prevalence (poor situation): 10-19% of population with BMK 18.5 High prevalence (serious situation): 20-39% of population with BMK 18.5

This classification is somewhat arbitrary, but reflects the distribution of BMI in many populations of developing countries and endeavors to take into consideration the societal consequences of the functional impairments commonly associated with low BML The figures from India speak for themselves, it is a near famine like condition already, and there is no time to experiment with food distribution. However urbanization appears to be beneficial for women with a smaller percent of women with chronic energy deficit (BMkI8.5 - 36.7%) and with a slight increase in the percentage in the normal BMI category (6-7%) when compared to the rural areas. In fact 11.6% of urban women can be called obese when compared to 5.5% of men. It is possible that women don't eat better in the cities but that they work a little less, they conserve energy in the urban areas which is expended in rural areas in search of food, fuel, fodder and water, resulting in better body weights. Going by weights, heights or BMI of Indians, it is obvious that about half of Indians have food inadequacy or 'go' hungry' every day. Looking at this grim picture it is important to remember that the conditions must be worse in areas with a high prevalence of goiter (iodine deficiency) anemia (in 60-80%), Fluorosis, malaria, TB, and other preventable diseases. Pregnancy would make things worse. Now where can we go from here? References: 1. Arvind Wadhwa, Manisha Sabharwal and Sushma Sharma "Nutritional Status of the Elderly, Ind. J. Med. Res. 106, I 997, pp. 340- 348. 2. Gopalan .C and others - Growth of affluent Indian girls during adolescence, NFI Scientific Report No. 10, New Delhi 1989.

3. Kapoor, S, Kapoor, AK, Bhalla, R, and Singh, I.P. "Parent off-spring correlation for body measurements and subcutaneous fat distribution". Human Biology, 1985, 57,141-150. 4. Nadamuni Naidu. N and Pralhad Rao. N, "Body mind” Index: A Measure of nutritional status in India population", Eur. J. Clin. Nutr. 1994, 48, Suppl.3, S 131-140 5. Narsinga Rao, B.S. "Nutrient requirements and RDA for girls and women", in Women and Nutrition in India, Ed. by C. Gopalan and Suminder Kaur. Special publication series No.5, NFI, New Delhi, 1989. 6. National Nutrition Monitoring Bureau Report for the year 1977, National Institute of Nutrition, ICMR, Hyderabad, 1977. 7. National Nutrition Monitoring Bureau Report for the year 1990-91, National Institute of Nutrition, ICMR, Hyderabad. 8. National Nutrition Monitoring Bureau Report Urban Surveys, 1993-94. National Institute of Nutrition, ICMR, Hyderabad, 1994. 9. National Nutrition Monitoring Bureau Report Rural Surveys, National Institute of Nutrition, ICMR, Hyderabad, 1996.

10. Pralhad Rao. N, Gowrinath Sastry. I and Murthy. H. V V, "Nutritional status of children of urban slums and poor neighborhood". Paper presented at National Consultation of urban child, New Delhi, NIN, Hyderabad, 1992. 11. Saroj Arya and Visweswara Rao. K, "Secular Trends in Heights of Rural Hyderabad adults", Statistics in

Health and Nutrition, 1990, PP.65-68. Proceedings of the Natinal Seminar, 1988.Ed. by K. Visweswara Rao, G Radhaiah, V Narayana, NIN, 1989. 12. Shatrugna Veena, Leela Raman, Sujatha. T. and Visweswara Rao. K, "Secular trends in the heights and weights of women from rural area around Hyderabad. 67p. Paper presented at Nutrition Society of India, 21st Annual meetings; November 24-25, 1988, Hyderabad, NIN. Table 1: Absolute Weights of Indians A 20 Year Trend Weight Mean 1977 Weight (kg) Mean (Rural ±. STD) I II Female Male Female Male 02 9.0 9.6 9.9±1.45 10.1±1.39 05 13.7 14.1 13.6±1.70 14.4±1.73 10 22.0 21.6 22.4±358 22.6±3.77 15 37.0 35.4 40.0±5.85 39.0±6.93 18 42.3 45.4 42.3±5.67 47.2±5.92 20-24 42.9 48.1 43.5±6.55 50.3±7.02 25-29 42.8 49.9 44.0±7.14 51.7±7.76 50-54 41.6 49.5 45.5±9.32 51.0±9.4l >=60 39.7 47.6 42.2±9.1O 49.1±8.79 >=70** 39.1±8A 47.3±8.6 Sources: ** Quoted by Arvind Wadhwa etal, from NNMB, NIN (Hyd.) 1990-91. I. National Nutrition Monitoring Bureau Report, NIN (Hyd) 1977. II. National Nutrition Monitoring Bureau Report, NIN (Hyd) 1996. III. WHO, 1983, Quoted by Hanumanth Rao & Vijayaraghavan, 1996.

Age (Yrs)

NCHS Median Values III Female Male 11.8 12.3 17.7 18.7 32.5 31.4 53.7 56.7 56.6 68.9 56.6 68.9 -

Table-2 Comparison of body weights (kg) of Indian Girls and Women at Different Time Points Groups Year

Age (yrs)

Children

02 05 10.12 16-19 >18

Adolescents Adults

Tamil Nadu Survey (rural) 1954*** 12.8 22.90 -

All India Survey (Rural) 1955-65* 1975-80** 9.30 13.90 24.76 33.93 41.83

9.14 13.72 24.60 41.01 42.50

Source: *** Nutrition work in states Madras, 1954. * Indian Council of medical Research Growth Studies, 1955-65 ** National Nutrition Monitoring bureau Rural Survey, NIN (Hyd) 1975-80; Quoted by B.S. Narsinga Rao, 1989. Table 3: Avg. height (cm) and Wgt. (kg) of Urban Children from different Soc-Economic Groups Age (Yrs)

Sex Height (cm)

Weights (kg)

HIG

LIG

IL

SLUM

R

HIG

LIG

IL

SLUM

R

2-3

F M

87.8 89.1

80.4 81.6

80.7 82.2

75.5 79.5

82 82.8

11.9 11.4

9.6 10

9.5 10.2

9 9.5

9.1 10.1

4-5

F M

100.2 100.4

94.1 95.5

93.6 95.2

91.1 92.6

95.6 96.1

13.8 14.4

12.7 12.8

12.3 12.9

12 12.5

12.7 13.7

Note: High Income Group (HIG) (n=816) Top officials like Secretaries, Dy. Secretaries, Directors, Professors etc. Low Income Group (LIG) (n=1884) Class IV Employees like Peons, Drivers, Helpers etc. Industrial Labour (IL) (n=2297) Labourers from the organized Industry or Factory Slum Dwellers (SLUM) (n=2386) Households from Big slums of the city Source: NNMB 1984; Quoted by N.Pralhad Rao et al. 1992. Table 4 : Commonly used Anthropometric Classification Classification Indicator

Cut-off lev. As % of NCHS medium

Type/degree of malnut.

<60 60-75 75-90 >90

Severe Moderate Mild Normal

<50 50-60 60-70 70-80 >80

Grade IV Grade III Grade II Grade I Normal

Gomez Weight/age

Indian Academy of Paediatrics Weight/age

Table 5 : Sex Wise % Distribution of (1-5 Years) Children According To Gomez classification ; 20 year Trend Nutrition Grades 1997* (R+U)

93-94** (U)

96*** (R)

10.9 10.3

12.2 8.4

10.1 6.7

F 41.1 M 41.1 Moderate (60-75)

41.8 38.8

40.1 41.1

F M

69.0 40.0

40.0 46.6

42.9 44.1

8.6 8.4

6.0 6.1

6.9 8.1

Normal

(>90) F M

Mild (75-90)

Severe (<60) F M Source: * ** ***

National Nutrition Monitoring Bureau NIN (Hyd) 1977. National Nutrition Monitoring Bureau, NIN (Hyd) 1993-94. National Nutrition Monitoring Bureau, NIN (Hyd) 1996.

Note : R- Rural; U- Urban Table 6 Distribution (%) of (1-5 Years) Children (Boys & Girls) According to Gomez Classification* Weight as % of Standard Income Group

N

>=90 Normal

75-90 Mild

60-75 Moder.

<60 Severe

HIG

191

48.2

40.8

10.5

0.5

MIG

798

38.8

45

15.7

0.5

LIG

933

20.2

47.6

28.7

3.5

IL

1167

19.4

46.1

31.1

3.4

SLUM

1287

12.7

40.7

38.6

8.0

RURAL

15169

13.0

41.9

37.0

8.1

Note : Standards are based on well-to-do children of Hyderabad. Source : Calculated from NNMB 1984, quoted in N. Pralhad Rao, 1992 HIG – High Income Group MIG – Middle Income Group LIG – Low Income Group IL – Industrial Labour SLUM – Slum. Table – 7 : Absolute Heights of Indians- A 20 yrs Trends Height (cm) (1997)

Height (cm) Mean ± SD (Rural, 1996) NCHS Median Value

Age

Female

Male

Female

Male

Female

Male

02+ 05+ 10+ 15+ 20-24 25-29 50-54 55-59 >=60

78.7 99.6 125.7 147.9 151.2 150.9 148.6 149.7 147.9

80.2 100.5 125.1 150.7 163.4 164.0 163.2 162.5 162.3

82.0±5.19 100.4±5.79 126.8±6.79 151.7±6.11 151.2±5.85 151.9±5.95 150.0±6.09 149.7±5.77 147.9±575

82.8±5.45 102.1±5.92 127.9±6.99 154.3±8.18 164.3±6.48 163.8±6.28 161.9±6.60 162.4±6.45 161.0±6.13

86.5 108.4 138.3 16.8 63.7 -

87.6 109.9 137.5 169.0 76.8 -

Source : I. II. III.

National Nutrition Monitoring Bureau Report, NIN (Hyd) 1996. National Nutritional Monitoring Bureau Report, NIN (Hyd) 1977. WHO, 1983, Quoted by Hanumanth Rao & Vijayaraghavan, 1996.

Table - 8: Secular Trends in the Heights of Parents and Adult Children by Caste, Class (Rural and Urban) Caste

Mother

Daughter

Father

Son

152.6+ 149.9 147.6 150.1

160.7

162.6**

150.0 156.4+

166.3

170.6@+

*Rural FC BC SC POLLED

150.6 149.3 147.2 149.1 Urban

# SLUM ## HIG

15.01 153.7

Note: + p<0.05 FC- Forward Caste BC- Backward Caste SC- Scheduled Caste Source: ## Kapoor, S. et al, 1957. # Shatrugna Veena et al, 1987

* Shatrugna Veena et al, 1988** Saroj Arya, 1980 @ K. Visweswara Rao et al, 1993.

Table 10: % Distribution of Adults According to BMI Classification 20 year Trend BMI Class

74-79* (Rural)

88-90* (Rural)

96-97** (Rural)

93-94*** (Urban)

51.8 55.6

49.3 49.0

47.7 45.5

36.7 42.8

F M

44.8 42.1

46.6 46.6

46.3 46.3

51.7 51.7

F M

3.4 2.3

4.1 2.7

6.0 4.1

11.6 5.5

<18.5 (CED) F M 18.5-25 Normal

>=25 (Obese)

Source: *NNMBM NIN (Hyd) 1991, Quoted by N. Nadamuni Naidu et al, 1994. ** NNMB, NIN (Hyd) Rural surveys, 1996. *** NNNMB, NIN (Hyd) Urban surveys, 1994 Table – II

Distribution % of Adults by BMI Grades Vs Community (Rural) BMI Grades

Community Chronic Energy Deficiency (CED)

Normal

Obesity

BMI

<16

16-17

17-18.5

ALL

18.5

20-25 ALL

25-30 >=30

ALL

SC ST Others

10.0 15.1 9.7

12.2 12.2 11.6

31.0 33.6 25.5

53.2 60.9 46.8

20.8 21.0 21.3

22.8 17.0 26.7

2.9 1.1 4.7

3.2 1.2 5.2

Source : NNMB, NIN (Hyd) Rural Survey, 1996.

43.6 38.0 48.0

0.3 0.1 0.5

Calorie Intake Patterns of Rural Indian Households: Evidence from National Sample Survey Data Brinda Vishwanathan and J.V. Meenakshi2 Nutrient intakes and health status are clearly interlinked. It is well recognised by medical practitioners and nutritionists that low intakes not only prevent an individual from achieving his or her biological potential, but they may also affect the immune system, increasing susceptibility to infection and morbidity. Social scientists— particularly, economists—have long been concerned with analysing nutrient intakes so as to better design effective policy interventions to improve nutritional status and thereby enhance the standard of living and labour productivity. The present paper considers variations in nutrient intake based on per capita calorie intake patterns of the rural population across 16 major states in India. More specifically, over the period 1983 to 1993/94, we (a) provide evidence of declining caloric intakes over time; (b) document the shift in calorie intake distributions; (c) identify the commodity and income groups responsible for the decline in calorie intakes; and (d) quantify the relationship between income and caloric intake. In the final section of the paper, we highlight some concerns regarding the use of nutrient intake norms. As is well known, data sources that enable tracking of nutritional status over time are woefully inadequate. In fact, obtaining consistent data on even nutrient intakes is a difficult task. In this paper we make use of the information available in the consumer expenditure surveys conducted periodically by the National Sample Survey Organisation. The 38th round conducted in 1983, and the 50th round, conducted in 1993/94, form the basis of this note. The surveys, conducted for well over 100,000 households nationwide, contain information on the quantities purchased and the value of the purchase of over 200 food items. The reference period used is the 30 days preceding the date of the survey. The food quantities thus recorded are then converted into nutrients (typically, calories, protein and fat) using conversion factors provided by the publication Nutrient Value of Indian Foods of the National Institute of Nutrition. Before presenting the evidence, it is important to note some specific features of the NSS data. First, the unit of analysis is the household. It is thus not possible to capture the distribution of nutrient intake within the household.' although there are indirect methods of evaluating whether there is evidence, for example, of gender bias in consumption (see for example Deaton). Second, the responses are based on the respondents' (usually an adult woman in the household) recall of food purchases and consumption; food intakes are not directly observed by the interviewer, Third, regional differences in cooking practices, and their impact on nutrient content are not incorporated into the data. Fourth, to avoid double counting in the case, for example, where cooked meals are provided to farm employees, the practice is to take these into account in the employer's household, but exclude them from the recipient's household. The impact is to overstate intakes in the employer's and understate intakes in the employee's household.

Fewer but better calories As indicated in Table 1, caloric intakes have declined on average in nearly all states. However, a declining average caloric intake may not necessarily imply a greater percentage of people with below-norm caloric intakes. It is customary to use a cut-off of 2400 calories per capita per day for rural areas in India, although the norm is itself open to question (further on this below). To facilitate comparison of intakes with the norm, we construct the following calorie classes: (1) less than 60% of the norm, (2) 60-70%, (3) 70-80%, (4) 80-90% (5) 90-100%, (6) 100-110%, (7) 110-120%, (8) 120-150% and (9) greater than or equal to 150% of the norm1

2

Brinda Vishwanathan. Madras School of Economics, Chennai. Email: vbrinda@ndf.vsnl.net.in. 1. V Meenakshi, Delhi School of Economics, University of Delhi. Delhi 110007. Email: meena@cdedse.emet.in.

It is apparent that the incidence of abject calorie deprivation has declined between 1983 and 1993/94, in the sense that the proportion of people whose apparent caloric intake is below 60% of the norm has declined in all but three states (Table 2)2 In fact, by 1993-94, in most states less than 15 percent of the population belonged to this class; the exceptions being states such as Kerala and Tamil Nadu. There is also a more dramatic decrease in the percentage of people in the topmost calorie class of 3600 kcal or more (150% or more of the norm) in all the states. However, the percentage of people in this calorie class is small (ranging between 1.5 to 8 percent in 1993-94). The major shift in the density occurs with a decrease in the percentage of people consuming between 110 and 150 percent of the norm, and an increase in the percentage of people consuming between 60 and 110 percent of the norm.3 The declining intakes of those who were already consuming close to or above the norm is thus primarily responsible for the declining average intake observed, Thus, inequality in caloric intake declined between 1983 and 1993-94 for all the states except Maharashtra. There is also evidence of significant commodity substitutions taking place: calories derived from cereals are increasingly being substituted by those from milk products, fats and oils and other processed foods. These substitutions are occurring not merely among the richer quintiles, but may be discerned-albeit to a more limited extent-among the poorest quintile as well. Thus to the extent that commodities such as milk are rich in other nutrients, consumers appear to .be substituting quality for quantity. Tables for these and further results may be obtained from the authors on request. As for intakes among the SC/ST communities, the evidence suggests that after controlling for income and demographic/occupational structure characteristics, caloric intakes among the SC/ST are no different than among other communities. Can the changing pattern of caloric intake be related to income? To analyse this, we first consider changes in average caloric intake separately for the poorest 40 percent of the population. In eight of 16 states, caloric intakes are seen to decline among the poor, in the remaining eight they have increased. It is interesting to note that the list of states where caloric intakes among the poor have declined include Andhra Pradesh, Haryana and Punjab, three states with an established record of rural prosperity; and those where it has increased include the poorest states of Bihar, Orissa and West Bengal. Furthermore, calorie deprivation can be discerned not just among the poor, but the rich as well. Thus while income is clearly an important determinant of intakes, its impact would appear to be mediated through other factors as well.

On the use of a calorie norm: The importance of the calorie norm that forms the basis of many of the comparisons above needs scarcely to be underlined. However, the norm itself is far from being well understood. Social scientists tend to take as given the calorie norm as a datum, recognising that the norm would of course vary by gender, age, occupation and so on. Dig a little deeper and confusion almost inevitably results: one discovers that there are in fact a plethora of norms. For example, the calorie norm prescribed by the FAO is far lower than that indicated by the National Institute of Nutrition.4 Furthermore, the FAO norm would appear to have declined over time, presumably to account for the fact that lifestyles have become more sedentary, and that improved hygiene and sanitation have meant fewer calories need to be budgeted for fighting water — borne diseases, for example. Consult the ICMR only to find that there are norms for each food group-a healthy individual must consume prescribed quantities of cereals, leafy vegetables, milk, fish, and so on. This is clearly troublesome; one implication is that the ICMR considers all vegetarians to be undernourished! P.Y. Sukhatme (1993) has suggested that the body's adaptation mechanisms enable those with lower body weights to utilise a lower amount of calories more efficiently, thus invalidating the use of a nutrient norm at all. Some work done by Minhas (1991) indicates that norms can be derived behaviorally, and that these vary by geographic region-Punjabis feel the need for a far greater number of calories; Gujarat is tend to be more parsimonious in their caloric intakes. The behaviorally-derived norm for Punjab is thus much higher than that for Gujarat.

Of what sanctity then, is the 2400 kilocalories per day? We as social scientists are singularly ill-equipped by our training to take sides on the debate over the norm. We use it in this paper because we believe it to reveal some important information, and to highlight the need for greater debate and clarity on this issue. While adequate nutrient intake is necessary to ensure positive health outcomes, it is by no means sufficient. Nutritional status is distinct from nutrient intake, and depends among other factors, on the availability of a clean surrounding environment and potable water. Assessment of nutrient status necessitates analysis of anthropometric and morbidity data, which are scarcer still than intake data. There is urgent need for research in this area as well.

References: Deaton, Angus (1997), The Analysis of Household Surveys: A Micro econometric Approach to Development Policy, The Johns Hopkins' University Press. Gopalan, C, B. Sastri, V Rama, & S.C. Balasubramanyam (197 1), Nutritive Value of Indian Foods, National Institute of Nutrition: Hyderabad. Minhas, B.S. (1991), "On Estimating the Inadequacy of Energy Intakes: Revealed Food Consumption Behaviour versus Nutritional Norms (Nutritional Status of the Indian People in 1983)" Journal of Development Studies, vol. 28, no.1. Sukhatme, P. V (1993) Note in Report of the Expert Group on Estimation of Proportion and Number of Poor, Perspective Planning Division, Planning Commission, Government of India;

Notes 1. The information on calorie intakes across households in each state grouped into nine classes formed on the basis of the percentage of the norm (2400 kcal per capita per day for the rural areas): This study looks at the percentage of population in each of the per capita calorie intake classes unlike the published NSS results which give the number of households.

2. The exceptions are the states of Gujarat Madhya Pradesh and Maharashtra, which show an increase. In fact, the increase in Maharashtra is the highest in this category: 46 percent. 3. There are some states that show an increase in the percentage of people in the calorie classes between 2 to 5, and a decrease beginning from 6th calorie class. 4. This norm of 2400 calories per capita per day is an average and includes a margin of safety. The FAO, for example, defines the minimum calorie requirements for Indians as 1810 kilocalories per day. This is an abridged and modified version of the paper presented at the Convention on Development, hosted by the Madras Institute of Development Studies, 16-18 November 2001. We are grateful to Mr. Sanjeev Sharma for help with the data; to the SDRD, Calcutta and Mr. Shukla and his team at the NSS Computer Center for promptly responding to all our queries. An earlier version of this paper was presented at the NSS Golden Jubilee conference in May 2001.

Table 1. Some Summary Statistics on Caloric Intake and Poverty States

Avg. Per capita per day

HCR-Calorie HCR-Poverty Line Norm (percent) (percent)

Coefficient of Variation

Calorie Intake (K Cal) 1983

1993-94

1983

1993-94

1983 1993-94

1983

1993-94

Andhra Pradesh

2206

2052

67.45

76.15

35.78 29.49

0.38

0.32

Bihar

2191

2115

67.76

85.19

60.47 65.28

0.37

0.30

Gujarat

2125

1994

73.02

78.92

39.03 29.78

0.39

0.32

Haryana

2557 2491

53.87

56.01

27.54 28.26

0.46

0.62

Himachal Pradesh

2624 2325

47.46

62.01

23.87 34.00

0.38

0.30

Jammu & Kashmir Karnataka

2572 2507 2259 2073

44.83 44.64 65.11 72.83

31.61 15.44 39.99 36.98

0.37 0.47

0.25 0.32

Kerala

1890 1966

80.49 78.19

48.46 33.25

0.45

0.34

Madhya Pradesh

2329 2165

62.07 70.09

53.73 35.86

0.44

0.33

Maharashtra

2146

1940

72.86 82.69

54.56 50.38

0.36

0.48

Orissa

2103

2199

70.70 67.28

66.15 56.41

0.36

0.30

Punjab

2681

2418

45.92 57.64

18.47 15.62

0.46

0.35

Rajasthan

2526

2470

53.31 51.15

46.66 26.11

0.65

0.30

Tamil Nadu

1908

1884

78.36 80.58

59.13 41.94

1.07

0.47

Uttar Pradesh

2402

2307

59.15 62.61

50.76 41.09

0.41

0.34

West Bengal

2028

2211

76.97 69.42

66.70 52.39

0.51

0.28

Note: (1) HCR: Head Count Ratio (2) The Head count Ratios are from Sen and Palmer-Jones (2001) and the other values are author's own calculations (3) Calorie norm is 2400 kcal per capita per day.

(4) Q1 and Q5 refer to the poorest and the richest expenditure quintiles respectively.

Table 2. Percentage Change in Proportion of Persons Across Different Per Capita Calorie Intake Classes between 1983 and 1993-94 States

Per Capita Calorie Intake Classes (kcal) Class I

Class 2

Class 3

Class 4

Class 5

Class 6

Class 7

Class 8

Class 9

Andhra Pradesh

-5.55

38.70

25.26

12.74

1.87

-11.65

-28.42

-40.23

-56.06

Bihar

-19.70

10.21

9.17

20.05

15.09

4.63

-1.40

-23.30

-59.53

Gujarat

10.45

13.56

22.83

-1.52

9.44

.25.18

-20.72

-21.69

-63.03

Haryana -5.08 Himachal Pradesh -12.23

42.12 65.00

12.35 54.56

-6.95 63.87

1.88 24.65

-0.68 -0.18

-6.36 -18.34

4.18 -44.89

-27.35 -63.94

Jammu & Kashmir -90.74

-37.79

-21.45

34.74

26.75

79.15

-14.48

-17.84

-55.54

Karnataka

-25.08

43.31

24.37

50.03

2.50

-10.31

-8.43

-22.53

-72.25

Kerala

-36.05

4.80

19.56

35.86

13.70

28.22

28.92

-1.01

-36.83

Madhya Pradesh

10.17

36.98

18.56

3.42

4.05

-10.31

-16.76

-25.38

-42.88

Maharashtra

48.35

32.39

18.28

1.64

-21.08

-26.10

-34.61

-45.41

-62.29

Orissa

-50.32

-16.07

-10.78

33.93

34.63

36.96

9.29

6.83

-35.42

Punjab

-42.70

29.66

50.44

34.91

38.75

12.64

-0.63

.28.94

-51.00

Rajasthan

-46.76

7.96

-1.87

4.14

15.38

22.67

l.27

11.25

-39.29

Tamil Nadu

-22.23

15.83

21.47

23.99

22.51

6.08

-9.34

-25.73

-58.78

Uttar Pradesh

-25.85

3.22

11.76

24.22

15.64

5.29

-9.88

-13.90

-38.32

West Bengal

-76.41

-21.35

16.17

30.52

67.17

56.17

47.21

26.26

-42.87

Note: Class I: less than 60%, Class 2: 60-70%, Class 3: 70-80%, Class 4: 80-90% Class 5: 90-100%, Class 6: 100110%, Class 7: 110-120%, Class 8: 120-150% and Class 9: greater than or equal to 150% of the norm of2400 kcal per capita per day.

Who rends the MFC bulletin? Contrary to all expectations, this is not a rhetorical question. A look at the list of the subscribers was quite revealing. The MFC bulletin's current subscribers include some of the most famous names in public health in India, renowned medical specialists practising in the metros, doctors practising in small district towns, other who have spent much of their working life in community health programmes in remote villages, deans and professors of medical colleges, university teachers and scholars, women's groups, trade union workers, activists, researchers, weavers' co operatives in the North East, people's organisations and voluntary organisations of all kinds and working in all regions of the country.

Articles from the bulletin are on the reading list of postgraduate courses in social sciences, social work and women's studies. The bulletin is often the development worker's first introduction to a more critical perspective on health. There are many different reasons for reading the bulletin. Some read it for information, some to find out what the MFC is debating, for others it is the means to make sure that the organisation is still alive and meets twice a year. There is also a good reason for printing the bulletin. Apart from the fact that email access is quite limited, unlike computer files, paper can not be deleted or corrupted or crash. No one even tears or throws away printed paper - they just pass it on. The important question is - who writes for the MFC bulletin? There should be a simple answer to the question posed above. Those who read the bulletin write for it.

The bulletin invites all readers to contribute to the bulletin. You could send us Full length articles unpublished articles on any issues related to health. Initiatives and Retrospectives - small write-ups on activates undertaken by MFC members in their own organisations or in their individual capacity. Retrospective pieces on such reports printed earlier in the MFC bulletin. Dialogue - Letters to the editor, response to articles printed in the bulletin, requests for information, assistance, opinions etc. Postbox - News about MFC’s activities programmes held, being planned, actions taken (Public Interest Litigation filed, signature campaigns etc.) Personal news that you may wish to share with the MFC.

Biodiversity: the basis of nutritional adequacy and food security Vanaja Ram Prasad Food security has been the agenda of the developing countries for a long time now. Countries in the Asian region have taken pride in participating in the green revolution and producing surplus food. Yet, a large percent of the population in these countries today suffers from chronic malnutrition and poverty. It is obvious that these countries have achieved neither nutritional well being nor food security. This state of affairs raises many questions. What exactly is food security? Is it surplus food production? Or is it access to food for the entire population? Is it producing for the growing urban population? It is always construed that low and stagnating productivity in agriculture is what contributes greatly to existing poverty and food insecurity across the developing world. Can centralised food production systems provide food security? Can monocultures of cereals grown with high external inputs be sustained over a long period? The logic of economies of scale and the elusive logic of the market - are they compatible in providing the food security? National food sovereignty and global trade - can they have a common goal? Privatisation of life and genetic engineering - are they justified in meeting the goals of food security for the poor and marginalised? Will biotechnology and genetic engineering fulfill the promise of feeding the growing population or will they compound the problem? These and many other related questions have no straightforward answers, and the absence of adequate information cannot be taken as affirmation and consent.

What is the answer to providing food security? The idea that biotechnology wi II solve the world's problems of food and nutrition is founded on myths that are being promoted to mislead the common person. In order to justify the introduction of genetically engineered varieties, it is argued that this technology will feed the growing millions who are undernourished. Time and again it has been pointed out that as long as social inequalities are perpetuated, merely increasing food production cannot reach food to the poor. If it were true there would be no need for the Supreme Court to observe that food grains overflowing in the godowns should reach the starving people. There are an estimated 208 million under-nourished people in India. Twenty six percent of India's millions have been identified as below the poverty line requiring subsidised food grains. Unfortunately, nutritionists define malnutrition very narrowly as a condition resulting from the deficiency or excess intake of nutrients. There is evidence from the National Family Health Surveys to show that 47 percent of children under the age of four years are underweight for their age and 13 percent arc severely underweight. They also point out that prevalence of vitamin A deficiency and other forms of nutrient deficiencies are high amongst vulnerable groups. The poverty population nexus is said to be the main cause of the malady. While it is clear that the nature of the problem is purely social and economic, the solution offered is solely a technical one. For long we have bought the argument that increasing production will solve the problem of food, but the outcome of the green revolution or the white revolution in the country does not support this. While recommending a technical solution we have often forgotten the market mechanisms that playa major role in the demand and supply. Even today it is the subsidy phenomenon that has made possible the cheap food produced on a large scale for the urban population. It is not only the nature of the chemical inputs that is short in its goal of food production, but also the subsidy that is given to achieve this goal, which is reaching the producer of the chemicals and not the producer of the food. The heavy burden of external debts and the ecological destruction of natural resources, which is the capital base of farmers, is borne by the producers. Indebtedness has increased to such an extent that agriculture has been rendered unsustainable and unviable. There is also the belief that people have to be educated because there are gaps in their knowledge base about the complexities of biology. This popular myth about people's ignorance has been well stated by the famous biologist Rupert Sheldrake9 Who says, "I was taught that direct intuitive experience of plants and animals was emotional and unscientific. According to my teachers, biological organisms were inanimate machines, devoid of any inherent purposes; the product of blind chance and natural selection and indeed the whole of nature was merely an inanimate machine-like system." He adds, "From the time of our remotest ancestors, it was taken for granted that the world of nature was alive. But in the last three centuries growing numbers of educated people have come to think of nature as lifeless. This has been the central doctrine of orthodox science - the mechanistic theory of nature." The fact that bio-diversity in food production ensuring nutritional needs no emphasis. There is a thin line separating nutrition and medicinal value. 9

Sheldrake, Rupert The Rebirth of Nature. Century (1990)

The scriptures speak of many varieties of rice, which include gandha sali or perfumed rice, rakta sali or red rice and sookma sali or small rice. Similarly, scholars who traveled in the state of Karnataka in 1880 speak of different varieties of paddy such as dodda batha, kembuthi, yelakki raja and so on". Traditional agriculture in India is one of the oldest and most advanced forms of food production. It has proved to be inherently sustainable over centuries and rates high in all aspects of total productivity, self-reliance, diversity and the depth of its indigenous knowledge. Traditional practices did not simply exist as a result of some divine revelation. They were, on the contrary, the result of an understanding of the mechanisms of nature; the result of a science that was accessible to people on a day-to-day basis and not of one confined to a laboratory. This involvement of the practitioners themselves played an important role in making the system sustainable. The navadanya and Baranaja systems of multi cropping elucidate the nutritional balance derived. The minor millets which have been designated 'crops of low value' have unique nutritional value. Table 1 gives details of the nutritive components of the millets as against popular crops like rice. The table also describes the nutritive value of the uncommon greens, which are a source of nutrition for the poor.

Nutritive value of crops (per 100 Gms) Crop

Protein Calories

Calcium

Iron

Vitamin A

Ragi (Finger millet)

7.3

328

44

6.4

42

Bajra (Pearl millet)

11.6

3101

42

5 -132

47

Jola (Sorghum)

260

48

625

145

47

Udulu (Barnyard millet)

6.2

307

20

209

Navane (Fox tail)

12.3

290

37

12.9

Arka (Kodo)

12.8

309

27

5.2

Samai (Little)

6.2

307

20

2.9

0

Amaranth

16-19 366

25-389

3-22

14,190

Rice

12

16-34

3

0

353

Examples of Leafy vegetables having high levels of carotene Leafy vegetables

Carotene

Agathi Amaranth, tender gangeticus Betel leaves Carrot leaves Coriander leaves Cow pea leaves Curry leaves Drumstick leaves Radish leaves Spinach

5400 5520 5760 5700 6918 6072 7560 6780 5295 5580

Farmers have contributed to the continuity of genetic diversity and the dynamic conservation of land. This informal system has relied on the skills of farmers to maintain, enrich and utilise crop diversity. The main selection criteria are: yield stability, risk avoidance, low dependence on external inputs and a range of factors associated with storage, cooking and taste. An area in which biotechnology plays a major role is the selection and breeding of crops. The basic need is to conserve and improve hardiness, nutritional value and yield of diverse crops used by the poor. The dominant research focuses on, for instance, gene transfer for drought resistance, pest resistance or herbicide resistance. All these exclude the possibility of rotational, mixed cropping and the diverse farmer’s varieties that are the basis of sustainable and ecologically balanced forms of agriculture and food security. With the kind of genetic diversity that can be accessed by farmers and the diverse knowledge system of farmers, is there really a need for genetic engineering? Specific qualities of seed varieties such as pest resistance, drought resistance and yield are identified today. This is predominantly seen in the inter species and intra species diversity. Some simple truths about farmers' varieties: While improved varieties have a higher potential than the farmers' varieties, the yield potential cannot be achieved in resource-poor environments. Farmers use locally adapted varieties or mixtures of varieties by which

they are able to spread the risk of crop failure resulting from pest and diseases or drought. Farmer's varieties are well adapted to diverse conditions. It is also observed that in highly variable environments the farm production is obtained from growing a range of crops adapted to the microenvironment. Inter-cropping provides balance in the food consumption, while agro-forestry takes care of the needs for fodder and fuel. Crop combination enriches soil fertility and allows for judicial use of soil nutrients by the different crops. Women's knowledge is captured in choosing the crop combinations that ensure food security. Traditional cropping patterns have helped in pest control. Since many of the pests are specific to particular plants, planting different crops in different years causes large reductions in pest populations. Such cropping systems require less irrigation, which has been found to prevent the spread of pests. In the light of this, do we really need pesticide-resistant, genetically-engineered crops?

Traditional pest management practice Farmers have innovated to control pests and have successfully kept out the use of chemicals. Some simple examples follow: Caseworm control in paddy If caseworms appear in the paddy field, the water is drained out. Gliricidia leaves are broadcast in the field. 2 teaspoons of pongom oil, 2 teaspoons of Neem oil and 3 teaspoons of soap powder are mixed well in a bucket (10 litres) of water. This mixture is sprayed 3 times with intervals of 2 days. The fields are kept clear are kept clear of standing water. Stem borer control in paddy In the growth stage, paddy is most vulnerable to pests. Stem borer and leaf roller moths lay their eggs on the plant. Farmers use traditional methods for pest management during this period: A fire is lit next to the paddy field in a spot from which the smoke will blow on to the paddy. Some bones are placed on the fire. Farmers believe that this smoke chases away the pests. Spiders are encouraged to spin webs in the paddy field. Flying insects such as the stemborer and leaf roller moths are trapped in these webs and can easily be controlled. Castor: Control of semi lopper problem Semi lopper is the major pest in castor. These caterpillars eat the leaves and can strip the plants down to skeletons overnight. The farmers of Thalli practice a very interesting method of pest control. They go to the field, carrying a small bowl of water with them. They pick the caterpillars and put them in the water. Later, the caterpillars are buried in a pit. Root rot control in groundnut Young seedlings of groundnut suffer from root grub and root rot problems. To prevent these, the farmers crush 1 kg of Neem leaves and extract the juice. They put 4 - 5 leaves of Agave Americana (Kathale) in the fire for a few minutes, then squeeze and extract the juice. The Neem and Agave Americana juice is mixed in 1 liter of cow's urine and left over night. Next morning the solution is filtered and mixed with 10 liters of water. This diluted solution is sprinkled on the groundnut before sowing. This innovative method of pest control was discovered by farmer Siddegowda of Kunigal.

The promises of genetic engineering It is clear that a broad genetic diversity is an absolute must for sustainable food production. Genetic engineering promotes promises of improving the qualities of crops, and perfects them. The technology promises to feed the growing billions. The technology also promises pollution free cultivation. Speaking about the role of genetic engineering, Dr Mae Wan Ho from the Open University in the UK refutes the need for genetic engineering to feed the population on the count that there is scientific evidence of actual and potential hazards of GMOs on people's health and the environment. There are several arguments why organic agriculture should be free of genetic engineering. This technology essentially changes the genetic make-up of plants and animals within the confines of a laboratory. These transgenic experiments involve the transfer of genes from one species to another, using vectors such as viruses or bacteria, since the construction of artificial vectors is fundamental to genetic engineering. Common arguments against genetic engineering Scientists in favour of genetic engineering use the argument that if most characteristics of genetically engineered foods are similar to its non-genetic engineering counterpart, they both are comparable. Long-term trials are avoided since they are expensive and time consuming. Genetically engineered foods can be hazardous for people because of the unintended sick effects. The technology is too new for long-term effects to show up. Until long-term human trials are done, no one can give a 100% assurance that genetically engineered foods are safe to eat. Genetically engineered foods present a false appearance of being fresh while they remain on the shelves for a long time, gradually being depleted of all their nutrition. Use of antibiotic-resistance genes in biotechnology can confer resistance to the drugs that help fight infection and diseases. Genetic engineering works on the principle of horizontal gene transfer. Many scientists have expressed concern that widespread planting and consumption of genetically engineered food will lead to a massive release of antibiotic resistant genes. An excellent example of the application of genetic engineering to alleviate human misery has been claimed by the biotech industry. Zeneca Ag Products made the following announcement in a news release issued on May 16, 2000: "A collaboration is announced today that will help fight blindness in developing countries through the use of genetically modified rice. The collaboration will help the inventors of 'golden rice' to deliver their gift of nutritionally enhanced rice to the developing nations of the world, bringing closer the health benefits for countries where Vitamin A deficiency is the cause of 50,000 cases of irreversible blindness each year. The inventors of Golden Rice have reached an agreement with GREENOVATION and ZENECA and are working with other agencies throughout the world to enable the deli very of this technology free of charge for humanitarian purposes in the developing world. This will bring closer the 1982 vision of the Rockfeller Foundation, which stimulated and funded this research into rice varieties that might offer global public health benefits." Michael Pollan, in his new book which is to be published, The Botany of Desire, has taken a critical look at the claims of the biotech industry. Pollan writes: "Watching the pitch of the advertisement and claims made by the industry, one can almost feel the moral ground shifting under one's feet." The unspoken challenge, he says, is that "if we don't get over our queasiness about eating genetically modified food children in the third world will go blind." It is observed that an 11 year old would have to eat 15 pounds of cooked golden rice a day to satisfy the minimum daily requirement of vitamin A. Apart from the many imponderables on the usefulness of the golden rice to solve the problem of vitamin A in children there remains the fact that the health consequences of malnutrition extends also to iodine, iron, vitamins like the B& C, calcium and many other micronutrients. There is no quick fix or magic solution, to solve the problem of inadequate nutrition.

Rice-eating populations have always consumed various sources of Vitamin A that were easily available to them in the form of undervalued and underutilized plants grown in their own fields. With the coming of the chemical applications, these uncultivated sources of nutrition were totally lost to the people. What was described as 'weeds' by the scientific community have provided the much needed source of various nutrients that go with subsistence food. Women depend upon uncultivated foods to meet their nutritional needs. Green Foundation's experience in conservation of diversity has gone beyond the question: "Can indigenous varieties feed the growing population?" Its concern is the concept of food security for the small and marginalised, who are the custodians of diversity. Reference Sheldrake, Rupert; The Rebirth of Nature; Century; 1990 Yagna Narayanan Iyer; Agriculture in Karnataka; 1945 The bulletin needs more than your subscription. Please use the bulletin as a space to share your ideas and opinions. — Editor

The politics of food: keeping the other half hungry Devinder Sharma3 "To those who are hungry, God is bread" — Mahatma Gandhi, 1946 Every day, some 24,000 people die from hunger, starvation and related diseases. And by the year 2015, by which the FAG aims at reducing the number of hungry by half, more than 122 million people would succumb to mankind's greatest shame - hunger and too at times of plenty.

A few months back, hundreds of people in the United States, mostly agricultural scientists, signed an AgBio World Foundation petition appealing to the seed multinational giant, Aventis CropScience to donate some 3,000 tonnes of genetically engineered experimental rice to the needy rather than destroy it. More than feeding the hungry, the appeal was a public relations exercise to demonstrate the concern of the biotechnology proponents towards feeding the world's poor. The appeal did not, however, motivate the Food and Drug Administration (FDA) and the United States Department of Agriculture (USDA) to listen to the "humanitarian intentions". The genetically modified rice was eventually destroyed. Aventis CropScience Company had expressed concern about the hungry in the world, stating that it is "working hard to ensure that US farmers can grow abundant, nutritious crops and we hope that by contributing to that abundance all mankind will prosper". And AgBio World Foundation, at the same time conveyed its "disapproval of those who, in the past, have used situations similar to this one to block APPROVED food aid to 3

Devinder Sharma is a journalist, author and a food and trade policy analyst. Among his recent works include two books: GA IT to WTO: Seeds of Despair and In the Famine Trap. He also chairs the New Delhi based Forum for Biotechnology & Food Security. Responses can be emailedat: dsharma@ndjvsnl.net.in . Address: 7 Triveni Apartments, A -6 Paschim Vihar, New Delhi-1I0 063, India. (Tel: +91-1I- 525 0494)

victims of cyclones, floods and other disasters in order to further their own political (namely, anti-biotechnology) agendas. Almost at the same time, the Indian Prime Minister, Mr. Atal Bihari Vajpayee, had said in his inaugural address to a national consultation on "Towards a Hunger Free India" in the last week of April, in New Delhi: "Democracy and hunger cannot go together. A hungry stomach questions and censures the system's failure to meet what is a basic biological need of every human being. There can be no place for hunger and poverty in a modern world in which science and technology have created conditions for abundance and equitable development." Laudable words indeed. And if wishes were horses, Prime Minister Vajpayee would certainly provide an easy ride to the hungry and starving millions. And so would the plant biotechnologists, who continue to swear in the name of the hungry. Their concern for feeding the world should not be misconstrued as aimed at eradicating hunger. It is only aimed at increasing the profits of the private seed biotechnology companies and that too in the name of the world's poor, hungry and severely malnourished. Eradicating global hunger is certainly a pious intention. For a mere 3,000 tonnes of genetically modified rice, the human health risks of which have still not been ascertained, the US Agri-biotech industry as well as its 'shouting brigade' had made so much of hue and cry. But when told that India has a surplus of 60 million tonnes of foodgrains, and that too non-genetically modified, and has a staggering population of 320 million people who go to bed hungry every night, the AgBio World Foundation and those who signed the appeal, were not interested. Suddenly, all their concern for feeding the hungry evaporated, "the humanitarian intentions" vanished into thin air. All over the world, molecular biologists are screaming over the need to push in biotechnology to increase food production, to feed the 800 million hungry who sleep empty stomach. Politicians and policy makers are quick to join the chorus, not realising that hundreds of million of the hungry in India or for that matter in South Asia are staring with dry eyes at the overflowing food granaries. Ironically, India, Pakistan and Bangladesh, perhaps the world's largest collective of the hungry, are overflowing with foodgrains for the past few years. And if the South Asian governments, aided and ably supported by the agricultural scientists and the Agri-biotech companies, were to launch a frontal attack to ensure that food reaches those who need it desperately, half the world's hunger can be drastically reduced if not completely eliminated now. And if you are wondering whether the international community is in any way genuinely concerned at the plight of the hungry, hold your breadth. At the time of the first World Food Summit (WFS) at Rome in 1996, Heads of State of all countries of the world had 'reaffirmed the right of have access to safe and nutritious food, consistent with the right to adequate food and fundamental right of everyone to be free from hunger.' They considered it unacceptable that more than 800 mil1ion people throughout the world did not have enough food to meet their basic nutritional needs. These leaders committed themselves to halving that number by the year 2015. In other words, they had postponed the monumental task to feed to world to the year 2040. The WFS vowed to feed half the world's 800 million hungry by the year 2015, meaning thereby that it would need another 20 years to provide food to the remaining 400 million hungry. And by the time the year 2015 dawns, the number of hungry would have multiplied to 1.2 billion. So in al1 plausible terms, the heads of State had actually expressed their helplessness in tackling hunger and malnutrition. Once again, the Heads of State of al1 the countries are expected to meet at Rome for the 'WFS plus Five' meet in June 2002, to take stock of the efforts made to reduce hunger since they met five years ago. And once again, they will make a pledge in the name of 'humanity' to eradicate hunger from the face of the Earth. In reality, while hunger will continue to be robustly sustainable, it is the hungry and acutely malnourished who will perish waiting for a morsel of food. Every day, some 24,000 people die from hunger, starvation and related diseases. And by the year 2015, by which the FAO aims at reducing the number of hungry by half, more than 122 million people would succumb from mankind's greatest shame - hunger and too at times of plenty. Such is the global concern for the hungry and poor, that while thousand of the poverty stricken die every day, the international community very conveniently prefers to turn a blind eye. The forthcoming 'WFS plus Five' is merely an opportunity to draw global attention to the 'pressing' need to strengthen the FAO. It is not at all aimed at drawing political will towards eradicating the biggest scourge of mankind.

The Food and Agricultural Organisation of the United Nations (FAO), the International Food Policy Research Institute (IFPRI), the World Bank and the IMF, are equally guilty of not making any tangible effort to build up political pressure to force countries of South Asia to feed their hungry populations. In India, the grain silos have been bursting at the seams for the past five years, with a record surplus build up of 60 million tonnes in 2001. By the time the 'WFS plus Five' takes place, and with the arrival of the new paddy crop, the food grain surplus wil1 grow by another 20 million tonnes, much of it left stacked in the open for want of storage facilities. The food surplus in India alone may be more than what is being international traded at present. For the FAO and for that matter IFPRI, keeping hunger alive is the only way to keep the international organisation functional. FAO was created at the 1943 Hot Springs Conference but its responsibilities were limited to gathering and disseminating data on agricultural commodities, production and trade. And this is what BoydOrr, the first Director General of the FAO had to say, "The poor required food and all that they got was statistics." The IFPRI, which organised a massive conference at Bonn in September on sustainable food security by 2020, talked about the desperate food deficit in Africa but for some strange reasons refrained from commenting on the scandalous paradox of plenty in South Asia. It is therefore obvious that the poor continue to be fed with an overdose of statistics while the onerous task to feed the hungry mouths has been now left to the market forces. Whether it's the mapping of the rice genome, or the development of Vitamin-A enriched 'golden rice', or the induction of agriculture in international trade, FAa has been on the forefront of welcoming these initiatives in the name of feeding the world and ensuring food security. FAO, in fact, is now openly supporting the corporate sector's role in increasing production and eliminating hunger. Politicians will join hands with economists and agricultural scientists to chant the mantra of the potential of genetic engineering in boosting food production and solving, hidden hunger and malnutrition deficiency. They wil1 say that the development of genetically modified technology holds great promise, with the potential to complement other, more traditional research methods as the new driving force for sustained agricultural productivity growth in the 21 st century. Such agricultural productivity growth is crucial if the world is to produce enough food to provide for what is likely to be a stable but large world population in this century. In the midst of such cleverly drafted statements, primarily to confuse the audience and diffuse the contentious issues concerning the application and desirability of GM technology, what to do with the bursting grain silos in the South Asian countries will again be pushed to the background. What is also not being accepted, and for obvious reason, is the startling fact that the South Asian godowns are overflowing when the average productivity of cereals hovers around 2 tonnes a hectare, amongst the lowest in the world. In any case, these countries have the potential to raise production at least by three times with the available technology. So why bring in an alien and risky technology in the form of GM crops? Strange, that those who will assemble at the ‘WFS plus Five' at Rome do not know that farmers in India are committing suicide because there are no buyers for the grains they harvested. In Pakistan, farmers have reportedly burnt the harvested grains lying in markets because the prices are too low. In Indonesia, farmers wait endlessly to sell rice while the government imports it from Vietnam. While the FAO and the multi-billion Agri-biotech industry is harping relentlessly on the need to produce more food keeping the projected global requirement in the year 2020 in question, farmers are increasingly abandoning agriculture. Farmers in India, for instance, are now being asked by the political masters not to grow more cereals because the government cannot buy it. Should the farmers continue to 'produce and perish' keeping in mind agricultural scientists repeated assertion that the world would need to produce more in the year 201 5? Should the farmers again produce a bumper harvest the next 'crop season knowing well that there would be no buyers for his produce? Should he be forced to commit suicide for producing more? And, should the poor and hungry wait for the year 2015 to get a morsel of food while the overflowing grain silos are being divested by insect pests and rodents and that too in front of his own eyes?

There IS money in hunger an ma nutrition. or ten Indian Government exporting surplus w eat an rice will bring in some much-needed hard currency and that too by keeping food out of the reach of the hungry millions. And for two American companies. it is immensely profitable to convert a traditional animal feed into a value-added nutritious food for the acutely malnourished Indians. Isn't it a strange paradox? While the government plans to export the 'surplus' and 'unmanageable' rice and wheat stocks, an American company, RiceX, has entered into a joint 'collaboration with the multinational agribusiness giant, Monsanto, to produce and test its patented technology for nutritious food, treating the Indians as guinea pigs. In simple words, the technology is being field tested for the first time on Indians.

In three to five years, if the tests prove successful, rice bran conversion into a nutritious food will yield a projected revenue of US $400 million a year. That would be a welcome contrast to the company's $5 million loss last year. Company Chairman Daniel McPeak has been reported as saying that the deal calls for RiceX and Monsanto to split the cost of sending two processing units, a container ship of US grown rice and staff to India. RiceX employees will process both US and Indian rice bran there during a six-month test phase. RiceX has developed a process to extract and stabilize nutritious rice bran from raw rice kernels. The EI Dorado company sees Third- World rice-growing countries as a fertile product market. And it has reasons to do so. After all, the numbers of the hungry and malnourished in India alone have been steadily rising. In the rural areas, from 224 million in the early 1990's to 250 million in the mid-1990's. This corresponds to an almost constant increase in the incidence of rural poverty and a slow decline in the incidence of urban poverty, the World Bank report states. But with the successive governments abdicating their responsibility to feed the nation, and in a country where poverty and hunger have proved to be robustly sustainable, the emphasis has shifted to food exports. In other words, the policy initiative is to strengthen the national exchequer by depriving the people of the basic human right - the access to food. In any other country, including the world's only super cop - the United States, food exports are only allowed after the nation's food requirements have been adequately met. In the US, for instance, the government spends US $ 54 billion to make available food to an estimated 25 million people languishing below the poverty line. In India, on the other hand, food buffers are built essentially by keeping the food away from the reach of the poor. With food prices continuously rising, and with the percentage of the population earning less than a dollar a day also keeping pace, more and more people are finding it difficult to meet their daily food needs. The result is obvious: foodgrains buffer continues to be comfortable and reassuring. Aware of the contempt that the government as well as the country's elite holds for the poverty stricken masses, the monumental task to feed and meet the growing nutritional needs of the nation is being left to the market forces. While the country is busy finding export markets for the golden grain even if it means selling it eventually for the cattle; western companies are taking the reverse route - trying to market cattle feed for human consumption in India!

Taking a lead, the Monsanto-RiceX project will convert abundantly available rice bran, which is being used as low-grade animal feed, into a stable and nutritious food source for the Indians. Says Mr. Charles F. Hough, Business Development Head - Nutrition and Consumer Sector for Monsanto, "We are keenly interested in expanding our activities in India. The RiceX proprietary technology could be a vehicle for Monsanto to contribute significantly to the nutritional well-being of the people of India."

Level of Malnutrition and Gender Difference in the prevalence of malnutrition among ICDS Anganwadi Beneficiaries; Working Paper Dr. Prakash V. Kotecha, Dr. Kailesh Bhalani and Dr. Samir J. Shah4 Background & Methodology There was a general feeling among many friends that the level of health care is gender insensitive even in health care set up like Integrated Child Development Services (ICDS). However adequate data of gender difference did not exist. ICDS being one of the most talked program in the field of health and nutrition and most widely reached to the community. A study to really assess the true situation was considered a matter of priority by the department of Preventive & Social Medicine, Medical College, Vadodara and the enthusiastic post graduate students seized this opportunity to convert that in to their thesis work for MD. Dr. Samir Shah carried out the first study in the three areas of Vadodara district picking up Anaganwadi’s form rural (45/185) from Dabhoi, tribal (45/77) from Tilakwada and urban (30/166) Vadodara slum. Total children covered included 823 urban, 704 rural and 999 tribal children. To see gender difference in the level of malnutrition was one of the major goals in his study. However what goes on at ICDS level about the regular attendees with reference to level of malnutrition was another important objective and therefore his study to covers a follow up data available for children in Anganwadi from Jan 96 to Jan 98. As a result his starting point was 30 months and above. Working further on the data, it was evident that more events in the level of malnutrition are observed at a younger age than 30 months; Dr. Kailesh Bhalani's study focuses on the children under 60 months of age from urban Anaganwadi’s only. His study from 30 Anganwadi in urban slum included total of 3310 children less than five years of age. The gender difference and performance of children while in Anganwadi are studied. The change of level of malnutrition form their status in 1996 to 1998 is analyzed total as well as with gender perspective. The comparison of the level of malnutrition is made between Indian Academy of Pediatrics (IAP) classification and National Centre for Health statistics (NCHS) standards recommended for the use in all the developing countries in both the studies. The difference between two ways of classifying malnutrition and its implications are highlighted.

Results: In general the level of malnutrition is higher in girls than boys according to the classification followed by the Anganwadi based on IAP. The level of malnutrition is not very high and ranges from 1% to 4% when grade 3 & 4 only are included in the malnutrition. The level is raised in the range of 16% to 35% of level of malnutrition when grade 2 of malnutrition according to IAP classification is included in the malnutrition category. (See table) What is evident here however is a gender difference. The level of malnutrition is consistently higher in girl child as compared to boys and the level of difference is on rise for severe grade of malnutrition to as high as two to three times more in girls than in boys. For the performance of ICDS with reference to their objective of reducing the level of malnutrition in children, the level of malnutrition in 1996 is compared with 1998, not in general but from individual child to 4

The authors belong to the Department of Preventive & Social Medicine, Medical College, Vadodara

child. It is revealing to note that a large proportion of healthy children in 1996 in fact have shifted to different levels of malnutrition in 1998 showing a retrograde trend. Further this shift form healthy to malnourished status is proportionately more in girl child than in boys. When these very data are analyzed with NCHS standards, the gender difference disappears and at places gets reversed. This can be explained on the basis of the separate standards prescribed for boys and girls while IAP classification fails to set different criteria for boys and girls and measures the level of malnutrition independent of the gender. This obviously will always end up showing more girls malnourished than boys will, as biologically they are separate and their expected weights are also different. What is further noteworthy is a remarkable change in prevalence of malnutrition by NCHS standards. The level of severe malnutrition shown in the range of 1% to 3% by IAP classification jumps to around 10% and moderate to severe malnutrition level rises from the range of 16% to 35% by IAP classification to the range of 50% to 75% by NCHS standards. The aim of the concept paper is to discuss this point and review: 1. Is ICDS really able to reduce the level of malnutrition as desired? 2. Whether the gender difference observed in level of malnutrition is true of limitations of IAP

classification? 3. Whether we should use IAP classification or it gives a false sense of security by showing lesser prevalence of malnutrition than real level?

Table 1 : Showing age wise of level of malnutrition IAP classification vs. NCHS classification in Vadodara Urban slums Age Group

Moderate to severe malnutrition %

Severe malnutrition (%)

(In months)

IAP

NCHS

IAP (Gr. 3%4) NCHS (<-3 SD)

<6 n=293)

18 (6.1)

17 (5.8)

4 (1.4)

1 (0.3)

6-11 (N=417)

51 (21.1)

124 (29.7)

5 (1.2)

26 (6.2)

12-23 (n=731)

154 (21.1)

386 (52.8)

21 (2.9)

100 (13.7)

24-35 (n=777)

205 (26.4)

494 (63.6)

18 (2.3)

132 (17)

>35 (n=939)

277 (29.5)

602 (64.1)

23 (2.4)

104 (11.1)

Total (n=3157)

705 (22.63)

1623 (51.4)

71 (2.2)

363 (11.5)

Table 2 : Sex wise comparison of IAP and NCHS based classifications in Vadodara urban slums Sex

Moder. To severe malnutrition (%)

Severe malnutrition (%)

IAP (gr. 2, 3&4)

NCHS (<-2 SD)

IAP (gr. 3&4) NCHS (-3 SD)

(n=1659)

280 (16.9)

871 (52.5)

16 (1.0)

192 (11.6)

Female (n=1498)

425 (25.4)

752 (50.2)

55 (3.7)

171 (11.4)

705 (22.3)

1623 (51.4)

71 (2.2)

363 (11.5)

Male

Total (n=3157)

Reprint Millions in India starving amid bumper harvests New Delhi: 'Our granaries are brimming only because people do not have the wherewithal to purchase food grain,' was how India's blunt speaking Agriculture Minister Ajit Singh summed up a piquant situation. Singh was the only ruling party politician either in the central government or in the states who chose to be candid about people dying of starvation when the government is groaning under surpluses expected to reach 80 million tonnes.

After newspapers and television showed graphic proof of starvation deaths and mass deprivation, India's Supreme Court intervened on 3 September to shame the government into ensuring that the poor received their due share of grain. Acting on a petition by the leading rights group, the People's Union of Civil Liberties (PUCL), the apex court observed that below-poverty-line (BPL) families were being doled out 25 kilogrammes of grain per month at four cents a kilogramme when they were entitled to 75 kilogrammes at the same rate. India's granaries were already bursting with more than 60 million tonnes of wheat and rice but yet another bumper crop in the current harvest is expected to take surplus stocks well beyond 80 million tonnes. With nowhere to store the grain, the government has begun to stack sacks of it on the tarmac at airfields covered under nothing more than flimsy black plastic sheeting. Vast amounts were reported to have rotted in rainy weather or been eaten by rats. But the states apparently have neither the will nor the mechanism to get the rotting grain to the starving poor, facts that did not escape the Supreme Court. In fact, the official apathy prompted the court to ask the PUCL to identify officers in each of the affected districts who have the 'spirit, inclination and drive' to prevent further starvation deaths. 'Let us target the areas which need immediate attention and Orissa appears to be in the greatest need,' the bench, consisting of Justice B N Kirpal and Justice Ashok Bhan, told the PUCL.

Besides eastern Orissa, the court has issued notices to the governments of southern Andhra Pradesh and Karnataka, western Maharashtra and Rajasthan, and central Madhya Pradesh and Chattisgarh states to report on starvation. None of these states could provide figures on the number of people who fell into the BPL category and were entitled to cheap food grain but the PUCL told the court that, in all, 200 million Indians suffered 'chronic hunger'. There are no official figures on the number of people who may have starved to death this year although it is believed that a few hundred have died in the affected states. Disaster-prone Orissa, which has a special relief commissioner (SRC), has identified the problem in the state as one of 'rapacious traders and moneylenders cornering BPL grain, leaving intended beneficiaries to grub on roots and poisonous mango seeds'. That was not surprising because even India's Planning Commission has admitted that more than 30% of food grain meant for the public distribution system (PDS) is misappropriated yearly by private traders and contractors. Two years ago, the government announced a National Storage Policy, which envisaged inviting foreign investors and modern technology to efficiently move grain from the farm gate to consumers. But that plan was shelved because the central government-run Food Corp of India (FCI), described as notoriously corrupt, and the contractors and traders it works with, did not find it convenient to their schemes. As the outrage of the Supreme Court became known, the federal Ministry of Food and Civil Supplies finally got tough and made it a punishable offence for traders diverting food grain away from the public distribution system. The phenomenon of continuing bumper harvests and overflowing granaries amidst mass-starvation has been a topic of academic discussion and newspaper reportage for at least two years now. Last April, a group of academics from the prestigious Delhi School of Economics resorted to an open letter to Prime Minister Atal Bihari Vajpayee in a bid to get the rotting surplus stocks released to poor and starving people. 'This year, with droughts affecting large parts of the country for a second or third year in a row, undernourishment and starvation are likely to intensify. Alarming cases of starvation deaths have already been reported in several states,' the April letter said. The signatories, including well-known economists like Jean Dreze, Pulin Nayak, and Badal Mukherji, argued that the stocks were a burden on the economy considering that there was no possibility of export while storage and handling costs were high. The economists pointed out that the $2.5 billion that was being spent on 'so-called food subsidy’ actually went into 'procuring, handling and storing food that does not reach the poor'. 'Bold intervention at the highest level is imperative to avert widespread deprivation and misery,' they urged in vain. 'It is shocking to see massive public resources being used to store food out of the reach of the poor,' the academics also said.

Ranjit Devraj Correspondent for Inter Press Service, with whose permission the above article has been reprinted. Also credit to Third World Network

Peasants' unhappy experience with globalisation The eradication of the Haitian Creole pig population in the 1980s is a classic parable of globalisation. Haiti's small, black, Creole pigs were at the heart of the peasant economy. An extremely hearty breed', well adapted to Haiti's climate and conditions, they ate readily-available waste products and could survive for three days without food. Eighty to 85% of rural households raised pigs; they played a key role in maintaining the fertility of the soil and constituted the primary savings bank of the peasant populations. Traditionally, a pig was sold to pay for emergencies and special occasions (funerals, marriages, illnesses) and, critically, to pay school fees and buy books for the children when school opened each year in October. In 1982, international agencies assured Haiti's peasants their pigs were sick and had to be killed (so that the illness would not spread to countries to the north). Promises were made that better pigs would replace the sick pigs. With efficiency not since seen among development projects, all of the Creole pigs were killed over a period of 13 months. Two years later, the new, 'better' pigs came from Iowa. They were so much better they required clean drinking waters (unavailable to 80% of Haiti's population), imported feed ($90 a year when the per capita income was about $130), and special" roofed pigpens. Haitian peasants quickly dubbed them 'les princes a quarter pieds' (four-footed princes). Adding insult to injury, the meat didn't taste as good. The re-population programme was a complete failure. One observer estimated that, in monetary terms, Haitian peasants lost $600 million. There was a 30% drop in enrolment in rural schools, a dramatic decline in protein consumption in rural Haiti, a devastating recapitalisation of the peasant economy, and an incalculable negative impact on Haiti's soil and agricultural productivity. Haiti's peasantry has not recovered to this day. Most of rural Haiti is still isolated from global markets, so for many peasants, the extermination of the Creole pigs was their first experience of globalisation. The experience looms large in the collective memory. Today, when peasants are told that 'economic reform' and privatisation will benefit them, they are understandably wary. The state-owned enterprises are 'sick', we are told, and they must be privatised. The peasants shake their heads and remember the Creole pigs. Note: Grassroots International [179 Boyston St., Boston, MA 02130-9901, USA, (6/7) 524-1400, www. grassrootsonline.org < http: / / www.grassrootsonline.org/> ] and Toward Freedom [P 0 Box 468, Burlington, VT 05402, USA, www.towardfreedom.com are working with Haiti's National Peasant Movement of Papaye Congress to finance the reintroduction of Creole pigs and improve veterinary services. A donation of $45 will buy a pig for a gwoupman (peasant group). Note: The award-winning film, A Pig's Tale, which documents the destruction of the Creole pig, is available from www.crowingrooster.org <http:// www.crowingrooster.org/> org <http:// www.crowingrooster.org>. Jean Bertrand-Aristide President of Haiti The above article first appeared in the magazine Toward Freedom. Credit to: Third World Network Features

Tentative Schedule of the MFC Theme Meet, Food Security and Nutrition Jan 24-25, 2002 Jan 24, 2002 9 am: Registration 10-11am: Registration and Introduction of Participants, Expectations, etc. 11 am -1 pm: Status of nutrition/malnutrition in India ... evidence from recent data. Paper Presentation by Veena Shatrughna and Discussion 2-4 pm: Presentation of Papers on Critique of Nutrition Policy and Gender. Reading of Papers of Dr Shubhada Kanani and Dr PV Kotecha and Discussion 4-6 pm: Field Reports. Presentation by Illina Sen, Vanaja Ram Prasad and others, and Discussion Exhibition by Rupantar Also Post Dinner Discussion on Papers and Issues emerging to continue post-dinner and 2nd day's programme may be modified accordingly.

Jan 25, 2002 9 am - 12 am: Plenary Presentations on Politics of Food Security/WTO, etc., (Devinder Sharma, Vandana, et al); Nutrition Interventions like PDS, ICDS, etc.; Issues in investigating and documenting under-nutrition, starvation and suspected starvation related deaths (Abhay, Amita, et al); Wages and employment and issues in nutrition; 12 noon to 4 pm: Parallel Group Discussions on the above 4-6 pm: Plenary on Issues Emerging and Future Plans for Mfc

Subscription Rates Rs.

Annual 2 years 5 years Life

U.S $ Individual

Institution

Asia

Rest of World

100 175 450 1000

200 350 925 2000

10 --100

15 --200

The Medico Friend Circle bulletin is the official publication of the MFC. Both the organisation and the Bulletin are funded solely through membership/ subscription fees and individual donations. Cheques/money orders to be sent in favour of Medico Friend Circle, directed to Manisha Gupte, 11, Archana Apartments, 163 Solapur Road, Hadapsar, Pune - 411028. (Please add Rs. 10/- for outstation cheques)

MFC Convenor's Office: S. Sridhar; ARCH, Mangrol 393145, Rajpipla Taluka, Narmada District, Gujarat. Email:sridhar.mfc@softhome.net Editorial Office: C/o Neha Madhiwalla; B3 Fariyas, 143 August Kranti Marg, Mumbai 400 036 Email: mfcbulletin@rediffmail.com

Editorial committee: Neha Madhiwalla, Sandhya Srinivasan, Meena Gopal, Tejal Barai. Editorial office: c/o Neha Madhiwalla, B3 Fariyas, 143 August Kranti Marg, Mumbai - 400 036; Published by Neha Madhiwalla for Medico Friend Circle, 11 Archana Apartments, 163 Solapur Road, Hadapsar, Pune - 411 028. Printed at Pradish Mudran, Mumbai - 400 004. Views and opinions expressed in the bulletin are those of the authors and not necessarily of the organisation.


