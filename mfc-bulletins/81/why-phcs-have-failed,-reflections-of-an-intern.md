---
title: "Why PHCs Have Failed, Reflections Of An Intern"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Why PHCs Have Failed, Reflections Of An Intern from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC081.pdf](http://www.mfcindia.org/mfcpdfs/MFC081.pdf)*

Title: Why PHCs Have Failed, Reflections Of An Intern

Authors: Swaminathan S

medico friend circle bulletin SEPTEMER1982

Low-Cost Drug Therapy (This article is the continuation of the discussion initiated by the author in his article in the August issue Editor.)

In the preceding piece I had put forward some points about high cost medicine. In this piece we will grope towards low cost drug therapy. Will the Govt. dare to do it? In India, the Hathi Committee report had suggested abolishing brand names of 13 essential drug formulations in an attempt to begin breaking vice like monopoly. Those formulations included certain commonly used pain killers, anti-anaemics, antibiotics, antihypertensive and antiparasitic drugs. In 1979 the government was reported to have decided to abolish brand names of just five drugs analgin, aspirin, ferrous sulphate, chlorpromazine and piperazine saltsbrands which are manufactured by multinationals. It carne as no surprise that the move was stoutly opposed. Not so strangely, Indian Pharmaceuticals firms also resisted it and it was quickly abandoned at least [or the time being. Sometime ago, the authorities had decided to force some semblance of sanity in the marketing of vitamins in India by permitting the sale of only two categories of formulations termed 'therapeutic' and 'prophylactic.' Accordingly multi- vitamin manufacturers in India are reported to have submitted their new price lists for the two categories to the Ministry of Petroleum and chemicals. The ministry was supposed to grant price approvals by Dec., 1978. For reasons best known to itself, the deadline was progressively postponed from then to July 1979, then to Sept., 1979 and finally to Dec, 1979.

Since then, however, the Ministry has maintained a mysterious silence over the whole subject prompting doubts that the whole matter has been· indefinitely shelved. A study of the Economic Times a while ago revealed that there were 68 companies that would be affected by new legislation, of which as many as 24 were multinationals. The 571 multivitamin formulations that have flooded Indian market, almost all would have to be reformulated, at a cost of Rs. I. 5 crores to the manufacturers. Enough reason, surely, to delay things as far as possible! Why does this happen? Dependence on multinationals for essential minimum drugs. Pattern of drug production is irrational and irrelevant to our health needs e.g. for Anti T. B. & leprosy drugs even half the requirement is not met. D. D. S. remains non available for 6 months. For many drugs installed capacity is far less than licenced capacity (50 % for insulin). Production of non-essential items like tonics exceeds licenced capacity. Primaquin and trimethoprim (for malaria) are not produced locally at all and imports of chloroquin exceed the production. Vaccine against influenza, mumps, measles and polio are not produced in India at all. What does it mean? We lack political will. Will the voluntary sector take the lead? Can the voluntary institutionsAdopt a policy of prescribing by generic names only? Build up a policy for dealing with persuasive pressure tactics of sales representatives?

Ulhas Jajoo Bajaj Wadi. Wardha

limit its formulary to the essential minimum drugs, in accordance with, Hathi Committee list giving priority to generic drugs? Adopt a policy of preparing some drug preparation e.g. Ointments, Syrups, drops, IV fluids, tablets? Develop a social audit system, analysing say prescribing practices, treatment cost analysis, hospital stay etc.? Lay down local standards for prescribing and "investigating common illnesses? Develop extensive peripheral health care network, draining the central hospital?

medical practitioner is to study the cost of a drug available in different brand names and then select one for use which is cheapest and also qualitatively expected to be good. Drug industry’s propaganda about the bioavailability of the product need not be accepted blindly until there is a scientific proof to support the claim. iii) The form in which drug is given: The drug available in form of an injection or available in syrupy form is much more costly than the same amount of drug available, as tablet or capsules. For example, a) A child if it can swallow the powdered drug, syrupy base can be avoided. A tablet or a capsule can be broken, and the

Does a conscientised health personnel have any scope?

powder can be mixed with honey or sugar juice to mask the

Will a medical practitioner—

bitter taste. Please avoid giving uncrushed tab1eisjcapsules to a

Openly denounce the drug industry's selfish motives?

child lest it acts as a foreign body.

Keep away from persuasive practices of drug industry, i.e. Gifts, research finance to scholar’s money for medical

b) Avoid injections if the tablet/capsule can be swallowed or can be put through Ryle's tube in the hospital.

c) Eye applicaps are much cheaper than the ointments

seminars, etc. Have an open mind towards other forms of treatment-seek information be willing to incorporate them. Think and believe that he is a consumer first and then

available in tube. Each eye applicaps (e.g. chloromycetin)

prescribe? Seek out the consumer movement in his/her areas and join hands with them in their fight or justice?

iv) Use of simple drugs (home remedies):

can be used for total 6 applications if it is opened up for the tail end of the tip. Some Home remedies arc accessible, cheap and provide a symptomatic relief. These remedies should be encouraged for the symptomatic treatment. e. g, 'Harda' or 'Jesthamad' as silogogues to suppress

I believe that a conscientious physician can reduce the cost of drug therapy considerable by observing certain rules for oneself.

irrigative upper respiratory cough. 'Tulsi kada’ or ‘Haldi’ for fomentation of a red throat.

When and if he is convinced about the need to give drugs, the following points should be kept in mindThe considerations which should guide the selection of appropriate drugs or the drug preparations are:-

i)

Cost of the drug: Allopathic medicines, clue to the selling game of the drug industry, are beyond the capacity of the needy. Choice of the cheapest

brand; select ion of generically available drug as much as possible; and preparation of some commonly used drug preparations in one's own dispensary; are the only ways which a conscientised medical practitioner can follow in the existing structure.

ii)

Quality of drug: Drug market is teeming corruption. A nongenuine drug selection must be avoided. Practical solution for a lone

v) Integration with other pathies. One must realise the limitations of allopathic science today. Allopathic drugs have offered a use for infective diseases, but for the vast majority of other problems it still remains a palliative help. Homeopathy science particularly claims better results for allergic conditions. A controlled study of such remedies from other pathies is worth undertaking. Preparation of some common drugs:A. Skin ointments: The skin ointments can be prepared in small amounts daily by using butter, Dalda, cream, paraffin or coconut oil as a base. The locally available base is particularly preferred because it is much cheaper and is not required to be purchased in the bulk (see table appended On P. 7).

KEEPING TRACK- II (This column has been started from last month to enable readers to keep track of important thoughts currents, ideas in the field of analysis of health care systems- Editor.)

Medical Hubris—a Reply to Ivan Illich

Limits to Medicine- Medical Nemesis. The Expropriation of Health.

critique of Illich's book. Horrobin does not dispute the facts

Ivan Illich, Penguin Books (Pelican 1977.) The foremost critic of trends in modern Medical Practice, Illich presents thought-provoking evidence that “the medical establishment has become a major threat to health and the disabling impact of professional control over medicine has reached the proportions of an epidemic". Discussing Iatrogenesis in great detail, Illich makes one of the most forthright pleas for 'demystification of medical matters' and exhorts lay people to reclaim greater autonomy over health decision making. He writes that "A professional and physician-based health care system that has grown beyond critical bounds is sickening for three reasons. It must produce clinical damage that outweighs its potential benefits, ·it cannot but enhance even as it obscures the political conditions that render society unhealthy; and it tends to mystify and expropriate the power of the individual to heal himself and to shape his or her own environment. The medical and paramedical monopoly over hygienic methodology and technology is a glaring example of the political misuse of scientific achievement to strengthen industrial rather than personal growth". The book is divided into four parts and deals with Clinical Iatrogenesis in Part I, Social Iatrogenesis (medicalisation of life) in Part II, Cultural Iatrogenesis (disabling impact of medical ideology on personal stamina) in Part III and The Politics of Health in Part IV. Interestingly Illich warns that ‘if contemporary medicine aims at making it unnecessary for people to feel or to heal, eco-medicine promises to meet their alienated desire for a plastic womb". He also warns that gullible patients should not be relieved of the blame for their therapeutic greed by making physicians scapegoats. Health must be seen as a virtue, as a right and people must be involved in “political action reinforcing an ethical awakening-that will limit medical therapies because they (the people) want to conserve their opportunities and powers to heal”. A thought provoking book to he read by all MFC members,

David Horrobin, Churchill Livingstone, 1978. This book should be read after the earlier one since it is the first serious presented by Illich, but disputes his interpretation. In spite 0 all the inaccuracies and exaggerations in Illich' s books that he attempts to point out, he concedes that Illich's first sentence" The medical establishment has become a major threat to health is right and that this book could prove to be one of the key medical documents of the second half of the twentieth century". In a very open and level headed assessment of the criticisms of Modern Medical Practice, the author gives his own tentative suggestions to bring about a change in this situation. He makes a plea fora) More "Science" in medicine to eliminate the errors encouraged by warm emotion that ‘to do something must always be better than to do nothing'; b) Less use of technology by subjecting them to stricter control to determine whether they really benefit the patient; (Continued on page-G) Low-Cost Medicine (contd.) B. Eye-Ear Drops: Can be prepared with lesser cost if the sterilised salt of the drug (available in the market in sterilised form as injections) is dissolved in appropriate amount of distilled water for fresh use (See table appended on page no 7.) These solutions ointments should be prepared in small quantities so that they are not required to be stored for a long time.

References: 1. “Insult or Injury." Charles Medawar. Social Audit. 2. Drug Dis-information: Charles Medawar, Social Audit. 3. Health for the millions: April-- June 1981 Issue. 4. Science today: Sept., 1981. 5. Therapeutic Guide lines. African Medical & Research Foundation 6. Drugging

the

Indian:

Shivanand

Karkal

"DEBONAIR July 1980".

**

in

Why PHCs have failed Reflections of an intern Primary Health Centre is a post-independence phenomenon consistent with the stated objectives of successive governments namely-assuring the rural masses of a minimum health facilities. While the working of these P.H.C.s is subject to frequent audit and inquiry by govt. bodies, no independent survey has been conducted to analyse the human aspects of the issue aspects which can be glossed over by cleverly displayed statistics, making deaths and disease seem like distant facts. There is an unhealthy preoccupation among health administrators and politicians to evaluate the success of a programme in terms of the money and resources expended rather than the results achieved. This attitude has led to a classic example of over expectation and under achievement. If an objective analysis is to be made in terms of standards of community health-Neonatal/infant child mortality, morbidity indices, effectiveness of public health measures in the control of epidemics etc. then the only conclusion can be-that the setting up of P.H.C.s and their functioning have been a miserable failure, at no time reaching the targeted goals. The only fields where a respectable degree of success has been achieved are those of mass immunization programmes, maternal health and family planning. Even here especially as far as immunization programmes are concerned, the success is in most part due to the large scale participation of voluntary and international agencies. As for family planning the programme can be called a success only as far as meeting of targets is concerned. The enormous expenditure of men and materials is such that the cost effectiveness of the whole programme would be questionable. What are the factors that have inhibited the functioning of P.H.C.s in the expected manner? This I call say with the limited experience I gained during a couple of months of rural internship. Unmotivated staff P.H.C.s are isolated units of health care. Interaction with each other is nonexistent while their only connection with the nearest hospitals is

through periodic visits. The working staffs of the P. H. C. have been traditionally arranged in the form of a pyramid, the doctors coming at the top. Doctors are extremely reluctant to be posted at P.H.C.s for it is literally a professional dead end. There is a fear of sophisticate skills becoming rusty. Also, a fear of an academic fade out due to absence of the type of stimulating atmosphere that one feels one obtains in city hospitals and urban practice. Another sorepoint for the P.H.C. doctors is the unsatisfactory working conditions, lack of adequate staff and equipment, primitive living quarters and a salary that hardly compensates for the troubles that have to be suffered. Very few among the staff may report for duty regularly. Even those staffs who do report for duty do so at their convenience. All this goes on with the full connivance of an indifferent medical officer who is rarely exemplary as far as performance of his own duties arc concerned.

USELESS EQUIPMENTS AND DRUGS

All this is not helped by the fact that equipment and drugs available at the average P. H. C. are of spartan nature. Refrigerators do not work (these are often gifts made by W. H. O., UNICEF etc.), sterilisers do not exist, syringes and needles are of such poor quality that most of the injectable spills out, bandages & dressings arc dirty & obviously unsterile, catheters are inheard of (I haven't seen one in the setting of a P.H.C.), microscopes are used solely for the purpose of the malaria programme & are not available for TC/DC, urine exam. stool exam etc. (this list of absent or malfunctioning equipment can be extended endlessly). As for the drugs stock-it is laughably inadequate. The only drugs available with any degree of abundance and consistency are the therapeutically insignificant ones- sulfadimidine tabs, multivitamins, A. P. C. (God only knows why this combination is still allowed), antidiarrhoeals (either-Diphenoxylate or phthaly sulfathiazole), inj Tetracycline, inj B 12, antituberculosis drugs is usually the entire inventory of drugs available. With this much of drugs & equipment the examination & treatment of patients even by the most idealistic doctor rapidly degenerates into a hypocritical farce. A long & disorderly I

queue of patients await attention as an overworked & impatient doctor performs a cursory & indifferent examination dishing out prescriptions empirically (a permutation and combinations of: the limited choice available). The notion of the patient that capsule is better than a tablet and an injection is a cure all is encouraged by the doctor who caters to these notions by hastening to prescribe an injection (in such a manner litres & litres of cyanocobalamin are poured into patients ). At best the P.H.C.s can be described as pill dispensing machines, tonnes and tonnes of MVT, SDT & APC being unloaded on a public that cannot afford a reasonable alternative. Thus it would be nothing but foolishness to imagine that these P.H.C.s are serving the health needs of the rural population. Like the drugs they dispense they are merely placebos for the politician who have to show 'results'. The' results' being buildings, staff, & equipment not the health of the community.

IS THERE AN ALTERNATIVE?

Based on our experience with P.H.C.s we must conclude that the whole concept ill the present form of implementation has been an utter failure. P.H.C.s have been conceived as outposts of medical care- an attempt being made to take them to the very doorsteps of the people. To this end they have been burdened with a complete list of programmes, targets & quotas which they are not suited to carry out due to the lack of matching infrastructure ( in the form of drugs, equipment & maintenance). As compared to P.H.C.s, small hospitals may serve the needs in a better fashion. With the present limitations, the only alternative to P.H.C.s in my opinion would be to open small fully equipped hospitals in rural areas with adequate complement or speciality staff (surgery, 'medicine, Dental, ENT, opth., obstetrics & gynaecology, pediatrics). One such 'Rural Mini Hospital can be opened to serve the 7-8 lakh population previously catered to by 10 P.H.C.s. This concentration may lead to better standards, attraction of better medical talent to rural centres, reduction of wastage & inefficiency (that arc inevitable in a larger operation) and better utilization of available infrastructure. P.H.C.s can he replaced by primary health workers. The primary health care should be the responsibility of P.H.W.s; for, after all

the standard of medical care presently available in P.H.C.s can be adequately & more efficiently delivered by well trained P.H.W.s. Health care in the country is at cross roads. Any meaningful solution can result only from a fuller understanding of the issues involved by the concerned authorities. Enlightened opinion from the medical community can change the deplorable state of affairs. Such involvement and participation has been strangely' lacking. Until then the present farcical set up will continue unchanged by health administrators seeking shortcuts & make do's. S. Swaminathan

Stanley Hospital, Madras 6.

Educational Campaign Against Diarrhoea In Pune all the leading dailies have published articles on diarrhoea written by members of the Lok-VidnyanSanghatana. I have prepared a small 12 page pamphlet in Marathi for LVS on diarrhoea and its management. 1000 copies have been rapidly sold out. The L VS has made a pictorial exhibition on diarrhoea and oral rehydration which is being shown in various bastis. The pamphlet has been translated into Gujarathi by Rashmi Kapadia and his friends. A small cyclostyled note on how to give oral rehydration has been sent to all the district news-papers in Maharashtra by Arogya Dakshata Mandal in Pune. It was published by many news-papers. This Mandal is also preparing in a Marathi pamphlet on diarrhoea and its management. Dhruv Mankad and his colleagues in Nipani distributed a one page instruction sheet during a recent out break in Nipani on how to give oral rehydration. Dhruv has also prepared a "Dear doctor I chemist" letter on diarrhoea like the one on Oestrogen-progesterone forte. Vimal Balsubramanyam has written an article on diarrhoea in the Daily - Telegraph of Calcutta. Articles written by other friends have as yet not been published. Those who want a copy of the-back-ground paper prepared by Dilip Joshi can still write to me along with a money-order of Rs. 5/The next issue will contain a report on the decisions made at the Jaipur meet and at the MFC Executive Committee meet at Tilonia —Anant Phadke

DEAR FRIEND T do not wish to disagree with the editorial comments (July 82) about prevention or control of tuberculosis. But your readers may be interested to know the experience of our health project at Chimer in Uran Taluka (Raigad District) during the last eight years. Apart from other health interventions T. B. diagnostic programme has been an important part of our efforts in the area. The first major diagnostic camp was in May 1976 and several others were conducted ill the following five years, last one in 1930. Our method was house to house survey by village volunteers and later CHVs and screening by Maharashtra State Anti-T. B. Association. About 120 cases have been diagnosed during this time. As patients have tended to come from variable distances it is not easy to give incidence figures although an attempt was made by noting down the patients' addresses. I will comment only on the situation in Chimer (Population 3,800) about which our knowledge is most complete and not comment on the surrounding population which has also benefited to some extent from our efforts. We were able to detect 10 patients/1000 populations in Chimer. With the help of the local Sarpanch and other leaders in the village as well as the Community Health Volunteers appointed by the Government in 1978 we have tried to ensure that every diagnosed person takes the treatment and I can say that we have succeeded in a significant way in improving compliance. We visit Chimer only in a fortnight. Our impressions are based on the patients that come to us (apart from the several house to house surveys-last in 1980). But the number of patients with tuberculosis that have come to our notice has come clown dramatically. While a fresh case was seen in 1975 and 1976 at least once a month, in the last two years 1980 and 1981 (same is true of 1982) there have been only one or two cases arising among the local population in the whole year. Only new cases were among those who had gone to live in Thana, Panvel or Bombay for a job and had come back to Chimer for rest after they were diagnosed. During these eight rears the economic condition has improved, but

only marginally. In 1977 an economic survey conducted by Mrs. C. K. Dalaya of Economics, Department of Ruia College, Bombay indicated that per capita GNP of Chimer was about 1/3rd or the GNP of India at the time.' We do not yet have a proper road nor a resident qualified doctor. But prevalence of tuberculosis in the village seems to have reduced. Although it is conventional wisdom to say that tuberculosis is a socio-economic disease if you can reduce the number or patients who go round infecting others, there is no doubt at all that prevalence of the disease will reduce substantially. 1 his is quite easy to achieve in small communities if you can win the co-operation of all the people. In larger cities it is a Herculean task. We can say with reasonable confidence that the new born babies in Chirmer are not exposed to the tubercular germ in the air, although they come in contact with several hundred people every day. Although they continue to live in small huts and houses, with insufficient ventilation they seem to escape tuberculosis. From this experience I have come to believe that it is not impossible for us to get rid of tuberculosis even before we are rid of poverty. V C. Talwaker Bombay-R

Keeping Track-If continued from page -3 c) Attempts to be made to keep medical institutions as small as possible and only for those who strictly need them; d) Asses is professional training and prescribe levels of training actually required to enable people to do jobs effectively and cut out unjustifiable part of courses; c) Challenge the discrepancy between the high ideals which doctors often profess and their personal life styles and ensure that the profession should be more humane and less a 'certain road to wealth and security' so that the rightly motivated people are attracted to it. These changes should be made at four main levels: of the individual doctor, of the organisation of the profession, of the relationship between govern merit and medicine and medicine related industries, and of the medical school. A book which puts Illich's criticism in proper perspective. *

*

*

Drug of Choice

Market Preparation

Cost in How to prepare Cost in Rupees_______________________ _________ _______ Rupees

Betamethasone 0.125

Betnovate

4.10/5gm

or Dexamethasone

(Glaxo)

A. Ointments 1. Corticosteroid

0.125% 2. Antibiotic

a) Neomycin 1%

b) Terramycin 1%

Neomycin sulphate 0.5% (archem Aristo) Terramycin 3% (Pfizer)

1.35/10mg.

Betnovate-N (Glaxo)

4.24/5g01.

1.76/5gm.

Betamethasone 1.25% + Neomycin l%

4.Hydroxyquinoline

Quninidochlor 3% Vioform cream or (CIBA) Di-ioclohyclroxyquinoline3% Betamethasolle1.25% Betnovate-C + (Glaxo) Quninidochlor 3%

1.97/20gm.

Salicylic acid 10%) Sulphur 10%

Mycozol (Parke & Davis) Eskamel

2.10/28.gm.

i) Corticosteroid

Dexamethasone 1%

Betnesol eye drop (Glaxo)

4.10/3ml

ii) Antibiotic eye drops

Chloromycetin

Vanmycetin 4% (Fairdeal)

2.29/5ml

6. Keratolytics 7. Sulphur

1.44+ 0.10

1.54/5gm

0.30 + 0.20

0.50/10gm

0.8 + 0.10

0.18/5gm

I TSF of base

3.Corticosteroid+ Antibiotic

5.Corticosteroid+ Hydroxyqinoline

12 tabs. Of Betamethasone powdered (Paran) +

4.24/5gm.

4.00/28gm.

1/3 cap. of neomycin (Unichem 350 mg) + 2 TSF of base 1/5 cap. tetracycline (Paran) + 1 TSF base. 12 tabs Betamethasone powdered (Paran) + 1/6 cap of neomycin (Unichem 350 gm) + 1 TSF base 2 tabs of enterovioform (250 mg CIBA) + 4 TSF base 12 Tabs Betamethasone (Paran) + 1/2 Tab Enterovioform + TSF of base 3gm salicylic acid + 6 TSF of base 3 gm Sulphur + 6 TSF of base

1.44-t" 0.15+ 1.10

1.69/5gm

0.20+ 0.40

0.60/20gm

1.44+ 0.05 1.10

2.59/ 5gm.

0.30+ 0.50 0.30+ 0.60

0.90/30gm 0.90/30gm

B. Topical Eye Drugs:

ii) Corticosteroid + Antibiotics

1%

Dexamethasone 1% Chloromycetin 1 %

Pyrimone (Fairdeal)

5.43/5 ml

Dexamethasone vial (Bicohem) + Add distilled water to make 8 milliliters Inj. Chloromycetin in powder form (Biochem) 0.5 gm + 5 cc. distilled water Corticosteroid eye drop

4.00 0.35

4.35/8ml

0.30+

0.50/ml

0.20

5 ml

4.00

80 mg. chloromycetin powder dissolved in water

0.50

water to take 8 cc

0.35

4.85/8 ml

+

FROM THE- EDITOR'S DESK Among the' various engagements the Prime Minister was to fulfill during her recent visit to the U. S., one was to meet the Indian Scientists working there. This, according to the newspaper reports, was to find and evolve ways to bring back the scientists to work in India. Whoever advises the Prime Minister on such matters, seem to harbours the misconception that what primarily ails our Science and Technology is the loss of talent quantitative and qualitative. Be that as it may, the newspaper report started a different chain of thoughts in my mind. Does the term scientist include the medical doctors? Are any attempts made to bring back the doctors too? When the issue of brain drain is discussed, somethings remain unsaid. But they are there, in between the lines, if one wishes to read them. It seems to be implied that our research laboratories are so equipped and maintained that they can utilise the advanced training acquired abroad; our hospitals are too ill-equipped to do so. This is true. Marry of our research laboratories have good funds, suffer less redtapisom and beaurocratic pressures. The workers in the research Institutes have greater freedom. This is not true of our hospitals. Concerned, or ever perhaps obsessed with the urbanrural disparities, MFC has more or less neglected the urban health care system. In reality, our urban and teaching hospitals are nothing to be proud of. In a comparative analysis of funds, personnel and equipment available in research institutes and urban hospitals, the latter fare very poorly. Although the term' Scientists' includes people in various fields and research institutes deal with

Editorial Committee: Anant Phadke

Christa Manjrekar Ravi Narayan Ulhas Jajoo Kamala Jayarao EDITOR

equally varied fields, as individuals concerned with health care we may look only into the medical field. The analysis may hold good for other fields too. Research institutes are set up and· exist at public expense. They are not there to help individual scientists to further their own interests. They are supposed to find solutions for the country's problems, and thus aid progress of the nation. The solutions worked out have to be implemented in the field. In this case, the term field also includes hospitals. If, therefore, the hospitals have no facilities, are ill-equipped and ill-staffed, how will the efforts of the research workers bear true fruit? Secondly, while on the one hand we wish our scientists and technologists to return home, what are we doing with the doctors? We are actively encouraging the brain-drain to the Middle-East and Africa-all in name of helping the developing, less developed, least developed, undeveloped countries. What happens to development in the health and medical fields in our own country?' Those going to such countries and working in their district and rural hospitals, mind you, are not just fresh graduates our professors and senior lecturers are to be found there. The MFC has been exercised about setting up of superspeciality units in teaching hospitals. But let us remember, that such hospitals are very few. The majority of the urban hospitals are also in a deplorable state. Let us not forget or ignore this. Trying to attract workers back to our research laboratories but actively draining out the medical specialists seems, to me, another glaring instance of wrong planning. Kamala Jaya Rao

**


