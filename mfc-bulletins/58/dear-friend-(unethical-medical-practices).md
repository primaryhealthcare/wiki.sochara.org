---
title: "Dear Friend (Unethical Medical Practices)"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Dear Friend (Unethical Medical Practices) from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC058.pdf](http://www.mfcindia.org/mfcpdfs/MFC058.pdf)*

Title: Dear Friend (Unethical Medical Practices)

Authors: Punwani Dushyant

Medico friend circle bulletin OCTOBER 1980

Kerala: A Health Yardstick for India Kamala S. Jaya Rao National Institute of Nutrition, Hyderabad This is based on an article published by P. G. K. Panikar, Institute for Development Studies, Trivandrum (1). The paper shows that judged in terms of conventional health indices, Kerala stands out from the rest of India. The paper tries to analyse the possible reasons for this. It shows that,' given proper policies and priorities, lack of resources need not be an impediment to improvement of health status. Following the presentation of Panikar's data, I have tried to analyse the nutritional status of Kerala's children and its role in child mortality. The Crude Death Rate, Infant Mortality Rate (IMR) and Toddler Mortality Rate are considered to be important indicators of the general health status of a community. With lack of protected water, poor sanitation and inadequate medical care, morbidity and mortality in rural areas are high. Therefore, the level of health indicators of the rural population would give a more accurate picture of the health status of a country like India. National averages not only mask the wide variations between states but also mask the differences between the rural and urban population. The Crude Death Rate in India is 16 per 1000; it ranges from 8'5 in Kerala to 22'0 in U. P. The rate among the rural population is higher, sometimes almost twice that of the urban population (Table 1). The difference is hardly much in Kerala. IMR in India is about 120 per 1000 live births. In Kerala it is only 64. Although this is much higher than in developed countries, it is the lowest in India, U. P. has the highest IMR of 179 (Table 2)

The mortality rate among children under five years is distressingly high in India. It ranges from 24 in Kerala to 86.4 in U. P., with a national average of 62 per 1000 (Table 2). According to Panikar, if the under- five death rate for the whole country were to drop to 24 (the rate in Kerala) the crude death rate will come down from 16 to 12. This argument may not be completely correct. Though the under-five death rate is low, as can be seen from the Table, the proportion of deaths in this age group compared to total deaths is still very high. It appears that the rate bas fallen down because the crude death rate has come down.

Table — 1

Crude Death Rates in India Rural

Urban

Total

Kerala

8.6

74

8.5

Punjab Haryana Karnataka Maharashtra Gujarat Bihar A. P. Tamil Nadu Rajasthan Assam Orissa M. P. U. P.

12.2 12.9 13.4 13.7 16.0 16.0 16.9 17.0 17.3 17.7 18.4 18.6 23.0

8.9 8.5 7.7 9.2 11.4 11.2 10.5 8.7 9.1 9.7 11.3 10.9 13.5

11.5 12.2 11.8 12.3 14.8 15.5 15.8 14.5 15.8 17.0 17.9 17.5 21.8

All India

17.3

98

15.7

If the crude death rate in the country were to come down to 8.5, under-five death rate will come down to 28 to 62. Therefore, the fall in childhood mortality appear to be due to general improvement of conditions. In Kuala and not particularly due to improvement of child health.

57% of Kerala’s hospitals were in rural areas. In most other states this was only 22-38% (Table 3). But, the mere presence of hospitals in rural areas does not bring down death rates. The proportion of rural hospitals is equally high in Orissa and Tamil Nadu but the death rates are nearly double.

Table-2

Panikar points out the obvious - that more than the availability of health services, it is the extent of their utilisation that really matters. According to him the success of Kerala is due to the fact that as much emphasis is given to promotive and preventive measures (such as provision of protected water, massive campaigns against communicable diseases, public health education using all media of mass communication, etc.) as to curative medicine. Good antenatal care, a high proportion of institutional deliveries 'and Infant 'care through home' visits by ANMs have contributed to the low maternal and infant mortality. Other important measures are triple vaccination, distribution of iron-folic acid tablets to pregnant mothers and antenatal immunisation against tetanus. While the female population of Kerala is 4% of the total population in India, maternal deaths In Kerala account for only 1% of total maternal deaths in the country.

Child Mortality in India (Rural) Infant-Mortality

Underfive mortality

Rate

Rate Percent of per 1000 Total Deaths

Kerala

61.4

24.0

39.0

Haryana Punjab Maharashtra Karnataka Tamil Nadu A. P. Orissa Assam

79.0 99.1 101.2 102.9 112.5 118.9 133.7 137.9 141.4

34.2 37.0 45.1 464 537 47.1 59.6 46.7

50.9 46.1 41.7 47.5 42.8 434 -492

-59.5

M. P. Gujarat

146.8

62.5 71.9

Bihar Rajasthan

152.4 162.6

366 77.5

-30.4

U. P.

1790

86.4

59.2

All India

138.3

61.7

53.2

The expectation of life at birth In India is 50 years for males and 49 for females. In Kerala it is 61 and 62 years, respectively. Note the longer life expectancy of females in Kerala.

Kerala is one of the economically backward states in India (Table 3). Yet; 10%of Kerala's total expenditure is on medical and health services. In most other states it is 68%. There are, however, some states where the expenditure is higher, despite which mortality rates are high. The doctor-population ratio and bed-population ratio in Kerala are also not the highest in the country (Table 3). What makes Kerala’s achievement in the health field particularly significant is that the rural population enjoys a much better health status than even the urban population in several states.

Panikar argues that the health consciousness of the public is an important factor in the utilisation of health programmes. Kerala's literacy rate is as high as 60% compared to the all India average of 30%. In 11 States it is less than 30 and in l0 others it ranges from 30-40 Rural literacy in Kerala is 59, in 10 States less than 25 in and 10 others between 26-32; More significantly, female literacy is very high in Kerala. Rural female literacy is 53%. In 20 States, it is less than 20; in U.P., M.P., Rajasthan and Bihar it is 4-7 %. A part from governmental efforts, the matrilineal system of inheritance might have significantly aided rural female literacy In Kerala. Panikar suggests, perhaps rightly, that the high general literacy and education of the females may have contributed most to the improvement of the health of Infants and children In Kerala. Though childhood mortality is the lowest in Kerala it is still very high. Also while the under-fives form only 15% of the total population, deaths in this group, as mentioned earlier account for nearly 40% of total deaths. Why is child mortality still very high in a state with an otherwise impressive health record?

Table-3

Measures of Medical Facilities in India

Per Capita State Domestic Product (Rs)

Per capita Expenditure on Health (Rs)

Population served by one doctor

Punjab

1190

8.32

5863

77

10

Haryana

924

8 67

-

56

-

Maharashtra

839

885

2592

68

4

Bengal

814

8.81

1747

90

32

Gujarat

791

9.28

4900

43

2Z

Tamil Nadu

691

8.65

1988

70

61

A. P.

609

9.09

4922

45

26

Kerala

573

6.93

4742

92

51

U. P.

573

7.88

7672

39

35,

Karnataka

554

7.85

5300

8.5

2J

Rajasthan

554

11.06

12662

51

39

Assam

554

9 09

3139

44

27

M. P.

530

10.62

21663

38

28

Orissa

511

8.24

7008

38

57

Bihar

443

6.61

6083

26

27

Two factors mainly govern childhood mortality in developing countries - infections and nutrition. Unfortunately, I am unable to get· a state-wise break-up of the incidence of infections in Indian children. I have, therefore, considered the data available for the whole population regarding the two most common childhood aliments, namely, diarrheas and respiratory diseases. The incidence figures in Kerala are much lower than in other States (Table.4) When it comes to nutrition, Kerala is in very bad shape. It has one of the lowest energy and food intakes in the country. While the mean calorie intake of adults in other States is 2000 or more, in Kerala it is around 1,800. Kerala also has the lowest intakes of iron, Vitamin A and Vitamin B-Complex. Consequently, the proportion of individuals receiving

Bed’s 100 Population

Percent Hospitals In rural areas

other states. Further, the incidence of protein-calorie malnutrition is also lower than in many States. Thus, though food intakes are very low in Kerala,' the growth and nutritional status of the children is Dot any worse than in other States and perhaps even better. How does one explain this paradox? In most States the proportion of adults getting adequate food energy is much higher than the proportion of children getting enough energy (Table 5). This means that food is eaten preferentially by adults, perhaps the wage earners. In Kerala, the picture is reversed showing a better distribution of food within the family Perhaps, female literacy is again responsible for this. It would appear that malnutrition may be greatly responsible for the high childhood mortality in Kerala. Kerala is a poor state with high rates of unemployment and underemployment. Thus poverty may be

the major operative factor in the high childhood mortality. It alls appears that even in conditions of poverty education of the women may help In Improving the' nutritional status and health of the children. Perhaps Kerala is the right place to organise and experiment with supplementary feeding programmes. In other states, apart from poverty, infections, inadequate rural health services ignorance and maldistribution of food within the family appear also to play important role in the high childhood mortality. The experience of Kerala shows that even with limited financial resources, proper health measures and improved rural and female literacy are possible and that these may aid improving child health and child nutrition. Kerala can therefore be considered a yardstick for judging health status in the country. References1) P. O. Panikar, Economic and Political Weekly, 14: 1803, 1979.

Tacle-4

Deaths by Diseases (Per cent of total Deaths) Diarrhoea Dysentery Cholera

Respiratory Diseases

Fever

Total

Kerala Gujarat Tamil Nadu Maharashtra Karnataka

2.2 1.6 5.0 4.4 4.1

9.1 4.4 8.8 15.5 9.8

9.3 24.0 22.2 16.8 27.5

20.6 30.0 36.0 36.7 41.4

A. P. U. P.

4.0 5.8

7.4 11.7

30.9 25.1

42.3 42.6

Assam Punjab Bihar Haryana

12.1 1.3 3.4 1.5

10.1 12.0 2.2 14.1

26.0 500 57.0 57.5

48.2 63.3 62.6 73.0

M. P.

5.4

11.4

56.0

72.8

Orissa

7.1

2.5

64.7

74.3

All India

4.6

9.2

32.9

46.7

Note: The figures should not be taken as absolute because errors in reporting are known. This is only to show the trend.

**

Table-5

Nutritional Status of Children Per cent adequate in energy Adults " Children

Per cent children with normal growth

Per cent incidence of PCM

Karnataka * *

75.3

614

9.9

9.2

Andhra Pradesh

69.2

33.3

153

5.1

Gujarat

62.1

53.6

10.1

6.1

Maharashtra

61 0

35.5

8.9

6.8

Tamil Nadu

55.6

41.7

14.9

1.4

Orissa

52.7

-

14.5

0.9

Madhya Pradesh

50.8

48.0

11.7

0.5

West Bengal

50.3

34.4

11.0

3.7

Uttar Pradesh

50.3

33.0

18.8

5.0

Kerala

10.8

21.9

28.0

0.7

* These figures should not be considered absolute, since there are some different calculations involved. This is to show the trends. ** Data available for only these States.

LEARNING FROM THE SAVAR PROJECT Abhay Bang (The last issue contained a brief overview of the Savarproject. This one gives a more detailed information from close quarters and raises very Interesting questions.) As the car was passing from Dacca airport to the Gonoshasthaya Kendra of Savar, the landscape of Bangladesh was unfolding before me. A decade ago while working as a medical volunteer in the refugee camps during the liberation war I had seen a few glimpses of Bangladesh by occasional infiltration. My Interest in Bangladesh dates back to that experience. The news of political upheavals and natural disasters kept on disturbing one about Bangladesh for last one decade, but at the same time some interesting, rather sensational news of the community health work started in Bangladesh by a group of young doctors led by Dr. Zafrullah Choudhury, their paramedic programme, paramedics doing tubectomies etc. bad created a curiosity in my mind. And here was I today beading towards the famous Gonoshasthaya Kendra (G. K.) passing through the main land of Bangladesh, seeing both her beauty and ugliness.

Beautiful and Ugly Beautiful because of the natural greenery and abundance of water. Ugly because of the poverty, the worst I have ever seen. The per capita >early income for Bangladesh is 560 Rs; one of the lowest in the world. There is gross disparity even in this small average income and the lower 50% of the population bas per capita yearly income of Rs. 225 or less. Population density in this country of 85 millions is one of the highest in the world (1375 persons per sq. rode). 91 % of the population lives in the rural area. 50% of the total population has either no land or less than half acre of land. Literacy rate is 20 %' but for women it is less than 10%. A passage from Dacca to G. K. offers sights of sharp contrast; tall buildings of American architect mushrooming on the expansive periphery of Dacca juxtaposed to and even engulfing the collapsing huts of the surrounding villages. It must be mentioned here that the most vulgar display of affluence is not only by the Government but also by the plethora of ‘aid’ agencies UN, World Food, US Aid and so on. But as we moved away form Dacca the poverty of the rural area started showing and soon the car turned

to the right to enter into the headquarters of G. K. The first to strike you are the buildings-two-storey hospital cum office building and a four- storey hostel residence for paramedic and other staff of the G. K. Total buildings cost in GK is 9 lac Rs. "Was it so essential? " one starts questioning in the mind. But same is the feeling of Zafrullah Choudhury, who later on said, “For Initial 1 year we were living in tents and temporary shades. Had no money for buildings. An armed robbery, heavy rains and Inconvenience to the patients and the staff created a need for buildings. Therefore, when we received generous foreign aid offers for buildings, we ware enamoured. We did the mistake of accepting the offer and within next two years these incongruous edifices steed up”. But what is more impressive is the simplicity and the austerity of the living style of the staff and the equality in relationship. I must admit that when I met Choudhuri for the first time, I mistook him for a PA or typist of Zafrullah. Except for a few families with children, all other workers live in the same building in similar accommodation. From the gate keeper to Zafrullah, all take the same, very ordinary food in a common mess. G. K. bas a novel rulereminding me of Gandhiji's Ashram In his time (not, now)everybody in the project works' for 1½ hour in the morning on the farm. "This not only helps us to become self sufficient in our food requirements, but also bund up a healthy equal relationship among us, an identification with the manual labourers of rural areas and also helps to screen and eliminate the elitists among the new recruits". All these things must have contributed in the creation of the warm, friendly and family relationship which exists in the whole team of G. K. I shall not describe the history and all the activities of G. K. as these things have already been published in the MFC Bulletin (Paramedic of Savar: issue No 57) Instead, after a brief description of the activities, I shall try to discuss some questions and inferences from their experiences and some of the recent experiments at G. K.

The Paramedics G. K. started in 1972. With only 2500 doctors working for the 75 million people in the rural area of Bangladesh (1 doctor for 30, 000 population) and

with only 700 trained nurses in the whole country, the Western health model was Irrelevant. “The purpose of our project is to evolve some s)stem by which the medical Care of the whole population of a particular area can be undertaken efficiently and effectively with the minimum expenditure and maximum benefit, with the employment of limited medical manpower" (from the original project proposal, Feb. 1972) In the last eight years, G. K. has been able to develop such a system with the paramedic as, its main health workers. There is a central 30 bedded hospital with X-ray, pathology and operative facilities. Office and training centre is attached to this hospital. The headquarters and its 4 subcentres together try to deliver primary health care to the 91,000 population of 100 villages of Savar Thana. There are in total 4 doctors and 64 paramedics at present (39 females and 25 males) - 16 paramedics in the headquarters hospital and clinic, 15 stay at the headquarters and cover the surrounding 40 villages moving on bicycles. (Because or the high population density a large number of villages are packed in small area). Other 20 stay at 4 subcentres (5 at each] and each subcentre coven 15 villages. 13 paramedics (mostly males) are village based-living in their own village, and serving them. Each paramedic (except village based)' cover about 2500 population (2 to 5 villages-depending up on the size of the villages). The subcentres have a weekly OPD when a doctor from the headquarter visits but offers emergency services all the seven days. Soma subcentres, managed entirely by paramedics, have small indoor also. The head quarter hospital runs twice a week OPD. Most of the cases are seen and treated by the paramedics. Doctors mainly work as a referral persons, as trainers and as administrators. A paramedic is usually 7th standard to SSC pass unmarried girl almost all recruited from the outside area because of the lack of educated women in the Savar area. They are given about one year's in service training, contents being similar to ANM training in India. They are full time workers of G.K. Dropout rate is 50% Salama, the paramedic with whom I went to a village on bicycle to see her routine village visit covers 4 villages. So she visits each village about once a week, some times twice; goes house to

house, covering about 25 houses in one visit, thus usually the same house is again visited once in a month. The main assigned jobs are 1) Treatment of minor illnesses 2) Immunisation-BCG, 2) Triple to all children and tetanus toxoid to all the women in child bearing age. 3) ANC check up 4) motivation for FP and distribution of oral pills S) Health education 6) Detection and referring complicated cases, specially among pregnant women and children to the doctor at subcentre OPD or at headquarters. The sincerity and the efforts put by Salama were worth seeing; but the response of the people and the health status didn't seem good. The causes of low health status were also obvious in the villages - terrible poverty, poor sanitation (water, mud and flies everywhere), ignorance and a resultant apathy. The paramedic was struggling against these odds with her small health kit and fighting spirit. Of course the picture of health might have been still worse without OK or without Salama. I accompanied Dr. Kamal, to a subcentre. That was the OPD day for the subcentre. 3 girls and 2 boy’s paramedics, all unmarried, stayed at that subcentre - must be a sensation in the rural Muslim community of Bangladesh. The OPD was overflowing with patients. One could observe that neither the subcentre paramedics nor the doctor were overusing antibiotics' or 'the injections. Same experience at the OPD at the bead quarter. GK has innovated some unorthodox methods, Diarrhoea and cholera are very common in Bangladesh. When Cholera Research laboratory (CRL) of Dacca evolved oral rehydration therapy with the electrolyte mixture, OK field workers, while applying it in the field conditions, modified it to “Lobon-Gur" that is salt and Jaggery mixture. Jaggery is easily available in every house, is cheap, and provides sucrose and potassium. CRL later on did field trials on this Lobon-Gur mixture and found it almost equally effective. Paramedics doing tubectomies at G. K. is famous, and now about 85% of the tubectomies are done by the paramedics with very low complication rate. Even more bold is the OPD method of tubectomy. Patient is discharged within two hours and spends the post operative period at home. This has been found to be safe and also preferred by the patients

who apprehended and avoided, tubectomies because of 7 days, hospitalisation.

What do people want? 'The study of the coverage and health impact of G. K. activities raises some questions which offer useful lessons. What is the coverage of the population by the project?'" The total visits by patients to the curative services offered by G. K. are about 60,000 in a year. It bas been estimated that if cost and distance are not the barriers, each individual seeks curative service on an average 3 times a year. So the 1, 000, 00 population in the project area should be seeking help 3,000.00 times it means that 60,000 visits cover 20% of the total curative requirements of the population. Remaining 80% are either unmet or met by other health agencies ('Quacks' mostly) what is the reason for this behaviour of the people? But even more interesting is the analysis of these 60,000 visits. In the year 1975-76 the OPDs (at headquarter and subcentres) treated 48,000 patients while the paramedics in their village rounds treated only 6000 cases. We all speak hoarse on behalf of the ‘dumb' poor people of the villages and advocate a decentralised, simplified, deprofessionalised, cheap medical care, for them. But in the GK experience when a fairly well trained (approx. 1 year) woman paramedic is going to the doctor step only few people are availing her curative services and the majority are preferring to walk: a longer distance to the subcentre or to the headquarter. There are 2 possible reasons which could be discovered during the discussion 1) People still fell that the curative services offered at subcentre or headquarters are superior to the services of paramedic. The mystification about doctors, indoor buildings and Injections influences their choice. 2) Paramedics are ill-equipped in their curative powers. They don't have chemotherapy beyond sulfas. This has acted as an impediment in her showing good curative results to the village people which in turn diminish their cooperation to her in the preventive activities. These lessons should help others in planning the curative services and understanding what people want. Thus far and no further What is the impact of G. K. health activates on the health status of the peoples?

Though comprehensive statistics are not available, the one offered by G. K. shows that the infant mortality rate in OK area is about 120 as against 140 in Bangladesh: and the birth rate is 29 as against 44 in Bangladesh. The Impact is definitely there but a point of stagnation has come, beyond which further improvement in health Indices has become difficult. I felt that whatever improvement G.K. could achieve is mainly because of cheap, effective, widely available curative services. A cure at an early stage is a major preventive force. Some improvement II attributable to lower birth rate because of family planning, oral rehydration therapy in the cases of dehydration and tetanus toxid to mothers. But probably all these measures have reached their saturation point. Some further improvement might occur if the curative powers of the paramedic are increased and if her acceptability increases. But poor paying capacity of the people will limit their utilisation of curative services at some point. Further significant improvement will not occur unless poverty, illiteracy and poor environmental factors are changed. Improving environmental factors is a difficult thing in Bangladesh, where most of the land is under water for 6 months in a year. Huge inputs will be necessary to change this situation, which people can't afford. So GK offers a good case, demonstrator to what extent the health status can be improved by the health measures alone and then how an impasse comes because of socio-economic actors acting as bottleneck. Such conclusions are possible because though GK has a comprehensive vision and has economic and educational programmes also, they are too small to effectively Influence the whole population and hence the main force is still the health activity.

What about peoples participation? In OKs experience it is very difficult to achieve active community participation for health purpose. The health volunteers from the villages were inadequate. The health committees formed in the villages almost never functioned effectively. The villages are factioned and health is not the priority. The paramedics of OK mostly are recruited from outside the G.K. area and being unmarried girls, they stay together in the dormitory rather than in the villages. Thus the community health programme of GK is in the Director Dr. Qasem’s words, “village oriented but not village based.”

In Jamkhed (MFC Bulletin No. 49, Jan, 1980) VHWS are form the same villages. But what about the apparent active participation by the villagers in the apparent active participation by the villagers in the programmes at Jamkhed, the respect and the response the VHW seems to get there in her preventive and educative activities as compared to the not so active cooperation by the villagers to the GK paramedic? Probably the peoples cooperation at Jamkhed Is not because of the health work (in fact GK paramedics are better trained than Jamkhed VHWs) but because of the massive feeding programme and the food for the work programme. If these big economic inputs are eliminated probably people won't have much enthusiasm to participate only for ‘a health programme' at Jamkhed also. G. K. has tried to achieve economic self reliance by a health insurance scheme. But the maximum they have been able to achieve is 50% economic self reliance. This was In spite of the fact that the project got vacancies and FP supplements at no cost. The main impediments are poverty and hence the poor paying capacity of the people (specially in Bangladesh) and the project not adopting unethical curative measures to satisfy people and compete with the quacks.

Some of the conclusions thus drawn may seem negative. But these are the hard facts of community health work and anybody jumping into this field would better learn these lessons from the G.K. experience rather than having illusions about massive people's mobilisation through health work, economic self reliance and improving health by health measures alone. I have found friends at GK very open and honest in accepting and discussing their limitations also. This is a rare quality in a successful project and this increases the educative value of GK very much. (To be concluded.) * *

Dear friend Permit me to share with your readers the following interesting news-items: In a two-piece article entitled “Dubious Medical Ethics’ in the Indian Express, Bombay, it has been said

‘G.P. as make up for it (consultancy fees) by administering unnecessary mixtures, tablets and injections…’ “This has then been justified by an officebearer of the local Indian Medical Association thus they (the patients) insist… so we oblige… otherwise we’ll lose them…..” Not only is this ‘justification' totally ridiculous, coming as it does from a. member of the elite, but it also very aptly summarises the attitude of the private practitioners towards their profession and the society they ‘serve'. It would be highly interesting and enlightening to learn from fellow members of MFC their reactions to this

attitude, and their suggestions to curb if not eradicate, the ills affecting our ‘noble' profession. Among the solutions suggested in the article it has been said that the patients should be made aware of their rights and the prevailing malpractice. Could not the MFC groups organise such educative meetings? I would even suggest that MFC members involved in the teaching of practitioners of tomorrow should make

every effort to actually point out such unethical practices and impress upon the youngsters the need to change the trend. So also, Department of Social Medicine could do a lot to improve awareness amongst doctors, and Pharmacologists stress on Therapeutics, including availability and costs of drugs, to improve the quality of prescribing & reduce the dependence of doctors on the pharmaceutical companies & their representatives.

Dushyant Punwani Bombay

Editorial Committee: Anil Patel Anant Phadke Abhay Bang Luis Barreto Narendra Mehrotra Rishikesh Maru Kamala Jay Rao, EDITOR Views, & opinions expressed in the bulletin are those of the authors and not necessarily of the organisation


