---
title: "The Return Of Liquid Lost"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled The Return Of Liquid Lost from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC140.pdf](http://www.mfcindia.org/mfcpdfs/MFC140.pdf)*

Title: The Return Of Liquid Lost

Authors: Werner David

REPRINT

140

medico friend circle bulletin MAY 1988

"

The Return of Liquid Lost This is a letter to ask your assistance in gathering information to help evolve a more integrated decentralized, effective, 'peoplecentred' approach to oral rehydration therapy (ORT).

'

As you know, in the last five years there has been massive international promotion of ORT. Indeed UNICEF and USAID now consider ORT and immunization to be the twin engines of the "Child Survival Revolution.” But whereas immunization has met with modest success in many countries, there is a general consenus (by WHO, UNICEF and others) that the impact of large scale ORT interventions (with a few notable exceptions) has been disappointing. Serious re-evaluation of ORT strategy is needed. It is important that non-governmental groups, popular organisations and the ultimate users of ORT be key participants in this reevaluation process. ORT, like other health and development technologies, has far reaching political implications. Any such technology can be promoted in ways that are either "people empowering" or "people debilitating" in terms of helping to overcome or perpetuate the underlying social causes of poor health. The "Child Survival Revolution," has often been compared to the "Green Revolution:' In retrospect, the Green Revolution, although technologically sound insofar as it increased total food production, in many countries proved to be socio-politically impoverishing since it was implemented in ways that widened the gap between rich and poor and left more landless powerless hungry families

Than ever before. It would be tragic if the "Child Survival Revolution:' for failure to confront the crucial conflicts of interest that we all know exist, were also to further entrench the social injustices and inequities that perpetuate poverty and poor health. Most agree that oral rehydration is an extremely important part of primary health care. But people disagree about how ORT should be promoted and implemented. While the issues debated often seem to be technical or logistic, they often have serious political implications. Some of the issues in the debate are listed below, (in a highly polarized form) according to their main proponents.

Need for wider participation in ORT evaluation and decision making Most of the formal studies, publications, high-level promotion, and international conferences on ORT have been conducted by large international and national agencies, whose experience and basis tend to favour the, strategies in the "top down" column of Chart. However, there are many small non-governmental and community directed programs that have long term experience in ORT. The experience and biases of these 'grass roots' and 'people-centered' programme tends to favour the 'bottom up approach. Also, there is mounting evidence that many 'folk remedies" and traditional forms of diarrhea management may work as well or better (at least in certain circumstances) than the ORS formulas most promoted by the health establishment.

CHART 1 TWO STRATEGIES FOR ORT PROGRAMMES

Strategy of health ministries and big international agencies (TOP DOWN)

Strategy of non-governmental field programs, popular organizations and community-based programs (BOTTOM UP)

Programming : integrated into comprehensive primary health Implemented as a separate program, or as part care (includes the social causes of poor health) of ‘selective primary health care' Main type of ORT promoted: —packets of ORS salts (glucose based) —standardized formula

—home based)

mix

(sugar

or

cereal

—formula adapted to local resources, conditions and beliefs Main focus and investment: —on products, (manufacture and distribution) -social marketing —social mobilization (getting politicians and celebrities to promote it)

—on education (through many channels: health posts, schools, etc) —awareness raising —community participation (mothers, popular organizations, healers, teachers, children)

Management : —centralized

—decentralized

—controlled by health sector

—collaboration from other sectors: health, education, communication, popular organizations

Main implementing body : Health ministry, health posts, health workers

How it is presented: As a medicine (to facilitate acceptance and use)

Multisectorial : school system, health system, women's organizations

As a food or drink (to demystify and promote understanding of concept)

Annual cost: —increases every year due to growing demand fairly constant for first few years, then rapidly declines as educational investment "pays off" and (for packets) sound ORT practices become "common knowledge" —or transferred to consumers through commercial sale of packets Evaluation —safety of ORS method based mainly on content of formula and accuracy of preparing solution

—safety of methods based more on social factors: availability and constraints of supply, peoples habits and attitudes

Indicators of success:

Indicators of success:

—number of packets distributed

—how many people understand concept and process

—number of people who know how to mix ORS correctly

—how many people use ORT in a way that seems to

—reduction in child mortality

—impact on children's, families' and community's well-being.

—reliance on hard data, statistics, controlled studies

work

—reliance on people's impressions and observations.

Main goal emphasized: —Child survival Political Strategy : win government support by using methods that strengthen and legitimize government and make people dependent on its provisions (government empowering).

—improved quality of life win popular support using methods that organize and empower people, and helping them to become less dependent, more selfreliant.

It is time to recognise the credibility of the Please send us any reports, information, exexperience and perspectives of these non- periences, anecdotes, program plans, teaching governments, people-centered and traditional materials and names of other people we should approaches of the management of children's write that you can that could help us document diarrhea. There are many reports of dramatic and formulate alternative "people-centered" apreduction in child mortality using home mix proaches to ORT. Please also let us know of ORT sensitively adapted to or building on the problems and obstacles you have people's traditions, local resources and encountered, with whatever approaches to constraints. Too often these successful grass ORT you have used or' observed. roots approaches are not seriously considered Areas in which we are Especially by scientists and policy makers because those most intimately involved in community work do Interested in Learning from you about include: not have all the 'baseline data' and, scien1. Comparison of different approaches: tifically controlled studies to validate their ORS, formula, home mix, and cereal findings. Yet, the cumulative findings, based ORT (including soured porridges), "impressions" and success of many etc. (including social. political, economic community-based endeavours may have issues). greater validity (especially in terms of long range social goals) than the expensive carefully 2. factors affecting safety (social, logistic, controlled (but no less biased and perhaps no technical and chemical). more accurate) studies by the experts. 3. Traditional forms of diarrhea control (e.g. It is time that those 'on the bottom' be using traditional weaning food as supplelistened to more carefully and that new 'peoplementary nutrition during diarrhea episode) centered' models of research be encouraged how well they work, and why. and recognized for their pragmatic validity. 4. Obstacles, problems, conflicts of interest, The controversial and polarized issues of and reasons for success or failure ORT strategy, as they relate both to technical (opposition from doctors, local authorities, issues of implementation and to societal issues traditional healers, etc.-and ways of of poverty and power, seem a good place to overcoming it), begin.. 5. Effect of ORT strategy on overall health We need the help of those of you who repand social goals. resent, work closely with, or feel accountable to the poor majority.

6. Relative advantages and disadvantages of Examples will be included (with your help) promoting ORT as medicine' or as food, from many community programs in different parts of the world. ways this is done and results. 7. Educational methods and materials and comparative results (including experiences in using school teachers, children, women's organization, political groups, nutrition workers, agriculture extension workers, etc. in promotion and implementation).

Part B The Politics of Oral Rehydration Therapy. This part will focus on ORT in the larger context of primary health care, and the root causes of poverty, poor health and death from diarrhea. It will explore conflicts of interest and try to give voice to those who are least heard.

8. Ways to stress importance of adequate This book will be presented in clear, simple fluid intake during and after diarrhea. language with many examples and illustrations 9. pros and cons of new development strate- so that field workers, community health gies applied to ORT-eg, 'social marketing' workers, and persons with limited formal 'social mobilization', 'technological fixes', education can understand underlying issues commercialization as compared to more and the politics of health interventions, so that people-centered strategies focusing on they can begin to participate in deciding about participation, cooperative action, methods and strategies. awareness raising, and popular Timeline for your input organization. I would appreciate your response as soon as 10. Implications, successes, hardships that possible. Please do keep sending me your obhave arisen through the commercialization servations and ideas throughout the next year, of ORS products (specific examples) as the proposed book will be an ongoing learning process. 11. Examples of ‘participatory research’. The information we are asking you to send 12. Examples of misleading data, statistics or us need not be well organized or studiously reports. presented. Even casual observations can be very helpful. 13. ORT as applies to other illnesses than diarrhea (e.g. measles). Please help us in helping to formulate and educate the health establishment about more truly "people-centered" approaches to ORT and 14. Ideas and suggestions for more effective to promote primary health care as a part of a approaches to ORT (especially in the worldwide struggle for social justice. context of primary health care and social change). We eagerly await your response. As thanks What we hope to do with the information for your help, we will be pleased to send you a gathered. complementary copy of 'The Return of Liquid Lost' when it is published. Thank you. We hope to write a booklet to be titled some—David Werner thing like: The Return of Liquid Lost :putting oral rehydration therapy in the people's hands. The booklet will be in two parts. The Hesperian Foundation, P.O. Box. 1692, Palo Alto Part A helping people learn about ORT. EmCalifornia 94302, U.S.A. phasis will be on non-formal education and communication methods that adapt to and build on people local traditions and beliefs.

Edited and Published by Sathyamala for 'Medico Friend Circle Bulletin Trust. 50 LIC Quarter, University Road, Pune 411016. Annual Subscription-Inland Rs. 20.00, Foreign Air Mail US $ 11. Subscriptions to be sent to-The Editor, F-20 (GF) Jungpura Extn. New Delhi-110014. Printed at Kalpana Printing House, L-4 Green Park Extn, New Delhi-16.


