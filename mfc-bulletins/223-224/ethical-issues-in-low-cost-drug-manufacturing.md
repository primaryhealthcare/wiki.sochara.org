---
title: "Ethical Issues In Low Cost Drug Manufacturing"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Ethical Issues In Low Cost Drug Manufacturing from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC223-224.pdf](http://www.mfcindia.org/mfcpdfs/MFC223-224.pdf)*

Title: Ethical Issues In Low Cost Drug Manufacturing

Authors: Srinivasan S

11.C

medico friend circle bulletin 223 224

October-November, 1995

ETHICAL ISSUES IN LOW COST DRUG MANUFACTURING") S. Srinivasan Ethics appears to deal with, among other things; the problem of right and wrong conduct, intrinsic good and evil and relevance of means to ends. All these three imply a certain world view and in turn imply a certain consistency of one's actions to this world view. We will examine the subject of low cost drug production with the above understanding of ethics. Our experiences are centred around LOCOST, a voluntary agency based in Baroda. LOCOST founded in 1983, has gone on from trading in formulations, to loan licence and to manufacture in its own formulations unit. LOCOST has been also active, now and then, in educational issues and in promotion of rational drug therapy. LOCOST has also handled project exports and technology transfer to a neighbouring country for a major bulk drug project, LOCOST has survived, and has, by and large, shown that low priced quality formulations can be an economically viable idea. Survival; and viable survival at that, tends to generate narratives and discourses that portray those involved in a favourable, even heroic, light. This is especially so when the sutradhars are the principal actors themselves. This narration of ethical issues involved is no exception, although an attempt will be made at some kind of reflective discourse. On Being Clean Voluntary organisations like LOCOST who are into business operate under twin compulsions: to be business like and to adhere to the ethos of voluntary organisations, and in our case of non-profit trusts.

One of the most important issues in the macro business environment in India is that of corruption. Have we corrupted and indulged in corruption? To our best of knowledge we have not. Bribing or speed money is basically to get your work done fast lest the rug is pulled under your feet by the many arms of the government. The effect of us not bribing is that-permissions get delayed. Others take 24 hours in getting things done; for us it takes even 24 weeks at times. This has led to some inbuilt delays. Our response time is slow in making changes as per customer's requirements. Have we indulged in indirect corruption? That is through consultants and intermediaries one goes through? Here we are not so sure. Except in one case where we certainly know that our consultant paid Rs. 500/- to a government functionary, without our knowledge, and without doing which he felt that our temporary electrical connection will simply not be on. Otherwise we have told our consultants not to give money on our behalf. Most of the time most of our consultants privately snigger a tour morality. But they put up with us. Often they get the work done by appeal to some residual element that is still unbrutalised by the environment of a government office; Sahib, this is a trust doing good work. They make essential medicines for the poor. The government functionary let goes, although not without scoring points in a school masterly fashion. In a few cases, the functionary genuinely believes he/site should not take money from a 'good-works' organisation. At other times the rent paid by the consultant on behalf of other clients exonerates us.

Some friends have counted 35 statutory acts/bodies which small-scale business organisations in India have

to comply with. We reckon that the regular ones are about a dozen. Most of these statutes have provision for inspectors who visit you and have the power to demand compliance, often on some abstruse technicality, but putting their 'observations' in the 'Visit Book'. The attitude of most of these functionaries is you are likely to have violated some provisions, so it is upto you to wriggle out of it. You are guilty unless you prove innocent. The onus of the proof lies with you. The easy way is to pacify government visitors from the start: Send your vehicle to receive them at the railway station (although they are entitled to TA/DA), give them lunch at a 'decent' place if it is anywhere near lunch time, and in general be very deferential and do not question their most illogical observations in your visit book. We on our part try to be deferential, within limits of what We think does not compromise with our dignity although it may try our patience.

An Excise and Prohibition functionary in charge of alcohol quota for our factory once decided to vent his spleen, not having found any fault, on the score that our Factory Manager does not serve water on his arrival. He insisted on taking some 'shakti ki davaee' and took some three to four bottles of syrups (total cost Rs. 50). More recently, one of the local government functionaries, who presumably came to advise us on our compliance leading to better manufacturing practices, brazenly asked for and was given 100 tablets of Chloroquine (total cost Rs. 53). In both cases the writer regrets to report that we were not strong enough to tell the functionaries concerned, "No, you cannot take the medicine". Regular companies have other strategies; of also giving a packet on departure; this prevents serious comments in the Visit Book and subsequent queries that can keep you~ busy for at least a month. When it is your turn to visit their offices, officials tend to extend their 'cooperation' when you have 'cooperated'. A business person, who has probably put family jewels on mortgage to complete his/her capital requirements, is threatened, with or without justification, by a Pollution Control Board inspector that his/her factory will be closed. The business person also tends to cooperate, though not necessarily to comply with anti-pollution requirements. We do not give packets. We are kept busy weeks, and at times, months on end in trying to comply, or argue out, points made in the Visit Book by government functionaries or nothings otherwise made in files.

An ethical response would probably be that we tell the functionaries off. We tried politely in the beginning. We now find it is not worth the trouble. Our main business is to produce quality drugs at low cost, so why invite unnecessary correspondence/meetings regarding so called compliance? But is our main business only to produce quality drugs but not also to oppose dehumanising tendencies anywhere? Bureaucracy humiliates and dehumanizes. Oh yes, we have not bribed in the classical sense, but when we know for sure, and have visibly seen, instances of illegal gratification, of company executives giving licensing authorities costly presents in cash and kind as Diwali gifts, should we keep quiet? Surely our short term expediency of 'avoiding trouble' and 'getting on with our main task' conflicts with our long-term goals. Also, with what clean conscience do we continue to advocate the (politically correct NGO) line of more role for the State in industry and public welfare when we find the State does not serve anybody but its own minions? A related issue is of size and scale; today our size is relatively modest. We are able to manage this aspect of our environment because we are small. What happens when the size triples, demand from our target market segment (namely those working with the poor) increases? Should we limit our size only because we cannot manage the spurious requirements of our statutory functionaries and babus? Is it ethically right for a voluntary agency to flee from a battle of right versus wrong?

Probably no thriving business today in India is run squeaky clean. There is some yielding to bureaucratic whims in anticipation of bureaucratic nitpicking; and bending of rules and even laws depending on one's confidence to 'work' the system. For ourselves at LOCOST, we are clear we will close down, even if it causes inconvenience to workers and to customers. LOCOST, if push comes to a shove, will pull down its shutters. However, such industrial bravery stories are quickly forgotten and life carries on. For our response would not have made an iota of difference to the system. If we are really serious about consistency, of means over ends for instance, we ought not to be using the services and products of many of these business concerns; and in case of formulation units like us, we ought not to be using products of many of these (ethically and ecologically) notso-clean bulk drug companies, as also

the services of many of our not-so-clean distributors, agents and brokers* of bulk drugs that feed our tablets, liquids and capsules. Where does this logic leave us? In some solipsist hell. So as an alternative, we construct a world, outside whose boundaries we tell ourselves, we are not going to get bothered too much. We will interact, have commercial exchange with the vast multitudes outside our boundary. We may even examine their colourful antecedents, making occasional forays that border on a crusade, but will refuse to let them make us wholly dysfunctional.

Choice of Products, Pricing and Ethics Any business has to make what it considers safe, ethical products. In drugs, safety is a relative issue. No drug is free from side-effects. In some drugs, certain new side effects and adverse drug reactions are known only as time passes.

It is practically impossible to become expert in the several scientific disciplines that feed into the pharmacological knowledge of medicines. This requires that as an ethical group one draws upon accepted standard sources of national/international expertise. At the same time one benchmarks available expert information with practical field experience. In the process, we need to develop awareness about the politics of the knowledge industry, and of medicine and health. An instinctive feel of who is saying what and why helps. At a first level, in drawing up a list of drugs for production, one takes recourse to standard essential drug lists like WHO, Hathi Committee, etc; and formulates, with the help of more immediately available local expertise, one's own essential drug list depending on local community requirements. Being an ethical drug manufacturer means you make only essential generic rnedicines. You are spared of innovating new fixed dose combinations that tend to mislead uninformed customers. Again among essential drugs, one makes only what current scientific and clinical experience considers rational. Thus we make only Amoxicillin and not also Ampicillin; Doxycycline only and not also Tetracycline. We are also in the

process of weeding out Indomethacin as it has more adverse side-effects; and other analgesics like Paracetamol and Ibuprofen are preferable. In each of these and a dozen other cases, the drugs not made and/or being weeded out are not banned, there is a flourishing demand for all of them, and probably the customer is not going to change his prescription practice despite our entreaties. And to top it all, not making them means foregoing that much amount of sales. In the case of LOCOST, we have imposed on ourselves another constraint; that of supplying medicines only to those working with the poor. This in principle means only NGOs and social action groups. The government sector is also used mostly by the poor. But till date, we have not been able to make any dent on the government sector. Nor have we really tried hard. The kind of compromises we need to make, of 'sharing the proceeds' with government functionaries, etc. have deterred us. Recently, one intermediary told us to stay off when he realised 'we were a public trust. This intermediary helps procures orders from DGSTD, defence, railways etc. He tried to impress us by saying how he has recently increased the orders from a government department, for a well-known Baroda-based company, from 35 lakh units to one crore units. His commission, the worthy told us, was 20 percent. .. This writer asked him how on earth anybody could be competitive, let alone make any profit, if he had to pay such a hefty commission for generic medicines; He told us that some of the supplies are made only on paper, bills passed and sales proceeds shared. It is difficult, he assured us, for anybody in the government to physically check as the orders run into crores of tablets and capsules.

Another model 'of milking-the State, if ever there was one. However this is not news to us. We often hear many such variants of how drug companies manage to quote ridiculously low prices. This is something we live with. In the Indian dream, not unlike the American dream, nothing succeeds like success. One practical consequence is that not all our customers are willing to go along with us in examining the total integrity of our competitive manufacturers, resting content with the end prices and performance of the

* A particularly annoying practice by bulk drug distributors is that of issuing an official bill and a supplementary debit note labeled as commission charges. This accounting device tides over the inconvenience caused when the market price is above the government-fixed price. As ethics conscious manufacturers, we need to make some noise on this market priceadjustment mechanism. We do not however, save for the occasional mild purring. We need all the guys down the line in the market for running our business.

products supplied. This is agonizing for us. So then, just to retain goodwill we cut down prices further, which in turn means generating more sales to even stay at the same level of revenue as last year (like Alice running twice as hard to stay put in the same place). Increasing sales and maximizing utilisation of capacity is desirable for another reason; we reach out to more of our target market segment. But this is turn affects the size of our operations; from a small-scale industry we become medium-scale and further more. With increased size, as noted before, there .is the question,-will we continue to be able to stick to our values?

as per the statutory minimum wage, one cannot, as an ethically oriented organisation, sleep peacefully in the night. While this is not the place to give an answer to this vexed question, we need to work out salary levels that can at least help a worker enjoy middle-class life style without middle class consumerism and vulgarity. In any case it is hardly ethical fora voluntary organisation to rest content with the statement that we are paying a worker his/her minimum wage, or worse, his/her 'market value' when we know well that the invisible hand of the market tends to tilt towards those already well-off.

And whither ethics like small is beautiful? Indeed if anything the ethic of small is beautiful takes a severe beating in a rapidly changing globalizing economy, and especially so in a highly inflationary environment like the one we have had for the last 4 years.

Reverting back to the issue of pricing, can we say then that one of the major factors that determine the scale of the organisation is the minimum comfort wages to be paid to workers which in turn ought to determine pricing?

Let us take pricing, profits and salaries — factors interrelated to each other. On the whole our pricing should be lesser than the market prices of drugs, otherwise it undermines the very rationale and logic of our existence. We have claimed that good quality medicines can be made at prices lesser than market formulations. We have done so in the past. Can we continue to do so? Is low pricing of medicines sustainable on a long-term basis given the many boundary conditions of being a voluntary, ethical organisation?

What then of surplus? Surplus generation is determined by, and in turn determines, prices, sales and the scale' of a low cost drug manufacturing effort. 'Surplus is necessary for funding depreciation, for ploughing back into capital investments in the future and to inspire confidence in bankers, funders and one's own workers. Nobody is going to subsidise a loss-making concern for long. Adequate surplus can also ensure lower interest burden in the future which in turn means lower costs of manufacture.

Clearly how low our pricing can dip, is contingent on how much revenue we need to generate to keep operations going, the break even' point. Minimum revenue to be generated keeps going up every year, thanks to inflation. Minimum revenue is also contingent on how much or how little you decide to pay to your workers.

What then should our workers get? At the least, they cannot get less than the official minimum wages. But as anybody who has looked into the politics of minimum wage announcements knows, minimum wages provisions are decided by the ruling elite of politicians, bureaucrats and business lobbies like chambers of commerce. They all need each other. SD the minimum wage fixed is what suits the collective political interests of this triumvirate.

These minimum wages do not buy a decent house, good education for one's children and a decent quality of life. They probably keep a single-wage earner family of four from continuous starvation. Clearly then by just paying

How much profit/surplus to make is also determined by other concomitant requirements like allocating money for research, education and product development. It is doubtful whether any significant research leading to product development and product innovation can be done by smallscale organisations in competitive markets. This is basically because there is not enough surpluses to do so. In LOCOST's case, our research and education is independently funded. We do not do any product development. Funding of research is at the best of times a complicated issue. Current market wisdom is forcing even CSIR labs to do a bit of their own generation. Can and should a low cost drug producing unit continue to source money for this from outside? More important is of course the issue of what type of research that a voluntary drug manufacturing unit can conduct, assuming that it should do research. There are no easy answers. After some discussion, LOCOST on its part has decided on the following as its current research priorities:

1.

Pricing

2.

Production and Study of Manufacturing Process

3.

Occupational health hazards

4.

Drug Laws (a critical review)

5.

Pharmacy Management Issues

6.

Researching partners experiences

7.

Drug utilization patterns of partners

8.

Baseline health survey of our workers as responsible manufacturers

9.

Effects of LOCOST activities on partners

10. Understanding of how atmosphere in factory has improved 12.

Detailed study of drug information in the market

13.

Analysis of advertisements

14.

Ayurvedic preparations - their rationality

15.

Creation of knowledge related to drugs and medicines but not related to the above five areas explicitly.

Quality Control If there is one area where ethical standards are visibly called upon in drug manufacturing, it is in the area of quality control. Today pressures of competition often force manufacturers to take short cuts. An ethical organisation cannot afford to do so. The pharmacopeias often sanctions a 10% tolerance limit. Manufacturers are known to save on costs by making formulations at the lower end of the limit (a 500 mg tablet of, say Paracetamol, would contain only 451 mg of the active substance). Legally, a 451 mg Paracetamol still passes in quality control. The customer would seldom know. Most Indians have low body weight, therefore the dosage by weight really does not suffer. So is it okay? No, it is not. It is misguiding the customer just as claiming extra advantages of branded over generic drugs is misguiding the public. The other area of quality control is to actually to do all the tests specified in the pharmacopeias. Again, if some tests are not done, it does not really seem to matter. But then in a very real sense, one is playing with people's lives. So it is better to do all the tests. There are also, expectedly, many grey areas. A raw material consistently fails in, say, iron content, and on the margins at that. And there are no other alternative manufacturers of the raw material counts. Do we take

the raw material for manufacture especially as' the small deficiency with respect to iron is not going to make a difference in the tablet? There is a big demand piling up. Customers are getting increasingly irate. Do we manufacture? No. We do not manufacture even at the risk of appearing cruel. Even if you know that other manufacturers are making a killing on faulty raw material. There is a related requirement; if a raw material or excipients fails, a report is to be sent to the drug authorities who in turn presumably force the negligent raw material manufacturer to correct his/her output. When there are serious problems with the quality of the sourced raw material, we do make a formal report, even if we have seldom seen any corrective action. Often we find it is more effective to talk to the manufacturer, apart from returning the failed raw material. Probably the market mechanism of many quality conscious manufacturers returning the faulty raw material will force the manufacturer to correct his/her manufacturing process. However there are so many if sand buts-in this kind of a posture. At best our own stance is wishy-washy ethically. If we are serious about ethics and systemic promotion of quality, we ought to be willing to take on manufacturers, suppliers and the Drug Administration and-follow up until we get an appropriate response. There was the time we went to the press by announcing that a nice-looking, blister-packed Rifampicin 450 mg of a manufacturer in Vapi had only 250 mg. A senior FDA authority later politely told this writer how we should approach the FDA and assured this writer avuncularly that if we had such problems, we will solve your problems, why go to the press? This is not the place for a critique of the FDA. For a part critique of the Maharashtra FDA, that licenses many of the major drug units of this country, the reader is urged to read the Justice Lentin Commission Report (1989) on the Mannitol tragedy. If only more manufacturers would come together and share their experiences, one can make the administration probably less venal and more responsive. But no manufacturer in the commercial sector would like to put his bottom on the sling; partly because everybody feels he/she has some skeletons to hide, or the authorities may create skeletons where none existed before. So everybody tends to be 'practical' and life carries on in liberalised India. There are something’s which are intrinsically not done (or 'evil') like making a substandard drug. During the manufacturing operation, we can claim to have set

yardsticks which are more stringent than the statute book, yardsticks which actually matter. Most manufacturers with some semblance of a conscience do set for themselves some such yardsticks. The State authorities, related to the 35 acts specified above, however tend to often look at the letter of the law than the spirit. In fact, the system lets off the hook those who violate the real intent of the law. Ordinary, wellmeaning persons can be made to look guilty offenders by an administrative ruling or an executive fiat. One develops over time an intuitive understanding of the relative importance of various administrative actions, often punitive in intent, and develop ethical ways of coping and yet being functional.

A ethical organisation in business by virtue of its existence needs to cultivate more humane values and ethos internally. Ethically, anything that conflicts with these values must not be pursued. LOCOST after some deliberation has decided that the following minimal values and processes are important*: 1.

Promotion of good quality generic drugs and rational therapy.

2.

Promotion of health education.

3.

Influencing national/state level health policy with a priority on drugs.

Operationally, the following values and processes would sought to be actualised in phased manner:

1: Encourage people to be as free and autonomous, as possible, with increasing sense responsibility and self-discipline.

of

b)

Reduction of differences between leaders and the led, and empowering of those who work to take decisions.

c)

Financial decisions must be taken after considering quality, price and market constraints.

d)

Partner profile: Continue as we do for 2 years and then reexamine any necessity for change in reaching out to newer consumer segments.

e)

All activities geared towards rational therapy (but not policy influencing or research) should be built into cost (like quality control).

f)

Reach out in symbolic ways, if be with a measure of tentativeness and deliberateness, understanding that those who implement must feel comfortable, even as there are built-in limitations of middleclass elite trying to actualise democratic and participatory goals. The former has choices and options in life which workers do not as a rule have. Nevertheless, those who have must consciously strive to minimise the differences and contradictions between our private and public lives. Value actualization is a process that cans never he completely, if not perfectly, achieved. At best, we should consider our sincere efforts experiments in truth.

Paradigm Ethics

self

2.

Clean, non-bribing culture with transparency and openness in all matters.

3.

Participation in decision making processes and a culture of egalitarian and humane ethos.

4.

Simplicity in every aspect of working, organising meetings, presenting ideas in speech and in writing.

5.

Internalisation of LOCOST by all employees as an alternative organisation with larger societal and personal goals.

Specifically the following shall be sought to be implemented in a period of 5 years to the extent possible: a)

levels better than industry and statutory norms. Benefits must be same for everyone - even if salaries continue to show difference.

There are many other issues which need discussion. But this essay is becoming long and that increases the risk of it not being read. For the sake of brevity we will just mention a few other important issues which. bear discussion in a longer effort; ethical issues in promotion, labeling and marketing of drugs; ethical dilemmas in concurrently manufacturing and marketing of drugs from other systems of medicine; issues generic to the NGO paradigm of organisation; ethical issues related to lobbying, public advocacy and our attitudes to the government; ethical issues in the liberalisation versus regulation debate; issues of human rights in the manufacture, marketing<and use of pharmaceuticals; gender issues in the choice, manufacture and marketing of drugs; and how should an ethics conscious person conduct himself/herself in a milieu of less than perfect people in a less than perfect world.

Reduction of salary differences between highest and lowest paid levels, or at least paying lower

* Quoted from the Report of Planning Meeting, LOCOST, Baroda, September 1993.

(Contd. on p.16)

RESEARCH IN PUBLIC HOSPITALS OF BOMBAY Abha Nagral If one reviews the academic work done in the field of Medicine in the city of Bombay, it would be obvious that the public sector far excels in this field as compared to the private sector, both in terms of quantity and quality of work done. In fact, some of 'firsts' in the country have originated from these public hospitals. "In vitro fertilisation" and heart transplant were first performed in India at the KEM Hospital. Reserpine was developed and discovered by Rustom Jal Vakil at KEM Hospital. The first Laparoscopic Cholecystectomy in India was performed at 11 Hospital. KEM Hospital has taken the initiative in establishing an Ayurvedic Research Centre for carrying out research on indigenous drugs. KEM Hospital also has a functional experimental animal laboratory. In a study on research done in public hospitals, it was observed that out of 128 public hospitals in the country which featured in the Science Citation Index, public hospitals of Bombay figured in the first thirty two, with KEM Hospital being seventh on the' list. Many factors have contributed towards making such achievements possible. One important factor is the existence of the full time system for the staff which promotes a full time commitment to the patients, research activities as well as undergraduate and postgraduate teaching. Also a research ethic may get inculcated into the student right from post graduation days if she has the right role models in front. An important step is the compulsion to write a thesis and hence do research, which if inculcated in the right direction would make a good and thinking research worker of the doctor in the future. Public hospitals in Bombay have also been the first to set up superspeciality departments in the country, thus creating further avenues for research. Contributions of individual doctors committed to scientific medicine have also gone a long way in making these achievements possible.

Lopsided fund allocation for research oriented expensive equipment: However, all does not seem to be well in the academic and research scene of the public hospitals of the city. It is often forgotten that the primary aim of these hospitals is service and not research. With the limited budget available for the hospital with none of it officially granted for research, one can see proliferation and favouring of superspeciality departments at the expense of the basic specialties which bear the brunt of the

patient load so characteristic of a public hospital. Expensive equipment is bought under the guise of providing improved facilities to the patients when actually it is meant to increase the research output of the department, Simultaneously, there seems to be an eternal shortage of basic drugs, IV fluids, disposable needles and also life-saving equipment like volume cycle ventilators in the basic speciality departments of the hospitals. Ian Kennedy in his series of lectures "The Unmasking of Medicine" calls this the technological imperative. The mere existence of equipment dictates that it be used. The doctor recommends it to be acquired and once it is acquired, feels compelled to use it. That it may not have been necessary, that it serves to perpetuate an unfortunate model of Medicine and the pursuit of health, that it attracts resources away from areas where greater benefit to health may be achieved, that patients do not serve to gain a great deal from it, that operational errors are made - all these and more are lost in the technological imperative. The wrong prioritisation of resources, especially when the administration is crying hoarse about scant resources seems unfair. Relevance of Research: Another important issue is how relevant is hi-fi research in our country and especially in public hospitals? Very often one sees research in India as synonymous with duplication of work done abroad. One finds scant basic research worth its name in India. In fact, public hospitals are found wanting in contributing to research in fields very relevant to Indian conditions like infectious diseases, occupational health, trauma, burns management etc. These are the unglamorous fields which deserve maximum attention but have been consistently neglected by researchers. American scientists have suggested that research in tropical diseases is best done in the West as scientists in the third world have neither the abilities nor the facilities to solve the problems they encounter everyday. Publish or perish: The research work done should be guided by a yearning to describe something original or earth-shattering and not just to ensure a publication in a journal to build up one's curriculum vitae, as is often seen. This behaviour cannot be justified even with the pressures of promotion, of getting scholarships to go abroad or simply to get recognition in your own country.

Ethics of Research:

Recommendations:

The ethics of research in public hospitals needs to be scrutinised. The role of Ethics Committees in public hospitals and their being actually effective beyond just a symbolic presence needs to be determined. Ethics Committees have to ensure that the clinical research studies conform to ethical standards and that concern for individual takes precedence over the interests of science and society. The concept of informed consent exists only on paper. How often is informed consent taken-from the unsuspecting patients who so willingly and ignorantly participate in these studies, a significant number of which are dangerously invasive? It may be convenient to argue that one cannot compare our patients especially from public hospitals who belong to the lower socioeconomic strata of society and are therefore illiterate and incapable of understanding the details of the study, with their Western counterparts. This raises the issue of whether ethical standards are relative or whether like scientific standards, they are absolute. Also, do these clinical research studies conform to international guidelines laid down by the Nuremburg Code or the Helsinki declaration?

The role of Drug Companies: The role of drug company sponsored trials also needs to be clarified. While on the surface everything seems to be as per the ethical guidelines and detailed informed consents being taken, the possibility of a close nexus developing between the investigators of the trial and the pharmaceutical company cannot be totally ruled out. Subtle personal favours like sponsoring trips abroad and hotel staying arrangements may reflect subconsciously or consciously in the results of trials carried out in the future. It is relevant to mention that drug companies are known to use third world countries for clinical trials of new drugs because of the lax ethical demands.

1.

There should be a strict scrutiny of the priority lists prepared for allocation of the hospital's resources for research. Maximum weightage should be given to buying equipment which will directly help alleviate patient suffering and be translated into better patient-care. That the equipment also has a research potential may be (in incidental but not the primary motive of buying the equipment.

2.

Emphasis should be laid on research relevant to our country. In a country like India tropical and infectious diseases should get priority over all other diseases. Work on indigenous drugs should also be encouraged.

3.

Attitudes regarding research have to be changed, Undue importance to publications should be stopped as the policy of "publish or perish" gets perpetuated. Good relevant original research work needs to be encouraged. A healthy outlook towards research with due credit to even the junior member in the department needs to be encouraged.

4.The role of Ethics Committee needs to be clearly defined. Members of the committee should be carefully selected and should include doctors of impeccable reputation, ethicists and also members from the lay public. The Nuremburg ·code, the Helsinki Declarati6nand the WHO guidelines should help the ethics committees to formulate their own guidelines. Each and every human based study carried out in the hospital should come under the purview of this committee with right to procure all documents and consent forms related to the study at its will. Spot checks on the studies should be performed. 5.

A regular open audit of research activities, their funding and the results should be carried out.

Positions at Locost LOCOST is a pioneer manufacturer of essential drug formulations in the voluntary sector. All our efforts are oriented towards keeping health care accessible to especially those who cannot afford. LOCOST invites applications form motivated individuals, especially women, for the following positions: Education Coordinator: To initiate, -coordinate and implement all rational therapy related educational activities of LOCOST. Research Coordinator: To design and implement research programmes of LOCOST especially as related to the economics and technology of drug production, use and misuse of medicines, GMP; occupational hazards in drug production and drug laws. Publications Coordinator: To conceive, design and produce user-friendly publications resulting from our education and research efforts. To coordinate a news-feature service on health and drugs. Applications are also invited for the positions of Programme Associates in education and research. Salaries are modest but will be reasonable. All positions are Barods based. Written competency in English and other Indian languages, especially Gujarati, desirable. Graduates/post-graduates/doctorates in medicine, pharmacy, bio-chemistry, economics, management, and related disciplines are particularly encouraged to respond. Please contact/write: Low Cost Standard Therapeutics, GPO Box No. 134, Baroda-390 001.

ETHICS IN CONTRACEPTIVE RESEARCH Swatija & Chayanika ~ The campaign against harmful drugs and devices related to fertility control has been going on for a long time. This campaign has challenged existing research methods and designs and has also asked for a radical reorientation of such research. In the process has emerged the new understanding of looking at contraception and all new developments in its technology as such.

Long acting contraceptive methods like the injectables and the implants have been at the centre of controversies for a long time now. Most of the times the case against these contraceptives has been fought on technical grounds. The safety, efficacy and to an extent the need for such methods were questioned. The focus ""as also on either the method of the research trials or the interpretation of the research results that had been obtained. In fact as the campaign grew the nature of the demands and the issues that we could connect have also become more complex and complete.

History of the campaigns in India: In India one of the major campaigns has been that against high dose EP drugs. These were banned by the efforts of various kinds of health groups and drug action networks with active support from women's groups and consumer groups. The major issue highlighted was the redundancy of the drug and its harmful misuse as an abortifacients. The issue was more at the level of proving that other treatments were available for the state of secondary amenorrhoea, that this drug was misused and that its effect on the fetus was harmful if the abortion did not take place.

The question was "is it ethical to sell a drug that can easily be replaced when we know of its rampant misuse"? The demand was for a ban on its sales and we were successful in getting a ban on the sale of the high dose EP pills. The injections of the same formulation were not banned and now we realise that high dose estrogen and high dose progesterone are separately available meant to be consumed together for the same purpose for which high dose EP was banned, namely as an abortificient!

Later the drug that was focused on was the injectable Neten. This contraceptive was being tested on women without their consent and so the issue of informed consent at the time of the clinical trials was highlighted. It was proven through an actual action that women would not 'volunteer' for such trials if they were told all the facts. This lime a ban was sought on the introduction of such contraceptives in the family planning programme. The ethics of the trials were questioned by objecting to the way in which women were being recruited for it. The trials, however, continued.

The facts about the injectable were also gathered arid its harmful effects stressed upon. The fact that such long time disruption of the cycle would be very harmful to women's health was also emphasised. An objection was also raised to the long acting character of the injectable stressing more on the fact that the woman had no control over it. The understanding of issues such as safety and extent of side effects were also raised. In spite of the Supreme Court case the injectable was allowed to be marketed and its actual sale began in 1994. The next major campaign has been against the implants. Once again the issue was of the lack of informed consent at the time of the trials but this time the whole design of the trial was also questioned*. The incompleteness of the research was pointed out in not doing a proper long term follow up of the women and the progeny born to them. The subjectivity and the anti-women bias in disregarding menstrual disturbances and other such side effects was also pointed out. The basis of arriving at the 20,000 menstrual cycle observations was also questioned for such a long acting method that was meant to be used for 60 months by every woman. Questions were also raised about the ethics of having such a long acting contraceptive on which the user had no control and for whose removal the woman was dependent on the provider. The fact that there was resistance from the provider to remove the device even at the time of the trial and the possible misuse in the coercive family planning programme was also emphasised. The tremendous loss to follow up at the trial stage itself was also pointed out to stress the futility of this method in the Indian context.

* These questions were raised through the Net-en case also -Ed.

In the meanwhile Depo was cleared by the US FDA after being at the centre of a controversy for a long time. The results of the animal studies have apparently been refuted by a WHO study with Depo for carcinogenicity-. This was preceded by the revised WHO guidelines for research on hormonal contraceptives in which results of animal trials were considered to be ineffective in gauging the extent of the problem. As a result they were said to be unimportant and redundant to an extent. Further it had been said that the only way in which any estimate about the nature of the side effects could be made with certainty would be through the large scale use of the contraceptive itself and so post marketing surveillance was said to be very important. In fact to justify this stand the example of the oral contraceptives was often given. The OCs were introduced without much research. For the last forty years in which they have been used they have, however, now become the method most used and hence most researched with a lot of facts coming to light about the harmful side effects. Instead of this being a lesson of having to complete the research before its introduction, this has been taken to mean that there can be no comprehensive data on any method unless there is widespread use of the method" A result of all this was the introduction of Depo in the Indian market apparently with a condition to carry out a post marketing surveillance. The details of the surveillance are not included in the packet itself and nor are doctors prescribing the injectable aware of the nature of the surveillance. In any case in a society where there is no system of any kind of check at the time of distribution, is surveillance ever going to be possible? In such a situation can this be considered an ethically right step?

contraceptive research or at least the questions that need to be asked to assess the ethics of contraceptive research.

Contraceptives are not only 'drugs': The issues of ethics for research into any drug can be roughly categorised into three stages and (he above history shows the campaign against harmful contraceptives successively taking up the issue at a different stage each time. These three stages can be said to be as follows:

(a) Ethics of the actual conducting of the trials. (b)

Ethics of the design of the research including the clinical trials.

(c)

Ethics of the direction of the overall research in the area.

Although the criteria for ethics for any drug research can be similarly categorised there is something specific about contraceptives being looked at as drugs and that too we would like to emphasise and highlight at the outset.

Contraceptives do not treat a state of illness. They are not curing a disease, they are in that sense not being taken by people whose 'normal' and 'natural' functioning of the body is impaired. Contraceptives are used by persons who might be otherwise healthy or ill but who do not use the method as a cure, In fact whether the user is healthy or ill in both cases the contraceptive method has to take note of both these conditions and research has to be particularly directed keeping this fact in mind. In fact it has to be borne in mind that not only is the contraceptive not getting rid of any disease, it is used because most people especially women need it. This is an important factor because the need would force many people to accept whatever is available. Seeing to it that the need is not exploited by others hence becomes an important criteria.

For the last two years moving on from this stage, there has been an international campaign asking for a halt to all research on the anti-fertility 'vaccines'. The question' 'is not at the level of whether the testing is being done adequately or not, or whether the women who are being tested upon are being informed or not but the question has been raised at the Further, the contraceptives are used over a long period of level of the ethics of developing such methods of time on a continuous basis. This means that if the method contraception at all. intervenes in the usual functioning of the body then this intervention is over a very long period of time, a fact that We have recaptured this historical context briefly here again has to be taken care of and looked into when because it very neatly sumps up the situation vis-a-vis determining the safety and the harmlessness of the method. contraceptives in India and also raises the issues of ethics Any contraceptive is also used in the reproductive age as raised by the campaigns of various levels. It is also the group of the population and it does obviously affect the reproductive cycle leading to the possibility of it also backdrop from which we are arriving at an understanding affecting the next generation - a fact that also has to be of what really should be the ethics of ethically cleared when clearing a contraceptive method at any stage.

* For a critique of this study see mfcb 220 - Ed.

Most importantly the situation that contraceptives deal with is not really 'medical'. There are social solutions available and in a sense it is a social situation that is being catered to by the medical science. This is an important aspect that keeps getting lost in today's context when every thing is being handed over to experts and technology. The ethical questions raised in the case of medicalising of a social situation are naturally going to be different from those asked when treating a situation that demands and can be looked at primarily from a medical point of view. In general the practice of medicine is becoming more and more 'technical'. The solutions, the diagnostic methods and the cure are based on a very technology oriented mindset. The practice of medicine is being governed more so by the profiteering attitudes of the market and predominantly by the pharmaceutical industry. The same holds in case of contraceptive methods too. Only here there are some additional factors.

The population control lobby has been not only a major force that directs all contraceptive research but their propaganda has affected the mind sets of most people including the researchers. Scientists like Prof. Talwar, pioneer in the AFV research openly claim these concerns to be the guiding force for their research and concern in this area. A more sophisticated method used by some other scientists is that of talking about reducing the high rate of maternal mortality by introducing more contraceptive methods because apparently the 'risk of pregnancy' is much higher than the risk due to the contraceptive method! Population control means controlling the numbers of certain populations and in that sense it is racist and against the poor. But along with these biases of population control, the implementation in practice involves a very strong antiwomen attitude well entrenched in the overall social mind. Women are considered responsible by society for reproduction, at times that is their only accepted role and contribution in society. Achieving control over this reproduction process and their fertility is also considered their responsibility and the research which further stresses on women taking on this role and responsibility in a way pressurises women to continue playing this role many a times with a total neglect of their own health and well being. Contraception, a different way of looking: When looking at any issue related to medical research into contraceptive methods, this has to be the

background and understanding. The differentiation between any drug research and contraceptive research has to be highlighted and borne in mind. Along with this we also need to really understand what we mean by contraception. Contraception is defined as prevention of conception. Since conception cannot take place if there is no fertilisation and the latter cannot occur if the ova and the sperm are prevented from mating with each other the simplest method of contraception would seem to be that which prevents the sperm and the ova from meeting. It could be through prevention of the mating between the man and the woman or through ways in which the sexual interaction takes place without the possibility of such a mating or again it could be simply by use of barriers in the path of the ova and the sperm. Traditionally people have used many such methods. It would have been a step forward to emphasise these kinds of methods and develop them so that they become easily accessible and usable by most people on their own. The direction of development that contraceptive research has presently taken, however, has been through prevention of the formation of the egg and (theoretically) the sperm. This has meant intervention in the natural hormonal cycle of women disrupting their normal body behaviour. It has meant long term disruption of the natural functioning of the body. It has meant consuming chemicals all the year round for preventing fertility of two days in a month.

Including men: Besides the damage done to women's bodies and health, this has also resulted in emphasising the biased social norm that reproduction is on the whole women's responsibility. Men's responsibility in contraception has been ignored by freeing the 'process of contraception from the act of sexual intercourse and locating it in the control over women's bodies. So much so that even the campaigns against harmful contraceptives have been made out to be 'women's issues' and dealt with accordingly, Contraception has remained women's needs and this need has been exploited by the various agencies to have long term impact and effect-on the whole population.

Basic ethical concern would demand that we take cognisance of the fact that reproduction involves both the man and the woman and hence give them both the joint responsibility of contraception. Considering and acknowledging the equal role played by men in the process of conception and ensuring their responsibility

in the process would be a step forward for biology and medicine. This would be going beyond the norms of society and hence probably help change or affect the norms of society also. At present technological 'advances' condone the regressive attitudes and norms in society and in a way strengthen them. Society looks at men's contributions and involvement in a very limited manner. Unwed mothers are taunted at and not accepted in society. The man in that case never comes to the forefront. When it is the question of fathering within the norms and institution of marriage, however, the man becomes the natural guardian with the assumption that the child borne by his wife is his. In this situation as far as the child goes the man is fully responsible in the socio-legal sense. Determining or acting to have or prevent conception of that child, however, remains woman's task. The cafeteria of methods is available only for her. This is medicine's contribution to maintaining the discrimination in society.

the case it is but essential that the contraceptive method be such that it not only help prevention of conception but also provide protection from STDs. The present day researchers and contraceptive research funders have very queer understanding of the issue, Recently some representatives of the campaign against the anti-fertility 'vaccines' had a meeting with the IDRC, a Canadian organisation funding research in India and to the Population Council on the anti-fertility 'vaccines'. When the women from the campaign questioned the IDRC officials about AFV users not being protected from STDs and AIDS, their health scientist replied. "For that condoms can be used!" But then why use the AFV? To this question there was no reply. This is not only carelessness on the part of the promoters of such research but also a callousness towards people in general and women and their health in particular. For women there is a particular dimension of the STDs. The socio-economic situation affects the overall health of women making them more prone to the diseases. The cyclical nature of the fertility cycle also has its own role to play in greater susceptibility of women to these diseases. Attention is anyway being paid to STDs because of the danger of AIDS to men. Here too after having looked at the issue of 'Women and AIDS' by just blaming the sex workers, now it is being realised that more women are getting infected by AIDS due to the constant exposure to it through unprotected intercourse with their lawfully wedded husbands.

The methods do not rely on men's co-operation and hence do not dare to intervene in the sexual act. Here men are looked upon as persons with uncontrolled sexual desires and passions incapable of taking responsibility and restrained action. This is not a very enviable condition to be in but that is what is propagated in society and accepted by not only science but also all people be they men or women. This kind of uncontrolled lust associated with men does not allow them to grow as persons,' as humans. Society either thrusts all responsibility on men or believes that they are incapable of accepting any responsibility and that is not a very respectable understanding of oneself as a human Not able to have any control over their bodies, or on their relations with their partners, suffering from ill-health in the being. bargain, women are the ones who are most disempowered by the .use of such contraceptives. Use of contraceptives Ethics of a process also in a sense determine the norms of should be a process of acquiring some control and the process. In the ethics is incorporated the vision of the understanding of one's body and life. If any method makes society. Believing that contraception has to place equal the process disempowering, can such a method be ethically responsibility on women and men is setting the norms for allowed? what is ethical and ensuring that all methods are accepted, rejected, developed or ignored after having been tested on "Providing more choice" is the catch phrase under which this norm of ethics would be the minimum that could be the introduction of many similar methods is justified. demanded from any ethical review of contraceptive Having more methods to choose from is made out to be research. empowering even though most of these methods are such that they can be used without the knowledge of the partner Beyond preventing contraception: by a woman totally at her own initiative and responsibility. In fact this is an argument used in favour of the method. Sexual interaction as is considered the norm in today's We do not think that being able to prevent pregnancy by a society has another biological expression other than reality method that cannot be seen by anyone else is any positive and that is increased exposure to or proneness to sexually achievement for a woman. It does not help her gain any transmitted diseases. It is obvious and yet important to note autonomy and nor does it offer her any real choice as far as that methods that help gain protection from STDs also help her life goes. prevent conception. When this is

Can any method of contraception that merely prevents pregnancy give any strength to women just because it claims to provide 'more choice'? How can a method that results in a woman bleeding for twenty days in a month and further being penalised by her partner for not being able to satisfy his sexual desires, be even considered as a contraceptive? How can a method whose reversal in case of problems is not possible until-the effect wears off be considered empowering? How can a method that disrupts her normal cycle" and patterns and affects her overall health and the family’s routine in the process be passed ethically? Contraception is women's need. It should equally be men's need. And Medicine should contribute towards meeting this need of women and men through an understanding that is guided and moulded by the above concerns. All methods, new and existing, have to be evaluated for their ethical validity on the basis of such criteria which arise purely from the point of view of how we look at contraception. A proper selection at this stage and a clear understanding about why at all there is a need for contraceptives will automatically reduce the problems that then arise at the level of the design of the research and the actual conducting of the clinical trials or even at the stage of distribution of the method. When the safety, integrity and respect of women and their bodies is at the core we can never have research designs that look only at prevention of fertility as the major achievement and the return of fertility as the yardstick for gauging the safety. We will not have a reductionist viewpoint which believes that all effects of the synthetic chemicals on the body can remain very localised and specific and hence there is no need to look at anything more than the effect on fertility. When safety of women would be the primary criteria, research designs would not be altered through steps like elimination of animal trials and intensive clinical trials. In fact the stress would be on conducting research to as

much depth as is possible before introducing them for large scale use. A proper follow up of the participants in the trials, detailed information to them about the possible effects, compensation to them in case of any problems, proper selection of 'volunteers' - all of these would get included automatically. The concern for safety would also mean that instead of interfering with the cyclical reproduction, its knowledge would be used to help women understand their body and gain co-operation from men to have safe and efficient methods of contraception. Distribution would be part of the research .and so also would be the methods of dissemination of all the information that is collected to the would be users of the method. There would be no question of not taking informed consent, in fact instead of hiding facts the emphasis would be on letting women and men know as much as is known so that they become participants in the process and not mere objects of hi-fi scientific research.

In essence we wish to state that it is the understanding and view of contraception that determines the' nature of contraceptive research. And we do believe that for an ethical approach to contraceptive research this understanding has to shift from its population centeredness to becoming people centred. The emphasis has to shift from the technology and the aggressive attitude of gaining control over fertility through it to a co-operative understanding of not only human bodies but also towards human relationships as well. Starting from reacting to and agitating against one method after the other, the campaign has matured and grown and is striving to arrive at an overall understanding. While we continue to evaluate the merits and demerits of the technical cases of each of the various contraceptive methods, it has also become urgent today that 'all of us women and men concerned about the issue individually and collectively help towards evolving and strengthening this understanding further.

Please confirm your participation: The second announcement for the Meet gives a list of background papers. Copies of these papers are available at the Convenor’s office. If you are planning to participate in the meet, please confirm your participation and receive background papers. Those who are not participating but would like to receive these papers are requested to send Rs. 25/- to meet the cost of papers and postage. —Convenor

The XXII Annual Meet of the MFC, December 27-29, 1995, Wardha Second Announcement The Annual Meet of the MFC will take place as scheduled at Yatri Nivas, Sevagram, Wardha. The theme of the Meet is "Ethics in Health Care". As you receive this second announcement, it would be time for you all to make bookings for your travel and also to let us know whether child(ren) will be accompanying you so that we can have some estimate of making arrangement for the creche.

In order to get the background papers in advance, you are requested to confirm your participation by sending a letter to the Convenor’s address. The following articles are suggested as background papers (they are arranged topic wise, MFCB is MFC bulletin and ME is Medical Ethics). AIDS: (1) "The Patient with AIDS", Sunil K._ Pandya (ME, vol. 1, No.3, pp 1-3). (2) "The Patient with AIDSA Response", Eustance J. DeSouza (ME, vol. 1, No.4, pp 5). (3) "AIDS & the Law - Opportunities & Limitations", Michael Kirby (ME, VoL2, No.1, pp 3-6). (4) "AIDS: ICMR Guidelines" (ME, VoL3, No.2, pp 35). Hysterectomy in the mentally handicapped: (5) "Suggested Guidelines for hysterectomy in mentally handicapped women" (ME, vol. 1, No.4, pp 1-3). (6) "Hysterectomy in the mentally handicapped", PARYAY (ME, VoI.2, No.1, pp 6-7). Organ Transplantation: (7) "Ethics of organ transplantation", Sanjay Nagral (ME Vol.3, No.2, pp 19-22). (8) "Ethical problems in renal transplantation", M.K. Mani (ME, VoI.3, No.3, pp 39-41). (9) "Problems arising from renal transplantations", Thomas George (ME, VoI.3, No.3, pp

If any of you have relevant articles/papers to share and feel they are useful for any of the themes of the Meet mentioned in the First Announcement do send them to us, while we still await some original papers commissioned for the Meet. The articles taken from the Medical Ethics for background papers have been made available to us by the Forum for Medical Ethics Society, Bombay. We are thankful to Forum for the same. The next lot of background papers (5 to 7 more papers) presently under preparation by authors will be published in the MFC bulletin (This issue carries three). Do inform us about your participation and especially if children and spouses will be accompanying so that we can make appropriate arrangements. If you need return reservations, you should write directly to: Mr. Shantaram Phokhmare Yatri Niwas Sewagram Wardha-442102 Maharashtra

For your return tickets please send to Mr. Phokhmare the appropriate amount of the ticket with an additional amount of Rs.25/- as booking charges and also let him know the relevant details - name, age, sex, destination, train name, 42). Health Policy and Health Care System: (10) date of travel, class of travel etc. "Problems in the health care system - a brief appraisal", V. Murlidhar (ME, vol. 1, No.2, pp 1-2). Research: (11) "Ethics Please also note that the winter in Wardha is quite severe in Human Medical Research: Views of a non-medical person", so please carry the necessary woollens. Participants will R.P. Ravindra (ME, vol. 1, No.4, pp 67). (12) "The case have to meet their own expenses for travel, registration against anti-fertility vaccines", Forum for Women's Health charges and pay for the lodging and boarding charges at (MFCB, No. 218, May 1995, pp 1-8). Principles of Ethics, cost which are very reasonable. Those wanting to share and Law and Human Rights: (13) "Medical Ethics: as display any material, information etc. are welcome to do so presented by Charaka, Susruta and other ancient Indian at the Meet. physicians", Sunil Pandya (ME, VoL3, No.1, pp C-I-IV). (14) "Medical Ethics: We hope to see you at the Meet. General principles", Anil Pilgaokar (ME, VoL3, No.2, pp C-V-VIII). (15) "Law, ethics and medical councils: With all good wishes, evolution of their relationships", Amar Jesani (ME, Vol.3, No.3, pp C-IX-XII).

Ravi Duggal Convenor Bldg. 4, Flat 408, Vahatuk Nagar, Amboli, Bombay-400058

TO ALL MEMBERS OF THE MEDICO FRIEND CIRCLE Invitation to the Annual General Body Meeting (AGM) The next AGM of the MFC will be held on Friday 29th December 1995 at the Yatri Niwas, Sewagram, Wardha from 11 am to 4 pm. All members are invited to be present at the meeting which will be held after the Annual Meet whose theme is "Ethics in Health Care". The First announcement was sent two months ago and has also appeared in the MFC bulletin.

The agenda for the AGM is as follows: 1.

Convenor's Report for 1995

2.

Passing of 1994-95 accounts

3.

Review of the bulletin and planning issues for the next year

4.

Selection of the theme for the next Annual Meet and its planning

5.

Reporting by cell/local group co-ordinators

6.

Election of new executive committee members in place of those who retire

7.

Selection of the new convenor for the period April 1996 to March 1998

8.

Response to the invitation by the Independent Commission of Health to become its associate member

9.

Any other matter with the permission of the chairperson

All members are requested to attend the AGM and the Annual Meet being held from the 27th·to 29th December 1995 at Sewagram. With all good wishes, Ravi Duggal Convenor

ANNOUNCING A NEW PUBLICATION IN ENGLISH 'NA SHARIRAM NADHI' My body is Mine by Sabala & Kranti It is a friendly and practical guide for women health activists and others to introduce 'self-help' into their life and work.

The book documents training experiences in self-help for women's health. It highlights the class/caste and gender biases in the medical system and links women's ill health and disorders to her subordinate status in family and society. It exposes the myths created around our bodies and provides the information that helps us to be sensitive and respectful towards it. This book looks at women's health as a basic human right. Contributory price Rs. 80/- plus Rs. 25/- for postage. Send your payment by DD or MO in favour of: Ms. Jeanette D'souza A-201, Vasant View D'monte Lane, Malad West Bombay-400064 Sorry, no outstation cheques.

(contd. from p.6) At the bottom of it all, our reading of reality, the frameworks we use, are often deep ethical issues. Organisations, low cost or otherwise, making modern medicines, are active supporters of the modern science paradigm, of a positivist paradigm of a reality out there. Our continuous ongoing production and distribution of drugs, often to public applause, comes across as an unproblematic espousal of modern medicine and modern science. Leave aside the debate of the hegemonic nature of modern science and medicine, and the violence it has done to a humane ethos. What is it we can do about the gigantic industrial chemical complex upon which medicines are based, and about what it does to our environment? How do we continue to sell medicines endorsed on basis of doubleblind clinical trials, or

otherwise on research methodologies, whose ethicality is suspect? At the heart of these dilemmas are questions of method in science, and of the nature of development and what they together do to human cultures. Unless one makes some progress in these issues, consistency of means and ends, one of the pillars of ethics, would appear to be in some kind of a soup. With it, drug production, low-cost or otherwise, does appear to be a bit of a flawed enterprise.

(The author wishes to thank T. Srikrishna for his comments on the draft. Also, a few unnamed others have been helpful in clarifying ethical issues routinely. The usual disclaimers apply.)

Editorial Office: Sathyamala, C/o Mr. Sultan Basha, B-7 (Extn.) 12-A, Safdarjung Enclave, New Delhi-110 029 Subscription Rates: Inland (Rs.) Annual Life Individual

30

300

Institutional

50

500

Asia (US $)

6

75

Other Countries (US $)

11

125

Please add Rs. 10/- for outstation cheques. Cheques/money orders to be sent in favour of Medico Friend Circle, directed to Anant Phadke, 50, LIC Quarters, University Road, pune-41l016. Edited by Sathyamala, B-7, (Extn.) 12-A, Safdarjung Enclave, New Delhi-110029; published by Sathyamala for Medico Friend Circle, 50 LIC Quarters, University Road, Pune-411 016; Printed at Kalpana Printing House, 1-4, Green Park Extension. New Delhi-110016.

Views and opinions expressed in the bulletin are those of the authors and not necessarily of the organisation.


