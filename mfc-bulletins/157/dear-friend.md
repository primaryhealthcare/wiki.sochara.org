---
title: "Dear Friend"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Dear Friend from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC157.pdf](http://www.mfcindia.org/mfcpdfs/MFC157.pdf)*

Title: Dear Friend

Authors: Shirdi Prasad Tekur

157 medico friend circle bulletin November 1989

Aluminium Phosphide: A Pesticide that kills hundreds SG KABRA

Aluminium Phosphide is a fumigant pesticide. When ingested, it is a deadly poison. Available in 3 Gm pellets, even half a tablet is lethal. No antidote is available.

Aluminium Phosphide: A Fumigant Pesticide A technical note circulated by P. R. Vishmbharan, Manager (Technical), Central Warehouse Co-operation, New Delhi, states: "This is a versatile fumigant available in solid tablet form. Each tablet weighs 3 Gms and releases 1 gm. of phosphine which is insecticidal on reaction with the moisture content in the atmosphere. The tablet contains 55% AI. Phosphide and 45% Aluminium Carbamate. When AI. Phosphide with moisture evolves phosphine, Ammonium Carbamate produces Ammonia and Carbon Dioxide which act as extinguishing agents for phosphine which is inflammable. The reaction is as under:

1) AIP+3H20 = PH3 + Al (OH)3 2) NH2 COO NH4 2NH3 + C02

=

The residue after the reaction is Aluminium Hydroxide which is harmless and can be removed easily. This fumigant can be used on all food grains and other agricultural commodities as Phosphine is relatively insoluble in water and oil and it can kill all stages of insects. It is safe to handle, is available easily in the market and the residue is practically nontoxic. The reaction takes place only after an hour of exposure to atmosphere"

Three things may be noted in the above statement: —that it is recommended as a versatile fumigant pesticide. —that it is safe to handle -that it is easily available in the markets.

Aluminium Homicide

Phosphide:

A

Pesticide

In, 1983, 35 deaths due to aluminium phosphide were reported from Udaipur Medical College. No one who swallowed a tablet of aluminium phosphide survived, including a politician who had swallowed it to demonstrate that the allegations about this popular pesticide were baseless. In all these cases the alleged brand of the pesticide was 'Celphose' (PTI despatches dated April! 13, May 10 & 20 and July 22 date lined Udaipur). In 1985, deaths due to aluminium phosphide ingestion were reported from Ratlam, Indore and Jaipur in The Journal of Association of Physicians of India. 'Celphose' was the brand named in all these cases. In 37 percent of the cases reported, the cause of ingestion was accidental.

In The British Medical Journal of 1985 15 cases were reported from the Postgraduate Institute of Medical Education & Research, Chandigarh. 15 cases of aluminium phosphide poisoning (in just 4 month) were reported from All MS, New Delhi and 55 cases from Medical College Hospital, Meerut, in The Journal of Association of Physicians of India A total of 285 cases were collected and reported from 6 teaching institutions in Delhi and its vicinity from AIIMS, New Delhi, in The Lancet (1988). In this series, 114 were from Rohtak alone. The report was aptly titled 'Epidemic Aluminium Phosphide Poisoning in Northern India' The authors elucidated several reasons to assert that 285 deaths in just one year (1987) was only a fraction of the total.

The Times of India, New Delhi of June 12, 1988 reported 150 deaths in Jaipur in '87-88'. Another report in a June 1988 issue of 'The lancet' carried the title 'Aluminium Phosphide: Worse than Bhopal: From the aforesaid the following facts emerge: —deaths due to Aluminium phosphide ingestion are wide spread —only a fraction of these cases come to light —the majority of deaths (60%) are impulsive suicides. —substantial percentage (35%) is accidental. —instances of alleged al. phos. homicides are not rare. —it is a deadly poison for which no antidote is available. —easy availability of aluminium phosphide at very cheap price in 3 gm tablet in small packets as a house-hold pesticide is largely responsible for situation,

Experimental Background Availability of a chemical, especially a toxic chemical is presaged with animal and other experimental studies to catalogue its possible ill effects on body and the measures to counter such ill effects on body and the measures to control such ill effects on inhalation have, therefore, been studied in detail, Measures to counter its ill effects to inhalation are also detailed. Unfortunately, however, its effects when ingested have not been studied and the treatment 0 aluminium phosphide poisoning consequent to its ingestion is not known.

It is not realised how explosive is the liberation of phosphine gas when the AP tablet COO1es in to contact with water or worse still, gastric juice in the stomach. In trying to conduct dog experiments at JLN Medical College, Ajmer, when we pushed a powdered aluminium phosphide tablet with water through a rubber tube already placed in the stomach of the dog, there was such a rapid liberation of the gas that everyone had to rush out of the lab abandoning the experiment altogether. The experience was so unnerving that the technician refused to assist in future experiments of this kind. Later, when insertion of the tablet directly in the stomoch was attempted, the liberation of phosphine gas was again so explosive that the wound in the stomach could not even be stitched. It was only after first placing a purse suture and then pushing a tablet through a punctured would inside the suture and quickly pulling the purse string tight as soon as the tablet was dropped in the stomach, that the experiment could be conducted to any degree of satisfaction.

This experiment showed that, half an hour after the tablet has been introduced into the dog's stomach there occured a sharp fall in the blood pressure which co u Id not be reversed. The experiments could not be completed for various reasons.

It may be noted from the above that—

—the liberation of phosphine gas when the aluminium phosphide tablet reaches stomach is so explosive that before anything can be done enough of the poisonous gas is already liberated. —the liberated gases, phosphine and ammonia are very quickly absorbed from the stomach into the blood stream. —the exact mechanism that leads to peripheral

circulatory failure has not been elucidated, —the methods in which the ill effects could be reversed have not been worked out.

Amount of Aluminium Phosphide needed by the Storage Agencies. As per the recommended dose of 3 tabs, of Al. Phos. per tonnes of grains for 460 lakh tonnes of grains 460 tonnes of pesticide would be needed, that too if this is the only pesticide used. Even for the total food grain production of the country of 1500 lakh tonnes, the total Al. Phos. required would be 1500 tonnes. It may be seen from the aforesaid—

A patient who has ingested aluminium phosphide, if brought early is fully conscious and coherent, has some air hunger, is restless and thirsty and has epigastric discomfort. There is then a sudden fall of blood pressure, the peripheral circulation fails and patient soon becomes comatose and dies.

At postmortem, the only finding is marked congestion of all the peripheral vessels especially those inside the abdomen.

that the amount of Aluminium Phosphide produced is far in excess of what the public storage agencies need.

Conditions of License The team of Chief Toxicologist and Legal Advisor of the Ministry of Agriculture, Govt. of India, which had visited Udaipur in 1983 to investigate reported deaths from Aluminium phosphide, had asserted that this pesticide is licensed

Production of Aluminium Phosphide The Status .Report on Pesticide residues vis-a-vis Consumer Production (1987) prepared by DST and VHAI reveal the following production figures for the pesticide in question as on 1.8.85:

Capacity

Sr. No.

Manufacturer

(Tonnes per annum) Licensed

Installed

1. Excel Industries Ltd.

300

300

400

400

1000

1000

120

120

Product

FUMIGANTS

1.

Aluminium Phosphide

Swadeshi Chemicals, Bombay 3. United Phosphorus. Vapi Bhart Pulverising Mills, 4. Bombay Punjab United Pesticides 5. (Registered with DGTD) 2.

Total—

150 1820

1820

Food Grain Storage Capacity of Public Agencies (India 1987, a Govt. of India Publication)

Food Corporation of India

211.96

lakh tonnes

Central Ware-house Corporation

57.87

,,

State Ware-house Corporation 83.21 Custom Bonded Ware-house Corporation 6.31

,, ,,

Total—

459.35

lakh tonnes

150

to be produced only for the use of public storage agencies. In any case, aluminium phosphide is banned from being sold in the market as a house hold pesticide.

8.

14. Revocation,

the Act. 4.

officers-The

Central

i) appoint a person to be the Secretary of the Board who shall also function as Secretary to the Registration Committee;

Insecticides Act 1968 The following are the relevant provisions of

Secretary and other Government shall

suspension

and

amendment

of

licenses

The Central Insecticides Boards

1) The Central Government shall as soon as may be constitute a Board to be called the Central Insecticides Board to advise the Central Government and .State Governments on technical matters arising out of the administration of this Act and to carry out the other functions assigned to the Board by or under this Act. 2) The matters on which the Board may advise under sub-section (1) shall include matters relating to—

a) The risk to human beings or animals involved in the use of insecticides and the safety measures

1)

If the licensing officer is satisfied either on a in reference made to him in this behalf or other wise that— b) the holder of license has failed to comply with the condition to which the license was granted or has contravened any of the provisions of this Act or the rules made the under,

Then, without prejudice to any other penalty to which the holder of the license may be liable under this Act. the licensing office may, after giving the holder of the license an opportunity of showing cause, revoke or suspend the license.

necessary to prevent such risk;

b) The manufacture, sale, storage, transport and distribution of insecticides with a view to ensure safety to human beings or animals.

3) The Board shall consist of the following members, namely i) The Director General of Health Services ex officio, who shall be Chairman; ii) Drug Controller, India. ex officio

26. Notification of poisoning- The State Government may, by notification in the official Gazette, require any person or class of persons specified therein to report all occurrence of poisoning (through the use or handling of any pesticide) coming within his or their cognizance to such officer as may be specified in the said notification. 27 Prohibition of sale, etc. of insecticides for reasons of public safety- If, on receipt under Sec. 26 or otherwise, the Central Government or the State Government is of opinion, for reasons to be recorded

in writing that the use of any specified in sub-clause (iii) of CI. (e) of Sec. 3 are any specified batch thereof is likely to involve such risk to human beings or animals' as to render it expedient or necessary to take immediate action then that Government may, by notification in the official Gazette, prohibit the sale, distribution or use of insecticide or batch, in such area, such extent and for such period (not exceeding sixty days) as may be specified in the notification pending investigation in the matter.

—Though it is not to be marketed as a house hold pesticide and is not to be made available in open market as per their condition of license, the manufacturers have not been responsible for contravening the law.

The Insecticide Rules 1971

—Thought duty bound to investigate the police, in case of accidental deaths and alleged homicides by aluminium phosphide do not pursue these cases to find out how the poison reached the deceased and who made it available.

41. Manufacturers etc, to keep sufficient quantities of antidotes and first-aid medicines. The manufacturers and distributors of insecticides and persons who undertake to spray insecticide on a commercial basis (hereafter in these rules referred to as operators) shall keep sufficient stocks of such firstaid tools, equipment, antidotes, injections and medicines as may be required to treat poisoning cases arising from inhalation, skin contamination, eye contamination and swallowing. The following are obvious from the aforesaid— —Though envisaged in the law that all deaths due to pesticides are to be notified to the specified authorities, no steps have been taken to implement it. —Through the Director General of Health Services of India and the Drugs Controller of India are Chairman and Member, respectively, of the Central Insecticides Board, they have either not cared to take cognizance of the large number of aluminium phosphide related deaths reported in the medical literature or have nor advised the Govt. about it.

—Inspite of overwhelming evidence that this pesticide may be reaching the market clandestinely from the public storage agencies no attempt has been made to investigate this.

—Almost all the deaths reported from aluminium phosphide poisoning in the medical literature are from one particular brand of the pesticide.

—The pesticide is readily and freely available at very low cost, in pallet and packed size for obvious home use. Its presence in the home leads to largely preventable impulsive suicides and accidental deaths due to its ingestion. In terms of the number of people killed by aluminium phosphide in this country every year, the tragedy is colossal. The remedial measures required appear to be relatively simple, if only we have the will. People killed by pesticides are not pests. There is no other pesticide in which causal relationship to death is so obvious or so direct.

Director Research SDM Hospital JA/PUR 392015

* *

Dear Friend, Congratulations on your proaching the subject of Medical Pluralism in the Sep/Oct 89 mfcbulletin, for the 20th time in 156 bulletins (to up date your tally). Now get ready for the Bouquets, Brick bats and Brass tacks.

Bouquets: a) Your stress on 'open-minded scientificity' of mfc is a laudable attempt at covering up the 'Allopathic culture bias'. Pardon, the slip still shows'

b) Using the term 'medical systems' in generic from while naming each system specifically cannot but be accepted. The meander through 'terminologies' was indeed good strategy. c) 'Revaluation' and 'Integration': i) the 'concepts' narrow down to disease' and therapeutics ii) the 'why l' boils down to pinning down Ayurvedic therapeutics and the 'how l' tries to be vaguely 'scientific,' d) The rest frankly, can rest. Could we attempt 'pluralism' and rise above 'dualism'? 2. Brickbats: refer to 'Bouquets' again 3. Brasstacks:

3,1 The term 'Allopathy' came up with the advent at 'Homoeopathy', underlining the difference in approach to health and disease. Both are basically 'western' or 'European' systems and Homoeopathy calls itself 'modern' (while referring to Allopathy as the 'older' or 'traditional' System) in most of its standards text books. 3.2 All non-Allopathic systems have an inbuilt 'holistic' approach to the health and disease. 3.3 All non-Allopathic Systems base their concepts on 'Vital force' / Prana / Tchi etc. as the fundamental factor operating in health or disease. Allopathy has unfortunately relegated this concept to a paragraph in the initial chapters of 'Physiology' teaching, 3.4 The orthodox scientific approach is to dismantle a complex system and focus on its fundamental components – quarks, genes, or whatever.

This is mathematically explainable by linear equations. In reality, most natural systems are not linear: Often they don't move in a straight line from cause to effect but in cycles, with effect feeding back on the cause and perhaps amplifying it. The weather and biological systems are outstanding example of this 'chaos'. Kauffman's computer model of the human genome has 10,000 variables interacting in a non linear way yet, it manages to generate order and settles into one of only 100 stable configurations. Chaos has its own pattern, a peculiar kind of order. It is being appreciated in the activity of the heart and brain and seems necessary for their healthy functioning in the milieu of constantly changing variable. Do the empirical or intuitive ideas of the non-allopathic systems understand / explain utilise it better? 3.5 Health is a very wide, where disease and therapeutics play a relatively minor role but have managed} been allowed to remain 'centre-stage'. And it is here that all quarrels of the different medical systems lie. Preventive, Promotive and rehabilitative aspects of health need proportionate emphasis and could form the basis of attempts at 'Integration'. The National Health Policy 1989 Statement plea to 'integrate services... especially in regard to preventive, promotive and public health objectives...' shows a mature understanding of health realities. 3.5 In India in 1984 only 5-6% of the population were able to afford or procure the modern drugs they needed; another 25% had limited access to essential drugs. Is it worth while our expending energies on 'medicine'? Or, do we sensibly look at 'health' in all its aspects? 3.7 There are many other questions which come up as one thinks more on this. Let us keep it for later, and try not to get further away from reality in our enthusiasm to 'integrate'.

Shirdi Prasad T ekur Member mfc

**

XVI Annual Meet of Medico Friend Circle 26th, 27th, and 28th, January. 1990. NUCLEAR ENERGY - CONTROVERSIES & APPROPRIATENESS

MEDICO FRIEND CIRCLE BULLETIN

It has now become almost important that we focus our attention on the problems posed by nuclear technology as we enter and pass through a new phase of the Atomic Age. Despite the fact that reactor technology is beset with hazardous shortcomings that threaten the health and well being of the people, nuclear power plants for 'peaceful' purposes are spreading throughout the world including India. Because of this proliferation of power plants and also of nuclear weapons, the likelihood of nuclear war, the most ominous threat to public health imaginable, b3comes greater every day. Moreover low level radial ion technology is increasingly used in medicine and other purposes such as food disinfection, without adequate research on its effects on human body.

Editorial Committee: Abhay Bang Anil Patel Binayak Sen Dhruv Mankad Dinesh Agarwal Padma Prakash Sathyamala Vimal Balasubrahmanyan S P Kalantri, editor Editorial Office: Block B; 8. Vivekanand Colony. Sevagram, Wardha442 102 Subscription/Circulation Enquiries: UN Jajoo, Bajajwadi. Wardha-442001 Subscription Rates: Annual Life Inland (Rs) a) Individual b) Institutional Asia (US dollars) Other Countries (US dollars)

30 50 6

300 500 75

11

125

Please add Rs 5 to the outstation cheques.

Cheques to be drawn in favour of MFC Bulletin Trust. Published by Ulhas Jajoo and SP Kalantri for MFC bulletin trust, 60 LI C quarters. University road, Pune 411 016 and Printed at Samyayog Press, Wardha. Views and opinions expressed in the bulletin are those of the authors and not necessarily of the organisation.

To debate the various aspects of health implications of this urgent topic Medico Friend Circle has planned to organise its next annual meet on this theme. In a preliminary discussion held in the core group meeting of mfc, following background papers have been planned tentatively. 1. Basic concepts of radiation hazarad 2. Safe levels of radiation ICRP. How are these decided? Issues of minor & major health hazards 3. (a) Health implications of Nuclear Fuel cycle. (b) Health Hazards of over ground explosion or leaks in reach 4. Summary of some debate on previous nuclear hazards controversies. 5. Indian scenario .nuclear plants, problems and safety records. 6. Comparative study of health implication in (a) Hydroelectric project (b) Thermal Plants. 7. Implications of low level radiation on radiologists and patients through x-ray. Any other background paper on this topic from you is welcome. You may also write an article on this theme for publication in the bulletin. The paper should reach us before 30th November, 1989. Attempt will be made to circulate background papers well in advance. No paper reading is allowed in the meet. As usual participants pay for their own lodge and board during the meet besides nominal registration fee to cover cost of background papers. Those interested to participate can write the convenor regarding venue and other details.

NARENDRA GUPTA Convenor


