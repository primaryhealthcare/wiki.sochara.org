---
title: "UID: Facility or Calamity"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled UID: Facility or Calamity from medico friend circle bulletin. You can find the original in the PDF at [MFC342-344.pdf](MFC342-344.pdf)*

Title: UID: Facility or Calamity

Authors: 
Jean Dreze


medico 342- friend 344 circle bulletin August 2010 - January 2011

Towards Universal Access to Health Care in India Concept Note for Medico Friend Circle, Annual Meet 2011 - Abhay Shukla, Anant Phadke, Rakhal Gaitonde1

I. Introduction and Basic Concepts In the 21st century, Health Care should be regarded as a basic human right. Since the second half of the twentieth century most of the developed countries (with notable exception of USA) have progressively achieved or have nearly achieved Universal Access to Health Care (UAHC). More recently some developing countries like Thailand, Brazil have also largely achieved UAHC. A few countries like South Africa, Cuba, and Venezuela have recognized Health Care as a fundamental right and have enshrined it in their constitutions. In these countries patients do not have to pay or pay only a token amount at the point of service. At the beginning of the twenty first century there is convincing evidence and experience in favour of and how to achieve a Universal Access Health Care system, there are no excuses for India not to move towards UAHC. During the last MFC annual meet in January 2010, we discussed barriers to UAHC in the Indian context. In the Annual Meet in January 2011 we would discuss major policies and health system changes needed to make UAHC a reality in India. For this purpose, we need to be clear about the meaning of UAHC and it’s implications, and the experience of other countries will have to be studied, and lessons learned from these experiences need to be applied within the context of specificities of the Indian situation. After World War II, thanks to the Keynesian policy of State intervention in certain spheres of the economy, Health Care was increasingly provided by the state or paid for by the State in developed countries. Thus in the second half of the twentieth century, UAHC has been achieved in developed countries mainly because the 1 Emails: <abhayshukla1@gmail.com>,<anant.phadke @gmail.com>, <rakhal.gaitonde@gmail.com>

commodity character of Health Care has been progressively undermined in most of these countries. The State Health Care Services are not commodities. Moreover even when services of private doctors are sought, ‘Third Party Payment’to the doctors by the State means from the point of view of the patients, health care is not a commodity and is more of a social service or social wage. The US is a definite exception where health care continues to be largely a commodity. It is not incidental that amongst the developed countries, despite its large GDP and high per capita expenditure on health care, the record of access of the US to health care is the worst. So it’s more a question of curbing the role of the market in health care and less a question of availability of resources. Given this background, in this note, we have put forth a broad canvas of the various issues that need to be considered and positions that need to be formulated, to be able to argue that it is possible to make UAHC a reality in India in the coming decade. It is hoped that in the forthcoming MFC Annual Meet on UAHC, there will be a detailed discussion on the various issues covered in this note and the this meet will hopefully come up with a common, consensus declaration on Universal Access to Health Care in India; as an urgent social necessityas well as a dream that can be realized during the second decade of the 21st century.

Defining Universal Access to Health Care Some key features of a UHC system are outlined below, which is modified from a section in the Report of the Knowledge Network on Health Systems, WHO Commission on the Social Determinants of Health (June 2007): Universal coverage is achieved when the whole population of a country has access to the same

2 range of quality services according to needs and preferences, regardless of income level, social status, gender, caste, religion, urban/rural or geographic residency, social or personal background, which is accessible as per need at all times. Such a system offers a comprehensive range of curative/symptomatic, preventive-promotive and rehabilitative health services at primary, secondary and tertiary levels, including common acute life saving interventions. This report notes, inter alia, that: “Such systems offer particular benefits to the poor by improving their access to health care, protecting them from financial impoverishment and ensuring that the rich pay a higher proportion of their income to support health care provision. Universal coverage approaches also generally require less administrative capacity and are more sustainable than targeted approaches. “Parallel bodies of cross-national epidemiological and economic evidence demonstrate that health care systems with universal coverage address economic inequality by re-distributing resources from the rich to the poor. Such systems tend to accord public funding a central role, charge no or very low fees for public services, offer a comprehensive set of services (with a clear role for primary level care in helping patients navigate the use of referral services) and regulate the private sector (including commercial providers and insurers and, in low-income contexts, informal providers) to protect equity gains. Additional strategies are also likely to be necessary fully to address the particular barriers to accessing care that disadvantaged and marginalized groups face.” Some basic principles and suggested operational tenets regarding a system for Universal access to health care are outlined in Annexure 1. Here we would only point out some of the implications of the Principle of Universality.

UAHC and Equity Universal Access to Health Care implies that everyone within a country can access health services on the basis of need. For this to happen, health services will have to be organized in such away that everyone will get the the same range of services as per need. There is thus an inalienable relationship between equity and universality. UAHC does not mean that all citizens will have access to each and every kind of health intervention. But commonly required health services as well as life saving

mfc bulletin/Aug 2010-Jan 2011 interventions needed in acute cases should be ‘universally’ available. We cannot expect a ventilator in every PHC but it should be available at about an hour’s distance in the CHC / RH for some snake bite cases (for example) that require it. For other services one essential guiding principle for planning UAHC remains: everyone within a country can access the same range of services on the basis of need. One of the implications of this principle is that the funding of an area for health care services should be on a per capita basis. Today the per capita public health funds for rural areas are only a fraction of per capita funds for urban areas. It would be quite a challenge to develop an equitable health care system. For example, as regards equity in access, a new-born baby immediately after birth can be attended by a trained dai or nurse or a doctor or a pediatrician. Would this be same in cities, villages and remote areas? To take a second example, information to the patient can be provided to the patient/relative by a paramedic, or by a doctor or through a brochure/leaflet or through an interactive audio-visual source on the internet. Would it be same in cities, villages and remote areas? As a nation, as a society we will have to make a distinction between what is ideal, desirable and what is optimally possible in provisioning of various health care services. The principle would be - only those services which can be made available to all those who need it will be introduced in the publicly funded system, while trying to ensure that the range of services is as comprehensive as possible. Secondly all aspects of health care (curative/ symptomatic, preventive-promotive, and rehabilitative services) will have to be covered by the system. Given this background, to make a beginning even within the limitations of a market economy, what would be the features of a universal and equitable health care system? It is clear from the evidence and from the experience of the many countries which have attempted to reach UAHC, as well as from the point of view of equity that the following principles will be core to the achievement of UAHC: • Public funding must play a central role. India needs to move from current 20% of total health care spending financed through public funds, to a goal of 80% to be reached in coming 10 years by a quantum jump in the scale of public health care funding. • There would be a single payer mechanism for an organized healthcare system, integrating both public and regulated private providers who would be paid by a public agency to provide services to people. Policy and regulatory action should ensure

mfc bulletin/Aug 2010-Jan 2011 that private sector contributes to universal access. • No fees should be charged for services at point of delivery. Resources to be raised through the tax and social insurance route • A set of comprehensive health services should be offered to the entire population without targeting or exclusion.

Some Key Issues Regarding UAHC Keeping this overall context in mind, concerning UAHC in India we should address the following issues: 1) Who would get what services - Universal coverage requires that everyone within a country can access the same, defined range of services on the basis of need. This range of services would of course include Primary Health Care. But a range of secondary and tertiary health services would also have to be made available. For example, one can argue that, MRI is required commonly for many rural patients and hence an MRI machine should be planned for each district civil hospital and not only for big cities. (However, the limitation that a trained team may not be available in each district place will have to be overcome through systematic planning. Till then, modern communication system can be used to see that experts see the images and assist in diagnosis. Secondly, special free transport facilities will need to be provided to take patients from rural areas to the expert facilities.) . 2) Who would pay, how - Do we visualize progressive health tax or a general progressive taxation in proportion to income? General progressive taxation can be levied and an increased portion of this enhanced tax collection can be allocated for health care. In the present tax policy scenario, a separate tax for health (as for education) may also be necessary to assure that adequate resources are earmarked for health. Like Thailand shall we prefer the social insurance route to generate more resources, by universalizing ESIS as a National Social Insurance through which about half of the workforce (organized and unorganised) can contribute premiums through their employers? Also using earmarked taxes like financial transaction tax (a levy on capitalist accumulation of speculative wealth) and earmarking it for social sector could generate substantial resources. While designing the financing model, it should be ensured that - No one is denied care due to inability to pay or contribute - Large scale redistribution is achieved because the rich (and relatively healthy) cross-subsidize the use of health care by the poor (and relatively sick). One option for this cross subsidization could be in form of differential premium rates. But if it is going to

3 be based mainly on tax-based mobilization of funds, this particular mechanism of cross subsidization may not be very important. 3) How would the services be organized -What would be the role of the public system and private sector and the relations between the two? Would there be any continuation of fee-for-service private health care outside the universal access system? Till the time that optimally best services can not be made available to each citizen from the universal access system and till the market economy continues, would separate privately paid services also have to be allowed? For example, supposing drug-eluting stents after angioplasty are superior to simple stents. In that case, till the former can be made available to all those who need them, will they allowed to be ‘purchased’ by the patients who can do so? This is different from allowing ‘luxury’ or ‘unnecessary services’. For example cosmetic surgery to improve aesthetic looks; like liposuction or to treat baldness through hair implantation etc. (different from rehabilitative plastic surgery). The availability of ‘higher’/’technologically more advanced’ services on payment in the same hospital/ clinic tends to ‘erode’ the whole system in the sense that the best doctors/nurses/resources tend to get attracted towards this ‘on payment’ system at the expense of the publicly funded system. We need clarity and consensus on this issue. What would be mechanism of mutual accountability of doctors with other doctors at lower and higher levels, with paramedics, with patients? Answers to these questions will have to be sought through our discussion within the specific context of the Indian situation.

Some Specificities of the Indian Health Care Situation While discussing the options for developing a system for UAHC in India, we need to keep in mind some specific features of the context: • There is a very large, predominant, highly stratified private medical sector (ranging from rural ‘informal’ providers to corporate hospitals). • Currently there is complete lack of regulation of medical practices – this involves large scale irrationality in health care, linked with massive wastage of resources and frequent exploitation of patients. • Public health provisioning is relatively weak and inadequate especially in most urban areas. In rural areas some basic public health framework is

4 defined but the emphasis is on preventivepromotive services, and the system generally falls short of health care needs of the population. • The middle class has largely opted out of publicly managed health services, and prefers to access various levels of the private medical sector. • The vast majority of workers are in informal sector with relatively small formal sector, making attempts to move towards social health insur ance challenging. With this background, which kind of policy measures would be required so that a system for UAHC can be realized in India? There would be three key aspects of designing a universal access system: • Financing, pooling of funds and combination of schemes • Service provision – defining and organizing roles of public and private providers • Governance and regulation, ensuring quality of care and participatory monitoring Let us now deal with each of these key aspects of a system for UAHC briefly.

II. Key Aspects of a System for UAHC in India A) Financing of UAHC and Pooling of Resources There is need to move towards building a few consolidated pools of health care financing. Experience of other countries shows that fragmentation, multiplicity of schemes must be overcome to move towards UAHC. There would be need for an overarching, publicly managed body which would combine and manage all these finances. The sources of finance may be as follows for various groups: • For middle class, tax payers and organised sector mandatory social health insurance mainly with contribution from employers and employees. • For unorganised sector workers, tax based funds plus some system of contribution from those employers / principal beneficiaries who use the services of these workers. • For the rural population, tax based funding linked with overall increased tax based public funds for health care. Overall, much higher level of public health funds would need to be mobilized to support the system. Once the system is in place, those private health facilities which remain out of the system, especially corporate / luxury

mfc bulletin/Aug 2010-Jan 2011 hospitals should be heavily taxed and all subsidies to private medical sector would be completely ended, leading to major savings of resources.

How much would be the Cost of Universal Health Care? According to various estimates, a system for UAHC in India would require in the range of 3 to 5% of the GDP. There is a need to go through the details of such exercises and to discuss the basis of these calculations. The National Commission on Macroeconomics and Heath through some detailed exercise had worked out that health care expenses of per capita Rs.1160 would have been adequate in 2003-04 to provide reasonably good medical care, if wastages are avoided. Taking into account inflation, today this would amount to say about Rs.1800-2000 per capita. This is much less than the current per capita health expenses in Indiaapproximately Rs. 2500 private and Rs. 500 public which together amount to about 6% of GDP. One reference point is the experience in Pune as regards the Pune Municipal Corporation (PMC) employees. In 2009-10 the PMC has budgeted Rs.1100 per capita for it’s employees to reimburse 80% of their private hospital bills in case they are referred to designated private hospital by the PMC doctors. In addition it has budgeted Rs. 500 per capita for medicines. There is a lot of wastage in this system and workers have reported that private hospitals inflate bills once they know that the PMC employee is covered under this reimbursement scheme. This needs to be factored in when estimate is made about the expenses required for universal access health care.

What would be the Concrete Mechanism to Pool, Dovetail and Raise Resources? a) Mandatory Social Health Insurance (MSHI) The framework being suggested for covering all income tax payers, salaried and self-employed middle class, and CGHS and ESI beneficiaries, all organised sector workers is mandatory social health insurance. This would be based on contribution from employers and graded contribution from employees based on their income. Such a mechanism could lead to assured comprehensive coverage and rational care for all these groups who presently have uncertain or variable coverage, may undergo unexpectedly high or catastrophic spending, and face a large range of irrationalities in care. The richest sections of the population may contribute as a legal requirement but may still opt out from accessing services, and may prefer to go to luxury very high-end private health care providers.

mfc bulletin/Aug 2010-Jan 2011 b) Social Security for Unorganized Sector Workers (SSUSW) There is a very large population of unorganized sector workers, estimated to be over 14 crores outside of agriculture. The principle of social security for all these workers has been accepted nationally based on proposals by NCEUS (National Commission for Enterprises in Unorganised Sector), and a minimal bare framework exists to provide social security to unorganised sector workers in form of the national ‘Unorganised Workers Social Security Act’ (which of course is in itself inadequate and urgently needs to be given effective content, including concrete schemes in the area of health care coverage). The system for health care coverage for unorganized sector workers would need to be developed in the broader context of social security. There are certain smaller models like welfare boards in Kerala, Mathadi workers (head-load workers) board which are successfully working to provide basic social security to certain sections and can provide lessons for an upscaled model. Benefits under such coverage must be made portable to ensure benefits to migrant workers and those who travel from one area to another, so that services can be accessed anywhere in the country. Employment regulated through boards can ensure some contributions from casual, shifting or part time employers (e.g. middle class employers of domestic workers). Principal employers may be roped in to contribute for contract workers employed under larger enterprises or activities (including Governments and state bodies as principal employers). c) Increased Tax Based Funds and Pooling of Various Public Health Funds Much higher public resources would obviously need to be devoted for health care. Besides drawing much larger amounts from the general tax pool, these increased resources can also be partially raised through some kind of health cess, higher taxation of luxury corporate hospitals, and ending subsidies to the private sector. We need some detailed estimate of how much can be mobilized through which measures, maybe some note can be planned for this. The present large scale of finances available in schemes like ESI and CGHS could be progressively pooled with the larger universal access system.

What would be the Overarching Health Authority to Manage Funds? Health care authorities would need to be constituted at National, State, District levels and possibility of authorities for larger Municipalities.

5 At national and state levels, a single authority would need to handle funds from MSHI, SSUS health funds and tax revenues, leading to significant cross-subsidization. Funds would need to be available to State health authorities across the country on the basis of block per capita budgeting, plus some additional allocation for special needs. Management of funds and services by the authority may be linked with a State level technical body to specify norms for costs, rationality and quality of care. At district and municipality levels, authorities would manage the funds and would purchase services from nonpublic providers based on capitation or fee per service provided. There would be decentralised choice of provision options based on each situation. The authorities would have responsibility for facilitating technical monitoring of quality and rationality of care. Forums with representation of consumers, community representatives would monitor social aspects of care, responsiveness of providers, illegal charging etc. The operational principles for functioning of such a system will have to be worked out in detail. Further, this framework needs to be articulated with the National and State Health Boards that are being suggested by the National Health Bill.

B) Provisioning of Health Care This would consist of organizing provision of a comprehensive range of outreach and preventive, promotive services, outpatient care and inpatient care as well as rehabilitative services. For all of these, population based norms for numbers of doctors, nurses, paramedics, CHWs, hospital beds, investigation facilities etc. would need to be re-defined and fulfilled in a time bound manner. There is presently a dearth of postgraduate, specialist doctors in Rural Hospitals, district hospitals and in rural, remote and poor areas. Assuming a norm of one bed per 500 population, rural areas have 6-8 times less hospital beds then urban areas. How can this deficit in rural areas be overcome? How would medical education be modified to support these changes? For various reasons, it is suggested that outreach and preventive services (e.g. immunization, antenatal care etc.) would need to be provided entirely through the public health system. This would necessitate major expansion of the public health system in urban areas according to certain norms for Primary health care services. For curative services a three level option of i) Public health facilities ii) Charitable / Trust / NGO hospitals and iii) regulated private hospitals could be exercised to ensure universal provision. Even assuming that Public

6 health facilities will be significantly expanded (say doubled) in the coming period especially in most urban areas, it is likely that non-public providers would be required to play a significant role for quite some time. What is the potential of revolution in communication technology for availability of health services? Telemedicine need not be an esoteric idea. Why can’t the PHC doctor or the family physician consult an expert through fax/phone/email and reduce the gap in availability of expert opinion? Are things likely to improve on this front during the forthcoming decade? Have there been any experiments so far in this regard? Have these experiments identified problem areas which would require further work?

Options for Service Provision a) Public Facilities It is suggested that preventive-promotive services should remain basically publicly provided. There would need to be continued emphasis on strengthening and expanding public health facilities as first option, especially in rural areas where private provisioning of some rudimentary level of quality tends to be concentrated in the towns and is not easily accessible to people from more remote rural areas. Individual specialist doctors may be ‘contracted in’ to public facilities esp. in small towns serving rural areas to expand the range of services provided. Health care facilities being managed by public bodies such as ESI, CGHS as well as Railways could be linked with the UAHC system to expand the pool of public providers. However there would be definite need for greater financial accountability, ensuring quality of care and effective community monitoring and planning in public health system. Within the universal access system, public facilities would need to be strengthened and encouraged to attract and retain users on the basis of quality of care and improved responsiveness to patients. Public health facilities could draw upon their natural advantages – publicly provided often reasonably adequate infrastructure and space, freedom from potentially distorting commercial pressures, relatively more equitable geographical distribution including presence in remote areas, often better trained and paid paramedical and supportive staff – to offer quality services and occupy a pivotal role within the UAHC system. In effect the public sector needs to be strengthened to ensure that it is a critical and formidable ‘player’ in the health care system and through this strength and quality it ‘forces’ the private sector to play a more socially responsive role.

mfc bulletin/Aug 2010-Jan 2011 b) Non-Public Facilities The involvement of private health care facilities to provide care in a publicly funded system is one of the most controversial and complex aspects of the entire UAHC system. Experiences of most of the ‘PPPs’ in the Indian health sector so far have often been quite problematic for a variety of reasons; continued and intensified irrational medical practices and unjustifiably high costs being offloaded onto the public system is one key problem. Lack of quality care being provided to users and excess charging, dual charging (charging from both public system and also illegal charging from users) and shunting off ‘difficult cases’ are other problem areas. Another important consideration related to specific ‘Public funding, private provision’ arrangements is the tendency for certain responsible public health staff to collude with private providers, and to channelise patients from public facilities to ‘preferred’ private hospitals, leading to decrease in utilization of the public facilities. There is a range of such already experienced as well as potential problems which would need to be dealt with effectively if the UAHC system is to achieve its objectives. In this context, regulation with a strong participatory component, control of quality, rationality and costs of care and due emphasis on enhancing utilisation of public facilities despite involvement of private providers would need to be ensured. Some system of regular audit of prescriptions and inpatient records would be necessary to help ensure rationality of care. Participatory monitoring combined with effective redressal mechanisms would be required to prevent violations of users rights and entitlements. Some of these issues are dealt with in the section below on governance and regulation. Keeping these riders in mind, in the UAHC system contracts for inpatient care could be made with NGO / charitable / trust hospitals as well as for-profit private providers based on clear norms for payment and quality / rationality of care which are monitored. Individual private doctors may be engaged to provide outpatient services on capitation basis especially in urban areas. The rates would need to be worked out for providers in different contexts, from large metropolitan cities to small towns, and in different states. These rates might need to be re-negotiated on a regular basis, based on certain broad criteria and norms.

Integration of Non-Allopathic Healing Systems As we know, in India roughly half of the qualified/ registered physicians belong to recognised nonallopathic or ‘AYUSH’ systems of medicine. There is a need to integrate such doctors, as well as the AYUSH systems of healing, into the UAHC system.

mfc bulletin/Aug 2010-Jan 2011 Firstly Ayurvedic or other AYUSH physicians could provide an option of non-allopathic care at all levels. Requisite medications and facilities of the respective systems would need to be made available in these facilities, to enable these physicians to practice their systems of healing. Secondly, a section of non-allopathic doctors with experience mayopt for short-term allopathic training and could provide basic integrated care, which would involve some basic allopathic medicines for primary level care along with their own system of medicine. Similarly allopathic doctors may opt for short-term training in specific AYUSH systems and provide basic integrated care. However the articulation and integration of various systems, and deciding what form of treatment to be used for particular ailments, would need to be worked out as part of basic integrated care. This of course raises the larger issue of the overall need for moving towards a system of integrated health care at various levels, which would require integrated research and organisation of care; we might learn from the Chinese health system in this regard.

Role of CHWs and Paramedics Community Health Workers (CHWs) in both rural and urban areas could manage minor ailments, facilitate referral, they could work as patient advocates and guides. We need positive examples and evolution of mechanisms to foster greater acceptance of CHWs by the community. Presentlythere is hardly any curative/symptomatic work on the CHW front in urban areas. We need to clarify the role of CHWs in three different types of areas – remote rural/adivasi areas, ‘developed’ rural areas and urban/periurban areas. The functions of CHWs as educators, counselors, community advocates/ monitors would become more important in developed areas, while the curative functions would remain significant in remote areas. But this needs to be clearly, convincingly established in the Indian context. ANMs/MPWs and equivalent staff in urban areas would continue to carry out preventive and promotive services (e.g., immunization, antenatal care, surveillance, etc.) as well as more effective role in curative/referral services. Cases in Secondary health care facilities would be accepted only through referral from CHWs and paramedics or Primary facilities.

Considerations in Urban Areas There is a clear need to combine various existing resources (including all public resources) in urban areas – including Municipal Corporation hospitals, State government hospitals, Medical college hospitals, Railway hospitals etc. There is a definite case to harness

7 large scale ESI facilities and resources, and pool these in the UAHC system. Provision in urban areas would be by a combination of strengthened and integrated public hospitals, combined with ESI facilities and regulated trust and private providers. The concept of Urban Primary Health Care - as per the Krishnan commission needs to be actively explored.

C) Governance, Regulation and Monitoring Plan for Governance of the Health Care System It is generally accepted that there are three broad requirements for the attainment of UAHC. (Of course each country will have its own unique path depending on its history and context). The three requirements are: • Adequate law: It is crucial that right to health is clearly enshrined in law. While countries like South Africa, Cuba and Brazil have such laws. In India there are attempts such as the Gujarat Public Health Bill, the draft Public health bill, the Law in Assam and West Bengal (ensuring emergency medicine) etc. The recently enacted National Clinical Establishment Act is only a first step in this direction. • The second is institutional structure and the characteristics of these institutions. These characteristics include how the members are chosen, how representative they are and how empowered they are. • The third aspect is of the culture of citizen’s participation. This is the spirit in which all of the institutions are run. It is a historically emergent property of the system. Basic operational principles for all these three aspects need to be worked out. Below we are outlining only some of the important requirements of UAHC that need to be kept in mind. While there is no one agreed upon way or structure or governance mechanism to ensure a Universal Access Health Care system, some of the crucial aspects of governance of health systems that need to be taken into account when designing such a system and working towards it include the following: • The need to develop a culture of universalism among health system stakeholders • Governance rules should ensure some level of accountability of the key actors in the system to the beneficiaries and the broader public. • A policy process that enables the interplay of the key competing interest groups to influence policy making on a level playing field. • Sufficient state capacity, power, and legitimacy to

8 manage the policy-making process effectively, to plan and design programmatic interventions, and to enforce and implement health policy decisions. • The need to create and strengthen institutions to ensure participation, accountability to and ownership of community. • The need to invest in health-care management capacity-building in developing countries. Governance depends upon the engagement and efforts of non-state actors in the policy arena, as noted, as well as in service delivery partnerships and in oversight and accountability. Thus some clear steps that need to be taken in the direction of ensuring / creating a Universal Access System include: 1. There needs to be a clear demand for Universal Access from people and civil society and these need to be converted into clear political will and a universal value in the system. For this it is clear that massive political mobilization of various groups needs is to be worked for. 2. Enactment of a law that makes the right to health and health care justiciable. This law needs to clearly define universal access and pin accountability in no uncertain terms. There should be clear responsibility for ensuring this universal Access with the government. The role of the private sector needs to be clearly defined and while regulation of the private sector is necessary a time line for the development of that capacity needs to be clearly defined. 3. Health care planning and provisioning need to be decentralized with the building up of capacity of the lower level of health staff. 4. There needs to be development of clear spaces for communities to participate in all aspects of the health system including policy making, planning, implementation and holding the system accountable. These spaces need to be designed based on the experience of the various committees from the village level upwards, the various autonomous bodies like health societies and SHSRCs etc. Care needs to be taken on integrating this with the constitutionally mandated spaces like the Panchayati Raj rather than come up with a range of parallel spaces that bypass democratic spaces and power. 5. Clear redressal mechanisms need to be defined. 6. There needs to be a lot of investment in designing a Health information system that will provide timely, adequate and adequatelydisaggregated data that is useful for health system managers. Similar

mfc bulletin/Aug 2010-Jan 2011 information that captures community perspectives and feedback like what is happening in the present community monitoring and planning process needs to be institutionalized and fed into the various management processes of the health system.

Regulation and Standardization of Private Health Care Providers Comprehensive regulation of the entire private medical sector (for standards and quality of care, patients’ rights) is a precondition for any involvement of them in publicly organised provision. Some information is needed from the Thai experience because may be theyalso have a large proportion of ‘unorganised’ medical sector and how have they managed to regulate it, would be quite instructive. Standards of care, rational treatment protocols and norms for costs would need to be ensured by National and State Health authorities. There is presently a huge gap in preparing Indian health care standards and protocols. Hence we need to go into some detailed exercise to identify the gaps and the kind and extent of work that is needed to overcome these gaps. Secondly when the state pays the bills, there is a tendency not to economize on costs while preparing standards, even when commercial interests who tend to do more interventions or jack up costing are absent. Plugging wastages, economizing is important in developing countries like India, when financial resources are quite limited. What mechanisms would be required to achieve economizing in face of limited financial resources? There have been very good examples in the NGO sector of low cost, good quality hospitals. It is expected that some of these examples are shared in the forthcoming MFC meet. What are the replicable aspects of such examples? How can these be generalized? Regulatory mechanisms may integrate professional self regulation and legally empowered social regulation. Fulfillment of Patients rights would be a major element of monitoring; this would be based on participatory redressal mechanisms. For broader planning and policy making, the participatory ‘Health Councils’ model which has been quite effective in Brazil may be considered. The ‘Health councils’ model of Brazil combines community representatives (50%), health employees (25%) and health officials (25%) for participatory planning and decision making. These Councils may be constituted at various levels – District, Municipal, State and National. Professional bodies like IMA, FOGSI, IAP may also be given representation in such councils. There would need to be a mix of community, political, technical and professional representatives, equipped with overall decision making powers and mandate for planning.

mfc bulletin/Aug 2010-Jan 2011 On a different level, regulation of the pharmaceutical industry and banning of irrational drugs and irrational drug combinations could eliminate huge wastage. In India, expense on medicines constitutes more than half of the out of pocket medical expenses in the private sector. But about half of it is wasteful. On the other hand, a large proportion of the population does not have access to even basic essential medications. Thus there are two opposing trends of change in expenditure on medicine that may be expected with movement towards a rational system for UAHC: • On one hand, with much wider effective coverage of the population, overall expenditure on medicine would be expected to significantly increase. Currently, about half of the Indians do not get the medicines they need – once they are fully covered naturally expenses for medicines would go up. • On the other hand, prices of good quality generic medicines range from 10% to 50% of the retail prices of brands; hence if expensive brands are replaced in the system by equally effective but much cheaper generics then there may be significant savings. If bulk purchasing of generic medicines is done in a transparent manner to achieve Public good, the profiteering and pilferage would be eliminated and hence the expenses for the same quantity of quality medicines would be even much lower as seen from the experience of Tamil Nadu Medical Service Corporation. Similarly the ‘new’, ‘me too’ medicines cost two to five times the ‘older, well established medicines’, an expense that could be reduced in a rational UAHC system. Further, presently prevalent large scale unnecessary medication (e.g. tonics, unnecessary injections and saline, irrational steroids and antibiotics etc.) would be reduced leading to significant savings. Keeping these clear possibilities in mind we need a concrete updated exercise to estimate the rational needs for medicines in India. This would help us to determine whether the current per capita expense of Rs. 300 to Rs. 400 annuallyon medicines would be enough for a system of Universal Access to Health Care, or it would need to be changed.

III. Suggested Range of First Steps and Way Ahead Although moving towards UAHC in India would take a rather extended and contested process, some first steps can be pushed for in the immediate future which would pave the way for UAHC:

Strengthening and Reorienting Public Health Systems • Moving towards considerable expansion of Public

9 Health Sector, so that it can act as the pillar of the National Health System. Currently it accounts for only about 2040% of health services. There is no basic structure for public health services in urban areas, which needs to be developed, while the range, quality and reach of services needs considerable strengthening in rural areas. • Currently the Public Health System is beset with corruption and bureaucracy. In many places it is led by officials who lack imagination, social motivation, community responsiveness. In its present form in most places it is not a strong counter magnet to the irrational, exploitative private medical system. Hence there has to be democratization within and accountability outwards to make the system much more responsive to society – comprehensive community based monitoring and planning with involvement of civil society at all levels may be one way to ensure this.

Regulation and Harnessing of Private Medical Providers • A public mechanism for management of all free beds in trust hospitals (10-20% of all their beds) needs to be put in place. (The Mumbai High Court has mandated that in the Trust Hospitals receiving govt. grants, 10% of beds should be reserved for poor and further 10% for economically weaker sections respectively and 2% of the gross earning of the hospital should be earmarked as Indigent Patient Fund – IPF.) This would make available significant number of beds for poor patients and would be a step towards socialization of these resources. In general, the direction of UAHC would be socialization and public management of private medical resources (opposite to the current trend of privatization of public resources). · Regulation of standards and quality of care need to be implemented across the entire health care sector (public and private). This involves developing standard guidelines for quality of care, treatment protocols, and rational cost range estimates of common health care services in different contexts. • Related to this, there should be guidelines for standardized maximum rates for services for hospitals which want to be categorized as ‘charitable hospitals’ which benefit from public subsidies. Genuine charitable and NGO hospitals meeting such criteria would be given preference for inclusion in the universal access system.

Reorganizing Public Health Insurance and Ensuring Health Care Coverage for Unorganized Sector Workers • There is a need to combine and rationalize existing social health insurance schemes, particularly ESI, and to effectively open up ESI health services for

10 unorganised sector workers. Current serious limitations of ESI need to be overcome, to enable it to become an effective pathway and component of UAHC. • RSBY (Rashtriya Swasthya Bima Yojana) should be improved, rationalized, made hassle-free, keeping in view current experiences of users and providers. Such an improved RSBY needs to be significantly expanded, there is need to break through the present limitations of BPL criterion and to open up the scheme, moving beyond relatively limited scale of coverage. At the same time, it is essential to put in place standards for quality of care, costs and norms for fair and hassle-free reimbursement to providers. • Health care coverage needs to be made an important component of proposed Social security for unorganised sector workers. Trade unions, movement platforms and networks working for social security for unorganised sector workers should become actively involved in the issue of universal health care coverage for workers. This large and active social movement should be brought on board to generate socio-political momentum for UAHC.

Major Expansion of Public Health Finances • As described above, there is need for substantial increase in the public health budget, and pooling of various public health funds to move in the direction of UAHC. This needs to be done in a time-bound and planned manner, raising public health finances to the level of at least 3% of GDP in the medium term, say within a decade , which would be used to support expansion of the public health system as well as making available an expanded range of health services provided by both public and regulated private providers. This is primarilya matter of giving much higher political priority to health.

Law, Governance and Equity-Oriented Information Systems • One key step would be universal access oriented modified drafting of the current bill, and enactment of a National Health Act that ensures the right to health and health care for all. • Community based monitoring and planning of health services needs to be given much higher profile and priority, and should be generalized across the country with involvement of civil society organisations working with rights based approach. • There is need to create a health information system with adequate disaggregation and relevance to health care planners, which also incorporates the data being generated by the community monitoring processes throughout the country. The achievement of these first steps and moving towards

mfc bulletin/Aug 2010-Jan 2011 a universal system would require significant and sustained popular mobilization around the demand for UAHC – this should be paralleled by the initiation of a debate among health care professionals, academics, trade unions and various civil and political groups.

How would these be Achieved? While there is no one way to achieving a system for UAHC, some of the basic principles and broad suggestions based on other countries experiences and discussions among members of MFC have been presented above. One critical question of course is - how to take this exercise forward? There are two levels at which we need to proceed. One is of course the further fleshing out of this model and evolving specific recommendations for the Indian scenario. The other is building up a coalition of activists, people’s groups and movements, trade unions, practitioners, academicians and politicians to own this idea and take it forward. The idea of this whole exercise is not just the development of a model but of creating an alternative people-oriented discourse in the present claustrophobic atmosphere of “TINA” and problematic ‘PPPs’ when it comes to ‘development’. It is hoped that the Annual Meeting of the MFC in January 2010 on UAHC would initiate further work on both these processes on a larger scale, and in partnership with a much wider coalition of progressive forces. Annexure1

Core Principles for Universal Healthcare Coverage in India (These points are taken from a note submitted to the High Level Expert Group on Universal Health Care byYogesh Jain, with inputs from certain MFC members)

1. Universality The system for UHC must be genuinely universal in its scope, covering all socio-economic classes and sections of the Indian population. Given the situation that currently most sections of the population, ranging from the middle class to the rural and urban poor, lack access to quality, affordable health care, such universality of the envisaged UHC system is an urgent social necessity. Universality is strongly linked with cross-subsidisation, social solidarity and effective public voice for proper functioning of the system, it avoids exclusion and eliminates considerable administrative costs related to targeting. Universality implies that on one hand, the system is not targeted to the poor only, it includes the better off sections too in its ambit; on the other hand, no one, including marginalised, hard-to-reach, mobile or traditionally discriminated groups would be excluded.

mfc bulletin/Aug 2010-Jan 2011

2. Equity Universality is integrally linked with equity; the envisaged UHC system must be based on the principle of equity, including the following dimensions: • Equity in access to services and benefits A common range of services of adequate quality would be offered to all those covered by the scheme, in other words hierarchies of entitlement within the scheme would be avoided. The same set of health services, of comparable quality should be made available to all persons with similar medical need, irrespective of socio-economic status, ability to pay, social or personal background on basis of the principle of ‘horizontal equity’ (equal resources for equal needs). Urban-rural and geographic inequities would need to be overcome to the maximum extent possible, by ensuring more equitable spread of health care facilities and services, as well as effective and timely transport services especially for remote and underserved areas, which could ensure more equitable access. · Equity ensured by special measures to ensure coverage of sections with special needs In any universal system, it needs to be kept in mind that universal, common provisions need to be supplemented with special provisions for sections with special situations or needs. For example adivasi populations or persons living with HIV-AIDS or differently abled persons would have, over and above health care needs common to all, certain special situations or needs for which additional programmes or measures would need to be implemented to ensure ‘vertical equity’ (more resources for additional needs). In a country beset with large scale social and economic inequalities , the Universal coverage system could act as a ‘great equalizer’, ensuring that irrespective of their life conditions, everyone would enjoy a similar set of health services, and would have more equal chances of leading healthy lives.

3. Comprehensiveness of Care The set of promotive, preventive, curative and rehabilitative services offered under the scheme at primary, secondary and tertiary levels will be comprehensive in scope and should cover the broadest range of health conditions and illnesses that is possible within the resources that can be mobilised. Care should include provision of competent health care providers along with health services according to treatment protocols, infrastructure, equipment, essential medicines, investigations, medical supplies, implants and prostheses, as well as patient transport which are all required for optimal care.

11 Even though a few extremely expensive forms of tertiary treatment may not be included in the scheme in the first stage, in the spirit of progressive realisation, it would be attempted in the medium term to include the maximum range of medically necessary services required.

4. Non-Exclusion and Non-Discrimination a. Universality implies that no person should be excluded from services or benefits of the scheme on grounds of current health condition (e.g. HIV/AIDS) or preexisting illness or condition. Similarly there should be no exclusion on basis of special category of health service required (e.g. maternity care, care for occupational illness or injury, mental health care). b. No person may be excluded or discriminated against in provision of services or benefits under the scheme on grounds of class, caste, gender, religion, language, region, sexual orientation or other social or personal background.

5. Financial Protection • Equity in financing: Level of contributions by various socio-economic sections should be linked with their levels of income, as befits a progressive system involving significant cross subsidisation. A large proportion of the Indian population contributes substantially to the economy but receives incomes which are at, or near subsistence levels (NCEUS report); this fact must be kept at the centre while deciding on contributions by various social sections. The scheme must be designed in a manner that no person should be excluded from services or benefits of the scheme due to their financial status or inability to pay. • There would be no payment or only nominal payment at the point of provision of all services under the scheme. 6. Quality and Rationality of Care under the scheme would be ensured through concerned regulation of all providers and their expected adherence to specified infrastructure, human power and process standards. Health services provided under the scheme would be expected to be delivered according to standard treatment guidelines which would be periodically audited. Along with medical quality of care, non-medical aspects of care and expectations of users (e.g. staff behaviour, hospital cleanliness, linen, etc.) would be appropriately addressed. To ensure genuine universality, any differentials in quality of care between different facilities or areas would need to be minimized.

7. Protection of Patients Rights, Appropriate Care, Patient Choice All services made available under the scheme would be

12 delivered in accordance universally accepted standards for patients and users rights including right to information, right to emergency medical care, right to confidentiality and privacy, right to informed consent, right to second opinion, right to choose between treatment options including right to refuse treatment etc. Care would be delivered in a socially and culturally appropriate manner, with maximum attempt to make adequate communications in a language and way that may be understood by patients and caregivers. Wherever relevant, patients would be allowed certain degree of choice of providers, treatment systems and modes of treatment within the parameters of the system.

8. Portability and Continuity of Care The benefits and coverage under the UHC scheme would be available to any covered person or family moving across the country, without any gaps in care. Migrant workers as well as those changing place of residence across states, districts or cities due to any reason would be assured of continuity of care. Those covered by Social health insurance that change employers or become unemployed would be assured of continuity of care. Seamless care during referral from one agency to another, including assured patient transport and care during such transport would be ensured.

9. Core Role of Public Financing, Substantial Contribution of Tax-based Funds, Single Payer System

mfc bulletin/Aug 2010-Jan 2011 pools various funds and pays all health care providers involved in the scheme.

10. Consolidated and Strengthened Public Health Provisioning as a Key Component of UHC During the movement towards a system for UHC, and while significantly expanding the pool of public health finances, it should be ensured that the public health care provisioning system is consolidated and significantly expanded, along with regulating and involving nonpublic providers. Under-utilized public facilities such as ESI hospitals, or currently segregated facilities associated with public agencies like Railways could be appropriately linked with the UHC system, thus expanding the range of public providers available under the scheme. Provision of promotive and preventive services would be essentially through the public health system, which would involve significant expansion of outreach and primary health services especially in urban areas. Increased finances, strengthening and significant expansion of public provision would need to be combined with ensuring and auditing quality of care according to defined standards, participatory and community based monitoring to ensure responsive services, and more efficient management of resources within the public health system.

11. Accountability, Transparency and Participation

Global experience demonstrates that UHC has not been possible to achieve through individual, voluntary insurance or small group insurance, even though community based insurance schemes may have constituted one step in moving towards universal access in some countries. UHC has generally been achieved on the basis of a core of tax based public financing, combined with some component of social health insurance in certain countries. In the Indian context, a quantum increase in tax based public financing would be required as the main component of financing the UHC system, especially given the relatively small proportion of the population employed in the formal sector who could contribute to Social health insurance.

The entire UHC scheme, including its authorities and various levels of providers, would be accountable to individual users, the general public and community representatives. Various major types of information concerning the functioning of the system would be made available in the public domain, and all specific information including that relating to public and nonpublic providers would be made available under RTI provisions. Appropriate complaint and grievance redressal mechanisms would be operationalised to enable any person aggrieved under the system to seek redressal. Without such mechanisms, universal access remains on paper as people keep away from an alien, bureaucratic system.

Universal health coverage in India would be best achieved on the basis of primarily tax based financing which could be supplemented by Social health insurance, managed by a publicly organised single payer agency, keeping in mind that across the world, UHC has not been achieved through operation of multiple, competing commercial insurance agencies. In fact UHC has been achieved in most countries through a single payer system where a unitary publicly managed agency collects and

The regulatory framework for the UHC system at all levels would be a combination of public authorities and multi-stakeholder bodies allowing for participatory regulation. Participatory bodies (analogous to various levels of health councils in Brazil) would include representation of relevant stakeholders including public health officials, public and non-public health care providers, elected representatives, civil society organisations, trade unions, consumer and health rights

mfc bulletin/Aug 2010-Jan 2011 groups, and organisations / associations of health care employees. This regulation would be combined with participatory or community based monitoring and periodic reviews of the system to ensure its accountability, effectiveness and responsiveness. The process of designing and developing the UHC system should itself be highly participatory, with active involvement of various streams of the health movement and civil society organisations as well as trade unions and organisations of doctors and health sector workers.

Supplementary Operational Tenets In conjunction with the core principles outlined above, the following operational tenets may help to guide the development of a UHC system in India: • Continuously evolving framework, periodic review of the system linked with ongoing corrections, progressive realisation of certain types of care • Sharing of finances between national, state and local governments with high degree of flexibility for state specific models within the framework of defined principles • Broader framework of social security, values of crosssubsidization and solidarity, employers contributions for healthcare of employees as part of the UHC system • Public principles and regulations to govern all interactions between the publicly organised UHC system and private providers • Trust and charitable hospitals to make available mandated free beds for poor patients, as part of the resources to be managed by the UHC system • Fair terms for health care providers, clinical autonomy of health care professionals and protection of rights of health care workers • Inter-sectoral coordination between the universal health care system and other health related departments and programmes to ensure overall improvement in the entire population’s access to social determinants of health Annexure 2

Relevant Country Experience: Thailand From 2002, a system of Universal coverage has covered the entire population of Thailand. Prior to this, Low Income Scheme was a targeted scheme providing cover to part of the poor and disadvantaged groups. CSMBS (Civil servant medical benefit scheme) covers Government employees and Social health insurance has been covering private organised sector employees.

Some Positive Features of the Thai Health System • There are well developed District health services

13 Various Schemes Contributing to Health Care Coverage in Thailand Scheme

Target population

Coverage

Source of funds

Civil servant medical benefit scheme (since 1963) Social health insurance (since 1990) Universal coverage scheme (since 2002)

Government employees, retirees and dependants Private sector employees

6 million, 10%

General tax, noncontributory

8 million, 13%

Rest of population

47 million, 74%

Payroll tax, tripartite contribution General tax, noncontributory

(DHS) in rural areas. • Mandatory rural service by all medical graduates for three years helps to staff the DHS. • Due to weaker private sector in rural areas, mainly public health facilities have been chosen as providers in rural areas.

Features of the Thai UC Scheme • There are comprehensive personal curative, preventive, promotion and rehabilitative services with some exclusion lists. • There is capitation based payment for Outpatient services – this has led to cost containment. There is Global budgeting and case based payment for Inpatient care. • Initially 30 Baht was charged from patients for every ambulatory visit or hospital admission, but now since Nov. 2006 there is no co-payment or user fee.

Moving from Targeted to Universal Access • The earlier targeting process using community mechanisms was ineffective since there were lot of leakages. It was found that coverage for eligible households whose monthly income was less than the poverty line was only 17 percent while 64.8 percent of cardholders were not eligible households. • Using the universal approach could skip the targeting process and now ensures coverage for the poor. The administrative cost of the universal approach is also quite low.

Choice of Financing Models • Community based insurance and voluntary insurance were tried but found not suitable to move towards universal coverage in Thailand. • Social Health insurance proved a more viable model, but since over half population is in informal sector, even this could not be the basis for generalisation • Tax- based funding has now emerged as the main basis of financing the Thai universal coverage system.

14

mfc bulletin/Aug 2010-Jan 2011

Human Faces of Universal Health Care - Dhruv Mankad1

Tips about what to do and what not to do for Human Resources for UHCi 1. Five Facets of a Universal Health Care (according to MFC’s Concept Paper) • Whole population of a country can access same range of quality services. • Access should be according to needs and preferences. • Access should be regardless of income level, social status, gender, caste, religion, urban/rural or geographic residency, social or personal background. • It offers a comprehensive range of curative/ symptomatic, preventive. promotive and rehabilitative health services. • It offers it at primary, secondary and tertiary levels, including common acute life saving interventions. 2. Five Tenets Regarding Human Resources for UHC • Whole population should have access to the same range of health care providers with ability to provide same quality of services – here the qualifications and competency level of the providers has to be same. • Access to them should be according to the health care seekers’ needs and preferences – a plural approach for providers qualified in all ‘pathies’ is essential. • Health Care Providers should be able to provide the services to patients regardless of income level, social status, gender, caste, religion, urban/rural or geographic residency, social or personal background. • The comprehensive range of health care providers for curative/symptomatic, preventive-promotive and rehabilitative health services should be available, accessible and affordable to the health care seekers. • Human resources should be available and able to provide primary, secondary and tertiary level of health care, including common acute life saving interventions. 3. Five Needs of Human Resources for UHC • Whole population of Human Resources should have the availability, accessibility and affordability of same range of education and trainings, 1

Email:<dhrvmankad@gmail.com>

responsibilities, quality of services and quality of life • Access to them should be according to the needs and preferences of their families and they themselves • Health Care Providers should be enabled to provide the services to patients regardless of their own income needs, social status, gender, caste, religion, urban/rural or geographic residency, social or personal background. • A comprehensive plan for opportunities to provide curative/symptomatic, preventivepromotive and rehabilitative services should be available, accessible and affordable to the health care providers as per their needs, expertise and experiences. • Human resources should be enabled to provide primary, secondary and tertiary level of health care, including common acute life saving interventions. 4. Five Do’s to ensure the Five Tenets about and Five Needs of the Human Resources for UHC (suggested doers are in brackets) • Prepare a blue print for number of providers, locations where they are needed for primary, secondary and tertiary care, for the variety of expertise including in public-private health sector [SHSRC with a HR repository] • Restructure admission processes in Government medical colleges to students from rural background (HSC from a tehsil or selected district junior colleges, vernacular language proficiency as added credit) [State Ministry overviewing medical education in tandem with state medical and paramedical councils] • Compulsory posting in public health services [State Ministry overviewing medical education in tandem with state medical and paramedical councils] • Initiate a Universal Health Services (UHS) cadre: [POLITICAL WILL in tandem with MCI in consultation with Army Medical Corps] o A short service 3 to 5 years convertible to Permanent Service if desirable by staff or required by public health services. Both should have attractive monetary and non monetary rewards – o a comprehensive career plan for IUHS cadre including family/ non family placements, e.g.,

mfc bulletin/Aug 2010-Jan 2011 remote PHCs can be considered as non family placements with residential, school, transport facilities for families at ‘family base stations’ (In Maharashtra, PHCs in several districts would fall under such categories. o a mandatory optimum level of quality of life, of quality of services, quality of placements and remunerations etc. o their career development – quality continuing education, supervision and training using new technologies, in new subjects e.g. after 3 years of working at PHC/Rural Hospital, on job DNB courses be allowed in selective RHs. (Add academic allowance to the faculty) o Plan and implement induction processes including management and communication skills from PHC to above level • Encourage a parallel cadre of health care providers to fill in the pyramid of primary care – multipurpose workers, pharmacists, licensed family doctors etc. [State Ministry overviewing medical education in tandem with state medical and paramedical councils in consultation with AMC] 5. Five Don’ts to ensure the Five Tenets about and Five needs of the Human Resources for UHC (the suggested regulators are in brackets) • Do not allow cross-practices under the shadow of ‘plurality’. An allopathic doctor not trained with Ayurvedic Ras-Shastra prescribing a bhasma is as much a bogus ‘ayurvedic’ doctor as an ayurvedic doctor not trained with clinical pharmacology using an antibiotic as a bogus ‘allopathic’ doctor. [State Ministry overviewing medical education and public health services in tandem with state medical and paramedical councils, respective medical associations]. This also requires to regulate cross practices between different levels of professional protocols, e.g., a neurosurgeon should stick to tertiary level intervention only of neurosurgery. [Respective medical and paramedical councils, accreditation authorities, medical and paramedic associations.] • Do not allow any unprofessional practices – “yes, I have a medical shop also and I am having a beauty parlor, too! Yes, I am also working in a government hospital and having my personal clinic.” These practices are not only illegal and unethical but thoroughly unprofessional. [Respective medical and paramedical councils, accreditation authorities, medical and paramedic associations] • Do not encourage ‘contract-labor’-ness of medical and paramedical staff, ‘contracting’ professionals is different from employing them on ‘contract’.

15 [State level Ministry overviewing public health services, Ministry of Labour, respective medical associations] • Do not place technical experts as financial administrators – the required professional expertise and perspective are quite different sometime opposite (unless they are trained as financial forensics during their career) [State level Ministry overviewing public health services, CAG, respective financial profession associations, IRDA etc.] • Do not protect the ‘non’-protectable lapses - breach of rules, ethics, management norms and procedures [State Ministry overviewing public health services, respective medical and paramedical councils, accreditation authorities, medical and paramedic associations] Some Caveats 1. For the Five Do’s - Some of them seem to be Old Wines in New Bottles but actuallythey are Old Wines in Old Bottles; not opened earlier correctly so spilt over. They are some of course New Wines in New Bottles. 2. For the Five Don’ts - Enforcement requires a political will, impartiality and objectivity. Are we prepared to universalize it? Almost all Do’s and Don’ts are based on evidences in Human Resources at Brazil, Thailand, China and selective health services in India – the Army Medical Corps. Some selective literature is listed here: i

I.

Thoresen SH, Fielding A. Inequitable distribution of human resources for health: perceptions among Thai healthcare professionals, Qual Prim Care. 2010; 18(1):49-56. II. Wibulpolprasert, Suwit and Pengpaibon, Paichit: Integrated strategies to tackle the inequitable distribution of doctors in Thailand: Four decades of experience, Human Resources for Health 2003, 1:12. III. Ellen M Peres , Ana M Andrade , Mario R Dal Poz and Nuno R Grande: The practice of physicians and nurses in the Brazilian Family Health Programme – evidences of change in the delivery health care model, Human Resources for Health 2006, 4:25 IV. Angelica Sousa, Ajay Tandon, Mario R. Dal Poz, Amit Prasad and David B. Evans: Measuring the efficiency of human resources for health for attaining health outcomes across subnational units in Brazil, Evidence and Information for Policy World Health Organization Geneva, March 2006. V. Fabio Ferri-de-Barros, Andrew W. Howard, Douglas K. Martin: Inequitable Distribution of Health Resources in Brazil: An Analysis of National Priority Setting, Acta Bioethica 2009; 15 (2): 179-183 VI. Prof. Sudhir Anand DPhil, Victoria Y Fan SM, Junhua Zhang PhD , Lingling Zhang SM, Prof Yang Ke MD, Prof Zhe Dong PhD , Lincoln C Chen MD China’s human resources for health: quantity, quality, and distribution: The Lancet, Volume 372, Issue 9651, Pages 1774 - 1781, 15 November 2008 VII. Policy Guidelines Army Medical Officers. January 2004 (http://www.scribd.com/doc/14124223/Posting-Policy)

16

mfc bulletin/Aug 2010-Jan 2011

Organizing the Provisioning for a Universal Healthcare System1 - Ravi Duggal2 The conversion of the existing system into an organized system to meet the requirements of universality and equity and the rights based approach will require certain hard decisions by policy-makers and planners. We first need to spell out the structural requirements or the outline of the model, which will need the support of legislation. More than the model suggested hereunder it is the expose of the idea that is important and needs to be debated for evolving a definitive model. The most important lesson to learn from the existing model is how not to provide curative services. Across the country curative care is provided mostly by the private sector, uncontrolled and unregulated. The system operates more on the principles of irrationality than medical science. The pharmaceutical industry is in a large measure responsible for this irrationalityin medical care. Twenty thousand drug companies and over 60,000 formulations characterize the over Rs. 800 billion drug industry in India.3 The WHO recommends about 300 drugs as essential for provision of any decent level of health care. If good health care at a reasonable cost has to be provided then a mechanism of assuring rationality must be built into the system. Family medical practice, adequately regulated, along with referral support, and implemented by a local health authority, is the best and the most economic means for providing good health care. What follows is an illustration of a mechanism to operationalise universal access to healthcare, it should not be seen as a well defined model but only as an example to facilitate a debate on creating a healthcare system based on a right to healthcare approach. This is based on learnings from experiences in other countries which have organized healthcare systems which provide near universal health care coverage to its citizens.

Family Practice Each family medical practitioner (FMP) should on an average have 400 to 500 families assigned to him/her by the local health authority; in highly dense areas this The following discussion is an updated version based on work done by the author earlier at the Ministry of Health New Delhi as a fulltime WHO National Consultant in the Planning Division of the Ministry. An earlier version was published as “The Private Health Sector in India – Nature, Trends and a Critique” by VHAI, New Delhi, 2000 1

2

Email: <rduggal57@gmail.com>

In addition to this there is a fairly large and expanding ayurvedic and homoeopathy drug industry estimated to be over one-third of mainstream pharmaceuticals

3

number may go up to 800 to 1000 families and in very sparse areas it may be as less as 100 to 200 families. For each family/person enrolled the FMP will get a fixed amount from the local health authority, irrespective of whether care was sought or no. He/she will examine patients, make diagnosis, give advice, prescribe drugs, provide contraceptive services, make referrals, make home-visits when necessary and give specific services within his/her framework of skills. Apart from the capitation amount, he/she will be paid separately for specific services (like minor surgeries, deliveries, homevisits, pathology tests, etc.) he /she renders, and also for administrative costs and overheads. The FMP can have the choice of either being a salaried employee of the health services (in which case he/she gets a salary and other benefits) or an independent practitioner receiving a capitation fee and other service charges. The payments will keep in mind the equity principle, that is where density is high and a larger number of families are enrolled with the FMP then the per family capitation payment will be lower than in a area of sparse population where per FMP enrollment is lower, etc. The FMP will also maintain family and patient records which will facilitate both epidemiological assessments as well as evidence based planning and allocation of resources.

Epidemiological Services The FMP will receive support and work in close collaboration with the epidemiological station (ES) of his/her area. The present PHC setup should be converted into an epidemiological station. This ES should have one doctor who has some training in public health (one FMP, preferably salaried, of the ES area can occupy this post) and a health team comprising of a public health nurse and health workers and supervisors to assist him/her. The ES should also function as a centre for normal deliveries and basic day care services and should have 5 to 20 beds depending on its population coverage. Each ES should cover a population between 8,000 to 20,000 in rural areas depending on density and distance factors and even up to 50,000 population in urban areas. On an average for every 2000 population there should be a health worker (subcentre of one male and one female health worker at 4000 population level) and for every six to eight health workers a supervisor. Epidemiological surveillance, monitoring, taking public health measures, laboratory services, and information management will be the main tasks of the ES. The health workers will form the survey team and also carry out tasks related to all the preventive and promotive programs (disease programs, MCH, immunization, etc.) They will work in close

mfc bulletin/Aug 2010-Jan 2011 collaboration with the FMP and each health worker’s family list will coincide with the concerned FMPs list. At the village level for every 500 population and in urban wards for every 1500-2000 population there will be a community health worker who will be a link person for the community with the healthcare system. The health team, including FMPs, will also be responsible for maintaining a minimum information system (individual/ patient profile), which will be necessary for planning, research, monitoring, and auditing. They will also facilitate health education. Of course, there should be other appropriate supportive staff to facilitate the work of the health team.

First Level Referral The FMP and ES will be backed by referral support from a basic hospital at the 50,000 population level. This hospital will provide basic specialist consultation and inpatient care purely on referral from the FMP or ES, except of course in case of emergencies. General medicine, general surgery, paediatrics, obstetrics and gynaecology, orthopaedics, ophthalmology, dental services, radiological and other basic diagnostic services and ambulance services should be available at this basic hospital. This hospital will have 50 beds, the above mentioned specialists, 6 general duty doctors and 18 nurses (for 3 shifts) and other requisite technical (pharmacists, radiographers, laboratory technicians, etc.) and support (administrative, statistical, etc.) staff, equipment, supplies, etc., as per recommended standards. There should be two ambulances available at each such hospital. The hospital too will maintain a minimum information system and a standard set of records.

Pharmaceutical Services Under the recommended health care system only the essential drugs required for basic care as mentioned in standard textbooks and/or the WHO essential drug list or a similar state pharmacopeia should be made available through pharmacies contracted by the local health authority. Where pharmacy stores are not available within a 1 to 2 km radial distance from the health facility the FMP should have the assistance of a pharmacist with stocks of all required medicines. Drugs should be dispensed strictly against prescriptions only. The hospitals should have their own pharmacy store. Payments for the drugs will be made directly by the local health authority from its budget. Patients should not make any direct payments for procuring drugs. On a random basis the health authority should carry out prescription audits to prevent unnecessaryand irrational drug use.

17

Rehabilitation and Occupational Health Services Every health district must have a centre for rehabilitation services for the physically and mentally challenged and also services for treating occupational diseases, including occupational and physical therapy. This will be attached to the Referral Hospital.

Referral Hospital and Tertiary Hospital For each 3 to 5 basic hospitals there should be a 150250 bed hospital at the 150000-250000 population level or the Block/Taluka level which would constitute the health district. This would function as a multi-specialty referral hospital for most higher level care, having all the basic specialties, somewhat akin to the smaller district hospitals that exist today with the same range of medical, nursing and support staff. As a further referral support each district would have a tertiary or teaching hospital which would be the apex hospital for the district. This hospital should be a 5001000 bed hospital depending on the density of the population and would be a multi-specialty hospital with all super specialties, like any existing average medical college and hospital. These hospitals from first referral units to tertiary hospitals should preferably be public hospitals but could also be contracted in private hospitals which provide services within a negotiated agreement which is regulated and monitored through a purchase agreement and audit provisions with the concerned health authority.

Licensing, Registration and CME The local health authority should have the power to issue licenses to open a medical practice or a hospital. Any doctor wanting to set up a medical practice or anybody wishing to set up a hospital, whether within the universal health care system or outside it will have to seek the permission of the health authority. The licenses will be issued as per norms that will be laid down for geographical distribution of doctors. The local health authority will also register the doctors on behalf of the medical council. Renewal of registration will be linked with continuing medical education (CME) programs which doctors will have to undertake periodically in order to update their medical knowledge and skills. It will be the responsibility of the local health authority, through a mandate from the medical councils, to assure that nobody without a license and a valid registration practices medicine and that minimum standards laid down are strictly maintained.

18

Managing the Health Care System For every 3 to 5 units of 50,000 population, that is 150,000 to 250,000 population, a health district should be constituted (Taluka or Block level). This will be under a local health authority that will comprise of a committee including political leaders, health bureaucracy, and representatives of consumer/social action groups, ordinary citizens and providers. The health authority will have its secretariat whose job will be to administer the health care system of its area under the supervision of the committee. It will monitor the general working of the system, disburse funds, generate local fund commitments, attend to grievances, provide licensing and registration services to doctors and other health workers, accredit health facilities, implement CME programs in collaboration with professional associations, assure that minimum standards of medical practice and hospital services are maintained, facilitate regulation and social audit, etc. The health authority will be an autonomous body under the oversight of the State Health Department. The FMP appointments and their family lists will be the responsibility of the local health authority. The FMPs may either be employed on a salary or be contracted in on a capitation fee basis to provide specified services to the persons on their list. Similarly, the first level hospitals, either state owned or contracted in private hospitals, will function under the supervision of the local health authority with global budgets. The overall coordination, monitoring and canalization of funds will be vested in a National Health Authority. The NHAwill function in effect as a monopoly buyer of health services and a national regulation coordination agency. It will negotiate fee schedules with doctors’ and hospital associations, determine standards and norms for medical practice and hospital care, and maintain and supervise an audit and monitoring system. It will also have the responsibility and authority to pool resources for the organized healthcare system using various mechanisms of tax revenues, pay roll deductions, social and national insurance funds, health cess, etc. The Panchayat Raj institution at the Block level can be used as the fulcrum around which the health district is built. The local health authority should derive its membership from amongst panchayat representatives, providers, health department officials and civil society representatives in equal numbers and this should be the key planning, decision-making and implementing agency, including for determining budgets and expenditures. The role of the national and state authorities should be one of oversight and for provision of appropriate resources. The rest should be left to the local health authority. The National Rural Health

mfc bulletin/Aug 2010-Jan 2011 Mission offers an opportunity for decentralized management of the healthcare system by creating agencies like district and block management units – these should constitute the secretariat of the local health authorities and not be program management units as envisaged under NRHM. We have to outgrow the program mode of functioning and look at healthcare from the perspective of comprehensive services delivered universally. In fact the NRHM clearly articulates the need for architectural correction. (MoHFW 2005) Such restructuring will be possible only if: • The healthcare system, both public and private, is organized under a common umbrella/framework as discussed above • The financing mechanism of healthcare is pooled and coordinated by a single-payer system • Access to healthcare is organized under a common system which all persons are able to access without any barriers • Public finance of healthcare is the predominant source of financing • The providers of healthcare services have reasonable autonomy in managing the provision of services • The decision-making and planning of health services is decentralized within a local governance framework • The healthcare system is subject to continuous public/community monitoring and social audit under a regulated mechanism which leads to accountability across all stakeholders involved The NRHM Framework (MoHFW 2006) one way or another tries to address the above issues but has failed to come up with a strategy which could accomplish such an architectural correction. The framework only facilitates a smoother flow of resources to the lower levels and calls for involvement of local governance structures like panchayat raj institutions in planning and decision making. But the modalities of this interface have not been worked out and hence the local government involvement is only peripheral. In order to accomplish the restructuring that we are talking about the following modalities among others need to be in place: • All resources, financial and human, should be transferred to the local authority of the Health District (Block panchayat) • The health district will work out a detailed plan which is based on local needs and aspirations and is evidence based within the framework already

mfc bulletin/Aug 2010-Jan 2011 worked out under NRHM with appropriate modifications • The private health sector of the district will have to be brought on board as they will form an integral part of restructuring of the healthcare system • An appropriate regulatory and accreditation mechanism which will facilitate the inclusion of the private health sector under the universal access healthcare mechanism will have to be worked out • Private health services, wherever needed, both ambulatory (FMP) and hospital, will have to be contracted in and appropriate norms and modalities, including payment mechanisms and protocols for practice, will have to be worked out • Undertaking detailed bottom-up planning and budgeting and allocating resources appropriately to different institutions/providers (current budget levels being inadequate new resources will also have to be raised) • Training of all stakeholders to understand and become part of the restructuring process • Developing a monitoring and audit mechanism and training key players to do it The implementation of the above process would be

19 critically dependent first on the state and central government agreeing to changing the financing mechanism and giving complete autonomy to local health authorities and health institutions as envisaged above. This of course would need appropriate legislation. With the financing mechanism in place the local health authorities would require appropriate capacity building to manage the restructuring of the healthcare system. The private health providers and their associations will have to be brought on board through discussions and explaining them the benefits of joining such a system and should be involved in the process from the planning stage. Those serving in public health institutions will have to be trained and informed appropriately to manage and run such a health system. And above all those in local governance bodies and civil society groups will have to be oriented and skilled to planning, monitoring and auditing the functioning of such a healthcare system. Such restructuring is difficult to undertake across the board immediately and hence initially the states can begin this process in a few districts with technical assistance from experts who would be associated with the National and State Health Systems Resource Centres which are being/have been created under NRHM.

Financing the Universal Access Health Care System - Ravi Duggal1 If a universal health care system has to assure equity in access and quality then there should be no direct payment by the patient to the provider for services availed. This means that the provider must be paid for by an indirect method so that he/she cannot take undue advantage of the vulnerability of the patient. An indirect single payer mechanism has numerous advantages as global experience shows, the main being keeping costs down and facilitating regulation, control and audit of services.

Tax Revenues: Major Source of Finance Tax revenues will continue to remain a major source of finance for the universal health care system. In fact, efforts will be needed to push for a larger share of funds for health care from the state exchequer. The present government at the Centre has committed to spending up to 3% of GDP on healthcare in its Common Minimum Program Declaration. To achieve this, alternative sources will have to be tapped in order to generate more resources. Employers and employees of the organized sector are an important source (ESIS, CGHS and other such health schemes should be merged with general 1

Email: <rduggal57@gmail.com>

health services) for payroll deductions and employer contributions towards social insurance payments. The agricultural sector is the largest sector in terms of employment and population and at least one-fourth to one-third of this population has the means to contribute to a health scheme. Some mechanism, either linked to land revenue or land ownership, will have to be evolved to facilitate receiving their contributions. Similarly selfemployed persons like professionals, traders, shopkeepers, etc. who can afford to contribute can pay out in a similar manner to the payment of profession tax in some states. Further, resources could be generated through other innovative methods. Health cess collected by local governments as part of the municipal/house taxes, proportion of sales turnover and/or excise duties (‘sin taxes’) of health degrading products like alcohol, cigarettes, paan-masalas, guthkas, etc. should be earmarked for the health sector, voluntary collection through collection boxes at hospitals or health centres or through community collections by panchayats, municipalities, etc. Given the increasing domination of the service sector economy, especially financial services, Tobin tax must be used more extensively to generate

20 revenues from all financial transactions in trade, stock markets, banking, credit card, etc.

Need for Commitment to Raise Resources It is not very difficult to raise additional resources if the government has some commitment to the social sectors. A health cess of 2% on sales turnover of health degrading products like alcohol, tobacco products like cigarettes, guthka, beedis, pan masalas, etc. which together have a turnover estimated at Rs.1000 billion would itself generate Rs. 20 billion which is 8% addition to the existing health budgets of central and state governments combined. Similarly, the financial transaction tax (Tobin Tax) introduced in the 2005-06 budget needs to be expanded and earmarked for social sector expenditures only (this should be an additional allocation and should not entail reductions from existing allocations out of present tax revenues). India is a rapidlygrowing financial sector economy and daily transactions in securities (government and stock market and forex) alone are estimated at Rs. 1000 billion per day and other cheque and financial instruments another Rs. 350 billion daily and a 0.1% Tobin tax on this would generate Rs. 135 crores daily for social sector budgets. And this would not hurt those transacting as it would be merely Re. 1 per Rs. 1000 transacted. Apart from this there are other transactions like credit card transactions, commodities trading etc. which can contribute substantially. There are also other avenues for raising resources for the health sector, for example a health tax similar to profession tax, a health cess on land revenues and agricultural trade so that the rural economy can also contribute to revenues for public health, health cess on personal vehicles using fossil fuels, on luxury goods like air conditioners, on house rents and property taxes above a certain value or size etc. The bottom line is that these additional resources should be strictly earmarked for the health sector and should not find their way into the general pool – with this caveat and evidence of its use for strengthening social sectors like health and education people will not protest against such levies. Further any attempts to raise revenues through user fees should be resisted as they are regressive and anti-poor. Of course, overall, with economic growth hovering around 9%, the government must also endeavour to raise substantially the Tax: GDP ratio closer to 25% from the present 17% by netting more taxes from incomes of those benefiting from this high growth. All these methods are used in different countries to enhance health sector finances. Many more methods appropriate to the local situation can be evolved for raising resources. The effort should be directed at assuring that at least 50% of the families are covered

mfc bulletin/Aug 2010-Jan 2011 under some statutory contribution scheme. Since there will be no user-charges people will be willing to contribute as per their capacity to social security funding pools. All these resources would be pooled under a single body, the National Health Authority, and this body would also make payments to providers of services. In order to do this, standardized protocols of treatment and charges will have to be evolved and this itself will have a major impact on both quality of care as well as on efficient use of resources.

Structural Changes Needed Further, we need to advocate with both ministries of health and finance for making structural changes in the way in which both resources are allocated as well as how the health system is organized and structured. The present mechanism of allocating resources to health facilities is very inefficient and also ineffective. Resources must be provided to health facilities whether hospitals or health centres on a block funding or per capita basis. Thus hospitals, for instance, should get funds @ Rs. 500,000 to 600,000 per bed linked to a defined occupancy ratio: because that is what it requires at today’s prices to run a reasonable district or rural hospital. A health centre providing comprehensive healthcare should get about Rs. 200 per capita for the 30,000 or 20,000 population it serves to provide a reasonable level of primary healthcare. This mechanism of financing will factor in rationality and efficiency in allocation of resources for public health. That is once a health centre or hospital of a certain size is created it must receive optimal resources for running that institution. The problem today is that financing is based on resource distribution on the basis of availability. Thus drugs procured are distributed to facilities not on the basis of their requirement but on an irrational basis like if an antibiotic like amoxicillin is procured then all health centres are given a more or less similar quantity irrespective of their actual requirement. Or, if ten different specialists are available instead of 60 for ten rural hospitals, then a couple of hospitals may get a gynaecologist, another may get a general surgeon, a third may be allotted a paediatrician, etc. That defeats the purpose of providing comprehensive healthcare. Thus, instead resource allocation should be made on the basis of appropriate costing. Thus a fifty-bed hospital to be run optimally (80% occupancy rate) requires at least Rs.350, 000 per bed per year, that is Rs. 17.5 million per such hospital. The local health authority should be allocated that amount for such a hospital and then it is their responsibility to acquire the necessary staff, drugs, maintenance of equipment etc. Similarly for a PHC to provide comprehensive curative and preventive primary

mfc bulletin/Aug 2010-Jan 2011 health care services it would need about Rs. 200 per capita. Thus if it covers about 30,000 population then it should get Rs. 6 million. What we are trying to get at is that allocations for healthcare facilities have to be based on realistic costing for provision of quality care. The costing is indeed an elaborate exercise but it has to be done.

CEHAT Study CEHAT undertook a small study to understand costs in selected facilities in both the private and public sector. It became clear at the outset of the study that cost data was not available in anyof the health facilities we studied. Wherever we got data it was expenditure. For instance, for the district hospital we got data on salaries, materials and supplies, utilities etc but we did not get the total expenditure on drugs because a large part of the drugs are centrally purchased and supplies are given in kind. Thus the district hospital we studied had an expenditure of Rs. 45 million for 254 beds or Rs 177,000 per bed per annum, excluding the drugs which are given in kind. It provided care to 13,800 in patients with an occupancy ratio of 74% and bed turnover rate of 55; and 145,000 outpatients, 570 per bed per year and 10.5 per inpatient, in 2005-2006. This district hospital was one of the best performing ones. It had a few vacancies of staff though. However it suffered from problems of inadequate funds for maintenance, inadequate drug supplies, and inadequate amenities for patients. Our assessment based on using Rs.350, 000 per bed per year (2004-05 prices) as a standard estimate2 for such kind of a general hospital is that Rs.200, 000 per bed should go towards salaries and Rs. 150,000 per bed towards other costs like drugs, maintenance, utilities etc.. For this hospital the per bed cost is Rs. 154,000 for salaries and Rs. 23,000 for all other costs. This is a gross mismatch which results in inadequate services being provided to patients. In the 30 bedded rural hospital the patient load was 49500 outpatients per year (1650 per bed and 29 per inpatient) and 1700 inpatients with an occupancy rate of only 26% and bed turnover rate of 41 per year. The total annual expenditure, excluding drugs, was Rs.4.4 million (Rs. 147,000 per bed per year), of which salaries was a whopping Rs.4.2 million (Rs. 140,000 per bed per year with one third of medical positions vacant), leaving only a paltry Rs. 7000 per bed per year for other costs. And this was one of the better performing rural hospitals of the state. The above clearly shows that non-salary costs This estimate has been worked out on the basis of full component of staff availability with 50% salary increase, all other necessary inputs like drugs, diagnostic equipment, adequate maintenance and capital amortization etc., and using a 60:40 ratio for salary:non-salary 2

21 are very inadequately supported in public healthcare facilities (and sometimes even full complement of staff is not available in institutions like rural hospitals and PHCs), and this must be changed as close to the standard norm referred to above if efficient and effective services have to be delivered. We also looked at a 324-bed multi-specialty tertiary care private hospital which had an occupancy rate of 68% and a bed turnover rate of 45 per year; it treated 18,800 inpatients and 81,000 outpatients (250 per bed per year and 4.3 per inpatient). The financial data we got from here revealed that the per bed annual cost was Rs 1.4 million (net of profit), and of this salary cost was Rs. 350,000 per bed and the rest of the cost was Rs 1 million per bed per year. The profit was equal to the salary cost that is Rs 350,000 per bed. This cost would exclude drugs and other consumables and accessories (valves, stents etc.) which patients buy directly. This is in sharp contrast to the public district and rural hospitals and indicates that economics of private healthcare are completely different from public health economics. Now we know why accessing private hospitals is so expensive.

Projection of Resource Requirements The projections we are making are for the fiscal year 2009-2010. The population base is a little over 1.18 billion. There are over 1.58 million doctors (of which allopathic are 780,000, including over 250,000 specialists), 1.4 million nurses, over 1.5 million hospital beds, 600,000 health workers and 25,000 PHCs with government and municipal health care spending at about Rs.700 billion (excluding water supply).

An Estimate of Providers and Facilities What will be the requirements as per the suggested framework for a universal health care system? • Familymedical practitioners = 500,000 • Epidemiological stations = 50,000 • Health workers = 600,000 • Health supervisors = 125,000 • Public health nurses = 50,000 • Basic hospitals = 24,000 • Basic hospital beds = 1.2 million • Basic hospital staff: o General Duty Doctor = 144,000 o Specialists = 144,000 o Dentists = 24,000 o Nurses = 432,000

22 • Other technical and non-technical support staff as per requirements, including about 1 million CHWs as link workers in the villages and urban wards. (Please note that the basic hospital would address to about 75% of the inpatient and specialist care needs, the remaining will be catered to at the secondary/district level and teaching/tertiary hospitals) One can see from the above that except for the hospitals and hospital beds the other requirements are not very difficult to achieve. Training of nurses, dentists, public health nurses would need additional investments. We have more than an adequate number of doctors, even after assuming that 80% of the registered doctors are active (as per census estimates). What will be needed are crash CME programs to facilitate integration of systems (there is no reason why the AYUSH doctors cannot be involved at the primary care level) and reorganisation of medical education to produce a single cadre of basic doctors. The PHC health workers will have to be reoriented to fit into the epidemiological framework. And construction of hospitals in underserved areas either by the government or bythe private sector (but only under the universal system) will have to be undertaken on a rapid scale to meet the requirements of such an organized system.

An Estimate of the Cost The costing worked out hereunder is based on known costs of fully functional public sector and NGO facilities. The FMP costs are projected on the basis of employed professional incomes. The actual figures are on the higher side to make the acceptance of the universal system attractive. Please note that the costs and payments are averages, the actuals will vary a lot depending on numerous factors. 3

Primary healthcare (Family Medical Practitioner + Epidemiological Station-PHC) with following features: • Staff composition for each PHC-FMP unit to include 4 doctors (one coordinating the ES-PHC), 1 PHN, 2 nurse midwives, 8 ANMs (females), 4 MPWs (males), 1 pharmacist, 2 clerk/stat asst., 1 office assistant, 1 lab technician, 1 driver, 1 sweeper and 20 CHWs –. Doctors and nurses either may be salaried or contracted in on a capitation basis as in the NHS of UK. The curative care component should work as a family medical practice with families (200 - 2000 depending on density) being assigned to each such provider. • Average of 10 beds per PHC • Average rural unit to cover 20,000 population (range 1030 thousand depending on density); average urban unit to cover 50,000 population (range 30-70 thousand population depending on density)

mfc bulletin/Aug 2010-Jan 2011 Projected Universal Health Care Costs (2009-2010 Rs. in millions) Type of Costs  Capitation/salaries to FMPs (@ Rs.600 per family per year x 236 mi families) 50% of FMP services

141,600

 Overheads 30% of FMP services  Fees for specific services 20% of FMP services

84,960

 Total FMP Services

283,200

 Pharmaceutical Services (20% of FMP services)

56,640

56,640

 Total FMP Costs

339,840

 Epidemiological Stations (@ Rs. 3.5 mi per ES x 50,000)

175,000

 Basic Hospitals (@ Rs.25 mi per hospital x 24,000, including drugs, i.e.Rs.500,000 per bed)  Total Primary Care Cost3

600,000 1,114,840

 Per capita = Rs. 945; 1.72% of GDP  Referral Hospital at Block level 5900 hospitals of 200 beds each @ 700,000 per bed

826,000

 District and Teaching Hospitals, 600 hospitals of 500-1000 beds each including medical education and training of doctors/nurses/paramedics (@ Rs. 1 million per bed x 4.8 lakh beds)

480,000

 Total health services costs

2,420,840

 Medical Research (2%)  Audit/Info.Mgt/Social Res. (2%)

48,000 48,000

 Administrative costs (2%)

48,000

 TOTAL RECURRING COST

2,564,840

 Add capital Costs (15% of recurring)

384,726

 Add 5% Contingency

128,242

 ALL HEALTH CARE COSTS 3,077,808  Per Capita = Rs. 2608.31; 4.73% of GDP (Calculations done on population base of 1.18 billion and GDP of Rs. 65,000 billion; $1 = Rs.44, that is $ 70 billion for all healthcare costs)

Distribution of Costs The above costs from the point of view of the public exchequer might seem excessive to commit to the health sector given current level of public health spending. However, this is less than 5% of GDP at Rs.2608 per capita annually, including capital costs. The public exchequer’s share, which is from tax and related revenues, would be about Rs.2010 billion or less than two-thirds of the cost. This is well within the reach of the current government’s commitment of

mfc bulletin/Aug 2010-Jan 2011

23

3% of GDP for public health services resources of the governments and local governments put together. The remaining would come from other sources discussed earlier, mostly from employers and employees in the organized sector (social insurance), and other innovative mechanisms of financing. As things progress the share of the state and central governments should stabilize at 50% and the balance half coming from other sources. Raising further resources will not be too difficult. Part of the organized sector today contributes to the ESIS 6.75% of the

salary/wage bill. If the entire organized sector contributes even 5% of the employee compensation (2% by employee and 3% by employer) then that itself will raise close to Rs.850 billion. In fact, the employer share could be higher at 5%. Further resources through other mechanisms suggested above will add substantially to this, which in fact may actually reduce the burden on the state exchequer and increase contributory share from those who can afford to pay. Given below is a rough projection of the share of burden by different sources:

Projected Sharing of Health Care Costs (2009-2010 Rs. in millions) Type of Source Central State/ Social Other Govt. Muncp. Insurance Sources 1. Epidemiological services

80,000

80,000

15,000

--

2. FMP Services (inc. drugs)

75,000

150,000

100,000

14,840

3. Basic Hospitals

50,000

350,000

150,000

50,000

4. Referral Hospital Block level

100,000

400,000

300,000

26,000

5. Secondary/Teaching Hospitals

150,000

150,000

120,000

60,000

6. Medical Research

30,000

10,000

3,000

5,000

7. Audit/ Info. Mgt./ Soc.Research

30,000

15,000

3,000

--

8. Administrative Costs

20,000

20,000

8,000

--

9. Capital Costs

100,000

100,000

100,000

84,726

50,000

50,000

28,242

--

685,000 1325,000

827,242

240,566

10. Contingency (5%) ALL COSTS

Rs. 3,077,808 million

Percentages

22

43

27

8

Mfc Statement on Binayak Sen We, Medico Friend Circle, express our outrage at the verdict of the Raipur district and sessions court, on 24th December 2010, declaring Dr Binayak Sen, General Secretary of the Chhattisgarh People’s Union for Civil Liberties and Vice-President of the National PUCL, guilty of sedition and treason, and sentencing him to life imprisonment. Dr Sen has an illustrious record of over 25 years of selfless public service in areas of health and human rights. He has been an active member and former convenor of the Medico Friend Circle, and has been closely associated with the Jan Swasthya Abhiyan. In recognition of his work, the Christian Medical College, Vellore conferred on him the Paul Harrison Award in 2004, which is the highest award given to an alumnus for distinguished service in rural areas. He continues to be an inspiration to successive generations of students and faculty. Many of his articles based on his work have been internationally appreciated. His indictment under the draconian and undemocratic Chhattisgarh Special Public Security Act, 2006, and the Unlawful Activities (Prevention) Act, 1967 is utterly condemnable. Not only has the farcical nature of the trial been reported in the media, the charges against Dr Sen, of engaging in anti-national activities, have been widely held as baseless. This judgment is an unacceptable attempt to intimidate and vilify those who advocate for the rights of the poor and the marginalized, and reveals the indiscriminate use of state machinery to stifle democratic dissent. We believe that a great injustice has been done, not only to Dr Sen but also to the democratic fabric of this country. We salute Dr Sen’s work, and demand that justice be delivered.

24

mfc bulletin/Aug 2010-Jan 2011

Contemplating UHC for Nashik - Dr Shyam Ashtekar, With the help of Dr Ratna Ashtekar and Dr Suhas Joshi1

1. Background We have been working in Nashik district for the last 25 years and have closely seen the plight of people when they seek medical care at government or private hospitals. Nashik is a better off and growing district and city in the so called Golden Triangle of Mumbai-PuneNashik. But it is also a story of contradictions. Based on European health plans (meaning social security systems in European countries like Germany, etc.), I presume that UHC (Universal Health Cover or Coverage) becomes universal due to three features or basic principles: a) Enrollment of all providers public or private b) Cover to all users and communities and c) Covering most or as manyhealth problems as possible. I presume that UHC (or UHAC, with the added A for Access) will amalgamate ‘public’, NGO and ‘private’ health care providers under a broad plan of accreditation and claim processing/ reimbursement. The differences between various sectors will gradually melt into a seamless UHC. I believe that a centrally designed UHC has to be somewhat customized to states and districts. Therefore facility map, provider profiles, costs, leverages, local politics and administration, etc., offer varying terrains for an umbrella national UHC plan. This necessitates generation of data on providers, user community, current costs of care, health problems’ profile and felt needs, etc.

states. This is area-wise the biggest district in the state of Maharashtra. Two major cities - Nashik (18 lakhs) and Malegaon (8 lakhs) - together have a population of about 26 lakhs. The entire district had a population of 49.9 lakhs in 2001. The rural part has 15 blocks and is divided in three parts: a) a Western Ghat area having tribal communities (Koli and Kokna for instance) and having most of the river-based dams; b) irrigated talukas of Nashik-Niphad-Dindori surrounding the city of Nashik, and c) drought-prone talukas in the eastern zone, including Malegaon. The city of Nashik is the divisional headquarters for North Maharashtra and a place of pilgrimage and Kumbha. The main crops are grape and other fruits, onion, vegetables and sugarcane, with seven sugar mills to harness the latter. The district has a good network of roads and is a major station of Central railway, but there is no major airway service. Being close to Mumbai, the incomes are increasing in all sectors. The Mumbai-Agra highway is being developed as a four lane expressway from Mumbai to Indore on BOT principle. Land and property prices have grown nearly ten times in the last decade. It is a boom time for Nashik only next to Pune and may be Nagpur. The weather and water are great assets of Nashik. 2.2 Health Sector in Nashik City

The idea in presenting details of Nashik district (or rather the city) is to stimulate thinking on possible ‘models’ for a UHC or inject the idea of terrain-customized or calibrated approach. UHC is a major political program; hence local details also become important. While contemplating how UHC can be implemented in this district, I have used my current understanding of the district and city and I am open to suggestions.

The health sector information regarding the entire district is incomplete - especially because private sector data from rural area is not available and is a subject of research by itself. Health related information about Nashik city per se is available. We have focused on Nashik city only in this paper - however one must add that the clientele in these facilities comes from entire district and not just the city. The NMC (Nashik Municipal Corporation) has a registry for hospitals and clinics that is updated every year. The information is not very systematic and is difficult to use. We have banked heavily on information from medical associations. It seems NMC has only a fraction of information regarding doctors.

2. Nashik District and City

Here is an attempted summary:

2.1 District Profile This is a district of contradictions, with rich agro-zones and wineries, along with tribal people in western half of the district migrating for farm and construction labor, drought-prone talukas, industry (though not big as Pune, Thane or Mumbai) and migrant workers from other 1

Email: <shyamashtekar@yahoo.com>

• The Shirdi Saibaba 200-bed hospital is 100 km away from Nashik, but attracts lot of clientele from Nashik district as its services are good and tariffs low. In fact it has influenced and brought down tariffs of heart procedures in Nashik. It offers a heart bypass surgery for as low as Rs 50,000 to poor patients, even this can be waived off if need be. The same surgery costs around Rs 2 lakhs in private hospitals. Probably other rates are not affected in Nashik.

mfc bulletin/Aug 2010-Jan 2011

25

Table 1: Medical Facilities in Nashik at a Glance Subject Head

Number Of Units

Popln./ Unit

Unit/10000 Popln.

Beds

4000

450

2.2

GPs

1700

1059

0.9

PGs

1106

1627

0.6

Dentists Pharma Shop

350

5143

0.2

1500

1200

0.8

Table 2: Doctors in Nashik

Source: Data from NMC and medical associations. The last five categories are in the GP sector

26

mfc bulletin/Aug 2010-Jan 2011

• The MVP General Hospital attached to a private medical college is another success story with a posh hospital offering economy rates. This has a big potential for UHC in near future.

2.3 Health Care Expenditure in Nashik District: An Estimate Here in Table 4 is a ‘reasonable’ guesstimate on health expenditure in Nashik district, assuming 55 lakhs population and number of providers and beds, etc., available from the study.

Table 4: Private Sector Revenue Estimate (income and estimates in Rs) Category Nashik City

Consultants Dentists GPs

Rural+Malegaon City

Consultants GPs

Add for Bed Income in the District at 50% Occupancy

1,041

Income per Day 6,000

350

4,000

511,000,000

1,600

1,000

584,000,000

300

5,000

547,500,000

2,000

800

584,000,000

4,500

500

410,625,000

Number

Estimate 2,279,790,000

Total

4,916,915,000

Add Pharma Sector Expenses at 33% of Doctor Incomes

3,428,986,970

Total Pvt Sector

8,345,901,970 N

Annual Bill

PHCs

102

9,600,000

979,200,000

CHCs of 30 Beds

24

7,500,000

2,160,000,000

District & Sub Dt Hospitals

4

10,000,000

384,000,000

Public Sector (Exp Est)

NMC Med. Expenses

85 Doc+500 Paramed+Medicines

130,000,000

Malegaon & Other Municipal areas.

26,000,000

ESIS Hospital/Disp Nashik

30,000,000 Total Public Sector

Trust Hospitals (Expense Estimate) Major Trust Hosp (Guesstimate)

Total 1000 Beds in 4 Units Grand Total

Rounded To

3,709,200,000

4

120,000,000

480,000,000 12,535,101,970 1,250 Crores

Per Capita Annual Exp on Medical for 55 Lakh Popln.

2,279

Rounded Per Capita Annual Exp

2,300

Note – The above guesstimates is based on following assumptions inter alia: Consultant incomes are taken to be just about lower values, equivalent to about Rs 1.5 lakhs per month. Actual incomes may be much higher since this includes, consulting, visit fees, procedure fees etc. Medicine costs are also taken at lowest. Public and Municipal health expenses are based on monthly bills for full staff, not on actual expenditure. Army medical services are not included in this table.

mfc bulletin/Aug 2010-Jan 2011

27

2.4 Health Care Facilities: Rural Profile

2.6 Learning from Doctor and Hospital Data for UHC

I have no up to date information on private providers in Nashik, nor of Malegaon. A detailed report on Health Human Resources (HHR) of the district, with breakup by cities, towns, villages, etc., is necessary to understand how UHC can manage provision and access. Some general points can be said here:

1. Good public hospitals are scant and crowded, while others are dysfunctional because of politics, corruption and malpractices. People use public hospitals as last resort or because there is no money to shell. 2. In Nashik, the hospital and consulting rates are high, but RSBY or low cost policies hardly meet the costs. Even co-payments cannot meet the gap. 3. The rates in the private health sector look high, especially for BPL and just APL people. Even Mediclaim finds it tough to pay these rates and the recent cashless row is mainly because of these high rates. RSBY rates are far too low for this sector, which is why RSBY does not find takers in the private sector even in poor districts like Sholapur (Source: My conversation with previous Director of Health Services of the state). 4. The MVP hospital rates are impressively low; the hospital has 500 beds and has occupancy of 50-60% at anytime. Being a medical college hospital, it is indeed a win-win arrangement for the institute and patients. 5. Primary Care Sector: While computing population ratios, we assumed that GPs serve only the city population which makes it almost one formally trained GP for 1250 population. Do we need more

• Rural HHR is likely to be a reverse picture of the Nashik city: fewer consultants, more GPs, unqualified doctors, etc. Malegaon city HHR can be a small replica of Nashik city. • Hospital beds will be more in public sector (taking together PHCs and CHCs) than private sector. Trust hospitals are scant or absent in rural areas. • MBBS doctors will be mainly in government centers, while non-allopaths will dominate the private sector. • Rural services are clustered in some villages: our study in five blocks of Nashik showed that all doctors are clustered in 16% of villages. This distribution would not be better today. • Barring some work in basic specialties, most of specialty work flows to the city. Hence rural communities have to travel to Nashik or Malegaon for all higher medical needs. 2.5 Fees and Rates

Item GP Fees (Usually Includes an Injection) PG-Consulting-Specialist’s Consulting Fees Super Specialist’s Consulting Fees Normal Delivery$ LSCS (Caesarian Section)$ MTP (Medical Abortion)* Appendix Surgery Hernia Hip Fracture: Surgery with Prosthesis Cataract (Indian Lens) Angiography Heart-Bypass Surgery (CABG) ICU Daily Charge

Table 5: Fees and Rates of Various Sectors in Nashik (in Rs) MVP Hospital Rates Private sector charges RSBY Maximum Cover (without medicine) 30-50 None 200 None 500-1,000 None 1,500 10,000-30,000 2,500 1,500 25,000-50,000 4,500 500 2,000-4,000 1,000 15,000-40,000 8,000 1,500 10,000-25,000 8,000 1000-1500 25,000-70,000 10,000 500 5,000-12,000 5,000 ? 6,000-10,000 10,000 NA 150,000-200,000 200 2,000-5,000 300 750

Ventilator Bed Charges 500 150 Sonography# 600 X-Ray Chest Adult Size 250-500 Hysterectomy Vaginal 25,000-50,000 10,000 Medicine Costs As per details 15,000 $ In MVP hospital 1st &2nd delivery and LSCS are free and there is no separate charge for LSCS. Hernia in pediatric age group is free. *MTP with TL is free in MVP, #Obstetric sonography in MVP hospital costs Rs 100

100-250 110 2,000 ?

28 GPs in the city’s health care pyramid? We also need primary health workers in the urban health system. Only 7 % of the GPs are MBBS. So we need ways to train others and ‘mainstream’ them. 6. Public and trust hospitals together command 50% beds, and can be a major leverage for UHC. 7. The public hospitals need to be made fully functional and trust hospitals utilized more fully. 8. Most public and trust/institute doctors are in some kind of private practice and hence the public hospitals are starved of dedicated doctors. This is also a major challenge to UHC. 9. NMC itself needs to work out a system of accreditation of nursing homes. The NMC data is quite deficient.

3.0 Mediclaim and RSBY (Rashtriya Swasthya Bima Yojna) in Nashik 3.1 Health Insurance Apart from the four public sector companies offering mediclaim policies, 18 private companies are in the fray. When I googled for information about my age, 1 lakh cover and city of Nashik, websites offered me 16 products from 15 companies including four from public sector companies. About 18-20 TPAs (third-party agents) operate in the city. The total number of insured people is not available but is surely far more than the 3% figure that is quoted. One reasonable guess is that 20% people of the city have some kind of cover-either from company, government or insurance. We also have company medical cover for employees, which can be with private insurance firms. According to Mr Kini, a Mumbai-based chartered accountant studying this problem, the current mediclaim business is a vicious circle where providers, TPAs, insurance companies and consumers are cheating each other. Consumers want to make maximum claims, providers want higher tariffs and make false claims, insurance companies are in claim denial mode, and TPAs ask for ‘cash’ to process higher claims. The costs of care are jacked up because of this vicious circle. RSBY tariffs can’t be fulfilled in this adverse environment.. Tariffs may be declared soon for all procedures. These are important lessons from UHC. 3.2 RSBY (Rashtriya Swasthya Bima Yojna) RSBY premium is borne by government, hence cards are issued free of cost to BPL families. However, the RSBY implementation in Nashik district is almost zero since the public sector company (the National Insurance Co Ltd) branch in Nashik is not active on this front, unlike Jalgaon district. This could be due to some factors including the issue of non-availability of providers at the

mfc bulletin/Aug 2010-Jan 2011 RSBY rates. Secondly BPL (Below Poverty Line) survey in the NMC area is not completed — this is what we learnt, so RSBY cards have not been issued to families, thus the funds are just not spent. In effect we have no RSBY till now in the district. The agency for each district is decided by Ministry of Labour (MOL), Government of India for 2010-11. The MOL offers Rs 70 per policy to the agency for all work up to issuing RSBY cards, which is a good incentive. (Arogyam had requested Government of India for getting this work. RSBY must be kick started in Nashik, without which BPL families cannot get cover.)

4. Making a UHC plan for Nashik Admittedly, making and implementing a full UHC plan for Nashik is a long journey. UHC needs four components: supply of health care, users, insurance and control bodies. Let us see about how these four components look in the context of a possible UHC. 4.1 Supply of Health Care We have a heavily clustered health sector in Nashik district, as is expected. Rural scene is dominated by GPs, many of whom are not qualified. Consultants dominate the cities. The district data on rural doctors’ lists only about 25% of doctors for various reasons, including the factor of bogus doctors. Consultants are mainly in the cities, so are the beds. The GP - primary sector - is dominated by non-MBBS doctors, the later being only7% in the city of Nashik. But the gross ratio of population per GP seems close to good (assuming 1000 per GP is good for India). We have no norms on population per consultant. This is very difficult factor to decide or guess, because it involves epidemiology, expectations of people, paying capacity, population density and transport access, supporting hospitals, the mix of public-private-trust sector and so on. The hospital beds are divided neatly between public and trust (semi-public) on one side and private hospitals on the other. One would like to think of using the publicsemi public sector as leverage, but the same doctors run these institutes in different time slots! They will try to protect the private domains rather than public-semi public sector. Accreditation and regulation of facilities is nearly absent although Nashik MNC has taken to registration long back. The ZP (Zilla Parishad) also started registration of doctors/nursing homes some 5 years ago. Even the data is incomplete.

mfc bulletin/Aug 2010-Jan 2011 4.2 Community and Users Obviously the rural-urban divide, socio-economic and organized versus unorganized sector disparities are all going to matter for UHC. The acceptance of UHC will be vastly different across disparities. UHC needs to be designed with focus on BPL and APL, but the better off may not want it in that form. Hence UHC may be sectoral and partial to begin with. 4.3 Pool and Insurance Although RSBY, mediclaim and low cost insurance get nowhere near a UHC plan, these are the building blocks for future UHC. A reformed and revised RSBY can be a major leverage, along with low cost policies that are described in later sections of this article. Group insurance schemes can make a swift move to pre-UHC formations. Even by modest estimation, the total annual cost of care in Nashik district is above Rs 1200 crores (per capita about Rs 2400). Theoretically, UHC has to reach a pool size of Rs 1200 crores just to meet all the claims, nearly half of it - the salary part included - coming from Government and urban local bodies. (For the state of Maharashtra with 11 crore population, this 50% provision means a support of nearly Rs 12000 crores through state, center and local bodies. The pool has to also provide for some capital costs for facilitating health facilities in unserved areas, but we are not taking this issue now. Currently the total public provision is not more than Rs 4000 crores including state Government health services, medical education department, local bodies including Mumbai Corporation and ESIS). 4.4 Control Agencies Any new UHC apparatus has to have a district body to manage and monitor. Since UHC will be a legal arrangement, the control body has to combine local bodies (Municipal Councils and Zilla Parishads), insurance bodies, associations like NIMA (National Integrated Medical Association), IMA-FOGSI, trust representatives, clients and client groups, etc. Single agency to pay for claims may be a risky idea, because of corruption and caste politics. The ground rules for claim settlement will be uniform, but agencies for claim settlement/servicing should be more than one with some element of competition and preference. It involves right actions from many corners and fronts — government, labour ministry, NMC, Zilla Parishad, trusts, medical associations, industries and employers, political parties and leaders, consumer groups, etc. In my opinion the most essential parts of UHC project are: a) Get enough good hospitals in secondaryand tertiary level in the city that offer low cost care with

29 scientific and rational parameters. This can be done by helping trusts/groups start hospital facilities in NMC or government-funded premises and equipment grants. b) Improve and include public hospitals of NMC and government of Maharashtra to service UHC (now mainly RSBY) at same tariffs, earn income from UHC and give incentives to doctors and staff. Gradually public hospitals should earn salaries from UHC services rather than treasury. . c) Make available low cost insurance cover in the context of tariffs in the ‘good hospitals’. d) Start good pre- and post-hospital care network through NMC-State Government-Trust-PPPs to reduce costs and exploitation, improve access and efficiency. We can begin with these small steps and work towards UHC.

5. Efforts and Experiences of RCT for Low Cost Health Care through Insurance RCT-Arogyam (RCT is Rukmini Charitable Trust, Thane. Arogyam is a local firm working with RCT for spreading low cost care. Therefore RCT-Arogyam is a combine of the two - this arrangement helps in a trust to be in the business of health insurance.) has a small unit working from its Nashik office. The insurance revenue at this point is Rs 88 lakhs, which is small but able to barely meet its expenses. So RCT-Arogyam itself has about 0.1 % share of private plus trust portion of Nashik’s health sector. RSBY is expected to cover some 1 lakh plus families, which costs the government about Rs 5 crores annually. No other company is offering low-cost insurance; and RSBY is inoperative. If we presume BPL and just APL sector together to be around 50% of population, which is 27 lakh population and hence about 5 lakh families, the RSBY business is about Rs 25 crores. Therefore it is a long journey on a narrow path. The goal is to ensure providers supplying low cost care in each ward, and to get APL families to buy insurance and seek care and RSBY made available at the earliest. This will enable the BPL+APL populace to get medical care and cover with dignity minus today’s hassles. RCT-Arogyam offers policies with the backing of Oriental Insurance. They have four individual policies, offering various plans much like mediclaim. The principal instrument for general category is the universal health cover (UHC) available at Rs 500 annual premium. This offers Rs 30,000 cover, within Rs 10,000 at one time, and includes pre-existing illnesses, excludes pregnancy and abortion. This is for a family of five.

30 5.1 RCT’s Group Medical Cover (GMC) Arogyam-RCT combine launched their group insurance scheme last year with very attractive terms. When I was in the YC Maharashtra Open University last year, we got about 300 plus families of employees covered for all problems (including pre-existing illnesses) up to Rs 1 lakh cover, except maternity, dentistry, plastic surgery and AIDS, for all family members from a 3-month old baby to 79-year old adults. The total payment was Rs 12.5 lakhs, which included a small accident insurance from LIC. As the year closes, the claims have crossed the premium amount and will almost double the premium by end of the term. I may point out here below the experiences. There was very stiff opposition to this by unions as some elements were interested in reimbursement schemes which were lucrative to middlemen as well as doctors in the league. The scheme was roundly attacked for 2-3 months. Even the new Vice-Chancellor asked me to close the scheme and shift to Wockhardt hospital. He did not know that Wockhardt hospital did not provide general services but only services for heart, joints, cancer, etc. Some influential private doctors harassed employees and got them to complain against the GMC. But at the end, it seems the university is giving them a renewal this year. It has saved the university about Rs 20 lakh this year, and the general employee is happy and thankful. But the premium costs will go up this year due to higher claim ratio. The GMC was purchased by some other institutes. At this stage of medical insurance, the national pool is able to absorb such losses at places. UHC will have to draw from state, city or district pool and account for losses. The ‘free meals’ will stop once the business gets serious. Somebody will have to foot the subsidy. Theoretically, GMC is much better than individual insurance. It saves many administrative hassles and creates a bargaining power from the organized sector and disbands the reimbursement rackets. We feel that even the state government and its bodies should go for GMC rather than reimbursement, the latter has created huge corruption rackets. Incidentally, the civil surgeon of Nashik was arrested in this period for amassing wealth from such a racket (cash Rs 12 lakhs was found in his office). GMC can pave the way to UHC in steps. But one problem is the huge number of workers who are in the unorganized sector.

6. Some Expected Hurdles in UHC and Suggestions Universal health access cover is far different from the sum total of individual policies available today, but the path to UHC has to cross this jungle. The Canadian health

mfc bulletin/Aug 2010-Jan 2011 reforms started off from a county. To understand the major effort on UHC, we need to understand and plan for a district of good size and average resources and assets. Nashik can be a case study. If UHC is the long term goal for making good care available to all, some problems need to be understood. Suggestions are given in italics. 6.1 Unions and Employers Unions do not support group insurance because of vested interests and mistaken ideas of good medical care at any cost and with company support (third party payment). We need educate trade unions to understand the health economics and the benefits of GMC. Women’s microcredit groups, associations of domestic workers, selfemployed or lone workers, farmers, contract labour and staff etc need to buy cover, perhaps with some help of employers. It is necessary reach out, propose good policies and products. 6.2 Consumers Unaware of RSBY The target consumers are unaware of both RSBY and APL policies. We need to tell them about the schemes, their rights, benefits and enrolment procedures. However the organizers themselves are wary of promoting any insurance, they see it as a liability. We can start from some organized and needy groups. 6.3 RSBY inoperative in Nashik Whatever the tariffs of RSBY, it can still be a major leverage for getting hospital care. The central government’s Ministry of Labour must monitor the situation in each selected district. The distribution of RSBY, implementation must be monitored by state Government health department. 6.4 Cashless! The consumer does not have to pay cash to the hospital; the insurance agency/TPA settles it by cheque at the time of discharge. Unless these cashless transactions cease; medical insurance will bleed the companies. Rates must be prefixed and declared by medical insurance companies, district associations and/or RSBY. Extra claims should not be entertained. 6.5 The Unwilling Provider This is a major hurdle; we cannot get enough private providers at economy tariffs. But they will be available as we go ahead with Trust providers. A long term alternative/addition is to get NMC owned public hospitals to serve the UHC and earn incomes and incentives for staff. The effort is on to negotiate with major Trust hospitals that are looking for stable clientele. This will optimize other private sector rates.

mfc bulletin/Aug 2010-Jan 2011 Some corporate hospitals may also be willing to serve the sector, with copayments. But co-payment needs to be capped. 6.6 The All Pervasive Private Consultant Public hospital doctors run their own clinics and hospitals. The same is true about trust hospitals. At the end of the day it is the private consultant calling the shots in all three areas. Can we address this problem under the umbrella of UHC? In the long run, yes! But immediate implications are scary, as private consultants will resist amalgamation and protect the private establishments. 6.7 Cross Practice Since GP sector is full of Ayurveda and Homeopathy practitioners, and that they have no systemic (and systematic) formal training in modern medicine; referral criteria are very flexible and there is no accountability to modern science or any apex body. The patients suffer and so do the good doctors who would like to stick to science. A bridge course is necessary for GPs for better diagnostic protocols and standard treatment guidelines. 6.8 The Quack Problem Quacks are ubiquitous and in various forms, the main form is a medical practitioner without any valid formal degree diploma. They are aplenty in many cities and rural parts. They influence many decisions of patients and doctors regarding interventions for purely business reasons. The good doctor suffers from them in several ways. Quacks cannot be wished away at least in rural areas. We should make available graded certificate courses for these village practitioners and let village panchayats give them place and clientele. This will restrict the problem to PRI framework. 6.9 Dysfunctional Government Super-Specialty Hospital The high cost MS hospital in Nashik is a non-starter for last 12 years. This is expected to take care of all major problems regarding brain, heart, kidneys and joints etc that require high cover and cannot be done through RSBY. he HHR is the key, which is a general problem with Maharashtra. There are no signs of this hospital starting in near future. I suggest that this hospital be handed over to the Medical University at Nashik, or Medical Education Department with full establishment costs. The state’s Directorate of Health Services has no capacity to run this hospital at all. This can be a PPP model and there is need to rope in successful hospitals like Shirdi Saibaba Hospital for its management for five years.

31 6.10 Malpractices Medical malpractices are quite common, and take many shapes and formats; some of them are now systemic like cuts and commissions. Here are some salient examples. • A reputed private company pays Rs 1 crore as premium for health cover, but the claim ratio reaches 4-5 times within the year. Every year they are ‘refused’ by the insurance company. This happens because of false claims, overbilling by hospitals and collusion of hospitals with worker unions. Many top companies have similar experience. My suggestion: switch to GMC or declare cap on medical expenses. This holds good also for state government establishments: give GMC and not reimbursement. • Some city hospitals have been blacklisted by insurance companies and TPAs, for serious malpractices. • Many hospitals charge double for insured patients. Fixing service costs is the answer. Clients will start choosing economy hospitals. Unless claim scrutiny is genuine and tariffs settled and accepted, no insurance company can profit from medical insurance. • Rampant cuts and commission practice (to the tune of 50%) has already made life vicious for patients and consumers and also for the ethical, good doctors. This causes false claims, overbilling and mistrust and a feeling of helplessness among good doctors and honest consumers. UHC will take care of this problem in the long run. A good primary care network that supports pre- and post- hospital care will reduce some of the problem. • Reimbursement is lucrative: Government itself has got this great addiction. Everybody profits in reimbursements, except the helpless taxpayer. Just to give an idea of the chronic scam: a Class 1government servant claimed Rs 9 lakhs for the two cerebral strokes of his 75-year old father (who eventually died in one year). He could not have made this money had he just some RSBY-UHC cover and no reimbursement scheme. My advice is: Wean away government establishments from reimbursements. 6.11 Ensuring Outpatient Services Prevalent insurance covers only inpatient costs and some procedures. Many costs are incurred as outpatients, especially most routine investigations. These are costly and so are medicines. Hospitalization is required less often, but outpatient consultation is required often. Unless the latter gets cover in the scheme, people are

32 not interested in cover; they would rather prefer to decide if and when hospitalization is imminent. It may be a good idea to declare consultation rates for each city and district on some criteria. But free outpatient is unlikely to succeed. 6.12 Covering Obstetric Events Childbirth, caesarians, abortions are not covered in most policies as these are ‘predictable’ events. But UHC must cover these events. 6.13 Corruption and Caste-Politics A real hurdle can be corruption mainly while settling claims, which is happening with cashless claims and some TPAs. This will balloon as the UHC grows. The users, providers, TPAs may align on caste-corrupt lines. This menace, along with caste affiliations will make UHC unattractive for many honest and leading providers. This needs careful consideration. This rock alone can shipwreck the UHC. 6.14 Short-Term Recommendations We need many action points from different agencies: 1. The NMC should take an active political role, but if this is not forthcoming, a trust like RCT –Arogyam has to engage in advocacy and promotion, negotiate with IMA and consultant bodies. 2. Conduct and publish studies of costs of care, expenditure patterns, utilization of public-private facilities. 3. Systematic registry of all care providers, with details and updates, start an accreditation mechanism with help of administration and IMA-Nashik IMA, etc. (the related nursing establishments regulations and laws must be active by this time). 4. Reform the hospital regulation mechanism or accreditation system to guide clients and consumers to hospitals with better practices. 5. Start and spread of RSBY, and low cost policies, GMC, and educate people about their importance. Work with small communities and associations like micro-credit groups, cooperative bank account holders. Get private providers and Trust hospitals to support RSBY and UHC. We need a scheme for promoting trusts and groups to start hospitals by supporting capital costs, offering place and space in each ward, with service guarantee. This will cut down hospital rates and make them UHC-friendly. This will also thwart growth of private hospitals that we see today. 6. Buttress RSBY with some co-payment, but declare official co-payments. 7. Establish a network for good primary care: pre- and

mfc bulletin/Aug 2010-Jan 2011 post-hospital care with help of Family Physician association, NMC dispensaries and urban health centers. 8. Activate Nashik super specialty hospital, if necessary by PPP with likes of Shirdi Hospital. 9. IRDA has to look into ‘regulation’ of the insurance sector and tariffs. Put information on website. 10. Set up a district fund pooling Government resources, NMC funds, donations, RSBY, etc., under a joint local body (which needs a State Act for support) which will manage UHC. There is no way India can revive its public service to commanding heights in competition with the current private sector. Building a UHC is a chakravyuha; but probably there is no other way for us. We have to start doing many seemingly imperfect things with a vision of UHC for low cost, rational and humane care for all people. It has to be a multi-systemic effort, and many players need to team up. It is also an ideological battle. It is nobody’s case that UHC is easy or a panacea. UHC will not be free of corruption and malpractice. But even then, we must all walk towards a UHC system.

Addendum Dr Suhas Joshi, Medical Director of RCT wrote the following response to this paper Dear Dr Ashtekar, The scheme which we launched last year in Nashik was an effort to reduce cost of treatment at all levels. First we launched a policy named platinum plan. The plan had pre existing diseases covered from 4th month onwards after taking the policy. For OPD we had empanelled about 10 GPs in the plan the policyholder will get free consultation for the year from the GPs. The idea was if consultation is made free people will go to the doctor early and with early treatment the number of hospitalization will come down. The trust had agreed to pay the GP Rs 50/- per family registered with them irrespective of whether the facility was availed or not. The scheme was a failure. The reasons for failure were as follows • Renowned GPs were not interested .They felt the amount too meager to attract them • Those who joined were all BAMS and homeopaths, and the policyholders were not ready to consult them even free as the trend is to consult directly a consultant. • As we could not provide the numbers the registered GP’s lost interest in the scheme. So from next year we omitted this facility. • We had empanelled few consultants who had agreed to give across the table discount to the

mfc bulletin/Aug 2010-Jan 2011

•

•

• •

33

policyholders in the OPD treatment. Very few people availed this facility. Those consultants who had some patients were cooperative but others started complaining that we are not getting clients from you. It was expected that the panel doctors understand the benefits of the scheme and promote this to their patients, which was in their interest also. We had taken a few dentists on panel who offered one free dental check up and concessional treatment. This benefit was taken by many. We had tied up with Thyrocare, Ranbaxy and Piramal Diagnostics who would give discount to policy holders at investigation level. The beneficiaries were small in number. As the treating doctor had his preferences about reports from a particular laboratory, this was conveyed to us by our policyholders. Pre-existing cover mediclaim was the only benefit for which people are coming to us. We had appealed to a number of agents working in insurance field to promote our scheme. A number of them have joined us, but they are selectively

selling our products to those who have planned treatment or severe disease resulting in high claim ratio. Unless we get all people to join this scheme the company may cancel the special policy sanctioned due to loss. • We had meetings with a number of senior citizens groups as we can provide cover for them, but the overall response is they want everything free of cost. • We met some politicians who are reportedly working for masses and are leading groups of workers, drivers, autorickshaw drivers, etc. The politicians are not interested in long-term projects like this. • This all boils down to numbers. If we can provide numbers many service providers will join. And doctors must promote health insurance as it is in their interest also. These experiences will surely help creating a roadmap for UHIS. Regards, Suhas Joshi A Note on Group Medical Cover (GMC) from Dr Suhas Joshi

Group medical cover for health insurance is a tailor-made health insurance policy specially designed for a particular group taking in to consideration their needs. The routine health insurance readily available on individual basis has got many problems. The individual medical insurance is available to all up to the age of 45 years. But in this policy pre existing diseases are not covered. Usually people avoid confirming their pre existing disease at time of insurance for fear that the policy may not be issued. Subsequently when it comes to settling of claim they suffer as doctor makes mention of pre existing disease. After the age of 45 pre medical checkup are necessary, involving cost, inconvenience and time. Any disease detected during the check up is labeled as pre existing and excluded from claim making the policy useless as that is the condition for which he will require treatment. The latest age of entry is 65. The GMC policy is designed to suit the needs of the group. You can take cover for pre existing diseases right from day 1 of the policy, maternity and newborn child can be covered, a cover usually not available with routine policies. The policy is issued to a defined group, for example employees of an organization, members of club, members of professional organization, members of a charitable trust etc. The condition is all from the group should be insured. Factories, employees of business establishment take the benefit of this policy. There is flexibility of terms as per requirement. We have issued a group mediclaim to Yashwantrao Chavan Open University covering all pre existing diseases from day one but without maternity cover. We have issued a GMC to a group of colleges from Dhule with routine terms, but the average premium comes less than normal rate of individual policy. We have covered a group of drivers with accidental cover, the cost per lack comes just RS 145,in it there is a death benefit of Rs.80000/- and hospitalization expenses reimbursement of RS.20000/-. Yashwantrao Chavan University has paid a premium of Rs 1139675 plus 10.3 % service tax, the claims so far settled are to the tune of Rs.1279343, which exceeds premium paid but still some time to go till December end the claim ratio will cross 100%.But as compared to reimbursement procedure here is a benefit of cashless treatment all over India. Of course the tendency of exploitation is seen here also.Well-known manufacturing units from Nashik are known for their false claims resulting in very high claim ratio and cancellation of the policy mid way. That also caused a few hospitals to be blacklisted for being hand in glove with workers in issuing false receipts. This is one way to insure as this gives a comprehensive cover and should be opted for by groups. More and more groups should be encouraged to take GMC. If given option as in individual policy many people do not take insurance cover for he/she feels he/she is healthy and nothing will happen. In GMC she/he gets automatically covered with the group.

34

mfc bulletin/Aug 2010-Jan 2011

Universal Health Coverage in Thailand: What Lessons can India Learn? - Renu Khanna1

1. Introduction In 2001 Thailand became the first developing country to introduce the Universal Health Coverage after various attempts since 1971 when the country began investing in infrastructure and health human resources. By 2002, Thailand had achieved universal coverage for the entire population through a general tax funded Health Insurance Scheme. This paper attempts to find out what lessons India can learn from Thailand’s experience. I examine first the context within which the health reforms and the Universal Coverage were implemented in Thailand, the design and the characteristics of the Universal Health Coverage package, the achievements and impact, including on Sexual and Reproductive Health care, and finally what lessons we in India can learn from Thailand’s experience. Situation Prior to Universal Coverage Just as in India, the situation prior to universal health coverage was characterized by fragmentation, duplication and gaps. In 1991 more than two thirds of the population was uninsured. Since 1975 there was a Low income Card Scheme (also called the Public Welfare Scheme) for the poor and the disadvantaged that was extended to those over 60 years, monks and children under 12 years of age. This was financed by general tax revenue. It suffered from underfunding and low patient/ user satisfaction. Because of the strong political support the Voluntary Card Holders increased from 1.4% in 1991 to 20.8 % in 2001 with the government subsidizing 50% of the premium (Tangcharoensathien et. al., 2007). However, as in India with the targeted BPL (Below Poverty Line) cards, the non-poor benefitted. Although 35% of the card holders were genuinely poor, only 17% poor were covered by the scheme. In 1983, there was a Voluntary Health Card scheme for the border line nonpoor households who were not eligible for the LIC. Because this was ‘voluntary’ it suffered from risk selection – the sick joined and those who were healthy left the scheme resulting in the scheme becoming financially unviable. There also existed the Civil Servants Medical Benefit Scheme (CSMBS), like our Central Government Health Scheme or CGHS. This was the most generous scheme but also very expensive and suffered from rapid cost escalations. This scheme covered 15.3% of the population in 1991. By 2001, with public sector downsizing, this shrunk to providing coverage to 8.5% (Tangcharoensathien et. al., 2007). 1

Email: <sahajbrc@yahoo.com>

There also existed a Social Security Scheme for the formal private sector employees. This was a tripartite payroll tax contribution scheme and was mandatory. It was extended from larger firms with 20 employees or more in 1990 to firms with even one employee by 2002. A study in 1996 showed that the uninsured were the poorest and the least educated (Tangcharoensathien et. al., 2002). Table 1 gives an account of how the health insurance system developed in Thailand.

2. Factors that Influenced the Reform Process Guiding Values Thailand has been a monarchy and a Buddhist state. The King’s ideas of ‘Sufficiency Economy’ – balanced and sustainable path of development have guided the national government policy. ‘Sustainability economy places humanity at the centre, focuses on well being rather than wealth, sustainability at the core of thinking, understands the need for human security and concentrates on building people’s capabilities than potential. It adds a spiritual dimension to human development.’ (Thailand’s Human Development Report 2007) The 1997 Human Development Report, stated that the country had to ‘avoid mindless growth’, growth which is ‘jobless, ruthless, voiceless, roofless, futureless.’ Definition of Health and Health System: The National Health Security Act of 2002 gave a new definition of Health by i. removing ‘absence of disease’, and ii. adding Spiritual Health at two levels – at the individual level to include ‘sound commitment to a healthy life’, and at the social level to promote ‘a public will to equity’. ‘Health System’, was redefined as ‘all systems holistically interconnected affecting health of people’. Political Context Thailand went through a process of democratization and got its new constitution in 1997 when new actors entered the arena of policy making. The Thai Rak Thai (TRT) Party used the slogan of ‘30 Baht to treat all diseases’ as its main campaign. The party turned the current economic crisis into an opportunity to capitalize on the problem of health care services which needed a reform. After winning the election the TRT Party kept its promise

mfc bulletin/Aug 2010-Jan 2011

35

Table 1: History of Health System in Thailand (before UC) Health Insurance Scheme

Target Population

Low-Income The poor, Card Scheme elderly, children < 12, (LIC) since 1975 disabled, monk, community leaders, health volunteers Voluntary Non-poor household not Health eligible for LIC Card (i.e. personal (VHC) since 1983 income > 2,000 Baht/month) Government Civil employees, Servant their Medical dependents, Benefit and retirees Scheme (CSMBS) from the public since 1980 sector

Source of Health Care Finance

Government Health Expenditure Per Capita in 1999

Provider Payment Method

Majority of Health Care Provider

General tax

363 Baht + additional subsidy

Global budget

Public providers, referral line for inpatient care

Household 500 Baht + General tax 1,000 Baht

250 Baht

Proportional reimbursement among 1st, 2nd, and 3rd care level

Public providers, referral line for inpatient care

General Tax

2,106 Baht

Fee-forservice

Public providers, (Private providers only for emergency)

Social Security Scheme (SSS) since 1990

Private formal Payroll tax 519 Baht Sector employee, Tripartite > 1 worker contributions establishment (employee, employer, and the government)

Capitation

Private Providers (Contracted hospital or its network)

Private Health Insurance

Better-off individuals

Fee for service with ceiling

Public providers and private providers

Household or employer in addition to SSS

N/A

(Source: Damrongplasit K., 2009)

of making universal coverage one of its high priority agendas. The civil society groups also contributed. In 2001 eleven NGO networking groups collected 50,000 signatures and submitted their draft Universal Coverage Bill to the parliament. There was also strong support from Ministry of Public Health officers and policy researchers. The generation of student activists of the 70s and 80s now became researchers, leaders in the new government, policy makers, and strong civic movement leaders. There was a close relationship between the reformists and the politicians, and the reformists and the researchers. The reformists thus played the bridging role. Thus, three important ingredients – technical capacity, strong political will and overwhelming public support

– were present to make Universal Health Coverage possible. This relationship also became another slogan – ‘the triangle that moves the mountain.’ The new constitution defined health as a human right to be protected by the state. It spelt out equal entitlements to health for the elderly, disabled, abandoned children and other vulnerable groups and mandated consumer and environment protection for the sake of health. The fundamental role of the state was acknowledged as ‘to provide public health services to all people at the same standard’, ‘…. disease control free of charge’, health services under the constitution to be characterized by ‘equity, efficiency, quantity, transparency and

36

mfc bulletin/Aug 2010-Jan 2011

accountability to the community’. The political reform of 1997 called for re examination of the health sector’s role and approach. It stated that the health sector had to reorient its vision and mission to achieve the constitutional mandate. Context of the Health Sector Successive governments had invested in public health infrastructure for two decades. The pro poor, pro rural policy was not merely rhetorical. Between 1982 and 1987, budgets for urban provincial hospitals were frozen and shifted to the development of lower level rural district hospitals and health centres. Even though the country was going through a period of low economic growth due to an oil crisis, there was reallocation within the health sector. The result was increased utilization in rural health centres and increased geographical coverage to the most peripheral level. Thailand was also following a long term human resource production plan, midwives were being trained. All categories of health care providers trained in publically funded institutions were required to do mandatory rural service in district hospitals since 1972. Between 1994 and 2004, the human resources to population ratio changed as shown below. Table 2: Increase in Human Resources M anpower to pop. ratio

1994

2004

Doctor

1 : 4165

1 : 3305

Dentist

1 : 19677 1 : 15143

Registered nurse

1 : 1150

1 : 652

(Source: Patcharanarumol W., 2008)

But the rural-urban disparity continues to haunt Thailand’s health system. There were no vertical programmes, integration of preventive, disease control and health promotion was the rule and not an exception. Economic growth rate was rapid through the decade of the 90s. Internal peace and stability freed up 12% of the total national budget for the social sector. The Ministry of Public Health budget increased from 4% in the 1980s to 10% in 2001 (Tangcharoensathien et. al., 2007). There existed institutional capacity for evidence generation and knowledge management in the country. There was an effective interface of researchers and policy makers for evidence based policy development. Several actors worked collaboratively for capacity building in health system and policy research. USAID had a Health Care Financing Programme, Pew Foundation supported the International Health Policy Programme (IHPP),

Health Systems Research Institute and Thai Research Fund. There was institutional collaboration between the Ministry of Public Health, IHPP and London School of Tropical Medicine. Analyses of the factors that affected health policy highlighted five critical issues: i. higher cost of health expenditure ii. unbalanced economic development iii.rapid technological evolution iv. political and social reform v. government sector reform Higher cost of health expenditure Although there was good health infrastructure and a communicable disease programme, it was running into crisis. Because of the expansion of the public and private health sector, the Thais were moving towards using more facility based services. Between 1980 and 1998, national health spending increased 11 times and per capita health expenditure increased 9 fold, higher than the per capita average GDP growth of 7%. The share of GDP taken up by Health doubled from 3.82% in 1980 to 6-21% in 1998. Access to and quality of health services were not good, the health service providers’ were overworked. (National Health Commission Office, Thailand, 2008) Unbalanced economic development Poverty decreased from 33% in 1988 to 11% in 1996 (National Health Commission Office, Thailand, 2008). But the development was unbalanced with increased disparities among marginalized populations. Economic policy shifted to exports in services and manufacturing. There was increasing environmental pollution and natural resource depletion. Rural to urban migration resulted in decreased agricultural growth. Thailand was facing a disruption of social structure and relationships and an erosion of social and cultural capital. Deterioration in social ecology resulted in HIV and AIDS, traffic injuries, stress, cancers. Rapid technological evolution Health technologies researched in industrialized societies were being imported to developing countries at high costs. There was a widening gap of inequity – accessibility of drugs, for example, for HIV and AIDS was for the wealthy. Similarly, for cancers, radiotherapy and use of medical equipment was for the wealthy. The government realized that ‘Universal health care can never be achieved with reliance on importation of costly health technologies.’ Investment in government health research increased from 0.2% of public health budget in 1992-96 to 0.52% in 1999, although compared to research in other sectors, health research was not top priority (National Health Commission Office, Thailand, 2008).

mfc bulletin/Aug 2010-Jan 2011 Political and social reform Civil society movements gained strength in the 90s. These played a decisive role in shaping the reform agenda based on Human Rights, democracy and participation. Government sector reform The Constitution emphasized accountability and transparency of the government sector. A National Counter Corruption Commission was set up and an Official Information Act was legislated. A three year Government Sector Reform Programme was launched in 1999 emphasizing decentralization, expenditure management, revenue management, human resource management. Human resource management stressed highest levels of efficiency, quality and integrity. Private Health Sector In rural areas the private sector is not well developed. Only a few private practitioners are available and cannot provide comprehensive services. However the ‘internal brain drain’ is a much quoted phenomenon in Thailand. Thailand’s policy to promote the country as a medical hub of Asia resulted in well trained professionals from rural public facilities to come join urban private hospitals.

3. Contents of the Health Sector Reforms

37 Table 3: Comparison of Systems Design between UC Scheme and SHI UC Scheme

SHI

Primary Care Network. 100 bed-hospital, as Service mostly urban Contractor Typical model: health centres and District population hospital, as mostly rural population Referral

Ensure better referral

No referral, covered within the contractor provider except some limited referral to other supra-contractor hospitals Capitation inclusive of OP and IP

Capitation for OP, Global budget and case base payment (DRG) for IP. This is to prevent under-admission of inclusive capitation Integrated into curative Separate package: Dental, maternity: flat rate Maternity package payment, dental: FFS and ceiling. Higher admin cost Payment Method

Coverage

All family members, Contributors only individual member card issued (not a family card)

Health Sector Reforms has had four fold objectives:

(Source: Tangcharoensathien V. et al, 2007)

i. Improvement of efficiency in the health sector – rationale use of health care facilities from the primary level, referral care at the secondary and tertiary levels, an Essential Drugs List and a Capitation Contract Model for cost containment. ii. Promoting equity – standardization of the benefits package across all the schemes, ensuring equal access to health care, convergence and standardization of level of resource use. iii.Ensuring good governance – by minimizing conflict of interest through Provider-Purchaser split functions, with the National Health Security Office as the purchaser and Ministry of Public Health and private sector as providers and contractors. iv. Ensuring quality of care through an accreditation system and utilization reviews. Universal Health Coverage The social health protection as implemented in Thailand since April 2002 can be divided into three groups – schemes for public employees, schemes for private employees and schemes for rest of Thai population, namely, those in the informal sector. The Universal Coverage Scheme strived to improve upon the earlier Social Health Insurance design of the early 1990s. Table 3 shows the comparison between the Universal Coverage Scheme and the Social Health insurance.

As can be seen from Table 4 maximum number of people – 47 million or 76% of the population are under the Universal Coverage Scheme which is termed as a Social Welfare Scheme. It comprises of a comprehensive package including ambulatory care, in-patient care, high cost care, accident and emergency care, maternity services, an annual physical checkup, preventive and promotive health services, services based on indigenous Thai systems of medicines. Medicines are covered as per the National Essential Drugs List. Private bed, special nurses and eye glasses are not covered. High cost care and Accidents and Emergencies are paid on a fee for service schedule. Universal Coverage Scheme is funded through general tax. There is no copayment. Health facilities have to register under the scheme. Beneficiaries cannot go directly to secondary and tertiary facilities without referral from primary health services, except in emergencies and accidents. What are the Main Characteristics of the Universal Coverage Scheme? • Primary health care services are the key mechanism for provisioning in the Universal Coverage policy for two reasons. First, primary level is thought to be the best setting for providing quality care based on a holistic approach. Its location close to the community makes care socio culturally appropriate

38

mfc bulletin/Aug 2010-Jan 2011 Table 4: Characteristics of the Public Health Protection Schemes under the Universal Health Coverage Policy

Characteristics

Public Employees (CSM BS as a prototype)

Private Employees II. SSS III. WCS

The rest of Thai (UCS)

Nature of Scheme

Fringe benefit

Compulsory

Social welfare

Model

Public reimbursement model

Public contracted model

Population coverage 2006

Civil servant of the central government , pensioners and their dependants (parents, spouse, children)

Formal sector private employee, >1 worker establishments

No. of Beneficiaries (million) 4.2

9.1

% coverage

Compulsory

Public Public integrated reimbursement model model Formal sector The rest Thai private population, who employee, >1 are not qualified to worker establishprevious columns. ments Same as SSS*1

47

Same as SSS

Ambulatory services

Public only

Public & Private

Public & Private Public & Private

Inpatient services

Public & Private (emergency only)

Public & Private

Public & Private

Public & Private

Choice of provider

Free choice

Contracted hospitals or its network with referral line, registration required

Free choice

Contracted hospitals or its network with referral line, registration required

Cash benefit

No

Yes

Yes

No

Conditions included

Comprehensive package

Non-work related illness, injuries

Work related illness, injuries

Comprehensive Package

Maternity benefits

Yes

Yes

No

Yes

Annual physical check-up

Yes

No

No

Yes

Prevention, Health promotion

No

Health education, immunization

No

Yes

Services not covered

Special nurse

Private bed, special nurse

No

Private bed, special nurse, eye glasses

Source of funds

General tax

Tri-parties 1.5% of payroll each, (reduce to 1% since 1999)

Employer, 0.2-2% of payroll with experience rating

General tax

Financing body

Comptroller General Department, MoF

SSO

SSO

NHSO

Payment mechanism

Fee for service for OP DRG for IP (July 2007)

Capitation*2

Fee for service

Capitation J

Copayment

Maternity, emergency Yes: IP at private hospitals services

Expenditure per capita 2006 (Baht) 8,785 Per capita tax subsidy 2002 (Baht)

8,785 plus administrative cost

Yes if beyond the ceiling of 30,000 No 200,000 Baht ( depend on severity of patient)

1,738 *

211

1,659

579 plus administrative cost

Administrative cost

1,659 plus administrative cost

*1. Employees of “Not-for-profit” associations are excluded from this scheme. *2 some medical and dental services are reimbursed by Fee for service. *3 some medical and dental services are reimbursed by Fee for service. *4 Calculated from total spending on health both by capitation and fee for services such as dental care package, cardiac surgery, chronic hemodialysis etc. (Source: Sakunphanit T.)

mfc bulletin/Aug 2010-Jan 2011 and acceptable. Second, it was envisioned that a system with primary care as a gatekeeper will have lower overall health care cost. Thus, primary care facility is the main contractor and unit for registration of families. Primary care facility is the fund holder. Referrals have to be done by the Primary Care contractor as well as paid for the by the contractor. Direct access to secondary and tertiary facilities has to be paid for by the patient, except in emergencies as mentioned earlier. • In rural areas the District Health System is the Contractor or Provider of health services. In urban areas, private hospitals may be the Contractor. Around 900 contracted facilities provide services under the Universal Coverage Scheme. The registered Contractor provides free care. Earlier there was a 30 Baht copayment for an outpatient visit or inpatient admission. This was abolished in 2006 by the new government. • There are very minimum exclusion conditions – cosmetic surgery, mental health problems (because there is a National Mental Health Programme) and Renal Replacement Therapy. (Renal Replacement Therapy, Dialysis, etc. were included by 2007 end.) Initially ARV treatment was excluded but by 2003 there is Universal ART for all people living with HIV AIDS. • Provider Payment: Capitation for Outpatient services Capitation plus fees for preventive and promotive services, for example, for pap smears there is a set fee. A fee schedule set up by the National Health Security Office (NHSO) for Accidents and Emergencies Global budget plus Diagnostic Related Groups and age adjusted Capitation formula for inpatient care set up at the Provincial level. Other expenditure of the Universal Coverage scheme are capital replacement costs and no fault liability i.e. the compensation monies paid by NHSO to settle patient claims related to problems of medical practice. The Capitation rate is based on actuarial estimations, peer-reviewed by partners and also externally reviewed by ILO. In 2007 the government spent up to 1988 Baht per capita using general taxation. For Universal Coverage Scheme, it was 1202 Baht per capita. Achievements of the Universal Coverage Scheme • According to a 2003 survey by the National

39 Statistics Office 34% of the beneficiaries of the Universal Scheme are in the poorest quintile (Q1) and 26% are in Q2. In contrast, 39% and 43% of the SSS and CSMBS are in the richest quintile (Q5), only 7% of members of the Universal Coverage Scheme are in Q5. • Utilization is increasing for both outpatient (OP) and inpatient (IP) services. Utilization has shifted from tertiarylevel provincial hospitals to primary care units and district hospitals. Efficient use of primary care is being seen. See Table 5. Utilization is increasing for both outpatient (OP) and in-patient (IP). Table 5: Increase in Utilisation Levels of Care

OP Million Visits

IP Million Admissions

2001

2003 2004 2001 2003 2004

Primary Care Unit

29.7

43.7 63.8

District Hospital

19

36.7 46.2 1.1

2.1

2.2

Provincial Hospital

24.5

14.8

1.4

1.8

20.1

2.1

(Source: NSO HWS2001, 2003 and 2004 quoted in Patcharanarumol W. , 2008)

• The Socio-Economic Survey conducted by the National Statistical office reported declining trend in the incidence of both catastrophic health expenditure (from 5.4% in pre Universal Coverage to 208% in 2004) and impoverishment due to medical bills (from 2.1% to 0.5%). • Discrepancy between the proposed and the approved capitation rate has been decreasing. • Concentration Index (CI) of financial contribution is an index of the distribution of payments. It ranges from -1 to +1. A positive value indicates that the rich contributes a larger share than the poor, a value of zero indicates that everyone pays the same irrespective of the ability to pay. Empirical evidence suggests that the total health financing in Thailand is quite progressive with the CI at 0.5929. Direct tax is the most progressive source of financing health care with a CI of 0.9057 and indirect tax and social insurance contribution are least progressive with a CI of 0.57. The CI for general tax (direct and indirect is 0.6996, which is considered satisfactory (Tangcharoensathien et al, 2007)

4. Reproductive and Sexual Health and Universal Health Coverage Before the Reform Reproductive Health Services have been integrated into the national health system in Thailand. A range of RH

40

mfc bulletin/Aug 2010-Jan 2011

services are provided by the sub district health centres, district, provincial hospitals, as well as private health care providers. Thailand’s population policy dates back to 1970 and the country has achieved a below replacement fertility rate. The current population policy focuses more on meeting basic reproductive health needs and well being of individuals. Thailand also has a Reproductive Health Policy (1997) which states that all Thai citizens, of all ages must have good reproductive health throughout their lives. This is to be achieved through ‘improving accessibility, equity, rights and choices in reproductive health services.’ (Tangcharoesathien et al 2002) The sexual and reproductive health services are divided into two categories: preventive and curative. Preventive care and promotive care includes: sex education and adolescent reproductive health care, family planning. Curative services include emergency obstetric services, treatment of reproductive tract infections and cancer, limited number of abortions permitted under the law, treatment of complications of unsafe abortion, infertility, and menopause services. Reproductive health promotion and sex education for adolescents is implemented through health facilities, schools, factories using public media. Thailand has shown remarkable results in HIVAIDS education, safe sex campaigns and condom promotion. Family planning has also been successful with Contraceptive Prevalence Rate increasing to 79.2% in 2000 (Tangcharoesathien et. al. 2002) and Total Fertility Rate coming down to 1.9. Nurses and midwives at sub district centres and district hospitals are the backbone of maternal health services. In 2001 Thailand’s maternal mortality was 28 per 100000 live births, the standard of four ante natal visits for all pregnant women was achieved for 92.9% women and 97.9% of all

deliveries were by trained health personnel (Tangcharoesathien 2002) Infertility services have not been a priority with the public health system because of policy makers’ belief that infertility is a problem of the rich and so should be self financed and be left to the private market. Publicly funded menopause services e.g. Hormone Replacement Therapy – were limited to the urban areas. Coverage of breast screening was 20%. Although the national guidelines laid down Pap Smear screening annually for three consecutive years for all women over 35 years, and then once every three years if all was normal, the coverage was less than 40%. Before Universal Coverage, under the PWS and the VHC scheme there were no restrictions on the number of deliveries women were entitled to have covered. Under Universal Coverage however two deliveries can be covered. Women having more than two children cannot access covered emergency obstetric care. Poor women can end up paying out of the pocket for a third and higher order deliveries. Table 6 shows the summary of the reproductive health services packages before and after the Universal Coverage. The authors argue that awareness of entitlements and the benefit package plays a key role in utilization of services. Preventive services like cancer screening, sex education and premarital counselling need even more educational efforts. Patients neither express demand nor adequately utilize these services. Such services cover, for example, screening for cervical and breast cancer; sex education; condom promotion; family planning and prevention of unplanned pregnancies; premarital counselling and voluntary HIV testing. These services have positive societal externalities

Table 6: Summary of Reproductive Health Services Packages before and after Introduction of Universal Coverage Components of Reproductive Health Service

CSMBS

SSS

PWS

VHC CSMBS

SSS

UC

Sex education and adolescent reproductive health

Poorly defined

Poorly defined

Poorly defined

Poorly defined

Poorly defined

Poorly defined

√

√ √ √ √ √

X

√ √ √ X Partial

√ √ √ √ √

X

√ √ X X

√ √ √ X Partial

√ √ X X

√ √ √ X

√ Partial

Partial X

Partial X

Partial X

√ Partial

Partial X

Family planning Essential obstetric care Abortion and abortion complications Menopause services Reproductive tract infections including HIV/AIDS Reproductive tract cancers Infertility (Source: Tangcharoensathien V. et al, 2002)

Before Reform

After Reform

√ √ X

mfc bulletin/Aug 2010-Jan 2011 for the control of diseases, but are not realized and expressed as effective demand by patients. Such interventions tend to be under-utilized, even where they are cost effective and have positive externalities. Policymakers must stimulate consumption of these services by providing information and education, as well as creating incentives to providers. Fees for services would send a stronger signal to service providers than capitation. A bonus payment on the achievement of a target for cervical cancer screening, for example, would be an interesting payment choice. (Source: Teerawattananon Y. and Tangcharoensathien V., 2004)

It is in the interest of the health providers under capitation with long term contracts, to reach out preventive services because then they will be able to reduce the curative costs in the long run. Competition will enhance the quality of service provision. Also monitoring by purchasers will maintain quality standards. Thus, regulation assumes great importance. Systems for problem solving, user complaint redressals have to be well developed and mentioned in the contracts. Table 7 summarizes the arguments. These mechanisms will empower users to choose their providers and increase responsiveness of providers to legitimate expectations thereby changing power relations between users and providers. Teerawattananon and Tangcharoensathien argue that

41 SRH services not offered in the original package (e.g. treatment of HIV infection, abortion services) should be considered and additional resources made available. The authors cite data which states that abortions account for the largest share (36%) of the maternal burden. Most problems are attributed to infertility following unsafe septic abortion. Based on the National Health Examination Survey 1999, the Working Group estimates a total of 0.3 million abortions are carried out per year. 74% of induced abortions are conducted outside hospitals, and 71% of these are provid ed by a non-health professional, often by introducing a hypotonic or hypertonic salt solution. This is due to the fact that abortion, except in rape cases and where pregnancy endangers the mother’s health, is still illegal. Health needs exist but are neither expressed by consumers, for their own reasons, nor are they offered in the package. Services in this group include the programme for prevention of sexual and gender-related violence by increasing consumer awareness to stimulate demand. There is no significant change in the UC package, or, in particular, in the SRH package. The scope of the SRH package was adopted from the previous schemes unchanged, but the per capita budget allocation for the UC is much higher than that of the previous public welfare scheme (Source: Teerawattananon Y. and Tangcharoensathien V., 2004)

Table 7: Conceptual Framework for Analysis of Impact of UC on Utilization and Delivery of Reproductive Health Service Preventive Services And Health Curative Services Promotion  Positive effect on utilization of services Demand Side  No effect on utilization among unaware consumers. with high consumer awareness and felt  Positive effect on utilization need. among highly aware consumers.  Positive effect on utilization when financial barriers are abolished, especially among the previously uninsured.  Positive effect on prevention  Positive effect on service delivery and Supply Side of health problems if response to legitimate expectations if providers have long-term capitation is adequate and fee schedule contract to minimize curative for high-cost care is attractive. expenditure.  Negative effect on provision of adequate  No effect on prevention of health services, and poor response to legitimate problems without long-term expectations, especially by errant contract. providers. Purchaser

 Positive effect on utilization, responsiveness and prevention of health problems if purchaser has good capacity to monitor contract and vice versa.

(Source: Tangcharoensathien V. et al., 2002)

 Positive effect on utilization and responsiveness if purchaser has good capacity to handle complaints and grievances and vice versa.

42

5. Lessons for India i.

It is possible for low to middle income countries to institute Universal Health Coverage funded by general tax and social health insurance and completely prevent out of pocket payments at the point of service delivery by families. Raising additional general tax revenue has been shown to be possible and successful in a number of developing countries, like Bolivia, Armenia, Estonia, and Slovakia (Wagstaff, 2007). ii. Three facilitating factors are required for successful health care reform: political commitment, strong public support and sense of entitlement, ownership of the agenda by Ministry of Health Leaders, policy researchers and health advocates. iii. It is imperative that efforts are institutionalized to increase awareness of entitlements among users as well as their capacities to monitor services provided by contractors. iv. An important pre requisite is investment in health infrastructure and health human resources, as well as a human resource policy to enable retention of staff in rural areas. v. It is possible to harmonise existing schemes – for India CGHS, ESI, RSBY, etc - to ensure equity across the schemes and universal coverage. vi. A robust primary health system (with contracting out and provider payment) reforms can play a critical role in achieving both equity and efficiency. Health services closer to people will lower overall costs and acceptability of services. Primary care model with an emphasis on prevention and health promotion will over time, reduce the burden on curative services. vii. A contract model with the District Health System will enable primary health care, rational use of integrated services and ensure proper referrals. A Provider- Purchaser split is desirable – in Thailand’s case the National Health Security Organization is the purchaser and the District Health System is the Provider. viii. Universal Coverage can provide a comprehensive package of curative services and preventive and promotive services. ix. Lessons from various Health Insurance schemes can be used to design the Universal Coverage Scheme. For example, Voluntary Health Insurance can lead to adverse selection because only the sick will join the scheme and the healthy will opt out. It is difficult to achieve universal coverage by a contributory scheme especially among the informal sector because of their livelihood insecurity. Also administration and premium collection costs will

mfc bulletin/Aug 2010-Jan 2011 be high. x. There is a need to build institutional partnerships between the statisticians’ constituency who generate health data and the health constituency who use information for policy making, equity monitoring, and to insert social determinants in national and sub-national health surveys. xi. Concentration Index (CI) shows that general tax revenue is more progressive than Social Health Insurance contribution. The tax financed scheme is sustainable in the long run. It links providers through a contracting model with close ended provider payment methods – capitation for out patient services and global budget with application of DRG for inpatient services. The capitation contract model is an effective means of cost containment and to support adequate quality of care, with assured monitoring. The fee for service payment mechanism can lead to cost escalations and inefficiency. xii. We have a lot to learn from Thailand about reaching out reproductive and sexual health services. A strong primary health care system, a well trained cadre of midwives and skilled birth attendants, emergency obstetric care, safe abortion services, adolescent sex education, quality contraceptive services, anaemia and malnutrition management programme, and other women’s health services including cervical and breast cancer management programmes, need to be part of the essential Universal Health Coverage package. Gradually other issues like infertility services, health services for violence against women, mental health services can be brought into the package.

References 1.

2.

3. 4.

5. 6.

7.

Teerawattananon Y. and Tangcharoensathien V. (2004), Designing a reproductive health services package in the universal health insurance scheme in Thailand: match and mismatch of need, demand and supply, Health and Planning 19 (Suppl.1), Oxford University Press Damrongplasit K. (2009), Presentation on Thailand’s Universal Coverage System and Preliminary Evaluation of its Success, UCLA and RAND Thailand’s Human Development Report 2007 Patcharanarumol W. (2008), Presentation on Universal Health Care Coverage: Thailand Experience, International Health Policy Program (IHPP), Ministry of Public Health-Thailand National Health Commission Office, Thailand, 2008 Tangcharoensathien V. et al (2002), Universal Coverage and Its Impact on Reproductive Health Services in Thailand, Reproductive Health Matters Sakunphanit T., Universal Health Care Coverage Through Pluralistic Approaches: Experiences from Thailand, International Labour Organization

mfc bulletin/Aug 2010-Jan 2011

43

Understanding the Canadian, Thai and Brazilian Universal Healthcare Systems: A Focus on Regulation and Lessons for India - Prepared by Kerry Scott, intern at SATHI, Pune1

Introduction: Regulation in Healthcare Systems This paper examines how the Canadian, Thai and Brazilian healthcare systems are regulated. The case studies are presented separately. For each country I first explain how the health system is designed and implemented. Second, I discuss how the system is regulated, focusing on the following components: the regulatory bodies; the system of paying workers; cost containment measures; efforts to maintain high quality, rational healthcare; and efforts to maintain an appropriate quantity and diffusion of healthcare across the country. I finish each section with a brief overview of potential lessons that India can draw from the healthcare system of the case study country, suggesting elements of the system that may work well in India and components. The Canadian case study focuses on the division between national and state (or in Canada’s case federal and provincial) power, since this division is the component of the Canadian system that is of greatest interest to India and efforts to bring healthcare to remote areas. The Thai case study examines on how the government worked out a system of universal coverage from a patchwork of preexisting schemes. India’s RSBY scheme and other government subsidized or funded health insurance schemes share great similarities to the Thai system prior to Universal Coverage in 2001. Brazil’s country profile aligns with India’s in several important ways that make its transition to universal access particularly relevant to India. Both countries have large populations and have seen rapid economic growth in the past decades but are still characterized by gross inequalities between the rich and the poor. Understanding how Brazil is making its SUS system of universal healthcare work offers several salient lessons to India.

1. Healthcare in Canada Basic Outline Most healthcare in Canada, commonly called Medicare, is publically funded through universal single-payer public health insurance but is provided both privately and publically by hospitals and physicians operating as for-profit or not-for profit healthcare provision units. Canada’s healthcare system is guided by the Canada Health Act of 1984 and administered separately by each 1 Report presented at Medico Friend Circle meeting, July 1011, 2010, Wardha, Maharashtra, India

of the 10 provinces and 3 territories. The Act states that all “medically necessary” hospital and physician care must be universally covered without co-payments in order for provinces and territories to receive federal fund transfers (Canada, 2010). It does not cover nonemergency dental care or prescription drugs. The Act does not say exactly how medical care should be organized across the country only that it must meet certain criteria (discussed below) in order to receive federal funding. It leaves the management of health systems to each province or territory. The Canada Health Act does not directly bar private delivery or private insurance for publicly insured services and each province has developed different laws surrounding private healthcare with most explicitly banning it. The primary objective of the Act is “to protect, promote and restore the physical and mental well-being of residents of Canada and to facilitate reasonable access to health services without financial or other barriers” (Canada, 2010, Secton 3, p. 8). It lays out the following criteria and conditions for funds transfers: (a) Public Administration All provincial and territorial health insurance plans must be “administered and operated on a non-profit basis by a public authority, responsible to the provincial/ territorial governments and subject to audits of their accounts and financial transactions” (Section 8). This condition does not deal with delivery, but with insurance. So insurance companies cannot profit from covering ‘medically necessary’ care but healthcare workers and organizations can profit. Private insurance companies can still profit from covering non-insured services, and/ or non-insured persons. (b) Comprehensiveness The health care insurance plans must cover “all insured health services provided by hospitals, medical practitioners or dentists” (Section 9). Generally, the insurance must only cover services in hospitals or through physicians. Hence emergency dental care but not any other dental care is covered. The provinces are allowed, but not required, to insure. (c) Universality This clause states that all insured persons must be covered for all the health services “provided for by the plan on uniform terms and conditions” (Section 10). Health insurance coverage is extended to all Canadian residents including landed immigrants although there is

44 a waiting period (three to five months) before coverage begins for new immigrants and Canadians who have lived abroad. (d) Portability This clause seeks to insure Canadians moving between provinces/territories are covered. Short trips to other provinces (three months or less) are covered by their home province. For people relocated for longer period, the new province imposes a waiting period (three months or less) during which the old province covers them before taking over coverage. (e) Accessibility The insurance plan must provide for “reasonable access” to insured services by insured persons. This means access cannot be limited by user charges or extra billing, cannot be limited by age, health status or financial circumstances. If a province imposes user charges or extra-billing the federal government can (and has) withhold the exact same amount from its next financial contribution to that province. For example, in 1993, British Columbia allowed approximately 40 medical practitioners to use extrabilling in their practices. In response, the federal government reduced B.C.’s payments by a total of $2,025,000 over the course of four years. In 1996, Alberta had their payment reduced by a total of $3,585,000 over the course of a few years due to the use of private clinics that charged user fees. Newfoundland suffered the loss of $323,000 until 1998 and Manitoba lost a total of $2,056,000 until 1999 from user fees being charged at private clinics. Nova Scotia has also forgone payment for their use of user fees in private clinics. Non-Hospital/Physician Services Prescription drugs, dental care, vision care, medical equipment and appliances (prostheses, wheelchairs, etc.) and the services of allied health professionals such as podiatrists and chiropractors need not be provincially covered for federal transfers under the Canada Health Act. However, the provinces and territories provide coverage to certain groups of people (e.g., seniors, children and social assistance recipients) for these services. Others must find private group insurance, usually through their workplace or as a dependent on someone with workplace insurance. Private Insurance/Payment for Medically Necessary Healthcare? The single criterion for accessing health care in Canada has been medical necessity. However, recent changes suggest a move towards a second criterion: financial capacity. The use of private insurance or out-of-pocket

mfc bulletin/Aug 2010-Jan 2011 expenditure to pay for health services covered under public insurance are banned by various provincial level laws in most provinces. However the 2005 ruling on the case Chaoulli v. Quebec may create space for private health insurance for medically necessary hospital and physician services. The Supreme Court of Canada found that Quebec’s prohibition on purchasing private insurance for government-insured physician and hospital services contravenes Quebec’s legislative act, the 1975 Charter of Human Rights and Freedoms (Dhalla, 2007). Quebec now allows citizens to purchase private insurance to obtain care for three procedures (cataract surgery and knee and hip replacement) at “a specialized medical centre where only physicians not participating in the health insurance plan” practice (Dhalla, 2007, p. 89). Alberta and British Columbia have also frequently considered allowing private insurance but the issue remains controversial. In general, provinces require physicians to fully opt in or opt out of the public insurance system (Hurley & Guindon, 2008). If a physician chooses to open a cataract surgery clinic, for example, and accept private insurance or out of pocket payments, he or she cannot also perform cataract surgery on other patients and bill the Quebec public insurance plan. Likewise, any physician operating outside the public system to provide services not included in public insurance covers (i.e., ‘medically unnecessary’ services such as laser eye surgery) must support the entire practice on money earned through private funding. Most provinces also regulate the amount physicians who have opted out can charge (Flood & Archibald, 2001). There are extensive differences between provinces: • Manitoba, Ontario and Nova Scotia prohibit optedout physicians from charging private fees greater than the fees paid by the public plan. • Other provinces permit opted-out physicians to charge fees higher than those in the public plan, however, all but Newfoundland and Prince Edward Island prohibit such patients from receiving any public subsidy. • Newfoundland is the only province that currently allows private health insurance coverage for publicly insured physician and hospital services, allows opted-out physicians to charge more than the public fee, and allows patients to receive public coverage for a service even when the fees charged are higher than those of the public plan. (Hurley & Guindon, 2008) Very few physicians have chosen to opt out of the public insurance system because so few people are inclined or able to pay out of pocket or buy private insurance for

mfc bulletin/Aug 2010-Jan 2011 services that are already insured through Medicare. Health Canada (2007) estimates that no physicians have opted out in seven of Canada’s 10 provinces, while six are opted out in British Columbia, 129 in Ontario, and 97 in Quebec. Regulation within the Canadian Health System In order to document compliance with the Canada Health Act (thus determining federal money transfers) the federal Minister of Health annually reports to the Canadian Parliament on how the act has been administered by each province over the course of the previous fiscal year. For non-compliance with the any of the five criteria listed above, the federal government may withhold all or a part of the transfer payment with “regard to the gravity of the default” (Section 15). Thus far all non-compliance issues have been settled through discussion or negotiation. Only the provinces can pass laws on the creation and administration of hospitals and the provinces handle health insurance regulation, the distribution of prescription drugs, and the training, licensing and terms of employment of practitioners (Makarenko, 2008). Each province thus has a college of physicians and surgeons that provides professional regulation, develops patient safety guidelines, registers all physicians, deals with complaints, and publishes ethical and legal guidelines. Over 95% of Canadian hospitals are operated as private non-profit entities run by community boards of trustees, voluntary organizations or provincial health authorities (Association of International Physicians and Surgeons of Ontario, 2004). Public hospitals are regulated by provincial regulatory bodies, usually the Ministry of Health and Long-term Care, under the Public Hospitals Act. The hospitals’ Boards of Directors report to the provincial Ministries of Health on how they have used their provincial subsidy grants, which in turn report to the federal Ministry of Health. Physicians working in hospitals and clinics are regulated by their provincial college of physicians and surgeons. Private health centres offering services beyond what are covered by Medicare (cosmetic surgery, laser eye vision correction surgery, executive health clinics that offer enhances physical examinations and wellness counselling) or performing surgeries paid for by private insurance in Quebec (see Private insurance for medically necessary healthcare?) have been flagged as lacking appropriate regulation (Lett, 2008). In some provinces, even private medical clinics that are integrated into the delivery of insured medical services lack regulation (ibid). Other provinces have regulatory acts, such as Nova Scotia’s Health Facilities Licensing Act, and

45 Ontario’s Private Hospitals Act 1973 working with the provincial college of physicians and surgeons (Association of International Physicians and Surgeons of Ontario, 2004). Efforts are currently underway to bring about regulation throughout the country. How Does the System Pay Healthcare Workers? Since each province handles administration differently, the method of healthcare worker payment varies. However, generally, for out-patient care, physicians bill the government insurance organization on a fee-forservice basis. Patients are occasionally contacted by the government insurance organization to check that they actually received the service the physician billed for. Physicians at clinics in remote areas receive financial bonuses from the government. For in-patient care at hospitals, healthcare workers have a flat salary, which depends on the healthcare worker’s experience, amount of responsibility, additional tasks (administration work, teaching medical students, etc.), as well as the hospital’s location (remote hospitals offer higher salaries to attract workers) and tier (tertiary/teaching hospitals are the most elite with the best equipment and the most complex medical cases). How are Costs Contained? By international standards, Canadian spends an aboveaverage amount on health care. Medicare was 6.9% of Canada’s GDP in 2005 placing it 9th highest in the world (Hurley & Guindon, 2008). Nonetheless there are many provincial level efforts to contain costs. Physicians and their clinics are audited to insure that they are not overbilling (i.e., charging for things they did not do or charging for unnecessary procedures or tests). These audits are extensive and serve to discourage overtreatment, over-medicalization, over-testing and cheating. Hospital costs are contained by maintaining a minimum standard of non-medical care, and allowing supplementary charges or supplementary insurance to upgrade non-medical elements of hospital care (i.e., people can pay or use private insurance for ‘perks’ such as shifting into a private room). Defensive medicine to avoid lawsuits is not a major issue in Canada. Physicans in private practice (i.e., most physicians) buy medical liability insurance, with the government reimbursing them for about 80% of their insurance premium payments. Medical malpractice suits are less common in Canada than in the US and liability insurance fees are lower for a number of reasons. A major reason is that Canada’s highest courts have set limits on awards. Also the country’s liability laws make establishing professional negligence quite difficult (Clarke, 2009). Finally, by making healthcare free and accessible to

46 citizens, chronic illnesses such as hypertension and diabetes are addressed earlier than in countries without coverage (i.e., the USA), where both the uninsured and those with low private coverage and high deductibles or co-payments delay care because of cost. Health planning including public health efforts to reduce the disease burden in the population (such as antismoking, sexual health and healthy lifestyle campaigns) can stop expensive and long-term health conditions from occurring, or facilitate early testing and thus more effective and less expensive treatment. The Canadian government has a financial interest in preventing illness and thus supports significant health promotion research and initiatives (for example high tobacco taxation and innovative needle exchange programs to reduce disease among intravenous drug users). Containing the Cost of Drugs In 2007, Canadians spent an average of $578 CND per capita on prescription drugs ($548 USD = Rs.25 615) (Therapeutics Initiative, 2008). Public insurance plans in Canada pay for about 44% of total prescription-drug spending (because of coverage for the elderly, those on social assistance and the disabled); private plans cover 34%, and patients pay the remaining 22% out of pocket. The Patented Medicine Prices Review Board (PMPRB) is a government review board that seeks to prevent ‘excessive’ increases in patent-drug prices that might result from manufacturers’rights to market exclusivity. The PMPRB can force price reduction based on the following criteria: • For most new patented drugs, the cost of therapy may not exceed the highest cost of therapy with existing drugs used to treat the same disease in Canada. • For breakthrough drugs and those that offer a substantial improvement, manufacturers can charge the median of the prices charged for the same drug in countries listed in the PMPRB’s “Regulations.” • Price increases after a product launch cannot exceed the rate of increase in Canada’s Consumer Price Index. • The price can never exceed the highest price for the drug in the countries listed in the “Regulations” (Mulligan, 2003). PMPRB regulation has consistently kept prescription drug in Canada lower than the international average. The provinces set the cost of generic drugs as a percentage of the brand name drug; the Ontario government recently reduced the cost of generics from 50% of the price of brand name drugs to 25% (CBC, 2010).

mfc bulletin/Aug 2010-Jan 2011 How is Quality and Rationality of Care Maintained? Provincial licensing bodies deal with malpractice issues and complaints. These bodies also handle discipline, which can range from suspensions to losses of the privilege to continue practicing medicine. The cost containment measures discussed above also encourage rational care, since physicians are generally not able to bill for unnecessary procedures. Also, the low rate of medical malpractice suits reduces physician likelihood to practice defensive medicine. The market structure of the Canadian Medicare system encourages physicians to provide the care that patients want. Physicians practice as private units and patients chose their provider so it is in the physician’s interest to provide attractive service (friendly, diligent, clear, appealing to immigrant populations, etc.). However, there is a shortage of doctors in Canada so many Canadians take whichever doctor they can find who is accepting patients. In addition, rural areas often end up having very little choice over their doctor. How is Quantity and Diffusion of Care Regulated? Institutions Equal access is a central component of the Canada Health Act and the federal government’s Office of Rural Health focuses on addressing issues of rural health and access to services. Most provinces have put in place programs to bring people to medical care services, or medical care to communities. Examples include the Underserviced Area Program and the Northern Health Travel Grant Program in Ontario. However, rural people in Canada still have to travel long distances to reach hospitals. Rural areas have helicopter emergency medical services (HEMS) that have consistently demonstrated that air transport to tertiary trauma care is lifesaving and cost effective . Healthcare Providers The government of Canada has implemented many strategies to attract physicians to rural practice. They have created a rural medical school (Northern Ontario Medical School) and a rural medical campus (Prince George Campus of the University of British Columbia) to train physicians who are themselves from rural areas. There are several scholarships and loan remission programs for rural medical students and new doctors who chose to practice in rural areas. Medical schools have altered admission policies to select more students from rural areas, increased generalist education, and developed rotations in rural settings. The Society of Rural Physicians of Canada has a taskforce that examines government efforts and suggests additional programs. Nurse-practitioners are registered nurses who undergo extensive additional training to function in many

mfc bulletin/Aug 2010-Jan 2011

47

important ways as physicians, including being able to prescribe drugs. Nurse-practitioners have been flagged as another potential solution to increase rural access to healthcare. Representatives from rural communities have also developed recruitment strategies, including frequent participation at conferences and recruitment fairs that are held for medical students and physicians. Many offer financial incentives, namely subsidized overhead costs, debt repayment, and financial grants, and promote other benefits of their communities, such as recreational opportunities (Jutzi, Vogt, Drever, & Nisker, 2009). Problems with Quantity and Diffusion Although 20% of Canadians live in rural communities, only 10% of the country’s family physicians practise in these areas (Jutzi, Vogt, Drever, & Nisker, 2009). Continued efforts are required to increase the number of rural physicians, including recruiting more medical school students from rural areas, offering greater financial incentives for practicing in rural areas, help rural physicians access specialist insight though better referrals systems and telemedicine systems, and implement comprehensive plans to enable physicians time off. Lessons from Canada’s Medicare System for India • India’s system of government, with a diverse range of state infrastructure, resources, histories, cultures, ethnic and financial capacity, would lend well to a public medical care administrator system similar to Canada’s. Creating an overarching Act controlling the transfer of national level funding, but administered and organized according to each state’s needs could suit the Indian situation. • The measures used in Canada to control medical malpractice lawsuits pay outs would certainly benefit India’s urban medical system which is seeing a rise of malpractice suits. • Leaving individuals to choose their physician, and leaving medical practices as private units where the charges are billed to the government insurance rather than paid out of practice, would likely find less resistance than attempting to convert all private medical practices into public government-run facilities. It is closer to the current state of affairs in India.

scheme because of a now-defunct co-payment system. This insurance program closed the gap on a patchwork of previous schemes and currently operates in conjunction with two other government healthcare insurance schemes: the Civil Servant Medical Benefit Scheme (CSMBS) and the Social Security Scheme (SSS). Members are enrolled in UC for free through presenting their home registration papers and gain access to a basic package of services through public or private health centres, emergency healthcare and a standard list of drugs. Registration is only possible in one’s region of permanent residence. Treatment outside the healthcare network in which one is registered is limited to emergency care

2. Healthcare in Thailand

Figure 1 shows the percent of the Thai population covered by each insurance scheme the year before and several years after the introduction of the UC scheme. The percent of uninsured people went from 20.3% in 2000 to 1.3% in 2004. UC is funded from the general tax system and is non-contributory. It provides coverage for treatments within a core benefits package.

Basic Outline In 2001 Thailand introduced the Universal Health Coverage (UC) scheme, sometimes called the 30 baht2 2

1 Thai baht = Rs.1.43

CSMBS is a general tax-funded benefit scheme for government employees, state enterprise employees and their dependents. Its coverage is the most extensive and flexible of Thailand’s three schemes because it provides reimbursement on a fee-for-service basis and can be used for complete coverage at public healthcare centres or significant, but not complete, coverage at private hospitals. SSS is for private sector employees and the self employed. It is financed based on tripartite contributions from the government, employees and employers, each contributing 1.5% of the payroll (Tangcharoensathien, Supachutikul, & Lertiendumrong, 1999). Members of the scheme register with a healthcare provider from a set list of potential providers (of whom about half are public and half private) who are paid a flat rate by the insurance purchasing agency based on the population of SSS holders under their care (i.e., capitation). Prior to 2001, the Thai government offered a public Medical Welfare Scheme (MWS) for the poor, the disabled, the elderly, children under 12, veterans and monks as well as a voluntary health card (VHC) scheme for farmers and other informal (generally rural) workers. While the MWS was free, the VHC scheme required that families purchase a health card for B500 (Rs. 720). VHC was originally a community-based insurance scheme; however by 1994 the government agreed to make a matching B500 contribution thus bringing the scheme into the government-tax funded fold. MWS and VHC were combined and expanded to make the UC scheme, which was introduced in October 2001.

48 Figure 1: Health Insurance Coverage in Thailand Leading up to and after Introduction of UC (%) Scheme (prior to UC, i.e., 2000 Scheme 2004 2001) (after UC) Civil servant medical benefit 12.0 CSMBS 8.0 scheme (CSMBS) Social security scheme (SSS) 9.4 SSS 13.2 Public welfare scheme for the 40.8 poor (PWS) UC 78.5 Voluntary health card scheme 17.5 (VHC) Uninsured 20.3 Uninsured 1.3

Source: (MoPH T. , 2001) and (MoPH, 2004) and (Jongudomsuk, 2008)

From 2001 to 2006 members were required to make a co-payment of B30 (about Rs. 43) per chargeable episode (unless they were elderly, disabled, under 12 or extremely poor). In 2006 the government removed the B30 co-payment, making no out-of-pocket patient expenditure necessary. Under UC healthcare is funded via the National Health Security Office (NHSO), a central purchasing agency. The NHSO pays provider organizations, which take the form of contracted units for primary care (CUPs) and can be public or private. Each CUP is typically comprised of a district hospital and a network of primary health centres and channels NHSO funding to healthcare provider payments. CUPs are given a per year allotment of funds based on the size of population they provide care for (i.e., capitation funding). CUPs serve a range of population sizes, with large city CUPs serving over 145,000 people to small rural CUPs serving a population of fewer than 17,000 (Hughes, Leethongdee, & Osiri, 2010). Since 2006, UC has moved from capitation to feefor-service funding for some prevention and health promotion services. In addition, some private hospitals have special contracts with the government for special disease management programs (Jongudomsuk, 2008). The Public-Private Mix in Thai Healthcare Economic growth in the 1990s led to increasing demand for high quality health care, especially among those in the growing urban middle class (Teerawattananon, Tangcharoensathien, Tantivess, & Mills, 2003). This economic growth, along with the extension of health insurance that could pay for private sector care through CSMBS and SSS and new tax incentives through the Board of Investment, spurred rapid growth in the private healthcare sector (Nittayaramphong & Tangcharoensathien, 1994). The urban areas have far more private healthcare providers than the rural areas. In 1996, among households in Greater Bangkok, 63% of health expenditure was used to purchase care from private clinics and hospitals, while only 21% was spent in public health institutions (Teerawattananon et al., 2003).

mfc bulletin/Aug 2010-Jan 2011 To date, Thai healthcare system remains is a mix of public and private provision. Those insured under CSMBS have a choice of any healthcare provider, public or private and the majority seek private healthcare (Somkotra & Lagrada, 2008). The SSS requires insured people seek care from the providers with whom they are registered. Half of these authorized providers are public and half are private. Under UC, patients are to present at their primary care facility first and seek all referrals and further care from there. While most of these primary care facilities are public (especially in rural areas) the government is increasingly open to contracting out healthcare to independent public and private institutions, rather than to directly manage the entire network of care itself. The supply of medical devices such as MRI, CT and mammography devices is dominated by the private sector, especially smaller private hospitals and standalone centres such as independent X-ray units providing X-rays only (Teerawattananon, Tangcharoensathien, Tantivess, & Mills, 2003). Pharmacies are a mix of public and private. Regulation in the Thai Medical System The Thai Medical Registration Division (MRD) and Food and Drug Administration (FDA) regulate medical institutions, professionals, and drug producers, suppliers and sellers directly through registration, licensing, setting rules, renewal requirements, and standards, and monitoring quality, safety, public information and advertising. The Thai Ministry of Public Health (MoPH) regulates private healthcare institutions through the MRD, in accordance with the Medical Premises Act 1998 which controls the licensing and renewals of private clinics and hospitals. Public hospitals require no registration. However, according to Teerawattananon et al. (2003), MRD records show that almost all requests were approved making renewal basically automatic and thus not a strong form of regulation. All private hospitals and almost all the clinics that enrolled with MRD were able to obtain their licenses and no hospitals or clinics have been temporarily closed or had their licenses revoked since 1995. Professional organizations such as the Medical Council, Dental Council, Nursing Council and Pharmacy Council serve a government sanctioned regulatory function through setting and enforcing rules and standards and examining ethical issues. The Medical Council can make sanctions in the form of official reprimands and probations for mild transgressions and suspension and revocation of licences for severe ones (Teerawattananon, Tangcharoensathien, Tantivess, & Mills, 2003).

mfc bulletin/Aug 2010-Jan 2011 Health-care purchasers such as the Social Security Office (SSO) and Health Insurance Office (HIO) play an indirect regulation role by setting financial incentives and disincentives and choosing payment mechanisms. Consumers and the media can express their discontent with health care services by seeking care elsewhere (‘voting with their feet’) or complaining to the government or professional organizations. The media can assist in the complaints process through publicizing issues. How Does the System Pay Healthcare Workers? Physicians in Thai public and private hospitals are employees of the hospital and are thus paid according to that institution’s budgetary structure (Puenpatom & Rosenman, 2008). Capitation payment comes as a sum per person covered by the institution. Determining the salaries of contracted health personnel is administratively complex, especially for public health facilities. Various models of how to deduct salaries of health personnel through the capitation system created an unstable situation at the beginning and affected performance of district health system in many areas (Jongudomsuk, 2008). How is Cost Contained? For those insured under the UC scheme, primary health facilities are meant to serve as gatekeepers to more expensive care. All referrals are to be made through PHCs to prevent unnecessary treatment-seeking by patients. UC has led to an increased rate of hospital usage by 2.2% per year since 2001. This improved access to healthcare increased the workloads of health personnel at public health facilities substantially and became one of the major causes of internal brain drain during this period (Sirikanokwilai, Aarbansrang, Hanworawongchai, & Thammarangsi, 2003). For those insured under CSMBS, the insured must make a co-payment for any drugs that are not on the essential list and for private rooms in both public and private facilities (Teerawattananon et al. 2003). The capitualation payment structure of both UC and SSS mean that the per capita per year payment provides a price control. In addition, several Royal Colleges have set standard prices for surgeries in private instituitions. However, costs have risen for the UC scheme since inception in 2001. The payment to CUPs per insured member was set at B1202 (about Rs. 1730) in 2001 and has risen year-by-year to B2202 (Rs. 3170) in 2009 (Hughes, Leethongdee, & Osiri, 2010). Containing the Costs of Drugs The price of drugs obtained through public facilities is set by the MoPH standards board while the price in private clinics and hospitals is determined by market

49 forces. The Thai Medical Premise Act 1998 was revised in September 2000 to require that prices of products and services produced by private hospitals be disclosed upon request by consumers. However, Teerawattananon et al. (2003) found that price notification upon request by consumers did not function because vendors provided biased information and, during emergencies, there was not enough time to seek price information. Patient Activism and Expensive Care Patient movements have successfully lobbied the government to add more expensive treatments, including ART and renal replacement therapy (Treerutkuarkul, 2010). The National Health Security Office’s benefit subcommittee for drugs and treatments set a 100,000 baht (Rs. 144,000) per quality-adjusted life year threshold. The cost of haemodialysis is about 400,000 baht per year, four times more than the government calculated that they could afford. However, patients argued that the two other government funded healthcare schemes (CSMBS and SSS) cover renal replacement therapy, UC coverage ought to as well. In 2008, the public pressure resulted in renal replacement therapy being included in the UC. By early 2010, 2% of the budget was going to renal replacement therapy for 8000 patients receiving haemodialysis and 4000 receiving peritoneal dialysis. Dr. Tangcharoensathien, Minister of Public Health, is concerned about this escalating cost, saying: “Without alternatives, renal-replacement therapy, when fully scaled up to target end-stage kidney patients, could consume more than 12% of the Universal Coverage Scheme annual budget, and push it to the verge of financial crisis” (Treerutkuarkul, 2010). The government has launched a prevention program to increase the country’s renal health in hopes of avoiding this situation. How is Quality and Rationality of Care Maintained? Quality of Healthcare Institutions All private healthcare institutions are licensed by the Ministry of Public Health through the Medical Premises Act 1998. Private institutions must renew their premises registration biannually (Teerawattananon et al., 2003, p. 329). Rationality of Healthcare Provision Within the UC system, doctors are paid through capitation financing and thus have no incentive to offer more services than are needed. Uncommon procedures must be granted permission through the central government. However, medical lawsuits against practitioners in private urban hospitals are becoming increasingly common and professional protection insurance is becoming increasingly popular. The avoidance of liability leads some private sector doctors

50 to order unnecessary investigations and treatments. These procedures raise insurance premiums and lead to wasted resources. Conduct of Healthcare Providers The conduct of physicians, dentists, nurses and pharmacists is regulated primarily by the Thai Medical Council, a professional organization. They licence physicians and nurses and demand re-licensing throughout each professional’s career. They also play a regulatory role by responding to complaints made by consumers, other healthcare professionals or raised in the media. Their sub-committee on ethics investigates each complaint and the sub-committee of inquiry penalizes professionals through reprimands, probation, licence suspicion, or licence revocation. Problems with Quality There is some debate about whether quality of care in public facilities has been sacrificed by the cost containment measures taken. Because public pay for physicians is substantially lower than private sector pay, many doctors in public facilities also work privately and give lower priority to their public sector patients (Teerawattananon, Tangcharoensathien, Tantivess, & Mills, 2003). A paper by Mills et al (2000) examined the quality of care offered to patients who were covered by the Social Security Scheme’s capitualation payment system, as opposed to patients with fee-for-service reimbursement (under CSMBS or out-of-pocket payment). Their findings suggest that capitation payment systems create incentives to skimp on care for patients, so that more funds are available for staff bonuses and facility ungrades (to attract patients that pay on a fee-for service basis). They found that public hospitals did not differentiate between patients covered by different schemes. However, private healthcare institutions had commonly developed seperate, parallel and inferior facilities for SSS patients. For instance, SSS patients were often only treated by a generalist while other patients accessed specialist phycisians. SSS wards (in private facilities) tended to have poorer staff to patient ratios and a greater use of nurse aids rather than qualified nursing staff. In addition, SSS patinets were more likely to recieve generic drugs and to recieve fewer drugs. Some private hospitals were also found to try to ‘dump’ higher cost SSS insured patients and discourage them from reregistering using techniques such as by delaying nonemergency surgeries. Overall, Mills et al (2000) cautioned that creating a larger capitualion based insurance scheme (which was in fact introduced the following year as UC) would require careful regulation and would benefit from strong public sector involvement, since public healthcare institutions

mfc bulletin/Aug 2010-Jan 2011 responded more positively to capitualion financing. Public care is generally subject to fewer complaints but is also subject to fewer regulatory controls. While some suggest that public healthcare institutions and providers are more ethical practitioners, since they are not driven by profit (because of capitation rather than fee-forservice payment), Teerawattananon et al. (2003) suggest instead that sub-par practice is equally common to public and private facilities. However, the public facilities serve to a poorer population with a lower sense of entitlement. In fact, public care providers may be less driven to provide good quality care because they cater to a captive population and have a standard payment system determined by the number of people in their catchment area, rather than a demand-driven funding source that requires attracting consumers. Professional organizations have been accused of protecting professional interests, failing to enforce standards and failing to make severe penalizations (Teerawattananon et al. 2003). Kick-back payments between private medical professionals surrounding referrals are said to be common (ibid). The Thai Medical Council has been found to be administratively inadequate to handle regulation, licensing and compliance (ibid). How is Quantity and Diffusion of Care Regulated? Institutions There is no policy controlling where private facilities will be developed and how many private facilities can exist. The MoPH has no authority to refuse to licence a private facility on the basis of over-provision in that locality (Teerawattananon et al 2003). This lack of regulation has facilitated the development of the current situation where the vast majority o f private healthcare facilities are in urban centres. The MoPH seeks to correct this urban bias by their policy that all districts of Thailand must have a district hospital and corresponding primary health facilities. Healthcare Providers The MoPH controls the distribution of healthcare professionals through a system of compulsory service requirements. Medical and dental graduates must work in community hospitals for 3 years, nurses for 4 years and pharmacists for 2 years. Those who do not abide by this rule are fined (Teerawattananon, Tangcharoensathien, Tantivess, & Mills, 2003). Once a healthcare provider completes this service, there are no policies controlling their diffusion in the private sector. However, the government has sought to attract physicians to rural areas through increased recruitment to medical school from rural areas and added incentives in the form of a hardship allowance for rural service,

mfc bulletin/Aug 2010-Jan 2011 accelerated progress through the civil service ranks and additional payment for on-call time, out-of-hours duties and community work away from the hospital (Hughes, Leethongdee, & Osiri, 2010). The Thai Medical Council controls the number of specialist through placing quotas on specialist training. Problems with Quantity and Diffusion A disproportionate proportion of larger hospitals and trained personnel are located in the affluent Central Region, leaving the North East and North under-served (Hughes, Leethongdee, & Osiri, 2010). A special advisor to the Thaksin Government, who headed the team responsible for devising the UC policy implementation plan, put the issue in the following terms. Because the hospitals are unequally spread the personnel are also unequally spread because they go with the hospitals. And so areas like the [Isaan] provinces would have very few doctors and if you ruthlessly apply the 1300 baht per capita [the capitation rate at that time] to every single province these guys – the [Isaan] provinces – would have plenty of money but no personnel. And the burden on doctors in these provinces is enormous because there is plenty of money but no doctors. Areas in the central plains were in a panic because, if 1300 baht is given to them also, the salaries alone would gobble up that amount. Whereas if ruthlessly applied it would mean that the government must be willing to transfer doctors from these areas and plonk them in [the Isaan] provinces (Hughes, Leethongdee, & Osiri, 2010, p. 449). Despite these concerns, the MoPH decided to continue with the capitation payments in hopes that healthcare staff would relocated to adjust for the lack of funding in over-staffed urban centres and over-funding of understaffed rural centres. However, workforce redistribution continues to be a problem. Lessons from the Thai System for India • First and foremost, Thailand proves that lessdeveloped countries can create universal access healthcare systems and can almost completely abolish out-of-pocket spending on healthcare for the poor. • Thailand’s system has been very successful in increasing access to healthcare for the poor and increasing financial protection for catastrophic illnesses. The effectiveness of the Thai system can be attributed in part to the extensive knowledge base from past health systems in Thailand (VHC, SSS, MWS, CSMBS) and from studying other countries’ systems. India can perhaps use lessons learned from the RSBY and other schemes to

51 inform a broader system of universal coverage • It is noteworthy that the 30 baht co-payment was abolished five years intoThailand’s UC system; this offers further support for a healthcare system with no out-of-pocket or up-front payment for health services to encourage the poor to seek care as needed. • Capitation payment offers a strong model of cost containment health financing that drastically reduces the prevalence of irrational care. However, it also creates an incentive for providers to provide minimal, perhaps even deficient, care. Capitation payment has been found in the Thai system to work best when there are strong monitoring and accountability measures and an active public rather than private healthcare system.

3. Healthcare in Brazil Basic Outline The 1988 Brazilian Constitution made access to healthcare a universal right and led to the creation of the Sistema Único de Saúde (SUS, unified health system), the national primary healthcare system. Prior to 1988, only the 60% of Brazilians who worked and paid social security taxes and their dependents had health insurance. Now, everyone in Brazil has the right to use any public health service in the country at no out-of-pocket cost. The Brazilian government also allowed citizens to opt out of the SUS and buy private insurance linked to private health centres, which is regulated by the Agência Nacional de Saúde Suplementar (ANSS, National Supplementary Health Agency). About 20 to 25% of Brazilians chose to opt out and thus do not use the public health service (Kepp, 2008; Abbes, 2008). The remaining 75 to 80% of Brazilians use the free national healthcare through SUS. Out-of-pocket purchasing of health services is also allowed but most Brazilians use the SUS or have a private insurance plan. The principles of SUS are those of universality, equity, public financing, decentralisation, popular participation and integrated service provision (Collins, Araujo, & Barbosa, 2000). The system is considered a ‘public contracting subsystem’ because public expenditure contracts care for citizens to both private (for-profit and not-for-profit) and public healthcare provider units (clinics, hospitals, family healthcare teams). The SUS operates through a three-tier system of devolved government: the Federal level, the states and the municipios (municipal level). The municipios are divided into two categories: lower, less developed municipios have ‘full management of basic care’and higher, more developed municipios have ‘full management of the municipal system.’ Both

52 categories receive monthly public sector health funding transferred from the State and Federal Health Funds into the Municipal Health Funds and also raise money through taxes at the municipal level. Lower municipios have control over the public component of healthcare service provision and some control over the SUS contracted units of the private sector. Higher municipios have complete responsibility for municipal level health services, controlling both the public and private provider organizations and paying for hospital stays, ambulatory services and complex procedures. Lower municipios do not control the SUS funding for this specialised care. Instead the money is directly transferred to the hospitals through the Autorização de Internação Hospitalar (AIH, hospital stay authorization) or to the ambulance service. Services Provided Services through SUS are funded in three ways, two of which are based on per capita funding and one of which is fee for service but the fee is paid to the service provider by the municipality or federal government, not by the patient. First, municipalities receive a set sum per person (in 1997 it was B10/year, i.e., Rs.260, $5.60 USD) to supply the following free, public services: health education, immunisation, nutritional care, consultation with physicians in basic specialities, emergency care, basic dental care, ante-natal care, minor surgeries, family planning activities and home birth attendance bya family physician. Second, the municipalities also receive programmespecific funding for the following five initiatives: • Community health worker programme and Family Health Programme (FHP); • Pharmaceutical provision (basic); • Programme against nutritional deficiencies; • Basic actions of public health control; • Basic actions of epidemiological and environmental control. Third, as mentioned above, there are special additional resources for complex procedures, hospital stays and ambulatory services which are given from the federal, state or municipal government to the service provider on a fee-for-service basis. Special Focus on the Family Health Program The Family Health Program (Programa de Saúde da Família, PSF), created in 1994, is Brazil’s main system of delivering primary health services and used 9% of the Brazilian government’s healthcare budget. Today there are over 28 000 PSF teams, each of which features a general practitioner, a nurse, a dentist, an auxiliary nurse,

mfc bulletin/Aug 2010-Jan 2011 and four health agents, and covers approximately 1000 families (Kepp, 2008). A total of 90 million people, over half of Brazil’s population, receive their healthcare through these teams. The health agents are responsible for regular home visits, with a variety of objectives which include detecting need for health care, especially among children, pregnant women and the elderly, encouraging visits to well-baby clinics, antenatal care, compliance with long-term medicine use, and participation in group meetings (Bertoldi, de Barros, Wagnerd, Ross-Degnand, & Hallal, 2009). PSF is considered one of the main reasons why life expectancy increased from 67 years in 1991 to 72 years in 2007 and infant mortality fell by 60.5% in the same period (Kepp, 2008). The Public-Private Mix in Brazilian Healthcare As mentioned above, 20 to 25% of Brazilians use private insurance and go to separate private hospitals and clinics. These private healthcare facilities are not regulated by the government but are generally of very high quality because they serve the most affluent people and charge more for their services. Within the 75 to 80% of Brazilians availing the SUS free public healthcare system, there is also a mix of publically and privately run institutions. Ashortage of hospital beds has led the government to contract out 65% of hospital care to private hospitals, most of which are underequipped (Kepp, 2008). In some cases, the government builds hospitals (as with the OSSE hospitals in Sao Paulo) and then tenders them for non-profit hospital organizations to run them. In addition to contracting out medical care, the government also tenders family planning and reproductive health services to the private sector (England, R & HSLP Institute team, 2008). Brazil’s SUS allows those who have opted out and purchased private insurance to use the SUS for services that are not covered by their private insurance. In many cases this means that more expensive procedures or treatment regimes (such as ART or haemodialysis) are sought from the SUS, leaving private health insur The Challenge of Providing more Complex Care The SUS has much to celebrate in terms of primary care provision. However, it has faced criticism for its poor capacity to handle health issues beyond primary care. For the 15% or so of the population that require more specialized care than can be offered by the PSF teams, the SUS’s services fall short (Kepp, 2008). While Brazil’s public hospitals are generally well equipped and well run, the private hospitals contracted by the SUS, which, as mentioned above, make up 65% of the SUS hospitals, are notorious for overcrowding and a lack of specialists.

mfc bulletin/Aug 2010-Jan 2011 Regulation in the Brazilian Medical System SUS is regulated by the Lei Orgânica da Saúde (LOS, Health Organization Law), which is actually two laws— Law 8.080 and Law 8.142—both passed in 1990. Responsibility for SUS is shared by the three levels of government: the Ministry of Health and the state and municipal councils, as well as their respective secretariats, which comprise an equal representation of providers and users. Inter-management commissions, Health Councils and Health Conferences contribute to integration, planning, management and accountability. Integration between the different levels of government is carried out through an inter-management commission, composed of authorities from each (Lobato & Burlandy, 2000). Inter-management commissions set the amounts of resources for states and municipalities and establish their management situation. These commissions maintain transparency in financing because all levels of government participate and the representatives of the different governments, in turn, have to account to the Health Councils, which are legally responsible for health policy, at the executive level (Lobato & Burlandy, 2000). Health Councils again operate at the three levels of government: federal, state and municipal. The National Health Council, for example, has a strategic development role and controls national health care policy implementation. It represents government, health care groups and suppliers, while service users take up 50% of Council places. The Health Conferences also operate at the three levels of government. The National Health Conferences, which take place every four years, have played an important role in the creation and development of SUS. They are important events attracting between 4000 and 5000 delegates elected in the municipal and state conferences which together gather more than one hundred thousand participants. The SUS has faced challenges surrounding the division of roles and responsibilities of the federal, state, and municipal governments. The Organization Law 8.142/ 1990 governs the transfer of resources from the federal government, but does not delineate the responsibilities related to the different agencies and levels of government, leading to duplication of some activities and gaps in others. Another major challenge facing the SUS is that its legislation is almost entirely restricted to the public sector. Although the government has the right to inspect and monitor both public and private sectors, neither the 1988 Constitution nor the Health Organization Law include any provision for private sector regulation (Lobato & Burlandy, 2000). Drug Regulation, Cost and Quality All SUS health facilities are required to stock essential

53 drugs. The SUS’s drug provision is effective for HIV, with almost every HIV patient on antiretroviral (ARV) drugs receiving them through the SUS (Kepp, 2008). Brazil has made headlines for their efforts to access ARVs at the lowest possible price, frequently butting heads with international pharmaceutical companies. Brazil has succeeded in accessing affordable ARVs through producing non-patented generic ARVs, procuring any patented ARVs with negotiated price reductions, and recently through issuing a compulsory license to import one patented ARV (Nunn, Fonseca, Bastos, Gruskin, & Salomon, 2007). However, beyond ARVs, the stocking of many essential drugs at outpatient facilities is deficient, with the average availability of drugs found in 2004 to be less than 50% (Karnikowski, Nóbrega, Naves, & Silver, 2004). Thus many Brazilians must buy drugs from one of the country’s 50 000 private pharmacies. The prices of drugs sold on the market is loosely regulated by the government but were found to be significantly more expensive than global averages (Nóbrega, Marques, Araújo, Karnikowski, Naves, & Silver, Retail prices of essential drugs in Brazil: an international comparision, 2007). Pharmacies mark up drugs by about 30% and taxes of about 24% are added to the cost. Data presented to a 2000 ParliamentaryCommission of Inquiry in Brazil revealed that as little as 20% of the wholesale price was actually related to production costs (Comissão Parlamentar de Inquérito, 2000 in Nóbrega et al. 2007). Brazil’s drug quality regulatory body is the National Health Surveillance Secretariat (CNVS). The CNVS handles applications for registrations of drugs, food, cosmetics and other products. The agency receives more registration requests than it can process and has subsequently been unable to meet its responsibilities. For example, in 1994, 800 drugs were re-released under the same name but with different active chemical ingredients without undergoing re-review (Kumaranayake, 1998). How does the System Pay Healthcare Workers? All professionals working in public and private health centres are salaried. Brazil’s social security agency Instituto Nacional deAssistência Médica da Previdência Social (INAMPS, national institute of medical care and social security) sets the price of all health services. This cost is billed through the institution and the healthcare worker’s salary is given out of the billed amount. How is Cost Contained? Efforts to contain costs include focusing on primary and preventative care, with specialized procedures and hospital stays receiving very little funding. Despite having diverse sources of taxation to find the system (individual income, property, goods and services

54 and banking transactions), the cost of universal health care is at risk of outpacing the revenue needed to sustain it. The World Health Organization estimates that health expenditures in Brazil have risen from 6.7 percent of GDP in 1995 to 7.5 percent in 2006. How is Quality and Rationality of Care Maintained? Brazil’s SUS is generally found to be of high quality when pertaining to preventative and primary care (Family Health Program) but of low quality for hospital and emergency care. The Brazilian emergency medical service is called the “Serviço de Atendimento Móvel de Urgência” (Mobile EmergencyAttendance Service). In 2002, the Ministry of Health outlined a document, the “Portaria 2048,” which called upon the entire health care system to improve emergency care in order to address the increasing number of victims of road traffic accidents and violence, as well as the overcrowding of emergency departments resulting from an overwhelmed primary care infrastructure. The document delineates standards of care for staffing, equipment, medications and services appropriate for both pre-hospital and in-hospital. It further explicitly describes the areas of knowledge that an emergency provider should master in order to adequately provide care. However, these recommendations have no enforcement mechanism and, as a result, emergency services in Brazil still lack a consistent standard of care. Rationality of Healthcare Provision The SUS uses a capitation payment system where health centres receive a set amount per population in their care to provide basic services. All employees are salaried. These organizational characteristics both limit incentives for irrational care. Special procedures are funded on a case by case basis and thus are assessed carefully. Among the 25% of Brazilians receiving care outside the SUS, most have private insurance that reimburses care at private institutions. Similar to India, there are major cases of irrationality within the private sector. The rate of caesarean deliveries in the private sector is extremely high (70%) and more than twice that in the public sector (Potter, et al., 2001). Potter et al (2001) concluded that “the large difference in the rates of caesarean sections in women in the public and private sectors is due to more unwanted caesarean sections among private patients rather than to a difference in preferences for delivery. High or rising rates of caesarean sections do not necessarily reflect demand for surgical delivery.” There have been several national efforts to reduce unnecessary caesareans including the 2006 Incentive towards Normal Childbirth Campaign but little change in the caesarean

mfc bulletin/Aug 2010-Jan 2011 rate. Wealthier patients frequently receive caesareans because of medical pressure and/or assumed or real preference. Many poorer women have been found to perceive caesareans to be superior care since wealthier women receive them, thus driving up demand even in the SUS. How is Quantity and Diffusion of Care Regulated? In 1995, the number of physicians per 1000 population by region varied from 0.52 and 0.66 in the poorer regions of the north and the northeast to 1.75 and 2.05 in the states of São Paulo and Rio de Janeiro, in the richer southeast region. The average for the whole country was 1.19. This gap in favour of richer regions is smaller than it was 25 years earlier, thanks to efforts to expand the coverage of the population by public services. But poorer and rural regions continue to suffer from less accessible health services. Dussault and Franceschini (2006) report that Brazil has successfully adopted policies to reverse the trend towards specialization, with positive results in terms of geographical deployment of physicians. The Family Health Program has been central to recruiting more full time health professionals into the public system because it offered a steady job with higher pay than previous public health system positions. This higher salary through the FHP is the main government effort to increase the number of health workers in rural and poorer regions; medical school education also emphasized primary and rural health issues.

Lessons for India from the Brazilian system • The inter-management commissions, health councils and conferences are an important mechanism for arranging transparent financial transfers, setting standards and maintaining accountability in a large country. • An argument to consider is that put forth by Wagstaff (2007). He suggests that allowing the rich to opt out and buy private insurance and access a higher quality tier of healthcare can benefit the entire public system in countries as stratified and unequal as Brazil (or India). He argues that general tax funding of healthcare can lead to the better-off accessing a disproportionate share of benefits from public services. He suggests that forcing the better off into the private sector if they want more sophisticated care, as in Brazil, Sri Lanka and Malaysia, would create a two tier system but is likely to be more equitable than the alternative of having richer groups skew public services in their favour. As Mills (2007 p17) points out, Wagstaff offers a more positive view of the role for private insurance, as a complement or supplement to general tax funding.

mfc bulletin/Aug 2010-Jan 2011

Key Terms Capitation: A form of compensation in which providers receive a periodic fee (usually a per member, per month fee) in return for delivering as many necessaryhealth care services as needed by the members of the population under their care. Co-insurance: An agreement between the insured and the insurance company where payment is shared for all claims covered by the policy. A typical arrangement is 80%/20% up to $5,000. The insurance company pays 80% of the first $5,000 and the insured pays 20%. Usually after 80% of $5,000, the insurance company then pays 100% of covered expenses during the remainder of the calendar year up to any limits of the policy. Co-payment: A small charge paid at the time a medical service is received. It does not accumulate toward a plan’s deductible or out-of-pocket maximum and is designed to discourage utilization. Cost containment: Efforts or activities designed to reduce or slow down the cost increases of medical care services. Cost sharing: The sharing of costs between the payment of premium costs and medical expenses by the health care plan and its insured through employee contributions, deductible, co-insurance and co-payments. Cost shifting: The increased cost of medical care to other patients to make up for losses incurred in providing care to patients who are under-insured or who have no coverage. Cream skimming: The practice byinsurance companies of insuring only those individuals least likely to make high claims (i.e., the young and healthy), thus reducing the risk sharing benefits of insurance. Deductible: The amount that the covered insured must pay before a plan or insurance contract starts to reimburse for eligible expenses. Fee-for-service: When doctors and other health care providers receive a fee for each service such as an office visit, test, procedure, or other health care service. Feefor-service health insurance plans typically allow patients to obtain care from doctors or hospitals of their choosing, but in return for this flexibility they may pay higher copayments or deductibles. Patients frequently pay providers directly for services, then submit claims to their insurance company for reimbursement. Fixed costs: Refers to those costs which are payable monthly and which do not relate to actual claims paid or incurred (for example, premium and administration costs). Moral hazard: When the behaviour of the insured party changes in a way that raises costs for the insurer, since

55 the insured party no longer bears the full costs of that behaviour. Moral hazard theory suggests that because individuals no longer bear the cost of medical services, they have an added incentive to ask for pricier and more elaborate medical service—which would otherwise not be necessary. Single-payer: Financing of the costs of delivering universal health care for an entire population through a single insurance pool out of which costs are met. There may be many contributors to the single pool (insured persons, employers, government, etc.). Single-payer health insurance collects all medical fees and then pays for all services through a single government (or government-related) source. In wealthy nations, this kind of publicly-managed health insurance is typically extended to all citizens and legal residents. (Ex Canada’s Medicare, Taiwan’s National Health Insurance, United Kingdom’s National Health Service, Medicare in the US for those over 65 or permanently disabled) Socialized healthcare: Healthcare systems that are owned, operated and financed by the government. All doctors and hospitals work for and draw salaries from the government. Ways to pay for healthcare 1. Direct or out-of-pocket payment 2. General taxation by the state, municipality or county 3. Social health insurance (SHI) 4. Voluntary or private health insurance 5. Donations or community health insurance Works Cited Abbes, C. (2008, October 30). Entrevista com Ministro da Saúde trás balanço dos 20 anos do SUS. Retrieved June 28, 2010, from Rede Humaniza SUS/Brasil in NOTÍCIAS DA PNH SUS: http:/ /www.redehumanizasus.net/ Association of International Physicians and Surgeons of Ontario. (2004). Hospital Structure. Retrieved June 24, 2010, from Information Resource: http://testunix.mediaforce1.com/ imginfo/img/healthcare_system/hospital_structure.htm Bertoldi, A., de Barros, A., Wagnerd, A., Ross-Degnand, D., & Hallal, P. (2009). Medicine access and utilization in a population covered by primary health care in Brazil. Health Policy , 89, 295–302. Canada. (2010, June 4). Canada Health Act, Consolodation: Chapter C-6. Retrieved June 25, 2010, from Minister of Justice: http://laws.justice.gc.ca/PDF/Statute/C/C-6.pdf CBC. (2010, June 29). Quebec riles drug makers with price cut. Retrieved June 29, 2010, from Canadian Broadcasting Corporation News: http://www.cbc.ca/consumer/story/2010/ 06/29/mtl-reaction-quebec-cuts-generic-drugs.html Clarke, S. (2009, June). Medical Malpractice Liability: Canada. Retrieved June 28, 2010, from Law Library of Congress: http:/

56 /www.loc.gov/law/help/med ical-malpractice-liability/ canada.php Collins, C., Araujo, J., & Barbosa, J. (2000). Decentralising the health sector: issues in Brazil. Health Policy , 52, 113–127. Comissão Parlamentar de Inquérito. (2000). Relatório final da CPI dos medicamentos. Brasília: Câmara dos Deputados. Csillag, C. (1995). Brazil tries to control drug regulation. Lancet , 346, 1480. Dhalla, I. (2007). Private Health Insurance: An International Overview and Considerations for Canada. Longwoods Review , 10 (4), 89-96. Dussault, G., & Franceschini, M. (2006). Not enough there, too many here: understanding geographical imbalances in the distribution of the health workforce. Human Resources for Health , 4 (12). England, R & HSLP Institute team. (2008). Provider purchasing and contracting mechanisms. Results for Development Initiative, Sponsored Initiative on the Role of the Private Sector in Health Systems in Developing Countries. The Rockefeller Foundation. Flood, C., & Archibald, T. (2001). The Illegality of Private Health Care in Canada. Canadian Medical Association Journal , 164 (6), 825-830. Gomez, E. (2009, September 2). Brazil’s Public Option. Retrieved Ju ne 3 0, 2 010, fro m Fo reign Policy: h ttp:// w w w. f o r e i g n p o l i c y. c o m / a r t i c l e s / 2 0 0 9 / 0 9 / 0 2 / brazils_public_option Health Canada. (2007). Canada Health Act - Annual Report for 2006-2007. Ottawa: Health Canada. Hughes, D., Leethongdee, S., & Osiri, S. (2010). Using economic levers to change behaviour: The case of Thailand’s universal coverage health care reforms. Social Science & Medicine , 70, 447-454. Hurley, J., & Guindon, G. (2008). Private Health Insurance in Canada. McMaster University, Centre for Health Economics and Policy Analysis. Hamitlon: CHEPA Working Paper Series. Jongudomsuk, P. (2008). How do the poor benefit from the Universal Healthcare Coverage Scheme? No nthabu ri, Thailand: Health Systems Research Institute. Jutzi, L., Vogt, K., Drever, E., & Nisker, J. (2009). Recruiting medical students to rural practice. Canadian Family Physician , o55 (1), 72 - 73.e4. Karnikowski, M., Nóbrega, O., Naves, J., & Silver, L. (2004). Access to essential drugs in 11 Brazilian cities: a communitybased evaluation and action method. J Public Health Policy , 25 (3-4), 288–98. Kepp, M. (2008). Cracks appear in Brazil’s primary health-care programme. The Lancet , 372, 877. Kumaranayake, L. (1998). Effective Regulation of Private Sector Health Service Providers. Department of Public Health and Policy, Health Policy Unit. London: London School of Hygiene and Tropical Medicine. Lett, D. (2008). Private health clinics remain unregulated in most of Canada. CMAJ , 178 (8), 986-987. Lobato, L., & Burlandy, L. (2000). Reshaping health care in Latin America: Chapter 5. Reorganizing the Health Care System in Brazil. Retrieved Ju ne 2 9, 2010 , from Interna tion al Development Research Centre: http://www.idrc.ca/en/ev35305-201-1-DO_TOPIC.html Makarenko, J. (2008, January 30). Canadian Federalism and

mfc bulletin/Aug 2010-Jan 2011 Public Health Care: The Evolution of Federal-Provincial Relations. Retrieved June 24, 2010, from Mapleleafweb: http:/ /www.mapleleafweb.com/features/canadian-federalism-andpublic-health-care-evolution-federal-provincial-relations Mills, A., Benn ett, S., Siriwan aran gsun , P., & Tangcharoensathien, V. (2000). The response of providers to capitation payment: a case-study from Thailand. Health Policy , 51, 163–180. MoPH. (2004). 2004 Universal Coverage Report. Bangkok: National Health Security Office. MoPH, T. (2001). Health insurance. Nonthaburi: Health Systems Research Institute. Mulligan, K. (2003, August 15). How Does Canada Keep a Lid On Prescription Drug Costs? (America n Psych iatric Association) Retrieved June 29, 2010, from Psychiatric News : http://pn.psychiatryonline.org/content/38/16/6.2.full Nittayaramphong, S., & Tangcharoensathien, V. (1994). Thailand: Private health care out of control? Health Policy and Planning , 9 (1), 31-40. Nóbrega, O., Marques, A., Araújo, A., Karnikowski, M., Naves, J., & Silver, L. (2007). Retail prices of essential drugs in Brazil: an international comparision. Pan Am J Public Health , 22 (2), 118–23. Nóbrega, O., Marques, A., Araújo, A., Karnikowski, M., Naves, J., & Silver, L. (2007). Retail prices of essential drugs in Brazil: an international comparision. Pan Am J Public Health , 22 (2), 118–23. Nunn, A., Fonseca, E., Bastos, F., Gruskin, S., & Salomon, J. (2007). Evolution of Antiretroviral Drug Costs in Brazil in the Context of Free and Universal Access to AIDS Treatment. PLOS Medicine , 4 (11), 1804-1817. Potter, et al. (2001). Unwanted caesarean sections among public and private patients in Brazil: prospective study. BMJ , 323, 1155-1158. Puenpatom, R., & Rosenman, R. (2008). Efficiency of Thai provincial public hospitals during the introduction of universal health coverage using capitation. Health Care Manage Sci , 11, 319-338. Sirikanokwilai, N., Aarbansrang, S., Hanworawongchai, P., & Thammarangsi, T. (2003). Factors Affecting Internal Brain Drain in 2001-2002. Bureau of Policy and Strategy; Ministry of Public Health and International Health Policy Program. Somkotra, T., & Lagrada, L. (2008). Payments for health care and its effect on catastrophe and impoverishment: Experience from the transition to Universal Coverage in Thailand. Social Science & Medicine , 67, 2027–2035. Tangcharoensathien, V., Supachutikul, A., & Lertiendumrong, J. (1999). The social security scheme in Thailand: what lessons can be drawn? Social Science & Medicine , 48 (7), 913-923. Teerawattananon, Y., Tangcharoensathien, V., Tantivess, S., & Mills, A. (2003). Health sector regulation in Thailand: recent progress and the future agenda. Health Policy , 63, 323-338. Th erap eutics Initiative. (200 8, November-December). Therapeutics Letter. (U. o. Columbai, Producer, & University of) Retrieved June 29, 2010, from Evidence Based Drug Therapy: http://www.ti.ubc.ca/PDF/72.pdf Treerutkuarkul, A. (2010). Thailand: health care for all, at a price. Bulletin of the World Health Organization (BLT) , 8 (2), 81160. Wagstaff, A. (2007). Social Health Insurance Reexamined. Washington DC: World Bank Policy Research Paper 4111.

mfc bulletin/Aug 2010-Jan 2011

57

Healthcare in China1, 2 - Dhruv Mankad3 During the initial phases of the opening-up of the Chinese economy, the overriding objective was to raise output and incomes. Economic restructuring undermined the health care system, which became increasingly privately financed, though remaining largely publicly-provided. While the population’s health status was improving, a rising number of people were priced out of treatment or fell into poverty because of health care costs. The relative price of health care rose markedly until 2000, pushing up the share of overall health care expenditure in GDP (Gross Domestic Product which in 2008 was around 4½ per cent). Hence, a marked change in the equity and efficiency of the health care system was needed. In recent years, several reforms have been initiated, including a number of major changes that have been launched in 2009, introducing two new health insurance schemes, whose design varies throughout the country, in addition to the two existing systems. Overall, nearly all the population is now covered by medical insurance. China has very successfully reduced deaths from infectious diseases. By the early 1990s, infectious diseases had been almost eliminated. Hygiene, sanitation, primarycare and health education through the barefoot doctors and mass action by the community to follow the leader to improve their own health were the two pillars which almost eliminated the infectious diseases by early 1990s.

The 2009 Health Care Reform Plan In April 2009, after extensive consultation, the government launched a new reform plan for the health system, in accordance with a decision of the State Council. It aims at providing safe, affordable, effective basic care to all citizens by 2020. It comprises both demand and supplymeasures and covers five major areas (Chen, 2009) • It aims to raise health insurance coverage to 90% by 2011 from 80% at end-2008. As from 2010, the government payment to the rural system will rise to CNY4 120 per person from CNY 80.

• A national essential drugs system will be established, with regulated prices and a high reimbursement rate. • Local medical care will be improved to reduce workloads in over-crowded city hospitals, with family doctors and nurses acting as gate-keepers. • Basic public health services will be improved for screening and prevention. • Pilot reforms of public hospitals will be launched aimed at improving their management and correcting the tendency for commercialization. This programme involves extra outlays of CNY 850 billion over 2009-11 – 0.8% of projected GDP. Local authorities are expected to fund 60% thereof. The cost of transfer to the rural health insurance and urban schemes plus the cost of public health provision will amount to about CNY 160 billion annually (0.5% of GDP and 60% of total outlays). The remaining money will be spent on training and infrastructure. New infrastructure will include 2 000 new county-level hospitals so that every county would have a hospital compliant with national standards. As well, 29 000 township hospitals will be built and 5 000 upgraded. In towns, 3 700 additional community health centres will be set up. Doctors from villages and community care centres will be retrained, while city-level hospitals will have to launch training programmes for the county hospitals for which they are responsible (Ye, 2009).

Health Status in China The past few decades have seen a significant improvement in the health status of China’s population. Health outcomes clearly improved in China and continued to do so in recent years. However, while in the late 1970s, the population enjoyed much better health than might be suggested by its income level, this is no longer the case. By 2006, life expectancy had moved back into line with its relative income level (Wagstaff et al., 2009), improving much less than, say, in Indonesia or Malaysia.

1 Excerpts from “Improving China’s Health Care System” - Economics Department Working Papers No. 751, Organisation for Economic Co-operation and Development, by Richard Herd, Yu-Wei Hu And Vincent Koen, 01-Feb-2010. Figures 2 and 3 omitted from the narrative. 2 Some excerpts from “Healthcare in China- Toward greater access, efficiency and quality: by Chee Hew, Senior Research Analyst, The IBM Institute for Business Value,China 2006. Email:< cheehew@cn.ibm.com > 3 Email:<dhrvmankad@gmail.com> 4 CNY = The renminbi or the Chinese yuan (sign: ¥; code: CNY) is the official currency of the People’s Republic of China (PRC), with the exception of Hong Kong and Macau Renminbi means people’s currency. It is issued by the People’s Bank of China, the monetary authority of the PRC.

58 The prevalence of infectious diseases has been markedly reduced and life expectancy has risen – albeit rather slowly compared to other countries. Overall, health outcomes are not so different from those in lower-income OECD countries such as Mexico and Turkey, despite lower incomes in China.

New Health Challenges The country now faces new challenges. Chronic diseases are causing more deaths and infant mortality is unduly high in a number of rural areas. Three sets of diseases are growing rapidly – lung-related illnesses (notably lung cancer), heart-related diseases and diabetes. These three diseases are preventable: the first two are related to high tobacco and salt consumption, and the last one to a growing incidence of obesity. While overall performance has been good, there remain serious regional problems. The poor health outcomes in lower-income areas are documented in OECD (2010).

Challenges to Health Care - Greater Access, Efficiency and Quality There are three main challenges while designing and implementing health reforms in China: • Lack of access to affordable healthcare • Inefficient use of healthcare resources • A lack of high-quality patient care. There is no simple solution to closing the gap identified by the government. Challenging questions need to be answered to fundamentallyimprove healthcare in China: • What changes need to occur in the short term to improve the situation while longer-term challenges are being addressed? • What is the role of the government and other players across the healthcare ecosystem? • How can technology be leveraged to improve the management and delivery of healthcare?

Lack of Access to Affordable Healthcare A study was conducted by IBM School of Business Values based in China in 2009. Simply put, a significant portion of China’s urban and rural population is without access to affordable healthcare. Rural areas are particularly hard hit, with 39 percent of the rural population unable to afford professional medical treatment.5 Furthermore, 30 percent of rural population has not been hospitalized despite having been told they need to be.6 This grim situation is largely attributed to

mfc bulletin/Aug 2010-Jan 2011 the abolishment of farming communes and rural health clinics that were replaced with private medical practices in the 1980s – without any alternatives established to date. The situation is not much better for urban residents, with 36 percent of the population also finding medical treatment prohibitively expensive. Historically, the majority of urban workers received free healthcare coverage through employment by SOEs, the Chinese government or universities. However, in the face of fierce competition, many SOEs have gone out of business. Workers who lose their jobs also lose any insurance coverage and so far, there are no other mechanisms to resolve this issue. Severe problems remain amongst migrant families in urban areas. A nine-city study of migrant children found that vaccination rates were some 10 percentage points lower for migrants than for the nation as a whole (Liang et al., 2008). As a result, the prevalence of measles infection was eight times higher amongst the children of migrants than amongst the registered population in Beijing and Shanghai (Vail, 2009). Malaria, hepatitis, typhoid fever, and respiratory infection were found with a higher incidence among migrants than the local stationary residents in Zhejiang and Guangdong. From limited investigations and reports, the incidence of occupational disease among township enterprise employees was high, at 15.8% in 2002. Rural migrant workers accounted for the majority of workplace deaths in 2003 and about 80% of deaths in the most dangerous industries (mining, construction and dangerous chemicals) were migrant workers (Zheng and Liang, 2005). Finally, migrants’maternal mortality after child birth is 83% higher than for mothers who were registered inhabitants (UNDP, 2008).

Healthcare Expenditures Healthcare expenditures, along with actual government funding, have been increasing steadily over the past 20 years. However, as a percentage of GDP, government health funding has, in fact, been decreasing.7 Even more problematic is the high percentage of the population that is uninsured in China. In 2003, almost 45 percent of the urban population and 79 percent of the rural population had to pay for medical services out ofpocket. As illustrated in Figure 1, the percentage of outof-pocket health expenditures has increased significantly since the 1990s.8

“High Costs Keep Ill Chinese Out of Hospitals.” Xinhua News Agency. November 2004. “China’s Health Sector – why reform is needed.” Rural Health in China: Briefing Notes #3. World Bank.April 2005 7 Ministry of Health. China National Health Account Report. 2004 8 China National Survey on Health Service, 2003. This document is available from the Chinese Ministry of Health website (in Chinese only). www.moh.gov.cn 5 6

mfc bulletin/Aug 2010-Jan 2011

59 Table 1: Distribution of Healthcare Beds and Personnel in Urban and Rural Settings 1980

1990

2000

2003

No. of beds/1000 pop Urban

4.47

4.18

3.49

3.67

Rural

1.48

1.55

1.5

1.5

No. of professionals/ 1000 pop. Urban

8.03

6.59

5.17

4.84

Rural

1.81

2.15

2.41

2.19

Source: China Statistical Yearbook, 2003; China Health Statistics Summary 2005.

Table 2 Utilization of OPD and Its Cost in Different Levels of Health Resources

MoH Hospital

Inefficient Use of Healthcare Resources The second key challenge is that current healthcare resources are often not allocated to and used effectively by the segments of the population that need them most. This imbalance is driven by inefficiencies in the supply and demand of healthcare services.

Supply of Healthcare Services A disproportionate amount of China’s healthcare resources have traditionally been concentrated on larger urban hospitals. This spending disparity is reflected in the number of hospital beds and healthcare personnel in rural and urban areas (Table 1) and is in line with the overall urban emphasis of China’s social security system.

Demand for Healthcare Services The inefficiency in resource utilization is exacerbated by patients who are more likely to use larger hospitals in urban areas. (Table 2)

Lack of High-Quality Patient Care There is widespread acknowledgement among healthcare system stakeholders that the qualityof patient care has been compromised in China. There are three key reasons contributing to the lack of high quality patient care.

Loss of Focus on Patient Care Faced with financial pressures and without clear and strict government guidelines, many hospitals have lost the core competency of providing high-quality clinical care. A closer examination of the income sources of a

Province owned Hospitals City Hospital at County level

Avg OPD/doctor 7.3

Avg cost/Patients (in USD) 28.36

6.2

21.08

4.4

9.32

Source:“China to send modern-day ‘barefoot doctors’to boost rural healthcare.” Agence France-Presse. August 2, 2004

hospital explains why there are economic incentives to over-prescribe drugs or diagnostic services without improving patient health. The typical hospital receives less than 10 percent of its income from the government, with large-scale, ministry hospitals receiving more funding. This means that hospitals have to generate the rest of their income from services and sales of drugs.

Over Prescription of Drugs Over prescription and inappropriate prescription of expensive drugs is a widely acknowledged problem. Although the government has set recommended prices for each drug, there are no strict guidelines in terms of the types and number of drugs to be prescribed for each illness. As drugs account for a significant portion of a hospital’s income, there is a tendency to condone the inappropriate prescription of drugs. In fact, almost 44 percent of a typical hospital’s income is generated through sales of drugs alone.9

Overuse of Medical Equipments Inappropriate use of medical equipment can also be attributed to the competitive pressures faced by hospitals. Many hospitals invest in expensive, high end medical equipment and advertise it extensively to attract new patients. The percentage of medical instruments in 9 Huang, Cary. “Ambitious health system sickened by rising costs.” The Standard. November 2, 2002.

60 all Chinese hospitals has been increasing steadily and there is growing evidence that availability of such equipment has exceeded actual demand. For instance, 30.6 percent of all Chinese hospitals own Computerized Tomography (CT) machines, already higher than in major European cities and the U.S. These and other unnecessary purchases divert resources from potentially more important investments, such as those to improve clinical care.10

Quality of Healthcare Professionals There have been significant improvements in raising the quality of healthcare personnel. However, quality has to be further enhanced to increase the level of patient care. Notably, there are challenges because no uniform definition exists to document the required qualifications of healthcare personnel. In addition, current training and experience of healthcare personnel is relatively weak. The inconsistent and low quality of healthcare workers is a particular issue in rural areas. A 2001 study of 46 counties and 781 village doctors in 9 Western provinces found that 70 percent of village doctors had no more than a high school education, and had received an average of only 20 months of medical training. (Wang, 2003) Not only are there fewer personnel in rural areas, it is very difficult to attract and retain skilled personnel to work in less developed regions of China.

Difficulty in Monitoring Level of Care Difficulty in monitoring the level of quality care within China’s very complex healthcare system also leads to a lack of high-quality patient care. Currently, there is no integrated health policies that apply to all hospitals. Provision and regulation of health service delivery is largely decentralized and managed by a multitude of different stakeholders, including the Ministry of Health, provincial and city governments, military, and even large state enterprises that continue to operate their own hospitals. This decentralization not only creates great variation in terms of quality of care across the healthcare system, it also makes it difficult to consistently monitor the level of care.

Is the Chinese Health System equipped to face the challenges? Not Oriented Toward Preventing Chronic Diseases The Chinese health system, however, is not oriented toward preventing chronic diseases and even treatment is not uniformly good. The trend in medical care worldwide has been to increase care at the primary level and reduce it at the level of hospitals. China’s new reform programme makes a start in this direction with the expansion of urban community health centres. If there 10

Ministry of Health Statistics, 2006

mfc bulletin/Aug 2010-Jan 2011 were enough of these, they could act as a network for primary care and serve as a cheaper method of treating chronic diseases than hospitals. Lack of Credibility for Community Health Centers Currently, community health centres and their counterparts in the countryside lack credibility with the population. Patients prefer to go to hospitals, as the doctors offering primary care have low levels of qualification. Many doctors are reluctant to move to primary care because the salaries are low and there is no long-term career path. The new reform programme aims to retrain a large number of the less-qualified doctors. Working in health centres needs to be more attractive and the government needs to take advantage of the ample supply of new graduates, after appropriate family medicine training. The human resources are available but need to be hired at salaries that reflect training. Furthermore, the new community health services need to integrate the previous maternal health service. Circumventing the Measures to Reduce Drug Cost The new reform aims to cut the cost of pharmaceuticals. A bulk buying programme is proposed for a limited range of essential products to be sold to centres under the condition that they are resold at cost. However, doctors have proved adept at circumventing previous attempts to regulate prescribing practices. The challenge is to change prescribing patterns and the pay systems within hospitals that link pay to prescribing activity. Need to Improve Hospital Operational Management System The Operational management practices of hospitals also need to change. The new reform programme stresses this and suggests that hospitals need to become less commercial. In some respects, hospitals resemble stateowned enterprises (SOEs) before reform. They effectively have a dual-track pricing system, with parts of their output sold at regulated prices that are below cost, while other parts are priced above cost in order to crosssubsidize other activities. Hospitals work on a contractual basis with local governments, receiving an annual subsidy and balancing their budget through fees. Like the SOEs of old, they operate under a soft budget constraint: high deficits result in greater subsidies while profitable hospitals receive no funds. As hospitals are public service units, recruitment is often determined by local government bureaus and salaries do not reflect market differentials, nor do the hospitals operate an accounting system that would accurately determine the cost of different activities. Need to Improve Hospital Level Financial Management System Movement to a more enterprise-oriented management

mfc bulletin/Aug 2010-Jan 2011 and accounting structure is needed. The problems with hospitals acting commercially have not arisen just because they seek to make profits but through their rational reaction to regulated prices. Regulated prices should be gradually abolished and replaced by negotiation between third-party payers and hospitals. The current system in which the hospital is paid on a feefor-service basis needs to be replaced by one that is based on a fee per procedure, independently of the number of diagnoses that are made. Such a reform would require that an efficient accounting system be put in place. Financing Healthcare The IBM study recommends a mixed Health Financing mechanism in China depending on the types of services – public, basic and specialist health care services. It is recommending the latter completely open to the market.

Recent Massive Health Insurance Programmes According to the OECD report, the government has successfully rolled out two massive health insurance programmes in recent years. They increased the share of the population with some form of medical insurance from 10% to 90%. In rural areas, the increase in coverage in a voluntary programme has exceeded expectations. In urban areas, though, there are still some problems. The extension of medical insurance to children and those elderly who are not former employees is welcome. Many cities, especially in western and central regions have wanted to keep costs down and so have not extended coverage to employees without cover, presumably on the ground that the employer should have joined the compulsory, but poorly enforced, social security medical insurance system. However, many of these workers are the poorest in the community. Migrants, be they from rural or urban background, generally cannot benefit from health insurance. This clearly hampers labour mobility and is not an equitable outcome. Merging the Current Health Insurance Programmes While coverage is broadening, there are still four main health insurance programmes with many different reimbursement rules and they are mostly restricted to limited areas. Once near universal coverage is achieved, including of migrants in their place of residence rather than their place of origin, the government ought to merge the different systems and ensure that a greater portion of their funding be shouldered by the central government. As to the financial management of the health schemes, attention needs to be paid to the high cost of collecting individual contributions and to why the schemes consistently run surpluses of the order of 30% of income which are kept in separate bank accounts that cannot be used by the local authority.

61 The New Rural Health Insurance Scheme The new rural health insurance scheme has been a success: the number of consultations at countryside health centres has increased markedly. The improvement to health status will take more time to become evident. In future, though, more consideration ought to be given to the benefit plan that produces the best health results. Relying on medical savings accounts to fund all outpatient illnesses may not be optimal. At the least, outpatient treatment for chronic diseases should be covered by the new insurance system as well as a number of preventive medical checkups and treatments. Catastrophic Illness and Poverty – A Concern Poverty caused by catastrophic illness remains a major concern. Indeed, patients are paid less than half of the theoretical benefits, the benefits decline with the seriousness of the disease (insofar as serious cases are sent to higher-level hospitals with lower reimbursement rates) and truly catastrophic illness (costing above two years of average per capita income) is not covered at all. Much higher average reimbursement rates are needed. At present, in rural areas, the contributions per participant would probably need to be tripled, to CNY 300, in order to stand a reasonable chance of markedly lowering poverty due to catastrophic illness. In addition, too high a proportion of the cost of the scheme falls on the local population. At present, individuals and taxpayers people in a county are responsible for paying 60% of the cost in the central areas and 100% in the eastern areas. Even in the more affluent eastern regions this can pose problems for some rural counties. In the poorer parts of the country, the problems are severe and a tripling of contributions might not be possible. Therefore, a much greater degree of central government involvement in financing will be necessary.

Future View of Healthcare in China The overarching focus for health reforms in China is to provide equitable, affordable, yet high-quality patient care. This vision can only be achieved through a series of changes initiated in parallel by keystakeholders across the healthcare system, with clear strategies and guidance set by the government.

Segmentation of Health Services through Unique Policies and Funding Mechanisms To formulate appropriate strategies and policies, the government will have to segment health services into three major categories: public health, “basic” medical services and special services. The government is expected to set different policies, use different funding mechanisms and allow different levels of competition for these three types of services (see Figure 4; Figures 2

62

mfc bulletin/Aug 2010-Jan 2011 Government’s Central Role to Improve Access to and Quality of Healthcare

and 3 omitted from these extracts.) to meet the needs of the vast majority of Chinese citizens. The government also wants to encourage the growth of private medical insurance to complement coverage of basic medical services. It already recognizes that the sustained development of the Chinese healthcare insurance market will require closer cooperation among various ministries. The government is also encouraging private insurance companies to develop more innovative products, new operational models and new business management techniques to fully position commercial health insurance to play an important role in the Chinese health economy. The growth of the private medical insurance sector will further fuel the growth of special health services, such as laser eye surgery and wellness management. It seems likely that the government will allow this special health services market to be open to competition from both local and foreign service providers. Establishment of “Service Provider Ecosystem” Today’s hospital system is highly inefficient, with both resources and patients concentrated in larger hospitals. This has resulted in a situation where large hospitals are growing rapidly and provide general, as well as specialist services. Meanwhile, smaller community hospitals and health centers are caught in a vicious cycle where the lack of patients and income make it difficult for these service providers to upgrade their medical infrastructure, which, in turn, further reduces their attractiveness to patients.

Figure 5 illustrates how the current three-tier system can be “inverted” by creating a service provider ecosystem. The key concept is to distribute patients across the hospitals, according to the level of services needed. For instance, patients should visit primaryor community hospitals for minor ailments. They should be referred “up” to secondary and tertiary hospitals depending on the seriousness of their conditions..Tertiary hospitals will focus on treating difficult cases. As patients recover and require only monitoring health services, they are referred “down” to recuperate or receive rehabilitative services in community settings. Actions for Government Figure 6 outlines the three-stage approach that IBM study envisions the government can take to help bring China’s healthcare system to international levels by 2010. It suggests that the near term focus, as outlined in Stage 1, is to first resolve the major challenges in the healthcare system. Design and Implement “Universal” Health Insurance System One of the major initiatives is to design and implement a health insurance system that meets the needs of the

mfc bulletin/Aug 2010-Jan 2011

Chinese population, yet is affordable. This is no easy task – the government has to consider a multitude of factors, such as how to: • Raise the funds for medical care • Distribute this financial burden fairly • Appropriately limit the scope of services • Obtain high-quality, consistent results. Establish Nationwide Health Network Another action that the government can embark on now is to facilitate information sharing by building a common platform linking service providers in a nationwide health network. A key theme of the effective healthcare system of the future is sharing information and integrating across the health system. A reliable, security-rich, nationwide network can form the backbone of a national health information infrastructure and facilitate information sharing among patients, service providers, regulatory agencies, health professionals, government and payers. This can allow automated quality and compliance reporting, and also create lower-cost capabilities for collecting, aggregating, analyzing and reporting clinical information in near real time. The network can be used as public health services, tracking of long-term health problems, and better analysis and understanding of the medical costs of diseases. The IBM study recognizes the challenges of the lack of standards, and difficulty in gathering and sharing information among service providers. The health network can start with capturing basic demographic and health status information about citizens, and gradually expand its usage in the future. Restructure Delivery and Management of Health Services In addition, the government can consider restructuring the management and delivery of healthcare services. Take the example of how Hong Kong steadily improved

63

the quality and efficiency of healthcare by separating hospital management from policy development.

Conclusion Major changes may occur in China Health Care System in the next four years. It wi;; be a challenge for the key stakeholders can make efforts to make forward positive changes. The vision of having every Chinese citizen enjoy affordable, high-quality healthcare is achievable. In this way, China can build a healthcare system that is on par with international standards and in line with its phenomenal economic growth to support its goal of achieving a “harmonious society.” References 1. 2.

3. 4.

5.

6.

7.

8.

9.

Chen, Z. (2009), “Launch of the Health-Care Reform Plan in China”, The Lancet, Vol. 373, No. 9672. Liang, Z., L. Guo and C. Duan (2008), “Migration and the Well-Being of Children in China”, The Yale- China Health Journal, Vol. 5. OECD (2010), Economic Survey of China, Paris. UNDP (2008), Access for All: Basic Public Services for 1.3 Billion People, China Human Development Report 2007/2008. Vail, J. (2009), “Managing Infectious Diseases among China’s Migrant Populations”, in C. Freeman 3rd and X. Lu (eds), China’s Capacity to Manage Infectious Diseases: Global Implications, Center for Strategic & International Studies, Washington D.C. Wagstaff, A. and M. Lindelow (2008), “Can Insurance Increase Financial Risk? The Curious Case of Health Insurance in China”, Journal of Health Economics, Vol. 27, No. 4. Wang G, Xu H and Jiang M.(2003) “Evaluation on comprehensive quality of 456 doctors in township hospitals.” Journal of Health Resources Ye, Y. (2009), “Backgrounder: Chronology of China’s Health-Care Reform”, Xinhua, http://news.xinhuanet. com/english/2009-04/06/content_11139417.htm Zheng, Z. and P. Lian (2005), “Health Vulnerability among Temporary Migrants in Urban China”, Paper presented to the Conference of the International Union for the Scientific Study of Population, Paris.

64

mfc bulletin/Aug 2010-Jan 2011

Brazil: A Country o f Inequalities 1 00

 Among the highest in income concentration

Brazil: A System for Universal Access to Health Care

 Gini = 60.7

 Important differences across economic levels in

Compiled by Abhay Shukla <abhayshukla1@gmail.com> based on various articles and documents

 health  education  employment

90

1188 4 67 283

80 70

1 26

60 50

58

40 30 20

29

10 13 0

Centiles of income distribution in Brazil. Values in US dollars.

 E a r lie r M o d e l – T h r e e S u b System s

Social Security

 Until the 1960s, health care services in Brazil were organized according to three subsystems,  Social security  The Ministry of Health and  The private sector

 Social security - the state began to participate in the financing of public and private companies’ social security benefits, to provide health care to workers and their dependants.  Social security institutions were organized according to professional categories and according to a classic insurance model: benefits depended on the ability of the category of employee to pay.  Social security became the dominant system for providing health care services in the country

Ministry of Health

Private Sector

The Ministry of Health, organized in a parallel structure, was responsible for preventive care. (vaccination campaigns, sanitation, and so forth) In terms of medical care, the Ministry was responsible only for the creation and maintenance of chronic care facilities

The private sector, was independent of the main subsystems and limited to services provided by autonomous physicians through direct payment

Pre-reform Situation

Growing Privatisation

Contributed to the creation of a specific model of health care in the 1970s, with basic characteristics that would become the principal targets of health reform.

 Relationship between the public and private sectors was restricted almost exclusively to contracting, based on fee-for-service payment, with no control over the kind of medical care provided.  Thus, medical care was characterized by highcost, specialized, curative, and hospital-based treatment.  The absence of policies based on the actual epidemiological profile and health needs of the population meant that services concentrated in the more profitable regions, causing an imbalance in supply

• high level of centralization • dichotomy of institutions within the health care system • growth in coverage through private provision of health care • incomplete coverage • regressive financing

mfc bulletin/Aug 2010-Jan 2011

65

Social Security Supporting Privatisation

An Inequitable Health System

 The National Institute of Medical Care and Social Security contracted more and more often with third parties to care for the increasing clientele.  This gave the private sector a progressively more important role in service provision.  As a result, the publicly owned network shrank and deteriorated. By 1976, for example, only 27% of all hospital beds were public, while 73% belonged to the private sector

 Health System was highly privatized, already established as a medico-industrial complex,  Preeminently curative, concentrated in urban nuclei and only in high-income neighborhoods.  Access was unequal, as were the services offered.  Services were not available in all regions nor were they prioritized.  Main feature of health care management was the non-existence of any public control over health policy.

S o c ia l S ec u r ity Cr isis I n 1980s  Proliferation of expensive medical care without a corresponding change in the method of financing;  A method of paying the private sector that stimulated an increase in expensive specialized procedures, as well as fraud;  Difficulty in controlling finances because of the disorganized structure of the system;  Deterioration in the quality of services;  A national economic crisis allied to a broader crisis of international scope.

The Health Movement The Health Movement strategically associated the demand for health care services with the demand for a democratic regime. The main principles of the Health Movement were that health is a right of all citizens, to be provided by the state through a universal health system based on integrity and equity in health care The effectiveness of the Movement required the construction of a political strategy that encouraged civil-society organizations to demand the universal right to health as an obligation of the state

Mobilisation on Two Fronts M o b ilisa t io n o n T w o F ro n ts Mobilisation proceeded along two broad fronts. First front was the production of knowledge, dedicated to promoting the political struggle, to the elaboration of case studies about the inequities of access to health care in Brazilian society and the inequities of the country’s health system. A Marxist perspective in health studies; the importance of Latin American Social Medicine



Second front consisted of the mobilization of those organized sections of society for the democratization of health care.



This proceeded along various lines of action, ranging from those professional areas linked to health care and education, to unions, religious social groups, social movements, and popular organizations



Led essentially, by two political forces –

 

The clandestine PCB (Brazilian Communist Party) The recently formed PT (Workers´ Party) - a mass left political party

S o m e D e m a n ds f or R e fo rm

8th National Health Conference

 restructuring financing mechanisms to broaden the support base beyond the payroll;  reversing the process of privatization and establishing ways for the public sector to control the private sector;  giving greater decision-making and financing autonomy to the states and municipalities; and  introducing the participation of social organizations in formulating and implementing health policies.

In 1986 at the 8th National health conference the reform became a policy This brought together not only broad sectors of civil society and representatives of the most important institutions in the sector, but also professional groups and political parties. The conference differed from previous ones in its participatory nature

66

mfc bulletin/Aug 2010-Jan 2011 G u id in g P rin c ip le s o f t h e H e a lt h C a r e R e fo r m

Guiding Principles of the Health Care Reform

 Health as a right of citizenship. All Brazilian citizens acquired the right to health care provided by the state, thereby characterizing health as an activity of public relevance.  Equal access. All citizens should have equal access to health services, with no discrimination of any kind.  Health as a component of social welfare. The health sector had to be integrated with the social welfare system,  A single administration for the public system. creation of a single system to aggregate all health services provided by federal, state, and municipal public institutions







Integrated and hierarchical health care organized to provide integrated care; activities had to be based on the epidemiological profile of the population. Provision of services had to be arranged with respect to the health care hierarchy and had to provide people with universal access to all levels of care. Social control and social participation. The system had to be governed according to democratic criteria, and the participation of civil society in its decisions was of paramount importance. Decentralization and regionalism led to a redistribution of the responsibilities between levels of government. Provision of health services had to become the responsibility of municipal governments, aided financially by the federal government and the states

The Brazilian “Unified Health System” (SUS)

T h re e H e a lt h C a r e D e liv e r y System s

 Created by the 1988 Constitution  Universal system

Presently, three main care health delivery systems coexist in the country:  the SUS, which provides free care to all residents in the country (covers 75% of population)  the Supplementary Health System (SHS) run by private healthcare insurance companies or health cooperatives (covering 35 million paying members)  the Private Health System (PHS), totally private, used only by the highest-income population  Health care funding in Brazil is drawn from various sources; two thirds are public and one third private

 covering everyone independent of contribution  offering preventive and curative care, dealing with simple and complex problems  decentralized at municipal level

Funding for SUS

S o u rce s o f F u n d s

 Funding for the SUS is guaranteed by a Constitutional Amendment, approved in 2000, according to which Federal funds should increase at a rate of 5% a year and States and municipalities are obliged to spend 12% and 15% of their respective revenues on health  Total national health expenditure is estimated at US$250 per capita.

Social and Health Expenditure - Brazil 1980-81

1982-89

1990-95

% GNP

2.9

3.5

4.6

Per capita

33.5

57.7

100.5

 Federal Government still provides over 70 per cent of funding for the health sector, with States providing 20 per cent and municipalities 10 per cent or less.  Funds are collected through taxation. A tax levied on money transfers into bank accounts was introduced to benefit the health sector.

P riv a te S e c to r h a s H ig h er Co s ts

Education

Health % GNP

3.2

3.5

4.6

Per capita

36.3

58.5

100.5

Social security % GNP

6.4

6.6

8.5

Per capita

73.3

108.8

185.6

 In 2007, the budget of the Ministry of Health was R$40 billion. This financed a system that potentially covered about 143 million beneficiaries of public health care  Around 40 million Brazilians have some type of private health insurance, R$60 billion were spent by those affiliated to the private health care system.  It is therefore a myth that privatization reduces costs

mfc bulletin/Aug 2010-Jan 2011

Distribution of Care  Seventy-five per cent of the population is covered by the SUS services.  Public institutions provide 75 per cent of SUS out-patient care.  Between 70 and 80 per cent of SUS inpatient care is provided by contracted private services.  University hospitals are mainly public and provide half of public hospital care.

PSF Implementation  Initially deployed in  areas not covered by a health center  poorest areas

 Next, existing health centers turned into PSF units  Eventually, all primary health care to be based on PSF  Ministry of Health estimate:  ~35% population covered

Health Councils All social sectors are represented in these councils:  clientele or community representatives (50%)  health providers plus health managers / officials (25%)  healthcare workers (25%)

T re n d s in th e H e a lth M o v em e n t  Disagreements over the manner in which public participation is being institutionalized and bureaucratized have led to divisions in the people's movement for health.  An MOP faction has defended creation of a People's Health Council as an autonomous forum to replace the State Health Council.  The dilemma of the people's movements in playing the role of State's opponent while interacting with the State; when the councils were created, some movements, upon being called, refused to have institutionalized participation  In 1992, at the Ninth National Health Conference, social movement members decided to create and maintain autonomous forums in order to preserve their independence and avoid the possibility of the forums being treated as instruments.

67

Family Health Program - PSF  Health facility with clear geographic coverage  Team formed by    

full-time general practitioner registered nurse nurse 4 community health workers

 Looks after 1000 families (~3000 people)  Competitive salary levels

L a rg e S c a le P e o p le ’ s P a r t ic ip a t io n A landmark of SUS is community participation, guaranteed by a network of over 5,000 Municipal Health Councils, 27 State Health Councils, and the National Health Council, involving some 100,000 individuals in this voluntary work. Most of the decisions on healthcare such as budget, construction of health facilities, implementation of health programs, etc., must be approved by health councils

Twelfth National Health Conference  The participatory process reaches its peak during the National Health Conferences: the latest, held in December 2003, involved approximately 300,000 people at three levels: municipal, State, and national  The Twelfth National Health Conference was held in 2003 on the theme  "Health is a Right for All and the Duty of the State – the Health We Have and the SUS We Want"was attended by nearly 5,000 people

M e c h a n is m s fo r T r a n s f e r rin g Funds Mechanisms for transferring funds from the federal to state and municipal levels – Previously such transfers were based on calculation – either of existing infrastructure or service capacity and provision. Starting in 1998 the transferring of funds became automatic and based on a fixed per capita value for basic health services — either individual or collective.

68

mfc bulletin/Aug 2010-Jan 2011 Distribution of Health Care Facilities

Public

T w o T y p e s of T ra n sfe rs With the creation of the PSF (Family Health Care Program) two components,  a fixed one (based on a set per capita calculation),  a variable component which allowed the transfer of federal funds to priority programs. These include the PSF, the Pharmaceutical Assistance Program and the Program for Controlling Nutritional Deficiencies.

Classes Basic units

n 6 038

98.0

131

2.0

Health centres Polyclinics

14 129

98.5

189

1.5

2 126

25.5

6 170

74.5

Emergencies

188

65.5

98

34.5

Hospitals Total

%

n

%

1377

21.0

5 155

79.0

23 858

67%

11 843

33%

Human Resources - Brazil

S h ift fr om P riv ate to P u b lic In 1995 the relation of public hospital beds per thousand people was 0.71 while that of private hospital beds was 2.29/1,0000 In 2005 these proportions had changed to 0.84 and 1.19 – trend of increase in public beds compared to private

Private

1976

1992

Public

Private

Public

Private

Physician 54 201 s

62 259

65 205

106 356

Nurses

40 200

46 785

48 242

Organogram

30 833

mfc bulletin/Aug 2010-Jan 2011

69

What it Costs to Provide Medicines to All Sick Persons in India - Narendra Gupta1 Expenditure on medicines constitutes the biggest component in overall health care expenditure. According to the NSS (National Sample Survey) 55 th Round1 on consumer expenditure, it has been found that out of total expenditure in health care, 77% expenses in rural areas and 70% in urban areas are incurred on medicines alone. It also revealed that poor families have to spend even larger amount on medicines. There are some other shocking findings such as nearly 23% of India’s total population never seeks any treatment because of their inability to pay2, between 1999-2000 about 32.5 million people fell below poverty line just after a single hospitalization3 and about 40% of those hospitalized even once were forced to borrow money or sell assets to meet the costs of medication4. This has been observed that even if patients are able to receive a free consultation at a government clinic, they are often forced to pay out-of-pocket for the medicines prescribed for their illness. Medicines purchased by patients from the local chemist can be between 2 to 40 times more expensive than the bulk prices offered to retailers, private hospitals, nursing homes and government agencies for same medicines. There are very few states in India where medicines are provided free by the state to its citizens. Most states have very small budget for medicines and even that budget is not spent fully or used for medicines which are not required so much. For instance Rajasthan has budget of less than Rs. 6 per capita when the requirement is several folds more. Even after purchase of medicines by the Government agencies, many of the times they just expire lying in the go downs of state or district. Owing to no price control regime for almost on all medicines in India, it is legally possible to sell medicines at any cost which pharmaceutical companies may decide. Similarly, prescription of unreasonable and unnecessary medicines by medical services providers at all levels is also rampant. Ironically, medicines which are unnecessary are the most expensive amongst all the prescribed medicines. Sale of drugs at exorbitantly high prices by the pharma companies and doctors prescribing unnecessary expensive drugs is a lethal combination and put patients and families especially from poor background in a very disadvantaged situation. In this context, it is quite important to undertake an exercise which may provide indicative computation to know that how much it would cost to provide medicines to all sick persons in India based on current morbidity pattern. Such an exercise may provide information for allocation of required funds and how should they be allocated. Based on the experience of Chittorgarh Generic Medicine Project, a computation has been attempted to ascertain what amount of financial allocation is required if all sick persons of India would have full access to rational treatment for their different ailments. It is essentially based on the cost of treatment through standard treatment procedure by using quality generic medicines. One illustrative list of procurement and sale costs of different medicines required for most of the diseases can be 1

Email: <narendra531@gmail.com>

seen at http://chittorgarh.nic.in/Generic_new/generic.htm. Chiitorgarh Generic Medicine Project initiated by the District Administration in April 2009 is a unique venture of its kind initiated with careful planning, precision and participation of different stake holders. To initiate the process, a committee was constituted comprising of the doctors from different specialties of the Government district hospital. The mandate of the committee was to draw a list of medicines in their generic names which are required to treat all different kind of diseases in the district. The committee drew a list of generic medicines required for treatment of different diseases by them after consulting wide range of doctors in the district. Once the complete inventory list of required medicines with their generic names was ready, a tentative list of well established pharmaceutical companies which produce them was prepared and their area sale managers were contacted to get their offer price list of medicines in branded generic category. The district cooperative society then was asked to further negotiate the rates and procure these drugs. The cooperative was also asked by the district administration to establish generic medicines shop in different hospitals. The listing of diseases is drawn from the list of most commonly prevalent diseases in India through International Classification of Diseases round 10 and Rajasthan Standard Treatment Guidelines 2006 – Medical, Health & Family Welfare Department, Government of Rajasthan. The actual prevalence of diseases is based on the data collected by the National Commission on Macroeconomics & Health, World Health Organization documents and reports from national Sample Survey Organisation(NSSO), National Family Health Survey(NHHS) and District Level Health Data (DLHS)and from other secondary sources, e.g., Park’s Text Book of Preventive & Social Medicine. The computation of cost of treatment is an indicative exercise and it is possible that there are large variations depending on the state taxes, pharmaceutical companies and purchase negotiations. The current computation is done based on the sale prices of drugs in Chiitorgarh Upbhokta Bhandar shops located in District Hospital, Chittorgarh. The computation also takes into account the gravity of the health problems and appropriate institutions for its management to evolve a rational system of morbidity burden. Therefore, listing of diseases has been organized according to morbidities treatable at primary health institutions, community health centres, i.e., first referral units and district & higher health facilities. The computation provides estimates on the treatment cost per episode of a disease, its national prevalence in India and thereby total episodes of the disease in a year and its total approximate cost. Based on this estimate, an allocation of about rupees 335 billion is required annually to ensure medicines to all people of this country, however, only about 60 billion is required annually to treat all sick persons who do not require any hospitalization. It is hoped that once the access to medicines for out patients is ensured irrespective anybody’s ability to pay, the morbidity load which requires hospitalization would reduce substantially.

70

mfc bulletin/Aug 2010-Jan 2011

Based on the experience of Chittorgarh Generic Medicines Project which led to significant reduction in out of pocket expenditure in health care , the author argues that the State should ensure free treatment to all as this will lead to fundamental changes in the manner treatments are currently administered in India. State provided free treatment would lead to •

Promotion of rational medication because the providers will not be able to establish any nexus with the pharmaceutical companies for gifts and commissions.

•

Reduction in catastrophic illnesses because the persons who have been avoiding seeking treatment till the disease turn serious would begin to report early. This will significantly reduce occurrence of severe morbidities.

•

Reduction in iatrogenic illnesses. This has gained new importance with the rise of newer kinds of viruses, like NDM1, owing to indiscriminate and irrational use of antibiotics and poor hygien

•

Money saved from medicines will appropriately be used by the families in buying nutritious food, secured housing and improving sanitation.

Allocation of Rs. 60 billion is not a big amount as the current Government in its political manifesto has committed to increase the health budget from current 0.9% of India’s GDP to about 3%. Allocation of Rs 60 billion would not increase the allocation to even 1.5%. The Government of India therefore should make this provision through legislation without any further delay. The accompanying tables of computation are divided in three sections: Diseases which can be treated at primary health centres – these are generally of the nature which does not require hospitalization. The second sheet is for the diseases which can be treated at the first referral units such as CHCs and then a list of diseases which require more specialized attention and can be treated at district or higher level hospitals.

1 . T r e a t m e n t C o st o f D ise a se s T r e at ab le at H e a lt h S u b C e n tr e s a n d P r im a r y H e a lth C e n tr e s Ind ia P o p u la tio n 2 0 1 0 (R u ra l) (P r ojec te d ) = 8 4 4 ,7 4 0 ,0 0 0 Level C hild h o o d di sea se s

D ise a se s

P e r u n it C o st o f drugs

A pprox n um ber o f c a se s p e r la k h p o p u lat io n

T o tal C o st o f D rugs

Im m u n iza tio n

10

A cu te R e spira to ry In fec tio n s: P n eu m o nia

6

3 ,1 2 0

2 6 ,3 5 4 ,2 3 8

1 4 9 ,1 54 ,4 4 5

13

3 ,1 2 0

2 6 ,3 5 4 ,2 3 8

3 5 5 ,5 45 ,0 2 3

52

312

2 ,6 3 5 ,4 2 4

1 3 6 ,8 83 ,9 2 8

1

312

2 ,6 3 5 ,4 2 4

2 ,0 4 1 ,6 6 3

D ia rrh o ea : w ith so m e de h yd ra tio n D ia rrh o ea : w ith se v er e de h yd ra tio n D y se n ter y

2 ,4 0 2

A pprox no. o f c ase s in In d ia 2 0 ,2 8 8 ,0 9 7

2 0 5 ,7 45 ,6 5 2

M a te rn a l di sea se s

A n ten a ta l ca re

156

2 ,4 0 2

2 0 ,2 8 8 ,0 9 7

3 ,1 6 3 ,6 7 7 ,1 9 2

283 284 34 1 1 78

49 431 22 566 787 1 ,5 9 5 2 ,4 0 2

4 1 6 ,5 7 6 3 ,6 4 4 ,1 6 0 1 8 2 ,2 0 8 4 ,7 8 1 ,7 0 1 6 ,6 4 5 ,1 5 6 1 3 ,4 7 3 ,4 5 0 2 0 ,2 8 8 ,0 9 7

1 1 7 ,9 47 ,4 7 2 1 ,0 3 4 ,3 4 6 ,7 1 8 6 ,2 0 3 ,4 3 3 5 ,3 1 3 ,4 6 6 1 0 ,7 7 3 ,3 7 1 1 ,5 8 5 ,9 2 8 ,6 7 7

B lin d n e ss

A b or tio n s F e m a le S teri liza tio n V a sec to m y IU D In sertio n O ra l c o n tra ce p tiv e s C o nd o m s P o st na ta l ca r e B lin d n e ss du e to re fra ctiv e err o r & lo w P a u cib a c illa r y M u ltib a c iilla r y N e w S pu tu m P ositi v e N e w S pu tu m N e g a ti v e T rea tm e nt a fter de fa u lt/R etr ea tm en t/F a ilu E x tra pu l m o na r y

L e pr o sy T u b er -cu lo si s

V e ctor bo r n e di sea se s

M a la ria : P .fa l cip a ru m

O th er s

M a la ria : P .vi va x & P .ov a l e K a la A za r R T Is/S T I s M i no r in ju rie s in clu d in g fa ll s O th er m in o r a il m e nt s S na k e B ite

T o tal C o st

27

263

2 ,2 2 4 ,0 0 1

5 9 ,0 5 8 ,7 8 4

110 665 235 203

37 19 356 347

3 1 2 ,1 9 9 1 6 4 ,2 3 9 3 ,0 0 6 ,6 8 1 2 ,9 2 9 ,5 8 6

3 4 ,3 5 5 ,2 5 8 1 0 9 ,2 99 ,6 0 2 7 0 7 ,9 96 ,2 9 3 5 9 4 ,1 35 ,3 0 6

471

36

3 0 6 ,7 7 5

1 4 4 ,5 94 ,5 6 8

203

73

6 1 6 ,7 5 5

1 2 5 ,0 81 ,1 1 7

9

74

6 2 8 ,1 6 5

5 ,6 7 6 ,0 9 8

7

112

9 4 2 ,2 4 7

7 ,0 1 0 ,7 9 2

101 124

2 450

1 3 ,3 5 4 3 ,8 0 0 ,5 7 0

1 ,3 4 3 ,7 0 3 4 7 2 ,7 79 ,4 8 8

93

2 ,1 6 7

1 8 ,3 0 6 ,8 7 3

1 ,6 9 5 ,1 8 8 ,9 6 7

5 125

2 4 ,2 1 1 361

2 0 4 ,5 1 7 ,1 0 8 3 ,0 5 1 ,1 4 5

9 4 1 ,4 33 ,1 5 0 3 8 1 ,1 35 ,3 6 3 1 2 ,0 5 2 ,6 4 9 ,5 2 8

H i gh -li g h te d i n ye llo w are n ot in clu de d in c osti n g a s t he se a re al re ad y a va il able fre e .

6 ,1 2 9 ,9 3 5 ,9 8 0

N e t C o st

5 ,9 2 2 ,7 1 3 ,5 4 7

mfc bulletin/Aug 2010-Jan 2011

71

2. Cost of Medicines and Other Procedures Carried Out at Referral Units/Community Health Centres Level

Diseases

Approx number of cases per lakh population

Cost of drugs Rs

Approx no. of cases in India

Total Cost of Drugs Rs

BASIC (CHC) Inpatient Tt Childhood diseases

Birth asphyxia

162

25

211,185

34,236,045

Neonatal sepsis

496

25

211,185

104,759,819

Low birth weight (Bwt 15001800g)

337

99

836,293

281,825,003

Low birth weight (Bwt 18002500g)

-

570

4,815,018

-

Acute Respiratory infections: Severe Pneumonia

931

322

2,720,063

2,533,433,307

Normal delivery

-

2,108

17,807,119

-

Puerperal sepsis

232

18

152,053

35,209,226

Septic abortion

232

5

42,237

9,780,341

Ante partum hemorrhage

279

12

101,369

28,326,356

Postpartum hemorrhage

143

21

177,395

25,320,710

Eclampsia

162

25

211,185

34,278,831

Obstructed labour

241

32

270,317

65,185,626

Remaining Caesarean sections

241

92

777,161

187,408,674

1,424

248

2,094,955

2,982,403,153

Cataract blindness

69

452

3,818,225

265,291,786

Malaria: complicated

210

40

337,896

71,093,116

-

-

Severe anemia Blindness Vector borne diseases Additional services Otitis media

Chronic otitis media

48

3,000

25,342,200

1,204,393,123

Diabetes

Without insulin

581

2,065

17,443,881

10,136,801,477

With insulin

3,730

885

7,475,949

27,884,585,536

-

857

7,239,422

-

With one drug

32

1,714

14,478,844

462,286,310

Hypertension

Others

With diet & exercise

With two drugs

319

857

7,239,422

2,306,136,637

Chronic Obstructive Pulmonary Disease

161

1,461

12,341,651

1,992,061,016

Asthma

579

2,330

19,682,442

11,397,220,389

Major surgeries

438

3,699,961

-

Accidents/ Major injuries

438

3,699,961

-

6,993

59,072,668

-

Counselling for psychiatric care

-

Total Cost

62,042,036,480

OPD Cost (35 % of total cost)

21,714,712,768

72

mfc bulletin/Aug 2010-Jan 2011 3. Cost of Medicines and Other Procedures Carried Out at District and Higher Hospitals

Level

Secondary care (DH) Cardiovasc. disease

Diseases

Cost of drugs Rs

Approx number of cases per lakh population

India Population 2010 (Project.)

Approx no. of cases in India

Total Cost of Drugs Rs

117 crores Coronary artery Disease: incident cases Coronary Artery Disease: prevalent cases

420

283

3,311,100

1,389,407,755

3,396

3,353

39,230,100

133,237,070,940

Rheumatic heart disease

113

72

842,400

94,782,131

Hypertension

Acute Hypertensive stroke

2,407

118

1,380,600

3,323,005,901

Cancers

Breast cancer

343

11

128,700

44,164,074

Cancer of cervix

6,310

10

117,000

738,282,308

Lung cancer

463

2

23,400

10,823,268

Stomach cancer Schizophrenia: without hospitalization Schizophrenia: with hospitalization of 10 days in 5% Mood/Bipolar disorders: without hospitalization Mood/Bipolar disorders: with hospitalization of 10 days on 5%

3,909

3

35,100

137,191,948

738

289

3,381,300

2,494,587,888

713

15

175,500

125,154,666

1,879

1,543

18,053,100

33,919,503,820

1,877

81

947,700

1,778,515,989

Common mental disorders Child & adolescent Psychiatric disorders Geriatric problems including dementia

1,292

2,030

23,751,000

30,679,463,588

728

2,517

29,448,900

21,448,105,052

5,082

406

4,750,200

24,139,038,613

Epilepsy Major injuries & emergencies (50%) Other major injuries (50%)

1,305

913

10,682,100

13,936,550,246

438

5,124,600

-

438

5,124,600

-

Mental diseases

Others

Total Cost OPD Cost (20% of total cost)

267,495,648,187 OPD Cost (20 % of total cost) 53,499,129,637

mfc bulletin/Aug 2010-Jan 2011

73

4. Requirement of Funds for Treatment of All Ailments as Outpatients in India (Based on Burden of Disease) At health Sub Centres and Primary Health Centres At Referral & Community Health Centres (35% of total load as OPD) At District & Higher Hospitals (20% of the total load as OPD) Total Cost

Cost (Rs.) 5,922,713,547 21,714,712,768 53,499,129,637 81,136,555,953

Total Population of India 2010 (Projected) Total cost of drugs in OPD care

1,170,000,000 81,136,555,953

Per Capita Cost required in a year App. per capita requirement after removing allocations done through national programmes

69 50

Costing is based on treatment through standard procedures by quality generic drugs procured at lowest possible rates in bulk through open tender process

5.Requirement of funds for treatment of all ailments in India (based on burden of disease) Cost (Rs.) At health Sub Centres and Primary Health Centres

5,922,713,547

At Referral and Community Health Centres

62,042,036,480

At District and higher hospitals

267,495,648,187

Total Cost

335,460,398,214

Total Population of India 2010 (Projected)

1,170,000,000

Total cost of drugs in OPD& IPD care

335,460,398,214

Per Capita Cost required in a year App. per capita requirement after removing allocations done through national programmes Requirement of funds for 57 crore citizens of India per year Costing is based on treatment through standard procedures by quality generic drugs procured at lowest possible rates in bulk through open tender process

287

74

mfc bulletin/Aug 2010-Jan 2011

Towards a Fair Effective and Sustainable Health Care System for India - Rajkumar Ramasamy 1 To improve India’s health parameters a radical strengthening of primary health care is needed with a revised health delivery system more than just extra health care funding! Newspapers carry frequent news about increasing government spending in health care and the availability of increasingly more specialised hospital care in India. While facilities in private and government hospitals have improved significantly, relatively little improvement has occurred in primary health care (PHC). India’s current hospital based health care system produces an unsustainable and spiralling health care expenditure on the Indian Government and poorer people. Sudden health care expenditure is already the most common cause of huge debts in poorer people. This article shows through case histories why this health care delivery system has to change and suggests what these changes should be. (I have used the terms ‘family physician’, general practitioner ’ and ‘primary health care practitioner’ to have the same meaning).

Critical Role of PHC through Case Studies These comparative stories are based on true events which have been changed to not identify individuals. Chandran and Mumtaz’s Story Chandran is aged 27 and married with two children and runs a small business. He has a long history of becoming anxious when he develops palpitations, difficulty in breathing, a feeling of impending doom and chest tightness. Chandran consulted a cardiologist who arranged several tests that quickly cost Rs 10,000. Chandran had to borrow money to pay for these tests. When his symptoms failed to improve Chandran finally visited a trained family physician who diagnosed generalised anxiety disorder. Chandran was seen by a team of staff in the health center who helped him understand how anxiety can mimic cardiac symptoms. Chandran was taught relaxation exercises. Chandran is now well and even if these symptoms recur he knows how to deal with them himself. Mild to moderate mental health problems like anxiety and depression are very common in India in all social groups and often mimic physical illness leading to unnecessary expenditure on futile specialised investigations. These illnesses can be Family Physician, KCPatty Primary Health Center, Kodaikanal Taluk, Tamil Nadu 624212. Email: <ramasamy.rajkumar@gmail.com>

1

Everyone has the right to sustainable and accessible health care that they are comfortable with.

diagnosed by a trained family physician with minimal expenditure and do not need a psychiatrist. The family physician can also treat these moderate psychological illnesses at home refer only those with more complex illnesses and doubtful diagnosis to a psychiatrist. When Mumtaz presented to the same skilled family practice physician with similar symptoms she was diagnosed and treated for just Rs 400 expended over 6 months with good results. Unfortunately in India existing private and government primary care doctors are not trained to practice primary health care. In countries with a sustainable health care system, 50% of postgraduate medical training is in primary health care, a recognised field of medicine given respect as a speciality in its own right. In India the number of primary health care postgraduate training seats is a woeful less than 0.5% of all post graduate medical training seats! There is not even a well developed modern syllabus in primary health care. Most government PHCs are staffed by specialists who both know little about PHC and waste their specialised skills there. Worse, primary health care or family medicine training is often confused with community medicine or social and preventive medicine. While PHC practitioners will need to draw relevant preventive skills from community medicine they need a wide range of clinical skills best taught in smaller hospitals and oriented to primary health care by ample practice throughout their training in a well functioning primary health unit. Unfortunately in desperately trying to encourage family medicine training the government is wrongly allowing big urban speciality hospitals to train doctors in family medicine.

mfc bulletin/Aug 2010-Jan 2011

75

Mani and Selvi’s Story Mani aged 14 months was the 3rd child in a family of poor agricultural daily wage earning parents. When he developed fever and cough one day the parents took him as usual to a local MBBS doctor with no primary health care training. As in previous illnesses Mani received an injection and some expensive tonics costing Rs 160. Unfortunately Mani became worse that night and developed difficulty breathing. He was taken to some spiritual healers the next day. Two days later the child was taken to a large hospital where he was diagnosed to have severe pneumonia and survived after the parents borrowed and spent Rs 10,000 for his hospital expenses. Selvi is of similar age and background to Mani. When she had developed fever in the past Selvi’s mother Rubini takes her to a health center staffed by doctors and health workers trained in primary health care. Normally when Selvi had similar fevers she only received paracetamol and advise on how to feed a child with viral illnesses. The health workers also took time to explain to Selvi’s parents why viral illnesses do not need antibiotics or injections and Rubini now understood that. However on this occasion the doctor showed Rubini that Selvi, though not yet looking ill, had a faster respiratory rate. The trained family physician also knew up to date clinical guidelines supported by the World Health Organisation that a small child with fever and fast breathing should be treated as having early pneumonia in a primary health care setting and that the most rational antibiotic is penicillin. Selvi was started on penicillin and then asked to return for review from home the next day. Since Rubini was familiar with health workers in the health center who were local people and since the health center was also close by she had no difficulty in returning the next day. Selvi was much better the next day and the treatment was completed now with oral antibiotics. The total cost was Rs 100 and Selvi’s life was never at risk. It is highly likely that Mani could also have had less danger and costs to his family if he had been treated by a skilled family physician at first. In the past when he had simple viral illnesses he would also have not had useless injections and tonics which would have both reduced expense on useless medicines and prevented the emergence of increasing antibiotic resistance. Even when pneumonia is severe, a hospital working closely with a PHC can refer patients back to a PHC early once the patient is better, reducing costs. It is also essential for PHCs to work as a team of doctor, nurse and local health workers. Not only will the weakest sections of the community (poor and uneducated or elderly) use them because they are located

My health center! Patient and health worker from local community: removes fear of coming to the health center

closer to their homes but also because the familiar staff means they are not afraid to use PHC services. Communication difficulties between busy doctors from urban backgrounds and poorer rural people can lead to misunderstandings and wrong diagnoses. Health workers can bridge that communication gap and provide explanations for treatment given that are then likely to be accepted and completed. Health workers can also be trained as health educators based in the PHC who visit schools and villages to provide health education in radical ways that motivate and encourage people to understand when to see health professionals and how to prevent disease. Parameswaran and Karthik’s Story Parameswaran, aged 38 years, met with a car accident. He was taken bleeding from multiple wounds and unconscious 45km to the nearest specialised hospital. There he was found to be in shock from blood loss. Though he had no major brain injury needing neurosurgery he suffered irreparable brain damage and is severely disabled simply because the low blood pressure from loss of blood severely increased the damage to the brain. The costs of such disability are enormous. When Karthik had similarly severe injuries he was first taken to the local PHC. The trained PHC team also know how to stabilise and safely transport critically ill persons. His bleeding was stopped with simple pressure bandages and fluid replacement stopped his blood pressure dropping. These critical interventions took only 30 minutes after which he was then taken to major hospital 50km away where he made a full recovery. Even in emergencies local PHCs in rural areas can provide the crucial stabilisation that allows later specialised care to

76

A critically ill patient stabilised at a PHC so that he can be referred with a much better chance of survival to the base hospital

result in a good outcome. For example emergency treatment called thrombolysis after heart attacks needs to be given as soon as possible if it is to be effective. PHCs with trained family physicians can initiate this treatment near the home and then refer patients to a specialised hospital. Murugan and Ravi’s Story Murugan is a small farmer aged 46 years who had a sudden stroke and was rushed to a specialised hospital. He made a good recovery but was found to have hypertension. He was sent home on several medications but after 6 weeks he could not afford to continue these. He also felt well and did not understand the need to continue the treatment. 12 months later he had a further stroke and since then he has tragically remained paralysed on one side. Ravi aged 43 had a similar stroke and was brought to the primary health center. His family were informed of the diagnosis and the costs and benefits of a hospital admission. He made a good recovery in hospital. His also had high blood pressure and he was advised not to stop the medication to prevent further strokes. The health workers explained how high BP is often silent and just because he was feeling well he should not stop treatment. The PHC has a ‘chronic disease register’ and a recall system to ensure that people like Ravi receive on going monitoring through home visits to ensure that treatment was understood and continued. When Ravi did not come after 6 months of regular treatment to collect his drugs the PHC staff informed Sumathy the HW who visits Ravi’s village. Ravi told Sumathy that the medicines prescribed in the hospital were too costly. Sumathy arranged for a joint consultation at the PHC with the

mfc bulletin/Aug 2010-Jan 2011 family physician. The daily cost of medicines was Rs 9. The family physician knew the relative merits of each prescribed medication and after discussing the merits with Ravi in simple language, it was decided that 90% of the protection from further strokes was provided by just 4 medicines that cost only Rs 1.50 per day which Ravi could now afford. Ravi remains well on regular treatment 4 years after the initial stroke. Ravi’s stroke was also partly due to his smoking and the health worker held a meeting with all the young men in the village who smoked and allowed Ravi to tell them how lucky he was to survive and how smoking harms them. Home visits and recall systems maintained in PHCs can encourage preventive care and ongoing care of those with chronic illnesses. Failure to implement this aspect of PHC will relegate preventive care programs to nobody’s responsibility even though preventive health care can save enormous amounts by preventing disease. A young smoker who stops smoking will save his family an average Rs 5 lakh through prevention of illness and lost working days, yet these interventions cannot be done through hospital based health care delivery. Targeted home visits also ensure that weaker members of the community are not neglected and that it is not just those who come to the health center who receive holistic care but also those who for whatever reason do not come to the health center also receive importance – an alien concept for most doctors but essential if health standards are to improve through preventive care! Jayanthy and Mariammal’s Story Jayanthy sold flowers at the market. She was pregnant with her 3rd child and her 2 previous children were born normally. She felt she would deliver normally again and on this occasion did not have any antenatal care.

A cardiac patient visited at home by a health worker to encourage continued treatment and follow up.

mfc bulletin/Aug 2010-Jan 2011

77

However near the time of delivery she had swelling of her face and ankles and at the time of labour pains suddenly had a convulsion. Her terrified family rushed her to a large hospital where she was found to be very anaemic and had high blood pressure which caused fits. It was after blood transfusions and an emergency caesarean operation that she survived but the baby died. The treatment cost Rs 20,000.

What the Government should do to improve the quality of primary health care in India?

Mariammal was also poor like Jayanthy and pregnant but was too busy to see a doctor for antenatal care. However in her regular home visits Health Worker Sumathy identified Mariammal as pregnant and regularly gave her iron tablets and checked her blood pressure. When the blood pressure was found to be high Sumathy patiently explained to the family the dangers facing Mariammal. She saw the family physician who started Mariammal on treatment and informed that a hospital delivery was safest. Arrangements were made to see the obstetrician and fears about hospital delivery were removed through explanations. Mariammal delivered a healthy infant normally in hospital and had access to timely obstetric interventions should she have needed them. Preventive primary health care had saved lives and costs by removing the need for blood transfusions through simple iron tablets in pregnancy and the early detection of raised blood pressure in pregnancy. Regular antenatal care where iron is given to pregnant mothers can halve maternal deaths in India.

2. Develop an up-todate and modern family medicine syllabus suited to India. The existing syllabus in family medicine of the National Board of Examinations and others are outdated and designed by hospital specialists and community physicians. Family physicians should develop these syllabuses.

These histories show that primary health care bya trained PHC team can prevent illness, help manage emergencies better, reduce costs of acute care and yet be accessible to the poor and weak as well as the rich. However for primary health care to be effective we need a health care team with doctors trained in primary health care. We also need a system that stresses the pivotal role of PHC in the health care delivery system and the government needs to realise that private practitioners must supplement its own primary health centers in providing primary health care because 80% of health care in India will remain through private hands. Broadly 2 steps are needed to improve PHC in India: • first improve the quality of primary health care and then • change the health delivery system so that primary health care plays the pivotal role in it, creating a fairer affordable and hence sustainable health care system for India

1. Make a regulation that henceforth all those entering general practice must undergo a 3- year post graduate training program in primary health care just like surgeons do similar specialised training. The Government of India should recognise primary health care as specialised field in its own right.

3. However since there are tragically inadequate numbers of trained family physicians in India available to train others, the government can still enforce appropriate family medicine training by ensuring that the final examination in family medicine tests skills that are appropriate to primary health care. Earlier in many other countries too it was not the selection of training centers that improved the quality of care but the quality of the final examination that doctors had to pass. Examinations based on those of the Royal Australian and UK colleges of General Practitioners provide practical examples of relevant exams that ensure that candidates will learn skills relevant to family medicine. These exams could be adapted to Indian conditions and include how to select sustainable health care instead of blindlyimplementing treatment regimens available only for rich countries. The aim should be to know what is ideal but also how to adapt it to what a family can accept and sustain. 4. Recognise training programs in family medicine that include a rotation in recognised general hospitals for 2 years followed by 1 year in a primary health practice with one day weekly reorientation classes in family medicine throughout the course. Later when there are adequate trained family physicians reaccredit training in family medicine to only those centers that are genuinely practicing family medicine. The aim should be to create an equal number of family medicine post graduate training seats to the existing hospital specialist post graduate seats in India! 5. All those practising at primary health care levels in government and private health centers, whether

78

mfc bulletin/Aug 2010-Jan 2011 vocationallytrained or not in family medicine, must do compulsory continuing medical education (CME) and be allowed to reregister every 3 years only if they have acquired the needed CME points. It will not be difficult to set up an internet online CME learning system for general practitioners by adapting and using the excellent online interactive CME programs developed by the Australian and UK colleges of general practitioners and many other family medicine/ primary health care on line educators. Hence the lack of trainers in family medicine in India need not be a hindrance to enforcing CME and therefore improved quality of primary health care in India.

6. Encourage similar developments in nursing where there can be primaryhealth care nurse practitioners. Further modernise the primary health care health worker syllabus for health workers supporting nursing and medical staff in PHCs. 7. In rural areas encourage and fund existing primary health centers that have an extended role and greater need for a multidisciplinary team. Regulating the Health Care System to be PHC-Based Now that the quality of primary health care is secured, it is necessary to remove the chaos created by specialists and primary health care providers working with no coordination together and no one being in charge of the health care of families. 1. Ensure that every Indian citizen is registered with a recognised family practitioner who will be the sole primary health care provider for that person. Encourage private and government hospital specialists are seen by patients only after a referral by this family physician except in recognised emergencies such as severe chest pain or trauma. This policy will ensure that essential diagnosis/ investigations are completed at primary care level and more appropriate referrals only are made to the correct specialist, saving time and expenses. For example patients who make direct self referrals to specialists can be made to pay charges while those referred via family physicians may attract a government subsidy. Specialists should also be encouraged to discharge to family physicians the ongoing medical care of patients with a proper written management plan after diagnosis and stabilisation instead of trying to provide simple ongoing care themselves. Overall government

health expenditure will markedly reduce because of reduced expense at hospital levels. 2. Provide subsidies to PHC practitioners when they show evidence of preventive care activities in their target registered population. For example when a high percentage of infants in their care achieve full immunisations, when antenatal mothers receive at least monthly health worker and a family physician or obstetric specialist checks in their pregnancy, those under 30 years have smoking and alcohol intake recorded and are given appropriate smoking and alcohol prevention interventions (of which there are many effective evidence based interventions now available); those above 40 years have their BP checked at least twice yearly and those on BP treatment achieve target blood pressures, etc. This system may need monitoring through the creation of area-wise family medicine boards but it will markedly reduce the nation’s overall health costs by preventing illness. 3. In the long term have an accreditation system where private and government health centers having necessary equipment and staffing are recognised. Criteria used for accreditation can be -having a multidisciplinary team, a chronic disease and preventive health care recall register for their target populations to ensure continuity of care in chronic illnesses and preventive activities are systematically implemented, ability to stabilise and safely refer those presenting with emergencies. Accreditation should then allow a government subsidy for each visit a target population member makes to the accredited primary care practitioner for emergencies, recognised preventive care activities like yearly health checks, antenatal care etc. This system will ensure that the reliance on the government alone to provide primary health care services is removed because that is simply not practical for a country of India’s size. The alternatives to these necessary radical changes will be an unsustainable health care system in India that is also of limited use for those weakest sections of our community most in need of health care. Medical practitioners interested in their own welfare will resist these changes because the current chaotic system benefits doctors’ income. However an elected government concerned with the needs of every member of the nation must act now if later government bankruptcy and suffering of the community is to be prevented.

mfc bulletin/Aug 2010-Jan 2011

79

Open Letter to Hon’ble Prime Minister of India October 19, 2010 Dear Dr. Manmohan Singhji,

Takeover of Indian Pharma Companies by MNCs and the visit of PhRMA Delegation to India on 21st October We the undersigned are writing to you on a matter of vital concern for a large number of people in the country. India is, today, the 4th largest producer of drugs in the world and a world class supplier of relatively cheap generic medicines. It is the largest supplier in the world of low-priced anti-retrovirals and exports medicines to over 200 countries. The pharma sector is a major foreign exchange earner, only next to the IT sector, with a turnover in excess of Rs. one trillion. However, major concerns regarding access to medicines in the country remain. The World Health Organisation (WHO) has reported that about 68 crore people, i.e., 65% of Indians are without access to essential medicines. Instead of building on the excellent manufacturing capacity to make available essential medicines for all, there has been progressive undermining of these gains especially in recent years. All the three major developments which resulted into India becoming the ‘pharmacy of the world’ have now been reversed — the Indian Patents Act of 1970; initiation of manufacture of medicines from the basic stage by Indian public sector companies in the sixties; and the 1978 Drug Policy that imposed several restrictions on the operations of foreign companies while providing preferential treatment to Indian companies. The 1979 Drug Price Control Order (DPCO), that imposed price control over 347 medicines has also been undermined by reducing the number of medicines under price control to only 76. Many in civil society and health professionals continue to express concerns about galloping medicine prices; increase in wasteful, irrational combinations of medicines; and deindustrialisation in the pharmaceutical sector in India.

Takeover of Indian Pharma Companies: India’s Health Security Affected The immediate cause of concern, and the reason for this letter, is the series of takeover of important Indian companies by MNCs and the trend towards domestic companies becoming junior partners of MNCs through tie-ups. Six major acquisitions of Indian companies have taken place in the last 4 years — Ranbaxy, Dabur Pharma, Shanta Biotech, Piramal Health Care, Matrix Lab and Orchid Chemicals, and more are on the anvil. Further, there have been several other tie-ups between MNCs and domestic companies – viz. GSK with Dr Reddy’s; Pfizer with three companies Aurobindo, Strides Arcolab and Claris Life Sciences; Abbot with Cadilla Health Care and Astra Zeneca with Torrent. Recently, a paper circulated by the Department of Industrial Policy and Promotion (DIPP) has pointed out that: “There is a concern that their takeover by multinationals will further orient them away from the Indian market, thus reducing domestic availability of the drugs being produced by them”. The reversal of production trends in the drug market is clear from the fact that of the 10 largest drug companies in India in 1998-99, only one (Glaxo Smith Kline) was a foreign company, while today three of the top ten companies are foreign owned (Ranbaxy, Glaxo Smith Kline and Piramal). The Indian drug industry, built by diligent application of public policy that sought to achieve self reliance in the pharmaceutical sector and availability of medicines at affordable prices, is poised to be handed over to Foreign Multinational corporations. These developments clearly affect India’s health security negatively. It is in this context that we note with concern reports in the media that a delegation from PhRMA (the representative of US based drug companies) is to visit India from October 21, 2010 to meet with different Ministries of the Government of India. We understand that the delegation is particularly interested in discouraging the Indian Government from applying a cap on FDI in the pharma sector and from issuing compulsory licenses for patented medicines. We are strongly of the opinion that an FDI cap on foreign ownership of pharma companies and liberal use of compulsory licenses are two vital avenues open to India to find ways to ensure much wider access to essential medicines to it’s citizens. We demand that the Indian Government be firm in keeping these options available to us in the future in order to strengthen self- reliance and not succumb to pressure by US based companies and the US Government.

80

mfc bulletin/Aug 2010-Jan 2011

We urge that the Government operationalise the following measures to rescue India from the opprobrium of being home to the largest number of people without access to essential medicines: • Scrutinise the spate of acquisition of Indian companies by MNCs and explore whether anticompetitive provisions of our Competition law are being violated • Examine the existence of collusive behavior and abuse of monopoly through intellectual property rights and resultant high prices of patented drugs in India. • Instead of automatic approval, FDI in the pharma sector be routed through Foreign Investment Promotion Board (FIPB) with an imposition of an FDI cap of 40% • Liberal use of the compulsory license (CL) provisions of the Indian Patents Act to secure access to patented medicines and therefore lower costs of medicines • Scrapping of the loan license system and contract manufacturing to discourage big companies from reducing their manufacturing activity • Formulate appropriate policies to incentivise bulk drug manufacture by Indian companies over import of bulk drugs. • Resist attempts to introduce measures such as data exclusivity, or to dilute/remove Section 3d of the Indian Patents Act, especially through bilateral/multilateral FTAs. • Revive Public Sector units through a robust plan and infusion of adequate resources, to ensure production of essential medicines and to provide security in emergency situations arising out of calamities such as war, natural disasters and epidemics. You will appreciate that the concerns we raise are of vital importance to the nation. We seek your personal intervention in the matter. Sd/ All India Drug Action Network, Medico Friend Circle, LOCOST, MIMS India, Sama and several other civil society organizations.

Comments on the DIPP Compulsory Licencing Discussion Paper1 1) We agree with the spirit of the suggestions made in the paper and congratulate the DIPP for the proactive spirit and vision and the concerns shown for patient welfare. 2) CLs are an important instrument of maintaining more access to medicines, India’s health security and self-sufficiency and the robustness of our generic drug industry. The pharma industry cannot be seen as any other industry. The classical ideas of competition and rational markets do not work here– at least most of the time – because of inter alia asymmetry of information, etc. and the consequent market failures. 3) We agree to the main strands of suggestions made in the DIPP paper, on asserting by India the right to CLs, and easing operational modalities thereof, creating policies that make it not so automatic for India’s pharma companies to be taken over and/or sold, and asserting access to price regulation and competition policies. However, we need to point out some collateral factors that are likely to, in a Circulated for Discussion by the Dept of Industrial Policy and Promotion (DIPP), GOI, during Aug 2010 and available at its website. 1

4)

5)

6)

manner of speaking, render this exercise nugatory from the point of view of the enduser/the patient. Some concerns we delineate below: The voluntary licensing (VL) of a drug by the patent holder to Indian drug companies. And terms thereof. If you examine the terms of VL, for example Gilead’s VL to Indian drug companies on Tenofovir, they are extremely restrictive and hence would constitute abuse of monopoly and anticompetitive. This issue needs to be examined. Importation should not be treated as working of the patent over and beyond a period of 2 years. This is in effect an exercise of cross-border patenting and allows a justification for the foreign company to maintain high prices. The DPCO provisions on price regulation of imported drugs are not effective. CLs may be deemed necessary in a wide range of therapeutic class, not just AIDS, cancer, etc. as India’s disease burden is the ‘highest’ in a wide range – for example 60 % of the world’s cardiac patients are in India with similar disease burdens in diabetes, TB, malaria, leprosy (which has been ‘abolished’ after a fashion in India), hypertension and a range of non-communicable diseases.

mfc bulletin/Aug 2010-Jan 2011 7)

Vaccines and sera also need to come under the focus of CLs with a concomitant national vaccine policy. (A draft of the latter arrived at after a recent consultation of a wide range of experts in India is available.) 8) A CL policy without an Essential Drug Policy and a policy that does not allow for patenting and licensing for manufacture only such drugs, is meaningless and will defeat the attempt to keep prices low. 9) Likewise administrative actions of allowing costly and unnecessary drugs for import and marketing in India (without even undergoing clinical trials, etc) is counterproductive to the CL policy discussed in the paper. For instance, since 2003, over 650 “Registration Certificates” have been issued to import finished formulations with the following distinguishable features (thanks to Dr Gulhati, Editor, MIMS India for pointing this out): i. Mandatory clinical trials not conducted; hence safety and efficacy not established. ii. Traders are importing and selling directly to patients on tips from doctors. iii. Prices: free for all. iv. Some Prices of such imported products: v. Remicade: Therapy cost over Rs. 200,000. vi. Herceptin: cost over Rs. 15 lacs every year. vii. Avastin (bevacizumab): cost over Rs. 28,000 for just one vial of 100mg. viii. Neorecormon (epoetin beta): A pack of 30,000 i.u. costs Rs. 9,500. ix. Pegasys (peginterferon alfa 2a) 180mcg costs Rs. 17,500. x. Tarceva (erlotinib) 150mg (10 tablets) is priced at Rs. 36,000. 10) Similarly factors that distort and skew the competition, such as there is in the Indian pharma market, like unethical marketing practices, overpricing of locally manufactured drugs, irrational and unscientific formulations, are inherently harmful to the patient apart from being collusive, indicative of abuse of dominance, etc. and hence (are) anti-competitive. One needs to add that as the case law on competition law is still not developed with the Competition Commission of India itself functioning from May 2009, appropriate administrative actions may need to be taken to bring these issues under the scope of the CCI in the interests of convergence with the issues discussed in the context of CL.

81 11) Indeed, if the government is going the extra mile for the pharma majors in the area of CLs, it needs to give due consideration about a convergence of all related policies that will work in the patient’s benefit. 12) CLs need to be issued only for rational and scientifically approved drugs. 13) Pricing of patented drugs by those taking advantage of CLs and its subsequent manufacture in India needs to be given thought. 14) Price undercutting byforeign companies with ‘deep pockets’ ought not to be a factor in issuing/not issuing CLs. 15) Convergence of a CL friendly regime being advocated in the DIPP paper needs to be in harmony with government policies on data exclusivity, patent linkage, evergreening (that is no dilution of Section 3d of the amended Patents Act), support to Bolar like provisions in the amended Patents Act, and a general no to any TRIPS plus measures in FTAs including the ones currently being negotiated, and unfortunately in secrecy. 16) TRIPS itself is up for review and the government should actively seek the same. After fighting for the Doha agreement, India has shown remarkable reluctance in using the discourse of Doha Agreement in trade negotiations. 17) CL policies also need to go in tandem with R & D and innovation policies in pharma and health sector and a policy for rewarding innovation not just by the mere recognition of patent worthy matter by product patents. However we do agree with the comment made in the discussion paper: “Even without any effects on innovation, compulsory licensing may create significant positive welfare effects on consumers in developing countries as a mechanism to maintain product variety.” 18) India needs to see itself a leader of pharma as also an enabler of pharma manufacturing expertise in other developing countries helping them productionise their CLs when issued in their respective countries. 19) Also strategic linking with say BRIC countries or any other suitable bloc of countries to strengthen long-term access to medicines for the poor world wide needs to be examined. India needs to take the initiative – if we do not we will not have an Indian generic drug industry to talk of in the next 10 years. From: S. Srinivasan on behalf of LOCOST, Baroda; AllIndia Drug Action Network, and Medico Friend Circle

82

mfc bulletin/Aug 2010-Jan 2011

Pentavalent and other New Combination Vaccines: Solutions in Search of Problems - Y. Madhavi & N. Raghuram1 This is the unedited version of the article published in the Indian Journal of Medical Research (IJMR), Oct 2010, 132:15-16. The pentavalent vaccine and many other combination vaccines waiting to enter Universal immunization Programme (UIP) have brought into sharp focus the gaping gap between lofty slogans of ‘evidence based medicine’ and the actual dynamics that drive policy on the ground1-4. Notwithstanding the theatrics of the ‘experts’ of the World Health Organization (WHO) and the Global Alliance for Vaccines and Immunization (GAVI) globally and National Technical Advisory Group on Immunization (NTAGI) here in India, it is becoming increasingly obvious that the pentavalent vaccine, like many other recent combination vaccines, is a solution searching for problems.

Whither Evidence-Based Medicine? The fact of the matter is that there is no scientifically valid evidence of a high enough disease burden due to Influenzae type b (Hib) or Hepatitis-B (HepB) that justifies universal vaccination in India5-7. Indeed, every attempt to find such evidence for HiB in India and elsewhere in Asia has failed4. In the absence of evidence for individual vaccines, it defies logic how one can justify combining them into a pentavalent vaccine. It also begs the question as to whether the industry made these combination vaccines in response to specific public health demands, if so who articulated them and with what evidence from which countries. It seems that there was no need for any such evidence, as long as ‘expert’ recommendations behind closed doors were unquestioningly accepted by all concerned. Unfortunately, increasing awareness and rising dissent against medicines-sans-evidence is forcing the policy makers to find post-facto evidence that is becoming increasingly difficult to manufacture. By now, it is obvious to all concerned, except to the determined ‘experts’ who drive our immunization policies, that there has never really been a real public health demand for manyof these new vaccines, let alone their combinations.

Marketing Tricks or Innovations for Health? Indeed, combination vaccines were invented precisely to overcome the poor penetration of the individual vaccines in the global market, as well as to overcome the expiry of their patents and establish eternal market

monopolies. Scientific evidences indicate that combination vaccines bring no new health benefit to the immunized people8-11, except the convenience of not having to take each vaccine separately, provided all those vaccines are actually needed. The issue of safety and efficacy of combination vaccines were often cause for concern12. For instance, MMR in combination with Varicella vaccine reported to have enhanced febrile seizures in children13-14, and Hepatitis A vaccine is not protective enough when combined with typhoid vaccine15. It is a marketing trick, which is no more scientific than the logic behind the bundling of Television channels or online journals. Just as many not-so-popular channels or journals need a piggy back ride on a popular channel or a journal in a bundle, every dubious new vaccine needs a Diphtheria Tetanus Pertussis (DTP), measles or some other essential vaccine to get a back door entry into the Universal Immunization Programme (UIP)16. Pushing Hib,Hep-B , Mumps Measles Rubella (MMR), rotaviral, Human Papilloma Virus (HPV), etc through combination vaccines among people who don’t need them (using UIP vaccines as piggybacks) is no better than beaming religious channels using news channels as piggybacks. It is also obvious why our ideologues of out-of-pocket financing of the increasingly privatized health industry suddenly need centralized government procurement of vaccines and are no longer content with doctoring customer ‘choice’. The sustainability of global (read Multinational Company) vaccine industry depends on adoption of new vaccines into the national immunization of large countries like India17-18, because the present prices make them unaffordable even in relatively affluent country markets. A more honest and straightforward way would be to recommend Indian government aid to support vaccination of needy children in such countries, rather than giving Indian children unnecessary vaccines to bring down global prices. But it is hard for the rich to accept donations from the poor, when they are so used to robbing them in benevolent style. So much for equity!

Equity for Health or Market? Why is it that ‘equity’argument is often given only when it comes to government spending on vaccines? Why not for all other health care services or other basic amenities such as food, shelter, water and clean environment, which are ruled by market forces? Why are health concerns so

1 National Institute of Science,Technology and Development Studies (NISTADS), New Delhi & School of Biotechnology, Indraprastha University, Delhi, respectively. For correspondence <y_madhavi01@hotmail.com>

mfc bulletin/Aug 2010-Jan 2011 muted when it comes to OPV induced paralytic cases? Is the government. or NTAGI willing to take responsibility and compensate for vaccine induced paralytic cases? Why don’t we have proper vaccine injury compensation in this country? Why should our immunization experts enjoy so much immunity from the unhealthy consequences of their advice for health? In any case, the hollowness of the ‘equity’ argument becomes obvious when we consider that the total coverage of ‘universal’ immunization is below 50% of the children in India, even for the most essential and affordable vaccines. If you don’t have bread, eat cake!

Public Sector Abuse for Medicine sans Evidence? Another side of the equityargument is that manufacturing these combination vaccines in public sector units (PSUs) would bring down their prices and make them more affordable to all. This would have been a welcome move (lest we too be branded as anti-vaccine), provided the public health need for these new vaccines is firmly established. Unfortunately, even well meaning minds in the government committed to reviving the crucial role of PSUs in Indian vaccine security seem to be lost in supply side arguments without firmly establishing the demand for these vaccines based on disease burden. This is inspite of having all the human, financial and technological resources to document disease burden scientifically. This is the fundamental tragedy of medicine-sans-evidence policy that rules in Indian vaccines. References 1. Kimman TG, Boot HJ, Berbers Guy AM, Vermeer-de Bondt PE, G Ardine de Wit, and Hester E de MelKer. Developing a vaccination Evaluation model to support evidencebased decision making on national immunization programs, Vaccine, 2006, 24(22): 4769-4778. 2. Madhavi Y., Puliyel J.M., Mathew J.L, Raghuram, N., Phadke, A., Shiva, M., Srinivasan, S., Paul, Y., Srivastava, R.N., Parthasarathy, A., Gupta, S., Ranga, U., Vijayalakshmi V., Joshi, N., Nath, I., Gulhati, C.M., Chatterjee, P., Jain, A., Priya, R., Dasgupta, R., Sridhar, S., Dabade, D., Gopakumar, K.M., Abrol, D., Santhosh, M.R., Srivastava, S., Visalakshi, S., Bhargava A., Sarojini, N.B., Sehgal, D., Selvaraj, S., Banerji, D. (2010) Evidencebased national vaccine policy, Indian J Med Res, May 2010,131: 617-628. 3. Mudur G. ‘Antivaccine lobby resists introduction of Hib vaccine in India, BMJ 2010; 340: c3508 4. Lone Z and Puliyel J. ‘Introducing pentavalent vaccines in the EPI in India: A counsel for caution, Ind J Med Res, 2010, 132: 1-3. 5. Gupta N, Puliyel J. vaccine Introduction where incidence of Hib meningitis is 0,007%. Deision-making based on heath economics or ideology, Ind J Med Res, 2009, 129: 339-40.

83 6. Beri, R. S. and Ojha R K. Is Hib vaccination required at all in India? Ind Pediat, 2002, 39: 1067–1068. 7. Madhavi Y The Manufacture of consent? Hepatitis B vaccination, A special article, Economic and Political Weekly (EPW), 2003, 38 (24): 2417-2424. 8. Food and Drug Administration (FDA). Guidance for Industry for the Evaluation of Combination Vaccines for Preventable Diseases: Production, Testing and Clinical Studies. Washington DC: US Dept of Health and Human Services, Food and Drug Administration, Center for Biologics Evaluation and Research, April 1997, Docket No. 97N-0029. 9. American Academy of Pediatrics (AAP), Combination Vaccines for Childhood Immunization: Recommendations of the Advisory Committee on Immunization Practices (ACIP), the American Academy of Pediatrics (AAP), and the American Academy of Family Physicians (AAFP) Committee on Infectious Diseases, 1998-1999. Pediatr, 1999, 103: 1064–1077. 10. Bar-On ES, Goldberg E, Fraser A, Vidal L, Hellmann S, Leibovici L. Combined DTP-HBV-HIB vaccine versus separately administered DTP-HBV and HIB vaccines for primary prevention of diphtheria, tetanus, pertussis, hepatitis B and Haemophilus influenzae B (HIB) (Review), The Cochrane Library 2009, Issue 3, p.1-57. 11. Buttery JP, Anna R, McVernon J, Chantler T, Lane L, Jane Bowen-Morris J, Diggle L, Morris R, Harnden A, Lockhart S, Pollard AJ, Cartwright K, Moxon ER. Immunogenicity and Safety of a Combination Pneumococcal-Meningococcal Vaccine in Infants, JAMA, 2005, 293:1751-1758. 12. Chen R T, Vitali Pool V, Takahashi H, Bruce G. Weniger and Patel B. Combination vaccines: post licensuer safety evaluation, Clin infect dis, Dec 15, 2001), 33: s327-s333. 13. White C. Jo, Stinson D, Staehle B, Cho Iksung, Matthews H, Ngai A, Keller P, Eiden J, Kuter B and The MMRV Vaccine Study Group. Measles, mums, rubella, and varicella combination vaccine: safety and immunogenecity alone and in combination with other vaccines given to children. Clinical Infect Dis J 1997; 24: 925-31. 14. Klein NP, Fireman B, Katherine W Yih, Lewis E, Kulldorff M, Ray P, Baxter R, Hambidge S, Nordin J, Naleway A, Belongia EA, Lieu T, Baggs J and Weintraub E. MeaslesMumps-Rubella-Varicella Combination Vaccine and the Risk of Febrile Seizures, Pediatr, July 2010; 126(1): e18. 15. Beeching NJ, Clarke PD, Kitchin NRE, Pirmohamed J, Veitch K and Weber F. Comparison of two combined vaccines against typhoid fever and hepatitis A in healthy adults. Vaccine, 2004, 23 (1): 29-35. 16. Madhavi Y. New combination vaccines: Backdoor entry into India’s Universal Immunization Programme? Cur Sci, 2006; 90 : 1465-9. 17. Nossal G. Living up to the legacy Nature Med May;4(5 Suppl):475-6. 18. Sharma S. Immunisation boost as vaccine price falls, Nov 2009, Hindusthan Times, Daily newspaper, New Delhi.( http://www.hindustantimes.com/Immunisation-boost-asvaccine-price-falls/Article1-477888.aspx) (Accessed on 2nd Aug 2010)

84

mfc bulletin/Aug 2010-Jan 2011

Dr Babu versus the IMA: Fighting Endorsements by Doctors Sequence of Events April 2008: IMA signs Pepsico endorsement deal for Rs 2.25-crore with Pepsico to allow Tropicana juice and Quaker oats to use its logo on their packs for three years. June 6, 2008:| Dr KV Babu files complaint with MCI June 30, 2008: MCI discusses the issue - agenda 27 Aug 6, 2008: MCI sentds notice to IMA and reminders on 07/05/2009, 19/06/2009, and , 03/07/2009. July 10, 2009: Approaches GOI to take action against MCI under relevant sections of IMC Act 1956. July 20, 2009: MCI decides they cannot act against IMA. July 29, 2009: Dr Babu approaches CIC under RTI Act July29, 2009: GOI in a letter to MCI asks them to take up the issue again. Dr Babu approaches CIC since no reply is coming from MCI on the question whether endorsement by medical organisations unethical. CIC directs MCI to give a time bound reply by Dec 31, 2009. Order of Annapurna Dixit dt Nov 10, 2009 Nov 2009: MCI seeks legal opinion - Nov 2009. Nov 30, 2009: IMA CWC at Nagpur decides to stop all endorsements in future. But will continue the already signed contracts till 2012, till the contract period is over. Dec 10, 2009: MCI clarifies in the Amendment to Code of Ethics that Medical Associations are under the Jurisdiction of MCI March 3, 2010: Still MCI ethics committee decides again, surprisingly on 03/03/2010 that IMA not under the jurisdiction of MCI - based on an outdated legal opinion of 01/12/2009 (received before the amendment in Code of Ethics dt Dec 10, 2009). April 1, 2010: Complaint to MCI ethics committee to take action against IMA leaders suomoto based on the amendments in Code of Ethics 6.8 since IMA is continuing the endorsements. June 30, 2010: Dr Babu tells NHRC that scientific studies had shown that packaged fruit juices, if consumed beyond a certain limit, were harmful to the health of children. “That is a human rights issue and needs the intervention of the NHRC.” NHRC directs MOHFW Secretary to take appropriate action on Dr Babu’s

complaint against IMA.1 July 17, 2010: Reply from P Prasannaraj, MCI Secretary that IMA is not under the jurisdiction of MCI with an assurance that action will be taken against individual doctors if there is specific complaint. Aug 14, 2010: Complaint filed against 187 CWC members who attended Puri CWC meeting to endorse Pepsi. Aug 18, 2010: MCI declares IMA is under the jurisdiction of MCI and sends show-cause to IMA and the doctors to reply by Sep 1, 2010. and served a show cause notice “for endorsing the product in violation of provision of Indian Medical Council (Professional Conduct, Etiquette and Ethics) Regulations, 2002”. Aug 30, 2010: IMA replies arguing that it had not endorsed the products, but only entered into an MoU with Pepsico for a “nutritional awareness programme”.2 Nov 1, 2010: Dr Roy Chowdhary declares IMA’s endorsements are unethical and should stop immediately. Penal action, if any, will be decided on Nov 9, 2010. Nov 19, 2010: Ghulam Nabhi Azad, MOHFW Minister, declares in the Parliament that MCI has decided to remove the name of IMA national President and Secretary for a period of six months and also decides to send censure letters to all 187 IMA executive committee members “to not repeat such practices in future”. This is the first time in the history of the IMA, which represents two lakh doctors in the country, that the names of its office-bearers would be removed from the register. November 23, 2010: Board of Governors of MCI ratifies the decision of ethics committee decision. In addition to penal action against IMA President and Secretary asks IMA to scrap endorsement of MoU. November 29, 2010: Dr Dharamprakash, Secretary, IMA challenges MCI decision and obtains stay from Delhi HC. Matter listed for February 22, 2011. Dec 6, 2010: Show-cause notices to 61 IMA members Dec 21, 2010: IMA obtains a court stay on the November 9 MCI order. Source: Dr Babu KV

1 Dr Babu had complained to the NHRC that the fruit juice was harmful to children and hence the IMA should be ordered to withdraw its endorsement. He had pointed out that the IMA would receive a huge sum of money from the company for the endorsement. The endorsement was, according to him, not only illegal but also was against professional ethics of a medical body. The company was using the IMA logo on the label of the products which, he said, would mislead the public as they would treat such endorsement as scientific verdict given by the medical body. He said the endorsement violated the provisions of the Food Adulteration Act. Dr. Babu had presented before the NHRC the IMA’s documents which referred to the endorsements and showed the estimated economic benefits. 2 In a similar case in 1988, the American Medical Association (AMA) had to pay $9.9 million (Rs 45 crore) to withdraw from a contract it signed with Sunbeam Corporation. While that was an endorsement of medical equipment, the IMA became the first professional body of doctors in the world to endorse a food product.

mfc bulletin/Aug 2010-Jan 2011

85

RTI Act Comes to Doctor’s Aid K.P.M. Basheer Drive against medical association’s Pepsi endorsement; MCI step against IMA for breaking ethics code; IMA says it will not endorse products in future KOCHI: A Kerala doctor’s campaign, using the weapon of Right to Information Act, against an unethical practice by the Indian Medical Association (IMA) has turned triumphant with the Medical Council of India (MCI) taking steps against the IMA for endorsing commercial products. Last week the MCI served a show-cause notice on the IMA for violating its code of ethics by endorsing Pepsi’s Tropicana fruit juice and Quaker oats which brought a mealy Rs.2.25 crore to the largest professional body of doctors in India. The step followed a decision by the MCI’s Ethics Committee which met last week. The IMA has been asked to send in a reply by September 1. Dharam Prakash, IMA’s secretary-general, told The Hindu over the phone that the association would soon reply to the notice and that it had already decided not to endorse any products in the future. He, however, said that the Pepsi endorsements would run through March next because of a contractual obligation. One-Man Campaign The MCI’s hand was forced by a relentless one-man campaign by K.V. Babu, an ophthalmologist of Payyannur in Kannur district over the past two years. Irked by the unethical endorsement, which ‘clearly was for easy monetary gains,’ Dr. Babu, himself a member of the IMA’s central committee, shot off letter after letter to the IMA asking it to cancel it. He pointed out that under the Indian Medical Council (Professional Conduct, Etiquette and Ethics Regulations) 2002, endorsement of commercial products was an unethical act. (Section 6.1.1. says: “Soliciting of patients directly or indirectly, by a physician, by a group of physicians or by institutions or organizations is unethical.”) He moved the MCI, the statutory body authorised to

regulate the medical profession, pressing it to get the endorsement cancelled. He petitioned the Union Ministry of Health and Family Welfare asking it to force the MCI to take action. He also moved the National Human Rights Commission. Obstacles Officials in the MCI and Health Ministry as well as the IMA stonewalled initially. But Dr. Babu wouldn’t leave it at that. He extensively used the RTI Act to source relevant documents. He marshalled a plethora of information through RTI requests, which helped him argue his case effectively. In December 2009, the MCI made amendments to its Code of Ethics. Section 6.8 was added to regulate the “code of conduct for doctors and professional associations of doctors in their relationship with pharmaceutical and allied health sector industry.” Dr. Babu was quick to point out that the IMA came under the definition of ‘professional associations of doctors.’ Meanwhile, a few months ago, the Central government sacked the MCI following corruption allegations against its then president Ketan Desai, who was later arrested by the Central Bureau of Investigation. The reconstituted MCI took a lenient view of Dr. Babu’s cause. It took the stand that the code of ethics applied to both individual doctors and their associations. Reacting to Dr. Prakash’s stand that the Pepsi endorsement cannot be withdrawn before it ran its full course, Dr. Babu wanted the IMA to cancel the endorsement at once. “How can an act, once it has been proved to be unethical, be allowed to continue for a few more months?” He wants the MCI to take ‘appropriate and exemplary action’ against all the 187 doctors who attended the April 12-13, 2008 meeting of the IMA’s central committee held at Puri, Orissa. This committee had backed the IMA decision to endorse the Pepsi products. Source: The Hindu, Aug 30, 2010. http://www.hindu.com/2010/ 08/30/stories/2010083055970900.htm

86

mfc bulletin/Aug 2010-Jan 2011

Letter to the Editor Article on Study of Private Hospitals Dear all, For over a decade, in Maharashtra, we have been advocating the inclusion of Standard Charter of Patients’ Rights in the Bombay Nursing Home Registration Act (BNHRA) as one of the measures towards self regulation and social regulation of the Private Health Sector. In this connection, recently I went through mfc bulletin article of Padma Deosthali and Ritu Khatri once again (‘Violating the Law, Yet Thriving: Study of Private Hospitals in Maharashtra’, Medico Friend Circle Bulletin, No. 339, February-March 2010). It summarises the CEHAT study ‘Health Care In The Private Health Sector: A Study of Private Hospitals in Maharashtra. This study updates the 1997 CEHAT study on the same subject and is an important one because of the Maharashtra State wide representative sample. The Standard Charter of Patients’ Rights mentioned in this article is from the draft rules under BNHRA that were approved by the Health Dept. in Maharashtra and were put on their web site for three years from July2006, awaiting in vain, for the final approval of Maharashtra’s Health Minister. This Standard Charter is reproduced verbatim in the box below. It will be seen that the CEHAT study does not cover the following points from this Charter: a. Right to relevant information about the nature, cause, likely outcome of the present illness, etc. etc. b. Right to access to his / her clinical records during admission c. Informed consent before anesthesia, blood transfusions etc. d. Right to seek second opinion. e. Compliance with national ( ICMR) and international guidelines There is a need to study these issues also. Anant Phadke SECTION 16, Rule 14

Standard Charter of Patients’ Rights 1) No person suffering from HIV may be denied care only on the basis of the HIV status, provided the curative or diagnostic care is available at the NH. Not having a Voluntary Testing and Counseling Centre cannot become grounds to refuse care. For management of patients who is HIV positive, the nursing home would follow guidelines circulated from time to time by NACO (National AIDS Control Organization) 2) Every nursing home shall maintain an inspection book and a complaint register (for the patients party), which shall be produced before the LSA as and when required. 3) All nursing homes must adopt a Standard Charter of Patient’s Rights, observe it and orient their staff for the same. This Standard Charter of Patient’s Rights would include that – A) The patients and / or Person authorized by patient should receive: • The relevant information about the nature, cause, likely outcome of the present illness. • The relevant information about the proposed care, the expected results, possible and the expected costs complications. Patient and All nursing homes must adopt a Standard Charter of Patient’s Rights, observe it and orient their staff for the same. This Standard Charter of Patient’s Rights would include that – B ) Patient and/ or person authorized by patient has a right to have • An access to his / her clinical records at all times during admission to NH • Photocopy should be available within 24 hrs when admitted to NH or within 72 hrs of making an application after discharge or death after paying fees for photocopy. • A discharge summary at the time of discharge, which should contain * The reasons for admission, significant clinical findings and results of investigations, diagnosis, treatment and the patient’s condition at the time of discharge. * Follow-up advice, medication and other instructions and

when and how to obtain urgent care when needed in an easily understandable manner. In case of death, the summary of the case should also include the cause of death. C) Treating patient information as confidential. D) Patient has a right to personal dignity and privacy during examination, Procedures and treatment. E) Patient and family rights include informed consent before anesthesia, blood and blood product transfusions and any invasive / high risk procedures / treatment. Informed consent includes information in a language and manner that the patient can understand, on risks, benefits, alternatives if any and as to who will perform the requisite procedure. Information and consent before any research protocol is initiated (see below). F) Patient and family rights include information on how to voice a complaint. Appropriate procedure for grievance redressal must be put in place by the hospital. G) Rights of women as patients • Privacy during examination. In case of examination by male doctor, a female attendant must be present. • Right to confidentiality of reports and information not to be disclosed to any person other than one who is authorized by the patient. • Confidentiality of HIV positive patients H) Patient has the right to seek second opinion. All medical and diagnostic reports must be made available to the patient or authorized person to facilitate second opinion. I) Non-discrimination on the basis of HIV status • Patients and families should be informed about the above rights in a format and language, that they can understand • Patients and family are informed about the financial implications when there is a change in the patient condition or treatment setting. J) In case of Nursing Homes undertaking clinical research• Documented policies and procedures should guide all research activities in compliance with national (ICMR) and international guidelines

mfc bulletin/Aug 2010-Jan 2011

87

UID: Facility or Calamity? - Jean Drèze Quite likely, someone will be knocking at your door a few weeks from now and asking for your fingerprints. If you agree, your fingerprints will enter a national database, along with personal characteristics (age, sex, occupation, and so on) that have already been collected from you, unless you were missed in the “Census household listing” earlier this year. The purpose of this exercise is to build the National Population Register (NPR). In due course, your UID (Unique Identity Number, or “Aadhaar”) will be added to it. This will make it possible to link the NPR with other Aadhaar-enabled databases, from tax returns to bank records and SIM registers. This includes the Home Ministry’s NATGRID, smoothly linking 21 national databases. For intelligence agencies, this is a dream. Imagine, everyone’s fingerprints at the click of a mouse, that too with demographic information and all the rest! Should any suspicious person book a flight, or use a cybercafé, or any of the services that will soon require an Aadhaar number, she will be on their radar. If, say, Arundhati Roy makes another trip to Dantewada, she will be picked up on arrival like a ripe plum. Fantastic! So, when the Unique Identification Authority of India (UIDAI) tells us that the UID data (the “Central Identities Data Repository”) will be safe and confidential, it is a half-truth. The confidentiality of the Repository itself is not a minor issue, considering that UIDAI can authorize “any entity” to maintain it, and that it can be accessed not only by intelligence agencies but also by any Ministry. But more importantly, the UID will help to integrate vast amounts of personal data, available to government agencies with few restrictions. Confidentiality is not the only half-truth propagated by UIDAI. Another one is that Aadhaar is not compulsory – it is just a voluntary “facility”. UIDAI’s concept note stresses that “enrolment will not be mandated”. But there is a catch: “... benefits and services that are linked to the UID will ensure demand for the number.” This is like selling bottled water in a village after poisoning the well, and claiming that people are buying water voluntarily. The next sentence is also ominous: “This will not, however, preclude governments or Registrars from mandating enrolment.” That UID is, in effect, going to be compulsory is clear from many other documents. For instance, the Planning Commission proposal for the National Food Security Act argues for “mandatory use of UID numbers which are expected to become operational by the end of 2010” (don’t miss the optimistic time frame). No UID, no food! Similarly, UIDAI’s concept note on NREGA assumes that “each citizen needs to provide his UID before claiming employment”. Thus, Aadhaar will also be a condition for the right to work - so much for its voluntary nature. Now, if the UID is compulsory, then everyone should have a right to free, convenient and reliable enrolment. The enrolment process, however, is all set to be a hit-or-miss affair, with no guarantee of timely and hassle-free inclusion. UIDAI hopes to enrol about half of India’s population in the next four years – what about the other half? Nor is there any guarantee of reliability. Anyone familiar with the way things work in rural India would expect the UID database to be full of errors. There is a sobering lesson here from the BPL Census. A recent World Bank study found rampant anomalies in the BPL list: “A common problem was erroneous information entered for household members. In one district of Rajasthan, more than 50 per cent of the household members were listed as sisters-in-law.” Will the UID database be more reliable? Don’t bet on it. And it is not clear how the errors will be corrected as they emerge. Under the proposed National Identification Authority of India Bill (“NIDAI Bill”), if someone finds that her “identity information” is wrong, she

is supposed to “request the Authority” to correct it, upon which the Authority “may, if it is satisfied, make such alteration as may be required”. There is a legal obligation to alert the Authority, but no right to correction! The Aadhaar juggernaut is rolling on regardless (without any legal safeguards in place), fuelled by mesmerizing claims about the social applications of UID. A prime example is UID’s invasion of the National Rural Employment Guarantee Act. NREGA workers are barely recovering from the chaotic rush to bank payments of wages. Aadhaar is likely to be the next ordeal. The local administration is going to be hijacked by enrolment drives. NREGA works or payments will come to a standstill where workers are waiting for their Aadhaar number. Others will be the victim of unreliable technology, inadequate IT facilities, or data errors. And for what? Gradual, people-friendly introduction of innovative technologies would serve NREGA better than the UID tamasha. The real game plan, for social policy, seems to be a massive transition to “conditional cash transfers” (CCTs). There is more than a hint of this “revolutionary” (sic) plan in Nandan Nilekani’s book Imagining India. Since then, CCTs have become the rage in policy circles. A recent Planning Commission document argues that successful CCTs require “a biometric identification system”, made possible by “the initiation of a Unique Identification System (UID) for the entire population…”. The same document recommends a string of mega CCTs, including cash transfers to replace the Public Distribution System. If the backroom boys have their way, India’s public services as we know them will soon be history, and every citizen will just have a Smart Card - food stamps, health insurance, school vouchers, conditional maternity entitlements and all that rolled into one. This approach may or may not work (that’s incidental), but business at least will prosper. As Wall Street Journal says about Rashtriya Swasthya Bhima Yojana (a pioneer CCT project, for health insurance), “the plan presents a way for insurance companies to market themselves and develop brand awareness.” The biggest danger of UID, however, lies in a restriction of civil liberties. As one observer aptly put it, Aadhaar is creating “the infrastructure of authoritarianism” – an unprecedented degree of state surveillance (and potential control) of the citizens. This infrastructure may or may not be used for sinister designs. But can we take a chance, in a country where state agencies have such an awful record of arbitrariness, brutality and impunity? Mr. Nilekani is no Big Brother, but the real toughies are not far behind – like Captain Raghu Raman (of Mahindra Special Services Group), who is quietly building NATGRID for the Home Ministry. Raman believes that growing inequality is a “powder keg waiting for a spark”, and advocates corporate takeover of internal security (including a “private territorial army”), to enable the “commercial czars” to “protect their empires”. Even Mr. Nilekani is not beyond a little compromise with thought control. His propaganda blitz, orchestrated by PR firms, would be the envy of Baba Ramdev. And criticisms are not taken kindly. Within hours of an enlightening critique of UID (written by Dr. Reetika Khera) appearing in a leading national daily, on 30 August 2010, Nilekani called the editors. Is it an accident that two editorials supportive of UID appeared in the same newspaper within two days? There are equally troubling questions about the “NIDAI Bill”, starting with why it was drafted by UIDAI itself. Not surprisingly, the draft Bill gives enormous powers to UIDAI’s successor, the NIDAI, with minimal safeguards. To illustrate, the Bill empowers NIDAI to decide the biometric and demographic information required for an Aadhaar

88

mfc bulletin/Aug 2010-Jan 2011

number (Section 23); “specify the usage and applicability of the aadhaar number for delivery of various benefits and services” (Section 23); authorize whoever it wishes to “maintain the Central Identities Data Repository” (Section 7) or even exercise any of its own “powers and functions” (Section 51); and dictate all the relevant “regulations” (Section 54). Ordinary citizens, for their part, are powerless: they have no right to a UID number except on the NIDAI’s terms, no right to correction of inaccurate data, and – last but not least - no specific

means of grievance redressal. In fact, believe it or not, the Bill states (Section 46) that “no court shall take cognizance of any offence punishable under this Act” except based on a complaint authorized by NIDAI! So, is UID a facility or a calamity? It depends for whom. For intelligence agencies, the corporate sector, and NIDAI, it will be a facility and a blessing. For ordinary citizens, especially the poor and marginalized, it could well be a calamity.

Contents Towards Universal Access to Health Care in India

- Abhay Shukla, Anant Phadke, Rakhal Gaitonde Human Faces of Universal Health Care - Dhruv Mankad Organizing the Provisioning for a Universal Healthcare System - Ravi Duggal Financing the Universal Access Health Care System - Ravi Duggal Contemplating UHC for Nashik - Shyam Ashtekar Universal Health Coverage in Thailand - Renu Khanna Understanding the Canadian, Thai and Brazilian Universal Healthcare Systems - Kerry Scott Healthcare in China - Dhruv Mankad Brazil: A System for Universal Access to Health Care - Abhay Shukla What it Costs to Provide Medicines to All Sick Persons in India - Narendra Gupta Towards a Fair Effective and Sustainable Health Care System - Rajkumar Ramasamy Open Letter to Hon’ble Prime Minister of India Comments on the DIPP Compulsory Licencing Discussion Paper Pentavalent and other New Combination Vaccines - Y. Madhavi & N. Raghuram Dr Babu versus the IMA UID: Facility or Calamity? - Jean Drèze Subscription Rates

Annaul

Rs. Indv. 200

Inst.

U.S$ Asia

Rest of world

400

20

15

Life 1000 2000 100 200 The Medico Friend Circle bulletin is the official publication of the MFC, Both the organisation and the Bulletin are funded solely through membership/subscription fees and individual donations. Cheques/money orders/DDs payable at Pune, to be sent in favour of Medico Friend

1 14 16 19 24 34 43 57 64 69 74 79 80 82 84 87

Circle, addressed to Manisha Gupte, 11 Archana Apartments, 163 Solapur Road, Hadapsar, Pune - 411028. (Please add Rs.15/- for outstation cheques). email: masum@vsnl.com MFC Convener Convening Group: Premdas, Sukanya & Rakhal Contact Person: Rakhal - +91 9940246089 c/o Community Health Cell, No.31 Prakasam Street, T.Nagar, Chennai 600 017 Email: <rakhal.gaitonde@gmail.co> MFC Website: <http://www.mfcindia.org>

Editorial Committee: Anant Bhan, Dhruv Mankad, Mira Sadagopal, C. Sathyamala, Sukanya, Sathyashree, 'Chinu' Srinivasan. Editorial Office: c/o. LOCOST, 1st Floor, Premananda Sahitya Bhavan, Dandia Bazar, Vadodara 390 001. Email: sahajbrc@gmail.com. Ph: 0265 234 0223/233 3438. Edited & Published by: S. Srinivasan for Medico Friend Circle, 11 Archana Apartments, 163 Solapur Road, Hadapsar, Pune 411 028. Views and opinions expressed in the bulletin are those of the authors and not necessarily of the MFC, Manuscripts may be sent by email or by post to the Editor at the Editorial Office address.

MEDICO FRIEND CIRCLE BULLETIN PRINTED MATTER - PERIODICAL Registration Number : R.N. 27565/76 If Undelivered, Return to Editor, c/o. LOCOST, 1st Floor, Premananda Sahitya Bhavan Dandia Bazar, Vadodara 390 001


