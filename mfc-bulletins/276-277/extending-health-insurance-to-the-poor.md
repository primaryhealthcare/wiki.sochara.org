---
title: "Extending Health Insurance to the Poor"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Extending Health Insurance to the Poor from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC276-277.pdf](http://www.mfcindia.org/mfcpdfs/MFC276-277.pdf)*

Title: Extending Health Insurance to the Poor

Authors: Anil Gumber

medico friend circle bulletin 276-277 Sept-Oct 2000

Editorial Universal Access to Health Care through Social Insurance It is becoming increasingly apparent that the right to health care can be effectively achieved only through some means of insurance. In a situation where the direct provision of free or subsidised services by the State is negligible and people are accustomed to purchasing health care from an unregulated market sector, what does evolving a system of social insurance entail? A social insurance-based system basically involves separation of the point of payment from the point of service provision. It means that risks are shared among a larger group so that those most vulnerable are guaranteed care even while the least vulnerable are able to access health care at a lower cost. Although social insurance does not necessarily ensure that everyone has equal access to care, it does bring into the fold those who are entirely left out of it in the present system. However, any system of social insurance must tackle certain serious problems. The class stratification within the health system has resulted in the creation of two parallel systems; one a system that relies on unqualified providers, unregistered institutions and lower standards of care and the other, which is actually recognisable as a health care system. Those who rely on the former system differ, no doubt, in class, but also in terms of their health needs, priorities and preferences. The experience of the for profit insurance sector, which survives by excluding everyone except the urban middle and. upper class, is hardly adequate to understand there needs. It is with the objective of exploring alternatives, which are ideologically different and also more efficient, that the next theme meet of the MFC has been planned. With this issue of the bulletin, we begin the publication of background papers for the meet. This issue includes two background papers, which evaluate insurance from the point of view of two marginalised groups the rural poor and self employed women in the informal sector.

Shyam Ashtekar presents a framework for a rural health insurance system, while Anil Gumber's paper reviews the experience of a scheme already in existence. Both papers raise important questions that must be answered before contemplating universal health insurance. Can universal insurance work in the existing situation where the overwhelming majority of health providers are concentrated in the urban areas and rural patients have almost no access to trained providers? How feasible is it, politically, to orchestrate a geographical reorganisation of doctors (who are both socially and politically powerfully as a group)? What are the means by' which local organisations can effectively monitor the provision of services? The next paper highlights the fact that there are areas of concern, especially for women, which may be difficult to encompass in a insurance-based health service. .Among women's major health needs are maternal care services and treatment of chronic problems. Both are fairly predictable events and, thus, classically, not suited for insurance coverage (which concentrates on sudden and overwhelms events such as hospitalisation following illness and accidents). Most existing schemes exclude maternity care and ambulatory care. While hospitalisation expenses are large and can be devastating in the short run, the bulk of household expenditure on women is on maternal care and ambulatory care, a slow but steady drain on household resources. Among the most common health needs reported by women are those related to pain relief in apparently incurable problems affecting bones and joints? Typically, women resort to multiple providers and multiple treatment regimens (including ad hoc self-education, traditional medicine, etc.) How does one devise an insurance system that has the flexibility to include such varied health providers?

With these two papers, we begin the critical examination of health insurance with a view that insurance is not just an alternative mechanism for financing the existing health system; it also involves the restructuring of health services and responding to the neglected health care needs of the community. - Neha Madhiwalla

Towards universal health insurance: a rural perspective Shyam Ashtekar I can see them as they approach our hospital. The man is carrying a boy on his back, as a woman - presumably the mother - trails behind. When they reach me, the boy is writhing in pain. It turns out that he broke his left leg falling from a cane-loaded tractor. He will need a fixation under anaesthesia. The doctor - a homeopath-s- they consulted in this bazaar town sent them here for an X-ray. He could not even perform first aid with a simple splint. For the fixation, he will call in a private surgeon from Nasik and charge the family up to Rs 10,000. They ignore my suggestion that they are better of/going to the district hospital. The parents' guilt about the child's pain will lead them to even sell themselves to pay for this treatment.

Let us take stock of the realities and the dream.

Our rural health services: doctors, health facilities and villages Do we have enough qualified doctors in the villages, and are they reasonably accessible to the people? In 1999-2000, we conducted a study in five rural blocks of Nasik' Eighty four per cent of the villages had no resident medical service. A similar proportion of private health care providers were not formally trained in allopathy - though all of them practiced allopathy. These are the realities we must keep in mind when talking about universal health insurance. What should be done about the uneven distribution of services and the domination of untrained practitioners?

Imagine if universal health insurance were available. The boy would have been taken to the village doctor who is trained in basic care. This doctor would place the fractured leg in a splint (as I did with the help of a medicine carton) and send the boy to a rural hospital where doctors could fix it, calling in experts from the district hospital if need be. All this would be done at a cost that even the poor family could afford. The village doctor charges a nominal amount — or nothing at all, if first aid costs are spread to other services. The entire service is inexpensive, and rational medical treatment is provided at well-equipped centres. The family's only responsibility would be to take the boy to the village doctor and then to the referral center. If they are above the poverty line, they would have to pay a small annual premium for this service. That is what universal health insurance should mean.

Any health system must meet a number of requirements. It must be accessible and affordable yet comprehensive. It must provide rational treatment, be flexible to local needs and personalised. Row would the current reality measure up to these requirements? Row would universal health insurance change the situation?

Introduction

Access

The private-public debate in health care will soon be complicated a third player - medical insurance. Based on the current scenario it is possible to predict that several companies will be competing with each other for a share of the 'urban-Indian cake' of health expenses. The 'Indian cake' is limited to that part of the urban population with sufficient guaranteed income to pay annual insurance premium. The rural community, already reeling under negative subsidies of all kinds, cannot afford such payments. Already, they find it difficult to gain access to health services; many if not most of the practitioners are unqualified, and facilities are beyond the reach of any type of regulation.

Poor access to health care is one of the many woes of living in a village. According to our own study, health care facilities (provided by qualified practitioners or quacks) are clustered in major rural centres. (Table 1) Most Indian villagers are forced to travel long distances, for which the costs are time, travel charges, loss of wages, suffering and the consequences of delayed treatment. The distance traveled can range from two to several kilometers, depending on the state and the local terrain. In Uttarakhand, for instance, patients may have to be carried in do lies over several hills and across several footbridges on rivers before reaching a modest health facility. Difficulty of physical access exists in most states other than Kerala, Goa and Punjab due to terrain. In India, access to any health care system can improve only with village-based health workers.

Current trends in health policy can be compared to taking a bull by the tail. We need to take it by the horns and one" way of doing this is universal health insurance (URI). Essentially, URI combines universal access to essential graded care, cost control, health sector regulation, protocols for rational treatment, separation of point of fee collection from points of service, a balance between the preventive, promotive and curative components of health care, health education - all supported by an effective public health systems.

What should universal health insurance mean for the villages?

Costs Compared to other countries in East Asia, India spends the 1The study was conducted by the author, alongwith Dr Dhruv Mankad. It included a survey of health providers, (geographical location, their qualifications etc.) and a household survey on health expenditure. Shyam Ashtekar is a doctor and health activist working in Nasik District of Maharashtra

Table 1 : Profile of health services available in 5 blocks of Nasik district of Maharashtra Description Surgana Peth Dindori Chandv Niphad ad Number of villages Population Number of villages with health facilities (percentage). Number of villages without health facilities (percentage) Number of government facilities rural hospitals and PHCs) Number of private hospitals run by doctors of any kind Number of private dispensaries of doctors of any kind

(rural)

172 175 144 110 135 128019 108929 208632 148307 291161 19 16 18 18 47 (11%) (9%) (13%) (16%) (35%) 153 159 126 92 88 (89%) (91%) (87%) (82%) (65%) 9 14 12 6 11

Niphad

Niphad

(urban)

Total

TOTAL

1 136 737 47533 338694 932581 1 48 119 (16%) 88 618 NA (84%) 11 52

3

5

8

6

28

6

34

56

38

19

61

66

189

20

209

393

Private doctors of all kinds

41

24

75

75

224

27

251

466

Total number of doctors (private arid government)

55

46

95

89

243

27

270

555

highest proportion of its gross domestic product on private sector health services. Insurance-reimbursement represents a mere 3.3 per cent of health expenditure. Health costs are a wildcard in family health budgets for the vast majority of Indians. Annual health expenditures vary from Rs 1,200 to Rs 3,000, which can be a severe drain on a family's resources. Services are not priced according to standard rates for procedures. For instance, an internal fixation for a fractured neck femur will cost Rs 6,000 in Nanded (a backward district), Rs 10,000 in Nasik and Rs 20,000 in Mumbai - with extra costs for medicines. Charges depend upon the service costs in each of these cities, plus fixed costs like prostheses. A related factor is the paying capacity in each community. (An invisible part of the expenditure is the commission paid to referring physicians, which can range from 10 per cent to 50 per cent of the total bill.) In private hospitals, patients' families will have little idea of what the final bill will come to, except in the case of high profile packages for coronary bypass surgery. I remember a poor farmer paying Rs, 60,000 after his sister was admitted for eclampsia, a serious condition arising in pregnancy. Private practice in primary health centres and community health centres has eliminated free care available to people in villages. URI is expected to ensure graded care at economical tariffs (free or subsidised for people living below the poverty line) with reasonable co -payments.

Health services: their content and scientific nature The health system is plagued by the existence of many unjustifiable practices - overuse of injections, antibiotics and drip infusions, unnecessary surgery, unwarranted hysterectomies and abortions. It is important to question whether a cheaper and safer alternative is available. Was the treatment justified in terms of its costs when the patient was

going to die anyway? At present, the scientific content of health services is influenced by other factors, such as the type of training that providers received the monetary returns that a particular intervention gives a provider, the absence of standardised procedures, the influence of promotional schemes by drug and equipment manufacturers, consumer illiteracy, lack of monitoring, etc. On a scale of one to 10, the content of current services in rural areas rates not more than 3. (Table 2). This hurts in two ways - escalating costs, and damaging people's health. URI can and should seek to address these problems, ushering in scientific, need-based health care with rational interventions. When we put a lid on profits per episode, we will pave the way for need-base, scientific health cares. Its humaneness will depend on the system we choose. Ensuring the human touch in the system is a challenge for insurance services.

Financial implications The general support for universal health insurance comes from the fact that today three-fourths of health expenditure comes from private pockets, and even rural families spend large amounts on health care, from Rs. 20 for an out-patient department visit to thousands of rupees (often borrowed) on major illnesses. If, say, 60 per cent of the population buys insurance, the costs will be spread out over the community, and the assurance of medical care will save both money and trouble. Such a programme will presumably need governmental support to pay for families below the poverty line. Naturally, there will be cross subsidies. Although, it will be an elaborate exercise to calculate and balance the costs to a family, individual, or the government and the returns for a doctor or health institution.

Legal reforms

Even committed doctors will resent control by what can degenerate into a 'babu raj' in the name of universal health insurance. Corruption, red-tapism and unreasonable delays exist in all public services in India. Even the most committed doctors will resent having to collect their salaries from an inefficient clerk.

Universal health insurance will need legal support. The basic provision will be to make it mandatory for local bodies to provide health care, operate UHI, and license providers to ensure quality and access to all. Though the law itself cannot provide health care, it will pave the way for universal access to health care. Similar legislation is being debated in China. Unless we make a law for the provision of universal health care, existing laws regulating medical services will remain ineffective due to the absence of services in rural areas. A URI Act will be a politically effective instrument.

Current, high levels of (doctors') incomes today are deterrents to any alternative arrangement of regulated payments, which will definitely mean less payment.

Other reservations

Table 2: Comparison of the current situation of health care with UHI Situation

Current

Ideal

UHI

reality

system

Proximity for first contact health care

2

10

10*

Access to referral grid

4

10

10

Short waiting time

5

10

10

Affordability even in risk

8

10

10

Fixed)

10

5-10**

Rational

2

10

8

Comprehensive

3

10

10@

Flexible to local/ group needs

5

10

5

Human touch

4

10

10

2 (sickness Oriented to health or illness

FCHC (First Contact Health Care) can improve only if we have village based health centers,

** depending upon UHI system, preventive/promotive care

@

One must also be cautious if interest groups talk of reserving government institutions for people below the poverty line. That kind of 'ghettoisation' will be disastrous. All providers must be brought into the universal health network, with a common framework for services and remuneration. India has a large number of alternative healing systems. Insurance schemes tend not to support, alternative systems. People must have the freedom to choose between systems. One way to do this is to provide all services and providers under the same roof, and ensure the same level of remuneration within each tier. The effort calls for a well-directed, carefully handled programme, and a huge political effort on several fronts. What we support as universal health insurance is seen as a disaster by others. The US could not carry out a health revolution though it was part of a winning election agenda. Germany took 100 years to evolve a good system of health care from the time of Bismarck in the late 19th century. India will have to carry out a delicate but sustained campaign on' this front.

Combines curative as well as

Obstacles to universal health access, and a pragmatic response to these problems Changes in democracy occur in increments, not overnight. We operate in a pluralistic, democratic society, with different pressures on every effort to reform. We must recognise our allies and our enemies. Panchayats and consumer forums should be our natural allies. Doctors are politically important to all parties, vocal in their opinions, and can shoot down attempts to regulate them. They may not support this effort for various reasons:

Basic tenets to follow for successful UHI 1. Educate providers and leaders: convince them that health care cannot be equated with other industries and trades, and is best provided by the nonprofit sector. 2. Avoid the temptation to use URI as a step to a state-paid tax model; it will destroy the confidence of care providers. URI is basically user-supported. 3. Pool all service providers (except some alternative healing practices) under the universal health insurance umbrella, doing this in' phases and by region if necessary. 4. Reorient and retrain doctors and paramedics at all levels.

Several providers will have to relocate, which they may find unacceptable for a number of reasons, including the effect on their children's education and their property interests.

√

This will entail suitable reforms of the medical education system.

on case records sent to the office, preferably electronically.

5. Establish a good standard graded care structure: First Contact Health Care (FCHC), first referral unit and tertiary hospital

8. Minimise costs with special and general preventive-promotive programmes

6. Waive fees for all non-emergency care if the FCHC is avoided 7. Establish a system of claim settlement at the district level which does not depend on the consumer physically following up the claim. This could depend

9. Conduct research to cut costs and update services and on user satisfaction 10. Avoid segregation of insurance providers in a given region into separate entities, to prevent fragmentation and gaps.

l_ A: Acceptable doctors/facilities

C. Subject to Course and test

Tertiary hospitals

FRUs State govt. grants for BPL clams

FCHC

Extending Health Insurance to the Poor Some Experiences from SEWA Scheme1 Anil Gumber Background More than 90 per cent of the Indian population and almost all the poor are not covered under any health insurance scheme. Their health care needs are primarily met through direct outof-pocket expenditure on services provided by the public and private sectors. However, various studies on the use of health care services show that the poor and other disadvantaged sections such as scheduled castes and tribes are forced to spend a higher proportion of their income on health care than the better off are. The burden of treatment is particularly unduly large on them when seeking inpatient care (Visaria and Gumber 1994). The high incidence of morbidity cuts their household budget both ways, i.e. not only they have to spend a large amount of money and resources on medical care, they are also unable to earn during the period of illness. Very often they have to borrow funds at very high interest rates to meet both medical expenditures and other household consumption needs. One possible consequence of this could be that these families are pushed into a zone of permanent poverty. It is estimated that in 1993 about six per cent of the household income was spent on curative care, which amounts to Rs. 250 per capita per annum.

The burden of expenditure on health care is unduly heavy on poor households indicating the potential for a voluntary comprehensive health insurance schemes for such sections of society.

Non-governmental organisations (NGOs) and charitable institutions (not-for-profit) have played an important role in the delivery of affordable health services to the poor but their coverage has always remained small (Dave 1991). The issue, which continues to bother us, is how to reach the unreached and, more recently, how to ensure that the uninsured get quality services. Public insurance companies so far have paid very little attention to voluntary medical insurance because of its low profitability and high risk, together with lack of demand. From the consumer's point of view, the insurance coverage is low because of a lack of information about private health insurance plans. Also, the mechanisms used by health insurance providers are not suitable to consumers. Further, in comparison to the Employees' State Insurance Scheme (ESIS) and the community-based schemes, the private plans cover a modicum of benefits (see Table 1) i.e. only hospitalisation and with a lot of exclusions. There is also a gender bias with men having better access to health care than women do, for various socioeconomic and cultural reasons. More specifically, poor women are most

Table 1: Comparison of coverage provided by various insurance schemes ESIS Type of Care/Cost Medical √ Inpatient Transport & other direct cost X Loss of earnings √ Medical √ Outpatient Transport & other direct cost X Loss of earnings √ Preventive Immunisation √ & Promotive Ante- & Post-natal care √ Maternity care √ Family planning X

There are also concerns about problems in accessibility and use of subsidised public health facilities. A majority of poor households, especially rural ones, reside in backward, hilly and remote regions where neither government facilities nor private medical practitioners are available. They have to depend heavily on poor quality services provided by local, often unqualified practitioners and faith healers. Further, wherever accessibility is not a constraint, primary health centres are generally either dysfunctional 'or provide low quality services, The government's claim that it provides free secondary and tertiary care does not stand; in reality it is charging for various services (Gumber 1997).

SEW A

√ X X X X X X X √ X

Mediclaim √ X X X X X X X X X

vulnerable to diseases and ill-health due to living in unhygienic conditions, the heavy burden of bearing children, a low emphasis on their own healthcare needs and severe constraints in seeking healthcare for themselves. Institutional arrangements have so far been lacking in correcting these gender differentials. This paper discusses some of the principal findings emerged from a larger study (Gumber and 1 This paper essentially draws upon a larger NCAER-SEWA study on Health Insurance for Workers in the Informal Sector (for details see Gumber and Kulkarni 2000).Anil Gumber is a Senior Economist at the National Centre for Applied Economic Research, New Delhi

Kulkarni 2000) undertaken on a pilot basis in Gujarat. The objectives of the study were: (i) to review the existing health insurance schemes both in India and a few developing countries with respect to efficacy and equity; (ii) to examine the health seeking behaviour, health expenditure and morbidity pattern of households protected under different health insurance environments; (iii) to estimate the demand for health insurance; and (iv) to suggest an affordable health insurance plan for workers in the informal sector.

Methods and Materials

households in different health insurance environments. Also, the survey was designed to estimate the demand for health insurance and the willingness and capacity to pay for services across socioeconomic categories of households.

SEWA Health Insurance Scheme SEWA is a trade union of 2, 15,000 women workers in the unorganised sector. It organises them towards the goals of full employment and self-reliance at the household level. Full employment includes social security, which in turn incorporates insurance. SEWA's experience repeatedly revealed that despite women's efforts to come out of poverty through enhanced

Prior to assessing the need for health insurance as a social security measure for the poor sections of society, a review of Existing health Table 2: Select Characteristics of the Surveyed Population by Health Insurance Status insurance schemes Characteristics Rura Urban was undertaken and NonSEWA ESIS NonSEWA ESIS Mediclaim lessons were drawn to explore the possibility insured insured Number of households 127 121 113 240 236 239 116 of extending coverage to workers engaged in Main source of household income the informal sector 37.0 43.9 2.7 26.2 22.9 0.4 29.3 Self employed (for details see 36.2 35.6 1.8 28.8 18.7 0.9 Casual labour -Gumber and Kulkarni, 5.5 11.6 93.8 15.4 23.3 88.3 46.6 Salaried-Organised 2000). The study 19.7 8.3 1.8 27.5 34.7 11.3 20.7 Salaried-Unorganised looked in greater 1.6 0.8 2.1 0.4 2.6 Others --detail at issues of 31164 31182 36711 33537 37715 38197 79086 Mean household annual income access and use of 2319 2299 2793 2484 2869 2887 5123 Mean household monthly expo health care services, 5.13 .1 5.50 5.47 5.42 5.88 5.64 4.63 Mean household size the level and Literacy rate (Aged 7+) (%) composition of out-of89.3 86.3 94.0 87.7 • 87.1 90.0 99.6 Males pocket expenditure on 63.5 68.2 75.7 68.6 75.6 73.4 96.8 Females health care and the 76.6 85.1 77.9 81.4 81.8 98.2 77.0 Both Sexes need for health Worker-population ratio (%) insurance for poor 54.0 53.7 48.8 49.5 50.5 50.8 56.1 Males households pursuing 31.0 33.9 22.5 22.9 28.3 16.9 11.7 Females varied occupations in 42.8 43.9 36.7 36.2 39.4 33.8 34.1 Both Sexes both rural and urban areas. It also examined the feasibility of health insurance for poor people in terms of both willingness and capacity to pay for employment opportunities and increased income, they were still the services and the mechanism for delivery of such services. vulnerable to various crises in their lives. These prevented them from leading a life free of poverty. The crises they continue to face are the death of a breadwinner, accident damage to and destruction of their homes and work equipment, and sickness. In 1999, a primary household survey was undertaken in Maternity too often becomes a crisis for a woman, especially if Ahmedabad district of Gujarat. The survey included about 1200 she's poor, malnourished and lives in a remote area. One of the households in rural and urban areas classified into four SEWA studies observed that women identified sickness of categories according to their health insurance status. About 360 themselves or a family member as the major stress event in their households belonged to a contributory plan known as lives. It was also a major cause of indebtedness among women. Employees State Insurance Scheme (ESIS) for Industrial Workers. Another 120 households subscribed to a voluntary plan (Mediclaim) and 360 households were members of the community-financing scheme, which was run by an NGO called The health insurance programme was, from the start, linked to SEWA. (See Table 2). Since 1994, SEWA has been extending a SEWA's primary health care programme which includes unique plan (covering health insurance, maternity benefits, life occupational health services. Thus, insured members also have coverage and asset insurance) to poor women engaged in petty access to preventive and curative health care with health occupations. The remaining 360 households were un-insured education. Health insurance accounts for the majority of claims and purchased healthcare services directly from the market. This and for fifty per cent of the premium paid out to the insurance last sub-sample was taken to serve as a control group. The idea programme by SEWA members. The scheme was introduced by of selecting such stratification was to understand the healthcare the SEWA Bank in March 1992 with the initial enrolment of needs, usage patterns and types of benefits received by simple

7,000 women from Ahmedabad City (Chatterjee and Vyas 1997). Later on it was extended to cover rural woman members from nine districts of Gujarat. Now its enrolment is 30,000 of which 50 per cent is from rural areas.

Table 3: Type of Coverage under SEWA Scheme Provider New India Assurance SEWA

Description of Coverage Accidental death of the woman member Loss of assets Accidental death of a member's husband Loss during riots, fire, floods, theft, etc.: (a) of work equipment (b) of the housing unit Health Insurance (Including coverage for: (a) gynaecological ailments (b) occupational health related diseases) Maternity benefits Natural Death Accidental Death

Coverage Amount (Rs.)

Premier (Rs.:

10;000

3.5(

10,000

3.5( 8.0(

2,000 3,000 1,200

30.0( (10:

(5 Health insurance is _. 300 an integral part of the 3,000 15.()( Life Insurance insurance programme 25,000 Corporation of of SEWA. The main India motivations behind the initiation of a Note: Total premium for the entire package is Rs. 60 plus Rs. 5 as service charge. health insurance scheme for women is that maintenance of active health seeking behaviour is a vital component for ensuring a good quality life and women tend to place a low Main Results from the Pilot Study priority on their health care needs. Poor women's health is most vulnerable both because of their unhygienic living conditions and the burden of bearing children. And persistent (a) Morbidity Pattern: Health seeking behaviour examined poor health of such workers costs them loss of working days against this backdrop, in the current pilot study, shows that and the corresponding incomes. the annual incidence of morbidity is around two episodes per capita which does not vary substantially among the households having different health insurance status except The coverage of the SEWA health insurance programme those subscribing to Mediclaim. However, female morbidity includes maternity coverage, hospitalisation coverage for a turns out to be higher than male morbidity in all the seven wide range of diseases, and insurance for occupational health population groups studied 1 (The detailed analysis has been related illnesses and other diseases specific to women (see attempted separately for seven groups of households namely, Table 3). More specifically its main features are: rural ESIS, rural SEWA, rural noninsured, urban ESIS, urban SEWA, urban Mediclaim and urban non-insured.) Occupational health coverage. Coverage for women specific diseases. Maternity benefit. As expected, both in rural and urban areas, the private sector Coverage for a much broader range of diseases (which are has played a dominant role in providing services for not covered by the GIC’s Mediclaim plan) Simplified ambulatory care (acute and chronic morbidity). Even among administrative procedures. the ESIS households, particularly in rural areas, there is Life and asset insurance coverage of the woman greater reliance on private facilities for the treatment of acute member. illnesses. The survey results clearly pinpoint the poor Life coverage for members' husband or other outreach of ESIS panel doctors, dispensaries and hospital members of household (in case of widowhood and facilities for rural insured households. In urban areas too, separation). only 54 per cent of acute and chronic ailments of the insured population were treated at an ESIS facility. However, for The SEWA health insurance scheme functions in coinpatient care, there is some reliance on government ordination with Life Insurance Corporation of India (LIC) hospitals by all categories of households (except Mediclaim and New India Assurance Company (NIAC). SEWA has beneficiaries). integrated the schemes of LIC and NIAC into a comprehensive health insurance package to address women's (b) Expenditure on Treatment: A special effort was made basic needs. The claimants are the needy health benefits to collect information on three types of expenditures on seekers and as the insurance is an additional benefit, the treatment: (a) direct or medical costs (fees, medicine, beneficiaries willingly pay the premium. Most of the insurers diagnostic charges and hospital charges), (b) other direct opt for fixed deposits of Rs. 500 or Rs. 700 (depending upon costs (transportation and special diet), and (c) indirect costs the type of coverage) with the SEWA Bank and the interest that included loss of income of the ailing and caring person, accrual goes towards annual payment of premium. It is the and interest payments on amounts' borrowed for large membership and assets of the SEW A Bank that have treatment.(Table 4) The share of medical costs in the total made possible the provision of the insurance coverage at low costs is about two-thirds in most of the population groups. premiums.

Even among urban ESIS households who have used an ESIS facility to a greater extent, the share of medical costs in the total costs has remained around 50 per cent. The average total expenditure on treatment, irrespective of health insurance status, turns out to be higher for rural than for urban households. Getting insured has helped in mitigating expenditure on treatment only for urban ESIS households. Their out-of-pocket expenditure on treatment for acute and chronic ailments turns out to be about 30 per cent lower and for hospitalisation about 60 per cent lower, as compared to SEWA and other 'non-insured' households, or compared to their rural counterparts. Mediclaim beneficiaries indicate high levels of expenditure for all the three types of illnesses. (c) Burden of Health Care Expenditure: The burden of out-of-pocket expenditure on households is computed after estimating the annual per capita expenditure on treatment of illnesses, use of MCR services and health insurance premium. The per capita expenditure on treatment turns out to be higher for the rural population. In urban areas the per capita out-of-pocket expenditure among both ESIS and Mediclaim is lower than in SEWA and 'non-insured' households. The expenditure as a proportion of income (burden of treatment) was the lowest for Mediclaim households and the highest for rural SEWA households.

illness episode, having reported lower incidence of illness as well as higher level of annual income, the burden is just 6 per cent. (d) Users' Perceptions on the Delivery of Health Insurance Services: On the subject of satisfaction with the schemes, the subscribers of the three insurance schemes under study responded differently. SEWA beneficiaries are largely satisfied with the various aspects of the working of the scheme. SEWA beneficiaries feel that the low premium and the availability of maternity benefits are the most positive aspects of the SEWA scheme. The point of dissatisfaction worth mentioning is with the coverage of family members. In the same vein, dissatisfaction expressed by the ESIS beneficiaries is distance, inconvenient location of dispensaries in the case of rural households, and the time taken to seek treatment in the case of urban households. Mediclaim beneficiaries (30 per cent) feel that the process of filing claims should be made simpler and claims should be settled faster. About 26 per cent of the respondents expect the coverage of OPD expenses/domiciliary hospitalisation in the Mediclaim plan.

(e) Demand for Health Insurance: The responses indicate a strong inclination towards subscription to health insurance schemes by the households in general and specifically by workers in the informal sector. Table 4: Cost of Treatment by Health Insurance Status of Household in Rural and Urban Ahmedabad A majority of the low income Type of Morbidity Rural Urban households wish to get NonSEWA FSIS NonSEWA FSIS Mediclaim enrolled to any health insured insured insurance scheme, despite the Acute morbidity perception of the many, in fJ7 Medical Cost 233 200 224 234 228 686 both rural and urban areas, that ff) Other Direct Exp. 77 62 4S 54 49 152 their health status is 'good' or 'excellent'. The demand for the Indirect Cost 90 33 93 50 54 55 85 SEWA health insurance Net Total Cost 401 295 380 331 336 202 923 scheme is the highest both Chronic morbidity among the SEWA members Medical Cost 347 284 214 210 261 135 216 who are not enrolled as well as Other Direct Exp. 115 81 215 56 51 74 43 the 'un-insured'. About half of Indirect Cost 236 86 225 98 25 5 60 Mediclaim households have Net Total Cost 451 644 364 371 234 263 en also expressed interest in Hospitalisation joining the SEWA health Medical Cost 2A27 :lJ72 2200 3246 2009 621 4045 insurance scheme. The next to Other Direct Exp. 444 557 589 431 780 318 935 follow in the preference order Indirect Cost 631 ff)4 305 439 413 206 464 is the Jan Arogya. There is a Net Total Cost 3502 4323 :lJ76 2954 3280 1146 4034 substantial percentage of 'nonNote: Medical Cost includes expenses towards fees, medicine, diagnostic and other hospital charges. Other Direct insured' and ESIS households Expenditure includes expenses on transport, special diet, etc. in the urban areas, which are Indirect Cost includes loss of income of the ailing person as well as of the caring person and one year interest payment (@ 24 willing to enroll to the Jan per cent) on the amount borrowed during the course of treatment. Arogya scheme. The main Net Total Cost = Direct cost + Indirect cost – Reimbursement. reason for preference towards Source: NCAFR-SEWA Survey, 1999. both SEWA and Jan Arogya is their low premium. It may be worthwhile to point out that there is a substantial percentage of non-enrolment among the SEWA members (42 If one includes the expenditures by households on MCR and per cent). The reason put forward by the majority of both insurance premium, the burden increases further. The burden SEWA members and those belonging to the 'uninsured' of total health care costs varies between 18 and 21 per cent category is the 'no knowledge of insurance'. Knowledge of in three categories of rural households; the corresponding insurance is also very limited among those who have range for urban households is 10 and 18 per cent. Although subscribed to one of the schemes. the Mediclaim households have spent the highest amount per

They have expressed very little knowledge about the alternative schemes. It is, therefore, not surprising that large sections of the sample whose income levels are not high are ignorant of the Jan Arogya scheme. Jan Arogya has been basically designed for those who cannot afford high levels of premium. The respondents suggested that those managing the scheme(s) should adopt more rigorous methods of spreading knowledge and awareness of the different health insurance schemes. (I) Expectations from a New Health Insurance Scheme: As far as broad expectations from a new health insurance scheme are concerned, among rural households the coverage of all illnesses and timely attention seem to be paramount. Among the urban households, however, it is the price of the insurance scheme that seems to be the most important factor considered for determining enrolment. Among the specific medical care benefits, coverage of hospitalisation expenditure is desired by more than 90 per cent of the respondents in both rural and urban areas. Hospitalisation being expensive, there is a strong demand for the coverage of hospitalisation costs among the respondents. The coverage includes fees, medicines, diagnostic services and hospital charges in rural areas. The urban respondents expected specialist consultation (as part of the coverage of hospital expenses). Also, about 50 per cent of households wanted the coverage of expenses for transport to be included in the plan. The expectation of coverage of OPD and MCH follows next to that. The availability of OPD facilities at government hospitals rather than at dispensaries and clinics is a better way of providing coverage towards expenses incurred for OPD health care. The room for improvement as suggested by the respondents lies in increasing the coverage of family members and in coverage of services. The SEWA beneficiaries are, in particular, interested in coverage of additional household members.

(g) Willingness-to-pay for the New Health Insurance Scheme: It is not that the respondents expected the above mentioned health insurance services free of charge. The rural respondents were willing to pay an annual per capita premium between Rs. 80 and Rs. 95 for the coverage of services of hospitalisation, chronic ailments, specialist consultation and the like. Further, with the coverage of costs such as fees, medicine, diagnostic charges, transportation, etc., the respondents are willing-to-pay an amount that is higher by 16 per cent. For additional benefits (such as life coverage, personal accident, etc.), the respondents are willing-to-pay an additional amount that is higher by around 7 per cent when compared to the amount that they are willing-to-pay for coverage of costs. The urban respondents (barring Mediclaim beneficiaries) are willing to pay an amount ranging from Rs. 82 to Rs. 84 by type of coverage of services. In addition to the above services, the respondents are willing to pay an amount extra by 13 to 25 per cent for the coverage of costs, and further 11 to 14 per cent more for the coverage of additional benefits. The corresponding percentages for the Medic/aim beneficiaries are 23.5 and 19.5. The preference for the type of management for a new health

insurance scheme varied by the place of residence. A substantial proportion of the rural respondents preferred management by non-governmental organisations (NGOs); the next to follow was public hospital-based management. Also, a section of the rural respondents are of the opinion that village-level institutions, such as Panchayats, should be delegated the responsibility of running the new heath insurance scheme. In the urban locations too, with the exception of Mediclaim beneficiaries, management by NGOs is most preferred. Public insurance company management follows it. Thus, it is quite indicative that most of the lowincome households have faith on the public system for delivering of services.

Implications for Designing an Affordable Scheme for the Poor The paper addresses some critical issues with regard to extending health insurance coverage to poor households in general and those working in the informal sector in particular. These issues have become extremely important in the current context of liberalisation of the insurance sector in India. There is no doubt that health insurance will be one of the high priority areas as far as consumers, providers, and insurance companies are concerned. However, developing and marketing a unique and affordable health insurance package for low-income people would be a great challenge. First of all, there is a strongly expressed need for health insurance among low-income households in both rural and urban areas. This need has arisen primarily because of the heavy burden of out-of-pocket expenditure on them while seeking healthcare. Despite a significant reliance on public health facilities, the poor households tend to spend nearly one- fifth of their income on treatment. Even among the fully insured households under ESIS, the burden is unduly large, particularly among rural ones. This clearly reflects upon a large-scale inefficiency operating in the delivery of services by both government and ESIS sectors.

The existing private health insurance plans just cover a hospitalisation expense, which, our analysis shows, amounts to only between 10 to 20 per cent of the total cost of curative care. If one includes medical expenses towards outpatient care then it forms 55 to 67 per cent of the total cost of curative care (Gumber 2000). Therefore, the new health insurance plan should aim at covering two-thirds of the total health care burden on households. While, innovative measures for improvement in ESIS and Mediclaim programmes are a necessity, these will continue to cover a small proportion of the population. There are hence many other emerging issues as far as future health insurance schemes are concerned. Expectations of lowincome households from a new scheme indicate that coverage of illnesses, coverage of services, amount of the premium to be paid as well as the procedural aspects such as filing claims are critical in the decision to buy an insurance. A strong preference for the SEW A type of health insurance scheme reinforces the fact that the beneficiaries

desire a system which is not only affordable but also accessible in terms of easy settlement of claims and other related administrative procedures. The range of services expected to be covered includes hospitalisation, maternal and outpatient facilities. There are several important issues regarding formulating, designing, operationalising and managing an affordable health insurance scheme for low-income households. Before launching a novel and unique plan, varied designs in different settings can be tried out on a pilot basis preferably at the district level because some of these would be having severe cost and management implications. The critical points and steps to be considered in this process are discussed below. (a) Defining the benefit package: The types of benefits to be included are: (i) Inpatient care: the event is unpredictable but rare and the cost of treatment is either unaffordable or payment pushes people, even the better off, into indebtedness and poverty. (ii) Outpatient care: Insurance is generally not well suited to routine ambulatory care because its requirements tend to be reasonably predictable and are of relatively low cost and people might be expected to meet these costs out of their own pockets as they go along without too much hardship. Most people, however, do prefer it to be included at least those services (diagnostic and clinical) having a bearing on their pockets. (iii) Chronic care: Insurance is also poorly placed to meet the needs of chronic care as such conditions, although high in cost, are not unpredictable. While members of a scheme may be willing to subsidies others in rare times of extreme need, they may be unwilling to heavily subsidies them on a regular basis. This is not to say providing protection for those with chronic illness is not important; it is just that insurance is not the best way of doing it. (iv) Maternity care: Another area that generates considerable discussion is that of the possible inclusion of deliveries in the package. It is possible to argue that mothers have nine months to plan to meet the financial costs of a normal delivery and should be expected to do so themselves; but that a scheme might include emergency deliveries which are rarer but expensive. If schemes do decide to include outpatient care and chronic care they must expect premiums to rise. Also, experience tends to suggest that women's groups do want deliveries to be included. If so, the costs of the scheme may rise and again a future requirement might be to include all antenatal care. Individual schemes may wish to deviate from these broad principles. One option might be to offer greater protection to the poorest in the group by perhaps offering them greater benefits, i.e. to cover the cost of ambulatory care where this benefit is not enjoyed by better-off member. Alternatively, schemes could decide to meet the non-medical costs of treatment (e. g. transport costs) or they may even wish to extend benefits beyond just health care to cover loss of income though this is unlikely and should only be considered when there is a demonstrated record of financial sustainability and ability to pay.

Another possible approach is whether referrals to the more specialised facilities are to be included. Should a scheme's benefits be restricted to secondary hospitals or should referral for, say specialist cancer treatment or heart surgery, be allowed? This could potentially be very high cost and drain the scheme's resources. What is an appropriate role for the State? The Government would presumably wish to give the insurance company some freedom to determine the benefit package but may wish to insist that certain elements be included e.g. inpatient and preventive care. Also a minimal benefit package should be defined so as to ensure delivery of only cost-effective services. (b) Deciding a Panel of Providers: If services are free there is little point in getting insurance. Likewise, if most of the costs are in the form of unofficial fees, health insurance can do little to help. Health insurance only makes sense when fees are being charged and this may rule out involvement with Government facilities. However, fees may differ significantly from Government facilities, where services may be free, to NGO providers, to expensive, for-profit providers. Obviously, higher the fees, higher the premiums need to be for a scheme to break even, but greater are the potential benefits from health insurance. The question is whether the services should be restricted to just one provider or whether members should have choices. This depends largely on what members want and where they are currently getting their services but the decision does have some implications as set out below. In some areas, especially rural and hilly areas, there may be little choice. Such an approach is perhaps easier to implement although lack of competition reduces any pressure on the provider to improve the services. In urban areas there may be a number of potential providers. Dealing with a number may be more difficult from a managerial point of view but it does offer better potential for driving up quality. A scheme offering a choice of providers must include incentives for members to access the quality of care they need in the cheapest setting. One way to do this is to charge higher co-payments - a small amount charged when services are used - for higher cost providers. (c) Type of Membership and Population Coverage: We must decide whether the scheme would cover just the villagers, slum dwellers, poor, occupation groups, thrift and credit groups (e.g. DWCRA), select geographic units, workers, women or adult groups. Unit costs will differ if switching over from individual to household memberships because children and other dependents (elderly) have different health needs from working populations. Clearly, on the one hand the more people covered the higher the premiums per household but on the other hand this means a reduction in adverse selection. (d) Reducing Moral Hazard and Preventing Adverse Selection: Although the ultimate decision should be left to the group itself it is likely that unless the majority of members join (ideally all) there will be problems. Ideally one

should cover the entire village or Panchayat or settlement and making the scheme compulsory while covering a minimal benefit package. Free services offered to members once they have paid a premium encourage the use of services even if their need is not great. One way of preventing such overuse (also called moral hazard) is to charge co-payments. Although far less than the user fees this would at least provide a deterrent to unnecessary use. Also, on the provider side there are incentives for over treatment and also the possibility of fraudulent practices as numbers and providers collaborate to falsify claims. One should aim at creating incentives for cost-effective treatment i.e. preventing unnecessary use of services and ensuring services are of the appropriate quality provided at reasonable cost. Again, approaches exist to contain, if not prevent, such practices totally. Members themselves would be expected to monitor use; it may also be possible to compare utilisation between groups to identify areas where overuse may be possible. Any managing organisation would also wish to monitor this issue. (e) Management arrangements: Management will be important at all stages of preparation, design, implementation and evaluation. One approach might be to contract an NGO to take this role. They might carry out the initial groundwork and be responsible for monitoring schemes, perhaps even negotiating special discounted rates with providers. If the groups are small the managing agency may wish to introduce an addition element of risk pooling by taxing schemes and redistributing these funds to schemes in need. The role of evaluation would, of course, be given to another institution. A managing institution, however, would require special skills in the following areas: community participation/community liaison/ marketing; book keeping and financial analysis; monitoring; access to medical expertise (to validate treatment decisions) and possibly to provide preventive care directly (funded through premiums).

(f) Payment mechanisms: There are a number of options here. In short, the approach should be as simple as possible and not open to fraud. Funds could be retained within the age group or held at the facility (if they can be trusted). Members might be given a card with photographs and be exempted at the facility with the scheme reimbursing the provider afterwards. Alternatively, the member might be expected to make payment up front and be reimbursed by the scheme later - though this may present a significant barrier to some. (g) The Role of Government: A number of important roles emerge for Government in the development of health insurance schemes, which includes financing, management, training, monitoring and evaluation. First of all it would be important to carry out a mapping exercise which would include: identifying the nature and activities of the target groups; assessing their knowledge of, and interest in, health insurance and carrying out advocacy and training as necessary as through workshops/individual visits; identifying interested groups for the pilot project; carrying

out willingness to pay survey and design and cost benefit package; carrying out a baseline survey on current health practices in a pilot project area and also in control groups of initial administration costs.

It might be reasonable to provide schemes with funds to meet the initial start up costs. Those would not be significant as the approach would be paper based. Although an ongoing subsidy is probably unwarranted - schemes would be expected to be self supporting with the benefit package tailored to meet what people are willing to pay - in the short term a subsidy might be justified. Government can also stimulate the interest in the schemes and guarantee interest so people can learn the benefits of insurance, an unfamiliar concept. It can also provide an extra incentive to well designed schemes - the subsidies may be made available to schemes which incorporate elements of best practices. (One approach may be to cover 50% of premiums for a fixed period of two years.) If Government wishes to evaluate schemes with a view to their replication it is important that the schemes are developed in a variety of settings (urban/rural etc). Otherwise they will leave themselves open to the criticism that they only work in particular circumstances. It is also important to be clear about what the schemes are intended to achieve so there can be some basis for evaluation.

References Chatterjee, Mirai and Vyas, Jayshree (1997): Organizing Insurance for Women Workers: The SEWA Experience, Self Employed Women's Association (SEWA), Ahmedabad. Dave, Priti (1991): "Community and Self-Financing in Voluntary Health Programmes in India", Health Policy and Planning, Vol. 6 (1) pp.20-31. Gumber, Anil (1997): "Burden of Disease and Cost ofI11 Health in India: Setting Priorities for Health Interventions During the Ninth Plan", Margin, Vol. 29 (2), pp. 133-172. Gumber, Anil (2000): "Health Care Burden on Households in the Informal Sector: Implications for Social Security Assistance", Indian Journal of Labour Economics, Vol. 43 (2), pp.277-291. Gumber, Anil and Kulkarni, Veena (2000): Health Insurance for Workers in the Informal Sector: Detail Results from a Pilot Study, National Council of Applied Economic Research, New Delhi. Visaria, Pravin and Gumber, Anil (1994): Utilization of and Expenditure on Health Care in India, 1986-87, Gujarat Institute of Development Research, Ahmedabad. ~

Dialogue Medico Friend Circle Bulletin Editorial Guidelines: A conceptual note Padma Prakash was given the task of preparing a short note on editorial guidelines for the MFC bulletin. These were presented at the MFC mid-annual meet in June 2000, but not discussed. We reprint the note and invite responses from all readers. A more detailed discussion on the editorial guidelines for the bulletin will take place in the General Body meeting of the MFC on 20'h January 2001.

These are some thoughts on the principles on which any guidelines should be formulated. They are open for discussion-but of course. Some of the points here may seem irrelevant given the small size of the journal and its reach. But perhaps lack of clarity on these issues has itself been a constraint to its growth. 1. A journal's guidelines are not 'forever' documents. The dynamism of a journal lies in revising its guidelines periodically. This does not mean that there is no structure or backbone to the document. But essentially, editorial guidelines are different from, say, a group or party statement of objectives. They respond to the needs of the readers, as much as to those of the publishers. Who are the editorial guidelines for? Is it our task to provide a list of dos and don'ts? To prescribe format and length and style sheets? I think we should do more than that here. A journal/bulletin/newsletter should formulates guidelines for itself as well for its contributors and readers. It is as important for those who produce a journal to understand who we are and where we want to go as for those who write for the journal to know its overall direction and perspective. 2.0ur first task is to locate the journal in its historical perspective and its contemporary location within the geography of the media world. Who publishes the journal? Does the publisher have a perspective which is projected in the journal-in other words, is it a group organ whose main task is to provide information and news about the body? Or is it a publication whose objective is to expand primarily the core philosophies of the organisation, and not necessarily the organisation itself? There is a difference here-for instance, among the objectives of the mfc may be rational therapeutics; but the journal does not only provide the derived understanding of rational therapeutics, but also projects the concept or understanding of rational medicine and offers space for debate, ensuring that opposing points of view are not stifled. [Again, this is my understanding of what the bulletin should do, and is open to debate] A journal, any journal, is prompted by the need to disseminate knowledge of one kind or another, in one form or another. It may be a bulletin started to provide data comma a journal to encourage debate, a newsletter

documenting events and happenings of a particular organisation, a means of keeping in touch with friends scattered far and wide or a platform for all of these. We have to determine a) what the dominant feature of the publication was when it began and b) how it has evolved at this pointthat is the point at which a particular set of guidelines is being formulated. For example it is important I think to note in the guidelines that the mfc bulletin's strong feature when it began was the linking of its members, a forum for exchanging notes about their activities, which over a period has disappeared. While it may have more or less disappeared, I for one believe that the bulletin ought to ensure that the space for such communication theoretically exists and will come into being once such communication begins to happen. Following from this will be the objectives of the journal at that point of time. 3. Tracing the trajectory is important because it provides an insight to the producers of the journal as well as the contributors of the rigidities and the flexibilities of the journal. It would of course be wrong to read the changes that have occurred in the journal entirely in terms of the directions envisaged or control exercised by the producers. For, often this direction or control may have been determined by the kind of material received, which given the constraints of periodical publishing found its way into the journal such that it begins to define the journal. This is in fact, why guidelines are important, so that such a change in direction-at its extreme, a 'takeover' - does not occur. At the same time rejecting every single piece considered reactionary, without looking at whether it pushes a certain debate forward, can sometimes have the opposite effect of stultifying the paper. Similarly, it is important to note that the very definitions of what should be the subject of mfc bulletin has undergone greater detailing - inclusion in the bulletin of say, the issue of large dams, water supply etc are evidence of this process. Whether given the expansion of literature on these issues is so vast that the mfc bulletin might like to limit or exclude it and therefore re-define the health focus is another matter. Similarly, women's issues in health care. Or for that matter, whether party perspectives should find space. 4. The above will define the nature of the material we publish and invite for the journal. The first category of material would be original contributions research summaries/debate initiators. What would be the areas in which we should be publishing these contributions? Given that our understanding of health is as a social science (!) which is therefore enriched by both medical and social sciences, do we a) allocate proportions for the two-with contributions in, say, developments in pharmacology and on medicine usage? Or do we b) define as our subject area the delivery of health care, and therefore consider only those aspects of medical/technical/ developments that have or are having a direct and visible influence on the issue of health delivery? In other words, does the bulletin see medical technology/ medical science only as a means of delivery of health care?

particular piece. This, I know, is a somewhat slippery area, but I do believe we need to discuss this even if does not end in a good formulation for the guidelines. For instance, there are any number of developments taking place and being published in medical science having to do with different modes of drug delivery, new information about pain pathways, surgical techniques, etc which do not immediately impinge on the issues close to our heart (or our practice). What kind of space do we give for them? In other words, does the bulletin function as an information clearing house of this sort? If so to what extent? Or do we decide case by case? What do we say to a new contributor who wishes to write and what do we offer a new reader who wishes to subscribe? There are members of the mfc one presumes, or hopes there will be, who work in laboratories or at the technical interface of medicine and health who may feel that they might like to share their work. Do we then allow only this -that is, publish technical/medical science material only in the course of 'sharing of work' and not as independent 'original' contributions? To sum up: What is the nature of content of the mfc bulletin? 5. Next what is our readership? Who do we address? An easy answer is 'members', primarily. But who are these members? Ate they doctors in large hospitals? In community health work? Social scientists? Political/social activists? Again it is important to give some idea of the changing characteristics of our readership-if such change has occurred. This also tells us if the content has matched their expectations, if it has been dictated by them, or if it has ignored the needs of the new readership. 6. This in turn defines the manner in which the mfc bulletin arranges or edits matter for publication. For instance, if our readership is primarily people who understand, say, a first year medical student's / a graduate medical student's technical/medical vocabulary, we need not waste time defining terms. Similarly, if all our readers have some understanding of statistical methods, we don't have to explain them, or for that matter sociological/anthropological understanding. It is difficult to determine the level of language comprehension of a readership for a bulletin such as the mfc bulletin; but it is possible to arrive at a minimum by eliciting and pursuing feedback from readers. To illustrate, take tables. How well do people understand tabular representations of data? Would graphs be better or should they be provided to those who want to see them and not to general readers-thus saving space? As a personal observation, I would say that every journal should aim at levels of understanding slightly above what we understand to be the norm. 7. What is our policy on nomenclature-the way names are printed-do we give the titles, degrees, etc? What order of appearance do we suggest? As per the copy submitted? The journal cannot dictate how and in what order names appear on a contribution, but they can make a general declaration that the journal encourages the inclusion of the names all those who contributed directly to the writing of that

8. Ethical questions; Journals cannot ensure ethical conduct of research or practice. But they play an important role in encouraging such practice. In scrutinizing any submission to the bulletin, it is important to make a note of the ethical queries that arise and to seek clarification. However, given that in health research the lack of ethical conduct may be because of a lack of information and insight into these issues, the mfc bulletin cannot afford to ignore members of the health care community, but encourage discussion. This part of the guidelines needs to be looked at frequently and revised in keeping with the spread of ethical concerns. 9. Structure of publication group. 10. A) role of editor: We need to have clarity on this issue. Just how much autonomy does the editor have? This again depends on how we view the journal-as an extension of the mfc or something more than that? Does the editor have the final say or all the say? Is the editor only a facilitator of a group constituted by the mfc executive or general body to publish the journal? Does the editorship revolve within this group? Should there be a tenure for the editor? Are the editor's contributions appearing in the journal, as editorials or otherwise subject to scrutiny by the group so constituted or by the executive body of the mfc? Should editorials be signed and therefore become the opinion of the author and not of the journal? For that matter, what constitutes an editorial? The collective mfc opinion? The collective/ representative opinion of the group? B) Process of review: This depends on how we view the bulletin and therefore how we constitute the publication group of the journal. Is a group to be constituted to help in the production process alone or is this group to be the final decision-making point for reviewing and refereeing all contributions? Or do we constitute from the general body 'experts', who will monitor and facilitate, say, networking and exchanges as well as various 'specialties', whose role and responsibility it becomes to ensure material for particular areas/sections? C) Criteria for Refereeing: This depends a lot on what the bulletin is or is envisioned to be. For instance, if we were to reflect on the content of debate within the mfc in the bulletin, then not all opinions/views or even data are well corroborated or documented in the course of the exchangewhich is necessary perhaps in the organisation, because it allows even those who do not have expertise in a particular area to participate in the debate. However, if we were to allow an exchange of ideas in the same way in the journal, there is a time lag between the appearance of the opinion and another which counters the unsupported facts and opinions. Or the editor will have to editorialise on these question marks. In sum do we bring to bear the same criteria for accepting all kinds of contributions, or do we, as many journals do, apply stricter criteria for some sections of the bulletin. This will evolve into guidelines for authors such as: " We request that all original papers (or whatever way it is to be described) to include sources for all data/facts. However for contributions to the section on 'Opinions'

'Exchanges' , we need some indication of source, even if full details are not available." There are bound to be a number of other issues we need to take up in formulating the guidelines. I think we need to spend time discussing these-especially determining where the bulletin stands vis a vis the organisation. We have, all of us, often been critical of the mainstream media, for its rejection of articles, as being biased or prejudiced. We should be aware that unless we have a clearly spelt out editorial policy, our decisions will also be seen as being biased.

Padma Prakash Mumbai

Supreme Court judgement on injectables Dear Friends, This is to inform you of what is definitely a historic event. On 24th August was the final hearing after which the Net En case has finally come to a close. Yes - the historic case against the injectable contraceptives, Net En, filed by Stree Shakti Sanghatana, Saheli and others in 1986. Although we feel as petitioners we have come out of this long-drawn out case honorably, we find it a bit difficult to say we have "won" the case because the order can be interpreted as a "victory" by both sides. However, as our counsel Murlidhar and Ganesh point out (and we concur), given the fact that there have been major changes in the economic, social and political climate since the case was filed 14 years ago, this is the best order we could have got. At that time, the plea was to obtain a stay on the Phase IV clinical trial of Net En, and prevent its entry into the Family Planning Programme. Subsequently, Net en became available in the private market, raising other issues over-the-counter availability, misuse, indiscriminate use by N.G.O.s etc, which were outside the scope of the case. The case was closed after we as petitioners accepted the following two paragraphs of the Government of India affidavit: Para 4. That perusal of the report! [Report of the special meeting of the Drugs Technical Advisory Board (DTAB) held on 16th February, 1995. The objective of this meeting ordered by the Supreme Court was to examine the recommendations of the technical sub-committee on certain issues raised by the Drug Action Forum in their petition. The recommendation relevant to DMPA is in Para 7: "The members had agreed for continued private marketing of Depo Provera injection. The drug, however, is not recommended for inclusion in the Family Planning Programme."] Shows that interim recommendations with

regard to (DMPA) are that it should not be allowed for mass use in the National Family Planning Programme and its use should be restricted to women who would be aware of all the implications of its use. It is submitted that the said drug, and also Net-en, although available in the. market against prescription but they are not included in the family planning programme." "Para 5. That as directed by this Hon'ble court, Net En was under examination for clinical trials with ICMR (Indian Council of Medical Research) and a Technical report had already been filed before the court after finalizing their trials. Thereafter the Department of Family Welfare has also filed an affidavit indicating that the Ministry of Health and Family Welfare is proposing to introduce Net En injectable as a new contraceptive in National Family Welfare Programme in such places only where adequate facilities for follow-up and counseling are available." This statement clearly shows that a restriction is recommended, and mass use of injectables is not advisable. The Indian government appears to be following the strategy outlined at the IRR (Institute for Research in Reproduction) meeting in 1998- the phased entry of injectables. We have been hearing strong rumours that injectables (some say only Depo) are soon to be introduced in the family planning programme. The word going around is; "Now that the case is finally over, we can peacefully introduce injectables." However, the order clearly states some kind of restraint, and cautions against mass use. We need to emphasise this fact. It is also important to remember that the Drug Action Forum case (in which Depo Provera is one of the drugs) is still pending. The recommendation of the DTAB in this regard (quoted above) is clearly against mass use. If anyone of you have any new information about introduction of injectables, please let us know. As of now, we do not have any immediate plan of action, except disseminating this information. Please pass on this information to others. In solidarity,

Vineeta, Vani and Laxmi Saheli Above Shop 105-1 08, Defence Colony Flyover Market, New Delhi 11002416

Judgement no cause for celebration Dear Friends, This is with reference to Vani, Vineeta and Lakshmi's letter from Saheli regarding the Net-en case and Supreme Court judgement. I am still not very clear about the judgement. However, I feel that the two points that were 'accepted' by the Court can be easily misused. The first four lines of Para 4," ... that it should not be allowed to (sic) mass use...," do not clearly mention Net-en; they only talk about Depo, that "its use should be restricted to women who would be aware

of all implications." Knowing the reality, I think 'victory' is still an illusion. What Para 5 says is a little more alarming. I wonder who cares about 'follow-up and counseling' once these injectables are in the National Family Welfare Programme. Even though the Drug Action Forum case is still pending, I am not at all confident about that. Kalpana Mehta and I had worked on that petition and I know the strengths and weakness of the petition. Our arguments in that petition were as follows: IN THE SUPREME COURT OF INDIA I. A. No. OF 1994 IN

WRIT PETITION By DRUG ACTION FORUM & ORS This is further to our petition before the Supreme Court dated November 7, 1994 in the writ petition no. 698 of 1993 asking for the stay on the sale of Depo Provera and Net-En on the following grounds: I. Insufficient research within the country, also there is not enough research on dose determination and on side effects. II. Adequate evidence of serious side effects from studies carried out in India and abroad. III. Manner in which licenses were granted for the sale of these drugs. IV. Sale of the above products without prescription and the inability of the Drugs Controller of India to monitor the same and to take punitive action. V. Intent of the Government of India to introduce these in the family planning programme of the country, where informed consent, adequate screening, monitoring and follow up are all severe constraints. Further, there is a gross potential of abuse and coercion. In 1995 THE DRUGS TECHNICAL ADVISORY BOARD passed a recommendation that "The members had agreed for continued private marketing of Depo-Provera injection. The drug, however, is not recommended for inclusion in the Family Planning Programme." But this was only for Depo and not for Net en. Meanwhile we have come across the letter dated November 10, 1999, from Dr. Sutinder Bindra, M. D., Medical & Regulatory Director, Pharmacia & Upjohn India Pvt. Ltd, S.C.O. 27, Sector 14, Gurgaon - 122001, (Haryana), India addressed to Dr. Michael Vlassoff, UNFPA Representative India, UNFPA, 55, Lodhi Estate, New Delhi 110003. This letter is based on the Post Marketing Surveillance Study on Depo-Provera in Indian Women (Protocol no. M/5400/ 0228) by Dr Soonawalla

voluntarily chose to use this method of contraception. In this study, each subject was given Injection Depo Provera for a period of 12 months (5 injections) and was followed up for a total period of 15 months. The study included 1079 women enrolled from 10

centres in India from June 10, 1994 to December 31, 1997. The subjects were healthy women of child bearing age, including post-partum and breast-feeding women, who met the study entry criteria. Informed Consent was obtained from all subjects before inclusion in the study. Majority of the non-serious medical events were expected menstrual disturbances, such as spotting, irregular bleeding, amenorrhoea and prolonged, menstruation. The results from the study indicate that Depo Provera 150 mg is a safe and effective contraceptive, and that sufficient pre-treatment counseling on the expected hormonal effects will greatly increase the acceptability of this method of contraception." From all these, I really wonder how much the second petition can do to stop the entry of these contraceptives into the family planning programme, in spite of the DTAB's recommendation. In the first petition (1986), the arguments were much stronger and we in fact used some of the points from the first petition and added some new information for the second. I think it is very important to take a fresh look at this issue and think of newer strategies. Perhaps we need to stop restricting ourselves on contraception alone and look at the Population Policy in its entirety. Just recently the National Population Policy was passed but we have not done anything about it. The way the two child policies are coming up all over, especially Uttar Pradesh, Madhya Pradesh or Maharashtra, we can be absolutely certain about what a dangerous role this kind of a judgement can play in all these plans. As you all know that Uttar Pradesh population bill was passed on this July 11. In that it says very clearly that: "The state government will include materials related to new technologies such as injectables, new types of IUCDs etc, their advantages and disadvantages, contra-indications, and side effects in various curricula developed for training of govt. and NGO sector providers under family planning programme. "The state govt. in consultation with the GOI will conduct operations research studies to examine the possibility of introducing injectables in family planning services provided by the state government." As we are well aware, for the establishment it is very easy and convenient to take our words to reshape and strengthen their own coercive plans. This, I am afraid, will be yet another example. I suggest, let us come together and rethink the whole issue and our future interventions. Looking forward to hearing from you all.

Here are some key points from that letter: "This open-label prospective study was designed to assess safety and acceptability of Depo Provera 150mg (DMPA) given once every three months in Indian women who

Sincerely

Sarojini SAMA, J-59, Saket, 2nd Floor, New Delhi 110017’

Postbox

MFC Annual Theme Meet 2001 Second Announcement

Press Conference on PHA in Mumbai Venue: Mumbai Marathi Patrakar Sangh Date: 07-11-2000, Tuesday Time: 2:30P.M. Reporters from about 15 newspaper dailies attended the Press Conference in Mumbai on the PHA. Dr. Anant Phadke addressed the Press Conference. He began by explaining the promises made by Governments in 1978, towards the goal of Health for All by the Year 2000 and how Governments and International Agencies had forgotten about the above commitment. Over 2000 organisations are involved in the PHA process all over India, he said. The objectives of the process would be to draw attention to poor state of the health services; the detrimental role of the globalisation process on health and health care to the poor; and the alarming process of withdrawal of State support to the health services the world over. Currently, the processes of a people’s enquiry into the status of health services and a dialogue with the health authorities asking for specific improvements are underway in over 50 talukas and urban settlements in Maharashtra. The results of this exercise will be discussed at the State Health Assembly to be held in Pune, on the 18th and 19th of November. The State Health Minister and the Director of Health Services would be invited. The delegates from all over the country would board one of four people's health trains to reach Calcutta for the National Assembly on the 30th of November and 15t of December. These trains would act as traveling health campaigns and workshops. The final event is planned in Dhaka, Bangladesh on 4th to 8th of December where 2000 delegates from many countries would meet at the International People's Health Assembly. Dr. Phadke elaborated that the PHA initiative would focus on 3 issues-

.Strengthening the functioning of the public health services

.Self-regulation/social regulation of the private health sector . Towards gender sensitivity The discussion brought out that primary care was very cheap and was well within the government's ability to provide. Also, some hard hitting facts about how the total expenditure on cosmetics in the USA and on pet foods in the USA and Europe could fund basic education, health and nutrition was communicated. The conference ended with the message that the People's Health Assembly would attempt to re-establish health and equitable development as top priorities in policymaking, with Primary Health Care as the strategy for achieving "Health for All- Now". Aruna Kartik

CEHAT 135 A/E, Military Road, Marol, Andheri (E), Mumbai 110059

Date: 18th to 20th January 2000 Venue: Yatri Niwas, Sewagram, Dist. Wardha The annual theme meet of the MFC for the year 2001 will focus on Universal Access to Health Care through Insurance; Problems and Alternatives. We request all those who have published work relevant to this issue to send the MFC Convenor a copy of their paper/ article to be circulated as background material for the meet. Unpublished papers will be tabled at the meet. Selected papers will be published in the Jan-Feb 2001 issue of the MFC bulletin. These papers must be sent to the editorial office of the MFC by 15th Dec. The Convenor and editorial committee will inform authors about acceptance of article/paper within 10 days of receipt. All those interested in participating in the meet are requested to contact the Convenor of Medico Friend Circle. MFC Convenor's Office: S. Sridhar; ARCH, Mangrol 393145, Rajpipla Taluka, Narmada District, Gujarat. Email:sridhar.mfc@softhome.net

Editorial Office: C/o Neha Madhiwalla; B3 Fariyas, 143 August Kranti Marg, Mumbai 400036 Email: mfcbulletin@rediffmail.com

Medico Friend Circle invites all readers to contribute to the bulletin. Full length articles Initiatives and Retrospectives Small write ups on activities undertaken by MFC members in their own organisations or in their individual capacity. Retrospective pieces on such reports printed earlier in the MFC bulletin. Dialogue Letters to the editor, response to articles printed in the bulletin, requests for information, assistance, opinions etc. Postbox News about MFC's activities, programmes held, being planned, actions taken (Public Interest Litigation filed, signature campaigns etc.) Personal news that you may wish to share with the MFC. Reprint Articles published elsewhere, which may be of interest to our readers. (Will be used subject to copyright rules)

Ethical guidelines in social science research - A Small Step Tejal Barai

Introduction Although the need for ethics in social science research has been long felt, the need has become more compelling because the issues that are being researched today are not only more complex, but also more invasive. With the new developments and changes taking place in Indian society, the scope of social research has increased to meet these needs and the problems arising therein. From studies of peasant movements, agrarian social structure, industrial sociology, urban sociology in the 1970's to issues such as medical sociology in the 1980's (Rao, 1982), and studies on sexuality and reproductive health in the 1990's, social research, in India, has come a long way. Adaptations and modifications were also to made in the methodology to suit the issues researched - from simple interviews to case histories and focus group discussions. In due course of time complications arose. Dilemmas that have no simple solutions needed to be handled. How does one report details about a sexual behaviour survey without offending either the public, the participants of the research themselves, and yet be able to report the findings of research? Or how can one get relevant data on issues such as abolition in a rural community, when the response of the women participants of research is very obviously affected by the presence of a relative during the interview? Moreover how can this data be made genralisable, when its validity itself is in doubt? Moreover, dilemmas have also been raised in case of research with AIDS and HIV positive patients. There are also issues related to the exploitation of research work of students and juniors. A demand for accountability and transparency from all those associated with research further increases the need to institutionalise ethics. The popular demand for ethical accountability and transparency has already emerged and consolidated itself in some countries. For instance the Blacks in the United States refused to participate in research on the grounds that the interviewers were black and that the black interviewers were "stooges" of the whites. (Barnes 1977) full stop, another example is the hostile reaction that the 1 Springdale (Vidich and Bensman, 1960 ) study received. The Springdale study, I consider, is a good example of the importance of anonymity as a right of the participants in research. It does not mean simply using pseudonyms. (Barai, 2000). This particular study had made use of pseudonyms throughout. However, specific characteristics and attributes made informants and participants were identifiable, resulting in a lot of information becoming public. The result was an outrage against the study with participants organising a parade on the 4th of July as a sign of protest. (Useem and Marx, 1983). The Indian community of researchers too has come across

not only questions from participants, such as why they were selected for the study (Visaria, 1995), but also a demand for their rights or a protest against their violations. A refusal to confront these issues not only has an impact on those who are researched but can, in the long run, affect the credibility of social science research. Thus social science research can give rise to dilemmas and issues at all stages, from the choice of study, to ensuring autonomy and rights of the participants to complexities arising out of publication, especially that of sensitive material, to the increased need for ensuring accountability and transparency. Could these issues have been handled better, had they been anticipated? Where does one look for possible solutions?

Draft Code of Ethics A national level ethics meeting to discuss "The Draft Code of Ethics for Research in Social Sciences and Social Science Research", was held on the 29th and the 30th of May 2000, in Mumbai. It was an attempt to address some of these issues collectively. It was attended by more than 50 researchers from all over the country. The process of evolution of the draft code involved the formation of a committee". The Research Secretariat was based at The Centre for Enquiry into Health and Allied Themes (CEHAT), Mumbai". Codes of ethics emerging from various were scanned. Needs, problems and complexities associated with research in India needed to be kept in mind while drawing up guidelines. Thus a review of studies from India was also undertaken. Over a period of one year, the committee met twice and discussed and debated draft guidelines drawn up by the research secretariat. The result of the effort was the evolution of the "The Draft Code of Ethics for Research in Social Sciences and Social Science Research". Prior to the meeting, the draft code was disseminated widely prior to the meeting individually and through academic journals. Presentations were undertaken by the research secretariat, at various colleges and institutes and feedback and suggestions were sought at these presentations, as well as through letters. A volume that consisted of the documented feedback, six background papers that were commissioned and some codes of ethics originally referred to, was sent as background for the meeting. The main body of "The Draft Code of Ethics for Research in Social Sciences and Social Science Research" consisted

Tejal Barai is a researcher at the Centre for Enquiry into Health and Allied Themes, (CEHAT), Mumbai.

of four sections, The Preamble (Section I), The Principles (Section 11), Ethical Guidelines (Section IV) and the Institutional Mechanism for Ethics (Section IV). The preamble dealt with the need for and the purpose of the guidelines. It laid down the need to have a consensus on the need to observe ethics in research and collectively evolve guidelines. It is required for the education and empowerment of researchers. Eventually a network of institutions can be formed to share experiences in solving dilemmas and implementation of guidelines. The principles include the Principle of Essentiality, Accountability and Transparency, and Totality of Responsibility among others. They were drawn up taking into consideration the broader principles of Non-maleficence, Beneficence, Autonomy and Justice. An attempt has been made to operationalise these in the form of guidelines. It appears in the form of ethical guidelines dealing with rights of researchers themselves and protection of their autonomy, as well as participants or subjects of the study and rights of juniors and students. The guidelines themselves address a very wide variety of ethical issues, such as the integrity of researchers, informed consent, ethical issues associated in relations with juniors and students, authorship credit among many others. Thus in effect, an attempt has been made to try and evolve ethical guidelines for all stages of research, from conception to publication of research.

An important feature of the code is that it is broad and general. The advantage is that it can be then applied to a wide variety of research issues and stages. Institutions, organisations and individual researchers can evolve specific guidelines to suit their research needs, keeping intact the spirit of the code. The Plenary address was given by Dr. Ghanshyam Shah and touched" on various issues that are relevant to social science research today. He pointed out that applied research, as it is conceived today, leaves very little scope for the understanding of the complexities of society. Policy research, today, implies that it is "tailor made" to serve the priorities and the policies of the government.

He cautioned that accountability and commitment to the growth of knowledge and to the needs of participants is fast declining due the compulsions placed on researchers by the scarcity of resources, increasing sponsored research and the pressure from funding agencies. No guideline will be able capture all complexities of a dynamic social situation. It could even carry with it the possibility of hampering research to a certain extent. However, the alternative is to accept violations of autonomy and freedom of a large population, including the researcher, the participants and those who might eventually get affected by the study. Where the credibility of the social sciences is at stake, a code of ethics is thus not only desirable, but also imperative.

The group discussions among participants raised several issues accountability should be seen in terms of professional, social as well as financial accountability. Researchers should have a right to opt out of an unethical research project.

Further, relevance of research should be laid down firmly and be based on prioritisation rather than exclusion. It was felt that ensuring privacy at the time of data collection though difficult in our country; was very essential and cannot be excluded from any ethical guidelines. Some of the participants of the meeting strongly felt that the ethical issues associated in research involving-the mentally ill need to be addressed separately. Moreover, proxy consent was found unacceptable. Consent from parents should not be required for children above the age of 14 years. Children below the age of 14 years should have the right to veto the consent given by parents. Results of research should be shared with the participants of the study. Authorship credit should also take into consideration the contribution made in terms of ideas and solving of problems and actual research. Section IV, Regarding the institutional mechanisms of ethics, these should be evolved through a participatory process within organisations in terms of designing policies, scope, role and composition. It could also be made independent of the guidelines. The guidelines, it was felt, were broad enough to be applied and adopted to various situations.

Conclusion The meeting was successful in gaining a consensus among a wide community of researchers from across the country for the need to observe ethics in research and to collectively evolve guidelines for research, drawing from the draft that was presented. The committee and the research secretariat would meet and finalise the guidelines, incorporating the suggestions received at the National Meeting. These are to be disseminated widely. The entire effort together with background papers is to be brought out as a volume. Efforts need to be made to get them accepted by funding agencies and other institutions and organisations. The meeting can be seen as the beginning of an effort of researchers from across the country to highlight the issue and promote the practice of ethics in research. It also filled a lacuna by providing some means to guide research, which are specific to our needs and evolved by our own researchers.

(The committee and the research secretariat met and have finalised the guidelines. For a copy of the document, write to Tejal Barai at cehat@vsnl.com) Notes

1. Useem, M. and Marx, G., Ethical Dilemmas and Political Consideration. An Introduction to Social Research, Volume I, of Handbook of Social Science Methods, Edt. Robert Smith, 1983. 2. Ghanshyam Shah, Lakshmi Lingam, VR Muraleedharan, Padma Prakash, Thelma Narayan, Ashok Dayalchand, Manisha Gupte, Sarojini Thakur, Geetanjali Misra, Radhika Chandiramani. 3. The Research Secretariat was composed of Amar Jesani and Tejal Barai 4. Dr. Ghanshyam Shah gave the plenary address at the National Meeting. This section is based on that address.

Registration Number: R.N. 27S6Sn6

References Barai, T., "Ethical Issues in Social Science Research - Some Basic Concepts", 2000. (Unpublished Paper)included in Background Papers at the National Ethics Meeting to discuss "The Draft Code of Ethics for Research in Social Sciences and Social Science Research", held on the 29th and the 30th of May 2000. Barnes, J. A., The Ethics of Inquiry in Social Sciences, 1977 "Ethics in Social Sciences and Health Research - A Draft Code of Conduct, Economic and Political Weekly, March 18 - 24, 2000 M. S. Rao, Sociology in the 1980's: Retrospect and Prospect, in Sociology in India, Nayar, 1982 Useem, M. and Marx, G., Ethical Dilemmas and Political Consideration in Smith R. (ed) An Introduction to Social Research, Volume I, of Handbook of Social Science Methods, 1983. Visaria, P., Visaria, L., Jain, A., "Contraceptives use and fertility in India - A Case Study of Gujarat, 1995

The Medico Friend Circle (MFC) is an all India group of socially conscious individuals from diverse backgrounds, who come together because of a common concern about the health problems in the country. MFC is trying to critically analyse the existing health care system while searching for a system of health care which is humane and which can meet the needs of the vast majority of the population in our country. About half of the MFC members are doctors and medical students, the rest include researchers, health and gender activists, community health experts, public health professionals, academicians and students from different disciplines. A loosely knit and informal national organisation, the group has been meeting annually for more than twenty five years.

Subscription Rates Rs Indv.

U.S $ Inst.

Asia

Rest of world

Annual

100

200

10

15

2 years 5 years Life

175 450 1000

350 925 2000

.. .. 100

.. .. 200

The Medico Friend Circle bulletin is the official publication of the MFC. Both the organisation and the Bulletin are funded solely through membership/ subscription fees and individual donations. Cheques/ money orders to be sent in favour of Medico Friend Circle, directed to Manisha Gupte, 11 Archana Apartments, 163 Solapur Road, Hadapsar, Pune - 411 028. Please add Rs. 101- for outstation cheques

MFC Convenor's Office: S. Sridhar; ARCH, Mangrol 393145, Rajpipla Taluka,. Narmada District, Gujarat. Email:sridhar.mfc@softhome.net

Editorial Office: C/o Neha Madhiwalla; B3 Fariyas,' 143 August Kranti Marg, Mumbai 400 036. Email: mfcbulletin@rediffmail.com

Contents Page

1

Editorial Towards Universal Health Insurance: A Rural Perspective

.. Shyam Ashtekar

2

.. Anil Gumber

6

Extending Health Insurance to the Poor: Some Experiences from the SEW A scheme

13

Dialogue

17

Postbox Ethical Guidelines in Social Science Research: A Small Step

. .Tejal Barai

18

Editorial committee: Neha Madhiwalla, Sandhya Srinivasan, Meena Gopal, Tejal Barai. Editorial office: c/o Neha Madhiwalla, B3 Fariyas, 143 August Kranti Marg, Mumbai - 400036; Published by Neha Madhiwalla for Medico Friend Circle, 11 Archana Apartments, 163 Solapur Road, Hadapsar, Pune - 411 028. Printed at Pradish Mudran, Mumbai - 400 004. -

Views and opinions expressed in the bulletin are those of the authors and not necessarily of the organisation.


