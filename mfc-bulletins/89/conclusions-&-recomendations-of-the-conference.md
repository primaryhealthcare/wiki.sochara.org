---
title: "Conclusions & Recomendations Of The Conference"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Conclusions & Recomendations Of The Conference from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC089.pdf](http://www.mfcindia.org/mfcpdfs/MFC089.pdf)*

Title: Conclusions & Recomendations Of The Conference

Authors: Mankad Dhruv, Anant, Padma & Satya

medico friend circle bulletin MAY, 1983

PEOPLE AND HEALTH - A BRIEF REPORT ON THE DHAKA CONFERNCE - DHRUV MANKAD 'People and Health - was the topic of the conference jointly organised by Gonoshasthya Kendra, and Jehangir Nagar University, (Bangladesh) at GK campus from 18th to 22nd March 1983. Around 70 delegates from Bangladesh and 30 delegates from abroad (13 from India out of which 6 MFC members) took part in the 5 day long deliberations with a view to lay down some guidelines on how a new medical curriculum - an alternative to the present extant is to be formulated the present medical education is hospital centred, urban biased and aimed at an individual patient. The participants also included around twenty five students and interns from various medical colleges in Bangladesh. On the eve of the official inauguration of the conference the organising committee and the delegates present decided upon the following Programme: DAY-1 SESSION-I Inauguration ceremony.

DAY -3 SESSIONS-I Workshop III: Traditional and Modern Medicine. SESSION-II 1. Plenary Session: presentation of resolution and minutes of the three workshops. SESSION-III 2. Working group discussion on the resolutions of the three workshops and formulation of the final public statement. SESSION-VI

DAY -4 1. Visits to Mymensingh and Barisal Medical Colleges by two teams of foreign delegates. 2.

SESSION-II Keynote address by Dr. D. Banerji, Jawaharlal Nehru University, New Delhi; Discussion on the Key note address. SESSION-III, IV - Country papers and discussion. DAY -2 SESSION-I Country papers and SESSION-II Discussions. SESSION-III Workshop I: Medica.1 Education & Social context. SESSION-IV Workshop II: Social Sciences and Medicine.

Plenum: - Presentation of the public statement and discussion.

Organising committee along with interested foreign delegates to discuss some details regarding the prerequisites, set-up content and methodology of training for the proposed alternative medical school.

DAY -5 Morning: Public meeting at Dhaka in the presence of exministers of Heath and release of the public statement. On Days 3, 4, and 5 the visiting delegates were shown round the Gonoshasthya Kendra Campus, including their School, the women's centre, the workshop and the factory of Gonoshasthya pharmaceuticals Ltd. A brief report of the deliberations follows:

KEYNOTE ADDRESS: Dr. D. Banerji in his keynote address pointed out that the introduction of Western Medicine in the Indian subcontinent was intended to serve the British Army and deprived the natives of their own indigenous system by destroying the economic base of their whole way of life. He contended that it was only the presence of conscious doctors like Dr. B. C. Roy within the mainstream of the anti-colonial movement that a national health policy was formulated after independence. He argued that though change in health status of the people is a function of the social economic change, a 'critical mass' of conscious doctors who could initiate such a change in the health status are necessary as a part of the broad movement for socio-economic change. He urged that it was with the aim of producing such a. 'critical mass' that the new alternative curriculum be formulated. He stressed the need to orient the new doctors towards community diagnosis and action.

The discussion that followed his Keynote speech in which many eminent teachers- from Bangladesh took an enthusiastic part was centered around mainly two points: 1.

Concepts of 'indigenous' versus western medicine.

2.

The way in which community medicine was taught today.

Dr. Banerji replying to a comment "there is no dichotomy in science and scientific truth is universal; neither indigenous nor 'Western''', argued that while one has to accept the scientific core of Western medicine it is a historical fact that the British having totally disrupted the Indian Social structure and the indigenous ode of living, had imposed on them a totally alien way of life and medicine. The concepts and practice of Medicine did not grow naturally out of the Scientific-cultural milieu in the Indian Sub continent but was transplanted over its ruins from above. A very pertinent point was raised by a young intern from Dhaka, Dr. Mahbub, regarding the teaching of Social and community medicine which was only a subject, not an attitude with the teachers. Only a. professor of PSM was supposed to teach it. While rest of the medical educationmedicine, surgery etc, deal only with its clinical aspects and not with the' orientation for making community diagnosis. Thus, the student who regards the clinical teacher as his or her idol never inculcates a community oriented approach to health problems.

This discussion was excellently summed up by the Chairperson Prof. Rehman Sobhan a famous economist from Bangladesh, with a thought provoking question: In a society dominated by market forces, where all commodities are on sale at the highest price possible, how can one expect, without touching these forces that the doctor should offer his or her service which itself is but a commodity, not at the highest return on his or her investment but where the services are most needed?

COUNTRY PAPERS: Delegates from Sri Lanka, Philippines, Malaysia, Nepal, Mozambique, Bangladesh, North Korea and Thailand presented their papers. The papers generally covered the health situation, the health setup of each country and the alternatives tried out in their respective countries.

The more enlightening of the experiences shared were those from Mozambique and Nepal in both these countries there was a virgin ground - no entrenched doctors' lobby!

Dr. Carlos in his paper pointed out the appalling conditions of the health services at independence, most doctors having left Mozambique. Under such difficult circumstances and in face of a stiff opposition from the vested interests amongst the remaining doctors of his team, set about reforming the health service structure recalling some of the experiences of the liberation army during the war of liberation. A four tier system, the topmost rung being that of a specialist doctor, was formulated. A parallel system was organised for nursing services with scope for vertical mobility along each rung. Now the candidates have to be selected by the village council and have to go back to serve the people of the village. The health workers are trained in diagnosis and management of minor ailments and management of community health problems. They are taught community diagnosis and health survey skills. The new rational health-system has achieved fantastic results. Dr. Prassai from Nepal reported in his paper the experiment being carried out in the medical school at Kathmandu which started with a course for village health workers. Later a course for training of doctors was started. Now, the school admits only those who have worked as health worker. For some time the emphasis is on community health work right from the first year. The students are allowed to assume respon-

sibility from the first year. Emphasis is laid upon field work with the student spending nearly nine months at a rural health centre during the five year course. The Indian delegation made a, powerful impact during the discussions and this was appreciated by many participants. The home-work we made here at Anand and at Bombay proved very useful. Since Dr. Banerji had already dwelt upon the salient features of Indian situation, yet another country paper, shared with the delegates 'a few of the experiments carried out in India and Anant Phadke reported the gist of the discussion on alternative medical school at Anand during the Annual meet as well as at Bombay prior to this conference.

Abhay contended that it is not enough to have good intentions of and to do sincere efforts towards training an alternative doctor. It is imperative that all the aspects especially the social context of the candidate, the setup, the methodology and the content of the alternative doctors' training must be properly worked out. To substantiate his contention, the cited the examples of two Indian medical colleges which were started with the idea of training doctors for rural service and which failed. He also described another experiment that did not succeed an experiment of integrating Ayurveda with Modern medicine - known as the Jamnagar experiment. It failed because of a lack of communication between the Ayurvedic and Modern doctors, each one having been trained to think in terms of entirely different concepts.

often the nurse or even a woman doctor is look upon by the men doctors, male patients and their me relatives as an object of sexual pleasure-real or vicious. Some reactions from the participants in fee underscored her point that a drastic change is need in the negligent attitude of existing medical professor towards women.

Following notes papers, prepared by the MF members were circulated in the workshop: 1)

"Medical Education an Alternative" by Ashwin Patel, a report of the discussion at Anand.

2)

"Towards a Clinical Syllabus for Alternative Medical Curriculum" by Anant Phadke based on the discussion at Bombay.

3)

"Ideology and Sociology in Medical Education" by Anant Phadke,

4)

"Woman's Health - Who Decides the Poling? By Padma Prakash.

"Integration of Traditional Medicine with modern Medicine in the proposed alternative Curriculum" b Dhruv Mankad. Two other notes not specifically prepared for this workshop were reproduced and circulate by the Organizers (1)

In this connection, Anant reviewed the discussion that took place amongst the MFC members at Anand on the alternative medical curriculum. He reiterated the MFC view that an alternative can be forged out only in the context of a broader movement aimed towards an alternative society. He also stressed that for any alterative to be valid and lasting, the doctor-trainee must be selected from a progressive social movement - be it a movement of peasants, workers, students, or even a progressive development project and must have a similar social milieu to work in after the completion of the training. He also elaborated other points discussed at Anand and Bombay.

Satyamala & Padma Prakash criticized the present health system for being baised against the' women. The contended that often doctors treat women's complaints very casualty, labeling her as neurotic: Even when she's taken seriously it is only because she is a potential mother. They argued that the nurse's role is not only a subservient one to that of the doctor, but

"Prejudices in medical system against Female Health Functionaries" by Rani Bang - paper pre pared for the recent Annual MFC meet at Anand

(2) "Politics of Medical Work" by Anant Phadke. WORKSHOPS: The plenum divided itself into three workshops: Workshop-I

On medical education and social context.

Workshop-II

Social Science and Medical Education.

Workshop-III

Traditional and Modern Medicine.

We had expected that in these workshops there would be an in-depth discussion amongst like-minded people. But we were confronted with a different situation - the group was quite heterogeneous. A lot time was therefore spent in arguing hew the product of existing medical education is not properly trained for the task he/she would be expected to do as the medical officer at a Thana, why alternative medical curriculum is necessary, what should be the principles of the alter-

native curriculum etc. The level of the discussion did rise above preliminary level but the discussion was relying. It was heartening to see so many doctors spiritedly arguing with each other on the medical education such enthusiasm and spirit would not be seen actors in India. This keen interest reflects the situation in Bangladesh. Things are hotter in Bangladesh, memories, issues, struggles of the Bangladesh librations war are still fresh there, A working group was set up which formulated recommendations of the conference taking into consideration the discussions that took place in these

workshops. These recommendations were presented for final

discussion

in

the

last

plenary

session.

The

recommendations are given below. It will be seen that these recommendations are quite general and whether the new medical

college

established

would

pattern

be

really

different

would

depend

upon

from how

the

these

recommendations are interpreted concretized and brought into practice. The real task lies ahead. The Organizers have won the first round by getting these recommendations passed in a big public conference. Acquiring legitimacy to their experiment atleast at the level of inception.

CONCLUSIONS AND RECOMMMENDATIONS OF THE CONFERENCE In the opinion of this conference the health care medical education in the most Third World countries is not satisfactory and there is a growing restlessness on this issue among the people as well as within the conscious section of medical profession. Attempts are being made to bring medical education more closely aligned to the health needs of individual country and this is reflected in the recent curriculum revision here in Bangladesh. Though the socio-economic factors like poverty, literacy and politics are the major determinants of the health status of the people the health services have an important role to play. Constraints do exist in the sent situation but health care workers will have to undertake the responsibility of trying to improve the health status of the people within the existing constraints instead of sitting silently and waiting for the deal conditions.

To remedy the present day system of health care is necessary to go to the people and subordinate medical technology to the needs of the people, rather than to serve the interests of the upper class. The conference took into account the health care items of the various countries in Asia and noted the majority health problems. The target groups identified enquiring immediate attentions are – Children, Women, and under-privileged classes in urban as well as rural areas. To deal with the predominant health problems of the target groups through primary health care a community health team approach is essential. In most cases Doctors as the leaders of the team will have to carry out this task currently. Doctors are not trained to perform this role adequately.

The new role will require a different kind of preparation so that doctors can effectively' perform the following functions: 1)

Diagnosis of health problems and priorities of the area, using scientific epidemiological tools.

2)

Planning, execution and evaluation of a community, health programme in accordance with the community diagnosis.

3)

Competently diagnose and treat the common as well as important clinical problems.

4)

Training of the team including the village health workers.

5)

Supervision and leadership of the health team.

6)

Act as a change-agent in the health system.

7)

Understanding the problems of the masses and actively associating with them.

To enable the new Doctor to be able to perform above functions the major areas in which Medical Education needs restructuring are as follows:(Sentences in the bracket would give an idea about hew these general recommendations can be interpreted as was done in an informal discussion amongst like minded people after the workshop was over) 1. Selection of students - giving special attention to the social origin, sexes the rural background as well as the values and, motivation of the aspirants. (Twenty committed students will be selected; half

of them would be females and half would be paramedics. Paramedics would undergo a year's preparatory training in general and biological Sciences). 2.

The location and organisation of the training set up Half of the training must take place in the community or in the community based health programme., (Half the time round be spent in a middle sized, district level hospital and half in oriented well, designed health' project in rural area).

a socially

3.

Clinical Training - with a commitment to excellence and emphasis on, the practical methods of diagnosis and management of the priority problems (Training would not take place in big, sophisticated hospitals. Maximum emphasis would be placed on practical problems. Preclinical training (especially, Anatomy, Biochemistry etc.) would be limited to fundamental and applied aspects and details would be avoided. These subjects be taught by clinical teachers - e.g. Anatomy by Surgeons. Preclinical and clinical aspects to be taught simultaneously and in an integrated manner).

4.

Community Health Skills - To be developed in applied training, so that the Doctor can fulfill the new role. (Students be given specific practical responsibilities during training).

5.

5. Teaching of social sciences - especially understanding economics sociology, anthropology, psychology and ethics as they relate to the genesis and management of health problems.

6.

The medical education institution should have a Department devoted to clinical research, and practice of traditional and non-allopathic systems of medicine. The research should be carried out using combined diagnostic and therapeutic techniques of the traditional as well as the modern medicine. The remedies found to be effective and safe should be actively included in the teaching and be practised.

7.

Personality development i.e. attitudes and values in personal life as in relation to the people, especially with under privileged classes and women; also an ability to work with a team. (Students imbibe the attitude of' their teachers. Hence the teachers must practice these values in their daily work).

It is not possible to undertake this during and innovative task in the existing system of medical edu-

cation. A new experimental medical education institution needs to be set up which will need flexibility and autonomy in the fields of administration, management, curriculum development and training method. It will also need necessary academic recognition and support. (Jehangir Nagar University may open a new medical college of 20 students with the help of Gonoshasthaya Kendra. All aspects of medical education I curriculum, selection of students, teaching methods E would be formulated and implemented in an autonomous manner, without regard to recognition by the Bangladesh Medical Council. The product of this Medical College can very well work in voluntary projects. He/she is superior to the existing doctor, society would recognise them in due course).

The discussion that ensued showed that many of the participants especially some of the establish doctors, teachers and students from Bangladesh not agree with many suggestions put forward.

One such recommendation that was serious contended was about spending more time in the field to learn from the people. Some participants felt that anyway people had little to contribute to the knowledge of a doctor and the time was better spent over acquiring the theoretical knowledge of the diseases and their management. Another recommendation that came under severe criticism from some students as well as some professor was about incorporating the study of economic sociology etc. as related to management of health problems. Some student felt that it would be too much of a burden for a medical student to learn subject that have no direct bearing on the clinical practice While a professor felt that if economics is taught the medical students that medicine should be taught the economics students. It was clarified that only t much economies sociology etc. would be taught as related to the healthcare-delivery systems and gene of health problems i.e. political economy of health.

The suggestion of opening a new medical college was opposed initially by a large number of student and professors who felt that it was a luxury for a poor country likes Bangladesh where there already were eight medical colleges and where already hundreds of doctor were unemployed. Ultimately, it was left to Abhay convince the plenum that such a step was indeed necessary by reminding the participants that the decision the conference was about to make was a historic one. With this, probably a new era in medical education was opening and all the developing countries were looking

upto Bangladesh to lead the way as it did in the matter if implementing the drug policy. Thus, the main programme of the conference ended on a very sentimental but an optimistic note.

VISITS TO MEDICAL COLLEGES: Two teams of delegates from outside Bangladesh went to two different medical colleges-one to Mymensingh and the other to Barisal to discuss the objectives of and the decisions taken at the conference. The common experience of both the groups was that the medical students and teachers of both the medical colleges showed a keen interest in the conference and its outcome, but much groundwork will have t0 be done by the organisers to convince the medical profession of the necessity of an alternate medical school. PUBLIC MEETING: It is difficult to report what went on during the public meeting as it was conducted in Bengali. But it was reported that the meeting was a success. Some of the ex-Ministers of Health attended it and under the pressure generated by the conference they had to admit to certain mistakes in the past and had to make some concrete promises.

leaflets for distribution to the medical students, teachers and the general public. They persuaded over two hundred doctors to sign a statement condemning the use of DepoProvera. A lot of debate went over the issue in the newspapers. Although this campaign was not successful, it had its effects on the campaign against Norplant. Some time before Norplant was to be approved for use in the National Family Planning Programme, they started talking to the doctors of the hospitals in Dhaka city. Two days before the probable date of approval, they published newspaper articles and distributed leaflets. As a result, Norplant was rejected. Later, the Depo-Provera Campaign was revived. Three months back, the manufacturers of Depo-Provera held a seminar in order to promote the product. The doctors from the post graduate Institute in Dhaka came out with posters and handbills and forced the chairman of the seminar to let them state their views. They only partially approve of the new drug policy in Bangladesh because they think the drug policy has several lacunage — 1.

Drug policy is not a part of any National Health Policy. It is essential to have one.

2.

An isolated stand against foreign domination in drugs cannot last long unless there is an attempt to free the whole economy from the hold of big multinationals.

3.

It has hit the local manufacturers the most, since they have not been given assistance in the matter of finance and technology to change over to the production of essential drugs.

4.

It is inadequate as it does not provide the Government with any control over either the purchasing of raw materials or over the marketing and distribution systems. Depo-Provera has not been banned.

5.

The production of drugs by these Multinationals has gone down and this has been used by these companies to layoff the active unionists in their factories. Neither the Govt. nor GK has done anything about this.

DHAKA MEDICAL COLLEGE STUDY CIRCLE: Some very interesting interactions took place outside the conference itself. The most fruitful of these was with a group of medical students, interns and young doctors — the Dhaka Medical College Study Circle. It was started in 1978 with the aim to fight within the system to make education and health services more relevant to the needs of the people. Right now, there are about forty members. They are all very active in the students' organisation and they make an attempt to educate the students on alternatives on health and to try to make health' a socio economic and political issue. They began by organising seminars and meetings and meetings on various aspects of health initially they thought that health was mostly technology. Later they put more stress on health education but now after a long experience have realised that health is politics. New their activities include a Clinical Board - a sort of wall paper on common health problems and common diseases stressing on the community aspects and suggesting a rational line of treating and preventing the conditions. The group takes up some live issue for campaign amongst medicos and non-medicos. They were the first in Bangladesh to launch a campaign against the extensive use of Depo-Provera and Nor Plant in the national population control Programme. During this campaign against Depo-provera, they distributed hand-outs to journalists, and politicians. They prepared

They pointed out that the multinational were allowed to criticise the Government's drug policy through newspaper ads, while they were not. Those of us who could meet this group (Dhruv, Anant. Padma, Satya) felt happy at having gained friends in Dr. Mahbub, Dr. Rashid, Dr. Jehangir and others of the Study Circle.

LETTER TO EDITOR Dear Friend,

Few of us fail to be shocked by the revelations behind the issue of steroidal and other forms of artificial and marketable contraception, and implicit threat to women's health and autonomy. Natural Family .Planning methods, long scoffed at as "ineffective", are drawing more and more serious attention from women in view of the persistent risks attributed to most if not all of the artificial methods. The outstanding advantages of the natural methods (symptomato-thermal or cervical mucus method) are low or no cost and noninvasiveness (by chemicals or physical objects). The major disadvantages are (1) the practical difficulties encountered by each woman in detecting the oncoming and passing of the event of ovulation (when she is fertile) in her menstrual cycles end (2) the fact that she still can be physically overcome by her husband or any other man during her fertile period while she is unprotected by other means of contraception.

However, in the context of a powerful women movement, where women can not be so easily over come, and where women can hope to exercise the intelligence to its full potential, natural methods will b revolutionary. The necessity for a male sexual partner to be understanding and respectful of a woman for this method to work cannot be seen merely as a disadvantage. We must tackle certain scientific problems which limit the effectiveness of this method for mass us (e.g. how to detect ovulation during lactation amenorrhea and long or irregular cycles). We must learn to use the method thoroughly ourselves. We must lean exactly how to teach it to our sisters responsibly and in vast numbers.

— Mira Sadgopal Kishore Bharati Group P.O. BANKHERI, Dist. Hoshangabad,

MP 461 990.

IS BCG VACCINATION USEFUL? KAMALA S. JAYA RAO The efficacy of BCG vaccination has never been questioned by most clinicians. We have perhaps assumed that the function of a vaccine is protection and therefore BCG vaccine will protect against tuberculosis. The protection, however, has never been complete. As WHO report says "BCG vaccine has been used extensively in tuberculosis control programmes since the early 1950's, when, for many countries it was the only feasible antituberculosis measure. At that time, it was known that protection from BCG vaccination was not complete, but there was little quantitative information in that respect. For this reason, several controlled field trials were undertaken. The results of these trials were contradictory, protection varying from nil to 80%. The main hypotheses put forward to explain this variation were that in some trials a vaccine of low potency had been used, and that infection with mycobacteria, other than mycobacterium tuberculosis had provided some natural protection against tuberculosis, thus 1 masking the effect of BCG vaccine". BCG vaccination continued to be recommended as an antituberculosis measure nevertheless. It was however soon recognized that no field trial was undertaken in developing countries. Therefore the ICMR, with

assistance from the WHO and the American government, conducted a 7½ year carefully controlled trial in Chingleput, Tamil Nadu. The study was planned sometime around 1968. Chingleput is highly endemic for tuberculosis. It also has a high incidence of leprosy and filariasis. It was not covered extensively by the national BCG programme. The initial surveyor "intake" comprised among other things tuberculin testing. X-ray examination and sputum examination. Of those identified as eligible on basis of x-ray examination nearly 3, 50,000 people were included in the study and all except infants below 1 month were tuberculin tested.

Two batches of tuberculin, PPD-s and PPD-B, were used for skin testing. Prevalence of the disease, as assessed by a reaction of 12 mm or more to PPD-S, was 54% in males and 46 % in females. The prevalence increased with age upto 25-35 years; and in males by 25 years of age it was 80% and in females, by 35 years it was 70%. Taking a reaction of 10 mm or more to PPD-B as evidence of infection, after the age of 15 years almost everyone was a reactor. It is believed that "this massive sensitization is caused by environmental mycobacteria, 2 which however, rarely cause living disease in man".

On the basis of x-ray findings, the prevalence of pulmonary tuberculosis was 1429/100,000 in males and 1978/100,000 in females. Those with two cultures positive who are definite bacillary cases were 598 males 205 females, per 100,000. Those with a single Ire positive were 994 and 237, respectively. According to the experts, the latter cases are considered to be mostly 'early cases', but there may be some false positives also. The population was then administered one of the strains of vaccine chosen for the study. As mention earlier, the study was carried for 7½ years and follow-up was done at 2½ years and 4 years, also. The results showed that BCG vaccination, over 7½ years, had no effect in offering protection against development of pulmonary tuberculosis, There were r salient findings in this study: 1.

A very low disease to infection ratio.

2.

Men were affected four times more than women.

3.

The large majority of cases occured among those already infected at intake.

4.

An almost universal skin sensitivity to PPD-B. The study report concluded that after taking all

possible reasons into account, "The high incidence of infection, the low incidence of disease among nonrectors associated with a high incidence of disease among reactors suggest that a large proportion of cases occur not as a result of primary infection but as result of 2

either endogenous reaction or exogenous infection" .. "It

designed to test the efficacy of BCG in infants and children. The relevant information on children is thus fragmentary. In view of the high endemicity in many parts of the country, it is perhaps a correct view that BCG vaccination of infants should not be given up. The serious forms of childhood tuberculosis, namely, miliary tuberculosis and tuberculous meningitis, are said to be often fatal, even if chemotherapy is given. Let us assume that unlike in the already infected adults, BCG vaccine will afford protection to infants and young children. The question that needs to be settled is, under these circumstances, till what age will the child remain' 'a good candidate' for protection? Ideally, BCG vaccine has to be administered in the neonatal period. However, considering that most births occur at home with no trained health personnel at hand, and in view of the various bottlenecks in the health delivery system, this idea situation will not be achieved. The Chingleput study showed that the incidence of disease even among the 1-4 year olds in the control or placebo population was nearly 2%. There is, therefore, an urgent need to know whether in a highly endemic area infants will be firstly protected and if so, till what age they respond. BCG vaccination is now a part of the ICDS programme and if we do not get an answer to this question early enough, the money spent on it may merely go down the drain. The Chingleput study once again underscores the point that vaccines are but of secondary importance in the control of disease in a highly endemic area. Unless concomitant and sincere efforts are made to control and improve the environmental factors, benefits from immunization programmes may not be commensurate with the money expended. (The. word environment is used in a broad perspective which includes socio-economic factors). It will not further dilate on this issue, since I have already done so in my editorial in Bull No. 79 (July 1982).

appears therefore, that while the infection rate is high in this study population and possible not declining, newly infected

persons

develop

disease less

frequently.

Tuberculosis is highly prevalent but only among the

REFERENCES: 1.

Vaccination Against Tuberculosis. WHO Tech. Rep. Ser. No. 651; 1980.

2.

Trial of BCG Vaccines in South India for tuberculosis prevention. Indian J. Med. Res. July Supplement; 1980 (for abridged version see; IJMR 70: 349; 1979).

3.

Bull. WHO 57: 819; 1979.

middle aged and especially, elderly men — individuals who must have been infected mainly years, even decades ago. It is possible that it is not the primary infection but rather super infection in the most already allergic from a previous infection that is e case of the 3

'adult' type of lung tuberculosis" . The study thus shows that in an area of high endemicity, adults and older children may not be protected against tuberculosis by BCG vaccine. However, the reports warn that the results of the study may not be extrapolated to infants, since infant tuberculosis was not observed in this trial. The field trial was not

Editorial Committee: Anant Phadke Padma Prakash Ravi Narayan Datar Ulhas Jajoo Kamala Jayarao - Editor

Views & opinions expressed in the bulletin are those of the authors and not necessarily of the organisation.


