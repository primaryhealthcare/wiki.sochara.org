---
title: "Life In The Vaccine"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Life In The Vaccine from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC088.pdf](http://www.mfcindia.org/mfcpdfs/MFC088.pdf)*

Title: Life In The Vaccine

Authors: Review Of International Development Research Centre

medico friend circle bulletin APRIL, 1983

APPROPRIATE STRTEGY FOR CHILDHOOD IMMUNISATION IN INDIA Adapted from a series of three articles of T. Jacob John and M. C. Steinhoff VINEET NAYYAR arid L. SHARADA C.M.C., Vellore One sixth of the world's children are in India. Measles, whopping cough, poliomyelitis and other immunisable diseases continue unabated and are the major causes of morbidity, mortality and deformity. The current system of vaccine delivery has not achieved adequate coverage. Of over 20 million children born in India, only about 6 million received their first dose of DPT {1}, whereas the rest (14 million = 70%) remained unprotected. These facts necessitate a fresh look into our immunization strategy. The techniques and tactics of immunization developed elsewhere may not be suited for our conditions. Hence, there is an urgent need to evolve a strategy that is appropriate for our conditions.

i)

The characteristics of the disease to be prevented, including both the incidence and the squeal.

ii) The effectiveness of the vaccine. iii) The safety and relative risks of the vaccine. Each vaccine can be evaluated thus and scores given. Vaccines can then be arranged in the order of priority according to the total scores. This has been 15 done in Tables 1 & 2. TABLE - 1 Vaccine Against

Recent experience with small pox vaccine has taught us some important lessons. A shift from the traditional strategy of systematic vaccination to surveillance and containment vaccination resulted in the eradication of small pox (2). The first step in this achievement was the assigning of high priority to the elimination of small pox. To proceed, therefore, with the problem at hand, it is important to assign priorities for vaccines in order to achieve the maximum benefits. A simple and realistic method of doing so is suggested, using available information.

Diphtheria Whooping Cough Tetanus Polio T. B, Small Pox Measles Typhoid Cholera

Product of Need

Efficacy

Safety

the three

1 3 3 4 4 0 4 2 1

4 2 4 2 or 4* 1 or 2 4 4 2 1

4 2 4 4 3 2 4 3 3

16 12 48 64 or 32 24 or 12 0 64 12 3

* Depending on 'cold chain'. TABLE - 2

A.

PRIORITIES:

The overall importance of a vaccine in a country depends upon:

1) Measles

4) T.B.

2) Polio

5) Diphtheria

3) Tetanus

6) Whooping cough

Diseases with a higher incidence e.g. Tuberculosis, or serious consequences. e.g. poliomyelitis", or both e.g. Measles and whooping cough4, 5 receive high marks. Similarly, diphtheria, tetanus, measles, small pox, rubella and mumps vaccines are very highly efficacious with protection rates of over 95%, and therefore score 4 marks for efficacy. A vaccine causing very little unpleasant reaction, and no risk to health or life ego Measles, diphtheria, tetanus, and polio, gets the, maximum marks for safety. From the Table, it would appear that measles, polio and DPT vaccines are the most important ones for Indian children. Therefore, it s recommended that these three should be considered the core vaccines for routine use. There is low priority for typhoid, mumps, rubella, tuberculosis and cholera vaccines for routine use, although a more efficacious tuberculosis vaccine, if discovered, would call for a revision of priorities.

Having thus sorted out the vaccines rationally, we are in a position now to examine current practices, and to suggest alternatives in our immunisation policy.

B.

IMMUNIZATION SCHEDULE:

Too many conflicting and confusing schedules are recommended by different experts for the im6-11 . Most of them remunisation of our children commend measles, poliomyelitis, diphtheria, pertussis, tetanus, BCG and Small Pox, but opinions vary regarding the number of doses and timing of these vaccines. The country needs a single schedule reflecting national policy. The adoption of a single, realistic, and a need oriented schedule will provide a firm frame work for health care personnel to apply a uniform policy for teaching parents and immunizing children. In formulating the schedule, epidemiological, immunological and logistical considerations are relevant. Epidemiological information has already been utilized in identifying the vaccines necessary for routine use. Immunological data suggest that 5 doses of OPV are necessary to protect 85-90% of infants 12 and children from poliomyelitis in India . It is obvious that these 5 doses are necessary prior to the period of maximum risk i.e. 7 to 12 months of age. Similarly, on immunologic grounds. measles vaccine should be recommended at 01 after 12 months of age and not earlier. For logistical reasons too, '12 months appears to be a better

age point than 9 months which is recommended by some schedules, Again, for logistical reasons, there is a need to minimize the no. of visits to achieve maximum acceptance. Both DPT and OPV should be administered together at a single visit, starting at 1½ months, so that the child is immunized against these diseases by 6 months of age. Most of the recommendations for booster doses are based in conjecture or schedules deployed by other countries. In our communities, because of more intense circulation of various infectious agents, it is not inconceivable that booster doses may be unnecessary. Specific data on the benefit of one or two booster doses is, however lacking.

Unfortunately, current immunisation schedules do not seem to take any of the above mentioned facts into account, and recommend as many as 20 visits at varying intervals for complete immunization. Equally distressing is the fact that higher priority items like measles and polio vaccines are still imported although the manufacture of polio vaccine in India, is expected to commence very soon.

Presently two factors impose severe limitations on the usefulness of a schedule as a tool to achieve systematic and wide coverage of immunization -

i) Parents should be well informed about the schedule, and be highly motivated to bring their healthy children for immunization to an institution usually meant for treatment of the sick.

ii) The institution should procure vaccines and store them throughout the year. Therefore, it is necessary to consider alternative, appropriate, community based strategies of achieving high immunisation rates. Such new strategies should not be bound down by an inflexible immunisation schedule, but should be free to modify it, in order to improve immunisation coverage.

C.

COMMUNITY BASED ANNUAL PULSE IMMUNIZATION:

Recommended here are merely the modus operandi for the government to make available a minimum number of weft chosen vaccines to every child in the country. This strategy is an indigenous

adaptation of the mass immunisation approach as opposed to the conventional strategy of clinic based sporadic immunization. The latter has been successful in developed or authoritarian and highly disciplined or small countries but in India. it has failed to make a meaningful impact on the incidence of any disease. On the other hand, pulse strategy, based on the responsibility of health institutions for immunization of local children, is likely to achieve better coverage because the vaccine is taken to the children, rather than vice versa. For reasons already discussed. a national programme of routine minimum childhood immunization should choose vaccines of high priority and established need and efficacy. The need is to protect every child against poliomyelitis, measles and 13 whooping cough, diphtheria and tetanus . It would take 5-6 doses of oral polio vaccine, one dose of measles vaccine and 3 doses for primary plus one booster dose of DPT per child to achieve this. In order to give these inoculations in the least number of encounters between children and health care workers, the schedule has been simplified as shown below Planning for the annual pulse will be done at district and local community levels. The district level planning will include the arrangements for obtaining, storing and delivering the necessary vaccines. A central vaccine store will be required to store vaccines and to dispatch them in cold boxes with ice to the peripheral points. Other materials such as syringes would also be managed at the district level. In addition, the organisation and deployment of vehicles for transport of staff, vaccines and other materials will be managed at the district level. By staggering the dates of immunization in different communities a supply of cold boxes and syringes may be used repeatedly, making the operation economically viable. Under the new scheme, unnecessary storage of costly vaccine, sometimes beyond the expiry date, under unfavourable circumstances existing in a peripheral institution (e.g. PHC) would be avoided. The vaccines would be supplied fresh and in keeping with the demand; wastage would be avoided. This system of dispatch of vaccine requirements from a central store, also eliminates the weakest link from the cold chain, and makes materials management simpler. Planning at the peripheral level would be done by PHC staff, VHW and community workers, who

would arrange the time and place of vaccination: children eligible would be transmitted by, a house visit, followed by tom-tom announcements, 2-3 days before the appointed day. immunization cards would be distributed to mothers of eligible children et the time of house visit. These cards would be collected from the mothers at the time of inoculation, and redistributed prior to the next 'pulse'. This will be repeated till he child is completely immunized. after which the cards serve as the family retained record of immunization. These cards also make it easier for mothers to come with credentials and for the staff to make entries in the register. The numbers of prospective recipients are classified as infants. one year aids and eligible recipients in school. The "local planners" transmit this information to the central stores and procure vaccines in sufficient quantities immediately prior to the campaign. The 'left over' are returned to the central supply agency without delay. In this strategy, the parents are given appointments; eligible children identified, counted and brought in clusters, so that even reluctant mothers tend to join in. The venue of immunization is so chosen. that large numbers of mothers can come quickly and return without delay. The decision of the mother will not be complicated with if. when, where and what vaccine; it will be a simple yes or no. One member of the team should be available the next day to examine children with fever; this will reinforce the confidence of the community in the team. The assembly of mothers and children may also be used for health purposes other than immunization of children. The contact of the local health staff and village level workers with the community will increase. Such reinforcements will tend to contribute to the development and popularity of the local health service institutions. 'Pulse immunization' with OPV induces better Seroresponse than with sporadic immunization". This is presumably due to the spread of vaccine virus in the community. Pulse vaccination for seasonal diseases like measles and pertussis can be given prior to the season of high incidence so that immediately afterwards. the community will immunologically resemble a post epidemic population. Measles and pertussis are unlikely to reappear before the next annual 'pulse'.

The above mentioned system is a "vertical" programme consisting of the deployment of a network of personnel to deal with disease, particularly its prevention. If, on the other hand, immunization becomes the assignment for a particular period of time for the staff of health care institutions, it becomes a vertical programme within a horizontal one. Such integration is economically more feasible and philosophically more sound. EPILOGUE

D.

We would like to encourage the application of 'pulse immunization' by government and nongovernmental agencies combined with evaluation. Evaluation should include both coverage rates and REFERENCES: 1.

2.

Pocket book of Health Statistic's of India. Central Bureau of Health Intelligence, Ministry of Health and Family Welfare, New Delhi, 1979. Basu RN; Jezel Z. Ward NA: The Eradication of small-pox from India. WHO New Delhi, 1979.

the impact on the incidence of immunisable diseases especially whooping cough, measles and poliomyelitis. We have used this system to administer OPV in about 200 villages and have been convinced of, its feasibility and the high coverage achieved. Many health centre mobile teams and non-governmental organizations have found enthusiastic responses when they have taken OPT or other vaccines. We believe that only where the conventional clinic based immunization is supplemented by the suggested community based immunization, will 3 more fuller coverage be achieved.

8.

Banker DO: Modern Practice - Immunization. popular Prakashan; Bombay; 1980.

9.

Ghosh S: The Feeding and Care of Infants and Young Children. VHAI New Delhi, 1977.

10.

Ussova; Gujral, Ossipova, Behgal H: Prophylaxis in children and its organisation. In: Some problems in pediatrics in India and the Soviet Union, Kalavati Saran Children’s Hospital New Delhi, 1971.

11.

Health care of children under five. Tata. McGriw Hill New Delhi; 1973.

12.

John T J: Antibody response of infants in tropics to five doses of OPV. Brit. Med. J. 1: 311; 1976.

13.

John TJ; Devarajan LV: Priority for measles vaccine (Guest Editorial). Indian Pediatr. 10: 57; 1973.

14.

John TJ; Joseph A; Vijayarathnam P: A better system for polio vaccination in developing countries. Brit Med J 281: 542: 81

15.

MFC Bulletin· May 1981.

3. Chaturvedi UC; Mathur A; Singh UK et al.: The problem of paralytic poliomyelitis in the urban and rural populations around Lucknow. Indian J. HY9 81:179, 1978. 4.

Ashabai PV; John T J; Jayabal P: Infection and disease in a group of South Indian families 8. The incidence and severity of whooping cough. Indian Pediatr. 6: 645, 1969.

5.

John T J; Joseph A; George TI et al: Epidemiology and prevention of measles in rural South India. Indian J. Med. Res. 72: 153; 1980.

6.

Manual on Immunization, Ministry of Health and Family Welfare; New Delhi; 1978.

7.

Choudhuri P: Practitioner's column - Immunization in children. Indian Pediatr. 14: 65, 1977.

ALL INDIA CONVENTION OF PEOPLES SCIENCE MOVEMENT On behalf of MFC, Anant Phadke had participated in the All India Convention of Peoples Science Movement at Trivandrum on 9th and 10th February, 1983. It was convened by the Kerala Shastra Sahitya Parishad (KSSP). In the Health Group following joint action programmes were decided 1) Ban on E.P. forte combinations— The drug companies were trying to lift the Government's ban order on these combinations. They had filed a petition in the high court and the Madras and Bombay High Court have given a temporary ruling against the ban order. It was decided to restart the campaign against this

drug and oppose the wrong arguments given by drug companies in opposition to the ban. It was felt that after this set-back, we should not leave this issue half-way but should pursue it till complete success is achieved. 2)

Educational campaign about anaemia in Women and against irrational antianaemic drug preparations in the market. Since the anaemia is one of most common health problem in women and has wider social aspects (role of woman in the family etc.). this topic was chosen. Padma Prakash in-

formed that the Lok Vidnyan Sanghatana in Bombay may take this topic as one of the topics for the Vidnyan Yatra in Bombay in May 1983. Material prepared on this occasion can be circulated and used by others. 3)

Educational campaign about rational management of diarrhoea and against misuse of drugs in diarrhoea. This campaign is to be taken up once again this year, starting from June 1983. MFC would prepare a revised edition of its paper on this problem and would take initiative.

4)

Campaign against Multinationals in Indian Drug Industry -

The All India Federation of Medical Representatives' Association. (AIFMRA) would take lead in this campaign and others would join them. To be started in October 1983 when KSSP would be organizing its Annual Jatha into the villages of Kerala. 5)

Study—

(a) Of scientific literature on non-allopathic system of medical care; (b) Of irrational drug combination in India on the lines of the Expert Committee in Bangladesh. It has also been decided to start a PSM newsletter from Trivandrum.

LIFE IN THE VACCINE The fragility of the vaccine is a probable cause of failure of immunization campaigns against childhood diseases. Live vaccines, such as- freeze-dried measles, deteriorate quickly when exposed to high temperatures or light. Avoiding these conditions, particularly in rural areas of the tropics, is often difficult. As a WHO report explains, the biggest practical problem is simply that of keeping vaccines safe and effective, through refrigeration, "from manufacture to child".

In the first phase, the researchers extensively tested the indicators and developed techniques for preaging them to match the WHO maximum standard for measles vaccine of seven days at 37° exposure. The work is being carried out in close collaboration with WHO. Toxicity studies were also undertaken to ensure that the chemical used on the indicators is safe in everyday use and a coating technique was developed that is protective but does not alter the indicator's performance.

What makes the problem even greater is that a live vaccine looks just the same as a "dead" one-there is no way for the health worker to know if the vaccine is good or not. Thus, according to one estimate, 10 million children ea.ch year receive inactive vaccines. The result is a large waste of time and money, and, perhaps more damaging, loss of public confidence.

The second phase of the project, begun in 1981: should complete the necessary steps to mass-produce the indicators. This will involve development of a printing technology using the chemical indicator, a "management indicator" to go on the containers in which vaccines are packed, information and support materials for health workers, and extensive field testing and evaluation by 300 health workers in Mexico, Indonesia, and the Philippines. Laison with vaccine manufacturers has been part of the project since the beginning, to ensure that the necessary machinery will be in place for mass application of indicators on individual vials and on shipping cartons.

This situation could soon change thanks to the development of a simple time-temperature indicator, a telltale coloured sticker that can be attached to the vaccine vial, and changes from red to brown to black as the vial is exposed to adverse conditions. The prototype was produced by an American company that subsequently lopped the project because it saw no chance of sufficient profit return. The technology is now being developed by the Programme for Applied Technology for Health (PATH). The International Development Research Centre, Ottawa, is one of the major donors to this project.

Barring unexpected setbacks, the indicator should-be available at-the end of 1982., In future, health workers will be able to see at a glance if . their vaccines are still effective. Source: Searching, Review of International Development Research Centre, 60 Queen St. Ottawa, Canada KIG 3HG. .

FROM THE EDITOR'S DESK In a report to the world health assembly in May 1982, the Director General of the World Health Organization said "in the absence of immunization programmes, some four out of every 1000 school age children will be disabled by polio-myelitis, without immunization almost all children will contract measles. ,Taken together the six diseases (measles, diphtheria, tetanus, whooping cough, poliomyelitis and tuberculosis) kill some five million children each year and cripple, blind or cause mental damage to five million more". State of our children in 1982-83 is such that these six diseases together account for one third of total deaths in children of the third world countries. Remember these are all preventable diseases with potent vaccines being available (may be that tuberculosis is the exception). Our government claims 60% .coverage with immunization at the moment and hopes to achieve a, target of 80% coverage by the end of current decade and 100% coverage by the year 2000 A.D. During last three decades government agencies through various plans and strategies tried to achieve the target. but failed, why? What are the reasons behind it? They are numerous starting right from cultural taboos related to the curse of the god (or goddess), ignorance regarding diseases, lack of concept of prevention etc. etc. But the major factor is lack of strategies to educate and motivate masses (people). We all talk about people's participation. Hare is the example of failure of the plans because of lack of people’s participation. To site an example, in Cuba before revolution, Cuban children were not vaccinated at all. But in last 20 years Cuba has achieved 100% coverage through peoples committees. There are many misconceptions in the minds of lay people as well as professionals. Even doctors do not give the dose of vaccine if child has a running nose or few loose stools. Injectable medicines are preferred by the people but injectable vaccine is not much acceptable to them. Most of them think, "why my healthy child should receive an injection and get fever?" Recently it was reported in the newspapers that in certain parts of the Maharashtra rumor spread that children are be-

ing killed in the schools by injecting some sort of a vaccine, and even well educated people in towns prevented their children from attending the schools. This shows that we have failed to educate, and motivate people on this aspect. Other practical difficulties encountered are, how to reach these enormous masses in their own villages and hamlets? How to bring them at a particular place for vaccination? How to ensure near 100% coverage? How to make them come for repeated doses and for boosters over a long period? (Drop outs at 2nd, 3rd doses is very high) How to ensure the maintenance of potency of the vaccine? (Cold chain). Dr. Jacob John in his article has tried to answer few of these questions. He himself has tried out a different plan, to achieve 100% coverage. We were impressed by his methodology to find out: priority for the vaccine. It is a very novel and innovative method. His cluster approach with pulse immunization has worked in his field area. He experimented with measles vaccine doubts were raised when we met Dr. John at MFC annual meet at RUHSA, that whether this strategy will also work with triple vaccine? We do not have an answer yet. What is the relationship between malnutrition and these diseases? There is definitely a vicious circle of poverty-malnutrition-infection. These diseases are major causes of malnutrition. Whooping cough can induce malnutrition by frequent vomiting and measles itself claims 10% of body weight in a quarter of cases and halts weight gain for several weeks, A malnourished child who contracts measles is approximately 400 times more likely to die of the disease than a child who is adequately fed.

Is 'immunisation against all these diseases is the only answer for prevention of malnutrition? Does malnutrition have wider perspective? Even the other infections like diarrhoea produce malnutrition in children. We must look at malnutrition in children as a part of total socio-political system of our country. So unless we try to change this system, immunization alone will not be able to wipe out malnutrition.

* Editorial Committee:

Anant Phadke Padma Prakash Ravi Narayan Shirish Datar Ulhas Jajoo Kamala Jayarao- EDITOR


