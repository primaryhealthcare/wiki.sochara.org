---
title: "Why I Loved MFC?"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Why I Loved MFC? from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC258-259.pdf](http://www.mfcindia.org/mfcpdfs/MFC258-259.pdf)*

Title: Why I Loved MFC?

Authors: Phadke Anant

medico friend circle bulletin 258 259

Sept-Oct, 1998

Re-thinking Public Health, Food, Hunger and Mortality Decline in Indian History Sheila Zurbrigg

Ever since the sanitarian movements of the mid and late 19th century in Europe - in England personified by the zealous activities of Edwin Chadwick - the concept of public health has increasingly been confined to the microbiological realm: that is, to the task of controlling transmission of microscopic disease agents, in particular, micro-organisms (germs), based upon public sanitation services and promotion of individual hygiene. Those enlightened planners who originally conceived of a centre of social medicine within the JNU social sciences school, on the other hand, clearly had a different public health in mind. They intuitively harkened back to an earlier understanding of public health as belonging to a far broader sphere, an understanding which interpreted epidemic mortality patterns as fundamentally a product of the social and economic organisation of society: poverty, unemployment, destitution, and hunger. It is unusual to say the least in Canada to have a centre of public health and epidemiology separate from a "parent" medical institution or college. It is rarer still to find one physically ensconced and intellectually grounded, as the Centre of Social Medicine and Community Health is, within the broader domains of economics, sociology, political science and history. So it is a privilege to address such a multidisciplinary gathering. It is unquestionably an important and rare opportunity, and I look forward to your cross-disciplinary input. The history of this broader pre-Chadwickian public

health entails tracing patterns of mortality from all causes within society in relation to broad shifts in social and economic conditions. It is a history which I generally refer to in my own teaching simply as the history of health, where health is understood in the most elemental sense of physical survival. It is thus the history of secular trends in human life expectancy. Throughout most of human history, life expectancy fluctuated in the low and mid twenties. In contrast, increasing sections of people in much of the world today can expect to live well into their 70s or 80s. This vast extension in average human life span is the result primarily of dramatic decline in risk of death from infectious disease. The history of health thus confronts in essence what is termed in epidemiology the basic "epidemic equation" bringing together secular trends in levels of human exposure to disease on the one hand, and changes in human (host) resistance to infections disease on the other. Strangely enough, the history of rising life expectancy (mortality decline) is to a very large extent unwritten in sharp contrast to medical history. Stranger still is that even the very question of health history is rarely posed, either in modern medical or public health texts. Unfortunately, the history of health is a subject which has also slipped between the cracks of the major historical sub disciplines, at least until very recently. It is difficult Presented to the School of Social Sciences, Jawaharlal Nehru University, New Delhi, 29 April 1997.

to explain this extraordinary omission, except in terms of the overwhelming assumption that we already had the answer: it lay in modem' scientific medicine. As many of you are already aware, the subject of mortality decline was, projected onto the historiographical stage as a question by Thomas McKeown in the late 1960s and 70s. McKeown and his co-workers did so by graphing trends in yearly death rates in England and

Wales from 1841 to the present for each of the major infectious diseases, and on each graph plotting the point in time at which effective "modem" medical prevention and treatment techniques became available. What became immediately clear in this process was that much, indeed most, of the decline in death rates for almost all the major infectious diseases (tuberculosis, whooping cough, pneumonia, measles, bronchitis and others) occurred well before the discovery and general availability of modem medical techniques such as antibiotics, immunisation, intravenous rehydration and vitamin supplements (Figs. 1-3). Modem medicine as we have come to know it today (in the sense of 'magic bullets'), he observed, could account for only a very small part of the increase in English life expectancy to 70 years by the mid-20th century. . Through a process of examining and eliminating other theoretical explanations, such as change in virulence of micro-organisms or in levels of specific (acquired) immunity in the population, McKeown went on to conclude that increased general resistance to infectious disease through improvements in nutrition was probably the main factor underlying this "transformation in health"1. He acknowledged that clean water supplies and sewage systems in the final decades of the 19th century played an important role in reducing exposure to waterand food-borne diseases, such as diarrhea, dysentery and typhoid fever. But these public health measures could

explain at most one-quarter to one-third of total mortality decline, and even here, he suggested, the same factor reducing lethality of air-borne diseases, viz. increased human resistance, may well have been contributing to the fall in water-borne infections, death rates at the same time.2 This is all by way of introduction since I am sure that many of you are familiar with McKeown's thesis, and indeed the contributions from Indian scholars such as Sumit Guha in this debate. What many of you may also be aware of is that across the 1980s there has been considerable questioning and contesting of McKeown's nutrition thesis, particularly among demographic historians. Some, such as Roger Schofield and Massimo LiviBacci have gone so far as to suggest that even the link between famine and epidemic mortality is likely primarily related to increased exposure to disease rather than reduced host resistance-starvation including migration and greater exposure to disease through lack of hygiene and what has been termed "dysfunctional social behavior". I will not pursue this European health history debate further here, except to suggest that a great deal of the "ambiguity" with respect to the nutrition-infection link may stem from a woefully inadequate conceptual understanding and methodological approach to the issue of hunger in history. It is a confusion (in my more despairing moments I term it conceptual chaos) which in no small degree stems from the word "nutrition" itself, an

. unfortunate term which in its general use by historians and indeed the medical profession obscures and confuses critically important distinctions with respect to the nature of hunger in human experience. The relative conceptual invisibility of hunger, both acute and chronic hunger, has marked historical analysis quite profoundly3.

1.

South Asian Health History

recognition of decline in crude death rates in India from 1920 onward, the reasons underlying the timing of this mortality transition have to a large extent been left unexplored. In no small part, it seems, because the answer was assumed to be already known: modem disease control efforts. To give only two examples, let me cite the following. The ninety-page chapter on "Population" in the 1983 Cambridge Economic History of India, for example, contains only a single paragraph devoted to post-1920 mortality decline where it is assumed to be due to public health measures of disease control6. The Visarias repeat this view as recently as 1994: "After 1921, a progressive control of cholera and plague epidemics began to lower mortality ... Since about 1951, the decline in mortality has been truly remarkable... Among the factors contributing to the mortality decline is the introduction of universal immunization in the mid1970s"7. It has been only in the last few years that the subject of mortality decline has begun to be addressed as a central question in South Asian historical analysis8.

South Asian historiography, unfortunately, has not esRe-Examining' the History of Malaria Mortality caped this conceptual chaos. Nor indeed has it avoided Decline: The Case of Punjab the neglect of health history as a central question of economic and social history. To an even greater extent It was no doubt my earlier practical experience working than in Europe, in fact, mortality decline in India and with the traditional village wives in Ramnad district, much of the "third world" has been assumed due to . Tamil Nadu, in the late 1970s which made me initially modem medical science. This assumption has been an sceptical of the medical technology thesis of South Asian even stronger one with respect to developing countries in part perhaps because so much more of the historical mortality decline has occurred recently, in the 20th century, and thus temporally overlapped increasing availability of mass methods of disease control and prevention. Mortality in India, for example, has shown sustained decline from historically high levels only from 1920 onward (Fig. 4). It has been historical demographers once again-among whom, Kingsley Davis perhaps the most influential who have most explicitly and tenaciously argued the modern medical science case4. As recently as 1991, Davis has stated that "the rapid fall in Third World death rates after 1945 was mainly due to achievements of medical science in the advanced countries.... By having access to the yield of these achievements, the developing countries received a free ride, so to speak. The techniques were transferred to them quickly at low cost, and thus the death rates were dramatically reduced in a short time”5. And among these techniques: Davis and many others as well, considered malaria control through DDT the primary example. We can see this assumption incorporated again and again in textbooks of Indian demography, public health and indeed economic history. While there has been fleeting

mortality decline, particularly for the pre-Independence period. But it was when I subsequently came to realize that the DDT explanation for malaria and general mortality decline had never in fact been subjected to systematic analysis for India (and indeed an inadequate one for Sri Lanka), that I became interested in pursuing research on the history of South Asian Malaria mortality decline.

I chose Punjab province for this work for pragmatic reasons. Punjab is one of the few regions of South Asia where, because of the highly seasonal character of transmission, it was possible to more confidently identify malaria deaths from within the general category of "fever" deaths in the annual vital registration returns in the colonial sanitary reports. The characteristic autumn rise in fever mortality in the Punjab plains had long been recognised as distinctly malarial. Across the colonial period fever deaths recorded during the months of October and November were considered a general indication of malaria intensity in any given year by provincial sanitary officials. (Other recurring epidemic diseases such as cholera and smallpox generally did not occur in these autumn months). Subsequent epidemiological studies of monthly human and mosquito infection rates in the 1920s and 30s provided the entomological explanation for this seasonal pattern of malaria mortality. They showed how malaria transmission, and particularly falciparum infection, was limited essentially to the post-monsoon months-the only period of the year when prevailing temperatures and, more importantly, adequate humidity made effective transmission possible. Though favourable conditions for malaria transmission might exist by late July, high infection rates amongst the population typically appeared only in late September. It took a minimum of eight weeks and several human mosquito cycles for infection rates among the population to multiply from pre-monsoon levels of perhaps for 2 per cent to high or near universal levels. Thus fever cases generally did not peak until late September or early October, with peak mortality from malaria appearing several weeks later still. In years of only mild epidemic intensity, increased fever deaths might be limited to October. However in years of greater epidemic severity (mortality), increased fever deaths continued on into the months of November and December. It was for this reason that colonial sanitary officials in Punjab came to use October-December fever deaths as the basis for estimating epidemic severity in any given year. In my own work I have used two different measures of annual malaria mortality. The first was simply the OctDec. fever death rate per 1000 population, as was used by the provincial sanitary commissioners. I also included a second measure termed "epidemic difference" in an effort to control for non-malarial fever deaths included within the autumn data. This second measure is simply the numerical difference between the autumn (Oct-Dec)

fever death rate and the spring-summer (April-July) fever 9 death rate , Trends in Malaria Mortality: 1868-1940 Both measures show essentially the same pattern of malaria mortality. Across the forty-one year period from 1868 to the epidemic of 1908, malaria mortality fluctuated in classic saw-tooth fashion (Fig. 5). After 1908 however this pattern changed dramatically. Mean autumn malaria death rate in the 33-year period between 1909 and 1941 dropped abruptly to less than one-third that for the earlier period. (If 1917 is taken as the change point the corresponding drop is to one-quarter). The size of this drop as well as its suddenness is surprising to say the least. All the more so because there' is no evidence of any decline in rates of malaria transmission, or infection, in the province across this second period as reflected in spleen and parasite rates. There was no significant change in rainfall neither in the province nor in epidemiological measures of falciparum malaria rates across the 1868-1941 periods. Nor does

there appear to have been any significant decline in severity of flooding in the post-1908 period. Flooding was considered by sanitary officials as one of the two key factors associated with severe epidemics in the pre-1909 10 period . What appears to have changed after 1908 was not the incidence of malaria infection in the province, but rather its lethality, that is, the proportion of infected people dying from the infection: in epidemiological terms, this is called a shift in "case fatality rate".

One explanation for declining lethality of malaria could be medical treatment. Yet per capita availability of quinine in the province was so low even by the late 1930s, in particular for the rural areas, as to make this explanation extremely unlikely. Further, per capita availability appears to have changed only marginally from the last severe epidemic in 1908. By the late 1930s, per capita quinine made available free or at subsidized rates through the public dispensaries and rural 'depots' by the Punjab Government amounted to only 1.1 grains annually. By comparison, the amount of chloroquine—roughly equivalent by weight to quinine—requested to treat 11 malaria today is 46 grains . On the other hand, one factor which did clearly change after 1908 was the incidence of famine. I mentioned above that severe flooding was one factor associated with soaring malaria mortality in the province in the period up to 1908. The second factor was "scarcity", high foodgrain prices associated with famine. And indeed, all but one of the peaks in the 1868-1908 period were periods either of frank ("official") famine or severe economic crisis secondary to sudden massive grain exports. The one exception, the epidemic of 1876, was in fact preceded by catastrophic floods in 1875 which had destroyed standing crops and prevented agricultural activities well into the subsequent year. Multiple regression analysis confirms that both monsoon (third quarter) rainfall and annual wheat price are both significant and independent predictors of malaria mortality in Punjab across the period up to 1908. District level analysis Showed wheat price to be significant in half of the 24 districts, and rainfall in 18 (Figs. 6 and 7). Interesting regional differences in the impact of foodgrain prices and rainfall were apparent. In the south-eastern districts where agriculture was primarily rain-fed, wheat price was more consistently a predictor of autumn malaria mortality than rainfall. The opposite pattern was the case in the north-central and western districts, where agriculture was much more dependent on irrigation and where crop losses were triggered typically more by severe flooding in years of heavy rainfall. Thus the importance of rainfall in the regression results was two-fold. Normal or above normal monsoon rainfall was an essential factor for malaria transmission to be

possible. Below normal rainfall (drought) altered entomological conditions so profoundly that little or no transmission of falciparum malaria took place in the rural areas of the province, and thus though famine might prevail, its impact on mortality would not be expressed through malaria (fever deaths in Oct-Dec) but rather increased mortality at other times of the year by other common infectious diseases. The impact of above normal rainfall, however, was not limited to entomological factors. Extensive flooding of the flat alluvial soils of Punjab also triggered severe economic stress, even in the south-east. Floods waters, which often stood for months on the Punjab plains, destroyed both standing crops and stored foodgrains; in addition, standing water often prevented sowing of the subsequent harvests crops. In interpreting the regression results for rainfall, it is impossible to distinguish the entomological (malaria transmission) impact of heavy rainfall in the province from its economic impact. We can, however, arrive at some indirect sense of the relative importance of these two separate consequences of heavy rainfall by comparing the relationship of rainfall and wheat price in the earlier period of highly lethal epidemics to that of the post-1908 period. Interestingly, the correlation between rainfall and malaria mortality remains equally strong after 1908 even as actual mortality from these epidemic declined dramatically. It is hardly surprising that the statistical significance of monsoon rainfall continued after 1908, given the essential nature of normal or above normal rainfall in making malaria transmission possible. On the other hand, the statistical relationship between wheat price and malaria mortality disappears after 1908. Together these findings suggest that heavy rainfall was a necessary but not sufficient factor in explaining the severe (highly lethal) epidemics of the pre-1909 period. Likewise, severe floods continued to occur after 1908, as in 1933, yet on their own did not trigger epidemic mortality on anywhere near the scale characteristic of

the earlier period to what can we attribute this change? Post-1908 Malaria Control Efforts Malaria control activities of this period were extremely Post-1908 Malaria Control Efforts localised efforts and limited in large part to urban areas. And as I noted earlier, insofar as these efforts involved quinine distribution, the amounts involved were totally inadequate to account for the decrease in mortality observed12. Changes in Famine Relief Policy There were, on the other hand, a number of substantial changes in famine relief policy (which I have documented in detail elsewhere13). In brief, after 1908, drought relief policy became more pre-emptive in character. There were considerable efforts on the part of the provincial sanitary administration to anticipate both "economic stress" and flooding in a given year, and to direct official relief toward those districts so affected. As a result official relief was increasingly sanctioned before frank famine 'had declared itself. In addition, the 1930 revisions to the Punjab famine code made provision for agricultural loans and gratuitous relief simply on the basis of "scarcity" - by definition, whenever staple foodgrain prices rose 40 percent above normal levels. In practice this meant relief not just for work starvation but in addition that for starvation induced by high prices alone. Finally, but no less significantly, after 1920 relief was sanctioned for flood-induced harvest losses in addition to drought14. The effect of these policy changes was to make relief available earlier and far more frequently— in effect, catching starvation with a finer—or perhaps more accurately—less coarse net. In other words, after 1908, there appears to have been a marked decrease in the frequency with which drought and/or floods proceeded on to economic collapse and epidemic starvation ("famine"). Chronic hunger (undernourishment) and what I term endemic acute hunger probably remained unchanged. So the identification of decline in famine is by no means a diagnosis of economic development, even less, of beneficent imperial rule. Simply removing recurrent famine— that is, reducing epidemic starvation—appears to have altered the epidemic equation with respect to malaria lethality quite profoundly. Let me say that I .am almost embarrassed to report this as a "new" finding. For the close link between famine and / or economic (read agricultural) crisis and soaring famine mortality is a commonplace in the historical records of the pre-1910 colonial period15. And I have little doubt that one could find this relationship expressed in folkloric terms even more clearly (as Braudel found with the Tuscan proverb: 'the best cure for malaria is a full cooking pot').

Yet such a conclusion flies in the face of current medical16 and historical understanding of the relationship between "nutritional status" and malaria. Understanding how this historical view can be reconciled with modern medical theory is of course a subject for an entire book, or indeed several. Much of the confusion stems from imprecise usage of the term "malnutrition". I am in the habit of putting the term "malnutrition" carefully within quotation marks (as my students can well attest!). I do so to remind myself and students of the nebulous meaning of the term. Briefly put, the emergence of nutritional science in the earlier decades of the 20th century conceptually and linguistically replaced very specific and practical understandings of hunger in human experience with the unfortunate term "nutrition". In the process key distinctions between acute hunger (starvation) and chronic hunger (undernourishment) were effectively lost. Any village labouring mother on the other hand could tell us most precisely the difference between acute and chronic hunger. To the extent that modern medical literature addresses the malaria-"nutrition" relationship, the focus since the 1930s has been overwhelmingly upon micro-nutrient deficiencies— that is, on qualitative issues of "diet" as opposed to quantitative issues of caloric intake. To a very limited extent chronic hunger (undernourishment), termed "malnutrition", has been included. But acute hunger (starvation) is effectively absent— absent both conceptually and investigation ally from this research literature. With one exception. The "Re-feeding Malaria" Thesis Writing in the 1970s the Murrays coined the term "refeeding malaria" on the basis of their observations during the course of the Sahelian famine. They observed that some children at the relief centres showed rising malaria parasite levels once in receipt of relief feeding, and went on to interpret this observation as suggesting that the earlier period of starvation (acute hunger) protected these children from malaria. This thesis has been highly influential within both the medical and historical disciplines, including recent South Asian famine analysis17. I will not go into the Murrays' deductive arguments, a subject which I have explored in detail elsewhere18. Briefly put, they conflate severity of malaria infection as measured by malaria parasite levels with lethality of malaria in terms of host survival rates. Their interpretation of "protection" was a microbiological one alone; no information was available, on morality rates/survival rates. It was simply assumed that lower parasite rates meant lower mortality. While this may be so in particular

instances, this relationship cannot be assumed necessarily, nor extended automatically to seriously altered host states such as starvation, where the "suppressive" effect on the human host of that starvation may well exceed the suppressive effect on the micro-organisms. In essence the refeeding malaria thesis confuses microbiological observations with epidemiological outcome. A rather cryptic analogy might be: the surgery was a success (lower parasite levels microbiologically) but the patient died anyway.

1943 Bengal Misinterpreted Interestingly, in arguing this protective effect, the Murrays' cited the 1943 Bengal famine as further evidence of the protective effect of starvation in malaria. The Woodhead Famine Commission Inquiry also noted that some starvation victims treated in the famine hospitals in 1943 Bengal were observed to have lower malaria parasite levels than non-starving malaria cases. Yet the Commission went on to conclude that these lower parasite rates did not appear to convey any relative advantage. Quite the contrary, it noted, the starving succumbed far more readily to the disease in spite of these lower parasite levels, and also were particularly unresponsive to medical treatment. It is difficult in the extreme to understand how anyone reading the 1945 Famine Commission report could come away with an interpretation of relative protection for famine victims. And yet this is the Murrays' inference. I believe what this speaks to is the extraordinary power of the germ theory in shaping interpretation of human experience. The Murrays' are not alone: this view of the Bengal famine is shared by a number of leading malariologists. It may well be that the refeeding malaria thesis has also informed Elizabeth Whitecombe's recent EPW study of 19 late 19th century Indian faminea , I hesitate to become embroiled in criticism of the minutiae of a particular paper; but cannot resist in this case because of the number of times her conclusions from this article have come up in conversion during my stay here at JNU since January, and also because her conclusions raise much larger conceptual issues with respect to health history more broadly. In her 1993 article, Whitecombe looks at several famines noting—as Maharatna, Dyson and certainly many colonial sanitary commissioners and Famine Commissions had before her— that malaria was often a leading cause of epidemic mortality associated with South Asian famines. She concludes that this association was primarily indeed, solely-entomologically driven; a function of 'prodigious proliferation' of anopheline mosquito vectors,

and secondarily increased human exposure to these vectors because of famine-induced cattle mortality (deviation of mosquito' feeding from cattle to humans). She dismisses in a single (unreferenced) observation that the “relation of (malaria) infectivity to the nutritional status of the host is even today obscure". And adds, "it is conceivable that the starving poor were less susceptible to malaria than the well-fed, as the Woodhead Commission was to suggest in reviewing the evidence of the Bengal 20 famine of 1943" . Whitecombe, now with a post-graduate medical qualification in addition to earlier historical training, is concerned to interpret historical malaria mortality experience "scientifically". Yet what is offered here is in fact a highly selective application of scientific theory, one restricted to the microbiological (germ) domain-the germ side of the epidemic equation. For one could argue that it is equally "scientific" to be assessing prevalence of hunger and its relationship to epidemic mortality patterns. As in fact did a number of leading malariologists in the early decades of this century (Christopher’s and Bentley come to mind, and later the painstaking research work of S.P. Ramakrishnan). That is, it is just as legitimate scientifically to address the condition of the human host. In other words, I think what we are looking at here is another conflation: scientific = microbiological. Germ explanations in the 1990s are somehow far more credible than socio-economic. This is so even though Whitecombe's theses of prodigious vectors, or cattle deviation, remain entirely in the theoretical realm. Any microbiological hypothesis, no matter how unsubstantiated empirically is better (more credible Lit seems, than a social explanation. Remarkably, not even the slightest attempt is made to provide supportive evidence with, say, scrounging up some kind of cattle mortality data. It simply is unnecessary. Such is the power of germ -theory. Never mind also that the thesis fails completely in explaining post-1920 epidemic malaria mortality decline, with no demonstrable decline in rainfall or prodigious anopheline. And other severe (lethal) epidemics of malaria such as in 1943-44 Bengal in which no climatic or cattle deviation explanations are even theoretically possible 21. In fact of course a great amount of historical evidence exists as to the centrality of hunger in epidemic, including malaria epidemic patterns. Most provincial sanitary commissioners began their annual reports in the first paragraph quoting the price of staple foodgrains prevailing in their jurisdiction, secondly wage rates and third the state of the harvests. This was fundamental and pragmatic preamble for interpreting the data on vital rates which were to follow in the rest of the report. Likewise most famine reports of the late 19th century

(and there were many) refer to the selectively greater mortality from malaria among the starving. In effect, they were making the important distinction between infections on the one hand, and mortality from that infection (lethality) on the other. And then of course by the early years of the 20th century, there was the work by highly renowed malariologists such as Christophers, and later S.P. Ramakrishnan, who undertook to quantify the relationship between acute hunger (starvation) and lethality of malaria 22 infection . I should hasten to add that this does not necessarily mean that most sanitary officials under the colonial administration were particularly socially-conscious. They simply couldn't help noticing. And being responsible for epidemic control, they also couldn't help being on guard when prices rose. Likewise, Christophers was unquestionably a most loyal subject of the crown and Raj. He was also a superb entomologist. But the relationship of epidemic malaria with starvation prevalence was too compelling not to document.

It has been difficult tracking down monthly vital registration data for Punjab districts during the 1940s and 50s. So I have been unable as yet to estimate annual malaria mortality trends across the period of the dramatic reduction in malaria transmission brought about in the early-mid 1950s with mass application of residual insecticides (DDT). I have come across however data on annual crude death rates for at least a single district, Ludhiana, which spans 24 this crucial period (Fig. 8) . The major initial period of DDT spraying in the district took place across late 1953 and 1954. By 1955 infection rates had come down markedly in the district. The impact of this near eradication of malarial infection in the district on crude death rate trends was modest however to say the least, barely discernible in terms of overall crude death decline. Infant mortality for the district however did show a significant decline over this period, from 124 to between 109-115. Yet relative to the amount of decline already in process by 1950 (IMR well over 200 in 1920; in 1908, 350), even this improvement can be seen as relatively modest.

All of this extremely rich, credible and consistent historical evidence is dismissed in a single sentence by Whitecombe, consigned to the dustbin of history, as it were. How can this From this data series Dyson and Das Gupta also be so? In one sense this is testament to the irresistible appeal and power of microbiological (germ) theory. It is germ2. history (as opposed to health history) with a vengeance. But the point of course is that Whitecombe is by no means alone. And indeed, she is quite correct to interpret modern medical theory as suggesting little evidence of a nutritional synergistic relationship between "malnutrition" and malaria. At the same time I am not suggesting modern medical research is wrong or unuseful. But rather simply lacking in this crucial experience with respect to epidemic starvation. But what I believe this example also suggests is the conceptual limitations and frank inadequacies of modern epidemiological theory for interpreting historical health and mortality experience. The error lies not in referring to modern medical theory but rather in deferring to such theory to the exclusion of historical experience and inquiry. If historians have been bamboozled by reductionist germ theory, it is a bamboozling which in the case of the history of malaria mortality decline has caught unawares economists even of the stature of Myrdal and A.K. Bagchi, both of whom have also accepted the DDT explanation of malaria 23 (and overall) death rate decline . They might be surprised to learn that the impact on 'mortality in India of residual insecticide spraying (DDT) has never been directly assessed.

calculated life expectancy trends. And what is also apparent from this series is that by 1948 average life expectancy in the district had already reached the level of over 50 years, pre-DDT 25. What this suggests I believe is that the medical technology view of South Asian health history needs a great deal of rethinking. It appears that the main decline in malaria mortality-as one of the leading causes of death history

in India-predates DDT. Yet in concluding this, am I suggesting that malaria parasites are irrelevant to "public health" as long as one has access to two square meals a day? Or indeed that there was no more hunger (chronic or acute) in Punjab by the 1950s? Absolutely not. Malaria infection particularly falciparum malaria has an inherent lethality which is by no means inconsequential (even with a full stomach). Without question many, perhaps thousands, of young children in Ludhiana were spared malaria deaths in the 1950s through the control programme of residual insecticides. It is simply that the number of deaths prevented is relatively very small compared to the toll exacted historically by the disease when combined with recurrent epidemic starvation. Similarly, it is not that Public health in the narrow Chadwickian sense of germ control is unimportant. Indeed, one might suggest this is the ultimate bamboozling-that is, the framing of the health history debate in such oppositional terms, an opposition between nutrition "versus" public health (disease exposure) creating a false dichotomy where in fact there is none. I'll return to this in a moment.

(To be concluded in the next issue) Note & References: 1.T. McKeown (1976), The Modern Rise of Population, E. Arnold. 2. McKeown was in essence recognizing the two sides of the "epidemic equation"- that is, germs (disease exposure) on the one hand, and the human host (resistance) on the other; and the importance analytically of distinguishing between infection, disease, and mortality from a given micro-organism. 3. The relative neglect of hunger in modern historical inquiry is curious, for as a pervasive reality in pre-modern existence, it fairly shouts at one from the historical records. The reluctance to address hunger conceptually and analytically stems perhaps from a sense that the term is unscientific, imprecise, or worse, emotional. "Malnutrition has the cachet of scientific respectability", Diana Wylie has recently remarked, while "hunger does not" (in "The Changing Face of Hunger in Southern African History", past and Present, Feb, p. 160). 4. This assumption was central as well to A. Coale and Hoover's highly influential 1958 Population Growth and Economic Development in Lowincome Countries: A Case Study of India's Prospects (Princeton). 5. K. Davis. S. Bernstam (eds), 1991. Resources, Environment and Population. 6. "The causes of high mortality (across the 1871-1921 periods) were primarily related to waves of epidemics". Plague "somehow subsided", and cholera "yielded slowly both to research and ... .inoculation p. 502 7. P. and L. Visaria (1994), "Demographic Transistion", EPW, Dec 1724, P. 3281. 8. See for example, S. Guha (1991) "Mortality decline in early 20th century India" IESHR, 28: 371-87; and C.Z. Guilmoto (1992) "Towards a new demographic equilibrium: The inception of demographic transition in South India", IESHR 29:247-89. 9. For details, see, S. Zurbrigg (1992), "Hunger and epidemic malaria in Punjab, 1868-1920", In. 25, PE:2-26; and S. Zurbrigg (1994), "Rethinking the 'Human Factor' in malaria mortality: The case of Punjab, 1868-1940", Parassitologia 36: 121-35. 10. For details, see S. Zurbrigg (1992, 1994); and S. Zurbrigg Hunger in India's Epidemic History, in preparation.

11. Private sales of quinine amounted to a further 1.5 grains per capita but because of the cost were beyond the reach of the large majority of the population. (See EPW (1992), footnote 120). 12. As late as the 1930s there were numerous reports as to reluctance to give quinine to young children. To some extent this must also have included directing quinine supplies. Yet the total amount of quinine made available for free or subsidized distribution remained so low that it is difficult to conclude that access was anywhere near effective. Further, it seems quite unlikely that those most vulnerable, the poor and young children, would have had priority access to those supplies available at village level. 13. S. Zurbrigg (1996), "Evolution of Colonial Famine Policy in South Asia, 1880-1940", IUSSP Conference on Asian Historical Demography, Taipei, Jan. 1.996. 14. The contrast with 1876 is stark-no relief whatever was granted in response to the extensive, severely destructive floods of 1875; even pleas for temporary suspension of revenue demand were turned down by the provincial administration. 15. S.R. Christophers, one of the leading malariologists in colonial India concluded his 120-page investigation of the 1908 epidemic in Punjab with the words that 'malaria only reaped the harvest prepared for it by famine' (in "malaria in the Punjab". Scientific Memoirs by Officers of the Medical and Sanitary Departments of the Government of India, New Series, No. 46, Superintendent Govt. Printing, Calcutta, 1911). His attempt to statistically measure the relationship between epidemic malaria mortality in the province and scarcity (high foodgrain prices) is what stimulated my own efforts in this direction. 16. The latest major malaria textbook (2000 pages) concludes there is little if any role for "malnutrition" in enhancing malaria mortality. See, W.H. Wernsdorfer, I. MacGregor (eds), 1988. Malaria: Principles and Practice of Malariology, Vol. 1, Churchill Livingstone, 763. 17. See for example, T. Dyson (1991), "On the Demography of South Asian Famines, Population Studies 45: 5-25; 279-97. 18. S. Zurbrigg (1997), "Did Starvation Protect from Malaria? Distinguishing between Severity and Lethality of Infectious Disease in Colonial India", Social Science History, Spring, 45-79. 19. E. Whitecombe (1993), "Famine Mortality", EPW, June 5, 1169-79. 20. Ibid, P, 1178. 21. On this, see Zurbrigg (1997).

22.S.R. Christophers (1911) "Malaria in the Punjab." New Series No. 46, Scientific Memoirs by Officers of the Medical and Sanitary Departments of the Government of India. Calcutta: Superintendent Govt. Printing, GOI; S.P. Ramakrishnan (1953, 1954) Indian Journal at Malariology, 7: 53-9; 8: 89-96; 8: 327-32.

National Disease control Program Expenditures Ravi Duggal and Sunil Nandraj

Selected diseases have at different points of time received special attention and separate allocation of resources. In the past small pox was one such disease which had a separate budget and staff to tackle the problem on a war footing. In the past many such programs were of a vertical nature having their own budgets and staff. Malaria and leprosy programs, apart from small pox were the main vertical programs. While the war against small pox was successful, that against malaria reached a certain success in the midsixties but after that malaria has come back with a vengeance and continues to be a major program (but without its vertical structure). Leprosy continues to be a vertical program and in recent years has shown good results. The tuberculosis and the blindness control programs have had no such luck and have always received a stepmotherly treatment under public health care. Disease programs on an average during this decade have received 10% of the state's health care budget and the trend is a declining one. In per capita terms at the national level today a measly amount of Rs. 8 per person is being spent on these programs. If one looks at the disease profile of the country then this expenditure itself is very low to fight these diseases. (Of course, it must be noted that three-fourth of health care is sought in the private sector and hence the actual per capita value would be four times.) The decline in expenditure on these programs is mainly in Assam, Bihar, Himachal, Karnataka, M.P., Orissa, Punjab, Rajasthan, and Tamil Nadu. If we break down the expenditure by various diseases we find that between 80% and 95% is spent on just four programs: malaria, leprosy, tuberculosis and blindness. Further, of the total disease program expenditure 50% to 60% is spent on the malaria program alone, followed by about 20% on leprosy. Tuberculosis and blindness control gets under five percent.

Malaria The Prevalence of malaria is very high right across the length and breadth of the country, with only Kerala and Goa being exceptions. The NFHS study in 1992-93 gives a 3 month incidence rate of 3324 per 100,000 populations, which means about 105 million new cases every year. The rural areas recorded an incidence of nearly twice that of urban areas. While most states show a fairly high share of expenditure for the malaria program from the total disease program budget, it must be noted that most of it goes to salaries of staff who may not be doing any work related to malaria. For historical reasons most multipurpose workers (MPWs) get their salary from the malaria department because they were erstwhile malaria workers and today are MPWs who may be doing very little malaria related work. Hence, what actually is spent to treat or control malaria may be a very small amount of the na-

tional malaria budget of about Rs. 5000 million which in itself may be quite adequate to fight malaria under a comprehensive health program.

Leprosy According to the 1981 census India had 4.2 million active leprosy cases. The NFHS survey a decade later in 1992-93 recorded a prevalence rate four times less than the 1981 census making for a caseload of 1.2 million cases. While one may argue that the NFHS may have made an undercount there is no doubt that the leprosy program has had a major impact, and this perhaps due to three reasons: reasonably sufficient allocation of funds, better management of the program albeit through a vertical structure, and treatment largely being availed in the public sector.

Tuberculosis The tuberculosis control program is perhaps the worst performer and the main reason is very poor allocation of funds in the public system. Further, since tuberculosis begins symptomatically with cough and fevers it is treated mainly in the private sector which exploits patients with irrational therapy comprising of cough syrups, tonics and broad spectrum antibiotics. Today there are about 14 million estimated active cases of TB in the country and the state pays very little attention to it. An evaluation team of GOI-WHO-SIDA found that the drugs available in the public system were sufficient to treat only one-third of the patients who actually were receiving care within the public system-this means that the average patient would get only one-third of the treatment required and hence would return with a relapse.

Blindness With 9 million blind persons and 45 million with severe visual impairment this is a very serious scenario. The present focus is on cataract surgery and vitamin A deficiency. The care of the completely blind is under the social welfare department. The resources available for handling cataract and vitamin A deficiency cases are very meager and needs to be enhanced substantially.

The salary syndrome Of the budgets allocated for various programs salaries take away 70% to 90% of the resources leaving very little behind for other inputs like drugs, equipments, travel etc ... While one recognizes that the health sector is clearly a labour intensive one where human resource is the most valuable input, it cannot be denied that without adequate drugs, diagnostics etc. the human resource has little value. Thus if in the present situation 80% of the resource, and increasingly so, goes for paying salaries then the health workforce cannot be effective with the meager resources left over to treat patients, and for preventive and Background paper, MFC annual meet, Jan 1998.

Table 1: EXPENDITURE ON SELECTED DISEASE PROGRAMS (As percentages to total health) Year Andhra Pradesh 1990-1991 1994-1995 Assam 1990-1991 1994-1995 Bihar 1992-1993 1994-1995 Gujarat 1990-1991 1994-1995 Haryana 1990-1991 1994-1995 Karnataka 1990-1991 1994-1995 Kerala 1990-1991 1994-1995 Madhya Pradesh 1990- 1 991 1994-1995 Maharashtra 1990-1991 1994-1995 Orissa 1990-1991 1991-1992 Punjab 1990-1991 1994-1995 Rajasthan 1990- 1991 1994-1995 Tamil Nadu 1992-1993 1994-1995 Uttar Pradesh 1990-1991 1994-1995 West Bengal 1990-1991 1994-1995 Arunachal Pradesh 1990-1991 1994-1995 Goa 1990-1991 1994-1995 Mizoram 1990-1991 1994-1995 Himachal Pradesh 1990-1991 1994-1995 Manipur 1989-1990 1991-1992 Meghalaya 1990-1991 1994-1995 Nagaland 1991-1992 1994-1995 Sikkim 1990-1991 1994-1995 Tripura 1990-1991 1994-1995

Malaria

TB

Leprosy

Blindness AIDS

All Diseases Total Health

10.11 9.82

1.25 1.42

4.62 4.97

.21 .28

.00 .48

16.11 18.79

3325.10 5043.53

7.36 3.90

1.42 .97

1.48 1.32

.75 .80

.00 .00

17.29 7.26

941.22 1883.92

4.96 5.41

.27 .19

339 2.89

.12 .08

.00 .00

9.18 10.34

3856.38 5574.54

4.59 7.12

2.48 2.95

1.78 1.60

.84 .78

.00 .41

10.89 13.76

2478.16 3593.73

10.72 1 1.57

1.81 4.36

.08 .05

.24 .75

.00 .50

12.30 15.33

917.60 1396.29

3.40 3.27

1.80 1.90

.84 .95

.29 .47

.00 .69

4.70 5.58

2698.20 5077.72

1.43 1.75

.80 1.01

.99 1.53

.20 .45

.00 .05

3.96 5.98

2224.32 3759.77

7. 18 7.12

.37 2.31

2.36 1.86

.70 .86

.00 .60

11.02 8.84

2647.20 4609.97

8.58 6.60

2.80 2.48

3.00 2.85

.10 .07

.07 .41

1434 11.87

4341.15 6803.92

5.36 5.73

1.46 1.67

3.66 4.33

.17 .27

.00 .00

11.29 10.98

1550.21 1565.99

8.43 5.67

1.72 2.31

.14 .19

.18 .38

.00 .48

11.88 6.90

1765.76 2312.75

6.66 5.74

2.56 2.23

.40 .36

.26 .44

.00 .14

8.65 8.18

2555.20 4556.96

* *

1.38 1.57

3.54 3.63

.28 .26

4.83 6.20

4894.22 5982.37

7.84 7.11

3.07 2.16

2.45 1.93

.51

15.85 17.35

5826.32 8003.05

6.89 4.27

3.74 3.07

2.43 1.63

.23 .23

13.20 9.18

3256.13 5397.64

2.60 4.57

2.24 2.40

.86 .68

.33

19.14 11.73

144.86 278.07

.99 .77

2.77 2.31

1.70 1.43

.36 .33

5.51 5.13

232.15 350.86

2.56 2.84

2.01 1.69

.57 .83

*

152.10 198.63

.96 .96

1.37 1.22

.57 .47

2.25 2.29

3.37 3.03

.74 .73

.25 .36

2.03 2.00

.59 1.02

3.95 3.56

2.64 2.38

.41

.65

4.07 3.92 4.84 4.17 4.81 2.72 8.71 6.00 7.63 6.52 4.90 3.16 11.24 4.86

1.12

.81

.34

.21

.04 .02 .. 00 .14 .00 .15 .00 .00 .00 .29 .00 .05

11.83 10.12

.00 .72 .00

.00

11.24

* 18.38 15.02

651.42 1274.81 165.10 216.53 212.45 367.38

1.27 .36

.00 .59 .00 3.24

16.16 16.62

203.80 310.82

1.12 1.44

.57 .69

.00 .00

9.80 8.66

70.80 145.80

2.56 2.31

.91 1.77

.00 1.53

15.47 9.42

259.86 368.26

Notes: *Data breakup not available; 1994-1995 data are budget estimates. Sources: Respective state government, Demand for grants, 1993-94 and 1994-95.

4.04

Table 2: EXPENDITURE ON SALARIES FOR DISEASE PROGRAMS YEAR

MALARIA

TUBERCULUSOIS

as %

As %

Andhra Pradesh 1990-1991 80.00 1994-1995 93.29 Assam 1990-1991 .00 1994-1995 9.55 Bihar 1992-1993 95.25 1994-1995 86.88 Gujarat 1990-1991 5.90 1994-1995 3.80 Haryana 1990-1991 81.14 1994-1995 77.95 Karnataka 1990-1991 .00 1994-1995 24.06 Kerala 1990-1991 92.05 1994-1995 90.80 Madhya Pradesh 1990-1991 79.81 1994-1995 66.18 Maharashtra 1990-1991 68.25 1994-1995 76.40

Actuals

LEPROSY

Actuals

As %

Actuals

BLINDNESS

As %

Actuals

336.46 495.42

82.53 81.91

41.85 71.58

85.22 88.88

153.69 250.64

10.21 8.43

7.05 14.36

69.32 73.51

16.60 56.59

13.43 18.20

.00 56.64

14.02 24.91

.00 .00

7.10 15.06

191.25 301.39

2.90 3.24

10.34 10.49

99.54 104.70

130.74 161.19

38.66 34.34

4.63 4.63

113.95 256.03

57.38 41.70

61.47 105.85

68.54 67.63

44.22 57.67

62.75 79.57

21.02 28.10

98.41 161.51

60.75 39.60

16.61 60.83

58.11 77.27

.74 .66

11.76 .00

2.21 10.54

91.97 166.27

66.92 48.26

48.82 96.65

35.60 10.50

22.67 48.48

.00 .00

7.85 23.80

31.95 65.86

51.96 53.59

17.90 37.90

97.69 96.67

22.04 57.37

89.24 70.80

4.46 16.92

190.22 328.04

35.36 73.61

10.04 106.56

83.83 86.50

62.72 85.95

66.29 61.13

18.66 39.46

372.50 448.81

49.66 48.48

121.84 168.50

80.87 78.59

130.39 194.07

.00 .00

4.43 5.04

82.74 84.05

83.14 89.68

69.37 72.72

22.72 26.21

85.53 86.88

56.88 67.75

92.83 32.70

2.79 4.22

68.28 83.45

148.98 131.15

79.32 65.38

30.41 53.50

91.13 87.05

2.48 4.48

52.94 55.77

3.23 8.75

71.46 71.41

170.37 261.60

67.49 62.84

65.61 101.57

88.96 92.04

10.24 16.59

33.53 17.98

6.77 19.85

*

.00

55.52

67.53

92.93

173.10

78.92

13.66

* 1994-1995 Uttar Pradesh 1990-1991 72.48 1994-1995 75.09 West Bengal 1989-1990 96.31 1994-1995 94.72 Arunachal Pradesh 1990-1991 93.39 1994-1995 78.52 Goa 1990-1991 78.88 1994-1995 85.93 Mizoram 1990-1991 95.00 1994-1995 99.36 Himachal Pradesh 1990-1991 83.96 1994-1995 82.95 Manipur 1991-1992 .00 Meghalaya 1990-1991 .00 1994-1995 86.11 Nagaland 1990-1991 74.98 1994-1995 88.70 Sikkim 1990-1991 91.35 1994-195 96.30 Tripura 1990-1991 56.33 1994-1995 45.17

.00

52.20

94.00

93.32

217.01

85.02

15.75

457.20 569.33

86.11 59.15

179.33 172.87

77.41 80.34

143.09 154.19

62.14 50.92

30.16 27.20

147.75 230.59

77.01 72.52

96.05 165.56

87.97 91.81

63.36 87.87

92.53 81.29

4.82 12.56

3.78 12.71

88.62 68.97

3.25 6.67

92.80 89.95

1.25 1.89

.00 .00

.59 .59

2.32 2.70

82.61 86.42

6.44 8.10

82.78 87.20

3.95 5.00

92.86 93.10

.84 1.16

6.20 7.78

88.46 93.27

3.90 5.65

90.20 97.61

3.06 3.35

90.80 95.76

.87 1.65

31.54 53.20

32.59 35.46

6.29 12.21

86.58 83.89

8.94 15.52

.00 .00

3.77 5.94

5.89

.00

4.96

.00

6.56

.00

1.57

18.61 22.03

.00 66.41

.54 1.31

.00 148.03

4.34 7.35

.00 72.73

1.27 3.74

·i6.27 20.27

75.36 82.17

7.71 11.05

114.73 123.00

5.16 7.39

.00 .00

.61 1.11

3.47 4.60

62.50 71.67

.80 .60

.00 .00

.80 2.10

.00 .00

.41 1.00

29.22 17.91

23.67 19.46

1.69 2.98

77.66 76.35

6.67 8.50

45.15 46.92

2.37 6.50

Orissa 1990-1991 1991-1992 Punjab 1990-1991 1994-1995 Rajasthan 1990-1991 1994-1995 Tamil Nadu 1992-1993

Note: *Data not available; Actuals are in Rs. millions spent on each disease program. Sources: Respective state government, Demand for Grants, 1993-94 and 1994-95.

promotive care. If for instance we look at the teaching hospital or other large city hospitals we find that salaries account for about 40% of the budget and thus these hospitals perform more effectively than their rural counterparts like rural hospitals and primary health centres. It must be emphasized here that percentages have been used in the data only as a proxy tool. A more realistic analysis would include using morbidity data to determine the financial requirements or costs needed to deal with it. Unfortunately at the present moment such data is difficult to come by, though we have made a brief attempt in Table 4, but its limitations are explained in the table itself. The data in the tables have been extracted from the CEHAT database which was put together for the national research program on Strategies and Financing for Human Development and this is available presently as a monograph titled "Financing of Diseases Control Programmes in India" by the present authors.

Table 3: PREVALENCE OF SELECTED DISEASES 1992-1993 (per 100.000 populations) STATE

MALARIA

TUBERCULOSIS

LEPROSY

BLINDNESS

ANDHRA PRADESH 7776 407 I 18 5984 ASSAM 10828 638 36 1106 BIHAR ·5712 595 123 2749 GUJARAT 12912 308 29 3266 HARYANA 3732 327 14 824 3412 245 18 869 JAMMU & KASHMIR'" KARNATAKA 1828 136 132 4900 KERALA 448 586 18 1404 MADHYA PRADESH 18912 435 136 3831 MAHARASHTRA 14968 293 72 3534 ORISSA 20592 555 96 3161 PUNJAB 10184 238 28 863 RAJASTHAN 20412 724 128 4661 TAMIL NADU 2304 703 209 836 UTTAR PRADESH 29580 560 222 3101 WEST BENGAL 2712 357 47 914 ARUNACHAL PRADESH 16852 938 110 1012 GOA 972 179 16 2714 MIZORAM 18544 3 11 33 1524 HIMACHAL PRADESH 4564 242 56 1384 MANIPUR 6564 941 199 1442 MEGHALAYA 22892 321 17 759 NAGALAND 11112 491 153 1373 SIKKIM NA NA NA NA TRIPURA 10476 289 0 1430 INDIA 13296 467 120 3001 Note: 1) * = Refers only to Jammu region. 2) Malaria data is incidence of cases. The NFHS data was for 3 months, we multiplied it by 4 to arrive at the annual figure. For other diseases it is point prevalence. Source: National Family Health Survey 1992-93: All India. International Institute for Population Sciences, Bombay, August 1995 (Pg. 205, Tables 8.2)

Table 4: NORMATIVE EXPENDITURE INCURRED PER CASE 1992-1993 (in rupees) The per case expenditure is a normative figure because it is well known that (a) actual utilisation of these government programs is only by one fourth to one third of the population and (b) the establishment costs (salaries etc.) takes away about three fourth of this expenditure. Therefore, the real expenditure per actual case is much higher, but this data helps us look at allocations in terms of disease prevalence across diseases. STATE ANDHRA PRADESH ASSAM BIHAR GUJARAT HARYANA JAMMU & KASHMIR'" KARNATAKA KERALA MADHYA PRADESH MAHARASHTRA ORISSA PUNJAB RAJASTHAN TAMILNADU UTTAR PRADESH WEST BENGAL ARUNACHAL PRADESH GOA MIZORAM HIMACHAL PRADESH MANIPUR MEGHALAYA NAGALAND SIKKIM TRIPURA INDIA

MALARIA

TUBERCULOSIS

77 29 37 29 210 NA 157 274 14 33 13 76 22 NA 15 109 43 212 52 186 46 42 116 NA 33 NA

186 52 19 587 567 NA 1001 96 214 529 146 567 207 167 158 448 658 3426 1985 593 270 733 1386 NA 124 NA

LEPROSY 2445 2448 1175 4693 189 NA 427 5875 811 3002 2185 390 281 1438 891 2484 2431 24070 12444 3628 1690 19265 2919 NA NA NA

I) *= Refers only to Jammu region. 2) The expenditure figures for Orissa and Manipur refer to year 1991-92. Source: Prevalence data: National Family Health Survey 92-93: All India, International Institute for Population Sciences, Bombay, August 1995 (pg. 205. Tables 8.2) Expenditure data: Respective state government Demand for Grants. 1994-95.

NOTE:

BLINDNESS 3 24 2 18 43 NA 5 12 9 2 4 22 6 28 8 25 280 47 136 49 56 139 68 NA 59 NA

Why I Loved MFC? The Medico Friend circle is now 25 years old. I was not a founder-member and came to know about MFC through the first ever issue of its printed Bulletin. I attended the next MFC-meet at Rasulia in 1974; and thereafter barring a couple of meetings, have attended all MFC-Annual, midannual meets, and have been one of those who have spent considerable time and energy for MFC. Why did I do so and why do I now feel somewhat detached from MFC? My brief personal explanation as an activist in the Health-movement in India may throw some light on the 'MFC-phenomenon.' I was (and still am) a Marxist and was looking for a broad platform interested towards working for a fundamental change in the health care system in India. MFC appeared to be such a body. I, therefore, got attracted towards MFC. The attraction was sustained because of the non-sectarian approach of the founder-members and founder-editors; many of whom were a new generation and a new type of Gandhian radicals. They welcomed us despite our known differing ideological background. It was a refreshing experience that doctors and other health-activists and concerned academicians could run a platform on the basis of some mutual, reciprocal trust (besides shared perspective) despite sharp ideological differences. Sincerity, frankness, openness, drives towards grassroots work amongst the rural poor and intellectually stimulating debates were the attractive features of MFC. No funds, no personal politics, no leg-pulling; on the contrary an overall friendly, informal atmosphere, despite some very sharp, rigorous debates, kept us together. MFC did not degenerate into a goody-goody mutual admiration society, (though it did act as a friendly peergroup) nor did it get unnecessarily split in a stupid way into fractions. Has MFC been socially relevant for the health-movement in India? In terms of the indepth discussions on a whole range of important health-issues from a grass-root, people's perspective, yes, MFC has provided the only all India platform for 25 years, for relevant debates; and almost all health-activists in India have learnt from and contributed towards MFC-debates. Given its ideological, geographical and now interest-wise heterogeneity, perhaps a substantially improved performance was not possible. However, more 'professional', socially productive performance was and is still possible. MFC has remained a platform for discussion and a thought current. A couple of health-surveys, including the two surveys of victims of Bhopal Gas Disaster, have been the only collective actions (apart from publishing of the MFC Bulletin) by MFC These surveys were well done, but also gave an experience that collective action is not MFC's forte, given heterogeneity of its members in many respects. This heterogeneity has been a strength when it comes to discussing complex, health issues from a grass-

root level perspective. But for collective action at national level, the same becomes an obstacle. Secondly MFC has not been very active even as a thought-current. MFC has not been responding to various National Health Policy issues-be it privatization of health care, or cut in the already meagre financial allocation for health care, or Universal salt iodization or new draft rules for blood banks, or a legal ban on cross-practice. Even the Bulletin has been quite irregular during the last 5-6 years. So even as a thought current, it has become more and more ineffective.

Compare MFC with, say, the Health-Committee of the Bharat Gyan Vigyan Samiti/All India People's Science Network formed a few years ago. They have been much more active, partly because they have some government funds, but also because they have political commitment to work as a collective at the national level. The National Medicos organization, or BJP-RSS outfit, is also active. One would not of course expect MFC to be active in the same way. But unless there is some attempt to take positions on certain national health issues and to propagate them through the media, MFC would further marginalize itself. (The MFC-declaration opposing the role of a section of medicos during the Post- Babri riots in Jan 93 and against the Pokhran nuclear tests in May'98 have been exceptions). MFC has been unique. It is neither a funded NGO like the VHAI nor a health wing of a people's organisation/movement. Without abandoning this uniqueness, MFC can be far more effective as a thought current. But it is depressing to see that this is not happening. In my view, the main reason for the decline and marginalization of MFC has been, apart from objective factors, the declining inputs by most of the old-timers. With increased responsibility in local work and family matters, and declining energy, since we are well beyond middle-age, it is difficult to give time for MFC. But, if there was more political understanding and hence commitment to develop this pro-people, secular, democratic platform, more inputs would have come forth by going beyond personal interests, likes, and dislikes. But barring exceptions, this has not happened. Only a handful of old-timers are prepared to give time for MFC-work and not many new members are interested in strengthening MFC as such, though many people attend the MFC-meet. I feel somewhat depressed about MFC's future and am feeling myself somewhat detached. The Bulletin continues to be in heavy loss, mainly because there are too few subscribers; and I do not see how it can continue meaningfully for very long. The silver lining to the overall pessimistic scenario is that an enthusiastic team has emerged in South India. Let us hope that it would be able to get wider support to sustain MFC, and to make it socially effective.

Anant Phadke, Pune (Former convenor)

MFC ANNUAL MEET, JAN 1999 Dear friends, Greetings from Vellore! MFC is 25 years old this year. In the mid annual meeting of July, it was decided to use this opportunity to look back into history and forward into time. The theme for the annual meet on January 28-30 is "25 YEARS OF MFC". The meeting will be at Yatri Niwas, Sewagram, Wardha. For each of us this process may have different meanings. Some may reminisce on the past; others may focus on self-evaluation and planning for the future; still some may use it as an opportunity to motivate others and to publicise our organisation. Some points for discussion that emerged at the mid-annual meet are:

* * *

* * *

*

What is MFC? Is it a group of friends or a movement or is it a thought current? What does MFC mean to us individually? How has it helped us in our individual lives and work? What have been the important focuses of MFC thinking? How have these ideas evolved over time? Which are the action strategies that have been taken up by MFC and what did they result in? What has been the influence of MFC on the health movement and health care in general? How can one begin to ask this question? What are the aspects of our organisational strengths that have helped us to survive and what have been its failings? What are the roles and responsibilities of our group at this historical juncture? What are our priorities and how do we propose to work towards these?

Preparatory work that was suggested is as follows: 1. Individuals and friends are requested to send reminiscences and reflections on MFC. 2. Previous Convenor’s and editors are requested to write reflections on their periods in 'office' and their views on the organisation and its future. 3. Abhay Bhang has been requested to write on the early history of MFC along with the other 'founder' members. 4. Amar Jesani and Dhruv Mankad have been requested to write about the organisational structure of MFC. 5. The Vellore and Bangalore groups would attempt to compile the themes of the annual meets and to review action strategies that MFC has undertaken. 6. CEHAT will coordinate a compilation of the index of the MFC bulletins. 7. A suggestion was that we invite groups we are involved with and those we would like to work with to the annual meet. 8. Individual members have been requested to write articles for the lay and the health press on the history of our organisation. The Southern groups will meet on September 20 at CHC, Bangalore, for more detailed preparation. Please send in your suggestions and write ups to the editor so that they can be published in the bulletin before the meet.

Prabir, Madhukar and Anand Convenor’s, MFC.

CONTENTS Author

Page

•Re-thinking Public Health, Food, Hunger and Mortality Decline in Indian History.

S Zurbrigg

1

•National Disease control Program Expenditures

R Duggal & S Nandraj

10

•Why I loved MFC?

A Phadke

14

•MFC Annual Meet, Jan 1999

-

15

The Medico Friend Circle (MFC) is an all India group of socially conscious individuals from diverse backgrounds, who come together because of a common concern about the health problems in the country. MFC is trying to critically analyse the existing health care system which is highly medicalized and to evolve an appropriate approach towards developing a system of health care which is humane and which can meet the needs of the vast majority of the population in our country. About half of the MFC members are doctors, mostly allopathic, and the rest from other fields. Loosely knit and informal as a national organization, the group has been meeting annually for more than twenty years. The Medico Friend Circle Bulletin, now in its twenty third year of publication, is the official publication of the MFC. Both the organization and the Bulletin are funded solely through membership/subscription fees and individual donations. For more details write to the convenor. Subscription Rates: Inland (Rs.) Annual Two years Five years Life Individual 100 175 450 1000 Institutional 200 350 925 2000 Asia (US $) 10 ........................... ................................. .................................. 100 Other Countries (US $) 15 .................................. ................................. .................................. 150 Cheques/money orders, to be sent in favour of Medico Friend Circle, directed to Manisha Gupte, 11 Archana Apts., 163 Solapur Road, Hadapsar, Pune-411028. Please add Rs. 101- for outstation cheques. Convenor's Office: Anand Zachariah, Madhukar Pai, Prabir Chatterjee; C/o Anand Zachariah, Medicine Unit I, CMCH, Vellore, Tamilnadu, 632004 Editorial Office: Sathyamala, B·7, 8811, Safdarjung Enclave, New Delhi-110029. Edited by Sathyamala, B-7, 88/1, Safdarjung Enclave, New Delhi-110029; published by Sathyamala for Medico Friend Circle, 11 Archana Apts., 163 Solapur Road, Hadapsar, Pune-411028. Printed at Colour print, Delhi-110032.

Views and opinions expressed in the bulletin are those of the authors and not necessarily of the organisation.


