---
title: Editorial
description: 
published: true
date: 2023-08-11T12:09:23.108Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T04:14:38.818Z
---

*This is a web-friendly version of the article titled Editorial from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC001-002.pdf](http://www.mfcindia.org/mfcpdfs/MFC001-002.pdf)*

Title: Editorial

Authors: Patel Ashwin

1-2 medico friend circle bulletin Jan-Feb 1976


> Go to the people 
Live among them 
Love them 
Serve them 
Learn from them 
Start with what they know 
Build upon what they have

# Editorial 
This monthly bulletin hopes to continue to provide a common forum for the exchange of experience and views among those who are genuinely concerned with health and health related problems. Many of the field workers have reached to the conclusion: “History and experience show that conventional health services organized and structured as an emanation of ‘Western—type, or other centralized thinking and mechanisms, are unlikely to expand to meet the basic health needs of all people. The human, physical and financial resources required would be too great; the necessary sensitivity to the specific problems of rural or neglected communities would rarely be attained, and even if it was, in limited populations it might not be in a form found acceptable in many communities. It is therefore essential to take a fresh look at existing priority health problems and at alternative approaches to their solution. This is clearly not just a question of applying a little more technical know how. In some situations drastic or revolutionary changes in the approach to health services might be required; in other, sat least radical reforms. The approach should be linked to human attitudes and values, which differ in different communities, and should require a clear motivation and commitment and the part of the people who have the knowledge and the political and/or economic power to introduce change.” 

Many are involved in evolving such a change. And many more are summoning courage to involve themselves actively either in field work or in educational and research fields. Some are bold enough to fight alone, while some need encouragement and friends to share their immense difficulties and numerous frustration, their successes and failure. 

This bulletin is a modest attempt to gather together such lone fighters seeking for an identity and friendship. The bulletin aims to motivate and involve the readers sitting on the fence through dialogue and debate. So it will be primarily a ‘readers bulletins’. 

At Sevagram meet of M. F. C., we found, many lacunae in our present knowledge and information about the present and historical health system structure, various forces determining health planning and new experiments in the field of hearth. This bulletin shall try to cover the deficiencies. Keeping the deficiencies in view, topics for main articles were selected by the editorial committee. (Please see report—A. 6.3) We expect that the style and level of content of the articles will not be those of conventional scientific or leaned journals, <u>but should be within the reach of common man</u>. 

As it is readers' own bulletin, we seek your sincere cooperation through your contribution in initiating and participating in the discussion on various articles and in sending relevent information, book review, reports of your activities and various projects. This number includes one main article on "History of health services in India" and proceedings of second all India meet of Medico Friend Circle held at Sevagram on 17th to 29th, December '75. Your reactions and responses are invited for the same. Keeping in view the above perspective and objectives of M.F.C., reader’s are requested to send in suitable title for the bulletin. 

It regretted that due to some unforeseen difficulties in getting the bulletin printed, we are compelled to bring out a combined issue for the months of January and February. In future the bulletin will be published regularly by the end of each month.

*—Ashvin J. Patel*

## About the bulletin

1. This is a monthly bulletin published at the end of each month. 
2. There will be four sections in the bulletin 
  2.1 Main article 
  2.2 Discussion on previous article 
  2.3 Reports of activities by various groups and projects. 
  2.4 Information, book review, etc. 
3. Each and every material for the bulletin should be sent before 15th of every month.
4. IV.Please type the material in double space or writhe in legible handwriting with enough margin. 
5. Articles etc, may be written either in English or Hindi 
6. Three issues of the bulletin may be sent free to anyone on DD request. Thereafter, if one wishes to continue to receive- the bulletin either one has to pay Rs. 10/- Payable to Ashvin J. Patel, 21 Nirman Society, VADODARA -390005) as yearly subscription or has to become member of M.F.C.