---
title: "Proceedings Of The Second All-India Meet Of MFC"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Proceedings Of The Second All-India Meet Of MFC from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC001-002.pdf](http://www.mfcindia.org/mfcpdfs/MFC001-002.pdf)*

Title: Proceedings Of The Second All-India Meet Of MFC

Authors: Sadgopal Mira

1-2 medico friend circle bulletin Jan-Feb 1976

1.

History Health Services in India D. Banerji* Every community has a health culture of its own— in own cultural meaning of its health problems, its health practices and its corps of practitioners. As a component of its overall culture, the health culture of a community is shaped by the interplay of a number social, political, cultural and economic forces1.The history of the health services system in India provides an account of the influence of 2such forces in giving shape to it. Henry Seigerist has drawn attention to this important aspect by contrasting the manifestly high standards of environment sanitation of the Indus Valley period with the level of sanitation that exists in India today. Health practices before the British rule Describing the five thousand year old planned city of Mohenjo Daro, Marshall3 has remarked that the public health facilities of the city were superior to all other communities of the ancient Orient. Almost all households had bathrooms, latrines, often water closets and carefully built wells. The elaborate nature of the Indus Valley public health organisation provides an indication of the extent of health consciousness among the ancient Indian people. It is difficult to conjecture the nature of the health problems of those of disease indicates a fairly mature attitude of the society towards the health problems that might have been prevailing at that time. The Vedic medicine that developed after the advent of the Aryans to the Indus Valley (? During the second millennium B.C.) had begun to show a tendency to develop rational methods of approaching health problems at quite an early 4 stage p. 462. Even in the Vedic Samhitas, purely religious books are found reflection of anatomical,

Physiological and pathological views which are neither magical nor religious and there are references to treatments which are impressively rational. Furthermore there exists the famous decree to Emperor Ashok Maurya (279-236 B.C.) in his second Rock Edict (257-236) “celebrating the organisation of social medicine shaped by the Emperor along with the lines of Buddhist thought and kindred 4 ethics (dharma)” p. 86. the works of the famous Charaka of the first century A.D. and of Susruta of the fourth century A. D. laid the foundation of the highly developed science of medicine which flourished in the tenth century after Christ—a period of all round social and economic progress often called the age of Indian Renaissance. There are also epigraphically evidences indicating that social medicine was practised in medieval South India4 p. 87. During the subsequent centuries, a series of political, social, and economic changes profoundly disrupted the ecological balance in Indian society. Perhaps the lowest point of this ecological crisis was reached during the decline of the Mughal Empire, a situation which set the stage for the British conquest of India. Even during this period the system of Indian medicine had retained some fragments of its past heritage; for example, the surgeons of the British East India Company learnt the art of rhinoplasty from Indian exponents of surgery5. It is noteworthy that during the early period of British rule in India, the western system of medicine, which was still dominated by such procedures as purging, leeching, scarification and blood letting, could not be considered to be any, superior to the prevailing methods of the Indian systems of medicine.

Health practices during the British rule The social, cultural, economic and political change that followed the introduction of the British rule, in India dealt an almost a fatal blow to the practice of the Indian systems of medicine. With the imposition of the British rule, almost every facet of Indian life, including medical and public health services, was subordinated to the commercial, political and administrative interests of the Imperial Government in London. In developing health services for certain limited purposes (for example. for the army), the patronage was shifted from the Indian systems' of medicine, to the western system. The decision to make this shift appears to be amply vindicated by 'the advances 'in the 'different branches of western medicine during the nineteenth and twentieth centuries. As a result of these changes the already stagnant Indian systems of medicine got caught in a whirlpool of a vicious circle: its very neglect accelerated it$ further decline and the decline, in turn, made it increasingly difficult for it to compete with the highly favoured and rapidly flourishing western system in capturing the imagination or the educated population of India. In the long run, therefore, not only did the professions of the Indian systems, of medicine get infiltrated by various kinds of quacks but the very basis of the sciences lot considerably eroded by forces of superstition and of beliefs 6 in supernatural powers and deities . The British had introduced western medicine in, Indra in the tatter half of the eighteenth century principally to serve their colonial aims and objectives. Medical services were needed to support the British army and British civilian personnel living in India. Later on, medical services were made available to a very tiny selected segment of the native population. At the lime of Independence, only the affluent and the ruling classes could get adequate medical services. Of the rest, constituting more than 90 percent of the population, only a small fraction could gel some form of medical care from hospitals and dispensaries run by Government agencies, missionaries, philanthropic intuitions 7 and private practitioners , pp. 35-44. Similarly public health services consist of some form of environmental sanitation in a few big cities. For the rest some public health services were provided only when there was an outbreak of break of massive epidemics of diseases as plague, cholera and 7 smallpox , p. 49. Because of these conditions, inspite of the availability of know1edge from the western system of medicine, there was widespread prevalence of such easily preventable diseases as malaria. Tuberculosis, leprosy, smallpox, cholera, gastro-intestinal infections and infestations, trachoma and filariasis; India was among the countries of the

world with the highest infant and maternal morbidity and mortality and gross death I ales. In addition, there was the enormous problem of undernutrition and malnutrition. India was among the lowest per capita calorie consuming countries 7 in the world pp. 88-14 6. At the time .of Independence, British India (population 300 million) had

17,654 medical graduates 29, 870

licentiates, 7,000 nurses. 750 health visitors, 5, 000 midwives. 7

7S pharmacists and about 1,000 dentists , p. 18-85. The colonial character of the health service had also profoundly influenced a1most all aspects of Medical education in India-in shaping the institutions, in developing the course content and perhaps most important of all in shaping the value system and the social outlook of the Indian physicians. The first medical college in India was established way back in 1835. It was quite natural that British teachers should have nurtured such institutions in their infancy. However, along with the "scientific core” of medical science (which was a most welcome diffusion of a cultural innovation from the western world), there came certain political, social and cultural over coatings which were definitely against the wider interests 8 of the Country . Also, opportunities for medical education in these institutions were made available to the very privileged upper class of the society. Additionally, the Medical Council of India accepted the British norms of medial education in order to gain recognition of the Indian medical degrees from the British Medical Council. This enabled some of the physicians, who were "the select among the select", to go to, Great Britain to get higher medical education. Acquiring Fellowship or Memberships of the various Royal Collage was generally considered to be the pinnacle of achievement in their respective fields. These four considerations— colonial values system of the British rulers class orientation of Indian physicians, their enculturation in British modelled Indian medical college and a more thorough and more extensive indoctrination of future key leaders of the Indian medical profession in the Royal college— provided a very congenial setting for the creation of what lord Macaulay had visualised as “Brown Englishmen”8. These Brown Englishmen acquired dominant leadership positions in all the facts of the health services in India. This arrangement proved convenient to both the parties. To the Indian physicians it ensured power, prestige, status and money at have. Their montors from foreign countries retained considerable influence on the entire health service system of the country by ensuring that the top leadership of the medical profession in India remained heavily depended on them. (Turn to page-4)

Go to the people Live among them Love them Serve them Learn from them Start with what they know Build upon what they have Editorial This monthly bulletin hopes to continue to provide a common forum for the exchange of experience and views among those who are genuinely concerned with health and health related problems. Many of the field workers have reached to the conclusion: “History and experience show that conventional health services organized and structured as an emanation of ‘Western—type, or other centralized thinking and mechanisms, are unlikely to expand to meet the basic health needs of all people. The human, physical and financial resources required would be too great; the necessary sensitivity to the specific problems of rural or neglected communities would rarely be attained, and even if it was, in limited populations it might not be in a form found acceptable in many communities. It is therefore essential to take a fresh look at existing priority health problems and at alternative approaches to their solution. This is clearly not just a question of applying a little more technical know how. In some situations drastic or revolutionary changes in the approach to health services might be required; in other, sat least radical reforms. The approach should be linked to human attitudes and values, which differ in different communities, and should require a clear motivation and commitment and the part of the people who have the knowledge and the political and/or economic power to introduce change.” Many are involved in evolving such a change. And many more are summoning courage to involve themselves actively either in field work or in educational and research fields. Some are bold enough to fight alone, while some need encouragement and friends to share their immense difficulties and numerous frustration, their successes and failure. This bulletin is a modest attempt to gather together such lone fighters seeking for an identity and friendship. The bulletin aims to motivate and involve the readers sitting on the fence through dialogue and debate. So it will be primarily a ‘readers bulletins’. At Sevagram meet of M. F. C., we found, many lacunae in our present knowledge and information about the present and historical health system structure, various forces determining

health planning and new experiments in the field of hearth. This bulletin shall try to cover the deficiencies. Keeping the deficiencies in view, topics for main articles were selected by the editorial committee. (Please see report—A. 6.3) We expect that the style and level of content of the articles will not be those of conventional scientific or leaned journals, but should be within the reach of common man. As it is readers' own bulletin, we sleek your sincere cooperation through your contribution in initiating and participating in the discussion on various articles and in sending relevent information, book review, reports of your activities and various projects. This number includes one main article on "History of health services in India" and proceedings of second all India meet of Medico Friend Circle held at Sevagram on 17tb to 29th, December '75. Your reactions and responses are invited for the same. Keeping in view the above perspective and objectives of M.F.C., reader’s arc requested to send in suitable title for the bulletin. It regretted that due to some unforeseen difficulties in getting the bulletin printed, we are compelled to bring out a combined issue for the months of January and February. In future the bulletin will be published regularly by the end of each month.

—Ashvin J. Patel

About the bulletin 1.

This is a monthly bulletin published at the end of each month. 2. There will be four sections in the bulletin 2.1 Main article 2.2 Discussion on previous article 2.3 Reports of activities by various groups and projects. 2.4 Information, book review, etc. 3. Each and every material for the bulletin should be sent before 15th of every month. 4. IV.Please type the material in double space or writhe in legible handwriting with enough margin. 5. Articles etc, may be written either in English 'or" Hindi 6. Three issues of the bulletin may be sent free to anyone on DD request. Thereafter, if one wishes to continue to receive- the bulletin either one has to pay Rs. 10/- Payable to Ashvin J. Patel, 21 Nirman Society, VADODARA -390005) as yearly subscription or has to become member of M.F.C.

The History of… (Continued)

The present state of the health services in India Considering the size of the population and the staggering nature of its health problems the existing health services are grossly inadequate. Further more, the bulk of the/ expenditure is earmarked for curative services and these services, are predominantly situated in urban areas and they are more accessible to the more privileged sections of the society. The privileged population has the additional advantage of being able to pay to avail of private nursing home services land services of private practitioners who are located almost entirely in urban areas. Fifty three present of the doctors in India are in private practice; another 7 per cent 9 are employed in the private sector p. 71-71, the community

spends about Rs. 100, 000 for the training of one doctor. India has barely, half a bed per thousand populations, while the corresponding figure is over 10 for industrialised 10 countries p. 69. Ninety per cent of these beds are located in cities and town, where only one fifth of the population lives. Even the 10 percent of the beds which are primarily meant for rural populations are ill-staffed, ill-equipped and ill11 financed p. 27 and 121. The expenditure for curative services 12 is about three times as much as for preventive.services . Again, in terms of the preventive Services, while over 90 per cent of the urban population is provided with some degree of' protected water, only four per cent of villages get piped water supply; while about 40 per cent of the urban population has a sewerage system, it is almost non-existent 13 for the rural population P. 20. Primary health centres and their sub-centres form the sheet anchor of rural health services of India. There are over 5.195 PHCs in the country; there are 32,218 sub-centres 10 attached to these PHCs p, 35. Each PHC and its sub-centers are expected to provide integrated health, family planning and nutrition services to a population of about 100,000. Provision of medical care, environmental sanitation. maternal and child health services, family planning services, eradication or control of some of the communicable, diseases and, collection of vital statistics are some of the functions of a PHC 14, p. 63. However, bath quantitatively as well as qualitatively the resources made available at a PHC are grossly inadequate for serving the population assigned to it 15, 16, 17, 18 . There are now 103 medical colleges which 'have an 10 p. 20. The annual admission capacity of over 13,000 number of doctors available in India has now increased to 10 p. 20. There are 88,000 trained nurses, 32.000 137,930 10 sanitary inspection and 54.000 auxiliary nurse midwives p. 20.

The, government is at Present financing about 9,000 dispensaries and 195 hospital which offer the services according to the Indian systems of medicine. There are 44.460

institutionally qualified and 111,371 non-institutionally qualified Ayurvedic registered practitioners in the country; the corresponding figure for the Unani and the Sidha systems are 6,013 and 18, 507 and 625 and 14,785 19 respectively . The Government runs two postgraduate collages in Ayurveda and one in Unani; there are also 91 19 Ayurvedic, 10 Unani one Siddha undergraduate colleges . That the present health services system of India needs considerable improvement is dramatica1ly brought home by the fact that in the year 1974 India happens to be one of the few' countries in the world which has not yet succeeded in eradicating smallpox. Much remains to be done before it win be possible to control such apparently easily controllable diseases as tuberculosis, leprosy, trachoma· 20, and 'filariasis p. 221. The fact that the National Malaria Eradication Programme continues to be a very heavy drain OD the very limited resources even today, instead of being eradicated by 1966.also provides an indication of the serious weakness in the system.

REFERENCES: I. Galdston, Iago (1961): Doctor and Patient and Medical History: The Seventh Annual Max Danzis Lecture, Newark N. J.: The Newark Beth Israel Hospital. 2. Marti-Ibanez, F. (1960): Henry Seigerist on History of Medicine, New York: MD Publications. 3. Marshall, J. H. (1931): Mohenjo Daro and the Indus Valley Civilization, London: A Probsthein.

4. Zimmer, H. R. (1948): Hindu Medicine Baltimore: Johns Hopkins Press. 5. Basham. A. L. (1954): The Wonder That Was India, London: Sidgwick and Jackson, p. 500. 6. Banerji D. (1964): Health Problem and 'Ha1da Practices in Modern India: A Historical Interpretation, The Indian Practitioner, XXII, 137-143. 7. India, Government of Health Survey and Development Committee (1946): Reprint, Volume I, Delhi: Manager of Publications. 8. Banerji. D. (l97.l): Social Orientation or Mc4ictI Education in India, Economic and Political Weekly VIII: 485-488.

9. Institute of Applied Manpower Research and the National Institute of Health Administration and Education (1966): Stock of Allopathic Doctors in India, IAMR Report No. 2/1966, New Delhi: Institute of Applied Manpower Research.

(Turn to Page 8)

Proceedings of the Second all-India meet of Medico Friend Circle held at Sevagram on 27th 28th & 29th December 1975 Report—A The Meet New and old members of Medico Friend Circle gathered at Sevagram (Wardha, Maharashtra) for the second annual allIndia meet which began on December 27. Those of as who were regular participants at the three-day meet numbered about 70, gathered from many far-flung regions: Delhi, Ajmer, Kutch, Baroda, Ahmedabad, Nagpur, Wardha, Bombay, Indore, Hoshangabad, Varanasi, Calcutta, Midnapore, Hyderabad, Madras and Cochin. Participant included practicing doctors, medical research scientist, post graduate student, medical college lecturers, medical students, research scholar, social workers, agriculturalist health worker, school teacher and classical singer. Among us, some had settled down to work on specific field projects involving rural health care development. Sevagram Medical College provided us with comfortable and congenial accommodation adjacent to the Ashram where Mahatma Gandhi lived during his last years. Smt. Sushila Nayar, the founder-director of the M. G. Institute of Medical Sciences of Sevagram, participated in the few of the discussion sessions. The initial group session was taken up by the participants turn by turn acquainting each-other with their present occupations, backgrounds, reasons for coming to the meet, and special interests, painted a colourful collage of personalities and aspirations

For discussion of the working papers we divided ourselves into tow equal groups. The paper prepared by Ashvin Patel (Vadodara) directed us towards a definition of health, its problems and needs, and was taken up first. The second working paper, prepared by Lalit Khanra (Midnapore, West Bengal) concerned with an elaboration of the alternative systems of medicine that are practiced today in our country: ‘•western’ (erroneously called ‘allopathy’) homeopathy, biochemic, ayurveda, yoga, naturopathy, Unani, astrology, acupuncture, folk medicine and magic. Third was a paper which Mira Sadgopal (Hoshangabad, Madhya Pradesh) had prepared to examine present and historical failures and successes in the continuing pursuit of practical health care alternatives for our country’s people. The fourth working paper, drawn up by Ashok Bhargava (Varanasi), placed the organisational structure and administrative and financial needs of MFC squarely before us to enable us to make some important practical decisions. On the basis of this guide and

Probably strengthened by insights gathered during the previous two days, we were able to make the following decisions: 1.

It was agreed upon that MFC should continue as a loose, extra-constitutional, unregistered organisation for the coming year at the end of which the position may be reviewed with membership being open to all persons involved in health and health related activities. 2. Ashok Bhargav was asked to continue his role as convener for an additional one year. 3. Dues for members were reconfirmed at Rs. 20/-

per year, payable in one or two installments to the Convener, Medico Friend Circle, Rajghat, Varanasi-221001 (This amount is roughly equivalent to the cost of production and mailing of twelve monthly bulletins and circulars to each member in a year.) Therefore, those who can afford it should voluntarily pay more, while in certain justifiable individual cases the dues could be partially waived, at the discretion of the Convener. 4. Membership (principally meaning right to receipt of bulletins) would be cancelled (by removal from the mailing list) incase of delay of paying dues without written communication for over three months. 5.1. Last year income and expenditure were reviewed, and the budget for the year 1976 was agreed to as follows:

Expenditure: 1. Convener’s salary 2. Office maintenance 3. Bulletin

1915, Rs. 3000-00

Rs. 1000-00 Rs. 1000-00

4. Transport Rs. 1000-00 5. Camp and annual meet Rs. 3300-00 6. Contingencies —

1976 (expected) Rs. 36OO-00

1000-00 Rs. 4000-00 Rs. 1400-00 Rs. 4000-00

Rs. 2000-00 Rs. 16000-00

Total

Rs. 9300-00

INCOME:

1975

1976 (expected)

Rs. 300-00

Rs. 2000-00 Rs. —

1. Membership fee 2.Shanti Sena Mandal

Rs. 5500-00

3.Grant from Voluntary

—

Action Cell 4. Funds raised by regional groups and individuals

Rs. 500-00

Total

Rs. 9300-00 Rs. 16000-00

Rs. 3000-00 Rs. 1400-00

5.2 Though it was agreed that MFC should have a full time paid convener with a pay of Rs. 300/- per month, Ashok conveyed later that he would not draw the salary.

5.3 The responsibility to collect Rs. 14, 000/- (no.4 of Income for year 1976), it is agreed, lies solely on the groups i.e. each group will have to collect at least Rs. 1500/- this year. 6.1 It was pointed out that the MFC Bulletins were ill planned and with proper planning the Bulletin might have put to much better use. Hence an editorial committee was selected from those assembled. These persons major function is to select priority fields of study from among the vast number of possibilities suggested and to stimulate logical and logical and progressive exchange of experiences and research in these fields through the monthly bulletin. Persons were included in the committee mainly on the basis of willingness to work and their relatively deeper involvement.

6.2 The editorial committee consists of following members: 1. Imrana Qadeer,

2. Prakash Bombatkar (Delhi)

3. Satish Tibrewala (Bombay)

5. Mira Sadgopal (Hoshangabad)

7. George Isaac

(Wardha)

4. Kamal Jay Rao (Hyderabad)

6. Abhay Baal (Nagpur)

8. Sathi Devi

(Cochin)

9. Bhoomikumar J.

(Madras)

(Varanasi)

10. Suhas Jaju (Nagpur)

11. Lalit Khanra12. Ashvin, J. Patel (Midnapore) (Vadodara), Editor. (Rajiv Raj Choudhry in particular and Vadodara group in general will be responsible for the production and circulation of the bulletin.) 6.3 The subjects chosen by the editorial committee for discussion during the year 1976 are as follows: A. Health Structure Analysis- i) History of health service in India. ii) Critical assessment of

the present health services. iii) Critical analysis of Pharmaceutical Industry

B. Present Health Problem-

i) Nutrition—a PCM

b) Deficiency anaemia c) Vit A

deficiency ii) Communicable diseases

a) Malaria

b) Tuberculosis iii) Population problem

C. Alternative Approach- i) Health team approach ii) Auxiliaries iii) Comprehensive Development Approach

64 It was decided that MFC bulletin should become a

printed magazine instead of cyclostyled one in order to accommodate more material. Articles may be written either in Hindi or English. 65 It was also decided to continue the practice of sending, three issues of the bulletin free to anyone on the recommendation of a member. Thereafter, if one

wishes to continue to receive the bulletin, either one has to pay Rs. 10/- as yearly subscription or baa to become member of MFC. 7. It was decided to have atleast one regional work camp (summer) and one annual meet (winter) organised by the Convener and attended by members from the whole country. 8. Local geographical groups of medico friends will meet from time-to-time for critical study and discussion, drawing upon their own day-to-day local experiences as well as from material distributed through the monthly bulletins. Form time-to-time, they should try to, distill

whatever insights are gained through these explorations and offer them to the wider circle, through the bulletins or through extra communications.

It was possible for several groups to form from the following regions: Delhi, Bombay, Calcutta, Nagpur, Ahmedabad, Vadodara, Indore, Sevagram, Wardha, Calicut and Varanasi. Other groups will naturally form as our circle grows.

9. An Advisory Committee was formed by the Convener to help him in implementing the programmes of MFC consisting of the following members. 1. Chanchala Samajdar. 2. Vidyut Katgade (Calcutta)

(Varanasi)

3. Nandkishor Joshi (Indore)

4. Rajivraj Choudhry

5. Kartik Nanavati

6. Satish Tibrewala

(Ahmedabad) 7. Kanchanmala, N. P.

(Bombay) 8. Suhas Jaju

(Calicut)

9. Bhakti Dastane (Sevagram)

(Nagpur)

10. Monghiben Mayatra (Kutch)

10.It was generally agreed that there need not be an intensive membership drive, but that rather we should seek to consolidate our group, to deepen our understanding and commitments, and to accept new members who join on their own initiative through personal contacts rather than through recruitment. (Turn to page – 12)

Report — B Report of the Discussion on the Paper

Our Present-day Health Problems and Needs This paper was prepared to define the concept of health and to decide our present day priorities about health problems and need. The paper consisted of three parts-I. Definition of health, 2. Present day health problems of India and, 3. Present day health Deeds of India. The house reached on the following conclusions: 1. Definition of health

Though the definition of health given by WHO (Health is a state of complete physical, metal and social wellbeing and not merely an absence of disease or infirmity.) was accepted, it was found deficient. It was emphasised that health should be viewed also as an autonomous ability to cope with pain sickness and health. 2. Present day health problems of India

Limited resources and multiplicity of problems compel us to draw priorities. In doing so though the individual should not be neglected the criteria for deciding priorities should be in the benefit of the community as a whole. The following problems were identified unanimously for immediate action: 2.1 Communicable diseases, nutritional problems and defective environmental sanitation need priorities to degenerative diseases, inborn errors of metabolism etc. 2.2 Ignorance of the education and that of the uneducated people are both important problems, but that of the educated elite is more deplorable. 2.3 Wrong allocation of funds to the powerful vasted interests was thought to be more responsible than sheer lack of economic resources. 2.4 Existing maldistribution of medical personnel (as) between urban and rural population) and shortage of basic health workers were expressed as major problems.

2.5 Irrelevance of the present medical education, research and health services were discussed again (as it was discussed in Ujjain Meet, December ‘74). Aspiration and identification with foreign affluent health system poor quality of medical teaching, migration of doctors to foreign countries, priority to training doctors rather than auxiliaries, selection system for medical personnel, expensive hospital based health services, non-involvement of traditional practioners and community in health services were identified as pitfalls in our present health system.

2.6 Over reliance on drugs by doctors, high cost and poor availability of drugs profit oriented drug industries, adulteration and black marketing of drugs were condemned. At the same time, use of naturally and easily available medicines was pointed out as an ignored area. Good quality work already done was not available for ready use. 2.7 Health problems can neither be viewed nor solved in isolation from socio-economic, political and other facet or community problems. 2.8 (Question was raised whether today's medical care is really beneficial. This question was viewed keeping in mind the: following proposition: “A professional and physician based health care system which has grown beyond tolerable bounds is sickening for three reasons: It must produce clinical damages which out weight its potential benefits; it cannot but obscure the political conditions which render society unhealthy; and it tend to expropriate the power of the individual to heal himself and to shape his or her environment ...Neither decline in any of the major epidemics of killing diseases, nor major changes in the age structure of the population, nor falling and rising absenteeism at the workbench have been significantly related to sick care not even immunisation…But the benefits claimed owe much more to the advancement in other areas like communication, agriculture etc. ") It was agreed that advantages cannot be generalised a regards today's medical cafe, as there were some advantages which could not be isolated from advances in other areas. 2.9 (Here question was posed whether today's, medical care encroaches upon personal freedom. The proposition was "The extention or professional control over the care of healthy people e.g. antenatal clinic, well baby clinic etc. and medicalisation of prevention and early diagnosis turns the healthy people into lifelong patient. This is considered as the encroachment on personal freedom and ability to react to an experienced reality e.g. pain, sickness, death and various stages of life span like childhood, adolescence and old age,”)

It was general agreed that mutable medica1 care system (as it is today) was viewed as not encroaching upon one's freedom but it was held that our system should be such that personal freedom is not jeopardised. 2.10 The problem of population control and that of high maternal and infant morbidity and morbidity were accepted to be interlinked. Anyhow, house was divided in fixing the priority between these two problems.

3. Present day health needs of India It was realised that present health system is capital intensive, curative and elite-based one but we Deed labour intensive, integrated and mass-based one. In the light of conclusion the following needs were identified: 3.1 Integrated (curative preventive and, promotive) approach was favoured. 3.2 Training of basic doctors and multipurpose health workers was emphasised instead of specialists and multiple workers. 3.3 Local resources and manpower could be used to fulfill the deficit of full-fledged medical services. 3.4 It was suggested that selection of health worker and medical student should be done by the local community. But it was felt that unless equal educational facilities are made available to all, simply a change in selection policy in medical college is not going to be of much use. Need oriented cubiculum will help in solving the problems of selection of medical students 3.5 The following changes in medical education were suggested Medical students should be involved is community development projects throughout their training i. e. dehospitalisation of medical education. Co-education with paramedical personnel was though to be important and possible. Dissertation and thesis in community problems should be encouraged, training in medical administration should also be incorporated in medical education. 3.6 The redistribution of health personnel and should be such that it would facilitate ‘chain of referal system’ rather than multiplying specialised institutes at the cost of peripheral health service.

3.7 The need for improvement in environmental sanitation was stressed. 3.8 Availability of inexpensive, easily available and effective drugs was emphasised. 3.9 It was realised that integrated economic development conducive to family planning and not mere emphasis on mass sterilisation or other aids to family planning would solve the problem of population explosion. 3.10 Need to educate everybody including administrators, planners, professional, politicians etc. about our real priorities was pointed out. 3.11 Need for dehospitalisation and deprofessionalisation of health services was repeatedly emphasised during discussion.

3.12 To implement the identified needs, we need to create an effective demand. Because Health system can not be changed in isolation, we need to touch the wider social problem and share the emergent responsibility.

—Kamala Jaya Rao, Kartik Nanavati, Vidyut Katgade.

Reference (Cont.) 10. India, Govt. of, Ministry of Health and Family Planning, (1973): Pocket Book of Health Statistics. New Delhi: Central Bureau of Health Intelligence, Directorate General of Health Services. 11. India, Government of, Ministry of Health Family Planning and Urban Development. The Study Group on Hospitals (1968): Report New Delhi: Ministry of Health Family Planning and Urban Development. 12. West Bengal, Directorate of Health Sen_ (1971): Health on the March 1948-69: West Bengal, Calcutta: State Health Intelligence Bureau.

13. India. Government of, Ministry of Health and family Planning (1973): Report 1972-73, New Delhi: Ministry of Health and Family planning. 14. Dutta, P. R. (I965): Rural Health Services in India: Primary Health Edition, New Delhi: Education Bureau.

Centres, Central

Second Health

15. The Johns Hopkins University, School of Hygiene and Public Health, Department of International Health (1970): Functional Analysis of Needs and Services, Baltimore: the John Hopkins University. 16. National Institute of Health Administration and education (1972): Study of District Health Administration, Rohtak (Phase I), N. NIHAE Research Report No. 7, New Delhi: National Institute of Health Administration and Education.

17. India, Government of Planning commission (1973)

Draft Fifth Five Year Plan: 1974-79, Volume I, New Delhi: Planning Commission. 18. India, Govt. of Ministry of Health and Family Planning, Committee on Utilization of PHC Beds in India (1974): Report. New Delhi: Ministry of Health and Family Planning. 19. India, Govt. of Central Council of Health (1974): Indian Systems of Medicine and Homeopathy, Agenda item No. 6, New Delhi: Ministry of Health. 20. India, Govt. Planning Commission (1972): The Fourth Plan: Mid-term Appraisal, Volume II, New Delhi: Planning Commission.

Report—D Report Of The Discussion On The Paper

Health for the People: Finding A Practical Way

The paper consisted of five major parts. First part was an enquiry into present and historical failures and successes to assist the participants in deciding the direction of their future commitments. Second part dealt with inter-relationship among the doctors, paramedical workers and community. Third part dealt with policy for train in paramedical worker and doctors.

Fourth part challenged the participants to draw a vivid picture of future society in which their practical

health system would function. Fifth part was a search for any outstanding health service out side of India having strong relevance to India’s particular needs. It was felt that there were little sensory inputs to

discuss and analyse the first part of the paper. The second and third parts were already touched by other papers. Fourth and fifth part could not be discussed due to lack of time. First of all, the direction set by the Ujjain Meet of MFC (December 1974) for developing, alternative approach for developing countries was recollected. Then following points were highlighted during the discussion. 1. Present system of health services bas failed to

2.

3.

4.

provide easily accessible, cheap and effective services to everyman in this country. Neither the basic medical science could be provided nor could positive health be achieved. Health services which are planned according to our economic, social and cultural circumstances and which meet the needs of our society are ideal for our country. It was emphasised that the role of a doctor is not merely if an expert but also that of a social worker, as health problems can’t be changed in isolation from socio-economic and political context. In the absence or massive social change, a system

depending on the personality of an outstanding and charismatic leader, is not likely to be meaningful and viable. So one must conceive health approach as a part of total comprehensive development process instead of in isolation. 5.

Training of doctors and paramedical workers should ensure co-ordination between each-other and with community. And following suggestions were recommended for the same:

5.1 Joint education of doctors and paramedical workers together for relevant subjects. 5.2 Interaction with community during training period. 5.3 Selection of trainee and student candidates by the community and from the community.

5.4 Interaction of useful western knowledge with traditional practice and cultural knowledge. 5.5 Distribution of doctors and paramedical staff’s time should be conducive to fulfil decided priorities avoiding the duplication of efforts.

—Ashvin Patel

Report-C

Report of the discussion on the paper Alternative Approach: Various ‘Pathies’. The discussion began with the gist of a Marathi article on the merits of Ayurveda being read out. Thereafter the discussion focussed on the original working paper. The question was raised whether the integration of different ‘pathies’ meant maintaining an open attitude towards one another or it meant a curriculum of undergraduate teaching where the basic principles of all the pathies should be taught to all students. After some debate it was generally reed

that teaching of all the ‘pathies’ in one curriculum will cause a lot of confusion of the different approaches of different ‘pathies’. So long as there it no system which can take into account the different aspects of all the • pathies ' it will be wise on our part to keep our minds open to the local utility aspect of other ‘pathies’ than our own and avoid futile debate about philosophical and pharmacological intricacies. In this context it was pointed out by some members that the working paper did not sufficiently highlight the social utility aspect of different • pathies'. It wall also suggested that members should try individually (or in group) to learn as much as they can about "pathies” other than their own. The group then 'listened to a short talk on the merits of Ayurveda from one of the members' present. The elaborate preventive aspects of Ayurveda come to many of us as an eye opener. The group looked for similar enlightenment about other • ‘pathies’; but this wish remained unfulfilled as the other "pathies were not represented sufficiently. It was therefore felt that MFC members should enlist co-operation of the members and practitioners of other ‘pathies’ by explaining to them objectives of MFC and to collect relevant information in order to eliminate existing ambiguities and effect integration through critical and cultural acceptance of health care facilities by the people:

-Lalit Khanra

Report-E

4. To arrange medico camps, conferences, seminars

Medico Friend Circle

specially on rural health projects and convening the MFC’s annual meet. 5. Finance the expenditure involved in all these central activities will, it is expected, be shared and horne by the members. The membership fee has been fixed Rs. 20/- per annum to be paid in not more than two installments. The money should be sent to Central Office. < Medico Friend Circle, Rajghat, Varanasi-221001. (It is understood that capable members should pay more than this minimum limit and that the Convener can waive or reduce this fee in deserving cases.)

Objectives Organization and Programmes Objectives

Medico Friend Circle is a group of people involved in health and health related activities, dissatisfied with the present system of health services in India and conscious about the' problems and their responsibilities in relation to society. It works with the following aims in view:(a) To

evolve a pattern of medical education and methodology of health care relevant to Indian needs and conditions; and,

(b) To make positive efforts towards improving the

non-medical aspects of society for a better life, more humane and just in contents and purposes.

Organisation MFC will not be constituted as a rigid organisation 'of medicos. It will remain a loosely-knit group, dissimilar though in ramifications but thinking and working for similar goals and as a homogeneous unit. Any person involved in health and health related activities can become its member. The members are expected to form groups and circles where possible.

Function Functioning of MFC will be mainly through its members and groups at various places. Central co/ ordination will be done by the Central Cell which among others will perform the following functions:I. Co-ordination, through a regular monthly bulletin which will contain; (a) Thought provoking articles encouraging questioning and discussion providing information about various issues in which MFC may be interested. (b) Reports of activities by members or groups. (d) Supply in a names of books and articles or any other material containing useful information. 2. Maintaining a ‘fact bank' which will store information data, facts and list of problems which may interest the members. Providing material for the bulletin and ‘pact bank' is the responsibility of the members who are expected to be active in writing and sending them to the persons concerned. 3. Maintaining of data of all members and persons with a of

helping

communication.

and

encouraging

contacts

(A) 1. Each member should select a problem 01 topic, study it thoroughly and then circulate the knowledge to others. The study should not be a mere academic gymnastic. It should be: something concerning health and health related activities keeping in view the social needs and perspectives e.g., poverty, mal-nutrition, failure of malaria eradication programme etc. To collect data or try to study any problem which can become a topic for research. Small practical problems and simple observations should be the choice. (B) 1. To study other-'pathies' to learn their useful parts; and seek and enlist the cooperation of their adherents. (C) 1. To emphasise more on preventive and total medicine during their educational period. Try to curtail the unnecessary use of drug, and use minimum amount of drugs. Emphasise more on health education, prevention of diseases during practice. To seek the cooperation of the N.S.S. and P.S.M. departments for these activities. (D) 1. Study sociology, economics, political

(c) Extracts of useful articles studied by members.

view

Programmes for individuals and groups

and

science and similar social sciences, because a doctor is not merely a physician of individual patients but also a social being and so he should understand society, its working and its problems. To discuss various socio-medical problems with other friends, try to create an awareness among them, and try to develop a group of medicos with similar interests at their p1acle. (E) 1. To learn clinical medicine perfectly, relying less on

costly investigations.

Try to learn nursing procedures and basic investigations yourself.

(F) 1. Not to accept ‘physician’s samples’ from the medical

representatives as it is a subtle corruption. Symbolic acts to change the social values e.g. doing productive labour etc, and to give up cultural slavery. To oppose ragging in Medical Colleges at individual and group levels. Try to improve relations among the different categories of health workers. Try to learn more about ‘health team’. Help blood donation activity. Opposing strikes by health personnel for selfish purposes. (This was considered to be a controversial point and any decision is postponed till further discussion.) (G) 1. To visit rural health projects during vacation so as to get a first hand experience of rural life, its problems and their solutions. 2. To develop medical services in rural areas and devote at least one year to develop a new pattern of medical care suitable to rural India. Some of the members of MFC are already working on rural projects in different part of the country. More are needed for similar action

Specific programmes for groups Having developed a group of medicos, interested in similar programmes problems, and want to do something, following programmes can be taken to increase and strengthen the group as well as to reach our goal. Develop a study circle, where friends meet and discuss problems. Doing collective social work like survey of a village community some medical or social problem, relief works inoculation health education etc.

To select and adapt a village or a community and try to study their health problems, their origin, the extent and nature, and if possible help them in solving them. This will live an opportunity to understand the health problems in society ‘in vivo’ - as they exist with its socio-economic context and will also help in developing social relationship. To have dialogues with general practitioners and develop groups to discuss problems as well as to provide medical assistance. Try to expose, whenever and wherever possible, the faults of the present medical education system and make an attempt to change it. Arrange week-end one-day camps for these activities.

Report-F Report on projects description th

Half of the morning session on the 8 Dec. ’75

was given to short description of some ten rural projects already undertaken or to be undertaken in near future. As it is not possible to do justice to the projects by describing them here, only those aspects which have relevance to MFC policies or have educative value for the members have been highlighted. The project described by Mira Sadgopal is yet to be born and that described by Lalit Khanra is still in its infancy. As yet they are not in a position to contribute much in the way of experience or experimental finding. The project at Thaltej, Ahmedabad (described by Kartik Nanavati) at Calcutta (described by Ashish Kundiur) and at Sevagram (described by Vidyadhar Ranade) are good examples of what determined medical students can do inspite of their heavy curriculum and burden of examinations. The Community Health Care Centre at Thaltej is the best organised and the most consistent among the three and it goes to their credit that they have been able to involve nurses and some teachers. The SSA project at Calcutta though not all that consolidated is an example of grim determination of the students in the face of heavy odds and total inexperience. It goes to their credit that they have been able to overcome artificial professional barriers by involving the veterinary students. Of the other five "projects described four were in tribal areas and four of them also have been started in co-ordination with previous developmental work going on there. The Arunachal project (described by Ashvin Patel), the Kutch project (described by Chandra Joshi), the Dharampur project (described by Navneet Fozdar) and the Govindpur project (described by Abhay Bang) are all in tribal areas and an seem to bring about any attempt to bring about change in their ways or life are perceived by these people as cultural aggression’. It is lesson for us all. Before we intend to begin any work in any area we should make ourselves as familiar as possible about the culture of the people there and learn to respect it; otherwise it is likely to increase resistance to change as has been the experience of Navneet Fozdar because of previous improper approach by the Family Planning workers. This also shows the necessity of adapting health care methods to suit cultural base line so as to achieve easy acceptability. The health project at Tilonia (described by S. P. Mathur) and all the above mentioned projects except that of Kutch have been begun as a component of integrated developmental work. All these projects including that of Kutch have found it necessary to train people from the locality

itself to build up a cheap and effective health delivery system and all of them have successfully trained health auxiliaries and paramedical workers out of people who would not otherwise ever be considered for the meanest health care job. This is a pointer to the direction in which we should move to build up an appropriate health delivery system. It also seems to be the common experience of all these people that preventive and family planning aspects of health work are by themselves not attractive to the people. They need some sort of sugar coating of curative services or of ‘food bank’ or something of the sort. A purely preventive approach does not seem feasible at the moment. Apart from all these the presence of so many dedicated workers at the meet was a heartening sign and probably would serve as an encouragement to those who intend to take up such work in future but cannot summon courage enough to begin for the fear of being isolated and left out.

— Lalit Kumar Khanra The Meet (Continued) The definition, objectives, organisation and programmes of MFC were discussed and finalised. (Please see Report E) Spliced between the regular working sessions, there were three other sessions which everyone could attend together. The first consisted of the presentation of individual field projects in which certain members were locally involved. Brief summary descriptions were followed answering a few basic questions posed by the others. (See Report F) 11.

The second extra session was an animated exposition of ayurvedic science by Dr. Tm M. Gogate who practices ayurveda, yoga and Unani systems in Amaravati. Discussion arising from his talk was not as useful as it should have been for it quickly degenerated into unscientific defence of the traditional as against the ‘allopathic’ system, and perhaps even strengthened the prejudices in the minds of some members. The third extra session was an informal postlunch playing of a tape-recorded street corner lecture by an unqualified ‘sexologist’. Abhay Bang and Ashok Bhargava has come across this individual in Varanasi

and had been impressed by his unexpectedly scientific down-to-earth approach to public sex education. An unexpected after-dinner surprise was our evening discovery of a number of fine classical and semi classical vocalists amongst us medicos. Their voices eerily swelled and tremulated with exquisite rhythmic precision, hypnotically luring our souls for away and beyond at the daylight cares of the present irrelevant health services. Some, however, were more transported by the frolics and mimicry of the more light-hearted, especially the spoof of a politician’s speech in elementary English (“ABCDE FGH IJKLMNOP, QRST UVWXYZAB CDEFG HIJK, LMNO! PQRSTU VWX YZAB? CDEFG….”). A friend from east Africa fell entrancingly into a soulful exposition of creative dance. The youngest participant brushed the hearts and consciences of many while reciting her sensitively conceived ‘story’. A few, though urban and western in daytime attire, approached a strange but convincing intimacy with their rural forbearers through folk song. Some old Hindi favourites loosened the intervals between several extraordinarily beautiful songs in Marathi, Bengali, Gujarati and Urdu.

One had the impression that although in the daytime we had been wrestling with problems complicated by our often yetunquestioned acceptance of western criteria for health care, “still the heart is Indian”. Perhaps this felling in some of us strengthened the conviction that, as Martin Luther King had told his people, “We shall overcome some day”. — Mira Sadgopal

NOTE— All Correspondence regarding the bulletin may please be held with Ashvin J. Patel, 21, Nirman Society, Vadodara390005 and regarding Medico Friend Circle and its activities with Convener, Medico Friend Circle, Rajghat, Varanasi-221001.

Editorial Committee: Imrana Qadeer, Prakash Bombatkar, Satish Tibrewala, Kamala Jayarao, Mira Sadgopal, Abhay Bang, George Isaac, Bhoomi Kumar Jaganathan, Suhas Jaju, Lalit Khanra, Ashvin Patel (Editor). Edited and Published by – Ashvin J. Patel for Medico Friend Circle from 21 Nirman Society, Vadodara390005 and Printed by him at Yagna Mudrika, Vadadora-390001.


