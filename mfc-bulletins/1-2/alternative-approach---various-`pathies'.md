---
title: Report C - Report of the discussion on the paper Alternative Approach : Various `Pathies'
description: 
published: true
date: 2023-08-11T12:45:33.291Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T04:14:29.373Z
---

*This is a web-friendly version of the article titled Report of the discussion on the paper Alternative Approach : Various `Pathies' from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC001-002.pdf](http://www.mfcindia.org/mfcpdfs/MFC001-002.pdf)*

Title: Report-C Report of the discussion on the paper Alternative Approach : Various `Pathies'

Authors: Khanna Lalit

1-2 medico friend circle bulletin 
Jan-Feb 1976

# Report of the discussion on the paper Alternative Approach: Various 'Pathies'


## **Proceedings of the Second all-India meet of Medico Friend Circle held at Sevagram on 27th 28th & 29th December 1975**

**Report—A**
**The Meet**
New and old members of Medico Friend Circle gathered at Sevagram (Wardha, Maharashtra) for the second annual allIndia meet which began on December 27. Those of as who were regular participants at the three-day meet numbered about 70, gathered from many far-flung regions: Delhi, Ajmer, Kutch, Baroda, Ahmedabad, Nagpur, Wardha, Bombay, Indore, Hoshangabad, Varanasi, Calcutta, Midnapore, Hyderabad, Madras and Cochin. Participant included practicing doctors, medical research scientist, post graduate student, medical college lecturers, medical students, research scholar, social workers, agriculturalist health worker, school teacher and classical singer. Among us, some had settled down to work on specific field projects involving rural health care development. Sevagram Medical College provided us with comfortable and congenial accommodation adjacent to the Ashram where Mahatma Gandhi lived during his last years. Smt. Sushila Nayar, the founder-director of the M. G. Institute of Medical Sciences of Sevagram, participated in the few of the discussion sessions. The initial group session was taken up by the participants turn by turn acquainting each-other with their present occupations, backgrounds, reasons for coming to the meet, and special interests, painted a colourful collage of personalities and aspirations

For discussion of the working papers we divided ourselves into tow equal groups. The paper prepared by Ashvin Patel (Vadodara) directed us towards a definition of health, its problems and needs, and was taken up first. The second working paper, prepared by Lalit Khanra (Midnapore, West Bengal) concerned with an elaboration of the alternative systems of medicine that are practiced today in our country: ‘•western’ (erroneously called ‘allopathy’) homeopathy, biochemic, ayurveda, yoga, naturopathy, Unani, astrology, acupuncture, folk medicine and magic. Third was a paper which Mira Sadgopal (Hoshangabad, Madhya Pradesh) had prepared to examine present and historical failures and successes in the continuing pursuit of practical health care alternatives for our country’s people. The fourth working paper, drawn up by Ashok Bhargava (Varanasi), placed the organisational structure and administrative and financial needs of MFC squarely before us to enable us to make some important practical decisions. On the basis of this guide and probably strengthened by insights gathered during the previous two days, we were able to make the following decisions:
1. It was agreed upon that MFC should continue as a loose, extra-constitutional, unregistered organisation for the coming year at the end of which the position may be reviewed with membership being open to all persons involved in health and health related activities. 
2. Ashok Bhargav was asked to continue his role as convener for an additional one year. 
3. Dues for members were reconfirmed at Rs. 20/- per year, payable in one or two installments to the Convener, Medico Friend Circle, Rajghat, Varanasi-221001 (This amount is roughly equivalent to the cost of production and mailing of twelve monthly bulletins and circulars to each member in a year.) Therefore, those who can afford it should voluntarily pay more, while in certain justifiable individual cases the dues could be partially waived, at the discretion of the Convener.
4. Membership (principally meaning right to receipt of bulletins) would be cancelled (by removal from the mailing list) incase of delay of paying dues without written communication for over three months.
5.1. Last year income and expenditure were reviewed, and the budget for the year 1976 was agreed to as follows:

Expenditure: 1. Convener’s salary 2. Office maintenance 3. Bulletin

1915, Rs. 3000-00

Rs. 1000-00 Rs. 1000-00

4. Transport Rs. 1000-00 5. Camp and annual meet Rs. 3300-00 6. Contingencies —

1976 (expected) Rs. 36OO-00

1000-00 Rs. 4000-00 Rs. 1400-00 Rs. 4000-00

Rs. 2000-00 Rs. 16000-00

Total

Rs. 9300-00

INCOME:

1975

1976 (expected)

Rs. 300-00

Rs. 2000-00 Rs. —

1. Membership fee 2.Shanti Sena Mandal

Rs. 5500-00

3.Grant from Voluntary

—

Action Cell 4. Funds raised by regional groups and individuals

Rs. 500-00

Total

Rs. 9300-00 Rs. 16000-00

Rs. 3000-00 Rs. 1400-00

5.2 Though it was agreed that MFC should have a full time paid convener with a pay of Rs. 300/- per month, Ashok conveyed later that he would not draw the salary.
5.3 The responsibility to collect Rs. 14, 000/- (no.4 of Income for year 1976), it is agreed, lies solely on the groups i.e. each group will have to collect at least Rs. 1500/- this year. 
6.1 It was pointed out that the MFC Bulletins were ill planned and with proper planning the Bulletin might have put to much better use. Hence an editorial committee was selected from those assembled. These persons major function is to select priority fields of study from among the vast number of possibilities suggested and to stimulate logical and logical and progressive exchange of experiences and research in these fields through the monthly bulletin. Persons were included in the committee mainly on the basis of willingness to work and their relatively deeper involvement.
6.2 The editorial committee consists of following members: 
1. Imrana Qadeer (Delhi)
2. Prakash Bombatkar (Wardha)
3. Satish Tibrewala (Bombay)
4. Kamal Jay Rao (Hyderabad)
5. Mira Sadgopal (Hoshangabad)
6. Abhay Baal (Nagpur)
7. George Isaac(Cochin)
8. Sathi Devi (Varanasi)
9. Bhoomikumar J.(Madras)
10. Suhas Jaju (Nagpur)
11. Lalit Khanra (Midnapore)
12. Ashvin, J. Patel (Vadodara), Editor. 
(Rajiv Raj Choudhry in particular and Vadodara group in general will be responsible for the production and circulation of the bulletin.)
6.3 The subjects chosen by the editorial committee for discussion during the year 1976 are as follows:
A. Health Structure Analysis
i) History of health service in India
ii) Critical assessment of the present health services
iii) Critical analysis of Pharmaceutical Industry

B. Present Health Problem-
i) Nutrition—
a) PCM
b) Deficiency anaemia 
c) Vit A deficiency 

ii) Communicable diseases
a) Malaria
b) Tuberculosis 

iii) Population problem

C. Alternative Approach
i) Health team approach 
ii) Auxiliaries 
iii) Comprehensive Development Approach

6.4 It was decided that MFC bulletin should become a printed magazine instead of cyclostyled one in order to accommodate more material. Articles may be written either in Hindi or English. 
6.5 It was also decided to continue the practice of sending, three issues of the bulletin free to anyone on the recommendation of a member. Thereafter, if one wishes to continue to receive the bulletin, either one has to pay Rs. 10/- as yearly subscription or baa to become member of MFC.
7. It was decided to have atleast one regional work camp (summer) and one annual meet (winter) organised by the Convener and attended by members from the whole country.
8. Local geographical groups of medico friends will meet from time-to-time for critical study and discussion, drawing upon their own day-to-day local experiences as well as from material distributed through the monthly bulletins. Form time-to-time, they should try to, distill whatever insights are gained through these explorations and offer them to the wider circle, through the bulletins or through extra communications.

It was possible for several groups to form from the following regions: Delhi, Bombay, Calcutta, Nagpur, Ahmedabad, Vadodara, Indore, Sevagram, Wardha, Calicut and Varanasi. Other groups will naturally form as our circle grows.

9. An Advisory Committee was formed by the Convener to help him in implementing the programmes of MFC consisting of the following members. 1. Chanchala Samajdar (Calcutta)
2. Vidyut Katgade (Varanasi)
3. Nandkishor Joshi (Indore)
4. Rajivraj Choudhry
5. Kartik Nanavati (Ahmedabad) 
6. Satish Tibrewala (Bombay)
7. Kanchanmala, N. P. (Calicut)
8. Suhas Jaju (Nagpur)
9. Bhakti Dastane (Sevagram)
10. Monghiben Mayatra (Kutch)

10.It was generally agreed that there need not be an intensive membership drive, but that rather we should seek to consolidate our group, to deepen our understanding and commitments, and to accept new members who join on their own initiative through personal contacts rather than through recruitment. (Turn to page – 12)


## Report — B 
**Report of the Discussion on the Paper**

**Our Present-day Health Problems and Needs**
This paper was prepared to define the concept of health and to decide our present day priorities about health problems and need. The paper consisted of three parts-I. Definition of health, 2. Present day health problems of India and, 3. Present day health Deeds of India. The house reached on the following conclusions: 

**1. Definition of health**
Though the definition of health given by WHO (Health is a state of complete physical, metal and social wellbeing and not merely an absence of disease or infirmity.) was accepted, it was found deficient. It was emphasised that health should be viewed also as an autonomous ability to cope with pain sickness and health. 

**2. Present day health problems of India**
Limited resources and multiplicity of problems compel us to draw priorities. In doing so though the individual should not be neglected the criteria for deciding priorities should be in the benefit of the community as a whole. The following problems were identified unanimously for immediate action: 
2.1 Communicable diseases, nutritional problems and defective environmental sanitation need priorities to degenerative diseases, inborn errors of metabolism etc. 
2.2 Ignorance of the education and that of the uneducated people are both important problems, but that of the educated elite is more deplorable. 
2.3 Wrong allocation of funds to the powerful vasted interests was thought to be more responsible than sheer lack of economic resources. 2.4 Existing maldistribution of medical personnel (as) between urban and rural population) and shortage of basic health workers were expressed as major problems.
2.5 Irrelevance of the present medical education, research and health services were discussed again (as it was discussed in Ujjain Meet, December ‘74). Aspiration and identification with foreign affluent health system poor quality of medical teaching, migration of doctors to foreign countries, priority to training doctors rather than auxiliaries, selection system for medical personnel, expensive hospital based health services, non-involvement of traditional practioners and community in health services were identified as pitfalls in our present health system.
2.6 Over reliance on drugs by doctors, high cost and poor availability of drugs profit oriented drug industries, adulteration and black marketing of drugs were condemned. At the same time, use of naturally and easily available medicines was pointed out as an ignored area. Good quality work already done was not available for ready use. 
2.7 Health problems can neither be viewed nor solved in isolation from socio-economic, political and other facet or community problems. 
2.8 (Question was raised whether today's medical care is really beneficial. This question was viewed keeping in mind the: following proposition: “A professional and physician based health care system which has grown beyond tolerable bounds is sickening for three reasons: It must produce clinical damages which out weight its potential benefits; it cannot but obscure the political conditions which render society unhealthy; and it tend to expropriate the power of the individual to heal himself and to shape his or her environment ...Neither decline in any of the major epidemics of killing diseases, nor major changes in the age structure of the population, nor falling and rising absenteeism at the workbench have been significantly related to sick care not even immunisation…But the benefits claimed owe much more to the advancement in other areas like communication, agriculture etc. ") It was agreed that advantages cannot be generalised a regards today's medical cafe, as there were some advantages which could not be isolated from advances in other areas. 
2.9 (Here question was posed whether today's, medical care encroaches upon personal freedom. The proposition was "The extention or professional control over the care of healthy people e.g. antenatal clinic, well baby clinic etc. and medicalisation of prevention and early diagnosis turns the healthy people into lifelong patient. This is considered as the encroachment on personal freedom and ability to react to an experienced reality e.g. pain, sickness, death and various stages of life span like childhood, adolescence and old age,”)
	It was general agreed that mutable medica1 care system (as it is today) was viewed as not encroaching upon one's freedom but it was held that our system should be such that personal freedom is not jeopardised. 
  2.10 The problem of population control and that of high maternal and infant morbidity and morbidity were accepted to be interlinked. Anyhow, house was divided in fixing the priority between these two problems.

3. **Present day health needs of India** 
It was realised that present health system is capital intensive, curative and elite-based one but we Deed labour intensive, integrated and mass-based one. In the light of conclusion the following needs were identified:
3.1 Integrated (curative preventive and, promotive) approach was favoured.
3.2 Training of basic doctors and multipurpose health workers was emphasised instead of specialists and multiple workers. 
3.3 Local resources and manpower could be used to fulfill the deficit of full-fledged medical services. 
3.4 It was suggested that selection of health worker and medical student should be done by the local community. But it was felt that unless equal educational facilities are made available to all, simply a change in selection policy in medical college is not going to be of much use. Need oriented cubiculum will help in solving the problems of selection of medical students 
3.5 The following changes in medical education were suggested Medical students should be involved is community development projects throughout their training i. e. dehospitalisation of medical education. Co-education with paramedical personnel was though to be important and possible. Dissertation and thesis in community problems should be encouraged, training in medical administration should also be incorporated in medical education. 
3.6 The redistribution of health personnel and should be such that it would facilitate ‘chain of referal system’ rather than multiplying specialised institutes at the cost of peripheral health service.
3.7 The need for improvement in environmental sanitation was stressed. 3.8 Availability of inexpensive, easily available and effective drugs was emphasised. 
3.9 It was realised that integrated economic development conducive to family planning and not mere emphasis on mass sterilisation or other aids to family planning would solve the problem of population explosion. 
3.10 Need to educate everybody including administrators, planners, professional, politicians etc. about our real priorities was pointed out. 
3.11 Need for dehospitalisation and deprofessionalisation of health services was repeatedly emphasised during discussion.
3.12 To implement the identified needs, we need to create an effective demand. Because Health system can not be changed in isolation, we need to touch the wider social problem and share the emergent responsibility.
**—Kamala Jaya Rao, Kartik Nanavati, Vidyut Katgade.**


## **Report—D**
 
Report Of The Discussion On The Paper

### Health for the People: 
### Finding A Practical Way

The paper consisted of five major parts. First part was an enquiry into present and historical failures and successes to assist the participants in deciding the direction of their future commitments. 
Second part dealt with inter-relationship among the doctors, paramedical workers and community. 
Third part dealt with policy for train in paramedical worker and doctors.
Fourth part challenged the participants to draw a vivid picture of future society in which their practical health system would function. 
Fifth part was a search for any outstanding health service out side of India having strong relevance to India’s particular needs. 
It was felt that there were little sensory inputs to discuss and analyse the first part of the paper. The second and third parts were already touched by other papers. Fourth and fifth part could not be discussed due to lack of time. 
First of all, the direction set by the Ujjain Meet of MFC (December 1974) for developing, alternative approach for developing countries was recollected. Then following points were highlighted during the discussion. 
1. Present system of health services bas failed to provide easily accessible, cheap and effective services to everyman in this country. Neither the basic medical science could be provided nor could positive health be achieved. 
2. Health services which are planned according to our economic, social and cultural circumstances and which meet the needs of our society are ideal for our country. 
3. It was emphasised that the role of a doctor is not merely if an expert but also that of a social worker, as health problems can’t be changed in isolation from socio-economic and political context. 
4. In the absence or massive social change, a system depending on the personality of an outstanding and charismatic leader, is not likely to be meaningful and viable. So one must conceive health approach as a part of total comprehensive development process instead of in isolation. 
5. Training of doctors and paramedical workers should ensure co-ordination between each-other and with community. And following suggestions were recommended for the same:
	5.1 Joint education of doctors and paramedical workers together for relevant subjects. 
  5.2 Interaction with community during training period. 
  5.3 Selection of trainee and student candidates by the community and from the community.
  5.4 Interaction of useful western knowledge with traditional practice and cultural knowledge. 
  5.5 Distribution of doctors and paramedical staff’s time should be conducive to fulfil decided priorities avoiding the duplication of efforts.

—Ashvin Patel

**Report-C**
**Report of the discussion on the paper** 
### Alternative Approach Various ‘Pathies’. 
The discussion began with the gist of a Marathi article on the merits of Ayurveda being read out. Thereafter the discussion focussed on the original working paper. The question was raised whether the integration of different ‘pathies’ meant maintaining an open attitude towards one another or it meant a curriculum of undergraduate teaching where the basic principles of all the pathies should be taught to all students. After some debate it was generally reed that teaching of all the ‘pathies’ in one curriculum will cause a lot of confusion of the different approaches of different ‘pathies’. So long as there it no system which can take into account the different aspects of all the • pathies ' it will be wise on our part to keep our minds open to the local utility aspect of other ‘pathies’ than our own and avoid futile debate about philosophical and pharmacological intricacies. In this context it was pointed out by some members that the working paper did not sufficiently highlight the social utility aspect of different • pathies'. It wall also suggested that members should try individually (or in group) to learn as much as they can about "pathies” other than their own. The group then 'listened to a short talk on the merits of Ayurveda from one of the members' present. The elaborate preventive aspects of Ayurveda come to many of us as an eye opener. The group looked for similar enlightenment about other • ‘pathies’; but this wish remained unfulfilled as the other "pathies were not represented sufficiently. It was therefore felt that MFC members should enlist co-operation of the members and practitioners of other ‘pathies’ by explaining to them objectives of MFC and to collect relevant information in order to eliminate existing ambiguities and effect integration through critical and cultural acceptance of health care facilities by the people:

-Lalit Khanra

### Report-E

**Medico Friend Circle**

**Objectives Organization and Programmes Objectives**

**Objectives**

Medico Friend Circle is a group of people involved in health and health related activities, dissatisfied with the present system of health services in India and conscious about the' problems and their responsibilities in relation to society. It works with the following aims in view:
(a) To evolve a pattern of medical education and methodology of health care relevant to Indian needs and conditions; and,

(b) To make positive efforts towards improving the non-medical aspects of society for a better life, more humane and just in contents and purposes.

**Organisation**

MFC will not be constituted as a rigid organisation 'of medicos. It will remain a loosely-knit group, dissimilar though in ramifications but thinking and working for similar goals and as a homogeneous unit. Any person involved in health and health related activities can become its member. The members are expected to form groups and circles where possible.

**Function**

Functioning of MFC will be mainly through its members and groups at various places. Central co/ ordination will be done by the Central Cell which among others will perform the following functions:
I. Co-ordination, through a regular monthly bulletin which will contain; (a) Thought provoking articles encouraging questioning and discussion providing information about various issues in which MFC may be interested. 
(b) Reports of activities by members or groups. 
(c) Extracts of useful articles studied by members.
(d) Supply in a names of books and articles or any other material containing useful information. 

2. Maintaining a ‘fact bank' which will store information data, facts and list of problems which may interest the members. 

Providing material for the bulletin and ‘pact bank' is the responsibility of the members who are expected to be active in writing and sending them to the persons concerned. 

3. Maintaining of data of all members and persons with a of helping and encouraging contacts and communication.


4. To arrange medico camps, conferences, seminars specially on rural health projects and convening the MFC’s annual meet. 

5. Finance the expenditure involved in all these central activities will, it is expected, be shared and horne by the members. The membership fee has been fixed Rs. 20/- per annum to be paid in not more than two installments. The money should be sent to Central Office. < Medico Friend Circle, Rajghat, Varanasi-221001. (It is understood that capable members should pay more than this minimum limit and that the Convener can waive or reduce this fee in deserving cases.)

**Programmes for individuals and groups
**

(A) 1. Each member should select a problem 01 topic, study it thoroughly and then circulate the knowledge to others. The study should not be a mere academic gymnastic. It should be: something concerning health and health related activities keeping in view the social needs and perspectives e.g., poverty, mal-nutrition, failure of malaria eradication programme etc. To collect data or try to study any problem which can become a topic for research. Small practical problems and simple observations should be the choice. 
(B) 1. To study other-'pathies' to learn their useful parts; and seek and enlist the cooperation of their adherents. 
(C) 1. To emphasise more on preventive and total medicine during their educational period. 
Try to curtail the unnecessary use of drug, and use minimum amount of drugs. 
Emphasise more on health education, prevention of diseases during practice. 
To seek the cooperation of the N.S.S. and P.S.M. departments for these activities. 
(D) 1. Study sociology, economics, political science and similar social sciences, because a doctor is not merely a physician of individual patients but also a social being and so he should understand society, its working and its problems. 
To discuss various socio-medical problems with other friends, try to create an awareness among them, and try to develop a group of medicos with similar interests at their p1acle. 
(E) 1. To learn clinical medicine perfectly, relying less on costly investigations.
Try to learn nursing procedures and basic investigations yourself.
(F) 1. Not to accept ‘physician’s samples’ from the medical representatives as it is a subtle corruption. 
Symbolic acts to change the social values e.g. doing productive labour etc, and to give up cultural slavery. 
To oppose ragging in Medical Colleges at individual and group levels. 
Try to improve relations among the different categories of health workers. 
Try to learn more about ‘health team’. 
Help blood donation activity. 
Opposing strikes by health personnel for selfish purposes. (This was considered to be a controversial point and any decision is postponed till further discussion.) 
(G) 
1. To visit rural health projects during vacation so as to get a first hand experience of rural life, its problems and their solutions. 
2. To develop medical services in rural areas and devote at least one year to develop a new pattern of medical care suitable to rural India. Some of the members of MFC are already working on rural projects in different part of the country. More are needed for similar action

**Specific programmes for groups**
Having developed a group of medicos, interested in similar programmes problems, and want to do something, following programmes can be taken to increase and strengthen the group as well as to reach our goal. 
Develop a study circle, where friends meet and discuss problems. 
Doing collective social work like survey of a village community some medical or social problem, relief works inoculation health education etc.
To select and adapt a village or a community and try to study their health problems, their origin, the extent and nature, and if possible help them in solving them. This will live an opportunity to understand the health problems in society ‘in vivo’ - as they exist with its socio-economic context and will also help in developing social relationship. 
To have dialogues with general practitioners and develop groups to discuss problems as well as to provide medical assistance. 
Try to expose, whenever and wherever possible, the faults of the present medical education system and make an attempt to change it. 
Arrange week-end one-day camps for these activities.

### Report-F 
**Report on projects description**

Half of the morning session on the 8 Dec. ’75 was given to short description of some ten rural projects already undertaken or to be undertaken in near future. As it is not possible to do justice to the projects by describing them here, only those aspects which have relevance to MFC policies or have educative value for the members have been highlighted. The project described by Mira Sadgopal is yet to be born and that described by Lalit Khanra is still in its infancy. As yet they are not in a position to contribute much in the way of experience or experimental finding. The project at Thaltej, Ahmedabad (described by Kartik Nanavati) at Calcutta (described by Ashish Kundiur) and at Sevagram (described by Vidyadhar Ranade) are good examples of what determined medical students can do inspite of their heavy curriculum and burden of examinations. The Community Health Care Centre at Thaltej is the best organised and the most consistent among the three and it goes to their credit that they have been able to involve nurses and some teachers. The SSA project at Calcutta though not all that consolidated is an example of grim determination of the students in the face of heavy odds and total inexperience. It goes to their credit that they have been able to overcome artificial professional barriers by involving the veterinary students. Of the other five "projects described four were in tribal areas and four of them also have been started in co-ordination with previous developmental work going on there. The Arunachal project (described by Ashvin Patel), the Kutch project (described by Chandra Joshi), the Dharampur project (described by Navneet Fozdar) and the Govindpur project (described by Abhay Bang) are all in tribal areas and an seem to bring about any attempt to bring about change in their ways or life are perceived by these people as cultural aggression’. It is lesson for us all. Before we intend to begin any work in any area we should make ourselves as familiar as possible about the culture of the people there and learn to respect it; otherwise it is likely to increase resistance to change as has been the experience of Navneet Fozdar because of previous improper approach by the Family Planning workers. This also shows the necessity of adapting health care methods to suit cultural base line so as to achieve easy acceptability. The health project at Tilonia (described by S. P. Mathur) and all the above mentioned projects except that of Kutch have been begun as a component of integrated developmental work. All these projects including that of Kutch have found it necessary to train people from the locality itself to build up a cheap and effective health delivery system and all of them have successfully trained health auxiliaries and paramedical workers out of people who would not otherwise ever be considered for the meanest health care job. This is a pointer to the direction in which we should move to build up an appropriate health delivery system. It also seems to be the common experience of all these people that preventive and family planning aspects of health work are by themselves not attractive to the people. They need some sort of sugar coating of curative services or of ‘food bank’ or something of the sort. A purely preventive approach does not seem feasible at the moment. 

Apart from all these the presence of so many dedicated workers at the meet was a heartening sign and probably would serve as an encouragement to those who intend to take up such work in future but cannot summon courage enough to begin for the fear of being isolated and left out.

— Lalit Kumar Khanra 

### **The Meet (Continued)**

11. The definition, objectives, organisation and programmes of MFC were discussed and finalised. (Please see Report E) 

Spliced between the regular working sessions, there were three other sessions which everyone could attend together. The first consisted of the presentation of individual field projects in which certain members were locally involved. Brief summary descriptions were followed answering a few basic questions posed by the others. (See Report F)

The second extra session was an animated exposition of ayurvedic science by Dr. Tm M. Gogate who practices ayurveda, yoga and Unani systems in Amaravati. Discussion arising from his talk was not as useful as it should have been for it quickly degenerated into unscientific defence of the traditional as against the ‘allopathic’ system, and perhaps even strengthened the prejudices in the minds of some members. 

The third extra session was an informal postlunch playing of a tape-recorded street corner lecture by an unqualified ‘sexologist’. Abhay Bang and Ashok Bhargava has come across this individual in Varanasi and had been impressed by his unexpectedly scientific down-to-earth approach to public sex education. 

An unexpected after-dinner surprise was our evening discovery of a number of fine classical and semi classical vocalists amongst us medicos. Their voices eerily swelled and tremulated with exquisite rhythmic precision, hypnotically luring our souls for away and beyond at the daylight cares of the present irrelevant health services. Some, however, were more transported by the frolics and mimicry of the more light-hearted, especially the spoof of a politician’s speech in elementary English (“ABCDE FGH IJKLMNOP, QRST UVWXYZAB CDEFG HIJK, LMNO! PQRSTU VWX YZAB? CDEFG….”). A friend from east Africa fell entrancingly into a soulful exposition of creative dance. The youngest participant brushed the hearts and consciences of many while reciting her sensitively conceived ‘story’. A few, though urban and western in daytime attire, approached a strange but convincing intimacy with their rural forbearers through folk song. Some old Hindi favourites loosened the intervals between several extraordinarily beautiful songs in Marathi, Bengali, Gujarati and Urdu.

One had the impression that although in the daytime we had been wrestling with problems complicated by our often yetunquestioned acceptance of western criteria for health care, “still the heart is Indian”. Perhaps this felling in some of us strengthened the conviction that, as Martin Luther King had told his people, “We shall overcome some day”. 

**— Mira Sadgopal**

