---
title: Dilemmas of Young Health Professionals
description: 
published: true
date: 2023-08-11T08:05:03.519Z
tags: 
editor: markdown
dateCreated: 2023-08-11T08:05:03.519Z
---

This is a web-friendly version of the article titled Dilemmas of Young Health Professionals from medico friend circle bulletin. You can find the original in the PDF at http://www.mfcindia.org/mfcpdfs/MFC382.pdf

# Dilemmas of Young Health Professionals
Background Paper for MFC Annual Meet 2020, Sewagram

- Shrinidihi, Mohammed Khader Meeran, Savithri ^1^

^1^ Email: datarshrinidhi@gmail.com, sairam1179@gmail.com, lightoftrichy@yahoo.com

## Why?
The last few years of MFC Meet have witnessed arising trend of attendance of younger members. We saw a lot of inquisitive faces and minds eager to know people who have devoted their lives to working  for those in need. Some of the youth were already working to create an impact while many others were still completing their college life. We also were a partof it, and from what we could see, we were welcomed by the MFC members. So much so that, we neverfelt we were ever away from it! It was a pleasure to see a group, members of which are doing incredible work but still grounded to their roots. It will not be wrong to say that somewhere this very background has culminated in the selection of the current theme of MFC. In the Annual meet that took place last year at Sewagram, young members spent considerable time talking with the senior members, asking them questions about their own lives and also seeking answers to their personal and professional questions. It was then that the AGM came up with the idea that these dilemmas per se be a topic for the next Annual Meet.

## Dilemmas of Young Health Professionals

The situation that the youth are currently in is not promising. ‘Healthcare’ has become a market-driven commodity over the years. The impact of the healthcare industrial complex could be made out by a statement made by Dr Devi Shetty (founderof Narayana Health) that the global healthcare and wellness industry is going to drive the world’s economy of the 21 st century and India’s healthcare industry will grow phenomenally. This statement itself is an indicator of how healthcare is viewed as a medium of growth of an ‘industry’ by even doctors! If we are to look at medical education alone, by design or by accident or by plan, private medical colleges in India are mushrooming. The number of private medical colleges in the country has exceeded the total number of government medical colleges last year (245 government medical colleges and 254 private medical colleges). More than 100 private medical colleges were started within the last decade. Among 313 dental colleges, only a meagre 49 colleges are run by the government. This means, every year more than half of the medical professionals are coming to the field of practice from the profit-driven private medical education sector.

In healthcare delivery, the private sector provides 80% of outpatient services and 60% of inpatient services. Over the years, the relationship between a doctor and patient has transformed into a ‘consumer-good approach' where s/he sees the patient as a subject through which s/he can make profits. As a result, private healthcare is expanding over the years. A market strategy is devised by the corporates in which medical practitioners are made to feel insecure about their employment and quality of life. By making them prescribe unnecessary medications and procedures, they make the young doctor earn a little bit more and in turn help make a greater profit for the corporation. Regarding updates and recent advancements in medical practice, practitioners get information only through CMEs. CMEs are yet another profit-making operation organised by private pharma companies to train the doctors to write their products by labelling them as superior to existing medicines. Every health professional – not just doctors – face similar dilemmas.

## The State’s Encouragement of the Private Sector
If one believes that health is a fundamental right and the state is accountable to provide its citizens with equitable, affordable and quality healthcare, the recent proposals by NITI Aayog are alarming. Based on models operational in Karnataka and Gujarat and so-called international best practices, a ‘concession agreement’ has been made to link private medical colleges and district hospitals by PPP. This is supposed to address the problems of both medical education as well shortage of doctors simultaneously! It raises the doubt as to whether NITI Aayog is still an independent body or is it hijacking the work of the Department of Disinvestment and Public Asset Management! On the one hand, the increasing drive towards privatisation poses multiple dilemmas in front of doctors about the public healthcare sector.

On the other hand, the changing political environment along with arbitrary undemocratic decisions such as withdrawal of special status to Jammu and Kashmir and the effects on the health status of the population of Kashmir has made it impossible for the medical fraternity to remain silent. Numerous reports have emerged regarding the difficulties in access to healthcare facilities, availability of medications, mental health conditions of people under chronic stress, injuries during incited violence during protests and related health issues.

Another distressing trend that has come up is polarisation within the medical community and this is becoming the basis for which doctor treats who! This is particularly evident in the treatment of protestors as reported by a few young MFC members who visited the protestors in Delhi.

It is noteworthy that even amidst such evident crises due to the changing political scenario, voluntary medical associations, bar a few, have remained largely silent on these issues.

## Organizational Context within which Young Professionals find themselves
In this setting, it is but obvious that a health professional who wants to do what his/her profession was originally meant to do, that is work for the health of the people, faces multiple dilemmas. Over the years, values among health professionals have changed as a reflection of changing societal values. Gone are the days, when a doctor with altruistic tendencies was the norm. Today, patient-centric behaviour, as we now call them, are seen as desirable but not mandatory. Among other factors, students often experience a dearth of role models who can inspire them to go beyond one’s immediate circumstances and work for the larger good. In this context, MFC can play a major role in filling this felt gap. Hence, many senior MFC members agreed that dilemmas in front of such a rare group of students must be discussed, if MFC has to act as a support group for them. 

## Planning the Annual Meet
When we sat together to reflect on how the theme must be presented in the Mid-Annual Meet, we decided that the experiential sharing of individual dilemmas can a good approach. Even though dilemmas were largely personal, still with some effort we could club different dilemmas under subheads and discuss them thereafter. The mid-annual meet (MAM) paper was again an example that collective knowledge can be used to generate something more complete.

Discussions during the MAM indicated that these dilemmas have their origins in the current sociopolitico-economic scenario and there is a need to understand these dilemmas in this context.

The MAM began with a presentation of the background note. The questions raised by the different contributors of the paper along with the ones compiled after discussions in small groups were combined.

The questions put forth were pooled into 6 major categories:
1. Lack of social orientation in education 
2. Financial issues 
3. Personal dilemmas regarding partner and family 
4. Medical education 
5. Dilemmas about working in existing NGOs 
6. Ethical issues 

It was also agreed upon to look at each of these categories at 3 different levels: first, an understanding of the problem and its genesis in socio-political milieu; second, a sharing by the members about how they have tackled/dealt with the situation/problem. The experiences of the senior members can help us deal with the problem and find our solutions to it. And third, to explore ways how MFC as a group can contribute in some way to help and support the youth navigate the question. 

The organising of the mid-annual meet was challenging in that we had to find a way to look at the diverse dilemmas of a personal nature presented by various health professionals without it becoming just an analytical discussion on human resources in health. The senior members of MFC helped us group these dilemmas and identify certain heads under which to discuss. Also, we wanted the members present at the MAM to share their dilemmas. For this, an informal discussion was also necessary. This ensured that more intense sharing happened and more points were shared. However discussions with the larger group were also important, and this led to some discussions being repeated and was overall time-consuming. But sharing in small groups improved the familiarity between members and this was along the lines of improving interaction among newer and older members of MFC. Also after the formal discussions, the informal interactions and sharing in the evening hours were enriching.

Drawing from the above experience, the OC (Organising Committee) discussed how the annual meet can be conducted effectively. Two important directions emerged from these discussions – one related to how the papers need to be written and two, how the discussions need to be taken forward in the annual meet. 

Right at the beginning of these discussions, it had become apparent that a traditional approach of only writing papers and having objective discussions was not perhaps the best way to approach these dilemmas. Along with, experiential sharing of the members, informal discussions, understanding each other at a deeper level, also should be given more time to deal with the theme. Therefore the papers designed for the Annual Meet also tries to meet this objective of effectively discussing the topic and involving members in various fields of health. Also, it was apparent that the young people that had spelled out their dilemmas were hoping for a discussion regarding what could be done with regard to their dilemmas, even though it became clear that there were no readymade solutions. The evening sessions of informal discussions seemed to further clarify our dilemmas and it became apparent that such informal discussions need to be given more time and importance, at least in the context of this particular topic.

Keeping this in mind, the current Annual Meet has been designed to give more time for informal interaction between the young members and the more experienced members in the MFC. The papers designed for the Annual Meet also are a reflection of the needs of youth today. There has been an attempt to carry forward the discussion in MAM in the papers of the Annual Meet.

## Individual Papers/Sessions

There is a paper on developments in the AYUSH sector where we attempt to present to the youth practicing AYUSH examples of low-cost, effective and ethical AYUSH practices in different areas of the country. This we believe can be a morale booster for the students who seem to appear lost in the vicious cycle of poor quality of education in AYUSH colleges, lack of role models and opportunities in the social sector. Also, it can be a good idea to discuss examples where integrated care is given to promote health holistically. 

Another paper is on what are the support systems available to today’s young generation who are looking to work for the people in need but do not get proper orientation in the current educational system. A very popular youth initiative NIRMAN in Maharashtra is discussed. The NIRMAN initiative has been instrumental in motivating a huge number of young persons to work in the social sector in various capacities. Also discussed are the initiatives like the Rural Sensitization Program (RSP) held in Sittilingi. Support systems play an important role especially when it involves meeting role models in the field, and more importantly, its a place to meet several of likeminded young persons who like us are also in search of a place where they are listened to by seniors and others. 

It was a collective view of the members that the problems and challenges being posed were from a small proportion of health professionals in the NGO sector. It was pointed out that there were a large number of young professionals working in the public and private sector that ideally MFC should be talking about and supporting. Keeping this in mind we have a paper on account of Tamil Nadu Medical Officers’ Association (TNMOA) which has acted as a support group for Government doctors in the state. Also, an account of Alliance for Doctors In Ethical Healthcare (ADEH) is taken as an example which is acting as a group of private practitioners who intend to practice rationally and ethically. 

A very interesting paper is on people working in conflict zones such as Naxal areas, civil wars, terrorist infiltrated areas, etc. It could be a good study to see how these people have managed to sustain in these difficult zones. 

The medical fraternity was rocked by the suicide of junior resident, Dr. Payal Tadvi, in Mumbai last year. Caste, class issues and ragging were all alleged to have traumatised the victim. This attracted the attention of activists across India. MFC along with other groups conducted a fact-finding study into the whole incident. The report is also to be discussed in the Annual Meet as it appears that we need to
understand caste, class and ragging issues in colleges and that it can be a serious barrier to learning for the students.

MFC has never shied away from responding to the current problems that the country has faced. If we are to consider health as political, socio-economic and political changes in the environment always have a bearing on the health of the people. Two events have rocked the country and have shaken our understanding of what it is to be a citizen and health professional:

1. The health situation in Kashmir 
2. Citizens Amendment Act, NPR, and NRC and related protests 

It was felt that the discussions on the above events need a prominent feature in the Annual Meet of MFC. 

Also included in the Annual meet is a compilation of life journeys of senior members of MFC who have been in the field of public health in different capacities and have been a source of inspiration. We as OC believe in learning and understanding their journeys and their dilemmas and paths, they choose to take that can help us to understand and find solutions to our dilemmas as well. Also, this exercise will give the much- needed informal and open atmosphere for such a discussion. We would like to extend a heartfelt thanks to all those members who have sent their journeys in the form of a writeup.

Medico Friend Circle (MFC) has been in existence since 1975 as a forum for discussion, debate, and friendships beyond ideologies. The discussions are based on various experiences encountered by individuals working at the ground level. The members try to understand the issue to the core and its implications on the larger level. However, being a friends’ group, friendships have always blossomed in the Meets, largely evident from numerous personal sharing between the members. The members have had incredible journeys in the field of public health as well as other aspects of improving human existence in spite of existing social injustices and inequalities. In this regard, MFC as a collective can be seen as a trove of wisdom and experience and a learning centre for young restless individuals who want to start their own story of contributing to the welfare of society. Therefore, it wouldn’t be all that unseemly to urge MFC to collectively take on a mentorship role for the younger crowd that is increasingly taking an interest. There is a paper outlining some suggestions in this direction and we hope a healthy discussion followed by concrete steps result. 

We believe the discussions this year MFC will be richer and more refreshing; bringing out the dilemmas and discussing what can be done. We are all happy that the problems of the youth are receiving so much footage in a forum like MFC and hope that it leads to positive outcomes. 

*****
