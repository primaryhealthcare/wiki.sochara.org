---
title: Some Readings on CAA/NRC and health
description: 
published: true
date: 2023-08-11T07:36:49.993Z
tags: 
editor: markdown
dateCreated: 2023-08-11T07:36:49.993Z
---

This is a web-friendly version of the article titled Some Readings on CAA/NRC and health from medico friend circle bulletin. You can find the original in the PDF at http://www.mfcindia.org/mfcpdfs/MFC382.pdf

# Some Readings on CAA/NRC and health


1. India’s medical community rallies to help protestors injured in police violence Anoo Bhuyan BMJ 2020;368:m203 doi: [10.1136/bmj.m203](https://www.bmj.com/content/368/bmj.m203) (Published 17 January 2020)
2. Indian health care caught up in violence https://www.thelancet.com/action/showPdf?pii=S0140-6736%2820%2930097-0
3. Protests in India: doctors condemn police violence and restrictions on hospital access https://www.bmj.com/content/368/bmj.m13
4. Statement, 23 December 2019 Re: CAA Protests, Medical Ethics And The Right To Medical Assistance http://phmindia.org/wp-content/uploads/2019/12/CAA-PROTESTS-MEDICAL-ETHICS-AND-THE-RIGHT-TO-MEDICAL-ASSISTANCE.pdf
5. STATEMENT By Health Networks, Health Activists, Health Professionals, Women’s Rights Activists and Concerned Individuals against Indiscriminate Use of Force At Jamia Milia Islamia University and Aligarh Muslim University (AMU) https://phmindia.org/2019/12/21/statement-by-health-networks-health-activists-health-professionals-womens-rights-activists-and-concerned-individuals-against-indiscriminate-use-of-force-at-jamia-milia-islamia-univer/
6. UNAFRAID The Day Young Women Took the Battle to the Streets- Women’s Testimonies from Ground Zero at Jamia Millia Islamia University https://counterviewfiles.files.wordpress.com/2019/12/unafraid_thedayyoungwomentookbattletostreets.pdf
7. How the police prevented medical volunteers from working at the CAA protests https://caravanmagazine.in/politics/how-the-police-prevented-medical-volunteers-from-working-at-the-caa-protests
8. Delay of Medical Care to the Injured by Police Is Unconstitutional https://thewire.in/rights/protesters-medical-care-police
9. Report on NHRC Mission to Assam’s Detention Centres from 22 to 24 January, 2018 https://avaazpress.s3.amazonaws.com/NHRC%20Report%20Assam%20Detention%20Centres%2026%203%202018.pdf
10. Health professionals must call out the detrimental impact on health of India’s new citizenship laws January 14, 2020 https://blogs.bmj.com/bmj/2020/01/14/health-professionals-must-call-out-the-detrimental-impact-on-health-of-indias-new-citizenship-laws/
11. Reports of students visiting Delhi (from MFC e-group)

## General Readings on CAA and NRC
1. The NRC is a bureaucratic paper-monster that will devour and divide India https://scroll.in/article/948969/the-nrc-is-a-bureaucratic-paper-monster-that-will-devour-and-divide-india
2. NPR, NRC: 2 sides of the same coin https://mumbaimirror.indiatimes.com/news/india/npr-nrc-2-sides-of-the-same-coin/articleshow/72985517.cms
3. Dr BR Ambedkar, Not Nehru, Gave Us the Preamble to Constitution https://www.thequint.com/lifestyle/books/bhim-rao-ambedkar-nehru-constitution-preamble
4. Why the National Population Register is more dangerous than the Assam NRC https://scroll.in/article/949097/why-the-national-population-register-is-more-dangerous-than-the-assam-nrc
5. CAA & NRC III: Who are ‘doubtful’ citizens NPR seeks to identify? https://www.businesstoday.in/current/economy-politics/caa-nrc-iii-who-are-doubtful-citizens-npr-seeks-to-identify/story/392587.html
6. Risks of digitalisation with NPR https://economictimes.indiatimes.com/blogs/et-commentary/risks-of-digitalisation-with-npr/
7. State Of Affairs In UP Shows Complete Collapse Of Rule Of Law: People’s Tribunal https://www.livelaw.in/news-updates/peoples-tribunal-on-state-action-in-up-was-conducted-in-delhi-151784