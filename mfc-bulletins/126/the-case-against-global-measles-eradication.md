---
title: "The Case Against Global Measles Eradication"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled The Case Against Global Measles Eradication from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC126.doc](http://www.mfcindia.org/mfcpdfs/MFC126.doc)*

Title: The Case Against Global Measles Eradication

Authors: Henderson D.A

﻿


126

medico friend circle bulletin

MARCH 1987




The Case for Global Measles Eradication

Donald Hopkins, Alan R. Hinman, Jeffrey P. Koplan, J. Michael Lane

The purpose of this paper is to advocate establish­ing global measles eradication as a goal, to be achieved by accelerated implementation of the World Health Organisation’s (W.H.O.) Expanded Programme on Immunisation (BPI). Eradication of measles is desirable from humanitarian and fiscal grounds, and it is feasible on technical and epidemiological grounds.

Impact of Measles on Health Measles is a major epidemic disease, particularly in children. Except in isolated populations, measles is nearly universal, and most persons are infected be­fore the age of 15. Measles causes serious complica­tions, particularly diarrhoea, encephalitis, otitis me­dia, pneumonia, exacerbation of protein energy malnutrition and death (1). Death following measles is especially common in malnourished children in the developing world (2). Therapy for measles and its complications is a major drain on medical care resources in most parts of Africa, Asia, and Latin America.

Walsh and Warren estimate that approximately 900 000 deaths from measles occur each year in the developing world (3). Measles is one of the leading causes of death of young children in many countries; it is the leading cause, or the second leading cause of death in children aged 1-4 in several Latin American cities (4). Measles outbreaks in Africa and Asia have case-fatality rates of 10-20% in children aged 1-4 years, many of whom are malnourished (2, 5). Measles complications may also result in develop­mental retardation, lifelong handicaps, both direct and indirect economic loss. Furthermore, in the developing

world, measles interacts with diarrhoeal disease and malnutrition to increase the morbidity and mortality from these conditions. The economic toll taken by measles, and the health effects it causes, should be reduced to the lowest level possible.

Prospects for Measles Eradication

The introduction of live measles virus vaccine in 1963 has had a profound impact on the occurrence of measles in the United States (6) and in other countries where it has been applied in an organised and widely supported programmer(7). However, the vaccine has not enjoyed the widest possible use because it used to be expensive and to require a smoothly functioning cold chain to preserve the viability of the vaccine virus, and because measles is not viewed as a serious problem in some countries.

Measles can be controlled, indeed eradicated, by widespread and logical application of vaccine. Three factors support this concept: measures to eradicate smallpox have been successful; measles vaccines that are more heat stable than earlier vaccines are now available, and experience has been gained in efforts to interrupt transmission in the United States (8) and Africa (9).

Prospects Based on Success of Smallpox Eradication:

The success of the smallpox eradication program (10) suggests that global eradication of measles is also possible because there are many similarities between measles and smallpox. Both viruses cause infections which are accompanied by typical rashes and which confer life-long immunity; and both viruses have no animal reservoir and do not produce a chronic carrier state in man. However, important differences bet­ween them preclude the drawing of exact parallels, between smallpox eradication and measles eradica­tion (11). The first is the highly contagious nature of measles, which is capable of causing explosive out­breaks and spreading rapidly. Typically three ­quarters of susceptible contacts will be infected on initial exposure (12). Smallpox generally spreads more slowly and can be contained by aggressive out­ break control measures. Usually, only one third of susceptible household contacts are infected after initial exposuire to smallpox (13). This difference in contagiousness suggests that an essential ingredient of any measles eradication programme would be the achievement and maintenance of very high immunisa­tion levels, probably in excess of 90 %, whereas small­pox was eradicated by outbreak and case contain­ment in many areas, with general immunity rates of less than 50 %(10). Thus measles eradication pro­gramme probably cannot be as focused in its efforts as was the smallpox eradication programme; measles immunisation would have to reach virtually all parts of the country simultaneously and successfully.

A second important difference is that the average age at which infection with smallpox occurs tended to be 4-5 years old or more, whereas measles infection in the developing world generally affects those 12-18 months of age. Also smallpox vaccine is safe and effective when given to newborn infants, but measles vaccine is not effective given before the 6th or 9th month of life because of interference by maternal antibody; indeed maximum seroconversion may not occur in some populations until the age of 12-15 months (14). Thus a permanent primary-care infra­structure capable of routinely delivering vaccines to the majority of the population is necessary to eliminate measles transmission. Smallpox eradication was in many areas accomplished by an intermittent or itinerant immunisation campaign, without a permanent primary-health-care infrastructure.

A third difference is that measles surveillance is more difficult than smallpox surveillance. Measles is more readily confused with other illnesses that pro­duce a rash, and mild cases are more likely to be miss­ed. Surveillance is the primary tool for evaluating the effectiveness of vaccine campaigns, and good sur­veillance data are necessary to shape adjustments and improvements in the programme. The difficulties of measles surveillance may impede the effectiveness of special operations when the number of residual cases becomes increasingly small. The fourth difference is that those who have ac­quired immunity to smallpox can be easily recognised by the vaccination scar or by pockmarks, but those immune to measles are not so easily identifiable. Determination of the true prevalence of immunity to measles in communities may thus require occasional serological surveys unless very good records are avail­able. Such surveys would add to the logistic and laboratory costs for a measles eradication programme.

Prospects Based on Availability of Heat-stable, Cheap and Effective Vaccine:

Two constraints to controlling or eradicating measles in developing countries used to be the sensiti­vity of the vaccine to heat, and its high cost. The constraints have been considerably eased by the deve­lopment of a vaccine which can remain potent in the freeze dried state at ambient tropical temperatures for 3 to 4 weeks, (15) by the availability of containers which can maintain vaccines at low temperatures, and by the reduction in the cost of measles vaccine to the current W.H.O. price of U.S. $ 2 for a 20-oose vial.

These improvements are helpful, but the adminis­tration of measles vaccine, by a sterile needle and syringe or by jet injector gun, is not as convenient and cheap as the administration of smallpox vaccine, which is given by intradermal inoculation with the bifurcated needle. Moreover, in developing countries it is more difficult to establish high standards of qua­lity control for the production of measles vaccine than it was for smallpox vaccine, which is produced by a simpler process.

Measles vaccine is quite, effective; it generally produces complete immunity in more than 90 % of the recipients when given at or after 9 months of age. Age at vaccination should vary by country and region, and depends on the persistence of maternal antibodies and the age specific incidence of measles. In the United States, for reasons which are not totally under­stood, seroconversion rates in 9-month-old are lower than when vaccine is administered after the first birthday (14). The age at which vaccine is given might be addressed by a WHO expert advisory group. Fortunately, there are essentially no contraindications or serious side-effects which hamper the widespread application of measles vaccine.

Prospects Based on Experience with Interruption of Measles Transmission:

Despite the likelihood that worldwide measles era­dication would be more difficult than smallpox era­dication, recent success in interrupting measles transmission­ and both 

2 and administrative advanc­es make measles eradication practical. Sizeable geographic areas have been maintained measles-free by a programme of routine immunisation, aggressive surveillance, and outbreak control. During the re­gional Smallpox Eradication/Measles Control Pro­gram in West and Central Africa in the late 1960s and early 1970s, The Gambia, with a population of about 350,000 persons, eliminated reported measles, and for over 2 years maintained a measles-free state, other than for occasional, imported cases which were rapidly controlled(9). Recent experience in the United States indicates that large portions of the country are al­ready free of indigenous measles transmission(8)­during 1980,77% of the 3144 counties in the United states were free of measles for the entire year(16). The U.S is making steady progress towards its goal of eliminating indigenous transmission of measles by October, 1982.

Implementation of a Measles Eradication Programme

Given that the mortality and morbidity of measles are considerable, and that the prospects for measles eradication are encouraging, consideration of global eradication of measles is inevitable. In fact, a mecha­nism for achieving this goal already is being developed: the global Expanded Programme on Immunisation (BPI) coordinated by the World Health Organisation. The EPI is successfully working with national govern­ments and international donor agencies towards mak­ing childhood immunisation (diphtheria, pertussis, tetanus, polio, BCG and measles) routinely available to all children by 1990. The goal of the EPI is at present stated in terms of service delivery, but morbi­dity targets will soon be established (17). Measles eradication seems to be an appropriate goal for the programme.

We believe it would be easier to eradicate measles by an intensive early effort in a limited period, rather than by a gradual effort over a prolonged period. Partial vaccination of the target population may change the epidemiology of measles such that susceptible persons may become dispersed in the population (6). In endemic areas, most individuals are infected with measles before their 3rd to 5th birthday, and the target group of susceptible children can easily be defined as those aged between 9 months and 5 years. Vaccina­tion efforts can be directed toward this group. A point to beware of is that imperfect coverage by a vaccination programme may result in the accumulation of susceptible individuals in older age groups because, as some children are immunised, the rate of infection declines, and some individuals may enter late child­hood without having been to measles cannot be as readily identified as those susceptible to smallpox, it will be more difficult to ensure that remaining sus­ceptible individuals are vaccinated than was the case during the Smallpox Eradication Program. Near universal vaccination of those less than 5 years old prevent this problem.

Benefits to Developed world of global Eradication of Measles

It is the interests of not only the developing world but also the developed world that would be served by global eradication of measles. For instance, in many developed countries immunisation levels are high enough to reduce measles incidence to a point where the disease persists but is no longer a constantly visible problem. In England, for example, measles vaccine acceptance in children is approximately 50 %, and stable numbers of measles cases and deaths continue to occur each year (average 125,000 cases per year in the period 1971 to 1980, inclusive, and 24 deaths per year in the period 1971 to 1977, inclusive) (18,19). Although this represents a 70% decline in cases and deaths compared with that in the immediate pre-vaccine era, a concerted public health effort could further reduce unnecessary death and disability from measles in England.

Even in places where indigenous measels trans­mission has been eliminated, such as in the United States, the nation's costs of routine measles vaccina­tion, surveillance, and response to imported cases will be approximately $ 10-25 million per year (total of private sector and public sector costs). The earlier the global target is achieved, the earlier the United States can discontinue these costs. It cost the US a considerable sum ($ 124 million a year) to keep itself free of smallpox for more than a quarter of a century before the global Smallpox Eradication Programme began. The $ 32 million which the US invested in the Smallpox Eradication Program over 10 years was thus recouped every 3 months once global progress against smallpox meant that she could discontinue routine vaccination and other activities related to protection against smallpox (20). The prevention of measles by vaccination was estimated to yield an annual net saving of $ 130 million from 1963 to 1972 in the United States (2l). Current annual savings are estimated to be approximately $ 500 million. Measles vaccinations in the United States is estimated to have a benefit: cost ratio of 10 to I. (The return on investment in the developing world, where morbidity and mortality for measles are higher, would be even greater-a prelimi­nary analysis of vaccine programmes in the Ivory Coast suggests the benefit to cost ratio may well exceed 20 to 1 (Shephard DS, personal communication). infected or vaccinated. Since persons who are susceptible 

3

Effects of Failure of a Measles Eradication Programme One must always consider the risk of failure. If global measles eradication is not achieved by the EPI, efforts to protect against measles will have to continue. However, if a measles eradication programme did not reach its target on schedule, the world would nonethe­less be a heal their place, with millions of children protected not only from mealses, but also from diph­theria, pertussis, polio, and tetanus. The structure to deliver other needed immunisations and indeed other aspects of primary health care will be promoted rather than jeopardised by an early vigorous attempt to eradicate measles.

Source: The Lancet, June 19, 1982.

REFERENCES

1. Krugman S. Katz SL. Infectious disease of Children, St. Louis: C.V. Mosby, 1981 2. Morley D, Woodland M, Martin WJ. Measles in Nigerian children, J Hyg Camb 1963; 61: 115-34 3. Walsh JA, Warren K S Selective primary health care: An interium strategy for disease control in developing countries N. Engl J Med 1979; 381: 967-74. 4. Puffer R R., Serrano CV. Patterns of mortality in childhood. Report of the Inter-American investigation of mortality in childhood. Pan American Health Organisation, Scienti­fic publication No. 262, Washington 1973. 5. John TJ Joseph TI, Radhkrishnan J, Singh RPD' George K. Epidemiology and prevention of measles in rural south India. Ind J Med Res: 1980; 72:153-58 6. Hinman AR, Brandling-Bennert AD, Nieberg PI. The' opportunity and obligation to eliminate measles from the United States. JAMA 1979; 242: 1157-62 7. Sejda J. Evaluation of the eight year period of compulsory measlses vaccination in the Czech Socialist Republic (CSR), J HygEpidem Microbiol Immunol (praha) 1979; 23: 273-83. 8. Hinman AR, Eddins DL, Kirby CD, et aI. Progress in measl­es elimination. JAMA 1982; 247: 1992-95. 9. Forge WH. Measles vaccination in Africa, Proceedings of the international conference on application of vaccines aga­inst viral richettsial, and bacterial diseases of man. Pan American Health Organisation Scientific Publ, No. 226, 1971. 10. World Health Organisation. The Global Eradication of Smallpox, Final Report of the Global Commission for the Certification of Smallpox Eradication, Geneva, 1980. II. Hinman AR, World eradication of measles Rev Inf Dis (in press). 12. Hope-Simpson R E Infectiousness of communicable disease	in the household (measles, chickenpox, and mumps) Lancet 1952; ii: 549-52 (Conld. on p. 6)

The case against global Measles Eradication Dr Hopkins and his colleagues have enthusiastically presented a case for global measles eradication and, although not proposing a specific time limit for its achievement, they argue that "it would be easier to eradicate measles by an intensive early effort in a limited period, rather than by a gradual effort over a prolonged period". This proposal is counter to the current strategy of WHO's Expanded Programme on Immunisation (EPI) which advocates the progressive expansion of childhood immunisation programmes against six major diseases, with the eventual objective of making the six vaccines accessible to all children by 1990.

The definitive goal of the global eradication of measles is attractive in that it would eventually permit cessation of measles vaccination. But how realistic is this objective and what are the implications of such a programme being launched, presumably with the endorsement of the World Health Assembly and with WHO coordination?

The eradication of measles poses far more substan­tial obstacles than did smallpox eradication. The continuing transmission that occurs today in all but small, isolated island nations stands as moot testi­mony to the difficulty. When the smallpox eradica­tion programme began, all industrialised and many developing countries had already successfully interrup­ted transmission with programmes of widely varying quality. It was apparent from the beginning that the eradication of smallpox was achievable with a reason­able margin of error in programme execution. Measles vaccine, although improved, is still much more heat labile than most smallpox vaccines, and the technique for inoculation, requiring a needle and syringe or jet injector, is far more cumbersome than the simple multiple-puncture method of smallpox vaccination. The technology of vaccination is crucial because measles spreads so rapidly that, in many developing countries, most children have been infected by 3 years of age. To interrupt transmission, a highly effective immunisation campaign would be necessary, focusing on the vaccination of children during a narrow window in time 

4





R.N. rtS65/76

From the Editor's Desk

Singanikuppam, a village with a population of less than 700, is situated 15 Kms away from the Taluk hospital in Tindivanam, Tamil Nadu. In Aug 1985, two months before the much publicised Universal Immunization Programme, there was an outbreak of measles which left 27 dead and 72 children hospitalized for severe complications. In 1983, two months before the outbreak, a charitable organisation had organized a mass immunization programme aga­inst measles in this village. And though the Taluk hospital was barely 15 Kms away, the epidemic came to the notice of the State Health Authorities after two months had elapsed by which time it had already run its course.

This particular incident is illustrative of an impor­tant principle with regard to measles eradication programme. To eradicate measles it would require the constant maintenance of very high immunization levels, probably in excess of 90 % among the suscepti­ble population. Even then, it does not eliminate the possibility of an epidemic if the remaining 10 % are in clusters as in Tampa in 1968. In what was considered a highly successful mass vaccination campaign, 26,200 children or 94 % of the estimated susceptible were reached on a single Sunday in January. In March a survey of a stratified random sample revealed that in reality, barely 80% of the target group had received the vaccine and that the remaining 20% susceptible were largely geographically clustered with 70 % susceptibility in some areas. In late April Measles hit these very clusters (as quoted in Fox 1983). In the case of Singanikuppam, the affected children were from those born after the immunization pro­gramme and those who had not received the vaccine during the camp. In two years time the accumulation of susceptible was large enough for the outbreak of an epidemic and this time it was more serious than the others the village had witnessed. Through computer simulations it has been esti­mated that annual mass vaccination campaigns would terminate

endemicity, within four years and that a continuing programme of immunization of children at six months of age (assuming 75 % acceptance) would accomplish the termination in seven months. No one is talking about the cost of course. Hence if the objective is to eradicate measles, it is obvious that vaccination cannot be attempted in a halfhearted man­ner and the programme has to be implemented simul­taneously in every part of the country in an intensive way. Otherwise epidemics will continue to occur owing to the accumulation of susceptible and due to spread from adjoining unprotected areas.

As to the global eradication of a contagious disease, its advisability remains questionable. The global eradication of small pox is considered "one of the most brilliant accomplishments in medical history, and indeed a historic milestone". That is as long as it does not become a mill-stone around our necks! I am referring to the situation as one that is highly con­ducive to a successful biological warfare with variola virus. Small pox passed on through infected blankets to reduce the resistance of North American Indian tribes is probably the earliest example of how the 'vul­nerability' of a disease free population can be taken advantage of. That this possibility exists and that the fear is not a product of a paranoid mind can be seen from the fact that even today, the military personal in the USA continue to receive small pox vaccination at the time of their recruitment and every five years there after. This is surely not because the US military personnel have been identified as a 'high risk' popula­tion or a 'cluster' that needs protection!

And finally in all these debates on global and natio­nal eradication through medical interventions, it should not be forgotten that in the USA, medical interventions for measles was responsible for only 1.38 % of the total fall in the age-sex adjusted death rates due to the disease.

—Sathyamala

........

Editorial Committee: Anil Patel Abhay Bang Dhruv Mankad Kamala S. Jayarao Padma Prakash Virnal Balasubrarnanyam Sathyamala, Editor

Views and opinions expressed in the bulletin are those of the authors and not necessarily of the organization. Annual Subscription - Indland Rs. 15.00 Foreign: Sea Mail US $ 4 for all countries Air Mail: Asia - US $ 6; Africa & Europe - US $ 9; Canada & USA - US $ 11 Edited by Sathyamala, B-7/8811, Safdarjung Enclave, New Delhi 110029 Published by Sathyamala for Medico Friend Circle Bulletin Trust, 50 LlC quarter University Road, Pune 41106 Printed by Sathyamala at Kalpana Printing House, L-4, Green Park Extn.. N. Delhi-16 Correspondence and subscriptions to be sent to-The editor, F-20 (GF), Jungpura Extn., New Delhi 110014. 
