---
title: "Paralytic Poliomyelitis : A Tragedy On The Rise"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Paralytic Poliomyelitis : A Tragedy On The Rise from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC130.doc](http://www.mfcindia.org/mfcpdfs/MFC130.doc)*

Title: Paralytic Poliomyelitis : A Tragedy On The Rise

Authors: Burrett Gloria

﻿


130

medico friend circle bulletin


JULY 1987

Paralytic Poliomyelitis: A Tragedy on the Rise Gloria Burrett

­

The Spastic Society of Northern India is a voluntary organisation which runs the Centre for Special Education. This is a special school for children with cerebral palsy. Here problems such as stubborn self willed limbs, specific learning difficulties, hearing problems and unintelligible speech are handled by a group of professionals working closely with the parents of the affected children. This article traces our increasing inter­vention in the area of polio, both in management and prevention. This is significant in that, cerebral palsy was intended to be our main area of specialisation. We also hope to project through this the enormity of the problem in that-epidemics of polio are occurring in an area well connected to hospitals; despite the OPV (oral polio vaccine) children are being affected by polio; and general practitioners are aggravating the problem by the indiscriminate use of injections.

Faridabad. One could say that the area we chose had access to primary health care as well to other referral services.

Soon after the rural centre was opened, we realised that there were several problems in starting a centre of this kind in villages. The whole area of "handicap" is not a priority. Children with severe cerebral palsy die or are starved to death. In poorer families like those of the daily wage' earners where the work load on parents is greater, each day's wage is precious; and where it is diffi­cult to feed one's normal children, the handicapped child is a problem that is best forgotten. So, often we had to create a problem in the minds of parents and make the child's handicap appear more serious than it seemed to them at that time. In our minds we could imagine these disabled children growing into handicapped adults.

Our rural centre: Two years after starting our centre at Delhi and following our brief experiment with the children from Madangir (an urban slum development area of Delhi), we felt it was necessary to reach out to the many children affected by cerebral palsy in the villages. Our centre would have to be sufficiently distant from Delhi for it to be viewed as a necessity and yet close enough for our regular visits. This October will have made it six years since the incep­tion of our rural centre at Dayalpur village (Farida­bad District). The centre is situated within the Primary Health Centre campus, 10 kms from the district hospital at Ballabgarh which is also the training centre for interns from the All India Insti­tute of Medical Sciences, and approximately 20 kms from the main civil hospital at 

Shift of Priorities:

Of the seven students we started with, only one, a severely handicapped, immobile, intelligent, eight year old with cerebral palsy was brought in by parents who were desperate for our services. The others we admitted were either mildly affected physically and/or had speech problems. One was a child with polio whom we wanted to integrate into a normal school as quickly as possible. There was always the feeling that with our limited staff and our initial intention of rehabilitating children with cerebral palsy, the condition of polio could never take precedence. We believed that the severely affected spastic children in the course of time would make up our numbers and take up our full attention. However, within six months, and increasingly so today, the great number of polio referrals and to a lesser extent those with hearing problems have .made us think otherwise. The following facts are indicative of our growing involvement with polio.

Today the breakup of our children is as follows:

 Polio — 52 Hard of hearing —24 Cerebral palsy —17 Others —13

These figures are not a complete index of cases existing in this area, but only those who attend our centre. There are many mildly affected polio cases who see no need for our services and some severely affected cerebral palsy children who are unable to attend our centre. But we are aware that the number of polio children who are not being catered to by the centre at present far exceeds the latter.

To cater to the growing number of cases other than cerebral palsy, a home management program­me was introduced for children needing specific help in one or two areas and needing to attend the centre only once a week, fortnight or a month. We held a six-months training course for local men and women to enable them to detect, diagnose and treat polio. Right now there are six fully trained rural rehabilitators who are instrumental in increas­ing the number of polio referrals. Their closeness with the community aids detection. They also have a special ability to motivate parents as three of them have children affected by polio.

Our medical follow-ups include regulation of drug doses for epilepsy, diagnosis of neurological conditions, audiograms, applying for free aids available in hospitals and operations. Of these, the most running around is involved in the case of operations. Upto five visits are necessary before a child is admitted and another four during admission to get feed-back from doctors. Of the 25 cases we have referred so far, 23 are cases of polio. Following surgery, and also in most polio cases not requiring surgery, calipers and crutches are essen­tial. This requires another round of hospital visits, to ensure that the recommendations we forward are heeded by the doctors. Getting into the area of polio sometimes forces from us a greater involve­ment by way of time, energy and manpower than we had bargained for and yet there is 110 choice for us.

What would happen if we referred all such cases to the nearest hospital? The Civil Hospital at Faridabad has two orthopedic surgeons but our operation cases were turned away with the state­ment that "this operation is not done here". The District hospital at Ballabgarh does not have an orthopedic consultant. Four years ago, during an ICMR project on polio, a team from the Rehabili­tation department of the All India Institute of Medi­cal Sciences visited the hospital regularly twice a week. Since then all cases requiring rehabilitation are referred to Delhi where the department provides us maximum support by way of diagnosis and provision of aids. A word about the ICMR project is in order here as an example of Government's intervention in the area of polio. The project covered the greater part of Faridabad district. The ICMR team conducted an impressive survey of the number of handicapped children of all catego­ries in all the villages. Measurements for calipers for all polio children were taken at one go and the families were promised delivery of free calipers at their homes. Although some children did receive the calipers immediately, we know that these were still being delivered as much as two years later. Apart from the obvious outcome of ill-fitting calipers and disappointed parents, even those with well-­fitting calipers discarded them in a couple of months due to a lack of follow-up. Many homes in the ICMR project area display calipers hung up on the walls as an 'object de art’!

Coming back to the question of referral services for the handicapped children, the chances are that they would be sent onto the hospitals in Delhi of which, Kalavati Saran Children's hospital. the All India Institute of Medical Sciences, and the Safdarjung hospitals are the most frequented. Generally in these hospitals, even if they boast of superspecialities, the rehabilitation units tell a sad tale. I recollect how an intelligent and a very confident parent of one of our polio children had to intervene in his son's treatment. He realised that the fixed contracture would need more than the wax treatment his son was being given. The Consultant was, fortunately, honest enough to admit the mistake—a costly one for the father, who had to travel the long distance from Haryana to Delhi, but the father was allowed to meet the consultant only on his third try when accompanied by me. We also have had parents tell how futile they felt the daily sessions to be and even after a year of treatment at one of these hospitals many parents have no real knowledge of the problem.



2 Over 50 % of our cases are drop-outs from such hospitals because of the problems of distance, time and money. They now have child­ren needing operations for unattended fixed joints because of parental frustration in the face of the uncaring medical profession.

All this has forced us to get into this area of polio in greater depth. We have had to follow-up old hospital cases, accompany parents to hospitals, be actively involved at every stage of the operation procedure, and motivate parents and children into doing therapy so that mobility and integration may follow.

This management of polio has made a difference in that it has helped us gain more credibility. While earlier, a high percentage of acute cases relied on the local village 'phalwan', who claim to have the ability to activate flail limbs through vigorous massage (even within the very vulnerable first month following the onset of polio), today, we are being referred acute cases as compared to the initial referral pattern of cases of plus-five years of duration.

From Rehabilitation to Prevention

The next step was apparently to go beyond the acute cases and evolve measures to prevent or lessen the incidence of polio. However, after giving it much thought, we felt ill-equipped to get involved with such measures. It would have meant involving ourselves with too many priority problems, a situation where we would be spreading ourselves too thin. We already had on our priority list the following priority areas: training courses, sheltered workshop, employment opportunities in factories, educational programming, integration, daily management of cases, following-up govern­ment facilities for the handicapped and completing the medical follow-up. But two issues forced us into the area of prevention sooner than we thought. The first was a recent case of polio following an injection administered by a private practitioner for low grade fever. The second an epidemic of six cases of polio at Madalpur village. This epide­mic would have gone unnoticed save for the referral by a Physical Training instructor in the regular school who had been approached by parents to strengthen their daughter's lifeless limbs (she had been a normal two years old till two days before the paralysis). Since then an isolated case of six-days onset was detected by us in Tigaon.  In all 10 acute cases were enrolled in the month of April 1987.

There were three disturbing facts about this epidemic. (1) A recent survey carried out by the Dayalpur primary health centre showed that out of­ the 36 cases who had received three doses of OPV, 18 had contacted polio; (2) why with two hospitals within half an hour away, should there be such an epidemic? Both these hospitals run primary health centres attached to them, and immunisation is meant to be a priority area for the staff; (3) It is a known fact that 90 % of the polio cases follow injections given during the early symptomatic stage of fever/cold. Why then were the doctors indiscriminately using injections for fever cases in children? The urgency of the situa­tion however, demanded that we take immediate action. Our action included working at four levels:

1. Private Doctors — A meeting was held where all the private doctors from the nearby villages were invited to find out their routine 'treatment' of polio as well as their awareness to the link between intra-muscular injections and polio. The meeting was most revealing in that none of the doctors present knew that an 1M injection could aggravate paralysis. They felt that as long as there was no reaction to the injection and as long as it was properly given, no problem could result. They surmised that only when the child was injected wrongly, 'injection palsy' could result and the fault therefore lay with the technique and not with the practice.

We did manage to convince them of otherwise with our practical experience, medical data, and explanations. They were also made aware of our seriousness in following up each such case with the doctor concerned. Even those who did not attend our meeting were sent a summary of the proceedings. Appreciating the doctor's problem of giving into persistent parents to whom an injec­tion is a magic remedy, we arrived at the following compromise. The doctors could continue using injections as before except in children under five years of age suffering from fever and chills and especially during the months of May to September. As a follow-up to our meeting, a private doctor ready to inject a child put the syringe away and persuaded the mother to rely on tablets, on sighting two of our staff who chose that moment to call on him!




3

2. Affected Families — A day after hearing about the epidemic, our local team went round Madalpur with their charts and songs on polio. As people who had gone through similar experienc­es, the members of the team supported and advised the parents of the affected children. Stress was placed on 'what not to do' in the first month follow­ing the attack. A report was lodged with the Sarpanch, and details’ regarding our centre was left with the parents. A month later as none of them had visited us, another visit was made to explain the change in treatment and to look into the matter.

3. Community — An all out effort was made to inform all our old and new parents as well as their neighbors about the epidemic, the need for immunization and the role of injections. Many who had not been given the OPV in Tigaon were referred immediately to Ballabgarh hospital.

Dear Friend,	

Inspite of being one of the 'cowards' myself J share the basic point that the Gadres have made in their article 'When Rome is Burning' (mfcb-124). In fact it is from the point of view of community health itself, defined in its broadest and most political sense, that I see the need for more attempts at and experiments in clinical medical practice. The process of evolving an alternative health sys­tem(s) has to include alternative ways of clinical practice. Community health theory has to in­clude an analysis of existing clinical practice (which it has to a large extent) as well as evolve a system of curative health activity conducive to our notion of an egalitarian, human, health system. But any theory evolving in a vaccum is meaning­less. Theory and practice together can be the only way of working out any socially meaningful alternative.

4. Government Health Authorities — Madalpur village is under the jurisdiction of the Chief Medi­cal Officer, BK Hospital, Faridabad.	Through a letter personally delivered to him, we appraised him of the situation and the need for follow-up action. We also offered our help. However, there was further feed back only when our local team made a second visit to the area a month later and found out that children had been given their first round of OPV. Following the CMO's delay in getting back to us, we approached a doctor at Ballabgarh district hospital who felt that it was not possible for one government organisation to interfere with the area that lies within the jurisdic­tion of another government organisation. He also felt that we, as a voluntary organisation, should not have delayed in buying vaccines and administer­ing them in the village when we came to know of the epidemic. It was obvious that the problem was back in our court. But the question remains as to why a voluntary organisation be asked to take on a major responsibility when there were well-equipped and well-staffed government hospitals that were supposed to provide comprehensive health care to the population it covered. We still have to meet the CMO, the Health Workers, and Super­visors at the two sub-centres (Madalpur and Tigaon) to find out details pertaining to the immuni­sation programme.

Of course, clinical practice alone has its limita­tions. But don't community health activities alone have their own limitations as well? Curative medicine always ends up forming an important part of any such programme and most of us fall back upon the conventional form of clinical prac­tice. However, our major impact in terms of the message carried to the people is often through the curative services rendered because they are the ones felt to be the most needed and relevant by the 'people' themselves. It is the most important contact point and a major need. But as yet, I feel, that we have failed to evolve modes of clinical practice in accordance with our understanding of the existing and of the desirable health system: the doctor-patient relationship, technology to be used, the approach	towards 'treatment' and 'healing', the holistic vs partial view of a patient/ person, the balance to be struck between treatment as human intervention and allowing nature to do its own work in the healing process etc. etc... Not that all these issues have been theoretically resolved as yet but neither can they be without trying out various options in practice and testing their validity in the social reality rather than as we think it to be.

There is no way of knowing how effective all this will be. Our credibility in this area has certainly gone up.

(Contd. on p. 7)

All I'm trying to point out is that it isn't a ques­tion of clinical practice vs, community health work. They are both a part of and complementary to each other. Both are socially relevant from a radical progressive view point if undertaken with certain value positions and with attempts 

4


­

at fresh thinking and innovative action in response to the social reality around us. One can understand the reac­tion of the 'community health wallas' to curative medicine because of the overriding emphasis on it with neglect of the social aspects and because of the nature of curative medicine in the established health system. Unfortunately it has a)so resulted in our acquiring a 'holier than thou' attitude border­ing on the contemptuous towards clinical practi­tioners, even those trying to practice honestly, ethically and creatively. This is what the Gadres are reacting against, and therefore the extreme positions and strong language that they have used.

And this kind of clinical practice is in many ways a much more difficult task than that of keeping one's hands clean and staying out, engaging either in safe, 'clean' work such as a community health project doing preventive, educative and social activities or engaging in 'research'. To be able to hold one's own and work amidst others who are corrupt, within a system which can easily elimi­nate, amidst a clientele which demands a certain kind of services which it has been habituated/ addicted to is an extremely intimidating task, full of frustration, helplessness and the fear of getting carried along with the tide. It requires much greater strength of conviction and an inner strength to be able to hold out. It also requires the ability to innovate and creatively use available resources in accordance with one's critique of prevalent medicine and clinical practice. Mutual support and exchange of experiences could be most helpful in such a situation.

The debate on 'medical care vs community health', which started with the Gadres' high pitched cry of "Down with Community Health" has reached a stage when, all parties concerned, need to pause and clarify a number of issues. First, neither Gadres nor Kamla Jayarao, Uplekar——and now Das, have clarified what they mean by the term 'Community Health'. Though, Kamla Jayarao has given us some glimpses of what she has in mind by implication, she has still responded only tangentially to Gadres' attack on community health.

Gadres' main complaint is (1) it is almost 'criminal' for doctors trained to deliver medical care to go into community health; (2) such a 'capitulation' is indeed glorified by the health establishment including the funding agencies. What we are not told very clearly is, whether they are against community health as such or, against community health by doctors alone. Das has now joined this debate by enlightening the "elitist and snob sections of the health activists" that the slogan "prevention is better than cure is no longer an in-thing in fashion" and also that "to a moribund patient.....community health carries no meaning except ridicule."

Gadres seem to have completely misunder­stood what community health really is and there­fore their attack is, in fact, a 'non-attack', attacking what at present goes in the name of community health (with some notable exceptions though). Das, too, confuses the issue further by flocking together community health with other equally unclear terms like self-help.

Therefore if we see the two (clinical practice and community health work) as parts of a whole I would consider it important to support and en­courage clinical practice of the kind referred to. I wonder if mfc can become, besides a group of doctors and others focussing on community health, also a forum for discussion and debate on ethical, innovative clinical practice. The mfc bulletin can probably be used to initiate the dialogue among such clinical practitioners and between them and those focussing on community health. Later a separate cell could be made for this if the need arises making the mfc, as I see it, more 'wholistic'. Or may be a separate organisation becomes neces­sary, but that is yet far in the future.

Ritu Priya, New Delhi

Therefore, let me try to clarify what, accord­ing to me, is meant by community health.

	"Community Health is……	a way of looking at the problem of dealing with ill health in indivi­duals in their social, biological, and physical setting. Community Health has at its focus both individual and environment. Community Health is………. 	about the health of all the individuals in the community". (emphasis mine) (1). The specific tool of Community Health approach is Epidemiology besides the conventional tool of clinical medicine.

Community Health is about asking the ques­tion 'why did this child suffer from pneumonia in the first place' after treating the moribund child with bronchopneumonia following measles-a child whom Das is so anxious to save.


5






(Contd. from p. 8)

protecting the individual immunized child, is pro­bably creating a condition conducive to the increase of paralytic poliomyelitis in the community due to its partial coverage. This is the first area of concern.

A study was carried out in Pune in 1983 in which 90 % of the eligible population in the slums were immunized against polio with 3 doses of OPV (3). The number of patients admitted in the Paediatric ward from 1979 to 1984 showed a sudden increase in the number of cases from 44 in 1983 to 84 in 1984. It was found that 35 % of the affected children had infact received three doses of OPV and the vaccines had maintained their potency as observed by random checks. The study concluded that in order to control polio, 100 % coverage of children below one year is absolutely vital and the dosage of OPV should increase from 3 to 5.

largely unnoticed and by the time it does get noticed it could very well be too late. Sathyamala

References 1. Jacob John, "How shall we control polio­myelitis in India?”, Indian Journal of Paediatrics, 48: 565-568, 1981. 2. R N Basu, "Measles Vaccine, feasibility, efficacy and complication rates in a multi­centric study", Indian Journal of Pediatrics, 51: 139-143, 1984.

3. M P Phadke et al, "Poliomyelitis in Pune vis a vis Immunization in urban slums", Indian Pediatrics, 23: 5, 351-354.


CHOPAM-II

Both the UIP and EPI recommend only three doses of OPV, and if indeed the three doses pro­vide inadequate coverage, it will not only aggra­vate the situation by increasing incidence of paraly­sis but more important, it will discourage parents from getting their children immunized against polio. This is the second area of concern.

CHAI announces the second 4-week course on Community Health Organisation, Planning and Management for middle level workers with deci­sion making powers in their programme. The medium of instruction will be English and the venue will be announced later. The training period will extend from 15th November to 15th December 1987.

It has been well documented that intramus­cular injections predispose children to paralysis if later they are exposed to poliomyelitis virus. Injections which cause an inflammatory reaction can increase the risk of paralytic poliomyelitis upto 25 times. The most sensitive period is in the initial stage when the child develops fever and the first thing that the general practitioner would do in such a case is probably to give an intra­muscular injection thereby precipitating paraly­sis. The educational programmes organized by UNICEF especially through the television concent­rate on the need for vaccinations but does not mention other preventive measures that need to be taken into consideration. This is the third area of concern.

It is possible that with EPI & UIP, all these factors have led to a proportionate increase in the incidence of poliomyelitis in rural areas also. But, as the article printed in this issue points out, even if there is an epidemic right under the collec­tive noses of UNICEF, WHO, and the Ministry of Health and Family Welfare (the Head Offices of which are in Delhi), the chances are that it will go 

For prospectus and application forms contact: Programme Director, Community Health Depart­ment, Catholic Hospital Association of India, PB No. 2126, Secunderabad 500 003, A P.

(Contd. from p. 4) Following repeated requests from the parents of polio children and the village leaders, a new centre has been opened at Tigaon. A recent survey done by the local society (Jan Kalyan Samiti) gave us the names of 50 polio children. This did not include the 8 already being treated by us, and those detected more recently. It is interesting to note that the list has 3 deaf children and one child with cerebral palsy. It does seem that we will have to change our priority from providing services for spastic children to helping children handicapped with polio. It is not so much the change in priority that is causing us concern, as much as the question-When the simple technology of immunization is available why should a situation of increasing incidence of paralytic poliomyelitis come up? Could this tragedy not have been averted?

7

From the Editor's Desk

According to UNICEF, WHO, and the Ministry of Health and Family Welfare, India is on the verge of controlling poliomyelitis by the year 1990. The Universal Immunization Programme (UIP) launched in 1985, envisages coverage of the 420 districts in India in a phased manner so that by 1990, the incidence of residual polio paralysis will be reduced to less than 5/100,000 population. The strategy of UIP, at least on paper, is better than the EPI (Expanded Programme of Immuniza­tion) but three aspects in relation to the control of poliomyelitis through" the administration of OPV (oral polio vaccine) raises concern­

It has been observed in the last few years that there has been a paradoxical increase in the inci­dence of polio in areas where a large amount of polio vaccine is being administered annually. For instance in Bombay, coverage with polio vaccine was found in 37 % of school children in std IV, 46 % in std 111,47 % in std II, and 54 % in std I. Contrary to expectations, the incidence of paralytic polio has been steadily increasing in Bombay. A similar situation has been reported from Delhi, Ahmedabad, Madras, Trivandrum, all of which are urban centres. One explanation has been put forward by Dr Jacob John of the Virology Unit, Christian Medical College Hospital Vellore, to understand why partial coverage with OPV not only fails to cause a proportionate reduc­tion in incidence but also apparently results in increasing incidence of paralytic poliomyelitis (1). R.N. 27565/76	_ 

Susceptibility paralysis increases with in­creasing age at first infection. Infection when the infant is very young, with immunity passively acquired through maternal antibodies, does not cause paralysis. Therefore -in un-immunized com­munities, intensive polio virus circulation is ad­vantageous since infection would occur early. In the passively immune, infection will induce active immunity without the risk of the disease. In others, as age is low, infection is attended by low risk of paralysis. Thus in these communities, the incidence of paralysis would necessarily be low. The increasing incidence in the partially immunized communities could be due to the passively immune infants escaping infection only to become susceptible to the risk of paralysis out­side the umbrella of protection afforded by the maternal antibody. The age at first infection may also be rising, although subtly, resulting in increased incidence in older children thus increasing the chances of paralysis. Finally, there may be an increase in the frequency of intra-muscular infec­tions which coincident with infection may provoke paralysis. The first two situations are the result of the retardation in the circulation of polio viruses in the community and at the same time the presence of a large proportion of susceptible population. The two factors responsible for the retardation of the circulation of polio viruses in the community are: improvement of hygiene and partial sporadic immunization coverage as immune individuals would be poor transmitters of the virus.

Generally, only a small proportion of infants and children infected with polio virus develop paralytic illness. Susceptibility to paralysis is determined by genetic disposition, age at infec­tion, and immune status; a fourth factor is iatroge­nic, namely intramuscular injections. Of these it is unlikely that the proportion of genetically pre­disposed children is increasing, and the current increase in the incidence of polio cannot be because of increasing proportion of children getting infected due to decreased immunity. 

In 1985-86 the OPV coverage in India was 65 % in the U I P districts and barely 27 % in the EI P areas. It has been reported that there is a high drop-out rate between the first and second dose (43.9%) and second and third dose (39.9%) of OPV (2). The effective coverage is probably even lower because of the enormous difficulties in maintaining the cold chain especially in the rural areas. Thus the UIP programme, while perhaps, (Contd. on p. 7)

Editorial Committee: Anil Patel Abhay Bang Dhruv Mankad Kamala S. Jayarao Padma Prakash Virnal Balasubrahmanyan Sathyamala, Editor

Views and opinions expressed in the bulletin are those of the authors and not necessarily) of the organization. Annual Subscription - Inland Rs. 15.00 Foreign: Sea Mail US S 4 for all countries Air Mail: Asia - US S 6; Africa & Europe - US S 9; Canada & USA - US $ II Edited by Sathyamala, B-7/88/I, Safdarjung Enclave, New Delhi 110029 Published by Sathyamala for Medico Friend Circle Bulletin Trust, 50 LlC quarter University Road, Pune 41106 Printed by Sathyamala at Kalpana Printing House, L-4, Green Park Extn., N. Delhi 16 Correspondence and subscriptions to be sent to-The Editor, F-20 (GF), Jungpura Extn., New Delhi-llooI4. 
