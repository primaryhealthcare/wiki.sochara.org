---
title: "One Daughter Family: Fact or Fancy?"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled One Daughter Family: Fact or Fancy? from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC146.pdf](http://www.mfcindia.org/mfcpdfs/MFC146.pdf)*

Title: One Daughter Family: Fact or Fancy?

Authors: Jajoo Ulhas & Kalantri S P

146 medico friend circle bulletin "

December 1988

PATIENT'S RIGHT ANIL PILGAOVKAR

Everybody, including Indian Medical Council, agrees that medical ethics, as an entity in the practice of Medicine is of great importance. Yet, neither is it taught in Medical Colleges, nor is it seriously considered in clinical 'practice. What is worse is that there is no evidence of any debate on ethical issues. It is with this background that this paper on patient's rights is being presented here. We hope that it will at least provoke readers to think afresh about medical ethics. (Editor) It is said that "medicine has had its own scheme of ethics for at least 2500 years, and although the moral rules of the Hippocratic Oath have undergone considerable development and modification, much of modern medical practice is atleast officially inspired by International medical fraternity i. e. the World Medical Association's declaration of Geneva, London, Helsinki, Lisbon, Sidney, Oslo, Tokyo, Hawaii and Venice." The scrutiny of these codes reveals that there is progressively increasing awareness, recognition and respect for 'autonomy' of the patient in medical care. The importance of the need for recognition of autonomy of patient becomes apparent when one realizes that: a) the very nature of medical persuit leads to intervention into a person or personality,

b) the specialization and super specialization in medicine has 'lured' the practitioners to be more concerned with 'parts' of a person rather than the whole person and c) the religion and medicine have always been associated in the past. History of medicine has references to priest medicine man or witchdoctor trying to employ special 'power' including communication with 'God' to effect healing. The erroneous self- perception of the healers had led them to take on a 'superior breed' position. The phrase 'Patient's Rights' might conjure in the minds of some medical practitioners an alarming picture of someone trying to snatch away from them something exclusive their domain. It might provoke a confrontist response stemming from a sub-conscious feeling of vulnerability that plagues human minds. On the other hand 'duties towards patients' is something that is more amenable to doctor's frame of mind, for he/she is consciously or sub-consciously aware of the ethical requirements of profession. The doctor's duties towards his patients and their rights are in fact, two sides of the same coin, and the two are completely compatible with each other. Dr George Jacoby, who was the first to differentiate medical ethics from medical etiquettes, puts it rather succinctly,

'they deal with the question of the general attitude of the physician towards the patient. World Medical Association Declarations (Codes): Transition from 'duties of doctors to 'rights' of patients The spirit of the various declarations of World Medical Association, made from time to time demonstrates a noticeable shift from doctor's duties in earlier codes to patient's rights in later codes: The Geneva Declaration (1948, modified in 1968 and 1983) Dedicate your life to the service of humanity. Let health of your patient be your first consideration. The London Declaration (1949)

country endorses the codes. The Indian Medical Council must see to it that ethics are properly implemented. The ethics have been described in detail in "Principles of Bio-medical Ethics" edited by Beauchamp and Childress, and are also available in the American Hospital Association's Bill of patient's rights. I reproduce them briefly here: The patient has a right. 1 to considerate and respectful care. 2 to obtain from his physician complete current information concerning his diagnosis, treatment and prognosis in terms that he can be reasonably expected to understand, unless it is not medically advisable to give such information to him (in which case such information should be made available to an appropriate person on his behalf.)

The physician shall owe to his patients complete loyalty,

3 except in case of emergencies, to receive from his physician information prior to the start of any procedure ie., informed consent and/or treatment.

He shall provide absolute confidentiality on all he knows about his patient even after the patient has died.

4 to refuse treatment to the extent permitted by law and to be informed of the medical consequences of his action.

The Lisbon Declaration (1981) A patient has full right to choose his physician. He has right to be cared by a physician whose ethical judgement are free from outside interference. A patient may refuse treatment after receiving adequate information. A patient has right to die with dignity. A patient has right to receive or call for spiritual and moral comfort.

5 to every consideration of his privacy concerning his own medical care program. 6 to expect that within its capacity a hospital will comply to the request of a patient for services. 7 to obtain information as regards relationship of his hospital ~o other health care and educational institutions in so far as his care is concerned. 8 to be advised if the hospital proposes to undertake human experimentation affecting his care or treatment. 9 to examine and receive explanation of his bill regardless of the source of payment.

Adherence to the Declarations: Moral binding on Indian counterpart

10 to have reasonable continuity of care.

India, a member of the World Medical Association, is a signatory to these declarations and therefore in spirit the medical fraternity of the

11 to know what hospital rules and regulations apply to his conduct as a patient.

No catalogue of rights can guarantee the patient the kind of treatment he has the right to expect. A hospital has many functions to perform, including the prevention and treatment of disease, the education of both health professional and patients, and the conduct of clinical research. All these activities must be conducted with an overriding concern for dignity of the patient. Since few issues in medical ethics receive more attention than right of information and informed consent, I plan to discuss them at length. Right of Information about diagnosis, treatment & Prognosis: Three arguments are often put forth for limited disclosure in practice: (i) Fear of alarming the patient, a sort of benevolent deception. It must be however remembered that such acts violate the principle of respect for persons and fidelity, as well as pose threat to the relationship of trust between physicians and patients.

Right of Informed Consent: In recent years virtually all medical and research codes of ethics have held, and it is legally binding that physicians must obtain informed consent of patients before undertaking significant therapeutic or research procedures. Though these consent measures have been designed largely to protect the autonomy of patients and subjects, they are observed more in breach than in adherence. Often the consent papers merely serve as legal passport for the treating physician to get away with anything; obtaining informed consent being just an "empty ritual." Since misconceptions regarding informed consent are common, it is worthwhile examining what important functions does informed consent serve, Alexander Capron has identified these functions: 1.

The promotion of individual autonomy

2.

The protection of patients and subjects

(ii) Health care professionals cannot even know, let alone communicate, the "whole truth."

3.

The avoidance of fraud and duress.

(iii) Some patients particularly the very sick ones, do not want to know the truth about their condition. Should a patient desire so, his/her wish ought to be respected.

4.

The encouragement of self-scrutiny by medical professionals.

5.

The promotion of rational decisions.

6.

The involvement of the public in promoting autonomy & in controlling biomedical research.

Now I shall take a hypothetical case: The declaration of Oslo (1970, revised 1983) on abortion has modified "the human life from conception" to "human life from its beginning.' The declaration gives a green signal to the therapeutic abortion. The Maharashtra State Act dealing with amniocentesis and prevention of Female Foeticide forbids a doctor from disclosing the results of amniocentesis to the patient. The Declaration of Geneva implores the doctor "not to use his medical knowledge contrary to the laws of humanity even under threat." Since revealing the results of amniocentesis tests might, and have been found to lead to discrimination against female foetus a doctor should keep his lips sealed when asked about the sex of the foetus. But then does this contravene with the spirit of the codes? How should a doctor reconcile with this situation? I should appreciate reader's response on this issue.

Beachamp and Childress have pointed that the act of consent must be genuinely voluntary and that there must be adequate disclosure of information. They note that there are two elements of informed consent, each presenting distinct issue. The information component refers to adequate disclosure of information and adequate comprehension by patients of what is disclosed, while the consent component refers to a voluntary decision on the part of a competent person. They question, "but how much and what types of information must be imparted how well must it be understood? Is consent valid if it is given under conditions of social, institutional or family pressure or if the consent is irresponsible?"

Informed Consent * The voluntary consent of the human subject is absolutely essential. — The Nuremberg Code (1947) * Informed Consent: Consent to surgery by a patient or the participation in a medical experiment by a subject after achieving an understanding of what is involved. — The Webster's Medical Desk Dictionary

* Patient has a right to self-determination and that he (she) is entitled to know and approve of his (her) treatment. — The American College of Physicians Ethics Manual  Willing and uncoerced acceptance of a medical intervention by a patient after adequate disclosure by the physician of the nature of the interventions, its risks, and benefits as well as of alternatives with their risks and benefits.

— Jonson et al. And this leads us to the main issue a t stake: Why medical ethics are observed more in breach than in adherence? The issue is complex and doctors and patients are both responsible for this situation. It is indeed a paradox that no medical student is ever taught medical ethics during undergraduate training. Medical Journals in India also seldom seem to discuss ethical problems. Journal of Medical ethics, which deals exclusively' with ethics, has carved a niche for itself in very few medical libraries. Given this continued negligence of the subject, medical students are hardly interested in knowing medical ethics, let alone implement them in future practice.

'Cancer' in 1987 published a study designed to determine why physicians have misgivings about the need for informed consent. The study showed that doctors see a loss of decision making power, feel uncomfortable because of increase in professional accountability, and are concerned that it might affect the ongoing relationship with the patient. It is too naive to assume that doctors are not following medical ethics simply because most of them are not aware of ethical issues. Doctors are becoming more money-conscious, they are more callous to the patient's needs than they were before, no longer are they 'Friend- PhilosopherGuide' to their patients. Such excuses as 'work pressure and outside influences' are lame. Since patient is always at the receiving end, it is necessary that he must be conscious of his rights. Benevolent paternalism from doctors; 'let me think for you: and patient's blind faith in his doctor: "After all he is the best judge of my problems, I would do what my doctor says anyway", will be harmful in the long run. Every consultation must bring these questions to the patient's mind: What is my doctor doing? Why he is doing so '1 Will it harm me? Do I have suitable alternatives?

REFERENCES: 1 Gilen R. Medical oaths, declarations and codes.

Br Med J, 1985; 290:1194-95 2 Fleloher J. Morals and Medicine, New Jessey Princeton University Press, 1979: 5-6 3 Beavcham TL, Childress JF, eds. Principles of biomedical ethics. 2nd ed. Oxf0rd: Oxford University Press, 1983: 336-338. 4 Taylor KM, Shapiro M, Soskolne CL, et al: Physician response to informed consent regulations for randomized clinical trials. Cancer 1987; 60: 1415-1422. 5 Capron A: Informed consent in catastrophic disease and treatment Univ. Pa law Rev 1974; 123: 364-376. **

Sex Determination and Female Foeticide In Baroda (A Report by Garbha Parikshan Virodhi Manch) Following the passage of the bill banning the use of prenatal diagnostic techniques for sex determination in the Maharashtra Assembly in March this year. The Garbha Parikshan Virodhi Manch, a front formed by the progressive organisations, doctors and other concerned citizens in Baroda (Gujarat), decided to conduct a survey to find out the misuse of such techniques in Baroda city and to roughly estimate the extent of their misuse for female foeticide. The Manch interviewed 1) 30 doctors 2) -some pathologists 3) some women who have undergone the test 4) some women who are against such test and refusing to undergo despite having one or more daughters and under social pressure 5) individuals from various economic, social and cultural strata’s. FINDINGS

A. Prevalence Techniques.

of

Pre-natal

Diagnostic

1 In Baroda mainly two pre-natal diagnostic tec-

hniques are used for sex-determination, viz. Amniocentesis and Chorion-villi-biopsy (CVB). Of these two, the Amniocentesis is much more prevalent as it is comparatively cheap. 2 Except very few gynaecologists in Baroda, al-

most all do amniocentesis. 3 Of those who are doing the test, except one, all

said that personally they do not favour the test. 4 More than 70% of them admitted that the am-

niocentesis could be harmful to the mother and the foetus. Sometimes it leads to spontaneous abortion and sterility. 5 The Manch also came to knew that some doc-

tors were doing amniocentesis with inadequate precaution and facility and thus endangering the lives of the mother and the child. 6 Women from all castes, religion and economic

classes go for this test. Many of them have taken loan to pay for the test. 7 A common view that parents with two or more

daughters go for such test V\as proved to be

incorrect. We found couples going for test at first conception. We also found couples a already having one son (the only child) going for the test as they wanted son second time too. B. The extent of Misuse: 1 According to one information, a well known

laboratory in Baroda has tested 20,000 samples of amniotic fluid in last 10 years. 2 In 1987, in Baroda city alone estimated 2400

tests. 3 We came to know that a sizeable number of

women go to Anand city to undergo the test the real number of women undergoing this test must be much higher than estimated by us. C. Some other findings: Following are some qualitative findings based on case studies and observations. 1 Although doctors did admit the possible harm-

ful effect of amniocentesis on mother and child, and risk of abortion, sterility etc, none of them maintain any record about the harmful effect observed by them. 2 Some doctors said that sex-determination and

the female foeticide should be encouraged as a part of family planning programme. 3 Many doctors said that they will stop doing this

test if government bans it. But till then cannot say no as other doctors continue to do this test and that will adversely affect their income. 4 Some doctors said that they are new in the

business, they have just started their practice "let us earn a lot for while; After sometime, we will stop doing this test." 5 Only one doctor believed that this test should

be offered to all couples with two daughters. According to that doctor, as long as daughters are unwanted in the world they have no right to take birth. 6 Only three doctors condemned the test and said

that it violates medical ethics and the medical profession should take initiative to banish it.

7 Except these three doctors, the others, though

did not favour the test at personal level, blamed the society for its spread. They believed that doctors give what the customers demand. They completely absolved doctors from any responsibility regarding the -test and the female foeticide. 8 People believe that banning prenatal sex deter-

mination tests \/Viii not radically change the situation. After such ban, the test will become costlier. WADI WHALE (Translators' note for the MFC Bulletin: These findings are taken from a mimeographed report of the Manch in Gujarati. The presentation is changed for stylistic reasons. On reading the Manch's report it was clear to me that the Manch has done more of an investigative report rather than a scientific survey. However, this does not minimise' the seriousness pf the problem they have painfully highlighted. The findings are startling. The hypocracy of the commercialised medical profession is fully exposed. We hope that some more systematic research will be conducted on this subject so that an effort can be made at national level to banish such inhuman * * medical practice).

DIALOGUE Abortion

It is a pity that qualified specialists in gynaecology are not permitted to offer facilities for MTP in rural centres on the ground that their clinics are ill staffed and do not have adequate blood bank facilities. It is different matter that with the same kind of set-up they can do, and indeed they do, Caesarean section. Secondly, I strongly object to abortion being considered an extended form of contraception. With the sword of unwanted pregnancy no longer hanging over their head, young weds have become very casual in their overall approach to family planning. Thus the husband discards barrier contraceptives and the wife conveniently forgets pills. Since abortion is no longer a taboo in the modern society, even if pregnancy is discovered in the first year of married life, 'wiser counsel prevails' and abortion is considered a practical solution to the 'unwanted and unforeseen' problem. Thirdly, abortion in the minds of naive couples is 'just another procedure: They attach no more importance to abortion than the procedure of say, draining an abscess. But than it is not a simple procedure and carries with it all the risks and complications of surgery. Women are not aware that abortion can lead to infection, bleeding and rupture of uterus and can also leave behind the legacy of blocked tube-so One can easily imagine the plight of a woman getting her first pregnancy terminated, only to develop intractable sterility for the rest of her life.

Arun Gadre It was a great battle everybody fought to legalise abortion in India. It was considered as a progressive step in the liberation of women. By legalising abortion it was thought that, i) unwanted pregnancies would no longer be hindrance in the progress of a woman's life. ii) MTP would offer a permanent solution to the problem of unmarried pregnancies; and iii) abortion could be taken out of province of quacks into the safer hands of an expert. None of these objectives, I am afraid, have been fully realised. I identify four main problems: Firstly quacks are still having a field day and are openly practicing abortion in rural areas, slums and even in cities. With Legal control over medical practice being what it is, quacks continue to enjoy scot-free status and are fully exploiting the situation.

Fourth{y and lastly carnal pleasures and human comforts have started taking precedence over the traditional mother child relationship. Since a couple wants to enjoy the marital bliss and a baby is' considered as an unwanted intruder, it no longer hesitates in getting an unborn off. May be our sensitivity to this issue has become blunted, may be an abortion no longer kindles subtle emotions in such couples, but the fact remains that an abortion has become an in-thing in the first year of married life. We must fight the misuse of abortion. It will be a difficult battle. Abortion is destructive, dehumanizing and disturbing. The earlier we take up this issue, the better. **

Victim Blaming Is Not The Solution

Amar Jesani One important negative off-shoot of the anti-sex-determination campaign is the stirring of antiabortionists. I remember that during the campaign the members of the Forum Against Sex Determination had to argue vociferously to distance their campaign from the supporters of right-to-life. Certain religious organizations and the doctors who saw problem of ethics in the MTP and the female foeticide (MTP) following amniocentesis as identical. The latter arguments might appeal to those who take a kind of moralist standpoint against the misuse and overuse of any technology. This note is to examine the problem of misuse and overuse of MTP from different angle and to point out certain lacunae in these arguments. Let us start with the enactment of the MTP Act. It is wrong to say that the MTP Act was formulated in response to the woman's movement's demand. If it was, it should have been called, say, Woman's Right to Abortion Act and there wouldn't have been any need to keep so-called medical indications for performing the MTP. It is know that the feminists stand for the unconditional right to abortion. The MTP Act does not give any such unconditional right. And what was the strength of feminist movement in the early 1970s? Thus, we have to look for elsewhere to find out real reasons for government to pass this Act. I submit that MTP Act is not an act for strengthening women's right (although as a by-product women do get some facilities for less restrictive abortion) but it is an act to regulate medical profession. It was brought in to strengthen dominance of allopathic doctors in the Medical profession. It was passed to confer near monopoly to perform MTP to the allopathic doctors. It is immaterial that quacks were and are bad. What is important is the net effect. Can anybody argue that the MTP Act did not help modern doctors to get the most benefit from it? Most of the doctors doing MTP are more concerned about the government's inability to stringently apply it so that the competition from the quack is eliminated. But this is true in all aspects of medicine practice, why to single out abortion only? It is dangerous to link up lack of health education in the MTP seeker woman with the 'misuse' of the MTP.

There is nothing like misuse of the MTP because I believe that woman should have unconditional right to have it. One can punish a pregnant woman (by forcing her to take baby full term) because she is less educated, or husband is less educated about contraception, or they are lazy or, woman has no authority in the sexual relationship and in the family in general. Pregnancy is a fact, a physical reality and to deny her right to abort is to compound one oppression with another. And medical profession is well known for victim blaming. Nobody will dispute concern for the plight of the women who has no option but to undergo MTP as she is a powerless person in the family, But the solution is not in denying her even this escape. Nobody will disagree with the plea for providing sex education and education on contraception to the woman and the man; and also plea to empower women in the society. But this has to be done by expanding the frontiers of women's right and power and not by restricting whatever right she has (for example that of abortion under the MTP Act). Firstly, can woman say 'no' to husband when she understands that she will conceive? Is there any law empowering woman to do so? Is marital rape considered a criminal offence? It is not sufficient to give sermon to the woman that she should not allow a sexual relationship which makes her conceive at a time when she does not want pregnancy. What is important is to make right to say no to unsafe sex a fundamental right under law for women and to create social support system all over the country to implement such a law. The latter is more important than the former because we do not want an empty law like untouchability and many others. Are doctors who are so concerned about the misuse at MTP ready to be part of such an effort? Let me assume that doctors would like to contribute in such an effort. However, for such contribution they will have to first break their mental barrier and prejudice against women's movement however painful they might feel as a man in doing so. For whatever effort that is being made in this country to empower women is being done by the women's organisations only. Such doctors will have to establish links with them rather than blaming them for the ills within the medical profession. This brings me to my last point, what does doctors do when he or she notices a woman undergoing repeated MTPs?

Denying her the MTP would be, I believe, not only in human but also unethical. The only way the doctor can help to 'cure' this problem is by patiently finding out the problem and then "to step out of medical confines" and "enter the sociopolitical field." This has to be done because there is no "medical solution" to such problem. One has to follow up such cases in the social fields by faking support of women's and health organizations, and by breaking the complacency of medical or doctor's organisations. The acid test is how many such doctors will be ready to demonstrate in support of such women? To make such a movement strong may take sometime, but that is the real solution, or that has some possibility of taking us nearer to the solution. **

One Daughter Family: Fact or Fancy?

Even a population expert agreed entirely: 'People are fanatical about having tow living sons. It is as sacred as religion to them.’ .

That our first Minister was endeared by children all over the Chacha Nehru, that he loved children as much as the rose on his jacket, that he had only one daughter and no son and that this award should therefore be named after him to ensure equal rights to women a la Nehru ervisaged all these things are admittedly true and might appeal to the reason. But what surprises one most is the Government's naive assumption that a carrot worth Rs. 10000 will lure many a couple to stop after one daughter. This approach takes no cognizance of the stark social realities. One can not help feeling pity at this logic or rather lack of it. How many couples will accept this idea and stop after first daughter voluntary? And will 'X' or 'Y' really make no difference to the hoi polloi? These are indeed Ten Thousand Rupees Qs I UN Jajoo & SP Kalantri Publishers **

To promote the concept of 'One Daughter Family' the Government of Maharashtra has introduced a novel award of Rs. 10,000 for couples undergoing permanent sterilization after one daughter but without a son. (sic) The award christened as Pt Jawaharlal Nehru Balkalyan Award was announced on 14 November 1988 to coincide with birth centenary celebrations of Pt Nehru. XV ANNUAL MEET OF MEDICO Strange as it may seem, this announcement FRIEND CIRCLE comes close on the heels of Raj Chengappa's 27th, 28th and 29th January 1989 Technology article in India Today (October 31) in which the in Health Care: Issues and Perspective author has exposed the limitations and hollow Venue: Alwaye (Kerala) claims of the Government's family planning Mr. Menon has very kindly brought to programmes in a threadbare analysis. He our notice that Alwaye, the venue of next denounced these programmes as a big hoax and MFC meet instead of being on Cochindismissed the popular slogan, 'Beti ho ya Bete. Trivandrum route, is on CochinTrichur road. Bacche do hi acchhe as a worn-out cliché. That Many thanks for correcting this geographical the Government has paid no attention to this error, Mr. Menon. article is obvious. For consider these reactions Those who wish to participate in the meet which Chengappa has chosen as representative of (for details please refer to MFC bulletin 142people's general out look at family planning. 43-44) are requested to get in touch with These explain why people-atleast from rural Narendra Gupta, Convenor MFC, for venue background-still long for a male child. A farmer, arrangements, charges, background papers for instance said 'I need two sons. Otherwise who etc. at the following address: will light my pyre, who wills carryon the family Narendra Gupta name, who will help me on the fields, who will Devgarh, Via Partabgarh, Dist. Chittorgarh look after me in my old age? Another farmer from Rajasthan spelled out reasons for aspiring for as Rajasthan - 312 621 many as five children: 'One to look after my cows. Another to tend to my sheep. One to help me on the field. One to help my wife at home. Views and opinions expressed in the bulletin are those of the authors and not necessarily of the organization. Annual Subscription-Inland Rs.20.00 Editorial Committee: Foreign: Sea Mail US 4 dollars for all countries. Air Mai: Asia-US 6 dollars Africa & Europe Canada & USA-US 11 Anil Patel dollars Abhay Bang Edited by Sathyamala, B-7/88/1, Safdarjung Enclave. New Delhi Dhruv Mankad 110029 Kamala S. Jayarao Published by Ulhas Jajoo & S. P. Kalantri for Medico Friend Circle Padma Prakash Vimal Balasubrahmanyan Bulletin Trust. 50 LlC quarter University Road, Pune 41t016 Sathyamala, Editor Printed at Samyayog Mudranalaya, Wardha. 442001


