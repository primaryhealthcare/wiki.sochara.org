---
title: "Dialogue"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Dialogue from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC049.pdf](http://www.mfcindia.org/mfcpdfs/MFC049.pdf)*

Title: Dialogue

Authors: Jajoo Suhas, Bang Abhay

medico friend circle bulletin JANUARY 1980

COMPREHENSIVE RURAL HEALTH PROJECT, JAMKHED R. S. Arole Jamkhed is a typical rural area in Maharashtra. The Comprehensive Rural Health Project was started in 1971 and originally covered 40,000 population scattered in 30 villages. Since 1976 the programme has expanded into the adjoining area covering a total population of 80,000 scattered in 60 villages. Jamkhed was selected as the site of the main centre because it serves as a central marketing area and is the administrative village of the entire Taluka, and therefore, is a natural catchments' area for the entire Taluka. The project was started with a view to develop a health care delivery programme, best suited to the needs and resources of this rural area. The sine qua non of the project is the community involvement in the activities of the project. Many invitations were received from the village leaders to start the health work in their districts, but the final decision to start work at Jamkhed rested on the fact that at Jamkhed, the community was willing to understand, cooperate and actively participate in the project activities. At Jamkhed the leaders made arrangements to provide accommodation for the staff of approximately 20 people. They arranged to clean up a veterinary dispensary and its surroundings' so that it could be used as a health centre. The leaders also tried to understand the basic concepts of the project and tried to involve themselves as far as possible. After the decision to start the work at Jamkhed was made an advisory committee was set up. This advisory committee had representatives from all segments of population - representing women, Harijans, various political parties, geographical areas and minorities. The functions of the advisory committee were to introduce the health workers to the village leaders and community. Community involvement in decision-making is

an important feature. The advisory committee and village leaders at Jamkhed discussed the health needs and other felt needs of the village along with the Director of the project. Suitable line of action was discussed and appropriate steps suggested, The priorities planned were: 1. Simple symptomatic primary care available in the Individual village at all times. 2. Care of pregnant and lactating mothers and deliveries. 3. Care of preschool children. a) Nutrition b) Immunisation c) Simple illness care 4. Family planning - Health education and availability of-all methods at local level. 5. Contra1 of chronic illnesses such as leprosy and tuberculosis. a) Identification of cases b) Regular treatment c) Rehabilitation at village level. 6. Prevention of blindness a) Nutrition b) Infection and eye injuries c) Surgery for cataracts, glaucoma, etc. 7. Facilities for follow up and emergencies. Besides Jamkhed the community has participated initiating health programmes1in their own villages at the periphery. The project" under-takes a health programme only when there is an invitation from the entire village community. They provide infrastructure for health work is considered essential. Significant contribution by the village is provision of a local woman to become a village health worker. The health team organises enthusiastic men of the village into a Farmers Club. The VHW and farmers club will be discussed in detail later on.

In order to achieve the given health objectives a three tier system of health care deliver, has been developed. The first tier consists of the village health worker resident bleach village. The second tier is a mobile health team visiting each village once a weak or once in a fortnight. The third tire consists of the health centre at Jamkhed with diagnostic facilities and facilities for emergency care and inpatient beds. To support the health activities there is an integration of development programmes such as agriculture, provision of safe drinking water, employment schemes, rehabilitation programmes for chronically ill and non-formal education. The Village Health Worker Majority of rural health problems in the rural areas are simple. These problems get worse and often cause death when not identified and remedied at the onset. To a large extent they are preventable and amenable to health education. Thus, highly trained physician geared to making exotic diagnosis, carrying on different laboratory tests and charging exhorbitant fees is not necessary to solve these problems. What is needed is a health agent who will be always available in the village and who will be patient enough to reach the local people, change their attitude to illness, bring about positive change in their habits and prevent serious disease. Often women in developing countries are relegated to a lower position. Though capable of great achievement these women, who form fifty percent of the human resource are not able to utilise their full potential. With sufficient encouragement and training these women have proved to be dependable and responsible workers. Involving women in development and educational activities means reaching larger segments of population than a male can reach. Though most men have all kinds of reservations about acceptability, caliber, dependability and performance, these women are capable of overcoming these criticisms provided there is adequate supervision and encouragement. Liberating these women, itself is sufficient reason to make them health workers. The female VHW will be specially useful to take care of mothers and children in a country where women are confined to their homes. Recruitment of, the Village Health Worker The local village community selects a woman volunteer to act as a VHW. Basic education is not considered important. Often she is illiterate. Out of the 60VHWs recruited, 48 were illiterate and none had reached high school level. It is essential that the VHW 'belongs' to the village and that she has her roots in the village in the form of a field or house. She is the vital link between the community she serves and the professional health team. Since, she is very much part of her village she is accepted by her people.

The VHW should be preferably a married, middle aged woman with life experiences such as bearing children, and raising them. (Two of the VHWs have not borne children. They had difficulty in being accepted in the village to conduct deliveries. Only after successful difficult deliveries were performed by them, were they fully received.) The VHW must be well motivated and have genuine concern for people. She should be willing to shed her caste difference and enter into any house in the village. Often the Panchayat may suggest the sarpanch's wife who is looking only for prestige and money. This can be avoided if all sections, particularly the poor are involved in the decision making. It must be emphasized that this work is an honorable one and not a 'dirty' job meant only for Harijans. Training of VHW Training is geared in such a way that they do not spend a long time away from their home and village. Initially after the local community had recruited them, they are brought to the centre 'for one week's orientation programme. Thereafter they come to the centre for two and half days every week. Most of the teaching is done from cases they encountered during the week and cases from the centre. Their training is geared to deal with priorities mentioned earlier. Emphasis is on promotion and prevention, but the are also trained in the use of few drugs. We health professionals are used to talking about taboos, and superstitions the village people have. The VHW is familiar with these beliefs and sometimes has herself accepted them. She is encouraged to bring up and discuss such beliefs freely. Care is taken to first convince her that her beliefs have no meaning or that they are harmful by appropriate examples and experiences and then teach right attitudes towards health and nutrition. The trainer here has to be sensitive not to rob her of her dignity by laughing at her ideas, nor should she be belittled. This sensitivity on part of trainer must be emphasized. Once the VHWs are convinced with a proper demonstration, they become the most ardent promoters of change. Similarly by demonstrating the acid fast bacilli of leprosy, the VHW is convinced that leprosy is not a divine curse. In addition to beliefs about disease one must consider social environment in which they live and work. Caste is a major social factor in rural India. Though one would like it to disappear from the villages, it is very much alive. Social contacts an determined by caste. It is essential to face Ii squarely and deal with it rationally. When asked what is the most significant change they need for successful health programme their answer is to remove caste barrier. Similarly through lectures, demonstration and example the VHW are taught to be compassionate, deal justly with others, with special concern for the poor. It is thus necessary to spend equal time in

showing their social responsibility as giving them technical know ledge. The training of VHW is a two way communication. The teacher gains knowledge about local beliefs and taboos and both VHW and teacher discuss together ways and means of overcoming these. Audiovisual aids are made locally using folk lore and local materials. As the VHW gains in knowledge she is encouraged to make up her health education material. Flash cards are commonly used. Sharing of experiences and using examples from the village help to a large extent. Functions of the Village Health Worker The VHW is responsible for the health of her village. Her primary role is to impart health education particularly with reference to the priorities of the project•. The VHW is responsible for the health of the under five child. She surveys the village and using the arm circumference method encourages children with malnutrition to attend the supplementary nutrition programme. She supervises the nutrition programme and sees to it that the malnourished child is specially cared for. Weights are taken every month and plotted on the underfive weights card. The VHW explains the card to the mother. If a child has lost weight for no apparent reason, it is referred to the health team. In 1972-1973 over one third of children were malnourished. Gradually the numbers of malnourished children have declined and only those children belonging to migrant workers remain malnourished. The VHW organizes immunization programmes with the help of the community and farmers club groups, and helps the health team to carry out the immunization. Over the period of three to four years diseases such as whooping cough which were rampant have disappeared. In project villages 80·90 per cent of the underfive population are immunized against the childhood diseases. Treatment of minor illnesses as early as possible is the responsibility of the VHW. She pays home visits to children with such illnesses, in addition to those after the nutrition programme. The VHW makes her rounds in the village and systematically covers the whole village in one week, In the course of her visits she sees all pregnant mothers and encourages them to take antenatal care. Approximately 80·90 percent' of' pregnant mothers accept the VHWs advice and take regular antenatal care. She also sees to it that they are seen by the health team and get examined and receive tetanus toxoid. The VHW continues on her rounds and advises people on contraception. Women practicing family planning are given oral contraceptives or condoms. Yet others may be ready for tubal ligation" if so the VHW accompanies her to the health centre. In the course of her visits in the villages, leprosy and TB patients and their families are followed up. Their difficulties are noted and brought to the notice of the

health team". The VHW also conducts deliveries and visits mothers in the post partum period. In addition to these activities the VHW conducts a creche, and also gather children who do not go to school and teaches them. On an average VHW treats 12-20 patients daily for minor ailments. Everyday she gives health education to various groups using audiovisual aids. These audio visual aids are made by a local artist at the centre in consultation with the VHW. The topic chosen for health teaching is usually a problem currently prevalent in the village e. g. measles, diarrhoea, conjunctivitis, etc. When the health team comes to her village, the VHW is ready with all those who need their special care, for example, she lines up the women needing antenatal care, children needing immunization, etc. Sick children are also brought to be seen by health team. She seeks support from the health team and brings her problems both technical and social. With proper guidance and support from the health team she becomes an effective member of the health team. The VHW is paid an honorarium of Rs. 50/- per month by the health centre. The VHW is an agent for change. Many health problems stem from social injustices and anti-social practices in the village. As such she should be in a position to deal with village leaders and Panchayat members freely in solving such problems. If she were to be paid directly by the Panchayat, the VHW would be under obligation to the sarpanch and often would not have the boldness to bring about change. Payment by the authorities will result in favoured treatment to the Influential section at the cost of the real needy ones. The finances 'for her services can be collected on fee for service basis. Will VHW herself exploit people? Yes if she does not receive proper training, is poorly motivated and does not have close adequate supervision. This also means that the medical team and the physician set the right example before her. If a health care system cannot provide this kind of motivation and supervision VHW should not be employed. The Mobile Team The 'mobile team' is the second tier in the health care delivery system. The team consists of a nurse or ANM, a para medical worker, and sometimes a social worker or doctor. The health teams reside at the centre and visit each village weekly. The role of the health team is to support the VHW in her activities, provide an effective consultative and curative service and it is also responsible to refer those patients who cannot be taken care in the villages to the health centre. The nurse is the organizer of the team. With the help of the VHW she conducts the antenatal clinic, examining each pregnant woman at least three times during her pregnancy. She advises the VHW as to whether she can safely undertake to do the delivery at home, or whether hospital delivery is necessary.

She immunizes all pregnant mothers with tetanus toxoid. The VHW takes her home visiting to lee all post partum patients. She checks to see if the VHW has conducted a 'safe delivery'. The nurses along with other members of the team help the VHW to weigh the underfive children and plot their weight card. The nurse then sees all mothers and children needing special care. Similarly tuberculosis patients are also followed up, sputum is collected from those patients suspected as having tuberculosis. If not confirmed by sputum examination, such patients are referred to the centre for X-ray, Leprosy patients are screened for deformity shoes are provided for those having anesthetics feet. The VHW reports on those not taking regular treatment and the paramedical worker visits such patients. All contacts are treated and checked periodically for signs of the disease. The nurse also conducts a curative clinic. All patients with various illnesses are brought to her for treatment, she examines, diagnoses and treats them. From time to time she immunizes the children as scheduled by the VHW. If the nurse feels that the sickness is beyond her capability, or if special diagnostic studies are needed, she refers such patients to the centre. She also refers seriously ill patients needing hospitalization to the health centre. The health team is able to carryon all these activities with the help of detailed standing orders from physician, The Training or the Health Team The health team has an in-service training programme once a week. They have to learn to support the VHW in her activities: In the beginning, nurses find it difficult to accept the VHW and even get jealous of her rapport with the community. It is necessary that the team learns to respect each member and realize that each has an essential role to play, The teams spend fifty per cent of time learning about the social problems, injustices, such as that meted out to women, the attitudes of people, communication, motivation, etc. The rest of the time is spent enhancing their technical skills, with special reference to the priorities of the project. The Health Centre The health centre situated at Jamkhed is the coordinating centre for all the activities of the project. It is the referral centre both for the VHWs and the health team. The centre is equipped to deal with problems related to the priorities of the project. Since the planning is at the grass roots in the villages, and the centre exists to solve these problems the diagnostic and curative facilities are kept to the minimum. For example X-ray facilities for project problems will be for confirmation of TB and fractures; an inexpensive simple X-ray machine suffices the need. Similarly laboratory tests are limited to blood counts,

smears for AFB and facilities for cross matching and blood transfusion, Operating room is mainly for abdominal emergencies, tubal ligation, obstetric emergencies, cataract and simple eye surgery. Arrangements are made with District Voluntary Hospitals to deal with elective surgical problems and patients needing extensive diagnostic services at Ahmednagar which is 46 miles from Jamkhed. The health centre has facilities for training the VHWs, health teams, and other medical staff of the health centre. All involved in health care are trained together as a team. Hierarchical behaviour is discouraged and horizontal relationship among workers is encouraged. Delegation of responsibility from highly skilled to lowly skilled is emphasized. Nurses are trained to screen the patients and take care of any routine problems. Similarly nurses delegate responsibility to the less trained workers. This helps to multiply the bands of the health workers. Similarly medical practitioners of various backgrounds are organized into a formal group. Majority of rural people seek medical care from private medical practitioners. These medical practitioners are in unique position to spread health education and improve health of the people. The local medical practitioners have been organized to immunize populations against epidemics, to organize eye camps and sterilization camps. Together with the local primary health centre doctor and block development officers, family planning camps are arranged. There is close co-operation in the field of family planning Roads have been built to remote villages, the government (Zila Parishad) providing cement, pipes, village people providing money and the project supervising the work and providing the food for those working on a food for work basis. Other Development Activities The third feature of the Comprehensive Rural Health Project is the integration of allied services such as agriculture, employment and water development with health programmes. Better nutrition, better conditions of living, various economic and social factors contribute to better health. Therefore it is necessary to encourage the development of programmes which will provide more food, water and better conditions of living. If the VHW talks about better nutrition in the absence of bare necessities of life, her talks are fruitless. Moreover when felt needs of communities were discussed it was found that the most urgent felt need of the community were employment, water, food and shelter. The Village people, specially from the poorer section of the villages are eager to improve their villages. These people organize themselves into young farmers clubs care is taken that all socio-economic backgrounds are represented but the majorities are drawn from the poorer communities. The programme

they undertake include providing employment improving agriculture, and development of wa1 resources and housing. Agencies such as CASA are contacted for help in development programmes Jamkhed is a chronic scarcity area with poor land and water supply. The employment schemes carryout by the young farmers clubs include building roads to their own villages to improve communication. They all join together and build the road on food for work basis. Other employment schemes include land development. Irrigation well are also dug. The farmers club plan that these programmes particularly benefit the poor. They also identify fallow land and bring such land under cultivation. The proceeds from these lam are used in the nutrition programme in their village. They pool their resources and buy farm implements such as spray pumps, threshers, etc. They collectively use bullocks, pump sets. The club member’s also tall active part in the health programmes of the village: They organize and conduct the nutrition programme they also help in weighing children arranging immunization programmes, etc. These groups with the village health workers at the agents of change. If supported and guided, the people can build a better and healthier community. Under this programme over 100 check dams have bee built in the area. Approximately 750 acres of Jan have been leveled and brought under cultivator Eighty six tube wells of safe drinking water have bee constructed and maintained by a repair crew locally trained. Approximately 50 community irrigation wells have been completed. Finances: The capital expenses were initially met by the present national and other international charitable organisations. These expenses included: 1) Building a 30 bed-health centre with out-patient department, operating room, laboratory and X-ray facilities, obtaining centre and storage facilities. 2) Staff quarters for 30 staff members such as doctors nurses and para-medical workers. 3) Equipment including X-ray unit, simple laboratory facilities, equipment for routine outpatient work, emergency surgery and obstetric services. It also includes a generator and audio - visual equipment. 4) Vehicles for mobile work. This capital expenditure is approximately Rs. 15/per capita. The recurring expenses of the project are on a self support basis. Curative care is a felt need of the people and most people are willing to pay for such services. The total expenses are therefore met by fees collected from patients at the centre. This income is used to provide comprehensive community oriented health programme in the village. The annual cost of such a programme is approximately Rs 5/- per capita. Only the leprosy rehabilitation work and pro-

gramme for prevention or blindness is subsidized by voluntary donor agencies. In addition to the health programme, voluntary agencies interested in community development help start programmes such as goat co-operatives, agricultural 'credits, food for work projects, poultry, and educational programmes. Records and Evaluation: Records are kept by the VHW, mobile teams, and staff at the centre. The purpose of records is mainly for the use of workers themselves to understand problems, help in follow up of priorities, and in setting goals and planning programmes. The VHW, though illiterate, maintains records with the help of a literate member of the family. She keeps record of the primary curative care she renders, maintains a list of all the pregnant women, records details of their antenatal care, type of delivery, the out-come of pregnancy and further the family planning status. She also maintains a list of children underfive, their weight cards and monthly weights, and immunization status. A record of eligible couples for family planning is also kept. The VHW also keeps the vital statistics of the village. She also keeps record of chronic diseases and their follow up. The VHW joins the mobile team in conducting case finding surveys. As an accepted member of the village she is able to get more accurate information on such subjects as abortions, infant mortality, maternal mortality, still births and deaths in general than the hired professional staff. The mobile team also maintains records according to the villages. The records pertain to the priorities of the project. They help the VHWs to maintain vital statistics. From time to time the mobile team along with VHW, Farmers Club members and visiting personnel conduct house to house surveys to evaluate their own work and to help improve their own programmes. Conclusion The following facts seem to emerge out of the Jamkhed experience to reach rural masses with health care: 1. Formulating clear-cut objectives and developing programmes accordingly. 2. Genuine grass root involvement of the people specially the weaker sections. 3. Availability of a primary health worker closest to the people at all times. 4. Team approach by the health professionals with proper referral system. 5. Par greater emphasis on social responsibility by professionals than ordinary technical service. 6. Willingness to learn from illiterate poor deprived masses and win their confidence. 7. Realization that health is not a priority of rural masses. 8. Health should be a part of total development.

Appendix I CRHP, Jamkhed

Population Surveyed Eligible Couples % practising, family planning Birth rate Infant mortality rata Immunisation status of under five Children Antenatal care

Project area 1971 Jan. 1490 269 2.5% 40 /1000 97 less than 1% less than 0.5% of pregnant women

1976 Jan. 1491 252 50.6% 23/1000 39 84% 78%

Non-project area 1976 Jan. 1405 244 10% 37/1000 90 15% 2%

Appendix-II CRHP, Jamkhed

1971

1975

1) ANC /PNC Care - Newly registered - Revisits 2) Deliveries - Hospital - Domiciliary 3) Family planning 4) Underfive clinics - Immunisation - Suppl. feeding programme

364 654 106 2

1476 10266 90 319

244

1239

7075 1150

3604 5400

5)-Tuberculosis - Cases followed up

6) Leprosy " " ” 7) Curative - diagnostic Services - Mobile Clinic (adults}

·.

4.

'…

-

1259

1139 -

5628

- VHW

-

3240

- OPD attendance at. Centre

-

18295

………………………………………………………………….

.

Comparison of the Doctor and the Village "Health Worker: David Werner. (In VHW, Lackey or Liberator) CONVENTIONAL DOCTQR

VILLAGE HEALTH WORKER

Class Background Usually upper middle class.

(at his best) From the peasantry.

How chosen by medical school for: grade point average; economic and social status.

By community for: interest, compassion, knowledge of community, etc.

Preparation

Mainly institutional, 12-16 years general

Mainly experiential.

schooling, 4-6 years medical training. Training concentrates on • physical and technological aspects of medicine, •and gives low priority to human, social, and political aspects. (This is now changing in some medical schools.)

Limited, key training appropriate to serve all the people in a given community: • Dx & Rx of important diseases • Preventive medicine • Community health • Teaching skills • Health care in terms of economic and social realities, and of needs (felt and long term) of both individuals and the community. •Humanization (conscientization) and group dynamics

Qualifications

Highly qualified to diagnose and treat individual cases. Especially qualified to manage uncommon and difficult diseases. Less qualified to deal effectively with most important diseases of most people in a given community. Poorly qualified to supervise and teach VHW. (Well qualified in clinical medicine, but not in other more important aspects of health care; he tends to favour imbalance; wrong priorities.)

More qualified than doctor to deal effectively with the important sicknesses of most of the people. Non-academic qualifications are: Intimate knowledge of the community. language, customs, attitudes toward sickness and healing. Willingness to work and earn at the level of the community, where the needs are greatest. Not qualified to diagnose and treat certain difficult and unusual problems; must refer.

Orientation

Disease I Treatment I Individual patient oriented.

Health I Community oriented. Seeks a balance between curative and preventive. (Curative to meet felt needs, preventive to meet real needs.)

Primary Job Interest

The challenging and interesting cases. (Often bored by day to day problems.)

Helping people resolve their biggest problems because he is their friend and neighbours.

Attitude toward the sick

Superior. Treats people as patients. Turns people into 'cases' Underestimates people's capacity for self-care.

ON their level.' Treats patients as people. Mutual concern and [interest because the VHW is village-selected.

Attitude of the sick Hold him in awe. Blind trust (or sometime. toward doctor or distrust). VHW

See him as a friend. Trust him as a person  but feel free to question him.

Conventional Doctor

VHW (At his best)

How does he use Hoards it. Medical Delivers ·services', discourages self-care, keeps Knowledge? patients helpless and dependent.

Shares it. Encourages informed self-care, helps the sick and family understand and manage problems.

Accessibility

Often inaccessible, especially to poor. Preferential treatment of haves over· have-nots. Does some charity work.

Very accessible. Lives right in village. Low charges for services. Treats everyone equally and as his equal

Consideration for economic factors

Overcharges, Expects disproportionately high earnings. Feels it is his God-given right to live in luxury while others hunger. Often prescribes unnecessarily costly drugs. Over prescribes.

Reasonable charges. Takes the person's economic position into account. Content (or resigned) to live at economic level of his people. Prescribes only useful drugs. Considers cost. Encourages effective home remedies.

Relative Permanence

At most spends 1-2 years in a rural area and then moves to the city.

A permanent member of the community.

Continuity of Care

Can't follow up cases because he doesn't live in the isolated areas.

Visits his neighbors in their homes to make sure they get better and learn how not to get sick again.

Cost Effectiveness

Too expensive to ever meet medical needs of the poor - unless used as an auxiliary resource for problems not readily managed by VHW.

Low cost of both training and practice. Higher effectiveness than doctor in coping with primary problems.

Resource Requirements

Hospital or health centre. Depends on expensive, hard-to-get equipment· and a large subservient staff to work at full potential.

Works out of home or simple structure. People are the main resource.

Present Role

On top. Directs the health team. Manages all kinds of medical problems, easy or complex, . Often overburdened with easily treated or preventable illness.

On the bottom. Often given minimal responsibility, especially in medicine. Regarded as an auxiliary (lackey) to the physician.

Impact on the Community

Relatively low (in part negative). Sustains class differences, mystification of medicine, dependency on expensive outside resources. Drains resources of poor (money).

Potentially high. Awakening of people to cope more effectively with health needs, human needs, and ultimately human rights. Helps community to use resources more effectively.

Appropriate (future?) Role

On tap (not on top). Functions as an auxiliary to the VHW. helping to teach him more medical skills and attending referrals at the VHWs request. (The 2-3% of cases that are beyond the VHW’s limits.) He is an equal member of the health team.

Recognized as the key member of the health team. Assumes leadership of health care activities in his village, but relies on advice, support, and referral assistance from the doctor when he needs it. He is the doctors equal (although his earnings remain in line with those of his fellow villagers).

***

Lingering Memories of the Year that has passed ................

Gowri and the International Year of the Child

Gowri does not believe it is the International Year of the Child (IYC). Silly girl. She does not believe that Mr. D.J Gooder was here the other day, all the way from the USA, to tell us to be kind to little children and not to exploit them this year. Gowri says her experience has been very different. Yesterday at play when she cut her foot and bled, all that the doctor did was to scold her for not wearing shoes. Where was the kindness? And her ayear-older-than-her brother, who runs errands for Peter (alias Raju) next door, came home with a bashed-up swollen face for not running fast enough. Gowri can be out of context. She listens intently when we tel1- her that this, year everything will be different. There are international, national and local committees made up of child-loving men and mostly women who will make this a particularly rewarding year. “For whom?” asks Gowri. There is no answer, so she continues,

"but are riot these the same people who work for prevention of cruelty to animals ?" We answer. "What' the diff .................. sorry, we mean how does it matter?" "Everyone loves a child", we tell her, "and this is going to be a very special, happy year for all the children of the world." Gowri beams for the first time. "Thanks;" she cries out in delight. "So you are going to like us. You will let more of us get born' and there will be more children to play with, promise?" That's not what we meant. ''You know Gowri," we smile indulgently, "we have just heard that during this wonderful year the government is considering to spend a big, generous sum of money for children. Now isn't that a nice gesture?" "Does that mean I will now be able to drink water without fear of contracting jaundice?" She asks seriously. "Of course not," we react. "Providing safe water is no simple matter. It will cost the government Crores of rupees to do this. Where do you think they could unearth that kind of money?" Gowri ponders. "I do not know," she replies, "But at least will I now be able to live with my parents in a house with real walls and a roof?" We feel it is our duty to set her values right. "You can be a demanding little girl, can't you", we chide. "Do you realise how impossible it is for the government to build houses for everyone. Be thankful for small mercies, little girl. At least you have a zopadpatti to sleep in every night. Have you ever thought of the several thousand pavement dwellers who have no place to go?" We fail to impress, so we try again. "Oh, cheer up child! In spite of all these limitations, there are lots of people in this world who are going to look after your interests. Not only our government but the United Nations and all its agencies like the WHO, UNICEF, FAO and the rest. Do you know how much money they have set aside for children this year?" "I am not interested in the amount", replies the impudent girl. "Just tell me this. Will I and all my friends now be able to get two regular meals every day?" "You must be joking! Don't be silly, Gowri. Do you know what that means? Enough food for 250 million children! India is not a rich country, you know, and there are so many mouths to feed. We do not know if you know this, but we are the second highest populated country in the world. And every year we add an Australia to our population." “What’s Australia?” Gowri asks indifferently. We are shocked at her ignorance. We tell her where



--> Australia is. She asks us if Australian children know about India. We say, "Of course! They learn about us in school." She shows us the door. "If it is really the International Year of the Child then, like all Australian children", she mutters, "we too should all be able to go to school." Gowri is typical of her kind. Terribly unrealistic. Always wanting more and more of the impossible.

* * *

We do not answer. We have other important work to do. 'We leave Borne posters with her and a list of agencies involved in the IYC. But she throws all this on the rubbish heap outside her hut. She does not believe us, the stubborn child. Will somebody please convince Gowri it is the International Year of the Child? S. NAIR Courtesy, 'Science Today'

* * * *

*

Dialogue Three Year Medical Diploma After a long lull Dialogue section of Bulletin is becoming lively again! (Bulletin 47-8). 'Three year Diploma Course' has provided the focal point of the dialogue. Abhay's juxtaposition of two sets of statements by Suhas, one in 1975 and another in 1979', is startling and even benumbing! Evidence appears to be overwhelming that Suhas has changed his position diametrically in last 4 years. There are two very different types of questions arising from Suhas's letter. 1. What is the substance in his new arguments? 2. Since Suhas has either overlooked the striking changes in his position or has chosen not to explain them, the question is why has he done so? Although personally I regard the first question more important, it is interesting to see that Abhay clearly regards the second question of greater importance. Even more interesting is his emphasis on the motivation and the alleged group interests of striking doctors, almost to the exclusion of the merit - what ever there may be - of the rival argument. For he says; 'therefore while opposing Three Year Diploma Course, what is your logic is not of much importance, (but) what is more important is your intention and outlook behind .it,' (my translation!) Perhaps in the specific context of Suhas's letter this approach is understandable. All the same it raises a crucial and more general issue, which I think has a bearing on the level and quality of debate we hope to develop in the Bulletin eventually. How far is it a sound practice to counter an argument by analysing the motives and interests of the author of the argument! This practice of referring to class I group interests is very much in vogue and apparently it is considered an irrefutable argument. It is necessary to examine this 'argument’ more closely. I t proceeds like this: In any existing society there are always classes or groups with their respective interests. Every person belongs to one of the classes. Member of each class knowingly or unknowingly always tries to protect, preserve, and further his class interests. This necessarily colours his opinions, viewpoints, and arguments. Thus objectivity is impossible to achieve. And now comes the crux. As the objectivity is impossible there is no rational way one could judge the relative merits of rival arguments when they happen to reflect the view points of rival classes! The only thing one can do h to analyse and to point out the class I group origin of the argument. The argument may stand refuted, if it is shown that it arises from reactionary non progressive- class's view point! I think this not only wrong argument but also

is to point out its class origin and there is no need of further arguments! No this is not correct. We must always answer any argument, even most disagreeable, by argument' based, on the facts as best as we can see them.

Anil Patel Rajpipla

Editorial Committee:

Anil Patel, Binayak Sen, Kamala Jaya Rao, Luis Barreto, Vidyut Katgade, Abhay Bang (EDITOR)

Views & opinions expressed in the bulletin are those of the authors and not necessarily of the organisation


