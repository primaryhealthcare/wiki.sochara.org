---
title: "The Trans-Technique Aspects Of Disease & Death"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled The Trans-Technique Aspects Of Disease & Death from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC145.pdf](http://www.mfcindia.org/mfcpdfs/MFC145.pdf)*

Title: The Trans-Technique Aspects Of Disease & Death

Authors: Kothari M.L. & Lopa A. Mehta

145 Medico Friend circle bulletin November 1988

The Trans-Technique Aspects of Disease and Death M. L. KOTHARI AND LOPA A. MEHTA

The technological triumphs of this century outweigh and outclass the aggregate achievements of the entire human past; yet modern man has been denied the elixir of cure for his disease and death. A logical survey of medicine's failures reveals that it is not that the technology per se is ineffective but that what technology has solved is trivial, and what it just can't touch is crucial, being beyond any technique. Most of human disease and death is trans-technique. Technique in medicine is whatever a diagnostician, therapist or researcher does to a patient. Trans-technique aspects of disease and death are those innate, ordinary, dayto-day features of human living and dying that Ie technique can in no way modify to a patient's advantage. Seemingly technology has reached its apogee, its Ultima Thule; the march is unimpeded, for the would-be-obsolete CT scan is likely to be replaced by the could-be-obsolete NMR imaging. However, a dispassionate, epistemologic evaluation of medicine's gains reveals them to be imagery, accessive, analytic associative, and amplificatory. The more the physicistic science and the physicianly art interact, the greater is the variety in which medical imagery can be obtained. Yet, to take but an example, roentgenography, xerography, CT scan, ultra-sonography and NMR imaging have left a cancer where it was-diagnosed always a little too late. The ability to cannulate the pancreatic duct/artery towards the diagnosis/treatment of pancreatic cancer is an

accessive advance that leaves the cancer's autonomy untouched. Increasingly refined biochemical techniques allow many a substance to be measured with pico- precision, thus analytically telling us a lot about heart attack diabetes mellitus or rheumatoid arthritis, but without the liberty to predictably/ and /or favourable alter the course of the disease. Epidemiology connects the husband's cigar to the wife's cancer, coffee to cardiovascular disease, and HLA-antigen to a host of maladies -an associative exercise that makes more anxiety than sense. The E/M amplifies the size of a T-Iymphocyte n-tuple times only to amplify our ignorance on the cell to the same magnitude. In the modern medical setting, technology glitters, but is, often not gold. A quartet of diverse medical men, (10, 17, 19, 27) in its recent generalization, is not far from the truth that 90% of the bad things that happen to man are beyond the ken of modern medicine. Armed with technical might, the doctor can, with vis medicatrix natura providentially at the patient's beck and call, revert to eu-states acute physiologic crises, set fractures, fix retinae, deliver babies facing dystocia, remove lumps and cataracts, replace a valve or a joint, correct mechanical defects such as cleft lip or hernia, all this comprising the 10 per cent of man's maladies that medicine can manage, The rest is trans-technique. Let us see how and why.

Systemicity Four biomedical factors account for the trans-technique scenario that we are witnessing. These are Cellularity, Systemicity, Uniqueness and Heredity. An integrated appreciation of these factors will help us understand medicine's limits, no matter what its technical might.

The human body is a holon that starts as a single cell, and sui generis, builds up a cytogalaxy that behaves as a single, concerted unit whose seemingly disparate parts form, grow, and decay in unison.

Cellularity: Celldom Unconquered

Acknowledgedly, cancer is a disease of the whole organism. (22) In the brain, where-from cancers usually do not spread, it is a disease of the whole brain. A cancer thus does not lend itself to complete destruction by surgery, radiation, chemotherapy or immunotherapy. Even if we were to nab the last cancer cell, (26) the next normal cell would foil the attempts by turning cancerous, through a process named neo-canceration or recruitment. (I3) The sole curative triumph against gestational choriocarcinoma is due entirely to the fact that such an eventuality of neocanceration is ruled out by the absence of the normal progenitor cells that comprise the discarded fetal part of the placenta. The appellation, a disease of the whole organism, is no less applicable to any form of blood vessel disease, be it the coronaries or the cerebrals. A bypass takes care of the block that the operator sees or has access to, but what of the vessels beyond, or before, or elsewhere. This explains why a patient of angina shows normal coronary arteriogram and the patient having normal coronary arteriogram can die a sudden coronary death. (23) As for diabetes, euglycemic agents touch the proverbial tip of the metabolic iceberg, affecting in no way the generalized, accelerated vaso-occlusion that is now an accepted part of the diabetic process.

There are features of a mammalian cell that make disease and death trans-technique in more ways than one. It is a fitting paradox that what advanced cytologic techniques have revealed about the cell has snowballed to drive home the truth that a cell's behavior, in health or disease, can hardly be trifled with. The micro size of a cell accounts for the fact that before a scan discovers a cancerous lump measuring one cubic mm and weighing one mg.,-the smallest tumor mass that one could ever hope to detect clinically-the cancer is already a million cells strong and several years old; early diagnosis of cancer is only a myth. (13) The same considerations apply to an atheromatous plaque, held by some as mitotic in origin. 21 Even if we end up with a scanner that can spot a single wayward cell, the latter could mislead by exhibiting the Shakespearean repertoire of looking benign despite malignant intentions and vice versa. Any attempt at flooding the body with anti-abnormal-cell-agents (radiation, chemicals) fails because of the selfsameness of all body cells, rendering selective destruction of undesired cells impossible, Supposing that a highly specific drug is developed and administered, the target cell can recall its microbial past to readjust its genetic machinery-mutate-to KO the drug, the mutative repertoire of a human cell bordering close to 256 followed by 2.4 billion zeros. Transplanted, the guest cells refuse to merge their identity, their unique selfishness as much as the host cells and all hell is let loose. Each of our body cells carries in its bosom a decision in advance of performance. (6) The thousand inner shocks that the flesh is heir to are indelibly programmed into it ab initio; all that we see is an unfolding of a built-in story with the flow of time.

Uniqueness Variability it's said is the only invariable law of biology, a natural propensity that unfailingly varies one cancer from another one heart attack from the next. If the uniqueness of every individual is an unsolved problem of biology, (20) than uniqueness of every disease is the unsolved/ unsolvable problem of medicine: “There are,” Carrel axiomatized "as many different diseases as patients." (2) The presumed identicality of the genotype in homozygous twins is unable to circumvent the Carrelian code, a nosologic non-concordance that is well-known but as yet poorly accounted for.

Cancer, indisputably traceable to precisely pinpointable/culturable culprit cancer cells, provides a remarkable example of what Dubos (5) would call the unprecedented, unparalleled, and unrepeatable nature of a disease. Writing; on the 'Uniqueness of malignant tumours,' Spriggs and coworkers (25) concluded that naturally occurring cancers are extremely diverse even when they carry the same diagnostic label. No two cases of coronary artery disease/stroke/diabetes/arthritis/autoimmune disease are identical either in their presentation or in their progress. The behavioral uniqueness of a disease, with its unpredictability, forms the basis of unexpected successes and the equally unexpected failures, given the same treatment. Cancers have been classified into good and bad, the good ones curable by any treatment, the bad ones by none-a retroactive judgment (13) applicable to any other disease and fully justifying the Chinese proverb that a therapy works in a" patient 'destined' to survive. Vis-a-vis the celebrated and honored practice of prognosing, what the doctors know are group statistics but when it comes to a disease in an individual, the physician has to contend with unknowns stacked upon unknowns, a situation that merits the title of a book by Francisco Sanchez, published in 1581: Quod Nihit SciturNothmg Can Be Known. In a recent study, in Bombay, of 535 sudden coronary deaths, 66% of the cases did not show coronary occlusion and 78% failed to show any [myocardial infarct; in many of these, the heart was too good to die. (23) And what of diabetes with the SMA-12 at the behest of the clinician I It is true in diabetes mellitus as in other chronic diseases that the prognosis for the patient is extraordinarily individual. (15) The unique reality of medical practice is that, be it Paul Dudley White and his patient Chales Thierry, on James Herriot and the dog Jock, it is a one-to-one encounter where the uniqueness of the individual, his disease, his very biologic trajectory is unpredictable, unalterable, and overwhelmingly important. For modern medicine, the most chastizing part of an individual's biologic trajectory is its refusal to provide any quantitative correlation ship between the earliness/lateness of a disease on the one hand, and the probability of the disease/death it may beget, on the other.

The healthy do not necessarily survive; the diseased do not necessarily die.

Herdity Herdity could well be described, at the very outset, as a corporate programme subserved by individual performance. Cellularity, Systemicity and uniqueness are features innate to an individual; herdity is a force that the human herd exerts on the individual. The individual-herd relationship is a remarkable biologic feature that more than vindicates John Donne's intuitive generalization that no man is an island of it; every man is a part of the main. As Dobzhansky (4) put it, mankind was and is a single inclusive Mendelian population and is endowed with a single corporate genotype, a single gene pool. Apposite to this is Carrel's description (2) of an individual as one who extends, in time as in space, beyond the frontiers of his body, and who is' linked to the past and to the future, regardless of the ephemerality of his present. Add to this, the conceptual framework of quantum physics that reveals a basic oneness of the universe wherein at a deep and fundamental level, the seemingly separate parts of the universe are connected in an intimate and immediate way, in a complicated web of relations between the various parts of the whole, (1, 28) We are now poised to view an individual's body, his disease, his cancer-each unfailingly unique-as a spatiotemporal manifestation of a cosmic order. I am what I am, and allowed to be so, for I know who all others were, are, and will be so as not to duplicate them, and they in turn know of me so as' not to make a duplicate of me or of my disease at any time. Climbing down from cosmic considerations to clinical reality allows us to appreciate the role herdity plays in the distribution of disease in any given group. As the general statistics go, the incidence of, say, acute lymphatic leukemia is 1 in 33000, of cleft palate/neural tube defect is 1in less than 1,000, of cancer 1 in 5, of blood vessel disease 1 in 2, at random, country after country, year after year. “Anybody who spends a little time brooding over the statistics of cancer must be struck by their unexpected constancy. From year to year the figures for each form of cancer show remarkably little variation.”

The evolution of the concept of polygenic inheritance has brought a shift in genetic thinking, from heredity to herdity, for polygenic inheritance is necessarily a statistical concept that concerns not the individual but Mendelian populations or population aggregates. (4, 7) Polygenic inheritance has been invoked to explain a wide variety of diseases, ranging from congenital malformations to cancer, porphyria to peptic ulcer. This means that most diseases do not have a cause. Causeless diseases cannot be prevented; they are an integral part of man’s growing; causewise and course-wise they are transtechnique. Herdity is trans-technique.

I

AN ILLUSION

Having so generalized, Glemser (8) cites figures: "Here there are 5,355 cases of cancer of the pancreas one year, 5.427 cases of the cancer of the pancreas two years later-almost the same number. Or in another country, there are 218 cases of cancer of pancreas one year, 221 cases of pancreas the following year." These regional constancies and interregional variations merge into a constant, global, human character when it is realized that although the anatomic distribution of cancer in different parts of the world is extremely varied, the overall death-rate from cancers at all sites is remarkably constant for humans the world over. (24) The age-specific mortality rates from cerebrovascular disease, year after year, decade after decade, and country after country "fit quite closely the same line (10)" There is something fundamentally human in the global impartiality with which disease and death treat mankind. The prevalence of diabetes mellitus (18) is more or less constant for all countries. Cancer, stroke, diabetes, hypertension, heart attack and so on are an integral part of humanity, of human herdity. This remarkable herd-certainty and individualprobability of pathologic events is a function of a corporate herd programme that finds expression at the level of an individual who has crossed a critical genetic threshold. (3) Herdity, thus, is a reciprocal relationship between an individual and his herd, what geneticists have been describing as polygenic inheritance.

The noumenon of herdity governs all the phenomena in relation to disease and death in a herd. The herd determines who will get what and when, in whom the disease will be slow, in whom fast, and so on. This would explain why the commonness of prostatic cancer beyond the age of 50 is paradoxically matched by the uncommonness of its malignant behaviour and how persons with bad coronary angiograms out live those with good ones. The most compelling evidence in favour of herdity is, in general, the programmed herd mortality that, as a physiologic function, (14) is seen in man, in animals in drosophila. Gompertz (9) saw this as a constant increment in mortality beyond the fifth quinquennium of human life, doubling every 8 years, a phenomenon no medical advance has been able to stem. John Knowles, as President of The Rockefeller Foundation, wrote in 1977 on "The responsibility of an individual" (12) charging the latter's "personal misbehaviour and environmental conditions" for over 99% of illnesses. Knowles' faith in reasoned behavior did not prevent the pancreatic cancer that killed him in 1979. He was but one of the 19,000 that develop pancreatic cancer and die from it in USA, every year. Knowles died at 52 some do at an earlier age, others at a later age, all a part of herd distribution, of herdity. That human herdity has been exercising such influences from times immemorial may be realized from the fact that King Herod of Judea, died of pancreatic cancer, in 73 B. C., at the age of 69. Cancer as a transtechnique problem has been curing itself of research and researchers, and may one day eliminate them altogether.

Conclusions Systemicity, Uniqueness, Cellularity and Herdity of diseasing and dying can be read as the latter's SUCH - ness, a Kantian ding-an-sich or as the Zennist Alan Wats summed up. This is it, the evolution of the trans-technique concept, based on SUCH- ness, explains technology's failures and limits and exercises restraints on this age of inflated expectations, encourages us to be radical enough to abjure straight-line solutions and many a technologic trap-to wit, the tyranny of massscreening, debilitating therapies, or killjoy preventionism. Jacob-Bigelow lamented in the last century, that most men have an exaggerated opinion of the powers of medicine. A recent editorial (11) title "The toss-up" bears eloquent testimony to the rationale of the foregoing. It is common experience that on a given case the proposed diagnostic, therapeutic thrust ranges from medical conservatism to surgical ultra radicalism. After attributing such divergence in medical thinking to the idiosyncrasies of the physicians, the authors propose: "Perhaps all these factors are involved in clinical controversies but we propose that one explanation has not been sufficiently recognized; that it simply makes no difference which choice is made. We suggest that some dramatic controversies represent ‘toss-ups’-clinical situations in which the consequences of divergent choices are, on the average, virtually identical." The identicality of the consequences no matter what the investigations and the therapy, is a function of the basic fact that the problem being talked is beyond the limits of technology. Scientia est potentia: knowledge is power. The knowledge that a lot in medical practice is beyond medical technique can as a concept propel us towards not doing in medicine. Munsif, an eminent Bombay surgeon, was fond of aphorizing that a good surgeon is one who knows when not to operate. What a medical man needs to learn, in today's technicalzed scene is when not to act, an intellectual and a therapeutic revolution that can safely rest on the concept of trans-technique. REFERENCES

1] Capra. F.: "The Tao of Physics". Bantam Books, New York. 1977. p. 57.

2] Carrel. A.: "Man. the Unknown". Mac Fadden Publications, New York 1961. 3] Carter. C. O.: Genetics of common single malformations. Brit. Med. Bull., 32: 21-26. 1976. 4] Dobzhansky, T.: "Mankind Evolving". Yale University Press, New Haven & London, 1967.

5] Dubas, R. : Foreword. In, "So Human an Animal." Charles Scribner's Sons. New York 1968. P,7.

6] Foulds, L.: "Neoplastic Development". Vol. I. Academic Press, London & New York, 1969. 7] Gardner, E. J.: "Principles of Genetics". Wiley Eastern Private Ltd 1968. 8] Glemser. B. : "Man Against Cancer". Funk and Wagnalls, New York. 1969. 9] Compertz, B... On the nature of the functions expressive of the human mortality and on a new mode of determining life contingencies. Phil. Trans. Roy. Soc, (London), Ser. A., 115: 513, 1825. Quoted by Kothari, M. L. and Mehta, L. A.: Treatment of Cancer. In, "The Nature of Cancer". Kothari Medical Publications, Bombay, India, 1973, pp. 515-837. 10] Ingelfinger, F. J.: Arrogance. New Engl. Me.,' 303: 1507-1511, 1980. 11] Kassirer, J. P. and Pauker. S. G.: The toss-up' New Engl... Med' 305: 1467-1469, 1981. 12] Knowles, J. H.: The responsibility of the individual. In, I Doing Better and Feeling Worse: Health in the United States." Editor: J. H. Knowles, W. W. Norton and Company. New York, 1977 pp. 5780 13] Kothari, M. L. and Mehta, L. A.: Cancer; Myths and Realities of Cause and Cure". Marion Boyars, London. 1979, 14] Kothari, M. L. and Mehta, L. A.: The transscience aspects of disease and death. Perspect. Biol. Med., 24: 658.666. 1981. 15] Krall, L. P.: Clinical evaluation of prognosis. In, "Joslie's Diabetes Mellitus". Editors: A. Merble, P. White. R. F. Bradley and L. P. Krall., Lea & Febiger, Philadelphia, 1971, pp. 211.216. 16] Kurtzke, J. F.: "Epidemiology of Cerebrovascular Disease". Springer-Verlag, Berlin, 1969. 17] Lipkin, M : "The Care of Patients-Concepts and Tactics ". Oxford Univ. Press, New York and London, 1974.

Computers in Medicine (Lancet, December 26, 1987)

TIME SAVED, FOR WHAT? THE LITTLE PRINCE AND THE MERCHANT (1)

"Good morning", said the little prince. "Good morning", said ~he merchant. This was a merchant who sold pills that had been invented to quench thirst. You need only swallow one pill a week and you would feel no need of anything to drink. "Why are you selling those 1", asked the little prince. Because they save a tremendous amount of time', said merchant. "Computations have been made by experts. With these pills, you save fifty-three minutes in every week:” "And what do I do with those fifty-three minutes?" Anything you like.' "As for me", said the little prince to himself "if I had fifty three minutes to spend as r liked. I should walk at my leisure toward a spring of fresh water," 1.

Desaint-Exupety A. The little prince (translated from the French by K. Woods). New York: Harcourt, Brace & World, 1943: 73-74.

THE SICK LITTLE PRINCE & THE COMPUTER (2) "Good morning", said the sick little prince. "Good morning", said the computer.

This was a smart computer that had been programmed to take a complete medical history. "Why are you questioning me, instead of the doctor?” asked the sick little prince. "Because, thanks to me, the doctor can save a tremendous amount of time. Calculations have been made by experts that I can save the doctor one hundred and fifty-three minutes a day." "And what does the doctor do with those one hundred and fifty-three minutes?" "Whatever he likes.” "As for me", said the sick little prince to himself "if my doctor had one hundred and fiftythree minutes to spend as he liked, I would like him to spend some of them talking to me:' Faculty of Medicine and Rappaport Family Institute for Research in the Medical Sciences. ROSALIE BER Technion-Israel Institute of Technology. POB 9649, Haifa 31096, Israel

2. Ber R. The little prince and the computer (in Hebrew). Harefuah 1983, 105:413.

REFERENCES: Continued 18] Malins, J.: "Clinical Diabetes Mellitus," ELBS and Chapnan & Hall, London, 1975, p. 47. 19] Malleson, A.: "Need Your Doctor Be So Use less?" George Allen and Unwin, London, 1973. 20] Medawar, P. B.: "The Uniqueness of the Individuals". Methuen, London 1957. 21] Peto, R.: Epidemiology, multi-stage models & short term mutagenicity tests. In, "Origins of Human Cancer". Editors: H. H. Hiatt, J. D. Watson, and J. A. Winsten. Cold Harbor laboratory, U.S.A., 1977, pp. 1403-1328. 22] Roe, F. J. C.: Cancer as a disease of the whole organism. In, "The Biology of Cancer" Editors. E. J. Ambrose and F. J. C. Roe., D. Van No strand, 1966, pp. 1-32. 23] Shah, S. J.: "Layer Concept of Coronary Heart Disease". States' People Press, Bombay, 1980.

24] Smithers, D. W.: "Clinical Prospects of The Cancer Problem". F. & S. Livingstone, Edinburgh and London, 1960.

25] Spriggs, A. I., Boddington, M. M. and Halley, W.: Uniqueness of malignant tumours. Lancet, 1:211, 1967. 26] Wiicox, W. S.: The last surviving cancer cell; The chances of killing it. Cancer Chemotherapy Rep., 50: 541-542, 1966. 27] Wildavsky, A.: Doing better and feeling worse; The political pathology of health policy. In, "Doing Better and Feeling Worse: Health in the United States". Editor: J. H.Knowles, W. W. Norton and Co., New York, 1977.pp.105-123. 28] Zukav, G.: "The Dancing Wu Li Masters: An Overview of the New Physics". Bantam Books, New York, 1979, p. 82.

BEWARE OF X-RAYS THE DANGEROUS DIMENSIONS OF DIAGNOSTLC X-RAYS. X-RAYS ARE HIGHLY HAZARDOUS: HANDLE THEM WITH CARE. IMPROPERLY USED X.RAYS ARE DEATH RAYS \~,

Dr. S. G. KABRA

It is plain to even the most casual observer that "Diagnostic Clinics" or X-ray Clinics" pepper the streets of urban, and a good deal of semiurban, India. More often than not these clinics are independent of hospitals and have made X-ray facilities as readily available as photocopying services. In fact, there has been a phenomenal increase in the number of diagnostic X-ray units in the country. From an estimated 10,400 units in 1975, the number now exceeds 30,000, according to a report of the Bhabha Atomic Research Centre (BARC). This proliferation of X-ray services is apt to be regarded as "a sign of progress" and public awareness of X-rays is such that an X-ray examination offers a tremendous psychological boost to a patient much the same way as an injection would. It is not infrequent for a patient himself to suggest to his treating physician that he be X-rayed and the compliance of the physician in this respect assures the patient that his problem is receiving serious attention!

Only individuals in the younger age groups are considered since the effects of radiation are manifest only decades after exposure. Of the 9 Crore persons X-rayed in the country in any given year, roughly 55% or 5 Crore are 30 years of age or less. * Estimated number of diagnostic X-ray units in India * Presumed (for the sake of calculation) that the number of patients X-rayed per day per unit * Number of patients X-rayed: per day

30.000

10

30.000 x 10 300,000

* Number of working days in

300

a year

What is not generally known is that diagnostic X-rays today constitute, as a whole, the biggest manmade source of ionising radiation, whose potential for harm equals that of ionising radiation emanating from a nuclear explosion. Judiciously and safely used X-rays are, of course, of immense value I in modern medicine. Used otherwise, X-rays cause more harm than good not only to patients, but also to the public at large and their future generations. Ionising radiation in any form and in any dim increases the risk, in exposed persons, of developing cancer and their progeny of being born malformed. Risk Estimates from Diagnostic X. rays For estimating such, the population X-rayed in ONE YEAR 'is taken into account. It is presumed that these persons will receive X-rays only once in their lifetime.

* Number of patients X-rayed: 300,000 x 300 per year — 90,000,000 * Number of such patients aged: 55%of 90,000,000 30 yrs or less 50,000,000

-

Below are the estimates of cancers and congenital anomalies that would occur in these 5 Crore persons in their lifetime and their progeny respectively over and above what would have occurred had they not been X-rayed. Since the risks vary according to the type of X-ray and the part of the body exposed, the different types of Xrays that would have been carried out on these 5 Crore people are also presumed in these calculations.

No: Persons Type or Diagnostic X-ray

No: who will develop cancer

X-rayed

Bone

Breast

(Lakhs)

Marrow

(Women)

Additional Lung

No : Hereditary Defects via Mother Father

Plain Chest and Ribs

130

10

39

26

10

10

Plain Abdomen

120

72

12

10

576

288

Barium Meal

30

39

IS

2

120

IS

Barium Enema

30

45

6

24

162

51

Oral Cholecystography

40

12

4

3

10

2

IVP

SO

25

135

7

500

200

Hip and Femur

50

25

3

3

22L1

900

Lumbar Spine

50

40

30

7

380

120

TOTAL

500

268

244

82

1978

1586

Additional Cancers Additional Hereditary Defects

594 : 3586

NOTE: These estimates are based on the following sources: (I) UN Scientific Committee on the Effects of Atomic Radiation, Report to the General Assembly, United Nations, NY, 1977 (II) Diagnostic Radiography: What are the risks 1 Drug and Therapeutics Bulletin 1980; 18.49-50 The above estimates are for X-ray machines which are properly maintained and monitored, and operated with all the prescribed safety precautions. The situation in our country, however, is quite the opposite, according to a report of the Division of Radiological Protection (DRP) of BARC. The DRP entrusted with the task of surveying X-ray units in the country to detect, and advice about, radiation hazards, found it impossible to keep up with the explosive increase in the number of these units. Though started in 1957, DRP had, till 1976, surveyed only 9%, i. e. 946, of the estimated 10,400 X-ray units in existence at that time. The situation has only worsened over the year. The Atomic Energy Regulatory Board (AERB) constituted by the Government of India in November

1983 is now estrused with the responsibility of developing and implementing appropriate regulatory measures aimed at ensuring radiation safety in all applications involving ionising radiations envisaged in the Atomic Energy Act, 1962. The status of radiological protection in small hospitals and clinics that constitute 95% of the total diagnostic units in the country "is so poor that most of the excessive exposure cases are reported from them" says the survey report of DRP. The causes of excessive exposures detected are improper layout of these installations (85% of the small hospitals and clinics surveyed and 60% of the large hospitals), and lack of protection measures in 25% to 90% of the small units.

The report says: "in short, the unawareness of radiation hazards, inadequate plan of installation, poor performance standards of the equipment, lack of necessary protective devices and above all a commercialization of the use of X-rays for medical diagnosis and consequent indiscriminate use" are some of the major factors leading to unwanted exposures. He RESULTS of the prevalent improper use of diagnostic X-rays are: —

Because of scattered radiation, for the 9 Crore patients X-rayed every year in the country, there would be, at least, 18 Crore others who are exposed to radiation unnecessarily. The risks of cancers and congenital anomalies to the unnecessarily exposed health people are similar to that of the patients. The continuing proliferation of "X-ray shops" in busy markets and public places are highly hazardous to say the least. Their potential danger and resulting harm should not be underestimated or, worse still, overlooked.

1.

Fantastic Risk from Bad Units

Excessive radiation dose to the individual to be X-rayed.

2. Radiation of parts of the body other than those

required to be exposed to X-rays. 3. Radiation to persons other than the patient. The "WHO Manual of Radiation Protection in Hospitals and General Practice" prescribes clear cut guidelines for X-ray personnel (appendix) and these norms have also been prescribed by the AERB in its safety booklet for diagnostic X-ray units (AERB Code No: SC/MED-2 Safety Code for Medical Diagnostic X-ray Equipment and Installations). These safety measures are almost universally ignored in India. Though the flouting of these basic safety norms is so open and obvious, it is most unfortunate that it does not attract the attention of the health authorities. Scattered X-ray Hazards to the General Public When an individual is being X-rayed, a significant part of the beam is scattered in all directions. These scattered rays affect not only every individual inside the room but pass out of the room because of their great penetrating power, and affect every individual outside and in the vicinity of the unit, unless the scattered X-rays are prevented from escaping the room by thick masonry walls and lead-lined closed doors and windows. Under the prevalent conditions and work culture in the X-ray units of the country, many more individuals than the patients X-rayed are exposed to ionising radiation escaping from these units. Everyone who accompanies the patient receives small doses of radiation many times during their stay inside or in the vicinity of the X-ray room.

The genetic risk from an improperly operated X-ray unit when compared to a properly managed one is colossal. To quote a WHO document; " The conclusion to be drawn from these results is quite fantastic; they imply that a single examination performed on person using a bad technique has the same genetic consequences a9 the same examination performed using a good technique on 22,000 persons of the same sex and life expectancy."

Genetic Hazards The genetic effects of X-rays are randomly distributed and their clinical consequences are late in occurrence. The changes they produce in germ cells (sperms and ova) are either chromosome mutation or gene mutation. While chromosomal mutations are mostly lethal for the germ cells, their only effect is a reduced birth rate. However, some of these of chromosome mutations lead 10 very serious hereditary diseases in the first generation. Gene mutations or point mutations are "microscopically invisible changes in the structure of DNA, the chemical substance responsible for heredity. The majority of point mutations are recessive, and a hereditary diseases connected with such a mutation will become evident only when two germ cells (a male and a female) bearing similar mutations unite by chance. The chance of this event obviously depends on the mutations present in the 'genetic pool' rather than the mutations in the germ cells of an individual person. The genetic pool includes germ cells of all individuals in the population who have a given probability of becoming parents.

Person who for various reasons (age, etc) are not expected to have children are not considered to be part of the genetic pool, and their radiation exposure is neglected as a component of genetic radiation exposure" (WHO manual on Radiation Protection). Gene mutations, because of their serious deleterious effects on the next and subsequent generations are, therefore, the matter of "highest public concern".

No Dose is Safe

and the serious consequences of their improper use are equally well established. Health authorities neglect the latter to the peril of the general public. The state governments are expected and required to appoint appropriate Radiation Protection Committees to monitor and supervise the functioning of diagnostic X-ray units in the state. There has to be a Radiation Safety Officer (RSO) for every diagnostic X-ray unit under the mandatory provisions of the Radiation Protection Rules (I97I). The AERB Safety Code lays down the qualifications, certifications, duties and responsibilities of the RSO.

X-rays, as they pass through living cells, produce radicals that act as strong oxidizing or reducing substances. "Thus even small doses of radiation disturb the delicate biochemical equilibrium of living tissues and must be considered damaging".

At an informal gathering of press persons and interested members of the public in Jaipur recently the problem of X-ray units operating in an uncontrolled manner was discussed. The response was varied- from curiosity to outright horror. Nevertheless, this meeting did help to publicise, to some extent, the magnitude of the problem. The The conclusions here are plain and consumer, as it were, can be excused for not unambiguous. "I n the present state of knowledge immediately sensing the gravity of the situation it must be assumed that the point mutations and his ignorance of his right to safe medical produced by radiation have a linear dose/effect diagnosis and treatment. After all, X-rays cannot relationship, without threshold, recovery or 'tole- be seen and there are never any immediate side rance dose', and with unrestricted accumulation of effects; the chances of tissue damage are small so all doses, even very small ones, received by the that, in all probability, he will never be affected. genetic pool". This is a most unfortunate and incorrect attitude. Damage occurs years later, when the memory of The seventy of X- ray damage, viz. carcino- an earlier X-ray examination is lost so that the Xgenesis and gene mutation is not dose dependent. ray itself is unlikely to be incriminated as the Even very low doses carry the same risk. culprit. Going even further, it would be a difficult However, the magnitude of the risk increases with task indeed, for congenital defects in future genethe dose, and the effect of every subsequent dose rations to be ascribed to "that X-ray that my great is cumulative. grandfather had". The villain in the Bhopal tragedy was easy to pin down and the Indian public still The benefits of diagnostic X-rays are smells blood because methyl isocyanate's aweundisputed. The potential for harm from radiation some effects were there for all to see. X-rays may be silent but they are no less lethal.

What can you, the public, do?

1. Do not suggest an X-ray to your doctor. Let him decide. 2. Whenever you are asked to have an X-ray carried out yourself, ask your doctor the reason for the same. Ask for a safer alternative. An ultrasound of the kidneys, for example often provides as much or sometimes even more information than an intravenous pyelogram. 3. If you are pregnant, do not allow yourself to be X-rayed unless there are very, very strong grounds for it. If your abdomen is to be X-rayed then have this done within 10 days of your period so that an early pregnancy is not inadvertently jeopardised. Males should insist that a lead shield be provided to cover their external genitals when their abdomen is being X-rayed. 4. Shop around for what you have heard is a reliable X-ray centre. If the attending radiographer or radiologist does not carry a film badge in full view, he can't be careful for his safety, much less yours. (A film badge is a small rectangular multi-coloured item that measures the total amount of radiation received by X-ray personnel. It is monitored regularly by BARC to ensure that personnel who are working with ionising radiation receive doses well within prescribed limits.) 5. Do not accompany your relatives into the X-ray cubicle. An X-ray unit is expected to have staff for purposes of supporting the patient, though on occasions it does become necessary for a parent to accompany a small child who is obviously scared stiff. Under no circumstances should you enter if you are pregnant, even to help out. Let someone else do so. If you have to be present with the patient in the X-ray cubicle, insist upon being provided a lead apron to wear. 6. Do not loiter outside X-ray units. Scattered rays can also kill. 7. Find out if the X-ray unit has a Radiation Safety Officer (RSO) on its rolls. They are required to, under the Atomic Energy Act. 1962. If there is no RSO covering the unit, a complaint can be made in this regard to the health authorities and the State Government.

Clinical Perspective: Chest Radiography SP KALANTRI A modern medical student, born and brought up in era of increasing radiological sophistication has started turning a Nelson's eye to the traditional bedside methods of making a diagnosis Perhaps he should not be blamed. The current culture in the academic institutions and private practice is steadily reducing his confidence on the utility of bedside signs. Even quite a few postgraduate teachers have seriously been toying with an idea of providing chest X. rays to the examinees in their long and short cases. Patients are also sharing the belief that short of being radiographed, their examination is highly incomplete. This overuse of diagnostic radiology, almost global now, the undue popularity of the Roentgen's rays and a gradual erosion of faith on the Laennec's tubes should call for concern. I made an attempt to go through the available literature and to put the chest X-ray in its proper perspective so far as chest diagnosis is concerned. In this task I have greatly been helped by a technical report series published by WHO in 1983; (1) its basic theme centres on the rational use of radiology in all disciplines of medicine. In the preparation of this article, this report has extensively been referred to. The questions to which I sought answers were: (i) Is routine CXR helpful in screening asymptomatic subjects? (ii) What is the utility of CXR in the overall assessment of respiratory diseases? (iii) Why chest X-rays are being over utilized? The criticisms that appear in parenthesis after some of the recommendation of WHO Expert Committee are my own. Routine Chest X-ray in asymptomatic population

I. Routine chest X-rayon admission to the hospital: Feingold (2) surveyed 39000 hospital admissions, the majorities of patients were elderly, chronically ill, poor and came from a population with a high incidence of tuberculosis. He concluded that if were no symptoms referable to chest and no fever, no tuberculosis was' found. There were few other significant abnormalities that could “have been detected by clinical examination'.

2. Routine chest X-ray in pregnant women.' The WHO Expert Committee thinks that unless there is high incidence of clinically silent chest disease, routine CXR has no role whatsoever in pregnant women. In support, it quotes a massive study of 12000 women by Bone brake, (3) in which not one patient with clinically unsuspected disease could be detected. 3. Preoperative chest X-ray.' Does preoperative chest X-rays, as is commonly believed affect the decision to operate, change the type of anaesthesia, and provide a useful baseline film before operation? Yes, says Sane, (4) who studied a series of children and found that in 3.8% the results of preoperative chest radiography changed either the anaesthesia or the type of treatment. Milne also considers them essential for comparison with a postoperative film if a patient develops a postoperative complication. There are, however, some large studies, which do not share this belief. The Royal College of Radiologists, (6) in a survey carried out in England, Wales and Scotland concluded that. CXR should be used as an adjuvant to careful clinical evaluation of the patient and should only be done when it is thought that they will provide additional useful information, Lloyd Rucker recently did a study (7) where patients were drawn fr9m almost all major surgical specialties. He proposed that certain risk factors would increase the likelihood that a patient's

Contributors MANU KOTHARI and LOPA MEHTA are Professors of Anatomy at GS Medical College; Bombay. SG KABRA is a director (Research), Santokben Durabhji Memorial Hospital, Jaipur. SP KALANTRI is a physician at Mahatma Gandhi Institute of Medical Sciences, Sevagram, Wardha.

preoperative CXR would demonstrate a serious abnormality. These were: history of cardiac or lung disease, cancer at any site, smoking, asbestos exposure, fumes, dusts, serious systemic disease, recent thoracic surgery, abnormal physical findings in the chest, heart, abdomen and age older than 60 years.

(What about Bhopal population then? Should every subject with respiratory symptom be radiographed there, or will pulmonary function testing be a suitable alternative? And should every subject from epidemic zones of tuberculosis be radiographed, _ irrespective of physical signs?)

(Unfortunately these recommendations are so comprehensive and cover so many aspects that virtually every 'patient needing surgery 'might end up with a chest X-ray. Though a number of other studies have failed to fil1d usefulness of pre operative chest X-ray independent of complete clinical evaluation the more recent and widely read surgical texts have avoided the issue entirely, making no specific recommendation.)

Chest X-ray in disease I. Tuberculosis: The 'WHO Expert Committee suggested three criteria for doing CXR in patient~ of tuberculosis: (i)

During chemotherapy. Periodic CXR at intervals that should be dependant on the diagnostic clinical condition and assessment.

(ii)

Treatment completed. Periodic CXR only if clinically indicated. .

(iii)

Defaulter. Further 'CXR 1f-'patient has failed to complete drug therapy.

Mass, Chest X-ray survey of unselected population: The following are the recommendations from the WHO Expert Committee on Tuberculoses (1974) (8): "Mass miniature radiography is very 'expensive_ screening procedure for tuberculosis, even when the prevalence is high. Other disadvantages of MMR are as follows: (1) it contributes only to a small' proportion of cases found; (2) it has no significant effect on the occurrence of subsequent smear positive cases, as they usually develop so rapidly that they arise between the rounds of mass radiography examinations ; (3) it requires the" services of highly qualified technicians and medical staff, who could be better used in the other health service disciplines; (4) the apparatus and the vehicle used' to transport it, are often out of service the committee concluded that, 'he policy of indiscriminate tuberculosis case finding should now be abandoned. Routine chest' X-ray selected population: , . The Expert Committee opined that the chest X-ray is only justified in: (i) Subjects occupationally exposed to respiratory hazards. (ii) Countries or areas where there is high prevalence of tuberculosis and similar infections.

(These guidelines are rather vague and it is difficult to interprete them exactly. Our criticisms are: (i) How precisely do we define clinical condition: based on symptoms or appearance of new signs; either or neither 1 (ii) if structural damage caused by tuberculosis can be picked up by physical examination, be it cavity or fibrosis, effusion or pneumothorax, how will CXR help in the ultimate management? (iii)In defaulters, CXR is useless in, differentiating an active from a healed lesion. (iv) In defaulters the rational approach should be to stop the previously used drugs and start fresh chemotherapy with atleast three new drugs" Won't further CXRs add to the cost of chemotherapy?) 2. Chronic obstructive pulmonary disease: The' Expert Committee considers clinical evaluation better than CXR in periodic assessment of COPD. In childhood asthma, however, it sounds a note of caution and tells us that severe asthma and repeated attacks may be an indication for chest radiography, even in the absence of other clinical findings. A recent study published in 1987(9') also confirms that routine Spiro gram and chest films have little role in the management of clinically stable patients._

1 The Physician's role:

3. Lung Cancer:

1. Lack of knowledge: 'Every patient- with chest pain needs a CXR'.

(i) A number of studies have proved that CXR is useless in picking up asymptomatic lung cancer and offers no benefit in early detection of lung cancer.

2. Undue dependence: How else can I follow my

patient of pneumonia, tuberculosis or lung cancer?

(ii) Routine follow up CXR for patients with lung cancer should only be dictated by clinical evaluation and natural history of cancer.

3 Powerless Radiologist: 'How can I stop a Physician, getting his patient’s, chest X-ray?

4. Systemic Disease:

4 Striving for perfection: 'The medical record should look complete: ‘I should not miss anything.’ ,,

The Expert Committee suggests that if there is no fever and clinical evidence of chest disease, CXR otters no benefit in the clinical evaluation (Two situations, we feel, defy this generalisation. In patients with miliary tuberculosis and meningitis, where fever could be absent due to low immunity and the chest signs are minimum, CXR is an important diagnostic tool. Similarly in patients with persistent weight loss without fever and chest symptoms/signs, CXR often uncovers hitherto unsuspected tuberculosis). 5. Repeated pneumonia:

chest

radiography

for

acute

Harrison's magnum opus on medicine sees no point in doing serial CXRs to know whether the shadow has disappeared. WHO report agrees entirely. The later regards clinical deterioration as the only indication for further CXRs in pneumonia.

5' CXR as a gold standard: 'I know its pneumonia, but am I right? 6, Busy OPD; No time to think: '1 had better buy time. CXR first, physical examination can wait.' 7 Peer pressure: 'If they corner me in hospital death meeting...’ II The patient's contribution: 1 Undue demands: 'I ought to have a CXR for my annual check up: 2. Reimbursement policies: 'But I am not paying from my pockets.' 3 Reassurance: There is something deep within my chest, why not rule it out? 4

Irrational Hope: 'I need CXR to get cured'.

III Social/Economic/Legal factors: (How about this idea: If history and bedside physical signs strongly suggest community acquired pneumonia why not do away even with an initial CXR? More cost-effective approach should be to treat the patient with penicillin, reserving CXR only if the patient does not respond). Over utilisation of chest X-rays Over utilization of X-rays has been defined as excessive radiation per film, excessive films per exposure and excessive examinations per patient. (10) Since the first two factors depend basically on the over use of radiology, I decided to find out why X-rays are being overused. The reasons could be grouped under three broad categories: physician's role, patient's contribution and social/economical legal factors.

1 Institutional requirement: 'Every patient admitted in medical service must be radiographed: 2 Defensive Medicine: 'If someone pulls me up in the court of law?' 3 Money matters: 'I scratch your back, you scratch mine'; reminiscent of the link-cum-cut practice.' 4 Cultural influence: 'When everybody around is doing the same thing, why shouldn't I?' 5. Down to earth logic: I invested 5 lacs in this machine, how else can I recover its cost?'. REFERENCES: next page.

REFERENCES: , 1 Report of a WHO expert committee: A rational approach to radio diagnostic investigations: WHO scientific group on the indications for and limitations of major X-ray diagnostic investigations. WHO tech rep ser 1983; 689 2 Feingold AO: Routine chest roentgenograms on hospital admissions do not discover tuberculosis. South Med F 1977; 70:579-80. 3 Bonebrake CR et al: Routine chest radiography in pregnancy. F Am Med Asso 1978; 240:2747-48. 4 Sane SM et al: Value of pre-operative chest Xray examinations in children. Paediatrics 1977; 60:.j69-672 5 Milne RA: Surg Clin North Am 1979; 2:83- 86

6 Royal College of Radiologists National Study on pre-operative chest radiography: Lancet 1979; 2:83-86. 7 Rucker Let al. Usefulness of screening chest roentgenograms in preoperative patients. J Am Med Asso 1983; 250:3209-11. 8 Report of a WHO expert committee. Tuberculosis, WHO Tech Rep Ser 1974; 552. 9 Owens M et al. Influence of spirometry and chest X-rayon the management of pulmonary out patients. Arch Int Med 1987; 147:1966-70. 10 O' Abrams HL et al. The over utilization of Xrays: New Eng J Med 1979; 300:1213-16.

Medical Technology: neither glitter, nor gold What happens if you rely on robots? You lose the use of faculties that you need for their maintenance and replacement, .so you can't rely on them. (I) 'Good servants but bad masters I' Maurice King has chosen only five words to show wt at modern technology is upto. If one goes through the recent ads appearing in our national newspapers, one can not help feeling that modern technology-its razzle dazzle and all that- has hit the third world countries as well. And so we have lithotripsy machines which 'leave no stone unturned: MRI scans which 'can see almost anything', annual health check-up programmes which 'discover disease even before it has started' and so on... With our doctors also deciding to play with exotic and expensive toys-our euphemism for advanced technology-the threat that medical technology is not posing is not de minimis-small enough to be ignored. Maurice king's warning could not have come at a more appropriate time: 'Should we allow ourselves to be seduced by the irrestible technological imperative, the seductive non Sequitar that because some expensive and sophisticated procedure is done in Detroit or San Diego, it must also be done in Dacca, Delhi or Dares-Salaam, then not only is frustration likely to follow, but there will be less money to spend in Tubvan and the millions of villages like it, all over the developing world, (2) While the idée fixe of the general public that technology can offer infinite solutions to the patient's problems can be understood, what is alarming is the abysmal ignorance and blind faith of medical community in technology. Worse still, as Gajanan Ambulkar shows so well in his cartoon (page 4), even the traditional patientphysician relationship has undergone painful mechanical metamorphosis. Manu Kothari and Lopa Mehta in their characteristic iconoclastic approach attack the myth perpetuated by medical industry and show that modern medicine is a farce. Many of us may not share their views on modern medicine, but the sheer logic behind their trans-technique approach and the evidence they have so painstakingly gathered to support the hypothesis, is indeed thought-provoking.

Kabra's article possesses the punch of an eye opener. The diagnostic X-ray units, the way they are operating without safety measures, the way they are proliferating-putting even that wild weed Parthenium (gajarghas) to, shame-is indeed a matter of great concern. With some 160 million Indians undergoing radiography each year, and considering the miserable state most of our X-ray Clinics are in, the number of reported-and unreported-cases of X-ray associated cancer must indeed be staggering one. In fact Evans et al (3) have recently shown that 267 cases of blood cancer (1 per cent) and 788 cases of breast cancer (0.7 per cent) that occur annually In US may be attributable to diagnostic radiology. Kabra was, and still is, concerned, rather angry, about the indiscriminate way in which X-ray is being exploited to day. His plea to check this hazard in his own state-Rajasthan-fell on def ears of the health authorities. Kabra then went a step ahead, and filed a writ the high court requesting, among other things, a state-wise checkup of X-ray clinics. More power to his muscles! That brings us to the last article. We do not deny that radiography has not benefited the society. Far from it, we owe a Jot of debt to Roentgen. But unless this technology is properly used, the mass popularity of Roentgen's rays will succeed only in creating its own Frankenstein's monster. It is in this regard that the WHO expert committee's report (4) on rational use of radiology needs in-depth study. It has shown that rational radiography can be productive and cost- effective without in any way compromising the quality of medical care. Which is what precisely should medical technology be all about!

UN J AJOO & SP KALANTRI Publishers REFERENCES 1 Anonymous: Br Med J 1986; 292: 1261. 2 Maurice King: Medical care in Developing Country. Oxford Text-book of medicine 1984, p. 3.8 3 Evans etal. New Eng J Med 1986; 315: 82830 4 WHO Tech rep ser; 1983; 689

Browsing through What is in a test? A urine by any other test would taste as sweet. Think of diabetes and the knee-jerk reflex of a busy house-officer in the hospital is to tick mark a laboratory form for blood sugar and hand it over to the staff nurse. Little does he realise that this test would require two venous punctures, a FolinWu technology and atleast Rs. 40 in the patient's pocket. Most of the patients in intensive care units, pre-operative rooms and polyclinics- have to go through this painful ordeal. Not too long ago, Sameer Mewar at Sevagram did prospective study to find out the precise role of urine sugar testing in the diagnosis of diabetes mellitus. He wanted to know whether urine test, properly timed and done, could obviate the need for blood sugar test in the routine screening for diabetes. He also assessed thereliability of urine test. He studied 122 subjects, not known to have diayetes, who either had a family history of diabetes or were suffering from atherosclerotic cardiac or cerebro-vascular diseases, and collected their blood and urine samples at 0, 1 and 2 hours following 75 gms oral glucose load. Diabetes was defined as per criteria suggested by WHO expert committee (1984). He tested urine samples by traditional Benedict's method-mix-heat-see and also by commercially available dip and see strips. All blood samples were tested by Folin Wu method with Veil ore modification. The results, much to his satisfaction, showed that both urine tests came out with flying colours in picking up diabetics. While all 22 diabetics were correctly picked up by urine tests, only in 2 cases the results were found to be false-positive. Retrospective interrogation revealed that drugs received by patients contributed to false positivity. Both tests were thus 100% sensitive & more than 95% specific for the diagnosis of diabetes. The high specificity also reassures that renal glycosuria much talked about and seldom detected-does not affect the outcome of urine tests. It was also found that impaired glucose tolerance could be detected by 1 hour post-glucose urine sample; here strips— (75%) out-scored the Benedict's solution (50%). This study, much simple as it may appear, has important implications. It shows that diabetes can be confirmed or ruled out by simple urine tests. These tests are highly cost-effective (50 times cheaper than blood tests), save a lot of time, Editorial Committee: Anil Patel Abhay Bang Dhruv Mankad Kamala S. Jayarao Padma Prakash Vimal Balasubrahmanyan

to do leave behind painful punctures over arms and display instant visual printouts. Sameer also recommends that pure glucose-sans vitamin Cshould be used for these tests. Vitamin C, a reducing substance, is capable of raising false alarms. Sameer Mewar. Role of Urine Sugar in the Diagnosis of Diabetes Mellitus and impaired glucose intolerance. Thesis Nagpur University, 1986. Keeping Track Social Sciences and Health Service Development in India (1986) D. Banerji. Available from Lok Paksh, Post Box 10517 New Delhi-11 0067 (Rs 150/-, PP 197) An analysis of social science aspects of Health generated through studies and analysis in India over a period of thirty five years. It hypothesises an alternative paradigm in which social sciences are not apolitical, a historical and a theoretical as the Western model' has been in the Indian situation but could be rooted in the sociocultural process and the ecology and history of the country. Interestingly it is devoted to the millions of oppressed who have been targets of motivational manipulation, victim blaming and social marketing all three representing the contribution of studies based on the dominant western reference frame which the author tries to counter with examples of alternative approaches. Health Atlas of India (1986) CBBI Available on request from the Director, Central Bureau of Health Intelligence, Ministry of Health and Family Welfare, Nirman Bhavan, New Delhi. (Gratis, PP 56) This atlas is an attempt to depict the information on all important aspects of Health and related activities in the field of socio-economic development in India through pictorial forms-mainly charts and graphs. Contents include population statistics, vital statistics, socio-economic indicators, investment and expenditure on health, m9dical and paramedical education, health manpower statistics, medical care, Community health services, public health, causes of death, nutrition, health programmes, Indian systems of medicine and homeopathy and international comparative statistics. Suggestions for improvement from the user are welcomed by the Bureau. Views and opinions expressed in the bulletin are those of the authors and not necessarily of the organization. Annual Subscription-Inland Rs.20.00 Foreign: Sea Mail US 4 dollars for all countries. Air Mai': Asia-US 6 dollars Africa & Europe Canada & USAUS 11 dollars Edited by Sathyamala, B-7/88/1, Safdarjung Enclave. New Delhi 110029 Published by Ulhas Jajoo & S. P. Kalantri for Medico Friend Circle Bulletin Trust. 50 LlC quarter University Road, Pune 411016 Printed at Samyayog Mudranalaya, Wardha


