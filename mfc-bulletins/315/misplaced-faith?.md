---
title: "Misplaced Faith?"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Misplaced Faith? from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC315.pdf](http://www.mfcindia.org/mfcpdfs/MFC315.pdf)*

Title: Misplaced Faith?

Authors: Arti Sawhny

medico 315 friend circle bulletin February-March 2006

Wishing away a Condition: Issues of Concern in the Control and Treatment of Leprosy -Jan Swasthya Sahayog(JSS)1

We started a Health Centre in village Ganiyari, district Bilaspur, Chhahtisgarh, in February 2000 with the idea of providing primary and secondary level health care to a population that was largely rural. When we began, we had no idea of the magnitude of the leprosy problem in the area and we were hardly equipped to tackle what we encountered without a dermatologist or a leprologist as a full-time member. From the first month, leprosy patients started pouring in, roughly at the rate of one patient every clinic day. They represented every part of the leprosy spectrum: paucibacillary, multibacillary, pure neuritic, and what have you. There was equal variety in the treatment history: some were seeking treatment for the first time at our clinic, some were already on MDT but not satisfied with the results, some had completed MDT but had relapsed. Once the novelty of seeing “textbook cases” wore off, the tragedy of the situation began to sink in. Even today, an unacceptably large number of patients already have a plantar ulcer or a claw hand when they present themselves for the first time. Ironically, many of them had suffered from tingling or burning sensations in the limbs or pain in the 1

Jan Swasthya Sahayog, Bilaspur (JSS), Chhattisgarh.. Email:<jss_ganiyari@rediffmail.com>. Presented at the mfc Annual Meet, Jan 2006, Vellore, by Biswaroop Chatterjee and Anurag Bhargava.

nerves for months or years, but because they had no visible skin lesions or numb areas on the skin, they had never been diagnosed as having leprosy; and had been treated with Vitamin B Complex injections instead. Several other patients gave a clear history of having developed neurological deficits while they were on MDT. The following slide show was presented at the Medico Friend Circle Annual Meet held at Vellore in January 2006. Much of the energy behind the presentation came from the simple need to vent the frustration of having to treat people, who come six months too late because of a general lack of awareness amongst patients and doctors alike about the different ways leprosy can present. Now that the anger has been vented, the slide show is being presented to a wider audience through the mfc bulletin with the hope of reaching out to those who are involved with leprosy and are not happy about the way it is being handled either at the level of the individual patient or at the community or national level. Leprosy seldom kills but it still does cripple thousands of people in the prime of life. For those of you who feel that it is a shame, let us join hands and strengths, and try to change things for the better. After all, it may not be as difficult as you think.

2

•

•

•

mfc bulletin/February-March 2006

Background Information on the Disease Leprosy is a disease caused by bacteria called Mycobacterium leprae that can infect the whole body. The most important target tissues however are the peripheral nerves and the skin and mucous membranes. Leprosy is feared mainly because of the consequences of the nerve damage that occurs in this disease.

•

Damage to nerves that convey pain and heat sensations allows cuts and burns to go unattended because of the absence of pain.

•

These unattended injuries get infected, the infection goes deep inside and leads to gradual destruction of fingers, toes, hands and feet.

1

•

•

Damage to motor nerves leads to muscle weakness with characteristic deformities of the hands and feet. Weakness of the eyelids causes inability to blink with consequent damage to the eyes and blindness.

2

The incubation period of leprosy, which is the time taken for the signs or symptoms of the disease to appear after the bacteria enter the body, is variable and among the longest known.

3

•

•

Background Information on the Distribution of Leprosy There were 265,781 people registered as patients in the entire country as on March 31, 2004, giving a national prevalence rate (PR) of 2.4 patients per 10,000 population.

4

The WHO considers leprosy to have been eliminated as a public health problem when the prevalence rate (PR) falls below one patient per ten thousand population.

Another 367,143 people had been diagnosed with leprosy and given treatment in the preceding one year. 5

• • • • •

Today, the following states are home to 94% of all leprosy patients in India: East: West Bengal, Orissa, Jharkhand, Bihar Central: Chhattisgarh, Madhya Pradesh South: Andhra Pradesh, Karnataka, Tamil Nadu North:Uttar Pradesh West: Maharashtra, Gujarat If you have noticed, most of these states are also home to some of the poorest people in the country. 7

6

As on April 1, 2004, – 17 Indian states had a Prevalence Rate (PR) that was greater than 1 and – 71 districts had a PR that was greater than 5

8

mfc bulletin/February-March 2006

3

•

The TARGET in 2001-2004 was to detect and cure 11.5 lakh new cases.

•

In REALITY, 14.36 lakh new cases were detected and treated.

9

The decline in the new case detection rate (NCDR) has been quite slow. This is not surprising, given the long incubation period of the disease.

Background Information on Epidemiology This happens because targets are often based on incomplete data. For example, it was found at JALMA, Agra that only 8% of OPD attendees had been visited by any health/ leprosy worker while the Modified Leprosy Elimination Campaign (MLEC) week was being observed. Source: “Leprosy scenario in Agra district” by Anil Kumar, VS Yadav, JK Chakma, Anita Girdhar & BK Girdhar, Central JALMA Institute for Leprosy & other Mycobacterial Diseases, Taj Ganj, Agra 282001, U.P. Presentation at the Workshop on Case Detection Trends held at JALMA, Agra on 12 Nov 2005 10

Evidence of Faulty Planning Since 2003-04, Modified Leprosy Elimination Campaigns (MLECs) have been taking place in only eight states. An arbitrary deadline of December 2005 was set down for bringing down the PR below 1 all over the country.

11

Now the general healthcare system is expected to mop up the remaining cases and take care of the ‘few’ cases that would continue to appear in the future. The ability of the general healthcare system to carry out this job, stretched as it already is to the limits, is doubtful.

12

It seems that the decision to change leprosy care from a vertically administered programme to a horizontally integrated service was based more in changes in funding than in ground realities.

13

Now to cover up the fact that the elimination targets may have been hastily drawn, individuals and organisations that are working in the field and are pointing out flaws in the government strategy, are being accused of having a personal vested interest in keeping the leprosy issue alive.

15

14

There is Strong Evidence that Prevalence and New Case Detection Rates are being doctored

16

4

mfc bulletin/February-March 2006

“Estimated population in each year has been inflated which resulted in lower prevalence rate (PR) per 10,000 population, whereas actual estimated population for each year was to be taken as per prescribed formula on the basis of 1991 and 2001 census for the period 1996-2001.” Source: The Report of the CAG of India, year ended March 2001, Vol. 1 (Civil). Quoted in “Busy Doctoring the Accounts”, The Telegraph, Kolkata, 22.10.2002. <www.telegraphindia.com>

“Test-check of district records revealed that survey was conducted by LCU / modified LCU under concerned DLS during 1996-2001 only covering 10 to 25 per cent of the population for detecting new cases and proper records were not maintained. Had the survey been total, as envisaged under NLEP, new case detection would have been more than that reflected in the state report.” (CAG Report, op.cit.)

17

“The SLO (State Leprosy Officer) issued an order in May 2000 that patients having no ration card or voter’s identity card should be treated separately. Their cases should not be reflected in the reports on the grounds that the PR (Prevalence Rate) was not coming down even after repeated efforts.” “During test-check, some cases were noticed where patients (of both paucibacillary and multibacillary cases) were discharged even before completion of required doses but shown under ‘released from treatment.’This irregular practice was done to depict lower PR.” (CAG Report, op.cit.) 19

18

Another easy way of showing a drop in PR and NCDR has been to shift from active case detection (going into the community and finding out patients) to passive case detection (sitting in the clinic and waiting for patients to come to you). Obviously one finds fewer cases when one does not look out for them. 20

“We have to bring down the prevalence of leprosy below one case per ten thousand population by next year by hook or crook.” “…now recently underreporting has been emerging as a problem as well.” - Newsletter of the WHO Goodwill Ambassador, August 2005

21

- Verbal Statement made in a Meeting of District Leprosy Officers in Chhattisgarh in late 2005 “How will you bring down the prevalence of leprosy if you continue to register all cases?” -Verbal Statement made in the same meeting

22

The NLEP has stopped registering patients with single skin lesions on the grounds that since leprosy is not being diagnosed by experienced healthcare workers any longer, there is a risk of other skin diseases with single skin lesions getting diagnosed as leprosy!

Part of the reduction in leprosy is merely a matter of semantics. One of our patients, a high school teacher, with an immensely thickened great auricular nerve on the right side with multiple draining abscesses even after one year of MDT, was told by the treating doctor: “You used to have leprosy but what you have now is not leprosy since it is not possible to have leprosy after MDT.” Officially he was not a leprosy patient any longer!

23

24

mfc bulletin/February-March 2006

5

Summary With the public perception that leprosy is near eradication, resources for patient care are rapidly being diverted, and the burden of patient care is being transferred to nonexistent or overloaded national health services and to health workers who lack the tools and skills needed for disease diagnosis, classification, and nuanced therapy (particularly in cases of reactional neuritis). Thus the prerequisites for a salutary outcome are increasingly unmet. - Gelber RH. “Leprosy (Hansen’s Disease)” in Harrison’s Principles of Internal Medicine. 16th ed. New York: McGraw25 Hill; 2004: 966-72.

“histologically documented complete damage to dermal innervation in 62.85% (22/35) of patients with BT leprosy, of which 14.28% (5/35) occurred during MDT. Of the patients with detectable dermal innervation at the onset of MDT, 27.7% (5/18) suffered continuing damage during MDT.” Source: Jain M, Singh N, Bhatia A, Arora VK. “Histological assessment of dermal nerve damage occurring during multidrug therapy for leprosy”. Int J Lepr, Other Mycobact Dis 2000; 68:167-71.

Issues in the Quality of Treatment Current Therapy Allows Nerve Damage to Progress during Treatment in Many Patients.

26

Approximately 10% of patients with multibacillary leprosy treated with MB-MDT develop acute nerve function impairment in the first six months after starting treatment.. Source: Croft RP, Nicholls PG, Richardus JH, Smith WC. “Incidence rates of acute nerve function impairment in leprosy: a prospective cohort analysis after 24 months.” Lepr Rev 2000;71: 18-33.

27

28

The risk of developing further impairment is almost double for those with sensory impairment or visible deformity at the time of diagnosis.

Most nerve function deficit occurs within the first two years after starting treatment. This means that it is possible to preserve nerve function by intensive monitoring and suitable intervention during this period.

Source: Reed NK, van Brakel WH, Reed DS. “Progress of impairment scores following commencement of chemotherapy in multibacillary leprosy patients.” Int J Lepr Other Mycobact Dis 1997; 65: 328-36.

29

And of course, like many other things, nerve damage is easier to prevent than treat. Source: Meima A, Saunderson PR, Gebre S, Desta K, Habbema JD. “Dynamics of impairment during and after treatment: the AMFES cohort.” Lepr Rev, 2001;72: 158-70.

31

Source: Croft RP, Nicholls PG, Steyerberg EW, Richardus JH, Withington SG, Smith WC. “A clinical prediction rule for nerve function impairment in leprosy patients revisited after 5 years of follow-up.” Lepr Rev 2003; 74: 35-41.

30

And one does not need expensive gadgets to detect incipient damage of peripheral nerves. Palpation of nerves …along with monitoring of sensory acuity with the help of monofilament nylon …works nearly as well as nerve conduction velocity testing to pick up nerve inflammation and injury. Source: Samant G, Shetty VP, Uplekar MW, Antia NH. “Clinical and electrophysiological evaluation of nerve function impairment following cessation of multidrug therapy in leprosy.” Lepr Rev, 1999; 70: 10-20.

32

6

mfc bulletin/February-March 2006

The TRIPOD 1 Study showed a nearly 4-fold reduction in the rate of acute neurological events while multibacillary patients were getting prednisolone, 20 mg, daily along with standard MDT. Source: Smith WCS, Anderson AM, Withington SG, van Brakel WH, Croft RP, Nicholls PG, Richardus JH. “Steroid prophylaxis for prevention of nerve function impairment in leprosy: randomised placebo controlled trial (TRIPOD 1).” Brit Med J, 2004;328:1459.

33

Steroids are not the only drugs that are useful: highdose clofazimine also plays an important role in saving nerve function.

Interestingly, the study concluded that steroids were not useful since the benefits were not sustained 8 months after steroids were discontinued. We may ask why steroids were stopped after four months? Aren’t there other diseases where steroids are continued for much longer? Were the patients asked if they would prefer the side-effects of steroids or the prospect of life-long disability or deformities? - Smith WCS, et al, op.cit.

34

And when neither steroids nor clofazimine work, one may have to take recourse to nerve trunk decompression. The goal should be to prevent nerve damage at any cost.

Source: Arunthathi S, Satheesh KK.“Does clofazimine have a prophylactic role against neuritis?” Lepr Rev, 1997 Sep; 68(3):233-41.

Source: Malaviya GN. Shall we continue with nerve trunk decompression in Leprosy? Indian J Leprosy, 2004;76(4):331-42. Also: Antonio Salafia and Gautam Chauhan. “Not by Steroids alone.” British Medical Journal, 23 June 2004

35

36

Issues in the Quality of Treatment (contd.) Current Therapy does not kill all the bacteria in many patients.

MDT does not kill all bacteria and this could contribute to the slow loss of peripheral nerve function seen in a significant minority of multibacillary patients after the completion of MDT.

37

38

“As identified by a significant growth in the footpads of immunosuppressed mice, the incidence of viable bacteria in a group of 26 multibacillary (BL-LL) patients released from multidrug (MDT) treatment was found to be two times more in the nerves (46%) as compared to skin (23%)..…..” Source: Shetty VP, Suchitra K, Uplekar MW, Antia NH. “Higher incidence of viable Mycobacterium leprae within the nerve as compared to skin among multibacillary leprosy patients released from multidrug therapy.” Lepr Rev, 1997;68:131-8.

39

So, why not use more active chemotherapy against M. leprae? The means are already available.

40

mfc bulletin/February-March 2006

7

It is interesting to note that the decision of the WHO to use Rifampin, the most potent drug against Mycobacterium leprae, only once a month and not every day is based on the cost of the drug, which is much cheaper today than what it was in the 1980s, when this decision was taken. In fact, in the United States, Rifampin for the treatment of leprosy is given every day rather than every month. Source: Wallace RJ, Griffith DE. “Antimycobacterial agents.” in Harrison’s Principles of Internal Medicine. 16th ed.. New York: McGraw-Hill; 2004: 946-53.

41

Our experience with Patients at the Jan Swasthya Sahyog OPD Clinic, Bilaspur On an average, we register three leprosy patients every week in our OPD clinic. Roughly 15% of them are patients who have already taken MDT for various lengths of time. 43

Reasons given for seeking treatment at Jan Swasthya Sahyog by the 24 patients, who had taken MDT for less than 6 months: * No improvement on MDT * Very slow improvement on MDT * Worsening symptoms while on MDT * No clear reason

4 2 12 6

“… Fortunately, three new drugs, or groups of drugs, have been identified, which achieve a 10 4 kill of M. leprae within 1 to 3 months…Among the 4-fluoroquinolones, ofloxacin has been shown to give such a kill in 3 months in mice, and ofloxacin (400 mg daily) and pefloxacin (800 mg daily) have given a similar kill in one month in a pilot study in man.” Source: Waters MFR. “Leprosy (Hansen’s disease, hanseniasis).” in Weatherall DJ, Ledingham JGG, Warell DA, (ed.) Oxford Textbook of Medicine. 3 rd ed. Oxford: Oxford University Press; 1996: 667-79.

42

In the past 3 years, we have had 82 patients of leprosy seeking treatment at Jan Swasthya Sahyog after having taken MDT for various intervals. Out of these 82 patients, * 24 had taken MDT for less than 6 months * 22 had taken MDT for 6 – 11 months * 36 had taken MDT for 12 months or longer 44

Reasons given for seeking treatment at Jan Swasthya Sahyog by 53 out of the 58 patients, who had taken MDT for six months or longer. * Persistent symptoms after completing 11 * MDT despite some initial benefit * No improvement on MDT 18 * Worsening symptoms while on MDT 16 * Recurrence of symptoms after completion of MDT 6 * No clear reason 2

45

46

Challenges Ahead: Administrative *Convince the Government to continue with a vertical leprosy control set-up with MLEC campaigns in the high-prevalence states until the PR truly comes down without fudging and remains down for a period of at least five years if we are to prevent a repeat of what happened with the malaria eradication programme that was prematurely wound up. *Active case detection in the high-prevalence states would also be needed to diagnose patients early enough to ensure recovery without neurological deficits. 47

*Convince the Government to support the dedicated establishments all over the country that specialise in the prevention, correction and longterm management of deformities that are seen in leprosy patients. *These centres could continue to provide priceless service to leprosy patients who have been cured in a bacteriological sense but have established deformities and also to patients with diabetic foot. *They would also be needed by people who would come down with the disease in the future and would not be lucky enough to avoid deformities.

48

8

mfc bulletin/February-March 2006

And finally, now that the number of cases is expected to come down, let us abandon the current mode of * MCP (Mass Chemotherapy Programme) that was primarily designed to reduce the load of infection in the community and where patient contact was often limited to handing over the 6 or 12 strips of MDT and go back to the time-honoured mode of PCM (Patient Centred Management) where management is nuanced to the patient’s needs and the goal of treatment is functional recovery rather 49 than merely the killing of bacteria.

Challenges Ahead: Scientific • To continue to explore anti-inflammatory regimens that permit recovery with lower rates of neurological deficits • To continue to explore chemotherapy regimens with even lower relapse rates since relapsed patients could prove to be important sources of infection in the postelimination era • Standardise analgesic regimens containing tricyclics and Carbamazepine and other newer drugs (and maybe even acupuncture) since persistent and unbearable neuralgia, particularly at night, seems to be a problem with a significant number of our patients who have presented for treatment in an advanced stage of the disease. 50

• Leprosy is one of the rare diseases where economic issues have clouded the science and art of medical practice. Many a time, talk about more effective bactericidal and anti-inflammatory therapy has stumbled on the issues of funding and logistics.

•

•

It is all right to talk about the treatment of leukemia that is tailored to the patient’s ability to pay. It is fashionable to talk about mastectomy vs. breast preserving surgery for carcinoma of the breast depending on the patient’s wishes. Have we ever consulted leprosy patients to find out if they would prefer the risks of steroid treatment for a year over the risk having clawed hands for the rest of their lives?

51

•

•

Explore non-invasive ways of confirming the diagnosis of pure neuritic leprosy before the onset of irreversible neurological deficits. In our experience, it constitutes at least 10% of all leprosy patients at our centre and it is possible that the proportion of such cases might be increasing in the near-elimination scenario, just as the proportion of multibacillary cases has gone up in recent years.

52

•

•

Conclusion Leprosy is on its way out. Unfortunately, leprosy is not a punctual customer and elimination may not be achievable by an arbitrary date set by man. There is no shame in accepting as much and modify our actions accordingly rather than expect Mycobacterium leprae to change its behaviour to follow our wishes.

53

•

•

•

54

Conclusion Now that the number of cases is expected to come down, it would be in order to redirect our focus from disease elimination back to the patient once again, tempered by the age-old motto, “To comfort always; to relieve often and to heal sometimes.” 21st century medical science has, in fact, given us the tools that can allow us, “To comfort always, to relieve always and to heal always.” The tools are already there; let us now use them. 55

mfc bulletin/February-March 2006

9

How to Count the Poor Correctly versus Illogical Official Procedures - Utsa Patnaik1 Introduction The correct theorizing of the questions of food security and poverty has become particularly important at the present time, which is one of rapid changes in the economic environment in which small producers including farmers and workers are living. In a poor developing country, the incidence of poverty is very closely linked to the availability of food, in which the staple food grains still remain predominant, accounting for three-fifths of the daily energy intake of the population. The measurement of poverty in India has traditionally adopted a nutritional norm specified in terms of an average daily energy intake measured in calories. The National Nutrition Monitoring Bureau has informed us that “the NNMB has consistently confirmed in successive surveys that the main bottleneck in the dietaries of even the poorest Indians is energy and not protein as was hitherto believed… the data also indicate that the measurement of consumption of cereals can be used as a proxy for total energy intake. This observation is of considerable significance as it helps to determine rapid, though approximate, estimates of energy intake at the household level.”2 (emphasis added) It is this strong link between the staple food grains intake and poverty based on a nutritional norm, which enables us to put forward an analysis of the recent trends in food security and in poverty, in the light of the impact of changing economic policies during the last fifteen years. The majority of academics and the Government of India today make two claims, which I believe to be factually incorrect, claims, which are underpinned by a wholly fallacious theoretical understanding of the current situation. They claim first, that there is ‘over supply’ 1

Utsa Patnaik, Centre for Economic Studies, and Planning, Jawaharlal Nehru University, New Delhi This is a an extract from “Theorizing Food Security and Poverty in the Era of Economic Reforms” by the author’s Public Lecture in the series “Freedom from Hunger”, India International Centre, New Delhi, April 12, 2005. Revised November 2005. A version of the paper is published in the Social Scientist, 386-387, Vol 33, No 7-8, July-August 2005. Issues of Social Scientist up to 2003 are available at <http:// dsal.uchicago.edu/books/socialscientist/>. The full lecture is available at the mfc website <www.mfcindia.org>. 2 National Nutrition Monitoring Bureau, 25 Years of NNMB (Delhi 1997). Emphasis added.

of food grains relative to demand, (which they assume to be growing normally) and so infer that food grains production should be cut back in favour of ‘diversification’; second, that poverty has been declining in India in the era of reforms, specifically in the decade of the 1990s. My contention as regards both propositions is that they are incorrect, and that the correct position on theoretical and factual grounds is precisely the opposite. First, there is not over supply of food grains, but a decline in food grains supply and an even more drastic decline of effective demand for food grains especially in rural India owing to an abnormally fast loss of purchasing power during the last six years: so, far from cutting back food grains output, the correct policy is to raise purchasing power and restore effective demand as well as restore access to affordable food grains through a combination of a universal, and not targeted, employment guarantee scheme and through reverting to a universal, not targeted public distribution system. Second, far from the percentage of population in poverty declining as claimed, the factually correct position on the basis of current data is that poverty is very high, affecting at least three-quarters of rural and over two-fifths of the urban population. Moreover the data show that the depth of poverty has increased considerably during the fifteen years of reforms, with more people being pushed down into a poorer nutritional status than before in most of the Indian states and at the All-India level. The reason that many academics and the Planning Commission reach the conclusion that poverty is declining, is that they use an estimation procedure, which has no basis in logic and is indefensible on academic grounds. What that estimation procedure is and how it differs from the correct procedure is one of the main questions I would try to explain, for I believe that it is part of the ‘right to information’ that the intelligent citizen should be able to independently reach a judgment about the validity of the official procedure and not simply take the truth of certain statements for granted. My lecture today will focus on the correct theorizing of these two main questions – of declining effective demand for food grains, and of the extent of poverty. This has become extremely important because the widely prevalent incorrect theorizing in academic and government circles is leading to policy formulations and measures

10 which will only serve to worsen mass welfare and plunge even larger sections of the rural population in particular into higher unemployment and food deprivation. The first and second sections will briefly discuss the deflationary macroeconomic policies combined with exposure to global price declines, which has led to massive loss of purchasing power in rural India in the last six years and is reflected in falling food grains absorption and falling energy intake. The third section discusses the interpretation of the decline in foodgrains absorption while the fourth and last section takes up the question of poverty estimation and how official and most academic estimates use a particular indirect method of estimation, which completely de-links poverty from nutrition norms by ignoring current data which show the ground reality of rising nutritional deprivation and increasing depth of poverty….3 …. 4. Alternative Measures of Head-Count Poverty: or, How to Count the Poor Correctly versus Illogical Official Procedures Poverty studies in India since the early 1970s, have been based on the use of a ‘poverty line’ expenditure level, defined as that level of expenditure per capita per month on all goods and services, whose food expenditure component provided an energy intake of 2400 kcal per capita in rural areas and 2100 kcal per capita in urban areas. All persons spending below the poverty line expenditure are considered to be poor. The required daily allowance (RDA) of energy was specified by the Indian Council for Medical Research and recommended by the Nutrition Expert Group to the Planning Commission in 1969. This is obviously a very minimalist definition of poverty, since no norms are set for essential non-food items of spending such as on fuel for cooking and lighting, clothing, shelter, transport, medical care or education. The database for estimating poverty has been the National Sample Survey Rounds on Consumption 3

Only the fourth section is reproduced here. The complete lecture is available at <www.mfcindia.org>. Table numbers and complete citation list, pertaining to the entire lecture, have been retained as in the original. –Editor. 4 The required graphs are 1) the ogive of cumulative percentage of persons below specified expenditure levels , and 2) the relation between per capita expenditure in each expenditure group and the per capita calorie intake for each expenditure group. With two relations and three variables – calorie intake, percentage of persons, and per capita expenditure – knowing the value of any one variable determines the other two.

mfc bulletin/February-March 2006 Expenditure, which take the household as the sampling unit. These surveys present the distribution of persons by monthly per capita expenditure groups, and since the quantities of foods consumed and their calorie equivalents are available, they also present the calorie intake per capita per diem by expenditure groups. That particular expenditure group whose food expenditure met the calorie requirement in 1973-74, was identified and the relevant expenditure was defined as the poverty line expenditure (often this is mislabeled as poverty line income, but we have no information on income). Large sample surveys are carried out at fiveyearly intervals, the latest available data being from the 55th Round relating to 1999-2000, from which the relevant data for All-India is reproduced in Table 8 using two published Reports of the NSS. A good idea of the current magnitude of head–count poverty can be obtained by the non-expert without doing any calculations, simply by inspecting the data in Table 8. Looking at the first, third and fifth columns, 69.7 percent or say seven-tenths of the rural population of India, spending less than Rs.525 per month per person, was below the average calorie level of 2403 (nearly the same as the 2400 norm), which was obtained only by the next higher spending group of Rs. 525-615. Since persons in the lower part of this group also obtained below 2400 calories, the poverty percentage is a bit higher than seven-tenths, and on plotting the data on a graph we obtain the more exact figure of 74.5 percent below Rs.565, the expenditure required to access the energy norm. 4 But, the official Planning Commission figure of rural head-count poverty from the same data is only 27.4 percent! The difference between the estimate obtained by direct inspection of the latest data and the figure as given by the Planning Commission, is 47 percent, so nearly half of the actually poor rural population, about 350 million persons, are excluded from the set of the officially poor. Again, from direct inspection we see that about twofifths of the urban population spending below Rs.575 per capita per month obtained less than 2091 calories (very close to the 2100 urban norm), which was the average for the next higher spending group. The exact percentage in urban poverty on plotting the graph is 44 percent. The Planning Commission figure for urban poverty for the same year is only 23.5 percent. What explains this big difference? The Planning Commission has never officially given up the nutritional norm of 2400 calories. The majority of economists in India believe that this norm is still

mfc bulletin/February-March 2006 being followed. The reality is that the actual estimation procedure followed by the Planning Commission has de-linked its poverty estimates completely from the nutrition norm. The poverty line was obtained following the norm, only in the year 1973-74 using the 28th Round NSS data, a date three decades in the past. For that year at prices then prevailing, the rural and urban poverty lines were Rs.49.09 and Rs. 56.64 per capita per month, since at these expenditures the 2400 rural and 2100 urban calorie intake norms were satisfied. It was found that 56.4 percent of the rural and 49 percent of the urban population were below these poverty lines.5 For later years, strange though it may seem, no use was made of a single iota of the actual consumption data and calorie equivalents, thrown up by as many as five successive large-sample surveys (in 1977-8, 1983,1988-9,1993-4, and 1999-2000). There was no official attempt to update the poverty lines on the basis of the available current information on what expenditure was actually required to meet the nutrition norm. Rather, the three-decade-old poverty lines (Rs 49.1 and Rs.56.6, rural and urban) were simply adjusted upwards by using a price index, while assuming an invariant 1973-74 consumption basket. The adjusted poverty line was then applied to the cumulative distribution of persons by expenditure groups, in current NSS data to obtain the ‘poverty percentage’. Thus the current data were, and are being used selectively, with only the distribution of persons by expenditure classes being used, and the associated energy intake part being ignored completely. The declining energy intake corresponding to official poverty estimates are never mentioned, nor do academics following the same method ever mention the lowered calorie intake corresponding to their estimates (vide the papers in Economic and Political Weekly, 2003, special number tendentiously titled ‘Poverty reduction in the 1990s’). The credibility of official and similar academic poverty estimates would certainly come into question if the educated public at large was informed how far below RDA (Required Daily Allowance) the consumption standard has been continuously pushed down, by the official method. For example the official price index adjusted poverty line for 1999-2000 was Rs.328 only (about 6.7 times Rs. 49) and this has been applied to the first and last columns of Table 8 to read the population below this line, which came to 27%. No attention was paid to the 5

It is a curious matter of chance that poverty lines were Rs.49.1 and Rs 56.6 while the corresponding poverty percentages were 56.4 and 49.

11 fact that at this expenditure a person could access at most only 1890 calories, over 500 calories per day below the RDA and nor is this fact ever mentioned to the public when poverty estimates are quoted by the Planning Commission. This amounts to suppression of information and is not an academically acceptable procedure. The same applies to the academics who follow the official method and who never allude to the lower and lower calorie intake inherent in their price index- adjusted poverty lines over time. Academics writing earlier (R. Nayyar 1991) however, had estimated poverty both by direct inspection of current data and by the official price index adjustment to a base year method. Nayyar had explicitly noted that the poverty figures estimated by the official method, diverged more and more over time from the much higher poverty percentages yielded by directly using current data. As the base year of the official method gets further back in time the divergence has assumed absurd proportions. For 1993-4 the official price index adjustment method gave a rural poverty line of only Rs.205, and 37.3 % were below it in the 50th Round distribution of persons by expenditure groups, and so deemed to be ‘in poverty’, but the fact that at this poverty line only 1,970 calories per diem could be accessed (over 400 calories below the RDA) was never mentioned to the public. Inspecting the same current 50th Round data showed that 74.5% of persons or double the official estimate, had an intake below the RDA of 2400 calories, because their monthly expenditure was below Rs.325, the realistic poverty line at which the nutrition RDA could be accessed. Mehta and Venkataraman (2000) in a short but significant paper, later also pointed out for the 50th Round data, this large divergence between the results of applying the official definition, and following the official price-adjustment procedure. They do not refer to the earlier discussion by Nayyar (1991) who had already pointed out the divergence for earlier Rounds and had also analysed state-wise divergence. In 1999-2000 as we have already noted the official estimate gives only 27.4 percent in poverty because these are the persons spending below the price index adjusted official poverty line of Rs.328, but again the further lowering of the associated energy intake standard to 1890 calories, over 500 calories per day below RDA, is never mentioned. The same current 55th Round data shown in Table 8 continue to give 74.5percent of persons actually in poverty, namely with

12

mfc bulletin/February-March 2006

intake below 2400 calories because their expenditure was below the Rs.565 actually required to access the RDA. (However greater poverty depth is seen by 199900, with more of the population moving below 1800 calories as compared to 1993-94). Thus in 1993-4 the official method had left out 37.2 percent of the total rural population who were actually poor, while by 1999-

2000 the official method was leaving out 47.1 of the total rural population or around 350 million persons who were actually poor. Table 9 summarizes the official poverty lines, poverty percentages and the falling calorie intakes at poverty lines, and it gives the true poverty lines required to access the RDA, along with the true poverty percentages.

Table 8: Percentage Distribution of Persons by Monthly Per Capita Expenditure (MPCE) Groups and average Calorie Intake per diem, 1999-2000, All-India RURAL Monthly per capita Expenditure (Rupees)

Average MPCE (Rupees)

Calorie Intake per diem per Capita

Per cent of Persons %

Cumulative per cent of Persons %

Below 225 225 - 255 255 - 300 300 - 340 340 - 380 380 - 420 420 - 470 470 - 525 525 - 615 615 - 775 775 - 900 900 & more

191 242 279 321 361 400 445 497 567 686 851 1344

1383 1609 1733 1868 1957 2054 2173 2289 2403 2581 2735 3178

5.1 5.0 10.1 10.0 10.3 9.7 10.2 9.3 10.3 9.9 5.0 5.0

5.1 10.1 20.2 30.2 40.5 50.2 60.4 69.7 80.0 89.9 94.9 99.9

ALL

486

2149

99.9

SUMMARY Rs 470-525 and less

2289 cal and less

69.7

Rs 525-615

2403 cal

10.3

Rs 615-775 and more

2581cal and more

19.9

mfc bulletin/February-March 2006

13

URBAN Monthly per capita Expenditure (Rupees)

Calorie Intake per diem per Capita

Per cent of Persons %

Cumulative per cent of Persons %

Below 300

1398

5.0

5.0

300 - 350 350 - 425

1654 1729

5.1 9.6

10.1 19.7

425 - 500

1912

10.1

29.8

500 - 575

1968

9.9

39.7

575 - 665

2091

10.0

49.7

665 - 775

2187

10.1

59.8

775 - 915 915 - 1120 1120 - 1500 1500 - 1925 1925 & more

2297 2467 2536 2736 2938

10.0 10.0 10.1 5.0 5.0

69.8 79.8 89.9 94.9 100

ALL

2156

99.9

SUMMARY 500-575 and les

1968 and less

39.7

575-665

2091

10.0

665-775 and more

2187 and more

50.2

Source: National Sample Survey Organization (55th Round, 1999-2000) Report No. 471, Nutritional Intake in India for calorie intake data by expenditure groups and Report No. 454, Household Consumer Expenditure in India – Key Results for the distribution of persons. The calorie intake data. refers to the 30-day recall so the distribution of persons by the same recall period is taken above.

There is no theoretically acceptable basis to the official claims of poverty reduction in the 1990s. The basic point is that the method of comparison over time is not logically valid when the consumption standard is being altered, as is being done in the indirect estimates. The consumption standard in 1973-74 was 2400 calories at which 56% was in poverty, by 1983 the official estimate of 45.7 % in poverty corresponded to 2060 calories intake, by 1993-94 the standard implicit in the official estimate (37% in poverty) was down further to 1970 calories, and in 1999-2000 for the official estimate

(27.4 %) it was even lower at 1890 calories. By the 60th Round, 2004-05 it is likely to be below 1800 calories and correspond to less than one-fifth of rural population. We will once more hear spurious claims of further ‘poverty reduction’ without any mention of the lowering of the energy intake. All this has been happening because the price-adjustment to a base – year poverty line does not capture the actual current cost of accessing the minimum nutrition, and this failure becomes more acute as the base year recedes further into the past.

14

mfc bulletin/February-March 2006 Table 9 : The Rural Poor as Percent of Rural Population in India

1973

1983

- 74

1993

1999

- 94

- 00

2004

MPCE (Poverty line) Rs

NSS ROUND:

28th

32nd

50th

55th

60th

1973 - 74 28th

Using Official Definition (< MPCE Giving 2400 cals)

56.4

70.0

74.5

74.5

n.a

49

120

325

570

n.a

56.4

45.7

37.3

27.4

20.3*

49

86

206

328

354*

2400

2060

1970

1890

n.a

(1.0)

(1.4)

(1.6)

(1.7)

n.a

Official Estimates and Implied Calorie ‘Norm’

1983

1999 - 00 55th

2004

32nd

1993 - 94 50th

60th

Source: First line calculated from NSS Reports on Consumer Expenditure, 50th Round 1993-4 and 55th Round 1999-00. MPCE is Monthly Per Capita Expenditure Note. That base year 1973-74 is the only year the official definition was correctly applied.- in all later years the nutrition norm is continuously diluted. The same exercise can be carried out for urban India. (Figures in parentheses are the ratio of the expenditure actually required to access the calorie RDA, to the official poverty line). * Provisional estimate, applying official poverty line of Rs.354 for 2004, to the ogive of persons by expenditure levels from NSS 60 th Round, January–June 2004, Report No. 505 Household Consumer Expenditure in India, Statement 3.2 R. Note that 60th Round is a thin sample.

How can anyone say how ‘poverty’ has changed over time using the above method? To give an analogy, when a set of runners are lined up in a row on a circular race track for a long-distance race, if the person in the innermost circle crosses the finishing rope first, it cannot be validly inferred that he has won the race: for the distance run by him is much less than that run by others. For a valid comparison of the runners’ performance, the distance run has to be the same standardized distance for all the runners, and staggering the runners does this. Similarly, in the official method the percent of persons below the same, standardized consumption level or levels, need to be compared but this is not the case in the indirect method. Rather, the method used 6

The analogy can be carried a little further. If the race is a short one over a straight segment of the course, lining the runners up in a straight line at the starting point is okay. Similarly if the base year of the price index is very close, say two to three years, then comparison over time can be made using the official method - which ignores every non-base year actual calorie intake - without leading to too much inaccuracy. But for a long race (a base year further back in time) absence of standardization will arise and make comparison invalid.

implies that the percentages below un-standardized and changing consumption levels are sought to be compared over time (see Table 9). 6 This is not legitimate, and any statement about decline (or change generally) is not valid. Present day heated debates between the estimators about whether poverty has ‘declined’ by ten points or seven points, when poverty has not declined at all, can be likened to debates over whether the inner-circle runner has ‘won’ by one metre or two metres, when the fact of the matter is that he has not ‘won’ at all, because the premise for valid comparison is violated. The official rural monthly poverty line expenditure for year 2004 (obtained by updating the 1999-00 poverty line of Rs.328, using the CPIAL), is Rs.354 or Rs11.8 daily, equivalent to 26 US cents at the prevailing exchange rate. This paltry amount will actually buy at most one bottle of water, but it is supposed to cover all expenditure on food, fuel, clothing, shelter, transport, health and education – in short all daily spending on goods and services for one person! Estimates of Indian poverty for 1999-00, 55th Round,

mfc bulletin/February-March 2006 by some individual academics like A.Deaton (2003b, 367) and S.Bhalla (2003) are even lower and imply a poverty-line of 20 US cents or less expenditure per day, one-fifth of the World Bank’s dollar-a-day measure. There is no logic in arguing that purchasing power parity should be considered and instead of one dollar therefore around one third of that should be taken as the local poverty line, for the comparison is not between advanced and developing countries at all but between developing and other developing countries. A quarter U.S dollar in India purchases exactly as much as Rs.11 does, at the prevailing exchange rate, and a quarter US dollar purchases exactly as much as 2 yuan does in China (whose current rural poverty line is also far too low at 2.2 yuan per day). Poverty level incomes in the USA are not set three times higher than the Chinese or Indian one, but are at least thirty times higher. Obviously, it is not difficult for either the Planning Commission or the individual academics to ‘adjust’ Indian poverty figures downwards when the consumption level embodied in the rural poverty line, is depressed to such sub-human levels as Rs11 or less per day. Few people can actually survive long below these levels –those who are there today are on their way to early death. The poverty estimators should try a test on themselves. Let them be handed the weekly equivalent of their own estimated monthly poverty line – they need not even exert themselves to earn it as the poor are obliged to do – and let them spend only one week in a village living on that amount, which would range from Rs.60 to Rs, 80. Since they will not be confident of drinking the local water all they would be able to buy would be a bottle of water a day and no food leave alone other necessities. What they would undoubtedly gain from their oneweek stay, would be weight loss. Urban poverty lines are almost equally unrealistic. Sometimes to justify the indirect method it is argued that the original rural consumption norm of 2400 was ‘too high’. First, it is not ‘too high’ because the average intake of those below it works out to about 1950 calories which is lower than in any other country in the world except the least developed countries. Second, even if it is accepted for the sake of argument that it was ‘too high’ it does not justify comparing 1999-2000 ‘poverty’ figures, which are all those persons below 1890 calories intake, to those persons below 1970 calories intake in 1993-94 and those 7

I have discussed the fallacy of equivocation involved in the indirect estimates, in Patnaik 2005b.

15 persons below 2400 calories intake in 1973-74. By all means, let us consider lower norms, in fact take several alternative norms including 2400, but when comparing over time, compare the proportion of population under the same norm at the two or more points of time – for only then will the comparison be valid. The indirect estimates fail on this simple but essential criterion of comparability over time and those who nevertheless undertake such comparison are committing a logical fallacy – the fallacy of equivocation. This is a well-known type of verbal fallacy, in which the same term is used with two completely different meanings in the course of the argument, so the inference is not true. In this case, ‘poverty line’ was defined and initially calculated with respect to a nutrition norm, while ‘poverty line’ as actually calculated is de-linked from the norm, so the inference regarding change (whether rise, fall or constancy) is not true.7 Not only is the official comparison of poverty percentages, and claims of poverty reduction over time, quite spurious; the comparison of the poverty levels of states at a given point of time, is equally invalid. As Table 10 shows, we have a bizarre picture when we calculate the maximum calorie intake levels below which people are designated as ‘poor’ by the official method in the different states of India. The calorie intake corresponding to the official state-wise poverty lines - from which the state poverty percentage have been officially derived - for the year 1999-2000, varies from 1440 only in Kerala, nearly a thousand calories below RDA, to 2120 in Orissa, less than 300 calories below RDA. The fact is that the official method in India today adheres to no nutrition norm at all. Nutrition has dropped out of the picture completely in the indirect method, nor is there any lower bound which is set, to the extent of decline in the calorie intake corresponding to whatever the price-adjusted poverty line happens to be. That is why we find states with 1500 calories or less intake corresponding to their official poverty lines in 1999-00. In as many as 9 states, the calorie intake associated with the official poverty lines was below 1800 calories in the 55th Round, while in four states it was 1600 calories or less (see Table 10). None of this is mentioned when poverty estimates are quoted by those making them. Not even the late P.V. Sukhatme, who was a consistent critic of the 2400 calorie RDA being too high, would

16 have accepted 1800 calories as a reasonable norm for estimating who the poor are, - leave alone 1600 calories or less. He had used a norm of 2200 calories in one of his own estimates (Sukhatme 1977). By 2004-05 the All-India official poverty line itself will correspond to an intake of 1800 calories or less, and at least eight states will have a 1600 or less calorie intake corresponding to the state-specific official poverty lines. The fact that comparability conditions are blatantly violated, is obvious. Officially it is inferred that poverty is much higher, for example, in Orissa at 48 percent, than in neighbouring Andhra Pradesh at only 11 percent. But how can we possibly infer that Orissa is ‘poorer’ than Andhra, when the ‘officially poor’ are those persons with below 2120 calories intake in Orissa but the ‘officially poor’ are those persons with below 1600 calories intake in Andhra? (As a matter of fact the below 2400 and below 2100 calories poverty percentages are both higher in Andhra than in Orissa as the same Table shows in the last two columns). Similarly, how can it be inferred that rural Gujarat with only 13 percent officially in poverty is much better off than West Bengal with 33 percent officially poor, when the associated calorie ‘norm’ in Gujarat has been pushed down to only 1680 compared to 1900 in West Bengal? As a matter of fact the below 2400 calories poverty percentage is marginally lower for W.Bengal compared to Gujarat and the below 2100 calories percentage is substantially lower for W.Bengal. And so the anomalies can be multiplied. Further, how can, for each state, the official estimate in 1999-00 be compared with that in 1993-94 and inference about ‘decline’ be drawn, when the associated calorie intake has been lowered in each state? (Except only one, Gujarat). As a teacher if I were to follow the illogical procedure of saying that student A who has 53 percent marks is ‘better’ than student B who has 59 percent marks, because I apply a 50 out of 100 marks standard to student A and apply a different, 60 marks out of 100 standard to student B, I would rightly face a court case. Yet our Planning Commission and individual academics have been allowed to get away with making patently illogical and untrue statements on poverty. The Deputy Chairman of the Planning Commission recently congratulated the Andhra Pradesh government on its success in reducing poverty. This ‘reduction’ was solely the effect of applying an extraordinarily low price-adjusted poverty line of Rs. 262 per month in 1999-00, or less than Rs.9 per day, at

mfc bulletin/February-March 2006 which less than 1600 calories could be accessed (See Table 10). Looking directly at nutrition poverty, on the other hand, we find that the proportion of persons below 1800 calories intake in that state has doubled to 40% by 1999-00 compared to 1983 (Table 11). To complete the story, the proportion below 2100 calories has risen to 62% at the later date, compared to 56 % only five years earlier in 1993-4, and 44% in 1983. What is the reason, the reader might ask, for the official method producing consistently lower estimates than the direct method, and why has the divergence been growing until now, the indirect estimate gives only 27 percent compared to nearly 75 percent by the direct estimate. It is not primarily a matter of the price index used: different price indices (different in terms of the extent of price rise, but all with the same base year quantity weights) do give different results but this accounts for difference of at most 10 percent or so of population in poverty, not the difference of over 47 percent of population which is actually observed. The basic reason for the large and increasing difference, is the assumption of an invariant consumption basket in the indirect method, held unchanged for three decades. In effect the official estimators are saying – if a person in a village consumed the same quantities of foods and other goods and services as 32 years ago, then Rs 328 per month is enough to access these quantities in 1999-2000 and Rs.354 per month is enough in 2004. If you do not get the calorie standard, it is the result of your free choice which has led you to consume in a different pattern” This is not however a reasonable position to adopt. It is as unreasonable as telling a 32-year old man, that the one metre of cloth which was enough to clothe him when he was one month old, and which cost say Rs 10 at that time, can be bought after price –index adjustment for Rs.70 today, and if this expenditure leaves him semi-naked today, then it is his problem of free choice to be in that state. Such a position ignores the irreversible structural changes the person has undergone which means his set of choices has altered over time. Of course, this is only an analogy – we are not arguing that the proportion of adults in the population has risen! We wish through the analogy, to drive home the point that over the last three decades certain irreversible structural changes have taken place in the economy. There has been increasing monetization of the economy and disappearance of common property resources, along with higher cost of utilities and health care. With a given real income people have to spend relatively more on essential

mfc bulletin/February-March 2006 non-food requirements, overcoming illness and earning a living. The actual current rural consumption basket which satisfies the nutrition norm, and to which the total monthly expenditure on all goods and services corresponds, costs almost double the priceadjusted poverty line (from Table 8 summarized in Table 9, at least Rs.570 is required compare to the official Rs.328). The official poverty lines are simply far too low and are getting further lowered as the base year becomes more remote. Rohini Nayyar (1991) in her careful doctoral study, had estimated poverty using both methods and had noted the widening divergence in the results between 1961-2 and 1977-8. She had taken some solace from the fact that though poverty levels estimated by the two different methods were drawing apart quite fast, at least they did seem to move in the same direction over time. The ranking of the states of India according to their poverty levels estimated using the two methods, was highly correlated: Nayyar found that Spearman’s rank correlation coefficient worked out to 0.89 and 0.84 (using the official estimate on the one hand, and two different direct estimate norms of 2200 and 2000 calories) and was significant at the 1% level. But in the 1990s this conclusion no longer holds. The poverty levels calculated by the two methods are moving fast in opposite directions and the rank correlation may soon become negative. Spearman’s rank correlation taking the poverty ranks of the states by the official indirect method, and by the direct method for 1999-2000, 55th Round data, works out to only 0.236 and 0.075 (using the same two direct estimate norms) and neither is statistically significant at the 1% level. 8 Inspection of Table 10 will tell the reader why this is the case: some of the states with the lowest official poverty, such as Andhra Pradesh, a by-word for agrarian distress, have some of the highest actual poverty. In general the official method produces the largest divergence from the direct method, in the case of the Southern and Eastern states. The rot in poverty studies discussions seems to have 8

“Poverty Estimates in India: A Critical Appraisal”, Ramanand Ram, M.Phil Dissertation submitted in JNU, 2004. 9

We could easily find out how much higher the direct estimate would be than 74.5 percent if those making the adjustment to the distribution of persons by expenditure class, had bothered to present the associated average calorie intake by expenditure class. As usual however they ignore the nutrition part completely in their papers.

17 set in with neo-liberal reforms in India, particularly in the late 1990s. The Indian Government was eager to claim success for the economic reforms and the proreform economists were eager to see poverty reduction in the data. In such a milieu, the inconvenient direct estimates showing high and in some states, increasing levels of poverty were swept under the carpet. Discussion of direct estimation of poverty virtually disappeared from the literature. The dominant trend of discussion focussed on the official indirect method, which, to the great satisfaction of the pro-reform academics and the World Bank estimators, not only showed very low ‘poverty’ levels but actual decline in these levels. Not one of the authors using the official indirect method, alluded to the nutritional implications of their own estimates. This meant that they were using and presenting the NSS data selectively, taking only the distribution of persons by expenditure classes to read off the poverty proportion corresponding to their indirect poverty line, while ignoring the associated energy intake figures completely. Such lack of transparency and selective use of data, is not acceptable academic procedure. Owing to this lack of transparency, to this day most economists in India not directly working with the data, and including even those examining research theses on poverty, are not aware that drastically lowered consumption levels over time and arbitrary variation of consumption levels across states, are the necessary implications of following the indirect method and arriving at low poverty estimates. They assume that the original norms are being followed when this is not true. There is a debate among the academics following the official, indirect method, that owing to change in the recall period during the 55th round, 1999-2000 compared to earlier Rounds, actual expenditure is slightly overstated in every expenditure class, and hence the distribution of persons by expenditure classes has been affected. Making the required adjustment for comparability alters this distribution slightly and raises the 27 percent below the Rs.328 official price – adjusted poverty line, by another 2 to 3 percent (Sundaram and Tendulkar, 2003, Deaton, 2003a, Sen and Himanshu 2005). If these adjustments are correct, quite obviously, the percentage of persons below the directly observed poverty line of Rs.570 would rise to an even greater extent than 2 to 3, since a higher proportion of people than before would also come into the expenditure interval Rs.328 to Rs 570, and thus the difference between official estimate and the direct estimate would increase further. Thus all those

18 with less than 2400 calories intake per diem, in 19992000 would be more than 74.5 + 3 = 77.5 percent of rural population, which is a rise compared to 74.5 percent in the 50th Round, 1993-94. Similarly those below 2100 calories would rise from 49.5 percent to more than 52.5 percent.9 However we have chosen to give the direct estimate for 1999-2000 unadjusted for recall period in all our tables, since the main point being made in this section, is the type of mistake involved in the indirect method itself which is leaving out nearly half the rural poor, and this basic problem with all indirect estimates not only remains but gets further aggravated, whenever adjustments are made by the estimators on account of altered recall period. It may be noted that with the adjustment for recall period, they are leaving out more than 47 percent of the actually poor rural population from their set of ‘the poor’ while without the adjustment, they were leaving out exactly 47 percent of the population. Some economists who are critical of the official priceadjustment method, which de-links the estimates from nutrition, have correctly put nutrition back at the centre of their own analysis, but they have followed another direct poverty estimation route, as compared to inspecting current NSS data – the method we have followed. They have estimated the minimum cost of accessing the calorie RDA on the basis of current nutrient prices, and thus have obtained normative food expenditure. By comparing with the actual expenditure on food in the NSS, they arrive at the percentage of persons failing to reach the RDA and this is 66 percent at the All-India level for the 55th Round (See Coondoo, Majumdar, Lancaster and Ray 2004, Ray and Lancaster 2005). Subramanian (2005) has used indirect method base years closer to the present, as well as the direct method we use, to see how the trends in poverty behave under alternative scenarios. 10

Meenakshi and Viswanathan 2003 present ‘calorie deprivation’ as though it is an independent topic, not essentially related to official poverty estimates, and although they usefully juxtapose their estimates of population below differing calorie norms, and the official estimates, they do not refer to the falling energy equivalent of the official or individual poverty lines over time which affects comparability. Their method of estimating the calorie distribution ogives using kernel density functions, gives higher estimates of population below various calorie norms, than our estimates using the grouped data and the simple method described in the note to Table 10. This is probably because their estimate includes all well- to- do persons who have lower calorie intake than RDA. There is no reason however to consider rich race jockeys, super models or anorexic people as part of the poor.

mfc bulletin/February-March 2006 Many critical voices (Suryanarayana 1996, Mehta and Venkataraman 2000, Swaminathan 1999, 2002) which had continued to draw attention to the high prevalence of undernutrition and malnutrition, to the secular decline in average rural calorie intake, to high direct poverty estimates using reasonable calorie norms and which criticized the indirect estimates, have been sought to be silenced by the pro-reform economists, by the simple expedient of ignoring them altogether. Not one critical author is referred to in the articles by those presenting their indirect estimates at a Conference and later collecting them in a special issue of the Economic and Political Weekly tendentiously titled ‘Poverty Reduction in the 1990s’ (Deaton 2003a, and 2003bTendu;lkar and Sundaram 2003 etc.). The only article on energy intake while juxtaposing the official and direct estimate does so somewhat uncritically.10 The critical writers on the other hand, have given cogent arguments to suggest why per capita calorie intake should be involuntarily declining in the lower expenditure classes over time. (It is also declining in higher expenditure classes but the problems of the initially over-fed, who may be reducing intake, do not concern us at present). They have pointed out that there has been substantial monetization of the economy over the last three decades. Wages which used to be paid in kind as grain or meals, valued at low farm-gate prices in earlier NSS Rounds, are now paid in cash which the labourer has to exchange for food at higher retail prices, and so can buy less of it for a given real income. Common property resources have disappeared over the last three decades: fuel wood and fodder, earlier gleaned and gathered (and not fully valued in the NSS data), now have to be purchased, restricting the ability of the poorer population, to satisfy basic food needs out of a given real income and leading to the observed energy intake decline. Staple grains and fuelwood or other fuels are obviously, jointly demanded since no-one can eat raw grain, and with a given real income a part of expenditure on grain has to be enforcedly reduced to purchase fuel. To this we have to add higher medical, transport and education costs as state funding is reduced and some services are privatized. The correct thrust of these arguments is that under-nutrition and poverty is very high, affecting three-quarters of the rural population by now, and observed calorie intake decline for the lower fractiles is non-voluntary. By 1999-2000 for the first time average calorie intake in rural India has fallen below average urban calorie intake.

mfc bulletin/February-March 2006

19

Table 10 : Official Poverty Percentage by States and Associated Calorie ‘Norm’ Indirect estimates, 1993-4 and 1999-00 STATE

Direct Estimates, 1999-2000

1993-94 Official Poverty percentage

Implied Calorie ‘Norm’

1999-2000 Official Poverty Percentage

Implied Calorie ‘Norm

< 2400 cal Poverty Percentage

< 2100 cal Poverty Percentage

Andhra Pradesh Assam Bihar Gujarat Haryana Karnataka Kerala Madhya Pradesh Maharashtra Orissa Punjab Rajasthan Tamilnadu Uttar Pradesh West Bengal

15.92 45.01 58.21 22.18 28.02 29.88 25.76 40.64 37.93 49.72 11.95 26.46 32.48 48.28 40.80

1700 1960 2275 1650 1970 1800 1630 1970 1780 2150 1810 2130 1650 2220 2080

11.05 40.04 44.30 13.17 8.27 17.30 9.38 37.06 23.72 48.01 6.35 13.74 20.55 31.22 31.85

1590 1790 2010 1680 1720 1600 1440 1850 1760 2120 1710 1925 1510 2040 1900

84.0 91.0 77.0 83.0 47.5 82.0 82.5 78.5 92.0 79.0 47.5 53.5 94.5 61.0 81.0

62.0 71.0 53.5 68.5 30.5 50.0 52.5 55.0 55.0 45.5 36.5 27.5 76.0 37.5 55.0

ALL INDIA

37.27

1970

27.09

1890

74.5

49.5

Source: As Table 8. From the basic data by states, the ogive or cumulative frequency distribution of persons below specified per capita expenditure levels was plotted, and on the same graph the relation of per capita expenditure and per capita calorie intake was plotted. Calorie intake corresponding to the official estimates was then obtained from the graphs. Note that for 1993-94 the mid-point value of each expenditure class has been plotted against the per capita calorie intake as the arithmetic average was not available in the published tables. For 1999-2000 it was available and has been used in deriving the figures for 1999-00. We find that for several expenditure classes the mid-point value coincided with the arithmetic mean, and for the others the difference of midpoint value from mean was very small, suggesting that the same would be true for 1993-4.

Concluding Remarks This paper has embarked on a brief but sharp critique of the prevalent analysis and prescriptions regarding food security and poverty, because of two reasons. First, the agrarian crisis is serious and widespread, and has been created by public policies, which have been deflationary, combined with trade liberalization when world primary prices have been declining. It is manifesting itself in slowing output growth, rising unemployment, unprecedented income deflation for the majority of cultivators and labourers, enmeshing of cultivators in unrepayable debt, and loss of assets including land, to creditors. Kidney sales and nine thousand recorded farmer suicides are only the tip of the iceberg of increasing deprivation, a crucial index of which is an unprecedented fall in foodgrains absorption to levels prevalent 50 years ago, and decline in average calorie intake in rural India. Second, the prevalent analysis by policy makers, the

Planning Commission and the government, however, can be summed up as an obdurate refusal to face the facts, and an attempt to construct a counter-factual fairy story, which is illogical, and in patent contradiction with the trends in the economy. “We must learn truth from facts” (Mao ZeDong) “or the facts will punish us” (added by Deng Hsiao Ping) is a dictum that our policy makers would do well to bear in mind. Their theorization interprets severe loss of purchasing power and enforced decline in effective demand for food grains, as its very opposite, as ‘overproduction’ in relation to an allegedly voluntary reduction of foodgrains intake by all segments of the population, and reaches the dangerous inference that foodgrains output should be cut back. It refuses to recognize that, while in developed societies, consumers can be separated from a minority who are agricultural producers, in a poor country like India the majority of consumers are themselves rural and directly involved in production as cultivators and labourers, so deflationary policies hit them hard in

20

mfc bulletin/February-March 2006 Table 11: States which have seen rise in the percentage of persons with less than 1800 calories intake per day during period 1983 to 1999-2000, and states which have registered fall but have had one-third or more of population below 1800 calories intake at either date

Andhra Pradesh Assam Haryana Karnataka Kerala Madhya Pradesh Maharashtra Tamilnadu West Bengal

RURAL 38th Round, 1983

RURAL 55th Round, 1999-2000

< 1800 calories Percent of total Persons

< 1800 calories Percent of total Persons

19.0 28.5 8.5 24.5 50.0 18.5 20.5 54.0 38.0

40.0 41.0 10.5 35.5 41.0 32.5 28.0 50.0 22.5

Source: Abstracted from estimates for all states, using NSS Reports No.471 and 454 for 55th round, and Report Nos.387 and 353 for 38th Round. Estimation method as in note to Table 10. Note that in 1983 only 3 states – Kerala, Tamilnadu and West Bengal had more than one-third of rural population below 1800 calories intake. By 1999-2000 all three states had improved, West Bengal substantially, while Andhra Pradesh, Assam, Karnataka, Madhya Pradesh and Maharashtra saw worsening. Thus by 1999-00, five states had more than one third of population below 1800 calories intake (six if we include the borderline Madhya Pradesh.).

both these roles of producers and consumers. Price deflation does not benefit even landless labourers since it is part of a process of income deflation, which raises unemployment faster than prices fall. Our economists estimating poverty by the indirect method are still caught in the old conceptual trap of equating relative food price decline with declining poverty, without understanding that the adverse unemployment effects of deflation can swamp out any benefit of food price fall: they should study the economics of the Great Depression for some insights into how deflationary processes actually operate. As Table 11 shows, by 1999-2000 as many as five states had one-third or more of rural population with less than 1800 calories intake, and in another three states the percentage of persons with below 1800 calories intake, had risen between 1983 and 1999-00, though not exceeding one-third at the latter date. (Note that Meenakshi and Viswanathan, 2003, obtain a larger number than we do, eight states with more than onethird of population below 1800 calories in the 55th Round – but their use of kernel density functions to obtain the calorie distribution ogive, is perhaps overestimating the nutrition poverty figures, since their method includes all high income but calorie deficient people as well).

Despite this worsening situation at the ground level being reflected in the nutrition data, it would be very sad indeed if the present Planning Commission is tempted to make further spurious claims of ‘poverty reduction’ as the previous ones had done, the moment the next large-sample NSS data on consumption becomes available. Their indirect method - which selectively uses the data by ignoring the nutrition part of it - is bound to show a further steep and spurious ‘decline’ in rural poverty by 2005-06, to around 18-19 percent of rural population from 27.4 percent in 1999-2000. This is because, owing to the unprecedented income deflationary situation itself, the rise in prices has been at a historic low between 2000 to date. The CPIAL actually declined in 2000-01 compared to the previous year, and rose only 1 percent the next year. With low inflation, the CPIAL- adjusted official poverty lines for 2004 works out to only Rs. 354, a mere Rs.26 or 8% more than the Rs.328 of 199900. (By contrast the CPIAL had risen 46% between 1993-4 and 1999-00) It comes as no surprise that the recently released 60th Round NSS data relating to January- June 2004, shows that only 22 percent of all –India rural population is below Rs. 354, the official price-adjusted poverty line, if schedule 1 is used, and only 17.5 percent is below it

mfc bulletin/February-March 2006 if schedule 2 is used.11 This is a share which is falling every year, solely because few persons can survive below such low spending levels – indeed it is amazing that there are people surviving at all on less than Rs.12 per day. One can imagine how adverse their height, weight, morbidity rates and life expectancy would be relative to the average. Of course, this alleged ‘decline in poverty’ will be necessarily associated with a further fall in the calorie intake level corresponding to the official poverty line, from 1890 calories to somewhere around or below 1800 calories, in short at least 600 calories below RDA. This information of declining nutrition standard associated with the official estimate is likely to be quietly suppressed as it has been in the past. The Government should bear in mind however, that any claims of ‘poverty reduction’ it might be misguided enough to make, will no longer carry credibility since the arbitrary and illogical nature of its method of calculation is today much better understood, and the contrast of any such claims, with all other adverse trends in the rural economy is too glaring to be ignored. Since such a large fraction of the population is already at very low energy intake levels, they have been trying to maintain consumption by liquidating assets against debt. Thus there are not only adverse flow adjustment (lowered nutrition levels) but also stock adjustments going on, reflected in the emerging recent data on rising landlessness. We may expect to see rise in the already high concentration of assets in rural areas. In such a scenario labour bondedness against debt is also likely to be increasing. The official refusal to recognize the seriousness of the crisis at the theoretical level, the consequent refusal to restore lost purchasing power through an immediately implemented universal employment guarantee, and the refusal to extend effective support to producers through continuing open-ended procurement at reasonable prices, all bode ill for the agrarian crisis, which is not being addressed. In fact the deflationary hammer has been applied once more on the rural population by the Finance Minister in the very first budget of the UPA government. The Tenth Plan, 1992 to 1997 sets out that Rs 300, 000 crores are to be spent by the Centre on Rural Development Expenditures (adding up as before five items).12 Three years of the Plan or two-thirds of the period is over: 11 Two schedules were canvassed for the first time for different sets of households in the 60th Round. Schedule 2 departs from schedule 1 because it uses a recall period of 7 days and not 30 days, for a range of consumer items. 12 Namely, agriculture, rural development, irrigation and flood control, special areas programmes, village and small-scale industry.

21 Rs.100, 000 crores or only one-third of the planned outlays have been spent, of which Rs.85, 000 crores spending was during the last two years of NDA rule, mid- 2002 to mid-2004, while there was a sharp cutback to Rs. 15,000 crores only in 2004-05. As in 1991 the first years after a general election are being used by the neo-liberal lobby in the new government, which controls finance, to apply mindless deflation although unlike 1991 there is deep agrarian crisis today. This cynical move to cut rural development expenditures in the face of rising unemployment and agrarian distress can only be in order to please international financial institutions and meet the arbitrary provisions of the FRBM Act. To achieve the 10th Plan target now, at least Rs.100, 000 crores must be spent both in 2005-06 and 2006-07, of which about 25 to 30 thousand crores should be on universal employment guarantee and 70 to 75 thousand crores on rural development expenditures. This level of planned spending would total only about 2.5 percent of NNP and it needs to be stepped up steadily in later years to reach the 4 percent of NNP which prevailed in the late 1980s during 7th Plan before economic reforms began. The entire false analysis which re-invents increasing hunger as voluntary choice, is today sought to be reinforced by bogus poverty estimates and invalid claims of decline in poverty. In such a situation it is the duty of all academics and activists who have not lost their sanity, to critique the official analysis and prescriptions, which if carried through will worsen immeasurably the already pitiable condition of the majority of the rural population. References BAKER D., G.EPSTEIN and R. POLLIN Eds, 1 9 9 8 , Globalization and Progressive Economic Policy Cambridge: Cambridge University Press. COONDOO, D., A. MAJUMDAR, G.LANCASTER and R.RAY, 2004 “Alternative Approaches to Measuring Temporal Changes in Poverty with Application to India” Working Paper, December CORNIA G. A., R. JOLLY and F. STEWART Eds 1987, Adjustment with a Human Face Vol.1, Oxford: Clarendon Press. DEATON, A. 2003a “Adjusted Indian poverty estimates for 1999-2000” ___________ 2003b “Prices and Poverty 19872000”. Both papers in Economic and Political Weekly Vol.38, January 25-31. HALEVY J., and J-M. FONTAINE, Eds, 1998, Restoring Demand in the World Economy, Cheltenham, UK: Edward Elgar. KINDLEBERGER, C.P., 1987, The World in Depression 1929-1939, Pelican Books MEENAKSHI J.V. and B. VISWANATHAN 2003 “Calorie Deprivation in Rural India” Economic and Political Weekly, Vo.38, Jan.25-31.

22

mfc bulletin/February-March 2006

MEHTA , J., and S. VENKATARAMAN , 2000, “Poverty Statistics – Bermicide’s Feast” Economic and Political Weekly, Vol. 35, July 1 NAYYAR, R., 1991, Rural Poverty in India (Oxford University Press). PATNAIK P. and C. P. CHANDRASEKHAR, 1995, “The Indian Economy under Structural Adjustment”, Economic and Political Weekly, November 25. PATNAIK, P 1999 “Capitalism in Asia at the end of the Millennium” Monthly Review 51, 3, July-August, Special number. PATNAIK , P 2000, “The Humbug of Finance” Chintan Memorial Lecture, delivered on Jan. 8, 2000 at Chennai, India. Available on website (www.macroscan.org), also included in P. Patnaik, The Retreat to Unreason (Delhi: Tulika 2003). PATNAIK, U., 1996 ‘Export-oriented agriculture and food security in developing countries and India’ Economic and Political Weekly Vol.31 Nos.35-37, Special Number 1996, reprinted in The Long Transition - Essays on Political Economy ( Delhi: Tulika, 1999 ). ____________ 2002, “Deflation and Deja-Vu” in Madhura Swaminathan and V K Ramchandran Eds, Agrarian Studies- Essays on Agrarian Relations in Less Developed Countries ( Delhi: Tulika) ___________ , 2003a “On the Inverse Relation between Primary Exports and Domestic Food Absorption under Liberalized Trade Regimes” in J.Ghosh and C. P. Chandrasekhar, Eds. Work and Welfare in the Age of Finance (Delhi: Tulika ) ___________, 2003b “Food Stocks and Hunger Causes of Agrarian Distress” Social Scientist Vol.31 Nos.7-8 July-August . __________, 2003c “Global Capitalism, Deflation and Agrarian Crisis in Developing Countries” Social Policy and Development Programme Paper Number 13, United Nations Research Institute for Social Development (UNRISD) October 2003 __________ , 2004a “The Republic of Hunger” Social Scientist, Vol.32, Nos.9-10, Sept.-Oct. __________ , 2004b “Alternative ways of Measuring

Poverty and Implications for Policy – A critical appraisal from the Indian experience” Draft paper presented at Conference on The Agrarian Constraint and Poverty Reduction – Macroeconomic Lessons for Africa” , Addis Ababa December 17-19, organized by The Ethiopian Economic Association and International Development Economics Associates. www.networkideas.org ___________ , 2005a “Ricardo’s Fallacy” in K.S.Jomo ed., Pioneers of Development Economics Dehi:Tulika and London & New York: Zed) ___________ , 2005b, “The Nature of Fallacies in Economic Theory” Satyendranath Sen Memorial Lecture delivered at The Asiatic Society, Kolkata, August 10 2004, forthcoming in the Journal of the Asiatic Society. RAM, R., 2004 “Poverty Estimates in India : A Critical Appraisal” M.Phil Dissertation submitted to Jawaharlal Nehru University, July. RAY, R. and G. LANCASTER, 2005 “On setting the poverty line based on estimated nutrient prices: condition of socially disadvantaged groups during the reform period” Economic and Political Weekly, Vol.XL, No.1, January 1-7. SEN, A and HIMANSHU (2005) “Poverty and inequality in India: Getting closer to the truth”, Economic and Political Weekly, January. SUBRAMANIAN, S., 2005 “Unraveling a conceptual muddle – India’s poverty statistics in the light of basic demand theory” Economic and Political Weekly, Vol. XL No.1, Jan.1-7. SUKHATME, P. V., 1977 “Incidence of Undernutrition” IndianJournal of Agricultural Economics, July-Sept. SUNDARAM, K.and S.D.TENDULKAR 2003 “Poverty has declined in the 1990s – A resolution of comparability problems in NSS consumer expenditure data” Economic and Political Weekly, Vol. XL, No.1, Jan.1-7 SWAMINATHAN, M., 1999 Weakening Welfare – the Public Distribution of Food in India (Delhi: Leftword Books) SWAMINATHAN, M., 2002 “Excluding the Needy - the Public Provisioning of Food in India” Social Scientist, Vol.30, Nos. 3-4, March-April.

“Scholars for 9/11 Truth” How is it that four large passenger planes could take off from the same airport, hijacked, and the ultrasophisticated defence systems of the USA could not inercept them; all four of them? How could the steelframed twin towers just sink (and not fall this way or that way) into their own basements after being hit by planes? What happened to the jumbo-jet which hit the Pentagon building and left only a small hole in the wall, with no trace of itself? An increasingly large number of scholars and scientists have come forward to challenge the LIES that the US adminstration has been pushing down the throats of people, at home and around the world. Documentries have been produced with impeccable evidence. The scholars’ reports could be accessed at: <www.scholarsfor911truth.org> and also at <http:// pdfserver.prweb.com/pdfdownload/338782/pr.pdf>. An influential group of prominent experts and scholars have joined together alleging that senior government officials have covered up crucial facts about what really happened on 9/11. The members of this new non-partisan association, “Scholars for 9/11 Truth” (S9/11T), are convinced their research proves

the current administration has been dishonest with the nation about events in New York and Washington, D.C. These experts contend that books and articles by members and associates have established that the World Trade Center was almost certainly brought down by controlled demolitions and that the available relevant evidence casts grave doubt on the official story about the attack on the Pentagon. They believe that the government not only permitted 9/11 to occur but may even have orchestrated these events to facilitate its political agenda. And then, there are two documentaries recently produced: 1. Loose Change, produced by Korey Rowe and directed by Dylan Avery. You could go to the following link to know about it, and order a copy: <http:// www.loosechange911.com/>. For clippings of this documentary as shown by Fox40 (WICZ-TV) Binghanton, please go to the following: <http:// www.wicz.com/fox40 video.asp?video=%2F12% 2D02+Terror% 2DConspiracy%2Ewmv>. 2. Painful Deceptions, 2 hours, NTSC format, created by Eric Hufschmid <http://www.erichufschmid.net/ ThePainfulDeceptionsVideo.html>.

mfc bulletin/February-March 2006

23

Cost and Quality Considerations in Secondary and Tertiary Health Care -Debabar Banerji1 (Note: I have picked up some suggestions for discussion at the Vellore Annual Meet of the MFC. This is meant to be a discussion document. MFC has commissioned two overview papers for the Meet. This, at most, is meant to serve as a secondary document. This brings up three interlinked issues: secondary and tertiary level hospitals; scope of health care; and, low cost and good quality hospitals. Comments and criticism will of course be most welcome.)

the revolutionary thinking of primary health care. This was made explicitly clear by the then Director-General of WHO, H T Mahler when he participated in a Conference on the Role of Hospitals in Primary Health Care held at Karachi as early as in 1981. Secondary and tertiary curative institutions, which are tuned to the epidemiological, socio-cultural and economic needs of the people, form only one element of the approach to primary health care. These institutions cannot be seen in isolation.

Secondary and Tertiary Level Hospitals Use of the terms secondary and tertiary level hospitals point to a three-tier system of curative services. First line institutions such as dispensaries at primary health centres (PHC) and elsewhere form the first of the three tiers. This distinction between a three tier curative system and a PHC and indeed the Primary Health Care, as envisaged in the Alma Ata Declaration of 1978, is not merely a terminological and academic hairsplitting. It has important operational significance.

Secondary level hospitals account for a vast majority of the beds and are of very great variety, starting from the beds in PHCs, to those in Community Health Centres to taluk/tehsil/sub-divisional hospitals to district hospitals to general hospitals and most of the teaching hospitals in urban areas; only a tiny proportion of them fall in the tertiary category. However, as they require highly skilled personnel and expensive equipment; they are necessarily very expensive. Medical Care and Health Care

The PHCs, set up in 1952, were envisaged to reach out to an assigned community with a package of preventive, promotive and curative health services. The philosophy of primary health care of 1978 envisaged a revolutionary change in public health thinking where the top position is assigned to the people, with those running various elements of health services occupying the bottom positions subordinate to the requirements of the people. It sought to virtually bring up-side down the organizational structure of the health services, According to this thinking, starting from home remedies, other endogenous practices such as those by local healers and practitioners of indigenous medicine get primacy. A locally elected/selected Community Health Worker, who acquires additional training to increase the coping capacity of the people forms an important element of this process of community empowerment. Inter-sectoral action and social control over the community health services, which envisages use of appropriate technology, incorporation of traditional systems of medicine, an integrated approach to health services and coverage of the total population, had been the components of 1 Professor Emeritus, Centre of Social Medicine and Community Health, Jawaharlal Nehru University, B-43 Panchsheel Enclave, New Delhi 11001. Email: <nhpp@touchtelindia.net>

When the term ‘health care’ is used along with secondary and tertiary hospitals, it gives it a considerably extended meaning to the term. Health care includes much wider issues, such as nutrition, housing, sanitation, occupation and income and so on. The great German medical activist had observed in 1848 that ‘health is nothing but practice of politics on an extended scale, as if people matter’. Even the term ‘medical care’ along with the discipline of hospital administration links the clinical practice in hospital to wider issues which transcend its four walls, such as taking account in clinical practice of living conditions of the patients, the family relationships, dietary practices and so on. Recognising the importance of taking a wider view, John Ryle (of Ryle’s Tube fame) gave up his chair of medicine in London University to take up the post of the Professor of Social Medicine at Cambridge. When asked why he ‘abandoned’ clinical medicine, he observed that he had not abandoned medicine: he had merely extended the etiological horizons of medicine. Regionalisation of medical care services form another important component of medical care. It implies what is called ‘to and fro movement of patients, medical personnel and records’ from hospitals of different levels of sophistication to ensure more effective utilization of limited resources.

24 Reconciling Low Cost Interventions with Good Quality Health Care This forms the culmination of this very short presentation. It is woven round the research question articulated by Anant Phadke in his circular letter to MFC members: How to make most effective use of the very limited resources to provide good quality health care to the needy persons in the country or a community? Towards this end, he has raised two significant sets of issues in that letter: 1. “You are specifically requested to contribute any experience of reducing the cost of care without reducing quality. It could be avoidance of unnecessary investigations, unnecessary medication, use of simpler, cheaper materials, use of certain management techniques etc. etc. Experiences at secondary, tertiary levels of care are especially welcome.” 2. “Backgrounders of specific experiments and experiences should form the basis for discussion, e.g.:Low Cost Effective Care Centre, Vellore; Community-based Palliative Care Network, Manjeri, Kerala; Arvind Eye Care Centre, Madurai; Jan Swasthya Sahyog, Bilaspur; Trauma Centres; Drug Procurement System - Tamil Nadu. Glass vs. Disposable Syringes: Dais vs. Institutional Deliveries; and Mitanin Programme, Chhattisgarh”. Considering we have the gigantic problem of meeting some of the basic health needs of hundreds of millions of our people, there is apparently a crying need for carrying out much more organized and extensive research on this line of ‘optimizing the use of limited resources’ for health service development in our country. This ought to involve use of such sophisticated research tool as operational research and systems analysis. This, as such, is an enormous task. This task has been made even more formidable by the powerful globalization and liberalsation driven market forces which have led to the formation of the so-called Great Indian Middle Class. This class has to be confronted.squarely. It is more interested in promoting such ventures as medical tourism, setting up of an AIIMS in each state and providing oxygen kiosks. The setting up of the Indira Gandhi and Sanjay Gandhi Postgraduate Meidical Institutes, respectively. at Patna and Lucknow, are examples. The ICMR too has shown little interest in research on low cost health services. Even when impelled due to political compulsions to raise the GDP allocation for public health services from the dismal 0.9% to ‘2-3%’, this class came up with the ill-conceived National Rural Health Mission, which is already showing signs of a

mfc bulletin/February-March 2006 breakdown. In a rare constellation of political and social events, which need not be gone into here, a number of steps were taken to bring health services, including those providing secondary and tertiary hospital service nearer to the people in the 1950s and 1960s. I frequently quote Milan Kundera: “Man’s struggle against oppression is a struggle between memory and forgetfulness…” I venture to end this by ‘reminding’ many MFC members about three events of low cost good quality research achievements from the field of tuberculosis programme of that period: 1. In 1956, the Tuberculosis Research Centre, Madras demonstrated that home treatment of tuberculosis patients was as efficacious as that done in sanatoria and thus brought down the cost of treatment to at least twenty-five times – from the then cost of Rs 2500 in sanatoria to Rs 100 at home. It had global a major impact. 2. By imparting sociological dimensions to the epidemiology of tuberculosis at the National Tuberculosis Institute (NTI), Bangalore in the early 1960s, it was shown that a large section of tuberculosis patients in the community had already been seeking alleviation of their suffering in various public health institutions in the country and that they can be diagnosed by a mere sputum examination costing a few paise, as against the then cost of more than a thousand rupees through the now well accepted poorer quality diagnosis by mass radiography. This too had a major global impact and WHO had to accept the NTI approach to case finding and diagnosis in its otherwise discredited vertical DOTS programme. True to their intellectual arrogance, scholars from the rich countries did not show the grace of acknowledging these contributions from India. 3. While conducting team training programme for implementing the National Tuberculosis Programme at that time, we used to pose the following question before the trainees: If you have Rs 1000 for dealing with the 100 patients in your community, who have a felt need, will you (a) spend that amount in treating one out of them through thoracic surgery and leave the remaining 99 patients to die? Or (b) treat 4 of them in sanatoria, and leave the rest of 96 patients to die? Or (c) treat all the 100 in a reasonably supervised domiciliary regime? This raises a profound ethical question. But the market is an unethical institution par excellence!

mfc bulletin/February-March 2006

25

Misplaced Faith? Arti Sawhny1 The decision to publish this note about my recent experience of receiving health care has been difficult to arrive at, as there is the possibility of drawing attention to the concerns of a single individual, and to myself, rather than the larger issues at stake.2 I have deliberately left out specific details of the related institutions, as I do not want to divert, confine and limit the focus to these. For I believe what I went through in these months, reflects an all-pervasive malady within the health care delivery system in the Indian context, and I sincerely want that to be the center of focus. I recognize the need for intervention, and all efforts, big or small, matter. I feel the readers may relate to parts of this experience and may want to take it forward in their own way. Having gone through a lifetime of asthma, and having experienced asthma in its severest form on several occasions, I am deeply convinced of the importance of the non-medical aspect of asthma cure. I therefore want to focus on and emphasize these aspects, not merely as additional psychosomatic support, but as a vital part of the treatment of asthma. As a member of the medical fraternity and as an asthmatic patient I have been very disturbed at the manner in which asthma is handled. In fact, on some occasions the level of anxiety and panic generated by the medical professional has led to a worsening of my health situation and to a consequent need for aggressive intervention, which I believe could have been prevented. What has been revealing is that irrespective of the nature of the institution, i.e., governmental, non-governmental (missionary) or private, the experience was the same. This is amply illustrated through some of my recent experiences within the last six months. Non-Governmental Institution Roughly six months ago, I was in a state of severe asthma and all forms of intervention failed to relieve the broncho-spasm. The difficult decision of where to shift me had to be made. Having experienced the 1

The author, a graduate of MAMC Delhi, has worked for over 25 years in Rajasthan and is based in Ajmer. She has worked with the community health programme in Tilonia, and later with the Sathin movement. Email: <artisawhny@yahoo.co.in> 2 It was when I heard from Sathya about the positive response of many of the members attending the MFC annual meet on ‘Quality of Health Care’ where this paper was circulated (while maintaining anonymity), and because of my continued distress at the state of affairs, that I have decided to go ahead and share this experience with a larger audience.

callousness of government hospitals on several occasions, I chose to avoid the ordeal and decided to go to a missionary institution, since, commitment to basic Christian values of service with love exist in these institutions and nursing care was relatively good. I arrived with severe bronchospasm and was quickly rushed to a private room. I received reassurance from the nursing staff and the director and preliminary care was attended to. The doctor arrived soon after, and the first thing he said was, “Very bad, the spasm is very severe.” Further examination only added to this conclusion. A quick study of the medical papers led to the pronouncement that I was steroid dependent and so a very simple formula of using every available drug in maximum doses was resorted to. Instead of the conventional steroids it was decided to use the more recent preparation of Solupred (methyl prednisolone) in a dose of 250 mg, 6 hourly, which was later, increased to 500mg. I have been since told that the recommended dose is to start with 40 mg and increase gradually to 150 mg, and in exceptional cases go up to 250mg. This medication was given along with other anti-asthmatic drugs, such as adrelanine, aminophylline and antibiotics. Clearly the doctor was calling the shots. In a very short span there was a complete takeover, and despite the fact of being a medical person, I was reduced to a state of total helpless and was at the mercy of the doctor. In retrospect, I can attribute this to being handicapped on account of my physical state, a situation that was greatly aggravated by the level of anxiety generated, and by my not being familiar with the newer forms of steroids. Later, when the drugs failed to produce the desired results, instead of analyzing the situation, the dosages were doubled. We had by then realized that the room was stuffy and did not have enough air. A decision to shift me to another room was taken only when my sister suggested it. However, all the aggressive intervention did not work. Finally, when everything failed to control my asthma it was decided to shift me to a Delhi hospital. This experience lead me to realize that the dedication and commitment of the missionaries was clearly being held to ransom by private doctors, for whom the medical profession seems to have provided a license to practice without any accountability or ethical norms; with the lay patient being completely left to the mercy of God. The Government District Hospital-Attached to the Medical College.

26

mfc bulletin/February-March 2006

Being on heavy doses of steroid greatly increased my vulnerability and it did not take very long for me to suffer chest infection again after a few months along with severe asthma. I was caught in a cycle, which was repeating itself with added severity. Each time we had to make the difficult choice of which medical institution to go to. Having experienced the ineffectiveness of the missionary institution, the choice this time, fell on the medical college.

on in the ICU only because I was a Doctor with influence. There is no record of what happened to those who had been discharged.

We arrived in the casualty after 9.30 pm at night. I was in a critical condition, literally gasping for breath. A group of doctors was huddled around a table, but to our utter dismay no one moved. There was total complacency and the group swung into action only on discovering that their patient belonged to their fraternity. Activating the doctors was, however, only a small achievement as we discovered, to our horror, that no medicines were available in the casualty and even emergency drugs had to be purchased from the market. That an oxygen cylinder was available was a huge relief! I was also grateful for owning my own nebuliser as I discovered the same mask being used by different patients, some of who were suffering from lung infection. It was a long wait for the senior doctor, and the treatment to be finalized. By this time the nursing staff on night duty had retired and had locked herself up in a room to sleep. In the morning I got to know that although the aminophyline drip had not been administered it had been recorded in the case sheet that the patient had refused.

A Metropolitan Hospital (Private Trust)

In order to be provided better care, especially since I was a doctor, it was decided to shift me to the ICU, which had been newly constructed. So I was taken to the ICU only to be confronted by a corpse being wheeled out. I had to deal with the sheer horror of being put on the same bed from where the dead person had been removed just minutes earlier, as that was the only one available. The place had not even been cleaned properly, though thankfully the sheets had been changed. Added to this was the dismay of discovering huge rats running around the ICU. There was no bedpan available, every patient had to purchase his or her own, and the condition of the toilets was beyond description. The toilet was located a long distance away from the ICU and for a person with breathlessness it was a heroic struggle to go there and return. The implications of these unsanitary conditions for someone on heavy steroids didn’t seem to matter. The final climax however, came when all the patients were suddenly discharged, because the President of India was visiting and there were standing government orders to keep vacant beds ready in the ICU and the emergency, incase of an eventuality of a terrorist attack. We were later told that roughly 40 patients in critical conditions were either shifted to wards or discharged. I had managed to stay

I was in the ICU for two days by which time it was more than clear that the only way to survive was to get out of there and travel to Delhi; a journey, which in my critical state amounted to huge risk but seemed to be the only choice.

I was shifted in a state of status asthmaticus, after having spent two days in the ICU at the Medical College hospital. I was brought in an ambulance, through a journey, which lasted several hours, and included being struck in a traffic jam as one entered the metropolis, by which time my condition had really deteriorated, and I was in a critical condition with hypoxia. At the Hospital there was immediate emergency care at the Casualty, then a shift to a room, and finally I was whisked away to the Medical ICU. Within minutes of entering the ICU curtains were drawn around me, I was shifted to Bed No 3 (which became my identity for the next 4 to5 days). Four to five nurses descended on me. I was immediately stripped naked, all my clothes removed and I was uniformed. My earrings, rings, cross etc were all pulled off and a bundle handed over to my sister, who was then asked to leave. I had been stripped of my identity, my dignity and reduced to a complete non- entity, with absolutely no support system. To make things worse when the curtains were finally drawn I discovered to my dismay that I did not even have the empathy of other similar patients. I could not build any silent bonds, as I was the only conscious patient in the entire ICU. All the others had surrendered to machines. This was extremely traumatic. Every bit of me was revolting. I felt as though I just would not be able to make it through this utter aloneness. I did over the next few days build bonds with the nurses and ward boys and finally it was their smiles and friendly gestures that helped me to relax. In fact the sensitivity shown by some of the sisters is something I am extremely grateful for. A lot of my anxiety revolved around the bedpan and this was handled sensitively. The problem however was that since I was the only conscious patient all the other patients automatically needed greater care and scored over me. Here I would like to state that I have for the last thirty years worked in challenging situations of deprivation and poverty, which have greatly enhanced my coping capacities and levels of endurance. I shudder to think of the detrimental health impacts such harsh and dehumanized ICU conditions can have on other patients who are more vulnerable than I am. In addition to the

mfc bulletin/February-March 2006 overall condition some of the things, which made my stay in the ICU difficult, were: • I was being sponged with cold tap water at 3 am. When I asked for warm water I was told this was available only later in the day, and the routine required sponging of patients before 5 am. For some one with a severe lung condition this was harsh and harmful. • It took four days for me to receive a pyjama. • There was no toothbrush or paste and I was given only a mouthwash. My family would have been happy to provide the same. • There was no possibility of a hot drink after 7 pm, right up to 7:45 am the following morning. I did on the second night ask for flask of hot water, but I did not receive any. Let me point out that this was not an indulgence but an essential requirement for me. • Within the restrictions imposed by the ICU, brief meetings with my relatives in the morning and evening assumed great importance. These were however, tense meetings with the security guards hovering around timing the few minutes that we spent together. There was almost a complete lack of possibility of communication with the outside world or family for conscious patients within the ICU. This was stressful and led to feelings of being imprisoned and isolated. • Finally, as I have already mentioned, there was no possibility of receiving support from fellow patients who were all comatose. This was extremely difficult to handle, and raises questions about putting conscious patients along with unconscious ones. After five days in the ICU I began to wonder if I had been convicted for a crime, and for how long would this harshness continue. Finally when I did receive orders to leave, all my belongings were bundled together in a callous manner. I was brought to a private room, with a bag of broken injections, spilt cough syrup, and even the water from my steam inhaler had not been emptied and had soaked the medicines I had carried with me in a polythene bag. The final bill for a stay of 11 days came to approximately Rs1, 20,000/I must also add that in this hospital, I am lucky to have access to a group of doctors who are dealing with me with sensitivity, as a person and not as separate body parts .It is their concern and professional skills, along with their constant support and ease of access that have been pivotal in my recovery. I had sent the Director of this hospital a letter describing my ordeal in the hospital so that it may improve the situation for the other patients. However, all that I received was a brief, polite, ‘we will look into it’, kind of a response.

27 Childhood Reminiscences As a little child when modern asthma cure was still not available and ephedrine was the most commonly used drug, I have a memory of sitting up all night troubled by the terrible wheezy noises, the tremors in my hands caused by medicines and the sheer discomfort of not being able to lie down because of breathlessness. I remember the long waits through the night for the first signs of daybreak. I remember the encouragement I received from my mother who insisted I would manage going to school, and almost invariably despite my tiredness, I did, and I used to return feeling much better. I remember a time when I had asthma and we went mountain climbing. I managed to climb with the faith my mother placed in me. I recall never letting the asthma come in the way of my doing anything. Where am I today? Even with all the advancement that science and technology has made and the modern cures, which have revolutionized asthma cure, I have been reduced to a dependent state affecting my activity level, which has now been severely restricted and I am almost house bound. I am therefore forced to ask isn’t something very wrong? Are we not missing out on something very basic? Concluding Concerns This specific experience with asthma would apply to a range of diseases, most specifically to those that are chronic with sometimes progressive disability. I have put my experiences down on paper only to bring to the forefront all those experiences of so many others, which fade into silence. The fact that this happened to someone from within the medical profession shows the degree to which the malady pervades the health care system today. That even a medical doctor was not spared the trauma, despite the knowledge, status and position, speaks of the extent to which the rot has set in. If, what is described was the fate of an empowered person, it is difficult to imagine what happens to those with no support economically or socially. Quite clearly the medical services are now being governed and dominated by market forces. Even medical doctors are increasingly being motivated by monetary concerns. This has lead to increasing the gap between modern treatment, cures and actual healing .The impersonal and mechanical interventions are now increasingly becoming a trap, it is becoming more and more difficult to extract oneself from their clutches. Under these circumstances for the Indian poor, who need the health care system the most, poverty ironically almost ends up becoming a saving grace; as they then fall back on their traditional systems of knowledge and healing, however inadequate these may be. It is a situation, wherein, to the struggle to survive poverty and the disease is added the struggle to survive the health care delivery system. New Delhi, 22.02.2006

28

mfc bulletin/February-March 2006

‘Hunger and Health: An Interdisciplinary Dialogue’: Joint Statement from a Workshop This is a statement following a workshop on ‘Hunger and Health: An Interdisciplinary Dialogue’1 attended by a cross-section of India’s nutritional scientists, health professionals, public health specialists, economists, agriculturists and grass root activists. Chronic and widespread hunger resulting in misery, disease and death, is entirely avoidable in a country like India with overflowing food stocks. The causes of such hunger have to been seen in the context of the production, distribution and price of food and the purchasing power of the rural and urban poor. The issue has implications for the health and well-being of our people, especially poor people, whose labour drives our economic growth. A balanced diet in adequate quantity is central to the health, growth, and development of human beings. Poverty, which impacts on the availability of food, is currently the biggest determinant of ill health in India and is intimately linked to several major public health problems, the most important being tuberculosis. India’s most prevalent and persistent public health problem, across ages, today is undernutrition. According to the UNICEF, every third malnourished child in the world lives in India and the average nutritional level of our children is worse than that seen in Sub-Saharan Africa. It is clear that this level of undernutrition contributes to a very high burden of communicable and non-communicable diseases, with high rates of morbidity, and premature and avoidable death in children and adults. Undernutrition is a cause as well as an effect of poverty: it affects cognitive development and work capacity, itself, a result of widely prevalent anemia. Undernutrition also extracts severe economic and social costs by contributing to illnesses. An estimated 300,000 children, for instance, are forced to drop out of school every year in India, because of an illness like tuberculosis in the family. Disturbing Trends in Nutrition in India A review of the national trends in nutrition over the past four decades is a sad commentary on our increasingly iniquitous model of development: we find levels of undernutrition have remained the same in large sections of our population. In our most disadvantaged classes, that is, dalits and adivasis, who together comprise 25 % our population, weight at birth and mean weights and heights at any age thereafter have not improved at all in the last 40 years. In rural areas, more than 40% of all adult women of all sections(nearly 50% of the poor)have a Body Mass Index (BMI, a measure of weight that takes height into consideration) below 1

Organised by Jan Swasthya Sahyog at Village Ganiyari, District Bilaspur, Chhattisgarh on February 10-11, 2006 2 National Family Health Survey-2, 1998-99. International Institute for Population Sciences, Mumbai.

18.52 . In a healthy society according to the World Health Organisation this figure should be less than 5%. This points to a critical situation indicating chronic starvation. In India we have exchanged the occurrence of famine with an enduring occurrence of chronic hunger. In fact, hunger, manifesting as undernutrition, is so pervasive amongst the rural and underprivileged sections of our population that it appears to be ‘normal’ in the eyes of health professionals, administrators and policy planners. This is possibly one of the reasons why undernutrition has never been accorded its rightful importance in the enumeration, analysis and management of illnesses in our country, and a deafening silence surrounds all these issues. Extent of Disease Caused by and/or Aggravated by Hunger in India It has been recognised by the scientific community for the past four decades - non-medical people have always known this to be true- that undernutrition predisposes to diverse illnesses. Undernutrition also causes many diseases to occur in a more severe form. Many of these illnesses, in their turn, perpetuate undernutrition, constituting a vicious cycle that often ends in death. Globally, undernutrition contributes to 55% of the deaths seen in children due to common illnesses. In India, the percentage of childhood deaths that can be attributed to undernutrition is 69%, which is the highest for any country in the world. There is evidence that hunger or undernutrition contributes significantly to adult deaths too. Despite these facts being well known, we are disturbed to find undernutrition being completely ignored in the planning and implementation of control efforts against major diseases of public health importance. A case at point is tuberculosis (TB). TB is the single largest killer disease in India, leading to the premature, avoidable and therefore unacceptable, death of nearly half a million people every year. Infection with the tuberculosis bacillus, Mycobacterium tuberculosis, is widespread in India, with 40% of the population harbouring the bacilli inside their body in small numbers. However, 90% of these people never suffer from tuberculosis since the immune system is generally capable of keeping the bacteria under check and the infection therefore remains latent throughout their lifetime. Only about 10% of infected people ever develop the disease when their immune system is weakened by different factors, of which, undernutrition is the commonest. Globally, it is Nutritionally Acquired, and not HIV Acquired Immune-Deficiency, that is the leading cause of a weakened immune system. Fortunately, unlike HIV infection, undernutrition is completely preventable and correctable. Therefore, improved nutritional status could greatly reduce the chance of these large numbers

mfc bulletin/February-March 2006 (400 million) of infected people ever developing the disease. The improvement in nutrition would simultaneously protect people, especially children, from many other infectious diseases. It is urgently necessary, in light of these facts, to improve the access of our population to food, which is the most versatile vaccine already known to mankind. In the developed world, the prevalence of many infectious diseases and the rates of death there from declined with improvements in nutrition and living standards, much before modern drugs or vaccines were discovered. 3 While acknowledging the historical role of some vaccines in reducing suffering and death, we express strong reservations about the current obsession with vaccines of all kinds, micronutrient supplements and other purely technical solutions for health at the cost of, and as a substitute to basic public health measures in the form of nutrition, safe drinking water and sanitation. Apart from communicable diseases, we note that rural India has a significant and unmet load of noncommunicable diseases such as hypertension, diabetes, and coronary artery disease, apart from cancer and respiratory illnesses, which has been acknowledged by bodies like the ICMR, in their recent documents.4 We have discussed new evidence5 that links maternal and foetal malnutrition and the weight at birth to a higher lifetime risk of non-communicable diseases such as hypertension, diabetes and coronary artery disease. We feel that the ongoing epidemic of chronic hunger and malnutrition in our country, coupled with the psychological stress of living in deprivation, has enormous implications for the emerging epidemic of non-communicable diseases. Improving the level of nutrition would therefore improve health across generations, leading even to a lower prevalence of noncommunicable diseases in the poor. Illnesses occur more frequently in the poor and the undernourished and lead to a vicious cycle of greater poverty and more illnesses through the loss of wages, disability and death. The rising cost of illnesses to the poor is leading to widespread indebtedness, liquidation of meagre assets including land and permanent descent into poverty. Studies in two major states in India have shown healthcare costs to be the second commonest cause of destitution in rural areas. Our surveys show that an increasing number of people are not accessing any healthcare at all for purely economic reasons. To 3

See Sheila Zurbigg, “ Rethinking Public Health: Food, Hunger, Mortality, Decline in South Asian History” where it is shown that in UK mean annual death rate of TB declined from 4000 deaths per million to blow 500 from 1838 to 1950 before discovery of TB medicines and similarly for measles, whooping cough, etc. 4 Assessment of Burden of Non-communicable Diseases. ICMR.2004. 5 Barker DJP, Eriksson JG, Forsen T, Osmond C. Fetal origins of Adult disease. Strength of association and biological basis. Int J Epidemiol 2002; 31:1235-1239. Also, Harding JE.. The nutritional basis of the fetal origins of adult disease. Int J Epidemiol. 2001;30:15-23

29 prevent a worsening of the situation, it is essential for the government to address the social and economic determinants of ill health, to strengthen the public health system in the rural areas and to regulate the cost of drugs and services provided by the private sector. Alarming Decline in Food Grain Availability: Worsening of Hunger in the Presence of of Surplus Food Stocks The workshop also dealt with the problems of agriculture, food availability and its results on the nutritional status of the people. We are extremely concerned about the serious decline in the average annual per capita availability of food grains since the early 1990s. This trend worsened from 1999 with the average dropping from 177 kg to 154 kg, wiping off whatever meagre gains in food availability had been achieved since Independence. This steep fall has entailed a sharp increase in the number of people in hunger, particularly in rural areas. We should remember that 154 kilograms is an average for the entire country and the amount available to the rural poor is actually even lower. In contrast, it is sobering to realise that the average food grain absorption/availability is over 850 kg in USA, 650 kg in Europe and 325 kg in China. Given this background of decreasing food grain availability, we are concerned by the promotion of micronutrients in health and disease as a substitute for availability of wholesome nutrition. The forces unleashed in India by the neo-liberal economic reforms package have led to an alarming decline in public investment in agriculture and have precipitated large-scale agrarian distress, the most acute manifestations of which are farmer suicides across the country and the chronic manifestations are of mass migration to the urban centres. Its impact on the health of the people is apparent and the already serious health situation seems to be poised at the brink of a crisis. There has been a stark decline in the food purchasing power of rural people over the last decade that has been responsible for the large surpluses of food stocks accumulating all over the country. The public distribution system (PDS) for food is the only source of food grains for large sections of the people, which, in view of the crisis in nutrition, needs to be retained in the public domain and strengthened with an increase in entitlements without bar. We think it will be catastrophic if the PDS is privatised, food prices are increased and entitlements reduced, as is being envisaged. We strongly oppose measures to subvert the PDS and dismantle the food supplementation programmes as is being done in certain states. However, at the same time, we seek to protect indigenous food and crop biodiversity and promote traditional agricultural practices that are sustainable in nature. Agriculture and the Threat to our Food Security, Diversity and Soil Health The system of food production promoted from the mid1960s has created serious anomalies in the diversity, production, distribution and availability of food grains

30 in our country and had a deleterious impact on the nutritional status of rural people. Side by side, alternative models of food production and distribution that are decentralised, community-controlled, low-cost, and based on the preservation of biodiversity and soil health have been ignored and marginalised. The ‘green revolution’ model promoted agricultural methods that were dependent on high external inputs and were limited to a few crop species. It also concentrated production in limited geographic areas and concentrated state resources to these areas. The production and marketing of pulses, coarse grains, millets and related were affected, thus degrading the farming systems and knowledge of farmers and adivasis on the one hand and the quality of the diet on the other with the decreased availability of pulses and millets. Erosion of food biodiversity, degradation of soil health, and pollution of the food chain with pesticide residues have been the other problems associated with this model. A reversal of this situation is needed through the promotion of diversified food production in dry land and rain-fed areas with state support to increase wage employment and regenerate wastelands with an increase in biodiversity. We express our concern at the propagation by transnational corporations and their Indian counterparts, of untested technologies and their unregulated use, under the rubric of biotechnology, with an attendant grave threat to food security and sovereignty. On the other hand, the potential of organic farming has now been tested and proven in many ways all over the country, one of the most prominent examples being the Madagascar system of rice production or the System of Rice Intensification. Poverty and the Question of the Poverty Line We have deliberated on the question of poverty and its measurement by the government. The official claim by the Government of India of the number of people below the poverty line (BPL) having gone down flies in the face of the reality of falling food grain consumption and widespread prevalence of starvationlevel body weights. On closer inspection, we also find that the method used for estimating the number of persons BPL is based on assumptions that are completely unscientific and irrational, if not deliberately misleading. Direct inspection of NSS data of the calorie intake corresponding to the quantities of foods consumed by persons in the various per capita expenditure groups, reveals a totally different picture: by 19992000, seven-tenths of the rural population was below the norm of 2400 calories per day (the norm originally adopted in all poverty studies); about one-tenth had an intake around the norm; and only one-fifth had an intake above the norm. This means that at least seventenths of the rural population was in poverty in 19992000. About two-fifths of the urban population was below the lower urban norm of 2100 calories. These are disturbing implications - both in terms of the

mfc bulletin/February-March 2006 extent of immiserisation of people of India as well as the complicity of policy makers, and scholars, in denying harsh realities. (It was estimated in 1973-74, to purchase the minimum monthly equivalent 2400 calories per diem in rural areas and 2100 in urban areas, one would need a poverty level income of Rs.49.10 for rural and Rs 56.60 for urban areas. In order to estimate the poverty for later years, it was assumed that the quantities people consumed, and thus the pattern of consumer expenditure, remained unchanged from 1973-74. Thus a price index was applied to the old poverty line to update it. Thus the indirectly estimated official poverty line of Rs.328 per month (Rs 11 per day) in 1999-2000 corresponds to less than 1900 calories per diem. The direct estimate gives a poverty line at Rs.567 per month (Rs 19 per day), over 60% higher than the official one. This means far more people are below the poverty line, that is far more people than the official estimates would have us believe, are consuming less that the required levels of calorie intak.) Indeed, because of lower calorie consumption over the years, a large segment of the rural masses in India have been reduced to the nutritional status of sub-Saharan Africa. On the basis of the NSS data on calorie intake for 1999-2000, one finds that about 40 % of the rural population was at the low absorption level of the subSaharan average. By 2001, there has been a disastrous slide-back to the low level of 151 kg per head food absorption in rural areas, a level not seen for fifty years. And preliminary data further indicates that by 2005-06, the situation has only worsened. We question the practice of calculating minimum wages on the basis of cereals alone, something that has kept the rural poor chronically undernourished in terms of access to other vital ingredients of a balanced diet. Right to Food Campaign Decreasing food consumption in the country is central to our concerns. We would like to look at this issue holistically within a framework of not just the right to food but also of food security and food sovereignty. We endorse the efforts of the Right to Food Campaign in mainstreaming the discourse on the right to food in India and we are committed to finding ways of working more closely with the campaign in bringing chronic hunger and its underlying causes back to the domain of public attention. We note that it is as important to delineate the relationship of widespread and chronic hunger with long-term morbidity and mortality as to focus on acute starvation-related deaths. We welcome the interventions of the Supreme Court in creating entitlements for addressing the problem of chronic hunger. We also welcome the initiatives of the Government in this regard, in particular the National Rural Employment Guarantee Act (NREGA), which could potentially transform rural livelihoods as well as the universalisation of the ICDS and the Mid-Day Meal

mfc bulletin/February-March 2006

31

Schemes. We fervently hope that State Governments will not dilute the provisions of the NREGA and instead promulgate more progressive legislation to ensure that the spirit of the employment guarantee is fostered.

14. 15. 16. 17.

At the same time, we are deeply concerned over the systematic attempts to reduce the entitlements of the poor as was done recently by the Government in reducing food grain quotas. We stand committed to a PDS that is universalised and addresses food sovereignty concerns. We urge the government to create entitlements for the availability of pulses, indigenously produced oils as well as locally available millets. We are also concerned by the continued and large-scale diversions of food grains meant for the poor and the continuing export of food grains despite the existence of starvation in large parts of the country.

18. 19. 20. 21. 22. 23. 24. 25.

To conclude… 26.

We urge the government to consider these serious issues as an integral part of the developmental plan for the equitable and sustainable economic growth of our nation. We should never forget that a nation of undernourished, anaemic and diseased children and adults with one of the lowest per capita food consumption in the world cannot become a strong economic power. There is a need to focus on sustainable agriculture in our economy. There is a need to focus on nutrition as a critical determinant of the health of our people. There is a need to consider our abysmal nutritional and public health indicators while drawing up the developmental balance sheet of our nation, alongside the economic indicators that apparently shine so bright. If we continue to choose to ignore these issues, it will only be at our collective peril.

27. 28. 29. 30. 31. 32. 33. 34. 35. 36.

List of Signatories 1. 2. 3. 4. 5. 6. 7. 8. 9. 10. 11. 12. 13.

Prof. Utsa Patnaik (Centre for Economic Studies and Planning, Jawaharlal Nehru University, New Delhi) Prof. Imrana Qadeer (Centre for Social Medicine and Community Health, JNU, New Delhi) Dr. Veena Shatrugna (Deputy Director, National Institute of Nutrition, Hyderabad) Dr. Sultan Ismail (Vice-Principal, New College, Chennai and Director, Ecology Research Foundation Dr. Yogesh Jain (Paediatrician, Village Health Programme, Jan Swasthya Sahyog, Bilaspur) Dr. B. R. Chatterjee (Founder President, Jan Swasthya Sahyog, Bilaspur) Mr. Jacob Nellithanam (Consultant Organic Farming, Jan Swasthya Sahyog, Bilaspur) Dr. Binayak Sen (President, Chhattisgarh State PUCL) Mr. Dunu Roy (Hazards Centre, New Delhi) Dr. Anurag Bhargava (Physician, Jan Swasthya Sahyog, Bilaspur) Dr. Raman Kataria (Surgeon, Jan Swasthya Sahyog, Bilaspur) Dr. Biswaroop Chatterjee (Microbiologist, Jan Swasthya Sahyog, Bilaspur) Dr. T. Sundararaman (Director, State Health Resource Centre, Raipur)

37. 38. 39. 40. 41. 42. 43. 44. 45. 46. 47. 48. 49. 50. 51. 52.

Dr. Ilina Sen (Rupantar, Raipur) Ms. Lalitha (Anveshi, Hyderabad) Dr. P.K. Sarkar (Foundation for Health Action, Kolkata) Dr. N. P. Chaubey (Secretary, Indian Academy of Social Sciences, Allahabad) Mr. S.Srinivasan (LOCOST, Vadodara) Ms. Renu Khanna (SAHAJ, Vadodara) Dr. Saibal Jana (Physician, Shaheed Hospital, Dalli Rajhara, Chhattisgarh) Mr. Biraj Patnaik (Principal Adviser to the Commissioners of the Supreme Court (Writ 196/2001). Ms. Sudha Bharadwaj (Advocate, Chhattisgarh High Court and Secretary, Mahila Mukti Morcha) Mr. Anant Johuri (Sarvadaliya Kisan Sangh, Anuppur, Madhya Pradesh) Dr. Madhuri Chatterjee (Physician, Jan Swasthya Sahyog, Bilaspur) Dr. Anju Kataria (Paediatrician, Jan Swasthya Sahyog, Bilaspur) Dr. Rachana Jain (Gynaecologist, Jan Swasthya Sahyog, Bilaspur) Dr. Madhavi Bhargava (Surgeon, Jan Swasthya Sahyog, Bilaspur) Dr. Yogendra Parihar (Advisor, Jan Swasthya Sahyog, Bilaspur) Dr. Sister Aquinas (Holy Cross, Mysore, Karnataka) Mr. Ranjan Ghosh (Jan Chetna Manch, Chandankiari, District Bokaro, Jharkhand) Dr. Lindsay Barnes (Jan Chetna Manch, Chandankiari, District Bokaro, Jharkhand) Sr. Elizabeth (Secretary, Raigarh Ambikapur Health Association, Pathalgaon, Chhattisgarh) Dr. Sarla Kataria (Retired Professor of Economics, Lady Shri Ram College, Delhi University) Ms. Rajashri Dasgupta (Freelance journalist, Kolkata) Mr. Rehmat (Manthan Vichar Manch, Badwani, Madhya Pradesh) Mr. V.R. Raman (Co-ordinator, State Health Resource Centre, Raipur) Mr. Amulya Nidhi (CRY, and Shilpi, Indore, Madhya Pradesh) Dr. Ashish Gupta (Shilpi, Indore, Madhya Pradesh) Mr. Sameer Garg (Koriya Initiative, Chhattisgarh) Ms. Sulakshana Nandi (Koriya Initiative, Chhattisgarh) Ms. Asha Shukla (Journalist, Nav Bharat, Raipur) Dr. Surabhi Sharma (Ayurvedic Physician, Jan Swasthya Sahyog, Bilaspur) Mr. Mahesh Sharma (Co-ordinator, Organic Farming, Jan Swasthya Sahyog, Bilaspur) Mr. Praful Chandel (Co-ordinator, Village Health Programme, Jan Swasthya Sahyog, Bilaspur) Ms. Santoshi Viswakarma (Co-ordinator, Village Health Program, Jan Swasthya Sahyog, Bilaspur) Mr. Suresh Sahu (Rupantar, Raipur) Gangaram Paikara (Right to food Campaign, Chhattisgarh) and other friends from the Right to Food campaign, Chhattisgarh Dr. Arun Gupta (Fellow, Community Health Cell, Bangalore) Mr. Shivnath Yadav (Dharohar, Kondagaon, Bastar) Mr. Shailesh Bambore (Chhattisgarh Kisan Sangh, Dalli Rajhara) Mr. Gajanan Singh (Jan Jagriti, Chhattisgarh) Ms. Durga Jha (Dalit Study Circle, Raipur)

32

mfc bulletin/February-March 2006

Announcements 1. CONVENTION ON CHILDREN’S RIGHT TO FOOD (Hyderabad, 7-9 April 2006) A convention on “Children’s Right to Food” will be held in Hyderabad on 7-9 April 2006. This is a follow-up to the second National Convention on the Right to Food and Work, held in Kolkata on 18-20 November 2005. The main focus of this convention will be on ICDS, mid-day meals and related means of protecting children’s right to food, including maternity entitlements. Special attention will be given to “universalization with quality” as the core demand of a united campaign on ICDS. This will be an actionoriented convention, built around plenary sessions, parallel workshops, cultural activities, and more. For further details, please contact Dipa (dipasinha@gmail.com, tel 9866023233) or Navjyoti (righttofood@gmail.com , tel 09350530150). Updated information will be posted from time to time on the website of the “right to food campaign” (www.righttofoodindia.org). 2. TRAINING ON NREGA IN DUNGARPUR (RAJASTHAN), 15-17 APRIL 2006 Mazdoor Kisan Shakti Sangathan (MKSS) and Jungle Jameen Andolan are organizing a two-day training on NREGA in Dungarpur district of Rajasthan on 15-17 April, 2006. The training, convened under the banner of “Rozgar Adhikar Abhiyan”, will be followed by a 10-day padyatra on 17-26 April. The purpose of the training and padyatra is to enable people to make use

Subscription Rates Rs. U.S$ Rest of Indv. Inst. Asia world Annual 100 200 10 15 Life 1000 2000 100 200 The Medico Friend Circle bulletin is the official publication of the MFC. Both the organisation and the Bulletin are funded solely through membership/ subscription fees and individual donations. Cheques/money orders/DDs payable at Pune, to be sent in favour of Medico Friend Circle, addressed to Manisha Gupte, 11 Archana Apartments, 163 Solapur Road, Hadapsar, Pune - 411028. (Please add Rs. 15/- for outstation cheques). email: masum@vsnl.com MFC Convener Ritu Priya, 147-A, Uttarakhand, JNU, JNU Campus, New Delhi -110 067. Email: <convenor.mfc@vsnl.net> MFC website:<http://www.mfcindia.org> of NREGA and to monitor its implementation. Trainees are expected in hundreds, not only from Rajasthan but also from other states. This is an important opportunity to create a cadre of well-trained activists, who may become trainers in their own area later on. If you are interested in participating, please send a line to rozgar@gmail.com as soon as possible.

Contents Wishing away a Condition: Issues of Concern in the Control and Treatment of Leprosy How to Count the Poor Correctly versus Illogical Official Procedures Cost and Quality Considerations in Secondary and Tertiary Health Care Misplaced Faith? ‘Hunger and Health: An Interdisciplinary Dialogue’: Joint Statement from a Workshop

- Jan Swasthya Sahayog(JSS)

1

- Utsa Patnaik

9

- Debabar Banerji - Arti Sawhny

23 25 28

Editorial Committee: Anant Bhan, Neha Madhiwalla, Dhruv Mankad, Amita Pitre, C. Sathyamala, Veena Shatrugna, Chinu Srinivasan. Editorial Office: c/o, LOCOST, 1st Floor, Premananda Sahitya Bhavan, Dandia Bazar, Vadodara 390 001 email: chinumfc@icenet.net. Ph: 0265 234 0223/233 3438. Edited & Published by: S.Srinivasan for Medico Friend Circle, 11 Archana Apartments, 163 Solapur Road, Hadapsar, Pune 411 028. Views and opinions expressed in the bulletin are those of the authors and not necessarily of the MFC. Manuscripts may be sent by email or by post to the Editor at the Editorial Office address. MEDICO FRIEND CIRCLE BULLETIN PRINTED MATTER - PERIODICAL Registration Number: R.N. 27565/76

If Undelivered, Return to Editor, c/o, LOCOST, 1st Floor, Premananda Sahitya Bhavan Dandia Bazar, Vadodara 390 001


