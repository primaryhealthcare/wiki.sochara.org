---
title: "Tonics : How Much An Economic Waste"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Tonics : How Much An Economic Waste from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC011.pdf](http://www.mfcindia.org/mfcpdfs/MFC011.pdf)*

Title: Tonics : How Much An Economic Waste

Authors: Jaya Rao Kamala S

medico friend circle bulletin NOVEMBER 1976

Tonics: How Much An Economic Waste KAMALA S. JAYARAO* MONG the pharmaceutical preparations that are indiscriminately prescribed are the vitamins, particularly those of the B-complex group. "Probably no single class of drugs (Sic) has been the target' of as much quackery, misunderstanding, misrepresentation and misuse as the 1 vitamins………” . There are however a number of reasons for this, some in my opinion condonable. Patients often come with vague symptoms which can be correlated to no known disease. The complaints may be genuine or psychosomatic, but the patient expects treatment. For example, a common complaint is pain in the back or pulling sensation in the legs. Or, it may be a simple complaint of general fatigue or loss of appetite due to no organic cause. What is one to do? One usually prescribes a multivitamin or a B-complex preparation. This may be done for three reasons. The physician may sincerely believe that vitamins will help the patient or he may feel compelled to prescribe something. Thirdly, the patient himself may demand some medicine, generally a 'tonic'. What does a tonic mean, anyway? In general parley it has come to mean a liquid preparation. However we do come across advertisements of ' nervous tonics' 'tonic for muscle strength' 'for energy' etc... This is pure baloney. One of the definitions given by the Webster Dictionary for tonic is 'something that invigorates, restores, stimulates or refreshes'. Could it be the generous quantity of alcohol in these preparations? If the physician believes that B-complex would be beneficial even if he has no scientific evidence or therapeutic basis, he need not in my opinion be castigated. We still do not know all the metabolic functions for which one or more members of the B-complex

A

may be needed. Hence, we are probably not in a position to recognize all situations which may respond to vitamin therapy, though severe deficiencies of single vitamin have been well characterized in most cases. The trouble arises with the dose that is prescribed. The physician should realise that in such undefined situation, the therapy is purely empirical. The burden rests on him to know whether he is prescribing the right amount, less or more. This brings us to the question of what the right amount is. Here we must differentiate between vitamins taken as nutrients to ward off deficiency and taken for therapeutic purposes, in established deficiency. The latter dosages are not based on as careful a scientific scrutiny as the former. They are prescribed for acute and severe, single deficiency states like beri-beri, pellagra, keratomalacia etc. Since water-soluble vitamins are considered to be relatively innocuous, the amounts prescribed are very high, the main aim being to tide over the acute situation. On the other hand, we have these various undefined situations which we attribute to vitamin deficiencies or anaemia. These are neither acute nor proven states of deficiency. If the condition is due to a nutrient deficiency, the deficiency is probably chronic and marginal or moderate in nature. Here the implication probably is that the individual is unable to meet his nutrient requirements. This is perhaps a justifiable premise since the prevalence of B-complex deficiency in our country is relatively high. According to certain surveys the prevalence rate is 5 per cent in pre-school children and 17.8% in pregnant women (assessed by the presence of 2 angular stomatitis and glossitis) . The percentage of those with less severe deficiency is expected to be higher. * National Institute of Nutrition, Hyderabad-500 007

Go to the people

What is a nutrient-requirement? The requirement for a specific nutrient is defined as the smallest amount of that nutrient that will ensure a good state of health. This will however, vary from person to person. Therefore, nutrient requirements are set down as recommended dietary allowances (RDA). These levels are believed to 'meet the known nutritional needs of almost every healthy person.' By experimental procedures, the highest requirements in 'a population are assessed, some further allowances are added and the RDA for each nutrient is fixed. Thus for many individuals the RDA will be higher than their actual requirement. No person need take more than the suggested RDA. The RDA for various nutrients have been fixed by 3 international organizations like the FAO and WHO and by various national bodies including the Indian Council of 4 Medical Research .

In the process of this search, I came across an interesting or disturbing feature, depending on how you wish to perceive it. Many advertisements do not say what ingredients the preparation contains, leave alone their quantities. Many inform you that the preparation is a unique formulation of generous amounts of vitamins or that it is a vitalizer with balanced amounts of vitamin (Incidentally, IDPL is one of them). The advertisement merely proclaims the efficacy of their product in specified condition. There is one advertisement by a leading company, which reveals nothing about the formula but claims that it is good for memory! It contains nothing but vitamins B1, B2 and B12. The companies are probably cocksure that the physician will rely more on their advice than on his own judgement (and they are dead right). This lack of needed information is one of the reasons why Table II does not have more preparations listed. But this is ample for what I have to say. There is also no reason to believe that those which escaped inclusion would be any different.

I was interested to know how some of the commonly available vitamin preparations fare when compared to the RDA suggested by the ICMR. Table I shows the RDA for some nutrients, for various physiological groups. For specific reasons, I have not taken the RDA for infants and children but in absolute terms these values will be less than those for adults. In Table II, I have presented the quantities of various vitamins purported to be present in each commercial preparation. It is however not the complete formula of the preparation. I have taken only some important vitamins into consideration. The list is by no means exhaustive. I culled them from some recent issues of the Journal of the Indian Medical Association. They are marketed by leading pharmaceutical companies.

The RDA for any nutrient is the amount which if taken regularly will ensure that a deficiency state of that nutrient will not develop. For example if a sedentary, house-wife takes 1.0 mg riboflavin daily she is expected not to develop riboflavin deficiency. As I said earlier, 1.0 is the highest level and most can afford to live on lesser amounts. The situations which are under discussion now, are considered to be deficiency states of mild or moderate degree. The individual might have depleted levels of the nutrient and may need higher amounts than the RDA. What

Recommended Daily Allowances*

Table-I Thiamine

Riboflavin

Nicotinic acid

Pyridoxine†

Folic acid

Vitamin B12

(B1) mg

(B2) mg

mg

(B6) mg

mg

mcg

1.2 1.4

1.3 1.5

16 19 26

1.4 -

0.1 0.1 0.1

1 1 1

1.0 1.1 1.5

1.0 1.2 1.7

13 15 20

2.0 -

0.1 0.1 0.1

1 1 1

1.1-1.3 1.1-1.5

1.2-1.4 1.2-1.7

14-17 14-21

1.6 1.8

0.1 0.1

0.5-1 0.5-1

1.2-1.7 1.4-1.9

1.2-1.9 1.4-2.1

15-22 18-25

2.5 2.5

0.15-0.3 0.15

1.5 1.5

Man: Sedentary Moderate Heavy work 2.0 2.2 Woman: Sedentary Moderate Heavy work Adolescents: 13-15 yrs 16-18 yrs Pregnancy (Second half) Lactation

* Taken from reference 4

† Taken from RDA of Food and Nutrition Board, U.S.A. 1968. Live among them

should this higher level be? For acute and severe states like beri-beri or keratomalacia text-books prescribe doses, empirically arrived at and found to bring quick relief. These are usually much higher than what would be required even for that degree of amelioration. Table III shows the prescribed therapeutic doses, as obtained from various standard books on nutrition and medicine.

per the above mentioned schedule they supply 2-4 times the RDA, and it was argued above that double the RDA should be enough in moderate or doubtful deficiency states. We must also remember that when a diet is considered to be low in a nutrient, it' is not totally lacking in that nutrient. The average diets of the low socio-economic groups provide 0.5 to 0.8 mg each of BI and B2. Items 7-9 provide about 5-25 times the RDA in a single dose. If even such preparations are prescribed thrice a day', the intake would be 15-75 times the RDA. Item 8 in a single dose supplies thiamine in a quantity prescribed for the whole day in beri-beri? Moreover in beri-beri it is not necessary to prescribe very large amounts of other vitamins. Thus preparations like 8 and 9 are not necessary at all.

For chronic, moderate deficiency states or for situations where vitamins are prescribed empirically we may assume that levels much lower than the therapeutic doses and slightly higher than the RDA should be enough. Let us be generous and double the RDA, remembering that the patient does receive a certain amount from his diet too. With this information I would like you to critically compare Table II with Tables I and III.

An argument may be put forward that since watersoluble vitamins are harmless compounds there is no necessity to raise a hue and cry about the dosages prescribed. This is no doubt true but, such practice is economically wasteful and in some instances, causes 1 financial hardship' .

Much of the time drugs are not prescribed according to any therapeutic schedule. They are usually prescribed as '1 dose or 1 tablet, three times a day'. Items No. 1-4 in Table II are close to the RDA with respect to vitamins BI and B2. Given as

Table-II

Composition of some multivitamin and haematinic preparations available in India. 1 Capsule

2 Cap.

3 5 ml.

4 5 ml.

5 Cap.

6 Cap.

2 2 1

1.0 0.75 0.15 7 0.45 .... .. F.A.C 185

1.6 0.8 0.8 4.0 2.5 .. …. Gluco. 35

3.0 1.0 0.5 30.0 5.0 … ....

5 2 1 10 5 1 50 Sulp. 200

7 Cap.

8 Cap.

9 Cap.

"

Vitamin B1 mg Vitamin BII mg Vitamin Be mg Niacin mg Vitamin Bill meg Folic acid mg Vitamin C mg Iron (Type of salt) mg

1 0.5 0.6 4. . 2 2 .. ..

Vit. A. I. U. Vitamin D. I. U.

.. ..

... .. 50 Sulp. 150 timed release ..

250 90

10 5 ml

11 Cap.

12 Cap.

13 Cap.

15 2 150 Fumarate 350

.. .. 10 .. 50 2.5 100 Fumarate 300

5 100 ... 200 Sulp. 41

10 50 10' 25 10 100 5 0.5 300

25000 1000

20 5 2.5 100 2.5 1.0 100

10000 1000

Table-II (Contd)

Vitamin B I mg Vitamin B II mg Vitamin Ba mg Niacin mg Vitamin BI \I mcg Folic acid mg Vitamin C. mg Iron (Type of salt) mg Vitamin A. Vitamin D.

Love them

25 2.5 .. Colloidal oxide 100

25 2.0 200 Fumarate 350

14 5 ml.

7 1.75 .. Colloidal ox. 500

15 5 ml.

15 2 .. Fumarate 125

16 Cap.

25 2.5 .. Fumarate 250

17 18 3. ml I.M. 5ml. I.M

100 .. 27.5 .. 1000

100 .. 25 .. 500

It must also be remembered that water-soluble vitamins cannot be stored in large amounts unlike the fat-soluble ones. This of course is one of the factors underlying their low toxicity. 'In prescribing thiamine it should be remembered that the healthy human body contains only about 25 mg of the Vitamin. Furthermore,' it has no means of storing any excess taken in the diet; the excess is lost rapidly in the urine. The human body is certainly an effective machine for dissolving thiamine pills and transferring the solution to the urinal '5. Moreover it has been shown, atleast for riboflavin that intestinal absorption is limited by saturability and that higher the dose, smaller the fraction absorbed. This is no case in favour of parenteral administration either, because higher the amount in circulation greater the excretion in urine. Thus, most of the 'high-potency' or ‘Forte’ preparations of multivitamins are a sheer economic waste. It is a drain on the patients' purse and the onus is on the doctor because he is making the patient buy a specific preparation. If bought by government or public sector dispensaries, it is a national waste. If preparations with smaller and yet adequate quantities were bought, for the same money more tablets could be purchased and a greater number of patients benefited. Manufacture of such 'high potency' preparations must also use up an unnecessary amount of the scarce foreign exchange resources, since quite a few, and probably all vitamins (raw materials) are imported. Thus it is not proper if one merely prescribes B-complex tablets and avoids brand name because he is a 'conscientious 'objector' to brand names. As long as there is no uniformity in the dosage employed in various preparations, it is necessary to know which brand supplies or claims to supply requisite quantities of Vitamins. Also, there is no need to blindly follow

Table-III

Suggested doses of vitamins for single, acute and severe deficiency Condition

Vitamin

Dose (Oral)

Beri-Beri

B1

10-25 mg bid

Riboflavin deficiency B2 megaloblastic anaemia Folic acid B12

megaloblastic anaemia of pregnancy Corneal xerosis Bitot's spots Rickets

Folic acid Vitamin A Vitamin D

or tds 5-10 mg 5-10 mg 5-10 mg

10 mg 5000-10,000 LU 1000-5000 L U.

the 'one t.d.s.' schedule. How much and how frequently, should be decided on the merits of the case. I also wish to draw your attention to one or two additional points. There is a widely held belief that a combination of vitamins B1 B6 and B12 is good for' neuropathies and other nervous disorders. I don't think this is based on any solid therapeutic evidence. The reason the three are combined, I think is because each one has been shown to be effective in a specific disorder of the nervous system. Hence the triad is used as a short-gun therapy, indiscriminately. In fact, the brand names of certain such preparations incorporate Greek terms like 'encephalo', ‘neuro ' etc. The manufacturers of one preparation even claim its efficacy in improving memory. 'It (thiamine) may be given, though without expectation of dramatic results, in cases of nutritional neuropathy. There is no reliable evidence that it is useful in any other disorder of the nervous system. The prescription of synthetic thiamine, either alone or in combination with other vitamins, as a general tonic or appetiser, is supported by no scientific evidence or is 5 now discredited.' ‘Vitamin therapy is often given to patients with polyneuropathy, although it is clear that polyneuropathy is not due to deficiency of vitamin Bi, B12 or any other known vitamin. Such treatment has a placebo value and probably no 3 other, but is not to be decried.....’ . For reasons mentioned right at the beginning I too do not decry the use of the combination as I do the dosage in such preparations. Items 17 and 18 in Table-Il are two classical examples. Both are meant for parenteral use, another characteristic of this triple combination, probably because of the presence of vitamin B12. The conventional prescription by physicians for parenteral B-complex is ‘2 ml I.M. once a day or once on alternate days'. Assuming the patient receives 6 ml in a week, he is given 600 Meg to 2 mg of vitamin B12! What a colossal waste considering that vitamin B12 is an expensive substance. The prescribed dose even for pernicious anaemia is 2 mg weekly even those who may argue that unlike the other Bcomplex vitamins, vitamin B12 is stored to a certain extent in the body may note that with each 1 ml goes 20-33 mg thiamine. Many of the oral preparation too contain unnecessarily high amounts of B12. The RDA for this vitamin is 1.0 Meg and in pregnancy and lactation, 1.5 Meg. Even conceding that a majority of the population cannot afford animal foods and hence many may suffer· from vitamin B12 deficiency, I see no

Serve them

reason why any preparation should contain more than 2 meg. and at the most 5 meg vitamin B12. This criteria is met by only 7 of the l6 oral preparations listed. If the preparations are Haematinics combined with iron, they have to be prescribed three times a day. In which case the preparation should not contain more than 2 meg B12. Items 10-13, 15 and 16 must be very expensive and those who really suffer from B12 deficiency can ill-afford then. I also wish you to note that mixed Haematinics-Iron preparation containing vitamins and minerals, are condemned by authorities in the field of anaemia. "Recovery of the patient with uncomplicated iron-deficiency anaemia is not helped by vitamin supplements or minerals'7. In our experience vitamin B12 and folic acid are not needed till haemoglobin levels come upto 11 gms. % or more. Let us now consider the vitamin A content of (these preparations. The prescribed dose of vitamin A for corneal xerosis and Bitot's spots is 1500-3000 /ug (5000-10,000 I.U). daily8, 9. The RDA during lactation, the maximum suggested for any group, is 3500 LV. Notice the vitamin A content of items 7 and 9. Who needs 25,000 I.U. vitamin A daily? Severe cases of deficiency like keratomalacia are not to be treated with oral preparations9, 10. Those who really develop xerosis can never afford a pharmaceutical like 7 or 9, whose price is further raised due to presence of other nutrients. Imagine to what extent the price can be reduced simply by bringing down the vitamin A content, even to 5000 LV., which itself is a high amount. Then, there is the practice of adding glycerol-phosphates to liquid, multivitamin preparations. I do not know of what therapeutic value these compounds are. They are not mentioned in any standard textbook of pharmacology and therapeutics. As far as I know (see any pharmacopoeia) they only form basic ingredients of syrups, possibly for flavour. However, a widespread misunderstanding is that they are 'energy givers' or 'tonics'.' Some brand names carry a prefix or suffix of 'phospho' and the advertisement says 'energy givers', 'vitalizer’ etc. This in my opinion is a fraud perpetuated by the drug companies and worse still, an unpardonable ignorance on the part of the doctor. The vitamins atleast, despite the excess and the wastage, do some good. I see no nutritive or therapeutic value for the glycerol-phosphates. Their presence is needed for syrup preparation but its name should not be included in the brand name and no claims should be made for its therapeutic efficacies. One of the nutrients commonly added to multi-

Learn from them

vitamin preparation is iron. Witness that out of the 16 listed items, only 4 do not contain iron. It is well-known that ferrous compounds are better absorbed than the ferric, and it is heartening to note that most are ferrous salts. A perplexing form is the colloidal iron oxide (items 10 and 14) which finds no mention in any book on pharmacology or iron metabolism. Since it is a colloidal preparation I doubt if the iron in it is easily available to the body. Of the various ferrous salts, ferrous sulphate is the least expensive and should be the treatment of choice, yet only 3 preparations contain it. It is said that contrary to popular thinking and claims, gastrointestinal intolerance to iron preparations depends on the total amount of elemental iron in the gut and on psychological factors; it is not a function of the 1, 7 form in which iron is administered. Thus claims made for compounds other than ferrous sulphate, of increased tolerance or decreased toxicity, are not genuine. Also, sustained - release (timed-release) compounds (no. 2) take the compound beyond the duodenum and proximal jejunum and thus reduce iron absorption. Therefore it is wasteful to prescribe such preparations. The RDA for Iron ranges from 20-40 mg per day depending on age, sex, physiological state etc. This of course is for food iron and for free inorganic salts would be less. The therapeutic dose, on the other hand, is 60 mg elemental iron, thrice a day. Ferrous sulphate, fumarate and gluconate contain 20%. 33% and 12% elemental iron respectively, Items 11-13 and 16 are probably meant for iron deficiency anaemia. Prescribed twice a day they supply 250-350 mg elemental iron which is higher than the therapeutic dose. Thus taken, 13 supplies 150 mcg vitamin B12. On the other hand, no. 7 supplies only 8 mg elemental iron per capsule. One may argue that this may be used as for prophylaxis and not treatment. Have a second look and tell me the situation where in an individual is grossly deficient in every vitamin one can think of and is yet not deficient in iron? This is a pure commercial gimmick to claim haematinic value for the preparation. As early as 1936 Strauss said" shot-gun therapy is to be deplored for a number of reasons. Most mixtures of substances fail to contain enough of anyone ingredient to give maximal effects. The patient must pay not only for the material he needs but also for the non-essentials" (cited from ref, 1). . One can go on endlessly in this manner. My intention in writing this is to bring to the notice of MFC members the fact that all multivitamin and haematinic preparations are not same.

1. 2. 3.

There is no uniformity in dosage employed. There is no authority· to lay down criteria for dosages. There is no authority to check· whether the claimed doses are actually present. 4. Doctors prescribe these preparations with total ignorance of or indifference to principles of nutrition and therapeutics. 5. High-potency preparations should be available separately for single vitamins. Multivitamins need not contain amounts much higher than RDA. They are economically wasteful. 6. The false claims made for improvement of unspecified and unproven conditions are perpetuated due to the ignorance or compliance of the doctors. 7. Most of the companies have foreign collaboration. Most of the raw ingredients are to be imported. Could this be one of the reasons for the high dosages employed? I am sure you will find asking yourself many more such questions. What should be MFC's attitude and its role in such situations?

References 1.

The Pharmacological Basis of Therapeutics. (L. S. Goodman and A. Gilman, eds.), Fourth edn. MacMillan Co., London. 1970. 2. Nutrition Atlas of India (C. Gopalan and K. V. Raghavan eds.) National Institute of Nutrition, Hyderabad, 1971. 3. Energy and Protein Requirements. WHO Tech. Rep. ser. No. 522, 1973; Requirements of vitamin A, Thiamine, Riboflavine and Niacin. WHO Tech. Rep. ser. No. 362, 1967, WHO Geneva. 4. Dietary Allowances for Indians (C. Gopalan, B. S. N. Rao) Indian Council of Med. Research, Spl. Rep. Ser. No. 60, 1968. 5. D. Davidson, R. Passmore, J. F. Brock and A. S. Truswell. (1975). Human Nutrition and Dietetics, Sixth edn. Churchill Livingstone, Edinburgh and London. 6. W. G. Bradley (1975). The treatment of polyneuropathy. Practitioner 215: 452. 7. T. H. Bothwell and C. A. Finch (1962) Iron Metabolism. Little, Brown Co., Boston. 8. S. G. Srikantia (1975) Human vitamin A deficiency. Wid. Rev. Nutr. Diet. 20: 184. 9. Reddy, V. (1969) vitamin A deficiency in children. Indian J. Med. Res. Suppl. to vol. 57, p. 54. 10. M. F. C. Bulletin 8, August, 1976.

A regional work study camp of MFC is being organised from 21st to 24th December '76 at Palia 'Piparia. For further information please writ, to: Dr. Mira Sadgopal, Village Palia Piparia, P. O. Malhanwada, via Bankhedi, Dist. Hoshangabad (MP) 461 990

THE BABY KILLER MIKE MULLER

"Wash your hands thoroughly with soap each time you have to prepare a meal for baby," is how the instructions on bottle feeding begin in the Nestles Mother Book. Sixty-six per cent of households in Malawi's capital have no washing facilities at all. Sixty per cent have no indoor kitchen. Nestles sells milk for feeding babies in these communities. "Place bottle and lid in a saucepan of water with sufficient water to cover them. Bring to the boil and allow' to boil for 10 minutes," says Cow and Gate's Baby care Booklet for West Africa, showing a picture of a gleaming aluminum saucepan on an electric stove. The vast majority of West African mothers have no electric stoves. They cook in a "three-stone" kitchen. That is, three stones to support a pot above a wood fire. The pot that must be used to sterilise baby's bottle also has to serve to cook the family meal - so sterilising and boiling of water will probably be forgotten. Cow and Gate milks are fed to babies in this kind of West African community. Nestles are particularly anxious to emphasis to critics that all their infant feeding products have instruction leaflets in the main languages of the country where they are sold. On the leaflet are simple line drawings to illustrate the method of preparing the feed. Most Third World mothers however, are illiterate, even in their native language. And the four simple line drawings, taken by themselves, are almost meaningless. Have Nestles undertaken any research to find out whether mothers' understanding can be improved? "We can't undertake work of that nature," say the policy makers of Nestles Dietetics (Infant Feeding) Division. "Cow and Gate .is a complete food for babies under six months and can be used as a substitute for breast feeding...."says the Cow and Gate booklet. In Nigeria, the cost of feeding a 3 month old infant is approximately 30% of the minimum urban wage. By the time that infant is 6 months, the cost will have risen to a crippling 47%. In Nigeria, as in most developing countries, the minimum wage is what the majority earn. Cow and Gate products are sold throughout Nigeria. Obviously, for the majority of mothers in these countries, bottle feeding is just not a viable alternative. For even if they can afford to buy enough milk, it is unlikely that they can fulfill the minimum requirements for giving it to the baby safely.

Start with what they know

This is recognised by most authorities. "In the less technically developed areas of the world.... immediate and serious basic difficulties attend attempts to artificially feed young infants on a cows' milk formula,” (milk powder) says the PAG (The Protein Advisory Group of United Nations) Manual on Feeding Infants and Young Children. "These include lack of sufficient money to buy adequate quantities, poor home hygiene (including water supply, fuel, feeding utensils, storage, etc.) and inadequate nutritional knowledge of the mother. Under these conditions, usual for the majority in less developed countries, artificial feeds mean the use of too diluted, highly contaminated solutions of cows' milk, resulting in the best in undernutrition; at worst, in marasmus and diarrhoeal disease.” A doctor puts it in even more forceful terms: "It is clear to all but those who will not see that informed, adequate and relatively safe bottle feeding must follow, or at least accompany, but never precede, literacy, education, infection free water supplies, sanitation and a standard of living which permits the purchase of enough baby foods, equipment and the means of sterilisation .” The present trend away from breast feeding in the Third World's present circumstances can only have calamitous consequences. The PAG comments: "The major overall need is to alert governments' health services, nutritionists and the food industry to the emergency situation likely to develop in urban areas in the near future. Its implications are not only the certainty of rising mortalities from almost epidemic marasmus and diarrhoea but also the economic burden of curative services and of obtaining breast milk substitutes on a large scale as well as the long term consequences of the effect of recovered -cases of infantile malnutrition on the intellectual level of the community.” In these circumstances, can the developing countries afford to have breast milk substitutes on the open market? Dilemma of the doctors Largely because of the Western emphasis in their education, doctors are applying Western solutions to Third World situations with often disastrous consequences. Details, like the routine bottle feeding of infants in maternity wards may be just an administrative convenience for the hospital staff. But viewed' by the unsophisticated eyes in the ward bed, they constitute an endorsement of bottle feeding far greater than anything the milk companies would dare to claim in their advertising. The medical profession

Build upon what they have

is also a key channel through which milk companies promote their products. "I believe that the health professionals, including Paediatricians, do not realise the effectiveness of what I term 'endorsement by association' or 'manipulation by assistance.' Both techniques are, of course, used widely, effectively and very economically by such firms," says Dr. D. B. Jelliffe. Manipulation by assistance The milk companies employ nurses and representatives who visit' doctors and explain the comparative "benefits" of their companies' milks. This is very similar to the approach used by pharmaceutical companies in promoting their drugs and it is interesting to note that the pharmaceutical industry is making big inroads into the baby-food business. Companies like Abbots, Glaxo and Wyeth can capitalise on their pharmaceutical reputations in their approach to doctors. They can also economize on the costs of promotion. Cow and Gate and Nestles do not have the advantage of a direct link with the medical profession so they create one. Nestles sponsors conferences on nutrition and allied fields and has helped to organise courses for Paediatricians on nutrition either as a company or through the medium of the Nestles Foundation. Just as doctors rely largely on the drug companies to keep them informed about advances in pharmaceuticals, so they are increasingly dependent on the milk companies for information about infant feeding. Endorsement by association Developing countries have little enough money for hospitals let alone for decorating their clinics. So the nurses will gladly accept educational posters from a milk company representative. Nestles, for instance, has just issued a new series of five. One on pre-natal care; another on cleaning and dressing the baby; one devoted to ways of preparing the baby's first solid foods; a fourth devoted to breast feeding. The last used to be incorporated with a guide to bottle feeding which has now been made into a separate poster. The only hint of commercialism in the posters is the Nestles feeding bottle illustrated and the mention of CERELAC, a Nestles product in the solid food poster. And of course the company logo in the upper right hand corner of each poster. The illiterate mother will find them interesting. And though the bright modern household, the clean white baby clothes, the crib and the recommended

foods are almost certainly 'out of her monetary reach, the feeding bottle is not. She might be given it free by a nurse.' And, if there are posters in the clinic about bottle feeding, there cannot be much wrong with it. She might react the same way when she notices' that her baby's clinic card has a picture of a tin of Lactogen baby milk. She does not know that the company gives the cards to the clinic. And she can almost certainly not read the English text inside which says "Breast feed your baby for as long as you can. Breast milk is best for baby and gives him the best start in life...."

a variety of toxic compounds in the plant world and we should be sure of the safety of each medicine. Recently I had an opportunity (I don't know whether it was opportunity) or rather an occasion to test a herbal root claimed to be antidiabetic. It was being used with impunity in diabetic patients. The concerned person refused to reveal the name of the source to us. When I tried this on rabbits I found that the herb no doubt had a hypoglycemic effect. But to my dismay I found that this was temporary, followed by a hyperglycemic phase which killed the animals! So, there is much to be done, understood and revealed in Ayurveda before it can be accepted in Toto by a community that should learn to think scientifically.

But the health services operate on a tight budget, and this helps them to cut their running expenses. Whether it is intended to or not, much of the promotional or educational material used by the milk companies, will, to the illiterate, appear to endorse bottle feeding. And the association of this material with clinic or hospital can only reinforce this impression. Courtesy 'War on Want.’

Dear Friend, Ayurveda and Allopathy

I I read with much interest, Bapalal Vaidya's article in Bulletin-It). I do agree with him that all is not well with allopathy as practised in India and that a system of medicine should be suited to the socio-cultural milieu in which it is practised. But let us not swing the pendulum too far. The fact that everything is not okay with one system does not carry with it the corollary that nothing is wrong with other systems. Practitioners in Ayurveda and those interested in it should help suit it to the contemporary age. Allopathy, as other systems of science, borrowed from the west has a beautiful system of analysis and inquiry in it. This should now be built into Ayurveda, Those who practise ayurveda should gain a good knowledge of physiology and biochemistry (which unfortunately is lacking even in students of Allopathy, in this country) and help in the integration of the two systems.

—Kamala Jayarao, Hyderabad

In complete one and a half page of the article writer has given all weak points of modern medicine and nothing else. Well, no system is perfect in itself. The author has himself agreed that in their system, there are no thundering names of diseases and it does not resort to forced feeding. But to tell a carcinomatous patient not to worry, for his lesion is a simple boil and wait till his appetite returns, directing him to cachexia, well" I shudder to think the consequences.

—Tejinder Singh, Gwalior

News • Third All India Medico Meet, Hoshangabad December 25, 26 & 27, 1976 The meet is being organised to discuss the Problem of Undernutrition. Besides the theme, future policy, programme and organisation of MFC will also be discussed. Dr. Kamala Jaya Rao will introduce the subject in general and Prof. Sukhatme will speak on socio-economic aspects of the problem.

As the author has pointed out, Ayurvedic drugs are mostly herbal. However he is wrong when he says that by inference they are non-toxic. There are •

The meet will be held at Friends Rural Centre, Rasulia, Hoshangabad (M.P.). If you are willing to participate in the meet, please write immediately to - Convener, Medico Friend Circle, 21, Nirman Society, Vadodara390 005. We are sending the programme and working papers of the Meet within a few days only to those who have asked for the same.

Editorial Committee: Imrana Qadeer, Prakash Bombatkar, Satish Tibrewala, Kamala Jayarao, Mira Sadgopal, Abhay Bang, George Issac, Sathi Devi, Bhoomi Kumar J., Suhas Jaju, Lalit Khanra, Ashvin Patel (Editor) Edited and Published by - Ashvin J. Patel for Medico Friend. Circle from 21 Nirman Society, Vadodara390005 and Printed by him at Yagna Mudrika, Vadodara-390001. Annual Subscription Inland - Rs. 10/For U.K. by Sea Mail £ 3/- by Air Mail £ 4/-; for U.S.A. & Canada by Sea Mail $ 6/- by Air Man $ 9/-


