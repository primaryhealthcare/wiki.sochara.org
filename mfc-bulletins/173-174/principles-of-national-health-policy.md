---
title: "Principles Of National Health Policy"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Principles Of National Health Policy from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC173-174.pdf](http://www.mfcindia.org/mfcpdfs/MFC173-174.pdf)*

Title: Principles Of National Health Policy

Authors: Jana Smarjit & Das S

173-174 medico friend circle bulletin July/August 1991

REGULATION OF PRIVATE MEDICAL SECTOR: WHY? HOW? ANANT R.S. PHADKE In the new lexicography of Indian economics, privatization is the panacea for all Ills of the Indian economy. In the field of health, private sector is already the dominant sector and there is a talk of privatizing it further. It would be Interesting to see the performance of the private sector so as to get an Idea about what will happen if there is 'further privatization in the healthsector. The first thing that strikes in the study of Indian private sector in the field of health is paucity of the data compared to the plethora of studies, data available on public health-policy Issues. A lot of systematic study is needed to document the structure sand functioning of the private sector in health. In this article, we would base ourselves on whatever fragmentary documented Information that is available on the private sector. Some of the facts mentioned in this article are well-known in the medical circle, though they have not been documented as such. On the whole, even a brief survey of the private health-sector would tell us that unless there is some mechanism of regulating the quantity, quality, price of private medical services in India, Indian people have no hope of getting good quality medical care through the private sector, at an affordable price. Let us see first why regulation of private medical sector In India Is needed. Let us do this by briefly going through the different components of the sector, one by one, and the problems they have generated. We would then turn to the question of how to regulate the private' sector.

PROBLEMS 1. Private medical colleges: The share of private allopathic medical colleges In India In the total allopathic medical colleges has increased from 3.57 % In 1950 to 17 % In 1986 (1) This increase has occured especially In the last 15 years. MFC has criticized medical education In India for being less relevant to the needs of4the vast majority of the Indian people In rural areas; for being hospital-based; wasteful (for teaching a lot of unnecessary, Irrelevant things) and for training doctors with a primarily curative oriented, elitist, technocratic approach to health problems. These problems of medical education are not solved by the private medical colleges. On the contrary, barring exception, they are' substandard, exorbitantly costly and have much more elitist urban bias. They therefore tend to exacerbate all the problems of health-care services in India. Most importantly, they lower the professional and ethical standards of medical-care .In India. After having spent 3-4 lakh of rupees on medical education, the medical graduates from such colleges naturally recover this 1nvestment' from patients. In competing with graduates from government medical colleges, they resort to unfair practices to cheat patients. Secondly, they exacerbate the urban- rural disparity in the availability of doctors In India. This Is because of the fact that most of the purchasing power is

concentrated in the cities and hence for a doctor wanting to make fast buck, rural practice is not a very good "'prospect. The availability of doctors In India has risen very rapidly after Independence. There was one allopathic doctor for 5750 population .In 1952. By 1986-87, the doctor population ratio has increased to 1 :2239 (2) If we take both allopathic and non-allopathic doctors together, the doctor-population ratio works out to be 1:935 (3) Considering the fact that more than 13000 allopathic and about 4000 non-allopathic fresh graduates are being added every year, there Is no need for any more medlc~1 colleges. On the contrary, the commercial private medical colleges should be closed down. 55% of Ayurvedic, 65 % of Unani, and 75 % of Homeopathic medical colleges are in the private sector. (4) There is no published information on the quality of these colleges. The quality of training in these colleges is somewhat irrelevant since it is an open secret that most graduates from these colleges primarily practice allopathy. Such kind of allopathic practice is quite Irrational, short of quackery. In majority of cases, the real purpose of running such colleges is to create entrypoints for admission into the bazaar of medicalpractice. Unless there is some regulation of the quantity and quality of private non- allopathic medical colleges, allowing such medical colleges is detrimental to the Interest of the common people. 2. General Practitioners: This is the most predominant subsection of doctors as compared to other subsections like: paediatrtians, surgeons, dentists, etc. This category includes not only allopathic general practitioners but also those nonallopathic doctors who do allopathic general practice. During medical education, there is no training for general practice. Due to the hospital-oriented training, a fresh graduate is quite at a loss In general practice. He/She settles down after a few days with the help of advice and tips' from senior colleagues. But this 'reorientation’s more often than not, not on scientific lines. Thus a fresh entrant may start picking up irrational therapeutics right at the beginning of his/her career. 2.1: Irrational drug use: The most important problem in the field of general practice from the point of view of Interests of the patients is irrational therapeutics; especially use of irrational drugs. The most important reason for this is the marketing of plethora of irrational drugs by the drugcompanies. The Irrational nature of most of the commonly prescribed drug combinations is now well established, thanks to the studies published mainly by MFC, LOCOST and VHAI. It may be noted that even rational drugs are used irrationally; for example: routine use of antibiotics in cold and cough or in diarrhoea.

Unnecessary use of injections and intravenous infusions is the second most important area of irrational practice. This trend started with the unnecessary use of Injections by some unscrupulous practitioners. Given the lack of health education In India, the 'prick' has now acquired a kind of magical quality in the eyes of even otherwise educated people. So much so that most patients expect a prick from the general practitioner and are disappointed if they don't get one. It is now difficult for a general practitioner to survive if he/she does not use the prick very liberally. Patient would go to some other practitioner, who satisfies them with the prick. A vicious cycle has thus set in. One Important reason behind the abuse of Injections 18 the absence of consulting-fees in general practice. Customarily, general practitioners (G.P) In India are not paid consulting fees as such. The G.P. recovers 'treatment charges' from patients. Such charges are more than the actual cost of the drugs used since it Is only In this way can the G.P. recover his/her examination-fee. If an Injection Is given, the patient is more willing to pay for the 'treatment-charges.' This tradition of not charging examination-fee separately has contributed a great deal in the use of unnecessary Injections. It is upto the doctors' community as a whole to take Initiative to break the vicious cycle of injection-culture'. 2.2 : Lack of Continuing Medical Education: One important cause of irrational-practice is the lack of Continuing Medical Education (CME) of doctors. CME Is quite necessary for practitioners since whatever education the doctor gets In the medical-college, needs to be re-emphasized; the limited exposure In medical colleges being insufficient to imbibe the scientific core as well as the finer aspects of different common and not so common disorders. Secondly, medical science is changing so rapidly that CME is necessary for updating the doctors' knowledge about diagnosis, management and prevention of various disorders he/she has to treat. Medical-care is one field in which updating of knowledge Is necessary even for routine work like treatment of diarrhoea, lower respiratory tract infections etc. Compared to this need, hardly any proper CME, of G. Ps takes place. The journal of Indian Medical Association (JIMA) is the most widely circulated source of scientific CME for doctors In India. But it reaches only its members, which constitute less that 25% of the total number of around 3.5 lakhs registered allopathic practitioners. Though GPs form the largest subsection of doctors, out of around 25 pages of scientific material in each issue of JIMA, only 3-4 pages are devoted to: 'GP-forum'. It is questionable whether even this much of material is read by recipient GPs. It should thus be noted that majoring of allopathic GPs and most of the non-allopathic GPs have no regular source of scientific CME some ‘learn’ from the prescription of consultants. Most are almost totally dependent upon Medical-representatives of drug companies for updating their knowledge about drugtherapy. Even those who are members of IMA, also rely heavily on the propaganda of the drugs companies. Some do buy books like: 'Kapoor's Guide to General Practioners' or newer editions of standard textbooks. But they form only a small minority of General Practitioners, 2.3 : Lack of Record Keeping: Most GPs do not keep even minimum necessary records. No clinical findings are recorded. It is therefore, not possible to evaluate the treatment given. Most GPs employ only one assistant who is

receptionist-cum-compounder-cum-general assistant. There is no muster-role, no record of payment made to the assistant. The minimum Wage Act is generally bypassed.

3): Consultants: Due to increasing competition in General Practice especially in the cities, there is an increasing trend towards post-graduation. Because of the very nature of their work, they generally require a hospital attachment. Since very few of them can build a hospital of their own, getting a good hospital attachment, a good place for consulting is an important problem for them. Since, atleast in the initial period, consultants depend upon referrals from GPs; in the highly competitive market especially in the big cities, the pernicious 'cut-practice' has taken firm roots. In a city like Bombay It has become the norm. Further, hiring and furnishing consulting-room means an Investment of anything from As 50,000 in a small town to a couple of lakhs of rupees in a city like Bombay. Add to this the 'wailing-period' for settling down in practice. It is no wonder that cut-practice, unnecessary medical Interventions etc. are eating up medical ethics. Consultants, surgeons are more reluctant to settle in villages because very few villagers can pay them at the expected rate. The 'logic of the market' results in these distortions especially when the market is far from perfect and is unevenly distributed. 3.1 : Irrational and unethical practices The consultants are comparatively much better trained. Majority of them are members of association of their own specialty and receive its journal, occasionally attend conferences. A small section does keep itself quite up-to-date atleast in matters related to their routine work. Yet majority of them do prescribe one or two irrational drug-combinations to a patient. This is primarily because of the Influence of the drugindustry coupled with indifference towards patient's Interests. But there is also probably an element of lack of CME. Irrational drug-prescription is not the only problem in consulting practice. The range of unethical practices is comparatively more. Following are the common irrational practices indulged into by many consultants, though there is no documented proof of it; unnecessary ECG, X-ray, unnecessary sonography (especially amongst those who own these machines), unnecessary surgery, (especially appendicectomies, caesareans, hysterectomies, tonsillectomies and now cardiac procedures.) Newer, sophisticated Investigations like echocardiography, sonography, CAT-Scan, endoscopies offer new opportunities for some unscrupulous consultants for indulging into unnecessary investigations. Many consultants in bigger, elite hospitals tend to over investigate even though they may not get a 'cut' from such practice. This over investigation is due to a kind of 'culture of Investigations' at such level of practice. This Is despite 'the fact that the false positive results of the tests far outnumber the true positive results when a test Is conducted in absenc9 of clinical suspicion; i.e. when the prior probability of the disease Is quite low. (Baye's Theorem). There are certain practices which by their very nature are unethical and hence need to be banned. For example, techniques for prenatal diagnosis of sex of the foetus. Majority of obstetricians Indulge In It.

Some obstetricians In Maharashtra continue to indulge into this practice, even though it has been banned in Maharashtra from 1988. Kidney transplantation from unrelated donor is perhaps not out rightly as unethical as prenatal sex-determination, but certain y problematic. Some nephrologists are not uneasy about it at all, whereas a few have gone too far and indulged in collaborating with the traders in kidney in exploiting the poor kidney-donors and the rich recipients. (5) 3.2 : Lack of rationalisation of fees: There is no principled rationale behind the level of fees charged by consultants. The 'law of the market' operates. But since 'the market is far from perfect, there is great scope for exploitation of the weaker bargainer I.e. the patient. The consulting fees vary from As 20/for a Paediatrician in a small town to Rs 150/- for a renowned physician in Bombay. The surgeon's fee may vary from As. 500/- for caesarean delivery in a small town to Rs 5000/- for the same surgery In Bombay. This is despite the fact that the skill and knowledge required in caesarean delivery does not vary so much, as is evident from the outcome in these two different situations. The surgeon's fees thus directly depend today on the purchasing-power of the patient and the reputation of the surgeon, and not so much upon the nature of the surgery performed. It is definitely possible to categorize different surgeries on the basis of the skill required and the organ involved, this has been done and used for deciding the amount of reimbursement permissible for different types of surgeries. Therefore, the system of arbitrarily levying charges is unnecessary and is against the Interests of the patients.

4. Private Hospitals, Including Trust Hospitals Most of the .private hospitals are small, with average bed capacity of around ten beds, are housed in an apartment or a small bungalow. These poorly ventilated rather cramped hospitals are not very suitable for the patients. Moreover the nursing staff is generally unqualified, poorly paid and hence poorly motivated. Even the house-officer is many a time not competent, being generally a 'postgraduate student' from nonallopathic medical college. The quality of care and service provided varies greatly. There have been Increasing numbers of malpractice/negligence suits against private-hospitals. This reflects the quality of care provided. In many district places and such larger towns, generally, there are a few medium sized hospitals set up by a single or a group of leading doctors in that town. They are better built, better provided, but comparatively costly. Some of these hospitals have grown Into Trust-hospitals. They are in effect private hospitals, being controlled and run as private enterprises. Trusts are set up to take advantage of taxlaws and other concessions, and to get donations, grants. They are being modernized with modern gadgetry. In order to recover the investment on the modern equipments, there is an increasing tendency in these hospitals to over investigate. This is specially true In case of those hospitals which have been set up relatively recently. These hospitals have brought modern facilities of diagnosis and treatment for the benefit of the patients, but due to their commercial nature they have become channels for siphoning off money into the hands of equipment manufacturers and the elite super specialist doctors, through the medium of unnecessary diagonals and therapeutic interventions. They are supposed to treat a certain per-

centage of patients at concessional rates. But it is a moot point whether poor persons do get these concessions as stipulated. The urban-rural disparity is for obvious reasons, much more pronounced In case of private hospitals as compared to dispensaries.

5 : Corporate Sector The rise of the corporate sector is the most important development in the private health sector during the last decade. Corporate groups have been establishing ultramodern multicrore hospitals on commercial terms. Earlier, the hospitals were started by leading doctors. These corporate hospitals are, however, started by money bags in search of capital Investment, doctors and patients being incidental. The trend was set up by Apollo-Hospitals in Madras In 1983 and within two years, between 1984 and 1986, Rs 200 crores were Invested In a string of 'diagnostic centres' and ultramodern hospitals all over the country by different corporate houses. These hospitals are quite profitable. For example, the Apollo Hospitals after an Initial loss (after tax) of Rs 861akhs in 1985, has Increased Its post tax net profit to 65.lakhs in 1986 to 1671akhs In 1988 and declared a dividend of 15 % in 1988 (6) This explains the rapid proliferation of corporate medicalcentres. This also explains why they are beyond the reach of common poor people In India. They are basically meant to tap and create a market for medicalcare amongst the new generation of upper-middle and upper classes formed In India during the last decade. The Corporate sector has enough recourse to employ the new revolutionary diagnostic and therapeutic equipment. If judiciously and discretely used, they can be a great help to the patient. But due to their profit-orientation they are neither properly used nor are they available for the vast majority of poor patients. The tie-up between the new medical Insurance schemes and this Corporate Medical Sector is one important mode of tapping and creating market in medical-care amongst the well-to-do. The Insured person is more ready to undergo 'health check-ups' and the attendant Investigations because the Insurance companies pay for it. Medical Insurance is a positive step forward. But In its corporatization, what matters primarily is the profit and not protection from illnessexpenses.

6 : Private Measures:

Sector

&

Preventive

Private practitioners necessarily deal with only Individual patients and their families, and not with the 'community at large. This Is because of the nature of the Implicit contractual relationship between the doctor and the patient in which the doctor is duty bound to give medical care to the patient only. The diseaseprocess is however social in origin. h Is therefore not enough to treat Individual patients only; but there should simultaneously be measures at social level also, In which the doctor should play the role of medical expert to tell the community that eradication of malaria requires elimination of breeding sites of anopheles, that the high prevalence of Ischemic heart-diseases Is basically caused by overeating, lack of exercise, tensions ...etc. Intervention at this level, though quite essential for the health of the people, is not done by private practitioners. Even health-education and medical Intervention for prevention at personal level is also generally not done. Thus Haematinics are prescribed for anaemic patients without dietary advice

for prevention of anaemia In future; Chloroquin Is given to malaria patients, not followed up by Primaquin. Only one dose of tetanus toxoid Injection is given, In case of injury with no attempt being made to inform the patient to come for the second dose to complete the primary Immunization. Though health-education is quite important, especially amongst illiterate and semiliterate people, private practitioners rarely do this work. This again derives from the mere curative orientation in' private practice and somewhat narrow view of doctor-patient relationship in which patient pays only for relief from suffering and a possible cure. Some doctors do healtheducation, write in lay-press, author books for lay people. But these are exceptions. Secondly, health education by most doctors suffers from serious blindspots. Typically, the patient, the victim, is blamed for his/her illness by pointing out towards the habit of tobacco, or alcohol or 'dirty-habits' without going into the root-cause of such bad habits. Producers of cigarettes, alcohol and the government (which on account of huge duty that accrues to it, through the sale of cigarettes and alcohol, has now a vested interest in these two toxins) are not mentioned in discussing casual relationship in alcoholism, or smoking. Secondly, there is a tendency to tell the lay-people merely the 'do's and 'don’ts, without explaining the rationale behind these instructions and there is too often the advice to 'see your doctor'. Such health-education creates technocratic illusions on the one hand and a market for the doctors on the other. CONCLUSION: One can conclude that the private health services though free from bureaucratic apathy and Indifference, have many features which are Inimical to the interests of the patients and a balanced development of health-services in our country. There has to be some mechanism to regulate its distribution, quality and price. Let us see how this can be done.

REGULATING THE PRIVATE SECTOR Before we talk of concrete measures for regulating the private sector, the other option of nationalisation has also to be considered. Though nationalised industries are not as inefficient and wasteful as they are made out to be, it is true that the problem of how to make the nationalised sector directly accountable to the people has not been solved. Experience shows that especially in the service-sector, unless the persons delivering the services are directly accountable to the people they serve, there is always a possibility that a lot of dissatisfaction amongst the people 'and indifference amongst the service-personnel is generated. Accountability, of course, does not mean domination of consumers over service-personnel but there have to be certain norms In terms of which services would be evaluated, not by an Inaccessible agency in Delhi, but through a local, decentralised set up. In the health-care delivery, today the better option seems to be a kind of standardised, privately managed but socially financed and controlled health-care delivery system as is present In many developed countries (except the U.S.). There has to be a Universal Medical insurance to be financed by the local governments. This is to ensure that every person irrespective of his/her Income would receive proper medical-care. Doctors would operate on a fee for service basis and would attract patients on the basis of their performance. The fees should be standardized.

The health-services should be privately managed, but the doctors would collect fees not directly from the patients but from the local government. So' long as the payer is each individual patient, he/she is not in a position to bargain with doctors for the regulation of health-services and oversee Its Implementation. This has been the experience In the West. Regulation of health- care in the West has taken place primarily due to the rise of medical insurance because of which the payer was a powerful group and could successfully bargain with the powerful doctor's lobby for rationalisation of its practices. The reforms suggested below would not be fully implemented unless there is strong body which is interested in such reforms. In addition, there should be independent organisations of the people, for acting as watchdog bodies. A lot of Irrationality, Injustice to patients and wastage of resources can then be avoided.

What are these reforms? 1) The private medical colleges should be banned. 2) The system of registering medical practitioner, on the basis of experience alone should be abolished. A regular compulsory training course for existing R.M.Ps should be started and these retrained practitioners should be allowed to use only a limited number of drugs, and handle only a specified types of conditions. The training of Village Health Workers should be upgraded and they should be paid on the basis of number of households registered for elementary healthcare with them. A new category of upgraded registered medical practitioner should be trained for management of a larger list of diseases. All such categories of primary health-workers should periodically undergo appropriate CME, the' renewal of registration being dependent on completion of such CME. 3) Cross-prescription of allopathic drugs by nonallopathic practitioners and vice versa should be banned. Unless a person has undergone training to a certain degree in a particular system of medicine, she/he should not be allowed to use medicines from that system of medicine. The number of drugs allowed to be used and the type’s bf cases that can be handled should be commensurate with the training. 4) A National Medical Committee should be formed to conduct CME. With the help of IMA and associations of specialties like Indian Academy of Paediatrics, it should run different periodicals for general practitioners, and the main specialties. It should also conduct periodic refresher courses. As in the US, registration should be renewed every three years subject to completion of a minimum number of CME programmes. There should also be a test, consisting of a questionnaire made up of objective questions to be answered in consultation with books and periodicals and to be sent back by post. This is just to ensure that the contents of the journal are read. A relevant journal should be sent to each medical practitioner and the subscription recovered at the time of compulsory re-registration every three years. 5) It should be mandatory for all doctors to keep minimum necessary record of clinical findings, probable diagnosis and treatment. As a matter of patient's right, when demanded, photocopy of this record must be made available to the patient on payment of photocopying charges. Treatment pattern should broadly follow the recommendations by 00& of the standard authorities. Any variation should have a rational basis.

6) Activities of drug companies should conform to rational therapeutics. A prioritized essential drug list based on standard medical authorities be prepared. All other drugs should be banned. This list should be reviewed every three years. No new costlier drug be allowed unless it has scientifically proved distinct therapeutic advantage over the existing one. The drug information supplied to doctors should have a prior approval of a body like the F.D.A In the U.S. Unhealthy Influence of the drug-companies or any other medical company on the doctors through supply of samples, expensive gifts, sponsoring trips for academic or non-academic purposes etc. should be eliminated. Drug-companies and medical equipment manufacturing companies may fund as a matter of social responsibility, CME progra'1'mes run by associations like IMA but they should not have any say in the content of the programme. 7) The doctors' fees should be fixed on rational basis. This would also hold for operative charges for surgeons. Some big employers like certain companies have already standardized reimbursement charges. Some Trust-hospitals have also standardized operating charges on the basis of the nature of procedure undertaken. General practitioners should get examination-fee separate from medicine charges. This would help to curb unnecessary use of injections. More suggestions can be made on the above lines. The basic point is to see that patients get good quality medical-care and should be saved from exploitation and manipulation. Doctors would also get Income according to the quantity of work they do which In turn would depend upon the reputation they get through good quality work. If India's medical care system is reshaped along the above lines, the government would have to spend far more on health-care than it spends today. Today the Indian Government spends 1.17 % of the Gross National Product (7) on health-care as compared to the recommendation of 5% of G.N.P. to achieve Health For All Strategy. But how much more funds would be needed? This has to be worked out. Since people would cease to spend money from their own pocket, a health-tax, can be achieved, But it should be certainly less than what people are spending to-day on private health-care. Secondly, preventive measures on a social scale, health-education of people etc. have not been considered in the above scheme of rationalization. How can these basic aspects of health-care be carried out? This has to be discussed. Thirdly, Universal Insurance would reduce urbanrural disparity because lack of market in rural area will not be a constraint any more. But if the local government bodies are to pay the doctors, they can't

do it unless they have commensurate sources of funds. This is a big policy issue which needs a thorough discussion. Fourthly, what happens to the existing public health: service? E S IS? This also needs discussion. In short, a proper regulation of private sector implies a total change in the health-policy in India. These implications of regulations need a thorough discussion, It may however be pointed out that the seven measures outlined above can be attempted without launching Universal Medical Insurance. But the success would be quite limited. Can we build enough public pressure for a comprehensive reform in the medical-system In India even If Universal Insurance is not launched? What is the alternative? Legal suits against the doctors for malpractice In order to discipline doctors Is one option. This Is the American path. But such measures without changing the overall structure Is no solution. We would end up paying more to the doctors as has happened in the US since the doctors would do safe, 'overcautious practice, investigating more and drugging more. In addition consumers would pay' for the lawyers as well as doctor's malpractice-insurance & premiums. After all, doctors would recover malpractice-Insurance premiums from the patients. A vicious circle of legalist positions would set which would ultimately benefit only the lawyers and would inflict harm to the broad sections of the people as well as the institution of medical care in general. Let us discuss these issues in the coming MFCmeet.

REFERENCES 1) Dr. Jesani Amar with Saraswati Ananthram, Private sector and privatisation in health-care services, F.R.C.H., August 1990 Table - 6. 2) Calculated from Ravi Duggal - Medical Education in India, who pays? Radical Journal 01 Health, Vol-III, No.4, Table-4 3) Ravi Duggal, op. cit, p.6 4) Dr. Jesani - S Ananthram, op. cit, Table-5 5) The Organs Bazaar, India To-day, 31-7-90, “Trafficking in Kidneys on the rise” Times of India, Bombay, 15-4-90. 6) Financial Express 13-12-1988, Centre For Monitoring .Indian Economy, Key Financial Data, May, 1989. 7) Health and related statistics, FRCH Newsletter, (Special Number) Oct-Dec, 1989, Table-VI a.

REGULATING THE PRIVATE HEALTH SECTOR RAVI DUGGAL AND SUNIL NANDRAJ The health services planning in India is characterized by its failure to take into account the holistic picture of the health care services. In the mixed economy model the social sector is planned with a view to provide for the externality and to redistribute the services in favour of the underprivileged masses. In the post independence period the growth of the private health sector has been

tremendous. This is inspite of the fact that the first five Year Plan had clearly set out the purpose of planned development vis-a-vis the private sector - "The distinction between the public and the private sector is, it will be observed, one of relative emphasis; private enterprise should have a public purpose and there is no such thing under present conditions as completely unregulated and free enterprise. Private enterprise functions within the

conditions created largely by the State. Apart from the general protection that the state gives by way of maintenance of law and order and the preservation of sanctity of contracts, there are various devices by which private enterprise derives support from the government through general or special assistance by way pf tariffs, fiscal concessions and other direct assistance, the incidence of which is on the community at large. In fact, as the experience of recent years has shown, major extension of private enterprise can be rarely undertaken except through the assistance of the state in one form or another" (First Five Year Plan 1951-56, Planning Commission, GOI pg. 33). Over the period this has not happened in the planning process simply because the planning commission never had a holistic picture of the size, distribution and growth trends in the health care services. India has probably the largest private health sector in the world. Even in the USA about half the resources of the health sector are provided by the public exchequer. Right through the Seven Five Year Plans the planners and policy makers have never discussed the private health sector which provides two-thirds of the health care in the country. Hence plans and policies are bound to be limited in their Impact. The private health sector consists of, on the one hand, private general practitioners and consultants of different systems (allopathy; Indian system and homeopathy) and a variety of non- qualified practitioners and on the other hand hospitals, nursing homes, maternity homes, special hospitals etc. In the hospitals, nursing homes, maternity homes etc., the private sector's share Is a little over half of all such facilities in the country. Besides this there is the pharmaceutical and medical equipment manufacturing industry which is overwhelmingly private and predominantly multi-national. There are also laboratories which carry tests right from blood testing to CAT scans. The share of the private health sector is between 4% to 5% of the gross domestic product (GDP). This share at today's prices works out to between Rs.16, 000 Crores and Rs.20, 000 Crores per year. This paper deals with regulation that exists in the private health sector. The implementation of the Bombay Nursing Homes Regulation Act in Bombay as a case in point is discussed and subsequently issues relating to a comprehensive regulation system for the private sector are thrown up for debate. Let us make it clear in the beginning that privatisation and liberalization are not synonymous with lack of monitoring or of regulation. Even in the USA with a 'free market' operating there are stringent regulations for medical practice and running hospitals and nursing homes.

1 Existing Regulations The private health sector consisting of general practitioners, nursing homes and hospitals involve two thirds of the medical human power in the country. Despite this there is hardly any regulation of the practice of this sector of health. This is indeed surprising because such activity cannot be carried out without registration. The medical professional has to be registered with the Medical Council which is a statutory body that sets the standard of medical practice, 'disciplines' the professionals, monitors their activities and checks any malpractices. The doctors, who decide to set up their own clinics as well as hospitals, nursing homes, polyclinics etc., have to register with the respective local body. The problem with the above is that the controlling bodies are virtually non- functioning. The reason for this is not only lack of interest but also weak provisions in the

various acts. They are also heavily influenced by the private health sector. Another agent in the private health sector which needs to be regulated further is the pharmaceutical industry. As a chemical Industry this agent is regulated to some extent but as a participant in the health sector it operates virtually unregulated. Whereas the public health sector due to bureaucratic procedures Is forced to maintain at least some minimum requirements (e.g. they will not employ non-qualified technical staff, follow certain set procedures of use of equipment or purchase of stores etc) and is subject to public audit, the private health sector operates without any significant controls and restrictions. As per existing law the health sector has provision for regulation under three different authories. 1. The Medical Council. The Medical Council of India and the respective State Councils have to regulate medical education and professional practice. Presently beyond providing recognition to medical colleges the Medical, does not concern itself with the practioners, unless some complaint is made and a prima facie case established. Even the list of registered practitioners is not updated' properly by the Medical Councils. The National body at present concerns itself wit'" only recognizing and de-recognizing medical colleges whereas the, State bodies function only as registers for issuing a licenser practicing medicine. (The Sate .Councils also facilitate recognition of private medical Colleges which the National Council has de-recognized!). 2. The Local Bodies (Municipalities, Zilla Parishads; Panchayat Samitis etc.) have the authority to provide a license to set up a nursing home, or hospital and regulate its 1unctions. However, besides providing the certificate’ to set up a hospital or nursing come the local bodies do not perform any other function, In spite of provision In the Act. 3. The Food and Drug Administration (FDA) has the jurisdiction, to control and regulate the manufacture, trading sale of all pharmaceutical products. This is one authority which has been provided some teeth by the law. But its performance is most embarrassing. It is ridden with corruption. In spite of the ridicule it faced as a result of the Lentin Commission Inquiry Its behaviour remains more or less unchanged: Given this state of affairs the people of the country are left entirely to the whims of the goodness of doctors. With highly commercialized medical practice the latter is very rare today. In view of the existing health situation and health practices, regulation of those who provide health care is an urgent necessity. Regulation exists in other sectors so why not in health? Hence, there is an urgent need for strong measures to control and regulate the private health sector. 4. The Main Features of the Bombay Nursing Home Act (1949) This act applies only to private hospitals and nursing homes. It must be t:\ointed out here that the public health sector has its own internal regulation based on the Hospital Administrating Manual. As Indicated earlier the very fact of the existence of a bureaucracy brings about certain minimum controls and regulation. But this does not mean that we ignore the public health sector. Our suggestions are equally applicable to the public health; sector also.

The objective of the Act is to provide for registration and inspection of nursing homes. This Act extends to the whole of Maharashtra. Nursing home means any premises which is providing for treat01ent and nursing of persons suffering from any sickness, Injury or Infirmity (and Includes maternity). Anybody intending to carry on a nursing home shall make every year an application for registration or renewal to the local supervising authority which could be the municipal corporation, municipal body, district board, district Panchayat and other like bodies constituted by the government. The Act lays down conditions under which the local authority can grant or refuse to grant a certificate of registration to any private nursing home or hospital. This certificate should be kept affixed" in a conspicuous place in the nursing home. Detailed Information should be provided in terms of qualified staff, adequate staff, sufficient or proper equipment and adequate accommodation, floor space for patient beds, whether the sanitary conditions are suitable and adequate. In the case of maternity home also whether it has got on its staff a qualified midwife. The local authority has the authority to refuse to register or renew registration of any hospital, or nursing home if it is not satisfied in terms of the provisions of the Act. The Act provides that the local authority formulate bye-laws. The implementation of the Act by the Bombay Municipal Corporation (BMC) in the city of Bombay is in a very sorry state of affairs. The private hospitals and nursing homes have over the years become tax in observing the provisions of the Act because of the Indifference of the concerned authorities. Redressal can be sought in govern ment hospitals because of same amount of accountably and public audit, but in private hospitals this is not the case. As a consequence we find that private hospitals are functioning in a very arbitrary manner without any control over them. In connection with the court case referred to above the BMC has given a list of hospitals and the visits made by their staff in the last 5 years. This list is not complete since many of the wards have not flied their returns. Registrations and renewals have become mere formalities. Visits are rarely, if ever, made by the representatives of the supervisory authority to the nursing homes, hospitals etc. There are not many Instances of registration cancelled or refused to any nursing homes or hospitals. Many nursing homes, hospitals etc. continue to operate without being registered. Hospitals, nursing homes etc. continue to exist in unhygienic conditions without the basic amenities like water, proper ventilation, basic equipment, qualified staff, lack of proper sanitation facilities etc. The Act itself is deficient on various matters. It does not lay down minimum standards to be followed for setting up of nursing homes. The rules do not prescribe any standards for facilities: The rules have kept the criteria for staff, equipment accommodation etc. vague by Just mentioning 'adequate'. It is not that this cannot be done, since there is a minimal standard to be followed by govt. hospitals and 'nursing homes, for govt. hospitals there is a manual which provides Indepth Instructions about hospital management. It is divided into chapters like hospitals, buildings and compound; casualty services, organization of out patient wards, operation theaters, X-Ray department, blood banks, medical records, etc. Though there is much scope for Improvement atleast they prescribe a minimum standard, private hospitals could be made to follow atleast this to begin with especially when they charge such exorbitant fees. The bye-laws which have been formed are very limited in scope. 2 What should a comprehensive legislation seeking regulation include? The following suggestions on regulation encompass the entire health sector. However, they are not an exhaustive list but only some major important areas needing regulation. (a) Nursing Homes and Hospitals: * Setting up minimum decent standards and requirements for each type of unit; general specifications for general hospitals and nursing homes and special requirements for specialist care, example: maternity homes cardiac units, Intensive care units etc. This should include physical standards of space requirements and hygiene, equipment requirements, human power requirements (adequate nurse: doctor: bed ratios) and their proper qualifications etc. * Maintenance of proper medical and other records which should be made available statutorily to patients and on demanded inspecting authorities * Fixing reasonable and standard hospital and professional charges. * Filling of minimum data returns to the appropriate authorities e.g. data on notifiable diseases, detailed death and birth records, patient and treatment data etc. * Regular medical and prescription audits which must be reported to the appropriate authority. * Regular inspection of the facility by the appropriate authority with stringent provisions for flouting norms and requirements * Periodical renewal of registration after a thorough audit of the facility. (b) Private Practitioners: .'

• • • • •

Ensuring that only properly qualified persons practice. Compulsory maintenance of patient records, including prescriptions, with regular audit by concerned authorities. Fixation of standard reasonable charges; Regulating a proper geographical distribution" and switching over to family practice. Filing appropriate data returns about patients and their treatment. Provision for continuing medical education on a periodic basis with licence renewal dependent on it.

(c) Diagnostic Facilities: *

Ensuring quality personnel.

standards

and

qualified

*

Standard reasonable charges for various diagnostic tests and procedures. * Audit of tests and procedures to check their unnecessary use. * Proper geographical distribution to prevent over concentration in certain areas. (d) Pharmaceutical Industry and Pharmacies: * Allowing manufacture of only essential and rational drugs.

Health Ministry from Chemical Ministry. * Formulation of a National Formulary of generic drugs which must be used for prescription by doctors and hospitals. * Ensuring that pharmacies are run by pharmacists through regular inspection by the authorities. * Pharmacies should accept only generic drug prescriptions and must retain a copy of the prescription for audit purposes. In view of the existing health situation and health problems and the context of commercialized practice, regulation of those who provide the nation's health care is an urgent necessity and this entire process of regulation must have the end user (consumer) "represented on the regulating bodies.

* Regulation of this Industry should be switched to

SIZE OF PRIVATE SECTOR IN HEALTH CARE DELIVERY SYSTEM IN INDIA Amar Jesani In the 1970's and early '80s, the debate on health care HEALTH CARE HUMAN POWER services chiefly focused on the orientation of the government or the public sector. The community health According to the Shore Committee Report (1946), care approach and the slogan of Health for All (HFA) there were 47,500 doctors (one for 6709 persons), 1000 occupied the centre-stage. Official agencies and the well dentists (one for 3, 18,660 persons), 7000 nurses (one for meaning voluntary groups, for various reasons showed 45,523 persons), 5000 Auxiliary Nurse Midwives (ANMs), unusual indifference to the growth and Impact of the 750 Health Visitors and 75 Pharmacist In the country at that prlvat8lsector. Ironically, when committed voluntary time. Since then a great Increase In the number of health' groups were battling to reestablish that the health status care human power has taken place. In 1986, the country had 'of people was largely determined by socio-economic factors and that the country's health care resources could totally 7, 63,437 officially registered doctors of all systems be better distributed and made accessible to the of medicine, of which, there were 3, 19,254 (41.8 %) underprivileged majority by using primary health care allopaths, e, 72,800 (35.7 %) Ayurveds, 1, 31,091 (17.2 %) approach, the private sector largely responsible for the Homeopaths, 28,711 (3.a %) Unani and 11,581 (1.5 %) mal-distribution of health care, was fast outgrowing the Siddha doctors. In addition, there were 9,725 registered public sector. Indeed those years are marked as much by dentists in 1986. The paramedical human power was the pro- people rhetoric as by the unprecedented 2,07,430 Nurses (1986),1,85,240 Nurse Midwives acceleration in the growth-of private sector in health care. (1986),1,08,511 ANMs (1987),88308 male MPWS (1987), We have no Intention to do a post-mortem to find out 18,819 female Health Assistants (1987), 29,7~1 male why there was a failure to bring the private sector into the Health Assistants (1987), and 3,87,472 Village Health ambit of discussion. The purpose of this paper is very Guides (1986). Thus excluding VHGs, in 1986-87 we had modest. We will try to give some important data on the totally 6, 38,039 paramedical workers in the country. size of the private sector in health care delivery so that the participants at the Annual Meet of the MFC can realistically appreciate the issues for discussion. Further, RURAL-URBAN DISTRIBUTION OF HEALTHin this paper we will deal only with the health care human CARE HUMANPOWERS power and the physical infrastructure whereas in a The data on rural-urban distribution are available only separate paper, Ravi Duggal has provided information on from the census. The census data are chiefly useful to the financing and expenditure for health care. In order to understand and proportions because the counting or make clear the magnitude of the problem posed by the coverage hitrends quite Inadequate. Table 1 gives 1961, 1971 private sector, we have compared the information with the and 1981 census data: size of the public sector and wherever possible we have It is evident from the Table that there is an actual, estimated the rural-urban distribution of both the sectors. decline (from 49.6 % in 1961 to 41.2 % in 1981) in the proportion rurally located doctors of all systems of The participants/readers must keep in mind that the medicine. data on private sector in health care "are few and difficult This decline holds true for doctors of all systems except to avail of. To compound the problem, the available data homeopaths and "other doctors". Similar decline is are often grossly inadequate and there are rampant observed in the proportion of rurally located paramedics discrepancies in the figures provided by different agencies over the last three decades. (From 47.5 % in 1961 to 43.1 % for the same head and year of information. At places we in 1981) have tried to fill-In the gaps with our own estimates, but it Applying the proportionate rural-urban distribution of was not always possible. Therefore, this macro-survey health-care workers to their actual stock in the should be treated as a preliminary attempt. corresponding or the nearest years, we get the following picture. (Table 2)

Table 1 Rural-Urban Distribution of Health Human power (in percentage) Health care

1961 census

1971 census

1981 census

Workers

Rural

Urban Total

Rural Urban Total

Rural Urban Total

All Doctors

49.6

50.3

100

48.8

51.2

100

41.2

58.8

a) A11epatbs b) AyI1rvcds c) Homeopaths d)Unani e) Other Doctors f) Dentists

29.5

70.5

100

39.4

60.6

100

27.2

12.8

100

61.8

38.2

100

62.6

37.4.

100

57.3

42.7

100

52.4

47.6

100

61.2

38.8

100

63.7

36.3

100

-

-

-

52.4

47.6

100

38.8

61.2

100

54.3

45.7

100

49.6

50.4

100

59.7

40.3

100

20.3

79.7

100

22.8

77.2

100

18.5

81.5

100

47.5

52.5

100

39.3

60.7

100

43.1

56.9

100

38.2

61.8

100

30.6

69.4

100

31.3

68.7

100

66.4

33.6

65.3

34.7

100

59.9

40.1

100

45.2

54.8

39.0

61.0

100

48.1

51.9

100

1.

2.

All paramedics

a) Nurses b) Midwives & Health Visitors c) Other Paramedics

100 100

100

Table :2 The Rural-Urban Coverage by the Health Care Human power

Category

1961

1971

Rural Urban 1) All Doctors

2) Only Allopaths

3) Nurses and Nurse Midwives

1981

Total

Rural

Urban

Total

Rural

Urban Total

91,565

93.041 1,84,606

2,05,448

2,15,551

4,20,999

2,74,120

3,91,220

6,65,340

(49.6)

(5iJ.4) (100)

(48.8)

(51.2)

(100)

(41.2)

(58.8)

(100)

24,708

59,048

59,545

91,584

1,51,129

73,090

1,95,622

2,68,712

(29.5)

(70.5)

(39.4)

(60.6 )

(100 )

(27.2)

(12.8)

33,149

53,629

86,778

. 49,198 1,11,581 1,60,779

93,603

2.05,447 2,99,050

(38.2)

(61.8)

(100 )

(30.6)

(100)

(31.3)

(68.7)

3,935

848

1,302

1,917

4) Population per doctor 5) Population pet allopathic doctor 14,582

1,337

83,756 (100)

(69.4) 506

(100)

(100)

408

,2,379

2,127

5,244

7,339

1,192

3,627

7,189

817

2,550

8,883

978

3,4b9

5,614

778

2,291

.

1,030

6) Population ,per nurse

Ia,869 1,472

5,061

7) Doctors per Nurse

2.76

1.73

2.13

4.18

1.93

2.62

2.93

1.90

2.22

8) Allopathic doctors per Nurse

0.75

1.1

0.97

1.21

0.82

0.94

0.78

0.95

0.90

Notes: a) Data on stock of All Doctors and Only Allopaths under column 1971 are for the yeap1969 b) Figures In parenthesis is percentages Table 2 does not need a long comment. It is evident that we have one doctor for thousand persons and one nurse for two and a quarter thousand persons in our country. However, as it is well known, the problem is that our healthcare human power is highly mal-distributed in favour of urban areas. The issue is: Is it now necessary to struggle for their redistribution through appropriate regulatory mechanism or should we continue to bypass this problem while overemphasizing the need for "functional" community health workers? Public-Private Sector Distribution of Healthcare Human power The data on the sectoral location of doctors are the most difficult to get. Therefore in compiling Table 3 we have done lots of estimations. The data pertains to allopathic doctors only. Sources: (a) The Bhore Committee Report (1946), Vol 1 pg 13 (b) Institute of Applied Manpower Research (IAMR) and NIHAE, "Stock of Allopathic Doctors In India", 1966 pg 71-72 (estimates of Govt-Private distribution are theirs) (c) The Central Bureau of Health Intelligence (the CBHI information is corrected and sectoral distribution is estimated by us).

Table: 3 Sectoral Employment or Doctors

Year

Govt. Service

Private Sector

Total

1942-43

1300 (27.4)

34,400 (72.6)

47,400 a (100)

1963-64

39,687 (39.6)

60,502 (60.40)

1,00,189 b (100)

1918-79

69,137 (29.3)

1,66,494 (70.6)

2,3S,31 c (100)

1984-85

81,030 (27.4)

2,14,199 (72.6)

2,95,829 c (100)

1986-87

88,105 (26.6)

2,42,650 (13.4)

3,30,755 c (100)

In 1986-87, according to 'our estimates, 88,105 doctors accounting for 26.6% of the total number of allopathic doctors (3, 30,755 in 1987) were in government services. However, when compared with the total number of doctors of all systems (7,63,437 in 1986), the proportion of goverr1ment doctors comes only to 11.54 %. This estimate ignores the employment of doctors of other systems in the government sector. Assuming that about 25,000 (a relatively higher estimate) doctors of other systems are also employed In addition to the allopaths, we have totally 1, 13, 105 doctors in the government services. But still this figure is only 14.8 % of all doctors in our country. It is even more difficult to get useful Information on the nature of private sector employment of doctors. According to the IAMR study in 1963-64, 01 the doctors in the private sector, 88.4 % were sell employed and the rest, 11.6 % were employed in the private health establishments. Thereafter, to our knowledge, there is no reliable information available on this aspect 01 private sector. However, it would not be so unreasonable to suggest that even now the proportion of itselfemployed doctors in the private sector has remained the same. On applying the above estimates to the total stock of doctors, we find that 75% of all doctors are selling employed and the rest are employed with the government or private Institutions. Since there is no hard data available on the rural-urban distribution of the govt. and private sector doctors, we extend our estimation in this field, too. Assuming that 50% (56,553)01 doctors are employed with the govt. are located In the rural area, and on applying the 1981 census finding, to the total stock of doctors In 1986, we find that 2,48,822'doctors in the private sector are rurally located. This comes to 38.3% of total doctors in the private sector. Training Infrastructure: Medical Colleges: Unfortunately, we 'do not have consistent and dependable data on the training Infrastructure for doctors' of nonallopathic systems of medicine. However, the Ministry of Health publication: “Indian System of Medicine and Homeopathy in India: 1986" suggests that, there were 222 non-allopathic medical colleges, of which 144 (65%) were controlled by the private sector. Many of these private colleges evidences receive substantial government financial and System wise break-up is as follows: Ayurveda-98 colleges of which 54 (55 %) private, Unani- 17 colleges of which 11 (64.7%) private, Siddha 2 colleges, none of them private, and Homeopathy-105 colleges of which 79 (75.2 %) private. In 1986, there were 123 medical colleges for the Allopathic system, of which 21 (17 %) were controlled by the private sector. It Is Interesting to note that of 21 private allopathic colleges, 11 were established in the period 1980 and 1986. Further since 1986, a number of private colleges have been established and more being proposed, but we do not have Information on them. Thus, all In all, in 1986, our countries had 345 medical colleges of an system of medicine and of them 47.8 %were controlled by the private sector. In 1987 there were 40 dental colleges, of which 23 were established between 1980 and 1987. Although we do not have concrete information, we believe that such spectacular growth in the number of dental colleges is brought about by the entry of the private sector. For the training of nurses, in 1983, there were only 8 colleges offering nursing graduation course (B. Sc). However, in 1986, there were 386 other Institutions offering training in the general nursing. We believe that of these other institutions,' a significant number is under the control of NGOs and private agencies. Out-turn of Health Care Personnel: The data on the out-turn of non-allopathic doctors seem grossly Incomplete, for as against the admission capacity of 10,521 in 19736, the out-turn in 1985 was reportedly only 3970. It is interesting to note that of the total admitted In 1986,673 it were in private colleges. In 1977, the allopathic colleges produced 13,783 doctors. Surprisingly, since 1978, the data on the oU1-tirrn of allopathic doctors are consistently Incomplete (e.g. 1984 figure Is 10,469 and 1985 is 9177) due to the colleges not supplying information to the government. However, given the fact that between 1977 and 1987, 18 more colleges were established and no reduction in the admission capacity of the existing colleges done, we would not be wide off the mark in suggesting that the outturn in 1987 was over 15,000. If we take all systems of medicine together, it appears that the out-turn of doctors is above 20,000 per annum. The reported out-turn of dentists in 1987 was 660. The out-turn of B. Se Nursing In.1983 was only 315 where as that of general nursing was 7750. No reliable data on their out-turn since 1984 are available. Private Sector in Training: Marty studies have shown that inspite of government’s direct participation in medical education, the caste and class backgrounds of doctors have persistently shown bias towards upper castes and classes. In this situation, the trend towards private medical colleges is even more alarming. For In violation of all policy statements made In every plan, since the Fifth Five Year Plan (1974-79), two third of the new colleges established were In the private sector. For the private sector colleges charge exorbitant capitulation fees to get admlsslo'1 and the annual fee payable is ten to fifteen times more than that charged by the government colleges. Therefore these colleges are beyond the reach not only of the lower classes but also of the middle classes. By all accounts, most q& these colleges are big profit spinning business ventures. Further ,many' of these colleges do not have adequate teaching and training Infrastructures, thus substantially lowering the standards of medical education, These colleges also have strong political patronage which Is useful to them for utilising the government health care Infrastructure for training students at very low cost. As a result the government hospitals come under increasing pressure.

It should also be kept in mind that since the private colleges makes private investment in medical education so 'huge, the resultant doors would tend to prefer high-cost, high technology, urban based medicine in order to get substantial returns. This together with the inevitable malpractices would 'further Increase the cost of medical care without improvement in quality and distribution of services, Thus one should not look at the private sector medical education in isolation but should understand the logical spin-off action that the private medical education heralds, Health Care infrastructure: In our country the Primary Health Centre (PHC) Infrastructure is almost exclusively for rural areas. By 1988, 14,145 PHCs were set up. These PHCs have some maternity and family planning beds but do not normally have indoor facilities for medical care. Hospitals, Dispensaries and Beds: Between 1951 to 1988, the number of hospitals Increased from 2694 to 9381 (3.5 times), dispensaries from 6587 to 27,495 (4.2tlmes) and hospital beds from 1, 17,000 to 5, 85,889 (5 times). Rural-urban Distribution Table 4 shows that increase in the number of hospitals, dispensaries and hospital beds has not brought about substantial benefit to the rural people. In fact, the proportion of rurally located hospitals declined between 1956 and 1979, and this decline continued till 1983. Since 1983, there is a slight increase in the proportion of hospitals in the rural areas, perhaps due to the establishment of 30 bedded Community Health Centres by the government. Similar trend is found in the location of hospital beds, which are in any case, proportionally much less than the proportion of rurally located hospitals. In the case of dispensaries, the trend is even worse.

Table 4: Rural-urban Distribution of Health Care Infrastructure. Years

Hospitals

Dispensaries Hospital Beds

1956 3,307 (39.3) 7,194 (84.1)

1,45,297 (23.0)

1961 3,054 (32.8) 9,406 (53.1)

2,29,634 (15.8)

1969 4,023 (30.7) 10,440 (79.1)

3,28,323 (21.0)

1979 5,766 (25.6) 15,968 (69.8) 1988 9,381 (31.5) 27,495 (47.3)

4,46,605 (13.1) 5,85,889 (15.8)

Source: Health Statistics of India-(Care), Statistical Abstract, 1984(CSO), Directory of Hospitals (CBHI). Note: Figures in parenthesis are percent rural. Between 1979 to 1988, 11527 new dispensaries were established but their proportion in the rural areas declined sharply from 69.8 to 47.3 % in the same period. Sectoral Distribution: The table 5 shows that there has been a spectacular Increase in the number of private hospitals whereas the proportion of private beds, though modest, is steadily increasing. Further, the expansion of private sector is a cause for concern because, the number of private hospitals increased by 43.07 % per year between 1974 and 1979, by 12.06 % per year between 1979 and 1984, and 17.21 % per year between 1984 and 1988. For the corresponding periods, the growth in the number of government hospitals was very low, being 6.37%,

Table: 5: Ownership Status of Hospitals and Hospital Beds. HOSPITALS YEAR 1974 1979 1984 1988

GOVT.

HOSPITAL BEDS PRIVATE

TOTAL

GOVT

PRIVATE

2, 11,335 (78.5%) 57,550 2,832 (81.4%) 644 (18.6%) 3,476 (100) (21.5%) 3, 31,233 (74.2%), 3,735 (64.7%) 2,031 (35.3%) 5,766 (100) 15,372(25.8%) 3, 62,966 (72.5%) 1, 3,925 (54.6%) 3;256 (45.4%) 7,181 (100) 37,662(27.5%) 4,334 (44:1%) 5,497 (55.9%) 9,831 (100) 4, 10, m (70%)

1, 75,117 (30%)

TOTAL 2,68,885 (100) 4,46,605 (100) 5,00,628 (100) 5,85,889 (100)

Source: Health Information of India (CBHI), Directory of Hospitals (CBHI) Note: Figures in parenthesis are percentages and the Govt ownership includes ownership by local bodies.

1.02%, and 2.61% per year respectively. Similarly the number of private sector hospital beds increased by 20.1 % per year in 1974-79 3.86% in 1979-84 and 6.81% in 198488 periods. The corresponding growth of hospital beds in the government sector was 11.35%, 1.92% and 3.29% per year respectively in these periods. It is quite obvious that if this tempo in the growth of private sector and slaking in the growth of government sector were to continue for one more decade, we would be witnessing a de facto privatization of health care sector without denationalizing a single hospital.

The growth of private dispensaries is really phenomenal (see Table 6). The data for the year 1988 are obviously incomplete. However, a comparison between 1981 and 1984 is sufficient to observe the trend. In this period, the number of private dispensaries grew by 68.13% per year whereas the government dispensaries grew by a meagre 0.26% per year. Similarly whereas the private dispensary beds grew by 101.26% per year the government ones grew only by 5.11% per year.

Table: 6: Ownership Status of Dispensaries and Dispensary Beds DISPENSARY

DISPENSARY DEDS

GOVT.

PRIV A TE

TOTAL

13,205 (86.2%) 2,115 (13.8%) 15.968 (a) (100)

26,231 (95.2%)

1,314 (4.8%)

2,77,306 (b) (100)

1984

14,694 (69.5%) 6,438 (30.5%) 21,780 (a) (100)

30,251 (85.1%)

5,306 (14.9%)

35,742 (b) (100)

1988

13,916 (50.6%) 13,579 (49.4%) 27,495 (100)

21,659 (90.8%)

2,187 (9.2%)

23,846 (100)

YEAR

GOVT

1981

PRIVATE

Corporatisation of Health Care: The fast growth in the private medical care is also reflected in the changing organisation of health care. The penetration of large business houses in the business of health care delivery has grown in the last one and a half decades. Recently the government made a minor but important change and declared the hospital as an industry thus making it eligible to receive money from financial institutions and to raise capital by Issuing shares in the stock market. In a way, the entry of business houses in medical care is not a new phenomenon. The important and crucial difference is that earlier it was done under the garb of a public trust (i.e. the Bombay Hospital, run by a Trust is controlled by a business house) where the direct profit motive is formally not disclosed, whereas the Corporatisation heralds the era of running hospitals primarily and expressly for profit by the business establishments. In a span of less that ten years we have seen a number of corporate groups such as Apollo Hospitals Enterprises Limited, Hinduja National Hospitals, Escort Heart Institute, Medinova (which Is a division of Standard Medical Leasing of the Hyderabad based Standard Organic Group), Surlux Diagnostic Centres and many\others, which have established for-profit hospitals and diagnostic centres. It should also be noted that this expansion of diagnostic centres with hi-tech instrumentation and the modern hospitals, has taken place with full government cooperation and at times with its participation. It is also connected to the government's policy of liberal imports and encouragement to foreign investment. Further, for example, in March 1983, the government set up a hospital Service Consultation Corporation (India) Ltd, to provide expert help in setting up modern hospitals and diagnostic centres, in the installation and maintenance of hi-tech instruments to the government and the private agencies in our country as well as abroad. This Corporation is actually the only profit-making body of the Health Ministry as it declared a 15% dividend on the paid up capital for the 1987-88 (for more information see Govt. of India, "Annual Report: 1988-89, Ministry if Health and Family Welfare,") Production of Medical Technologies: In this paper we will briefly deal with (1) Drugs and Pharmaceuticals and (2) Medical Instruments. Between 1974-75 and 1983-84, the production of bulk drugs increased from 94 crore to 325 crore rupees and that of formulation from 500 crores 10 1760 crores. In the production of formulations the private sector has consistently accounted for over 90% of total production but in the bulk drug production the public sector share has uniformly declined, from 35.1% in 1974-75 to 20.7% in 1983-84.

TOTAL

The production of ‘new’ medical equipments was started in the year 1970s in India and has grown from Rs. 2.5 crores in the earlier years to Rs 19 crores in 1983 (source: Confederation of Engineering Industries-CEI, undated). However, it is estimated that 80% of all medical equipments are imported through private companies. In 1986-87, the Govt. was allowing 237 types of equipments for imports without insisting on the “Not Manufactured in India (NMI)’ NMI certificate, the procedure is highly simplified. In the case of the Govt. or the Govt. aided hospitals, only the head of the institution needs to certify the NMI. For the private hospitals, this can be done by getting a certificate from the Director General of Health Services. The increasing demand for the hi-tech medical instruments can be understood from the fact that while the Sixth Five Year Plan (1980) estimated it at As 30 crores, according to the CEI, the working group on Electronics for the Seventh Five Year Plan estimated it around As 900 crores for the plan period. Conclusion As stated in the beginning, the purpose of this paper was to provide you with as estimation of the size of private sector in health care delivery and to identify certain trends in the growth. Hopefully this data base will be useful to all in having a better appreciation of the problem. Three general findings are Inescapable. Firstly, the size of private sector in health care delivery is much larger than what the government agencies would like us to believe. In fact the sheer size of the private sector makes it urgently necessary for the progressive health groups to devise a strategy to tackle the problems posed by it. Secondly, the speed at which the for-profit private sector is growing is alarming. In fact it is virtually bringing about a macroprivatisation of health-care sector without effecting any denationalization. Corollary to this is the slower growth of the government sector. Thirdly, the phenomenon of for profit health care and Corporatisation of hospital based services (and diagnostic facilities) are bringing about profound changes in the way health care was hitherto organised. The effect of this phenomenon is wider than its actual size visible in the statistics. For its effect is almost universal-from the type of people who enter medical education, the way medical education is organised etc. to the type of medical technologies that are produced and the way medicine is practised. (Acknowledgement: Much of the data presented in this paper were collected in collaboration with Ms. Saraswathy Anantharam for a Review titled Private Sector and Privatisation in the Health Care Services" for ICSSR-ICMR Joint Panel on Health, at the Foundation for Research in Community Health, Bombay).

EARNINGS IN PRIVATE GENERAL PRACTICE An exploratory Study in Bombay

Alex George There seems to be a widespread belief which was expressed in 1974 by the participants of a UNESCO sponsored seminar on social research and development, that unlike the affluent countries, the vast majority of people in the developing countries depended mainly on the public health sector. They contended that in these countries only a minority of well-to-do people were seeking private health care.1 This belief however does not correspond to facts atleast as far as India Is concerned. Recent studies conducted and in the process of completion, show that around 70% - 75% of health care contacts are made In the private health sector.2 This calls for a greater emphasis in studying the role of the private medical practitioners In particular and private sector in general In the over-all health delivery system. The almost ubiquitous presence of private medical practitioners in every lane and by-lane of Bombay indicates that they are one of the major providers of health care in the city. They thrive probably due to the under-expansion of the Government medical sector. However, the private practitioners themselves cannot be regarded as paragons of medical virtue. People's preference if any for them is largely due to the inadequate volume of public health facilities, the difficult access to such facilities and the near total indifference they have to encounter in the Govt. hospitals. The apparently personalized sugar coating of the private sector medical package is its major marketing ingredient. Of late the reputation of private medical practitioners has been questioned in the media and by consumer bodies. This has been mainly on account of the unreasonable fees they charge, the various medical malpractices they engage in to boost up their fees;3 the suppression of medical information about their patients4 and the dubious qualifications of some of them5. The present study which is primarily of an exploratory nature had the following three objectives: (a) To understand the type and nature of medical practice. (b) To evaluate the cost of private medical practice. (c) To document details of expenditure on privately purchased medicines We have covered only the General Practitioners (G. Ps) among the private practitioners for our study.

Methodology Considering the preliminary and exploratory nature of the study we limited our sample to a small number of 45 doctors and between 1-5 patients of each of them. Probably because we have highlighted the questions pertaining to the problems of private practitioners at the very beginning of the Interview Schedule itself, we could elicit response from 33 out of 45 private doctors whom we contacted which works out to a response rate of 73.33%. Of the 12 (26.66%) doctors who have not responded as many as 6 are allopaths out of whom 5 are MBBS degree holders and one an MD. In order to give representation to the practitioners of different systems of medicine in our sample we have contacted doctors from all systems. By and large even those who were qualified in the non-allopathic systems of medicine were practicing Allopathy in spite of the fact that they were not entitled to do so. Among the Homeopaths also there were only a few who actually practiced their own system of medicine. The response rate could not be raised beyond the 73% at which it stands, in spite of the fact that the investigators were given an official introduction letter from the organization, where it was stated that the information on the practitioners Income will be treated as confidential and used only for aggregate analysis. The researcher himself had also participated in the data collection to initiate the Investigators into It. This was necessitated because we found that doctors had a tendency to brush aside investigators saying that they were too busy. In order to ensure response we had also resorted to telephoning and taking appointments or even personally meeting the doctors once to confirm the appointment. Since there were no up-to-date lists of doctors available from which a selection could be made for sampling, the only way out was to go to the different localities, trace the lanes and by- lanes and then randomly pick up the doctors and patients for the interviews on the basis of the display boards of doctors. We have taken care to Interview doctors from the diverse localities of Bombay. The sample Includes doctors from rich residential areas, middle class localities as well as working class dwellings. We have selected doctors operating on the sides of the main

roads and having better practice and also those practicing in the Interior by-lanes. Eliciting response from the patients was also equally difficult. The patients of as many as 10 out of 33 doctors who co-operated did not respond. Out of the patients of the remaining 23 doctors only one patient each responded In the case of 5 doctors. This slim response from the patients can only be attributed to the medico-scientistic power wielded by the doctors over the very bodies of the patients. Patients were found to co-operate when they themselves saw the researcher/investigator interviewing the doctors. But this also was not possible always as those patients who waited to see the doctor's interview getting over would like to have their consultations fast and rush home. But on the whole it was more difficult to convince those patients who were not in the clinic when the doctor was interviewed. Even when the patients co-operated there were several cases where they haven't given the expenses at the clinic. As many as 20 out of the 65 patients who have responded have not given any information about their expenses at the clinic or out of the clinic. This is mainly because of the tight professional grip of the doctors on their patients. The lack of response from patients leaves us with the responses of only 45 patients of just 23 doctors which seems to be a very small sample to compare and cross-check the responses on the doctor schedule. Therefore for the time being we present the information gathered from the doctor's schedule alone. Professional Information on Doctors About 1/3 of the doctors interviewed had 6-10 years of experience in private practice. This category constituted 30.30% of the total 33 doctors who responded. On the whole about half ie. 42% of the doctors had within 6-15 years of practice to their credit. There were some i.e. 12.12% of doctors who had put in more than 20 years of practice. On an average a doctor puts in around 43 hours of work during a week. Almost all worked for 6 days a week, taking an off on Sunday. Thus the daily hours of service of most doctors worked out to a minimum of 7.16 hours per working day. Exactly 2/3 i.e. 66% of the 33 doctors who responded did not have any attachment with either Govt. or private hospitals, the ESIS and other corporate bodies such as the LlC, Air India etc or private companies. This indicates that the mainstay of their professional Income came from their clinics. Investment Income & Expenditure in Clinics Of the 37 clinics run by the 33 doctors interviewed

20 I.e. 54.05% were owned by them. The mean investment for setting up a clinic comes to about Rs. 85, 000 /-. Bank loans are resorted to when a higher quantum of loan is required. The average monthly number of patients attending a clinic was found to be as high as 945. The main component of doctor’s income is his service charges including Income from dispensing medicines, his Income from ad. ministering injections and that from home visits. Consultation fee as such is not available in many cases. It is submerged into a common 'medical fees' which Includes charges from dispensing medicines. One can say that these medical fees make it appear as though that the doctor is offering his services free. The average monthly proceeds of doctors on account of service charges and medicines come to Rs. 17,675. Average monthly income from injections come upto Rs. 5466 and constituted a sizeable proportion in the total income of doctors. It amounts to 29.82% of the net Income of doctors. The blind belief of the patients in the efficacy of the injection is being made ample use by the private practitioners. The average net Income of doctors after deducting the Items of expenditure such as drug costs, rent, maintenance charges, attendants' salaries etc. amounted to Rs. 18332.88. M average as it is, it must be noted that there are doctors who earn below this sum and those who earn far above this. But it should still be noted that even the median Income which is not affected by extreme values comes to a huge sum of Rs. 16,560/-. At the face of it this amount would seem to be very high. But our consultations with some socially committed practitioners revealed that it is quite a reasonable average for private practitioners in a large city like Bombay. There Is In fact very little authentic secondary data on the Income of doctors which can be used for comparison. However we would like to present some of these data, only to show that far more serious research has to be undertaken to generate reasonable and reliable information in this field. An average derived out of voluntary disclosures by doctors for the 1981 census, painted a sorry picture of them with their Income coming to only Rs.1, 526.83.6 A closer look at the census data revealed that of the 42361 doctors who responded only 11.67% were from the private sector? While the public sector doctors gave an underestimation of their salaries and did not mention their private professional incomes, it seems the few private practitioners who responded also did not divulge a figure any where near their actual Incomes. Mother study of doctors In Jodhpur City which does not state the year in which It was conducted, but was published In1985 gives the average Income of private

practitioners at an abysmally low Rs.783.8 per month’s We do agree that the Income figures of doctors in Bombay will be considerably high than the rest of the country because of the large population that provides a huge market to the private practitioners. But neither the 1981 census figures nor the subsequently quoted study has arrived at a reasonable average. Looking Into the expenditure of doctors we find that expenses amount to only 17.95% of the total gross income of the 33 doctors. This leads us to conclude that even after giving due weightage to the economic value of the doctor’s services, the profitability ratio in private clinical enterprise will be far too high. Since 20 of 37 clinics run by 33 doctors are owned by the doctors themselves, most of the doctors are able to save on rent which is a major expense item in Bombay. The drug cost which is the most important ingredient of the doctors' expenditure constitutes around 65% of their total expenses. The mean drug expenditure of Rs. 2681.5, It must be noted is only 12% of the total average turn-over of the doctor. Though the private practitioners are permitted to dispense medicines they are not supposed to make profit out of such dispensing. The reality however is the reverse. Not only do the private practitioners run a drug business of their own, taking perhaps a higher profit rate than the medical stores, they also put to use their professional monopoly over medical knowledge to sustain themselves in the business. Patients are never given the prescriptions when medicines are dispensed in the clinic. To further ensure that the patients never come to know of the names of medicines even the foils In which drugs are packed are 'safely' removed before they are given to patients. Dis. pensing medicines assure the doctors not only of a high profit but more importantly it virtually camouflages the consultation charges, making it look as though that the doctor is offering his services free. The patient is so conditioned as to consider that the little extra price on the drug is all that the doctor gets. This Is an Important fact for policy makers and the Medical Councils to consider because it is not only a question of doctors overcharging but also one of medical ethics (profiteering from sale of medicines). Hence there is a need for a strong policy Initiative to both standardize the consultation fees that doctors charge on some rational basis and to prevent making of profits from sale of medicine through strong strictures on drug sales and demanding proper prescription practice from doctors. Infact a code for good medical practice needs to be developed and put into force. CONCLUSION Our findings though based on a small sample indicate that Private medical practice is one of the best paid professions in Bombay. Very few other professions,

even in metropolitan Bombay can assure one of an average net Income of above Rs. 16,000. The fact that the doctors are dealing with the very physical existence of the human beings and that too earning such large sums9 drawn from the society must demand from them some social responsibility. Large sections of the society who depend on the private practitioners for medical service have begun to suspect them on the grounds of their medical ethics. It is in this situation that some form of regulatory mechanism to control and monitor the functioning of the private practitioners has become necessary. ACKNOWLEDGEMENT

The author acknowledges his thanks to Mr. Ravi Duggal who made various suggestions In the course of this research work and Dr: N.H. Antia who commented on an earlier draft. Ms. Shanana Shetty and Ms Sandhya Prabhudesai have rendered research assistance. REFERENCES 1. Madan T.N. et al. Doctors and Society, Vikas Publishing House, New Delhi, 1980, pp. 5-6. 2. Duggal Ravi & Amin Sucheta - Cost of Health Care: A household Survey in an Indian District, FRCH, Bombay, 1989, p. 49 and Provisional Results of a Household Health Expenditure Study in Madhya Pradesh being conducted by FRCH. 3. Siva Kumar Revathi. The Great Medical Scam, The Times of India, Bombay, January 7, 1990. 4. Iyer Saroj. The Mystic files, The Times of India Bombay, December 15, 1990. 5. Krishnan G.V. - "Quacks in T.N. Ducking the Law". The Times of India, Bombay, October 10, 1989 and "Crackdown on Bogus Doctors, Indian Express, Bombay, July 11, 1988. 6. Dept. of Science and Technology and Council of Scientific & Industrial Research Degree Holders and Technical Personnel Survey. Census of India 1981 Statistical Tables on Scientific and technical Manpower - Vol. 1, pp. 354-58, 366- 70 and 384-88. 7. Ibid 8. Chandani Ambika - The Medical Profession A Sociological exploration, Jainsons Publications, New Delhi, 1985, p. 32, See also Chandani Ambika, "City Doctors: A Social Profile" in LaISheo Kumar and Chandni. Medical Care: Readings in Medical Sociology, Jainsons Publications, New Delhi, 1987, p. 64. 9. "Doctors should not become money-machines". The Hindu, Madras, Feb. 24, 1989.

PRIVATE HEALTH EXPENDITURE RAVIDUGGAL Health Care Services Scenario India has a wide variety of health care services available to its population. On the one extreme there are the hightechnology hospitals and diagnostic centres (both private and pubic) in metropolitan cities, and on the other, one has village health guides, folk healers, faith healers and quacks in remote village. Between these two extremes there are district general hospitals (civil hospitals), private hospitals, 'trust' hospitals consulting and general private practitioner dispensaries and clinics (allopathic, ayurvedic and homeopathic...) rural/cottage hospitals, primary health centres and sub-centres. Are there an adequate number of health care providers In India to meet the health care needs of the population? This is a difficult question to answer. If one looks at the official/published data then the aggregate ratios that emerge (doctor: population, bed: population etc...) reveal that there is a large shortfall when one considers any adequate minimum standard. For instance, In 1988 In India there was one allopathic doctor per 2300 population and one hospital bed per 1300 population (CSHI, 1989). As per the standards set by the Shore Committee in 1946 these ratios should have been 1: 1600 and 1: 175, respectively, distributed evenly all over the country (Shore, 1946, /11.3, 4). For the figure on doctors if we also consider the non-allopathic registered practitioners then we are well ahead of the Shore Committee's recommendation today, the ratio being 1 doctor per 975 populations. Of course, this is not evenly distributed all over the country. If we disaggregate the 1988 figures for India on the basis of their location we find that the urban areas are nearer the Shore Committee standards whereas the rural areas are embarrassingly far behind. In rural India the (allopathic) doctor population ratio is 1:7900 and the bed population ratio 1:5440 whereas in the urban areas it is 1:790, and 1:400, respectively (CSHI, 1989). The ratio for the rural areas would improve considerably if we include the non-allopathic and the non- qualified practitioners. Like hospital beds, the number of hospitals, dispensaries, health centres, nurses and other paramedics are far from adequate, especially in the rural areas. As for medical practitioners if we consider practitioners of all systems of medicine and add the non-qualified practitioners (quacks) then their number for the country becomes more then adequate. The same is true for pharmacists also. The reason for the large number of medical practitioners and pharmacists is very obvious - a thriving for- profit private health sector (private medical practice and the pharmaceutical Industry). This scenario thus reveals that the for-profit private health sector exists in India In an adequate quantum but this (the qualified lot) is not available to the entire population easily because of its urban _ metropolitan concentration; and secondly the quality of a large proportion of this sector is questionable. The Shore Committee's recommendation of the minimum decent standards was for the public health sector but in the last 45 years this sector's performance has been very poor. Over three fourths of the investment of the public health sector has taken place in urban areas, where less than one-fourth of the country's population resides. When we consider medical care specifically, the public health sector's performance in the rural and other peripheral areas is even worse. In contrast, the private health sector has grown rapidly in the post colonial period with State support. The State's health sector policies have encouraged the growth of the private health sector in medical care - specifically curative services -'by investing resources in medical education, providing subsidies and soft loans to set up hospitals and private practice, by giving tax and duty waivers to the hospital sector and for Import of medical equipment, and by allowing graduates of medical colleges (who have been trained at public expense) to set up private practice freely or to migrate abroad in large numbers. Given the above scenario private health expenditure assumes a great significance because to support such a huge private health sector including the non-qualified) the quantum of household resources being expended must be phenomenal. Private Health Expenditure Information about health expenditure In India is very scanty. Public Health Expenditure is fairly well documented (officially only) because of the sheer fact of accountability of expenditure to the office of the Comptroller and Auditor General of India (see Table 1). In contrast to this, expenditure on private health care is very poorly documented. The

National Sample Survey's (NSS) earlier rounds (nineteen fifties) have recorded fairly reliable Information but later rounds have not paid any heed to this category of consumer expenditure. The Central Statistical Organisation (CSO) has been making estimates, partially based on NSS data (see Table 1) but when one compares their data with smaller empirical studies then CSO estimates appear to be grossly under-estimated. In fact their estimates are contrary to the growth of the private health sector. In the seventies and eighties, when the private health sector was rapidly expanding (see Amar Jessani’s paper) the CSO's estimates of private health expenditure were declining with respect to public health expenditure I That Is, over the years the share of expenditure of the state sector has enlarged in comparison to private health expenditure as well as in terms of proportion of GDP. (Table1). This is a difficult proposition to swallow when we consider the rapid growth rate of the private health sector in the past 15 years. We will not go into further details of Table 1 because it is self-revealing. A review of known studies on private health expenditure is presented here. This will be followed by a concluding section on issues emerging out of the existing scenario. When the Bhore Committee set out to examine the state of the health sector in India it had only one estimate of private household expenditure. This was A.S. Lal's Singur study which showed that in 1944 private household expenditure on health care was Rs. 21/2 per capita. In comparison the State health expenditure in the same year was only 36 paise per capita. (Shore, 1946) This totaled upto 4% of the GDP with private health expenditure having a share of 87%. The third round of the NSS In 1951 recorded a private health expenditure of As. 5.77 per capita per year (NSS, 1952). Together with State health expenditure in the same year it worked out to 2.53% of GDP and here too the share of private expenditure was 87%. In the fifties and early sixties Prof. S.C. Seal and his colleagues conducted pioneering general health surveys in districts from nine States. In these surveys private health expenditures were also recorded. The average was As. 3.34 per capita and these varied in different districts from between As. 0.40 to As. 7.20 per capita (Seal et. al; 1961, 1962, 1963) but what was remarkable was that this health expenditure worked out to between 3% to 4% of the respective SDP and the private health expenditures share was between 83% and 88%. Similar smaller studies were done in the sixties and seventies which also recorded household health expenditure A.L. Parker in Narangwal In 1968-69 and 1973-74 recorded a private health expenditure of As. 7.65 and As. 21.30, respectively, per capita per year. Sunder Rao in North Arcot in 1973 recorded As. 80 per family per year. NIHAE in 1973 recorded As. 72 per family in rural Delhi. (Quoted in Banerji, 1980). These private health expenditures again amounted to a share of over 80% of total health expenditure. The NSS results 01 the 28th round (1973.74) also corroborates this.

Table I: Health Expenditure in India by Plan Period (Including Plan and Non-plan Expenditure) Plan I

Plan II Plan III Non-Plan Plan IV Plan V

1951-56

1956-61 1961-66

1966-69

1969-74

1974-79

1979-80

1980-85

1985-.90

1990-95

393.74

712.59

723.59

2238.48

4728.32

1439.88

12970.53

28,000

65,000

85.59

161.23

265.62

261.98

720.09

1553.49

433.93

3517.73

6,800

15,000

3. 1 as % of GDP

0.41

0.60

0.71

0.77

1.00

1.19

1.41

1.59

4. 2 as % of GDP

0.18

0.25

0.27

0.28

0.32

0.39

0.42

0.43

5. CSO's estimate of Pvt.

536.00

1513.00

3768.00

8331.00

2577.00

13821.00

1. State Health Expenditure 197.30

Plan VI Plan VII- Plan VIII*

(Rs. crores) 2. Of which Medical

Services (Rs. Crores) (n)

796.00 1508.00

Health Expenditure (Rs. crores)(b)

5as% of GDP

1.11

1.21

1.51

1.62

1.69

2.10

2.52

1.69

7. 2 as ratio of 5

0.16

0.20

0.18

0.17

0.19

0.19

0.17

0.25

6.

(a)This includes only medical care provided for the general population and hence excludes medical expenditures on schemes like ESIS, CGHS and others which are benefits only for employees of the state sector / public sector. (b)CSO's estimates refer only to medical services and medicines and hence are comparable with row (2). (*) estimated (@) Projected Source: CAG, various years, CSO, 1989. In studies undertaken by FRCH in the eighties similar results were obtained. Art exploratory survey in "Bombay of a middle class and working class population revealed private health expenditure to be as high as 6.9% and 12.5%, respectively, of their average income in 1984 (Duggal, 1986). In a fairly large study in 1987 in Jalgaon district private health expenditure was recorded as 5.2% of income, (Duggal and Amin 1989). The results of all these studies are in sharp contrast to the CSO's estimates of private health expenditure. They may be small studies but they show a definite pattern and that too over a long time period. What is evident from the above review is that the financial burden of households in meeting their health care needs is substantial. Households spend between 4 to 7 times of what the State spends on health care services. This is not a very happy state of affairs considering the fact that more than half the country's population has resources that barely meet their food requirements. When illness strikes it necessarily eats into food consumption and worse still the capacity to earn if the patient happens to be a breadwinner. Thus it is important to understand the consequences of such a high private health expenditure in the context of the socio- economic scenario of widespread poverty. * We will now briefly look into some analytic Issues that emerge out of the Jalgaon study (Duggal & Amin 1989) referred to above. This will help in raising relevant issues vis-a-vis the consequences of a burdensome private health expenditure. The Jalgaon study made an effort at revealing class differentials of morbidity, treatment and health expenditure. This kind of an analysis has not been attempted in the past except by the studies conducted by Prof. Seal. NSS probably has this kind of data but it has never published it there is hope that they will be doing it for the 42nd round (1987) results. Though the data of Seal's studies and the Jalgaon study are not strictly comparable, we nevertheless give in Table 2 the two sets of data Just to indicate the similarities that are evident in class wise disaggregation of private health expenditure. Given the socio-economic conditions in India the distribution in Table 2 is not surprising. When health care services have to be purchased most often as commodities such a distribution is bound to emerge because purchasing power (Ppower) becomes a crucial factor. The Jalgaon study threw up a serendipitous finding. Contrary to expectation we found that morbidity prevalence increased with rise in class status. After a careful analysis of al: associated variables we hypothesize that definition of illness is closely linked with the availability of P-power to buy health care services in a market economy. This hypothesis is strongly supported by class differentials of health care utilisation and health expenditure also. To summaries these interrelationships with rise in class status: Morbidity prevalence increases (Pearson's r = +0.81), nonutilisation of any health care facilities declines (Pearson’s r = -0.90), use of private health facilities increase (Pearson's r = + 0.98), use of public health facilities decline (Pearson’s r = -0.89) and per capita health expenditure increases (Pearson's r = +0.94) (Duggal & Amin, 1989). Thus, the poorer classes, due to their impoverished conditions and lack of P-power perceive a lower morbidity rate because they cannot afford to spend on every small illness or chronic ailment that may afflict them. Even of the morbidity that they perceive' a fairly large proportion stays unattended because they feel it is an expenditure that can be avoided; and when they decide to use a facility, they prefer public health services because they cost the least. Table 2: Class wise Distribution of Private Health Expenditure Seal el. al (1957) and Duggal & Amin (1987) (Figures are relative (%) expenditure when Mean = 100) Poorest Study (Ref year)

Seal (1957) Duggal (1987)

1 33 29

II 51 83

Richest III 101 140

IV 194 228

Source: Seal et. al. 1961, 1962, 1963 Duggal & Amin 1989.

V 765 202

Mean 100 100

These findings then clearly provide a basis to question the commodification of health care, the existence of the private health sector and as a consequence expending of vast sums of personal health expenditures by households. Another important related issue that emerges, especially in the context of increased private sector expansion, is that of user charges. Results of studies like the Jalgaon study have a tendency to be misused because they supposedly show that people have the capacity to pay. Hence they conclude that people can also pay user charges at public health care institutions. This is a highly dangerous conclusion because most people spend on health care not out of choice but forced by circumstances, especially the nonavailability and inadequacy of public health care services. In conclusion, we would like to emphasis’s that the large volume of private health expenditure in India is probably one of the largest in the world when viewed as a proportion to total health expenditure. Even in the USA about half the expenditure on health care is incurred by the State. In the European capitalist countries the State's share is now over 80% (Schieber & Poullier, 1988). These facts thus indicate that even under capitalism private health expenditures are on their way out. This situation has arisen in these countries for two reasons. Firstly, a demand for universal and relatively equitable health care, and secondly the need to curb rising cost of health care. In both cases only Increased State intervention has helped sort out matters. Thus in India one needs to look at the private health sector and private healthexpenditure in this context also. (This paper is partially based on my Ph. D dissertation, which I am currently pursuing on a ICSSR National fellowship and partly based on the ICMR sponsored studies on Health Expenditure in India).

References Banerjee, 1980: Study of Public Expenditure arid its Beneficiaries in a PHC, M.D. Dissertation, University of Delhi. Bhore, 1946: Report of the Health Survey and Development Committee, Vol. I to IV, Sir. Joseph Bhore (Chairman), GOI, Delhi. CAG, various years: Combined Finance and Revenue Accounts, 1951- 52.1984-85, Comptroller and Auditor General of India, Ministry of Finance, GOI, Delhi. CBHI, 1989: Health Information of India, Central Bureau of Health Intelligence, Ministry of Health, GOI, Delhi. CSO, 1989: National Accounts (New series) 19511979and 1980 - 1986, Central Statistical Organisation, GOI, Delhi. Duggal, 1986: Health Expenditure in India, FRCH Newsletter, Vol 1(i) Nov-Dec. 1986. Duggal and Amin 1989: Cost of Health Care: A Household Survey in an Indian District, FRCH, Bombay. NSS, 1952: Report of the 3rd round 1951-52, National Sample Survey Organisation, GOI, Delhi. Schieber and Poullier, 1988: Intervention Health Spending and Utilisation Trends, Health Affairs Vol 7(4), Fall 1988. Seal et.aI1961, 1962, 1963: Report of General Health Survey in CDP Blocks in MP, Bengal, Bihar, Rajasthan, Assam, U.P, Manipuri, Delhi, DGHS, GOI, Delhi.’

PRINCIPLES OF NATIONAL HEALTH POLICY. S JANA & S DAS. 1.2 Health Care, Medical Care and related terminologies: According to current vocabulary, HEALTH CARE means all rounds care of those requirements of life, each of which exerts a determining influence on health. These involve food-clothing-shelter, water, sanitation, environmental pollution, education, employment and, of course treatment of diseases. Pari passu, MEDICAL CARE is understood to be treatment of diseases i.e., care of the sick. It is clear, therefore, that 'Medical Care' is 'an integral part of 'Health Care', the latter being much more than the former and one is not synonym of the other. It may be mentioned that though the commentators, academics and policy makers are aware of this distinction, they often use these term synonymously, perhaps through carelessness. We should avoid it. 'Public Health' is another term which is used to mean the measures on sanitation, water supply, prevention of communicable diseases, hygienic practice etc., including immunisation. It thus constitutes a part of overall health care. Certain other terms e.g. 'Curative' 'Preventive', 'Promotive', 'Rehabilitative' heath care are self-explanatory.

'Curative' means treatment of diseases, 'Preventive' may be equated with Public Health, 'Promotive: 'concerns with health education and hygienic measures, and 'Rehabilitative' deals with measures of physical and occupational rehabilitation of the handicapped and disabled.’ Primary Health Care' is a term introduced to signify a set of minimum socially acceptable (i.e., the standard varying from society to society) measures for 'Health Care’. ‘Community 'Medicine ' or 'Community Health' is another category which is not very clearly understood. It presumably means a system of health care service which provides for all round health care of the community (here community is the unit of recipient as against individual). 'Traditional Medicine', 'indigenous Medicine', 'Tribal Medicine' etc., identify old systems of medicine or therapy which were prevalent before the Introduction of modern western system of medicine and are still practised mostly in the third world countries. 'Alternative Medicine is something which aggregates all kinds of therapy other than the modern medicine. 'Integrated Medicine' is a concept which envisages integration of different acceptable elements taken from all varieties of systems of medicine and therapy, on a pragmatic, empirical basis. 2. Critique of National Health Policy: The Report of the Bhore Committee (1946), constituted by the British Government, not 'only gives

the perspective, data base and analysis of the health scene but is also a policy document. The postindependence Government of India owes much to this report for their conceptual education and policy exercises. In 1983, the Congress (I) Govt. presented a National Health Policy (NHP) before the parliament and it was accepted with any discussion. This NHP has actually been egged and overwhelmingly influenced by 'Health for All by 2000 AD' (HFA) programme of WHO in 1977, to which the GOI is a signatory; one may even observe that NHP is an adoption from HFA. One should note here that the left political parties or individuals neither made any critical observation of NHP in the Parliament or outside nor the left Governments in the provinces have ever ventured to make any sort of policy declaration on health except towing the line of Cong(l) policy when occasions demanded. There are of course individuals or groups who may be described as radicals or progressives, who did make critical analyses of the NHP from time to time. Their comments and actions, added to protest actions by common people and agitative actions by medical community, particularly junior doctors and service doctors, have made some sort of impact on the society to the effect that health policy is now being increasingly debated and concerns expressed. 2.1 The NHP acknowledges the Constitutional pledge of raising the standard of living of the people and improvement of public health among the State's primary duties, and India's rich heritage of medical and health sciences in the ancient past. From here on, the NHP frequently expresses self-contradiction in discussing major issues. Health policy and programmes pursued so far, it says, has gained considerable achievement in the promotion of health status of the people as evidenced by improved vital statistics and expanded infrastructure. Thereafter the NHP cites vital statistics, incidence of communicable diseases, poor facilities of potable water supply and basic sanitation, miserable state of poverty and ignorance etc. to show that 'the demographic and health picture of the country still constitutes a cause for serious and urgent concern.' Again, declaring that the old health policy 'may have generally served the needs of the situation in the past', the NHP makes a decisive conclusion as regards the failure of the old health policy. It is necessary to quote at length: The existing situation has been largely engendered by the almost wholesale adoption if health manpower development policies and the establishment of curative centres based on the western models, which are inappropriate and irrelevant to the real needs of our people and the socio-economic conditions obtaining in the country. The hospital-based disease and cureoriented approach towards the establishment of medical services has provided benefits to the upper crusts of society, specially those residing in the urban areas. The proliferation of this approach has been at the cost of providing comprehensive primary health care services to the entire population, whether residing in the urban or the rural areas. Furthermore, the continued high emphasis on the curative approach has led to the neglect of the preventive, promotive, public health and rehabilitative aspects of health care'. The concept underlying this diagnosis of the ills of our health care service should be critically analysed and challenged first before one proceeds to examine the remedy prescribed by the NHP on the basis of this diagnosis. The conceptual basis is important, infact, crucial since it is very apparent that people of different levels and interests i.e., right, left, radicals and whatnot, have accepted the validity of this concept

originally mooted in the capitalist countries and later dressed up and adopted by the WHO and ruling circles of our country. 2.2 Conceptual Distortion: With the help of medical care, health of an individual or a community can neither be protected nor improved. When there is a breakdown of health and is manifested, medical care can repair the body or remove the immediate precipitating cause of the breakdown but cannot prevent recurrence or remove the more basic causes of the breakdown. Health care on the other hand, provides for elements which determine the health status it can combat the basic causes and thereby prevent breakdown of health. By preventing breakdown, by keeping the body healthy, it prepares the ground, for further enhancement and consolidation of the body defence in order to maintain a sustaining status of disease-free state i.e., improvement and promotion of health. It is therefore clear, that building up of medical care infrastructure i.e., curative centres amounts to siphoning the money and resources down the drain, a colossal wastage of meagre resources. But the western model of health care service taught us just that. We swallowed it and put emphasis on medical care and that constitutes the basic cause of the failure of our health care service. We should not have done that, the NHP observes, since' the approach of our ancient medical system was of a holistic nature, which took into account all aspects of human health and disease. Since this concept possesses a substantial grain of scientifically established truth, the resulting conclusion gain's credibility and draws acceptance. The concept, however, is irrelevant, the reading of the situation is erroneous and the conclusion turns out to be motivated. True, curative care can not prevent and preventive care does prevent ill health and therefore, real freedom from disease does lie in the way of preventive care. But curative care and preventive care are not contradictory or opposing to each other but complimentary.1t should not require much intellect to realise that curative care is needed by the ill whereas the target of preventive care is the non-ill i.e. who is not afflicted with disease. No amount of preventive care-can restore health for the ill who will suffer loss of earning /limb/life without curative care. i.e. curative care is a must for the ill persons. For the non-ill, the question of curative care does not arise at all, their need is preventive care. So it is clear that the constituencies of medical care and health care are different and do not coincide. An individual or a community needs both curative care and preventive care, each at different points of time. WHO and NHP pose the question in the mannercurative vs preventive or medical care vs health care, either due to naivete or from ulterior motive. It is a distortion of scientific truth. The question of emphasising one, burying the other does not arise. A community should receive both medical care and health care in adequate amount and of optimum quality -no more -no less. The reading of the Indian situation is also erroneous through and through, it is not true that the western model emphasises medical care, rather quite the opposite. The historical development of the western model shows that public health service developed much earlier than public medical service and attention to preventive care was drawn at a very early stage. At its present developed stage, both these elements are highly developed with rather more attention to preventive care and in any case there is no competition between the two. What we have adopted here is not the real western model but its caricature. Secondly, it is

not at all true that the Indian State has put high emphasis on curative approach; had it been so', problems and deficiencies in curative service would have disappeared or diminished. It is, on the contrary, increasing. Thirdly, the excuse that emphasis on curative care is the cause of neglect of preventive care is a poor attempt to cover up the guilt. How can there be a conflict or competition between programmes of medical care and health care? Medical Care is the job of the health administration while provisions for foodclothing-shelter, water, 'sanitation, employment, education, environmental sanitation etc. are managed by other branches of the government. Is it being implied that disproportionately larger allocation was given to health administration depriving others of their legitimate dues? No such plea is tenable. Even a cursory look at the budgetary allocation of the Centre and provinces will show that percentage allocation on health has steadily come downward In course of time. The excuse, therefore, appears to be an attempt to deceive. Fourthly, it is also not true that the postindependence health infrastructure was built with a curative approach, particularly in the rural areas. The rural health care service centering around the health centre model had rather put more emphasis on those elements of preventive care which fall under the jurisdiction of health administration; curative care was envisaged to constitute only a small share. That preventive activity has later gone to the dogs and health centres are now overburdened with curative demands is an entirely different story which will further show, as discussed later, the untenability of the conceptual basis and 'conclusions of NHP. Rightly, the conclusion that differential accrual of benefits to the urban/affluent and the rural/poor due to so-called emphasis on curative care is an attempt to pass the buck on to the other shoulder. True, the urban affluent has benefited most from the free supply of costly State medical care but that is due to their dominant sociopolitical status and one should not forget that this differential account of benefits is not peculiar to medical care service but prevalent in all other welfare services distributed by the State e.g. education, housing, water supply, etc. No wonder therefore, that such lopsided conceptual basis, an utter lack of recognition of reality and a peculiar gullibility leading to uncritical acceptance of WHO's concepts carry the risk of landing into wrong remedial measures. But it may not be that simple i.e., it may not be simply a matter of honest mistakes and wrong calculations. The Government of India may not be playing a passive role. The NHP may be a calculated policy emerging from political compulsions. This suspicion is strengthened when the programme offered by the NHP is examined. 3. Programme: Major elements of the health care service programme are summarised with critical analysis point by point. 3.1 A comprehensive primary health care service (i.e. incorporating preventive, promotive, rehabilitative, and curative) for all, delivery mechanism reaching as near to the community as possible. A three tier system of curative service primary (general curative service by paramedical and medical personnel), secondary (common specialty service) and tertiary (superspeciality service) centres, integrated by a well organised referral system with which non-State sectors i.e. private practitioners of all systems and voluntary health institutions will be

functionally incorporated. For this purpose, private involvement will be encouraged and assistance will be given to private practitioners and voluntary organisations. In the secondary and tertiary centres, the poor will receive free service while affluent will have to pay. First priority will be accorded to tribal, hill, backward areas and the sections of population vulnerable to endemic diseases. Coordinated programmes will be launched for the handicapped, disabled, infirm and the aged. The service of indigenous practitioners will be utilised specially for preventive, promotive and public health objectives. No doubt, this is an ambitious scheme. It appears that NHP did not hesitate to set itself this tall task for the simple reason that the scheme is self-contradictory, fantastic and almost impossible to implement. In the policy itself, there is a call for deprioritisation of curative care but in the programme, an almost universal coverage of medical care has been proposed. Reality of the state of medical care is the fact that there are two parallel systems at work - State sector and private sector. According to one estimate, the private sector comprises of more than two-third of medical care. The NHP is aware of such reality but does not offer any clear programme to assign each to its respective role, beyond making some banal comments e.g. free care for poor and paying service for affluent, incorporating services of private practitioners and voluntary agencies, encouraging non-State investment etc. Who will cover the population? State sector or private sector or both together? And if together - how? No answer. It may be emphasised here that according to the prevailing system both sectors are available to all sections of populations. In any case, is it possible, in the present socio- economic reality, for the Govt. to provide universal coverage of free medical care? The clear answer is -No, this implies that the NHP knowingly offers a clearly non-implementable programme. The obvious purpose then appears to be deception. 3.2 Pari passu this medicare programme, NHP calls for a Nutrition programme 'Food of acceptable quality must be available to every person in accordance with his physical needs'. Ram Rajya- isn't it? Then comes the water supply and sanitation and environmental protection; here mercifully, no talk of univ61salisation and so, there cannot be any accusation of deception; since universalisation has not been aimed at. The question of mechanism or feasibility is ignored. Hereafter, comes universal immunisation programme and MCH services, school health programmes and occupational health services. All these are treated in as casual a manner as possible signifying the importance they occupy in the attitude of the policy makers. Finally mentioned are- health education, management information system, medical industry, health insurance, health legislation, medical research, intersectoral cooperation, population control, medical education, and few other issues. In terms of health indices, e.g. IMR, life expectancy etc., the document has set up targets to be achieved at each category for the years 1985, 1990,2000. Needless to say, the declared targets upto the current year have not been attained 4. For a people's health policy: In order to formulate a people's health policy we need to develop a clear understanding of the conceptual issues and the attending programme in the perspective of socio-economic culture reality. For this exercise, the role of politics emerges as a major factor. 4.1 Conceptual understanding:

It has already been realised in the earlier discussion that the elements of health care determine the status and future of health of, the individual and the community. That means - health is predominantly dependent on provisions or input which is traditionally considered as non-health elements. In clear languagethere cannot be any 'health for all' without food- waterhousing-sanitation for all, education for all, employment for all etc. Surely, all these elements cannot be tackled by the health administration. Broadly speaking, health policy involves all the major nonhealth departments of the govt. obviously therefore', dealing of non-health matters by the health administration is a mere idle exercise entirely useless, 'unless those matters are taken up and dealt with by the respective non- health administrations. Certainly, it cannot be the task of the policy makers of health to formulate food policy, education policy, etc. The primary question therefore, is - what are the issues the NHP should deal with? We suggest that NHP should deal with only those issues which are dependent on and related with medical intervention. These are - medical care, immunisation, drugs, medical rehabilitation, medical and health education, medical aspects of birth control, MCH, school health service, occupational health service etc. Among these issues, the major element is medical care and it is necessary to determine the role of medical care once again. A de-emphasis on medicare is not only a shut-eye towards reality but also appears to be an exercise in deception, Medicare is a universally felt-need of the people just like food and water; one cannot escape this reality. It is an independant need regardless of the standard of living and status of health care. It has already been established in the earlier discussion that the posing of antagonistic or conflicting relationship between medical care and public health is not only a distortion of scientific principles but a sort of absurdity. For those who need medicare, there is no substitute for it. Further, need of medicare, particularly its lifesaving and ameliorating functions, is an acute and urgent need which tends to override all other needs. All these considerations lead to Inescapable conclusion that an affordable minimum standard of medicare of equitable distribution ought to receive the first priority in NHP. The programme which stems from this priority will be discussed later. 4.2 Politics of medicare: Medicare is a commodity sold in the market at a price and hence, it is always inaccessible to those without adequate purchasing power. Because of its humanistic character there is always social effort to provide free medicare to the Indigent people, like giving alms to beggars. Upsurge of socialist movement and national liberation movement in the twentieth century has steadily led to a crisis of capitalist State eroding its legitimacy and consequently, in order to regain legitimacy, the concept of Welfare State emerged. The capitalist State took upon itself the task of providing welfare services to the mass of people to regain their confidence; these measures include free medicare, education, relief to the distressed, subsidy to essential commodities, etc. alongwith certain other democratic rights. It is not, however, suggested that these were offered entirely voluntarily. There was pressure of people's demands as well as an eye to fulfil its own needs. Among these measures, free medicare was a very popular measure which definitely met the need of the people to some extent and earned the blessings of the people enhancing the image of the

State and reinforcing its legitimacy. Later, tremendous development took place in medical science bringing in potent weapons to combat death and debility as well as prevention and reduction of mass morbidity and mortality from dreaded communicable diseases. As a result, demands of modern medicare (including public health) services went on increasing sharply and spreading widely. Instead of being grateful receivers of charity and benevolence, the people began to consider medicare as a matter of right. The State, however, was not prepared to meet this enhanced demand. The steadily increasing cost of modern medicare swelled the rank of seekers of free service including the middle class and upper middle class. The situation changed. On the one hand, distribution of free medicare no longer brings the blessings as it used to do at the beginning as a welfare measure. On the other hand lack of inadequacy of State medicare threatens to erode the image and legitimacy of the State since it was increasingly being viewed as a matter of right. Parallel picture prevailing in the socialist countries providing universal free medicare of equitable distribution has contributed to the sharpening of the crisis. In fact the issue of medicare attained political status and turned into a political crisis particularly in the advanced capitalist countries. The problem faced by the ruling circles is very clear. What is the point in taking so much trouble to earn a profit if one has to eventually spend it on charity? Efforts were thereafter underway to curb increasing State allocation in welfare measures. HFA is one outcome of such efforts, which seeks to curb State-spending on medicare. The move appears to be calculated one. It glorifies public health and non-medical elements of health care, identifies spending on medicare as the stumbling block in the way of achieving health care measures, calls upon the third world countries, in the name of cultural autonomy, to revive and develop cheaper indigenous system of medicare in order to replace costly western medicare and attain self-reliance. Apparently, all the ingredients of this policy framework are attractive as these are consistent with scientific principles, nationalism, anti-imperialism, and self- reliance. Then what's wrong with it? There cannot be any quarrel with increasing Stale allocation for non-medical health care measures. Who will oppose the slogans of food for all, education for all, employment for all, etc.? The point, however, is exactly opposite. Who will believe that Gal will really take effective steps to transform these slogans into reality by 2000 A.D.? Perhaps, none will. The entire basis, therefore of these populist slogans and new emphasis is false. It is just not possible for any Govt. on earth to implement these slogans in reality keeping the existing exploitative economy intact. The real purpose of HFA, therefore, ill not priority to nonmedical health care measures but deprioritisation of medicare in order to tackle the political crisis. In order to retain the Welfare-State image and to prevent erosion of legitimacy, the entire programme is embellished with populist, progressive, scientific make up. The result- the people neither get their health care nor medicare. On the basis of this analysis, people's policy is formulated. 4.3 Conceptual and political bases of a people's health policy: To repeat, a national health policy need not bother itself with non-medical determinant elements of health e.g. food-housing education-employment etc. as

enumerated earlier. These demands are the basic political demands of any society and will be achieved by political struggles without necessary intervention or meddling by the people's health movement. There is just no call for it. A people's health policy ought to decisively deal with the issues connected with medical intervention and medicare is the major element within this perimeter. With this point of departure, it is imperative to face certain legitimate questions. In a market economy governed by a welfare state, why should the State provide free medicare to the affluent? Do the poor citizens have a right to life saving medicare? If so, where from they will get it, if they are not able to purchase it from the market? Who will determine the nature and standard of State medicare service? Since the State can afford to provide free medicare, to only a part of the population, who is entitled to receive it? Answers to these questions have so long been avoided by all concerned including the political forces and these should constitute the basis of peopled health policy. The primary duty of a Welfare-State is to provide any, guarantee, free medicare to the indigent people who lack purchasing power. Until this is achieved the State has no business at all to look at the affluent. In clearer words, the free medicare service of the State ought to be reserved exclusively for the indigent people and the rest of the people should be assigned to market medicare. It is the only way to proceed towards equitable distribution and achieve universalisation. Equal distribution of State service to haves and havenots increases and perpetuates inequality and injustice, but unequal distribution favouring have nots will bring equality and justice. The choice, therefore, is clear. Will the State or NHP strive for equality or inequality, justice or injustice? 4.4 Medicare programme of a people's health policy: An outline of the programme is drawn after taking into consideration the existing medicare delivery system, financial and operational feasibility, and socio-political limitations. 4.4.1 State medicare service be exclusively reserved for meeting the need of the section of population living below the poverty line or below a pre-determined level of income. 4.4.2 The section of population living below this level of income or say, another predetermined level will have to depend solely on market medicare. 4.4.3 Inspite of or because of the above-mentioned restriction, there will be a middle section of population who are neither poor nor possess adequate purchasing power for market medicare. For them, an insurance system, as successfully operating in many capitalist and socialist countries, with co-operative entrepreneurship will be organised by State assistance, financial or otherwise. 4.4':4 All employed people who are now protected under various schemes of medicare service organised by employers including the ESI (MB) scheme will be confined within those systems. 4.4.5 Inspite of these three compartmentalised delivery systems, there will perforce be certain overlaps, particularly in emergency medical care, and in remote undedeve1eped areas of the country. In such situations non-entitled persons will be permitted to receive State medicare, but on a paying basis, not free.

It is apparent that the market medicare is open on payment to all categories of population but that is what the market means. 4.4.6 Under the compulsion of the above scheme, State medicare service will have to expand, particularly in the hitherto unreached areas, and be rearranged to meet the needs of quantitatively predictable clientele. Hence, it will then be possible to actually make workable planning with regard to medical and paramedical personnel, drugs, equipments, beds, buildings etc. which in turn will facilitate workable planning on production of human and material resources as well as deployment mechanism and consequently allocation sharing. In the same way, non-State resources will then be invested on compulsion and price and quality of market medicare will be regulated by well established laws of market. 5. Consequences of the people's health policy (PHP) and task of the people's health movement. It is not, however, suggested that this PHP is easily implementable and if launched, everything will run smoothly to the completion. In fact, the consequences will be exactly the opposite. Here, in the discussion of consequences, it is now necessary to refer to the optrepeated feature in NHP and HFA which has not been mentioned in the earlier analysis. NHP and HFA repeatedly assert that people's participation, even control, in the health care service programme is an essential condition for its success and people must somehow be persuaded or inspired to participate. It has not, however, been explained how this persuasion will come about. 'People's participation', 'people's Involvement' etc. are populist slogans used nowadays by all ruling circles to earn credibility and legitimacy but it is never realised. This is not only because of Insincerity of the rulers but also due to inherent structural barriers in the way, that is, power structure of the society. Real participation is not possible without control and control operates through power. Power is wielded in society by those who own property, possess political and administrative authority, belong to upper class and caste and, therefore, these people actually participate in social projects, in the spending of social surplus. That is why, the major share of welfare allocation of the State is eaten up by these sections of the population. In view of above, the deprived sections of the population can be expected to participate if and when they find a way to establish control over the social project. An exclusive reservation of State medicare service for them will open a way for them to strive for holding and protecting it for themselves. For the beneficiaries own narrow self-interest, they may be expected to organise and fight to establish a control over the State medicare service in order to actually enjoy its benefits and keep it that way. The others on the other hand, will not leave it unchallenged, will not give up their privileges voluntarily without a fight. Hence, a conflict will ensue. Without conflict, without winning over adversaries in a battle, people cannot earn anything concrete and they will be able to exercise control (i.e. really participate) only as a winner of the battle. May be, if will be seen afterwards that it is merely one of the battles. It may be revealed later -that unless one is able to exercise control over the State itself, it is not possible to hold on to the control over apart of the State, i.e. the medicare system. But that is another story.

ROLE OF COMSUMER MOVEMENT IN REGULATION OF PRIVATE SECTOR IN HEALTH CARE: ANIL PILGAONKAR. Note: (i) In this paper it is assumed that there is full agreement on the premise that in a society an individual's activity, which has pervasive impact on the other members of the society, needs to be regulated. Thus one has the right {freedom) to smoke (and ruin one's health) but when this smoking has pervasive impact on others, it needs to be regulated and is regulated in more and more stringent manner as is necessary. Taking the same line of argument, private sector (or for that matter any sector) in health care needs to be regulated and if the existing regulations are inadequate, then these need to be modified, strengthened, fortified and monitored. In this paper, therefore, some important areas where regulation is either absent or feeble and which need to be more stringent and/or effective are taken up. It is realised that in this attempt to raise many issues (of consumer interest), the paper could tend to become patchy and fragmented but the important consideration was to raise issues for debate. (i) The term 'consumer' in respect of Health Care must be considered in its wider perspective. Those who "avail of health care facilities must indeed be considered as consumers but those who cannot avail of these facilities are also to be viewed as "consumers. -even though semantic-wise", there could be debate on this. Thus all people become consumers of health care. (iii) As a corollary, all voluntary organisations working in the area of health care are supportive to the cause of consumers of health care, and are thus a part of consumer movement in health care. 1 The extent or the spread of private sector in health care The .private sector" In health care extends to a wider area than the common .in focus. DOCTOR-HOSPITAL-DRUG INDUSTRY arena. Below we list some major elements under this. I SERVICE (INSTITUTIONAL) i. Hospitals, nursing homes, specialized centers like obesity centers, yoga centers etc. ii. Health Insurance Companies (now many of these falls into Public Sector) II SERVICE (INDIVIDUAL) i. General Practioners, specialists from various disciplines and various schools (Allopathy, Ayurved, Homeopathy etc.) ii .Para-medics: Medical Social Workers, nurse’s physiotherapists, occupational therapists etc. III MATERIAL SUPPLY I. Drugs & Pharmaceutical companies. II. Other consumables like gauze, cotton etc.

iii. Medical equipment & instruments. iv. Wholesalers & retailers. v. Other supplies e.g. iodised salt, mosquito repellents etc. IV MEDICAL EDUCATION I. Private medical colleges. 2 Hosplta1s & Nursing Homes in Private Sector: (i) This is perhaps the least 'regulated' sector in health care. Whereas there are laid down minimum requirements to run a drug store, there are no laid down requirements for operating a nursing home. A consumer expects a minimum standard for running a nursing home and takes for granted that the 'noble' profession must have ensured that all the requirements for safe, optimal hospital service and must "be there" and often comes to grief. To illustrate the point here is a case that came to our consumer organisation. A woman was taken up for "elective" hysterectomy. On the day of the operation, the patient's relative was asked to bring a bottle of blood. After some hassles when the relative did bring the bottle blood, he was chided for not bringing a transfusion set with it as the nursing home did not have the transfusion set. The point one would like to make is 'can a hospital be allowed to function without a transfusion set?' Inadequate equipment and instrumentation is more a rule than an exception with many private nursing homes and hospitals. Doesn't this situation merit stringent regulatory measures? ii) Proliferation of nursing homes in the city of Bombay Recently, Bombay Municipal Corporation submitted to the High Court a list of some 590 private nursing homes and hospitals as the number of nursing homes registered at Bombay Municipal Corporation. (We feel there is more than this number as some of the nursing homes we know are not on this list.) However the question one would like to raise is - how many hospitals and nursing homes are optimum for the city of Bombay? After all, it must be understood that all these nursing homes on private sector) run on "patient's money." Statistical Outline of India (Tata Services Ltd. publication) 198889 informs us that in Maharashtra (as in 1986) the number of private hospitals was 1120. This would mean that more than 50% of these are in Bombay. Does this runaway growth of nursing homes in the city of Bombay not call for stringent regulation? Other questions like whether there is a mis-match between incidence of a disease and the number of beds reserved for that disease is also of relevance to the consumer. The premise that market forces would regulate the growth of nursing homes on this case) is not true simply because in health care the person who ultimately foots the bill - the end-user, the patient, is

hapless with no control over the situation with no say in the matter. He/she is presented with a 'no win' situation as enough psychological pressures are subtly built up through various means that if remedial (medical) action is not taken at the earliest, the manifestations could be very alarming and grave and then he/she is presented with a take it or leave it option. Little wonder then that the number of ICCU beds in private nursing homes (in Bombay) is vastly over-proportionate to the incidence of the disease. (It is guesstimated that there are some 350 such beds). It 'pays' to have an ICCU bed in the nursing home, since its tariffs are very much higher. iii) Good Nursing Home Practices. For the Drug Industry, there is FDA (however poorly it may function) to monitor the Industry, but there is no such monitoring to oversee nursing homes. In drug industry, through constant pressures from the people, today there is an ushering of GMP (Good Manufacturing Practices). The premise behind the Introduction of GMP is that drugs are not Just "any goods and the lives of people depend on quality of these. By the same token, shouldn't there be a written procedure of Good Nursing Home Practices or Good Clinical Practices? 3. Doctors: The dissatisfaction of patients with doctor's services would be dealt by another paper which analyses the patients' responses to the letter on the issue and so is not taken up here. However, we feel it would be worthwhile to raise two issues here (i) overprescribing and (ii) over-investigating. Over-prescribing: There are studies that show that doctor's prescription is heavily influenced by samples and gilts that the drug company gives (see Aspects of Drug Industry by Mukarram Bhagat) Over-investigating: Though there are no 'studies' as such, it is commonly conceded in the medical profession that there is rampant .cut-practice" (i.e. when a doctor 'refers' a patient to another doctor for investigation, the second doctor gives a 'cut' from the fees he/she levies on the patient to the first doctor). This has led extensive referrals for uncalled investigation. The plea here is that this conversion of doctors to 'middleman' or 'agent' role needs to be restricted, controlled and ultimately abolished. 4 Para-medics: Professional independent paramedic practioners is comparatively a recent phenomenon. There is not enough experience with this entity for us to comment. 5 Drug Industry: This entity has been in focus in voluntary "action" for quite sometime now and has been a subject of periodical regulatory measures but

despite this it has still managed to remain exploitative and unfair. There is much more of regulation needed but since this has been so extensively documented that it is felt (whilst writing this paper) that it need not be repeated here. 6 Medical Equipments & Instrumentation: With the advent of newer, enormously expensive instruments like NMR, PET etc. ill selected cases for investigation is going to be heavy burden on the patient. Already there is a feeling that more often than not, these investigations are advised keeping the "viability of the instrument in mind and if this is so, it needs to be regulated. 7 Private Medical Colleges: An inescapable facet of all private medical colleges is either the capitation fees or "premiums on reserved seats". These are so enormous that eventually they have their manifestation on the fees levied on patients. The grapevine has it that the recent private medical college in Bombay (remember, In Bombay there are four full-fledged medical colleges in public sector) has reserved seats and the under the table' premium on reserved seat is Rs.15 lakhs. If true, in the six years of studentship it would appreciate to 30 lakhs which the doctor would then try and squeeze out of his/her patients to the shortest feasible period. Shouldn't this be banned? VOLUNTARY SECTOR IN HEALTH CARE In the voluntary sector, there are hospitals, research institutions, drug production units, drug supply units, retail drug counters and all of these technically fall into the "private sector category" but operatively they run on "NOT FOR PROFIT BASIS' and so even though the size of this sector is very small, they hold tremendous restraining influence on the private sector. Annexure I gives some more details of this sector. Successes of Voluntary Action Groups In regulation of private sector. (a) Bringing about the ban on Amniocentesis and Sex-determination tests (Discriminatory Practices) (b) Banning of E.P. drugs (Hazardous Drugs) (c) Boycott of Drug Companies (Public and Professional) to enforce the companies to withdraw their irrational drug combinations after their recourse to stay orders by the court. (d) The generation of scientific, public and lobby pressures to have the essential drug list drawn

out by the State. And many more. Newer Thrusts Required If the voluntary (consumer) movement has to achieve greater successes, it would need to make substantial gains in the following: i) People’s Audit of Pharmaceutical Industry.

ii) To usher public pressure for (a) prescription audit (b) medical audit, (c) clinical audit etc. iii) To draw out a list of patient's rights and inform people about it and enable people to exercise these. iv) To work towards the goal of having an end-user of health care facility on regulating bodies.

Annexure 1 The Range of Pro-consumer Functions of Voluntary Organisations in Health Care in India Category Organisations Functions CHAI, CMAI, Sewa Rural, Providing health care, health education, Hospitals SEARCH, ARCII, Trust for fostering consumer awareness, propagating Community Health Reaching Unreached, Jamkhed Rational Therapeutics, ethics for self Projects etc. Project, Pachod Project & many regulation, Health Research & Action others FRCH, VHAI & its Researching, documenting relevant material Research & Constituents, CED, KSSP, for health care professionals & public and Documentation & MFC, ACASII, AIDAN & its taking requisite action. Education & Action constituents & many others. Producing quality essential drugs & marketing them under generic names with full unbiased Drug Production LOCOST, CMS information on a not-for profit basis. Fostering ethics in this area. Supply of essential/rational drugs on a not-forDrug Supply CDMU, LOCOST, CMS profit basis. Fostering an alternative model to whole selling. SEW A (Ahmedabad), Loksewa Retail Drug Counters Retail supply of drugs on Not-for-profit basis. Aushadhalaya. (Malshiras, Pune Dist.) N.D. All the organisations listed above and many more are constantly involved in consumer education and all of them have ethical codes for self-regulation. The list is not exhaustive. • Reproduced from Health Action Vol4,No 5, May 1991 - page 18

PRIVATE SECTOR IN HEALTH - SOME INTERNATIONAL EXPERIENCES SONYA GILL This note reviews the private health sector in the industrialised capitalist economies of Western Europe, N America & Japan (all members -'Countries of the Organisation for Economic Co-operation and Development (OECD), Paris). In all these countries, barring the USA (& Turkey), a public or national system of universal health care coverage exists. By 1987, these national systems provided over 90 percent coverage of costs for hospital care [with the exception of Netherlands (73%) and USA (43%) and ambulatory care (exceptions being Ireland (37%), Netherlands (67%) and USA (43%)) while the coverage against costs of medical goods was similarly above 90 percent [exceptions: Canada (34%), Ireland (40%), Netherlands (61%) & USA (10%)]. These figures illustrate that in almost all these countries there is a collective (public) system of paying for health care needs of individual citizens, and that the range of services provided are near comprehensive, and even include advanced

medical care. The expansion in access and coverage occured between 196075, (only the British NHS was established in 1950s) when these countries enacted legislation that -

(i) made health coverage compulsory for all citizens, (ii) defined the scope of benefits to be equally available to all or the majority, (iii) established a financing mechanism to universalize coverage, and (iv) provided for the public regulation of the delivery system.

The two main models for the public financing of health care are: a) the National Health Services (Beverige) model characterised by universal coverage, national general tax financing, and national ownership and/or control of the delivery system; and b) the social insurance (Bismarck) model, characterised by comprehensive universal coverage generally within the framework of social security, and financed by employee and individual contributions through non-profit insurance funds, and public and/or private ownership of the delivery system. It is only in the USA that risk-based private insurance, borne by individuals or employers, is the dominant method of payment. It is estimated that 37 million people, 15 percent of the population, in the USA are uninsured, underinsured and medically indigent. As universal coverage has expanded, the share of health care in the GDP of these countries has risen from an average of 4.2 percent in 1960, to 7.5 percent in 1986-87. The share of public financing has risen from 60 percent in 1960 to 78.2 percent in 1986. There has been a leveling off of public expenditure in the '80s as public coverage has been fully achieved. In these countries the private sector accounts for just 10 to 20 percent of health expenditures, whereas in the USA it is over 50 percent. And it is only in the USA that overall health cost inflation continues to outstrip the growth of its GDP, and is likely to reach 15 percent of the GDP by the year 2000. The public financing of health services does not necessarily coincide with the public provision of health care. The hospitals are State-owned in the UK (NHS hospitals) and Sweden (County-owned), but in other countries constitute a mix of public and private ownership. In Canada, W. Germany and France, the non-profit, private voluntary sector (charitable, religious, community) pre- dominates. In Japan the physician entrepreneurs run the smaller hospitals, while corporate type activities are prohibited. In USA too, the largest hospital sector is the private, non-profit one, but investor-owned, for-profit hospital systems account for 15 percent of all hospital facilities. Ambulatory care is provided mainly by independent practitioners, in solo or group practice. The majority are in a contractual relationship with the national or statutory health programme, either Individually (UK) or through an association of practitioners (W. Germany, Canada, Japan), and are reimbursed by various methods: per capita payment for all patients on a physician's list, or fee per item-of-service based on standardised fee schedules. Only in Sweden are all physicians salaried workers in the public system. Since the vast majority of citizens are compulsorily covered by the national scheme, physicians and hospitals are necessarily contracted to the scheme, and there is hardly any scope for determining their own fees or levying these private charges on the individual patient. Canada completely bans doctors contracted to the national scheme from undertaking any private practice, whereas in other countries, notably UK and Sweden, there is a system of part-time private practice. Implications of a Universal Programme for the delivery of Care. 1. The coverage is comprehensive, as both the benefits and universal access is mandated by law. 2. It is easier to allocate resources for preventive services by building these as reimbursable benefits in the programme. 3. A monopoly payment system ensures that expenditures are contained even while providing comprehensive coverage. 4. It lays down a standard rate of reimbursement for doctors and facilities. In all these systems the uniform fee schedule is established through negotiations between insurance associations, the government and the professional associations. The same, reimbursement agency is also in a position to review practice patterns (eg. Germany, Canada) without, however, impinging on professional autonomy. In fact the US system of utilisation review is thought to be much more intrusive. 5. Hospital growth and expansion can also be planned to an extent, as all these systems have moved towards the regionalization of resources. Governments, central or local, have to approve major capital expenditures and acquisition of sophisticated technology, Conclusion While greater and greater State intervention was necessary to ensure universal and comprehensive coverage In all the western capitalist societies, there Is no discussion of a universal system of health care for the post - colonial, poor, capitalist countries of Asia and Africa, It is only those that adopted a socialist path of development, such as China and Cuba, that ensure free and universal health are to all and have achieved the best health and social indicators. A handful of other countries such as Sri Lanka put together certain welfare programmes at Independence Itself, which has provided a safety net against poverty and made rapid improvements in the survival and longevity of its people. However, in the majority of the poor, capitalist countries the discussion in the '80s has been mainly on how to curtail the role and responsibilities of the State. The Primary Health Care approach has come to mean very limited and selective preventive services as the domain of the public, while curative care is to be left to the private, for- profit sector. An expanded role of the private sector paid for by individuals and private insurance schemes, imposing user charges in public facilities, curtailing the public sector's role to limited MCH and child survival services, is the health care policy being advocated. A comprehensive set of medical and health services for the people 'and protection against the financial consequences of illhealth is nowhere on the agenda.

Editor, I read with considerable interest your issue No. 169/170.1 feels that the small part of the discussion on page 3 requires some elaboration from the editorial desk. I refer to the view expressed that the 'State and local level structure does not allow for any experimentation and cynicism is so dominant that it is difficult to envisage as initiative from people. 'If the above had been made as a General Statement, it could possibly have been ignored (though even then it is, I feel too 'cynical' a view it is precisely because of such attitudes that NGOs have continued to glorify their 'islands' of excellence while major little discernible impact overall). However the singling out of Bihar and West Bengal is, to put it mildly, amazing. I do not know whether it reflects the political bias of the participant or is a broader reflection of collective viewpoint within MFC. With all its shortcomings, the experimentation with decentralisation of power structures in West Bengal is worth emulation by a numbers of states. The reference to West Bengal is hence subjective, biased ad if I may say so, motivated. One needn't be an admirer of the West Bengal Govt. to make this point. However if West Bengal is seen to be the stumbling block to community participation and not state like U.P., Haryana, Rajasthan or M.P., I feel MFC is acting as a front for partisan, sectarian and motivated interests. I am writing this more in anguish than in anger having regards for MFC activities and having interacted fruitfully with a member of MFC activists at various fora. I only hope my appreher1sions are incorrect and will be viewed objectively and in the proper spirit. Thanking You, Yours Sincerely, Dr. Amit Sen Gupta (for Delhi Science Forum). Editors Note The above said statement surely does not reflect MFC's collectives opinion; it is merely a participant member's statement. The editors request Dr. Sengupta on behalf of MFC readership to kindly elaborate on how W.Bengal's health services differ for them & say Maharashtra. or Karnataka, so that the readers will get a better idea of how the decentralisation in the W.B. has helped the health services in that state. We shall publish the same in MFC bulletin. Editor

ANNUAL MEET Dear Members and well Wishers, The Annual Meet of the MFC is scheduled to be held in Bombay on 5th and 6th September, 1991. It will be followed by Annual General Body Meeting of MFC and a Public Meeting on the theme on 7th September, 1991. THEME: PRIVATE SECTOR IN HEALTH CARE: NEED (and means) FOR REGULATION. VENUE: "VINAYALAYA" (Retreat House), Behind Holy Family School, Mahakali Road. Chakala, Andheri East, Bombay 400 093.

MEDICO FRIENDS CIRCLE BULLETIN Editorial Committee: Abhay Bang Anil Patel Binayak Sen Dhruv Mankad Dinesh Agarwal Padma Prakash Sathyamala Vimal Balsubrahmanyan Sham Ashtekar Anita Borkar (Editors) Editorial Office: C/o Abhivyakti. 41, Anandvan, Yeolekar Mala, Nashik-5. Subscription /Circulation Enquiries: Anil Pilgaonkar, 36-B Noshir Bharucha Marg, Bombay - 400 007 Subscription: Rates: Annual Life Inland (Rs) a) Individual 30 300 b) Institutional 50 500 Asia (US dollars) 6 75 Other Countries 11 125 Please add As.5 to the outstation cheques. Cheques/M.O. to be sent in favour of MFC (Address: Dr, Anant Phadke 50, LIC Colony, University Road, Pune 411 016, India.) Published by Sham Ashtekar for MFC and Printed at Impressive Impressions, Nasik. Typesetted by Lagu Enterprises, 1, Shanti Niketan, Mahatma Nagar, Nasik 400 447. Views and opinions expressed in the bulletin are those of the authors and not necessarily of the organisation.

CONTENTS Sr. No. PAPER

PAGE NO.

1 REGULATION OF PRIVATE MEDICAL SECTOR: WHY? HOW? BY ANANT R.S. PHADKE 2 REGULATING THE PRIVATE HEALTH SECTOR 5 BY RAVI DUGGAL AND SUNIL NANDRAJ 3 SIZE OF PRIVATE SECTOR IN HEALTH CARE DELIVERY 7 SYSTEM IN INDIA BY AMAR JESANI 4 EARNING IN PRIVATE GENERAL PRACTICE BY ALEX GEORGE 5 PRIVATE HEALTH EXPENDITURE BY RAVI DUG GAL 6 PRINCIPLES OF NATIONAL HEALTH POLICY BY S JANA & S DAS

7 ROLE OF CONSUMER MOVEMENT IN REGULATIONN OF PRIVATE SECTOR IN HEALTH CARE BY ANIL PILGAOKAR 8 PRIVATE SECTOR IN HEALTH: SOME INTERNATIONAL EXP'ERIENCES BY SONYA GILL

12 t4 16 20 22

MFC GENERAL BODY MEETING Members of Medico Friend Circle are hereby informed that Annual General Body Meeting will be held on September 7, 1991 at 10.00 a.m. and will continue till the deliberations are complete. Agenda: i) Statement of audited accounts ii) Other organisational matters iii) Any other matter that is put forward with the permission of the chair All the members are requested to attend and participate in the meeting. Anil Pilgaokar, Convenor, MFC. 34, B Naushir Bharucha Road, Bombay 400 007. Tel.: 386 8608. AVAILABLE NOW! Medical Education: Re-examined An anthology of paper's and articles taking a critical look at our medical education and suggesting alternatives on some aspects. Available from: Price: medico friend circle Paper back - Rs 35.00 34, B Noshir Bharucha Marg, Hardcover - Rs 100.00 BOMBAY 400 007

MEDICAL EDUCATION: RE-EXAMINED We are extremely sorry for the delay caused in publication of this fourth anthology. Unfamiliarity with new technology resulted in a major gap, necessitating 'redoing' of the bound book. This resulted in incurring higher printing and binding cost which has been borne by MFC. But this redoing also gave us the opportunity to present the book to you in a better form at a marginally increased cost of Rs.35/- (as against As.30/earlier) We regret the inconvenience caused. Yours sincerely Anil Pilgaokar Convenor, MFC


