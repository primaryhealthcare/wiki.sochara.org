---
title: "Immunity, Ethics & Control Of Leprosy"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Immunity, Ethics & Control Of Leprosy from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC242-243.pdf](http://www.mfcindia.org/mfcpdfs/MFC242-243.pdf)*

Title: Immunity, Ethics & Control Of Leprosy

Authors: Chatterjee B.R

medico friend circle bulletin 242 243

May-June, 1997

Immunity, Ethics & Control of Leprosy B.R. Chatterjee President, Indian Association of Leprologist, Leprosy Field Research Unit, Jhalda.

Since a mycobacterium, M. leprae, is associated with leprosy, it is an infectious disease. Indeed this assertion comes handy to combat superstitions that surround the cause and effect of leprosy. It is carried a bit too far however when public advertisements assert that it is one of the least infectious diseases. This statement is wholly scientifically wrong. What they mean to .convey is that leprosy does not occur easily. The scientifically correct assertion should be while getting infected is quite easy, infection does not automatically result in leprosy, the disease. And, this phenomenon, transformed into scientific jargon would imply that human ability to contain the infection and prevent development of clinical leprosy is high. This also means, mobilising of protective immunity to leprosy is the rule, and occurrence of leprosy as a disease is rather infrequent. Now since the word immunity is brought in here, we will use a few words to explain how it relates to leprosy. Immunity normally connotes resistance. However, the immune process and, the immune system is not that simplistic. Immunity can both be protective, as well as, devastatingly damaging to health. Of the various immune processes delayed type hypersensitivity, or DTH, is one such process that can do a lot of damage to host tissues.

We all know what happens to a patient of tuberculosis

with excessive DTH there is liquefaction at any site where M.tb is lodged, and in matter of weeks both the patient's lungs will be destroyed making living impossible. In the normal run, i.e., in patients not excessively hypersensitive, DTH offers temporary retreat though, in the way of cassation, sealing off of the affected part with dead tissue and cessation of blood supply making the bacilli in it dormant and the infection latent-M.tb are aerobes. This is far from being a protective response and' can break down any time.

The Immune Process In leprosy, DTH, as manifested by Type-I reactions in the so called paucibacillary leprosy (Indeterminate, tuberculoid, borderline-tuberculoid, or BT, and neural, or polyneuritic leprosy) bring with them neuritis and deformity (initially reversible), lesional exacerbation and dissemination (intensity of neuritic pain may drive a patient to commit suicide) and transient, low-grade bacillation. A BT patient that is nominally bacteriologically positive, will become positive of a grade that will be easily detectable in a skin smear. If the neuritis is not promptly treated with steroids and/or neurolysis, deformity

Background paper for the MFC Annual Theme Meet, Dec. 1997.

which is initially reversible with the subsidence of the reaction will be permanent after suffering a few such reactive episodes. Similarly, since the reactional site provides ideal sanctuaries for M. leprae to resurge, the paucibacillary (PB) disease will slowly become multibacillary (MB) — a reaction site has a low P02 and pH, both conditions favouring M.leprae multiplication. Further, with increasing bacillary load, the lipids of M.leprae that are notoriously immunosuppressive add to the suppression with incremental loss 'of immune response of the cell-mediated (CMI) variety, with the antibody component becoming hyperactive, producing large quantity of antibodies that do not protect but add an extra dimension to the disease in Erythema Nodosum Leprosum (ENL), called in leprological jargon a type-II reaction. This is an acute vasculitis resulting from deposition in small blood vessels of something like a small embolus consisting of bacillary fragments that ties up the abundant antibodies which, in turn combines with complement. This is really an immune-complex disease, is very painful and relieved only with steroids or thalidomide, and clofazimine. After healing, thick scars are left behind on the skin, and repeated attacks leave the skin in a cosmetically revulsive state and the normal texture of the skin does not return even after years of recovery.

Reaction & Deformity

disease due to various reasons. The important cause for downgrading are these reactive episodes, and a common, cause of reactions are some anti-leprosy drugs, notably dapsone. Notwithstanding whatever has been said in defence of its anti-reaction role, I am convinced this drug dapsone is the most destabilizing of all anti-leprosy drugs and almost all those begging on the streets, or at the places of pilgrimage with badly deformed hands, legs, feet and eyes are largely the result of indiscreet treatment with dapsone that they were taught to go on taking for life to prevent relapse. . The other reaction-inducing agent is concurrent crossreactive mycobacteria-these are quite frequently encountered in attempted culture to grow M.leprae, and many a scientist has gone through this experience of 'growing' M.leprae, when actually they isolated the coexisting cultivable mycobacterium that behaved initially quite fastidiously as if they were indeed non-cultivable. Immunocytes of LL-BL leprosy, while non-responding to M.leprae, are easily stimulated by non-leprosy mycobacteria inducing release of the cascade of cytokines. This leads to DTH, and the oxidative metabolic burst in the phagocyte, leading to production of toxic oxygen and nitrogen radicals, the lethal chemicals that damage the lysosomes, releasing the lytic enzymes in them that can kill both the bacilli and their phagocyte sancturies.

To Dapsone or not to Dapsone

Unless these immune-mediated dimensions are underAll aspects of the disease of leprosy, and morbidity arising stood, appreciated and always kept in mind, and guide our from it are reaction-related. These reactions are always actions, rational designing and delivery of antileprosy occurring at a micro-level. Only when they are clinically chemotherapy or immunotherapy is not possible. obvious and cause much suffering do we call them reactions. The basis of leprosy pathology is reaction, appropriately The guidelines of the Directorate General of leprosy, that can be taken to mean the WHO, to leprosy staff (of all called immune-pathogenesis of leprosy. The term immunegrades, from the State Leprosy Officer down to the field pathogenesis should imply that pathogenesis of leprosy is level supervisor and paramedical worker) strictly advise immune-mediated. the workers to continue with dapsone in full dosage (l00 In the spectral classification of leprosy, we have two mgm a day for an adult), reaction or no reaction. How I extremes—at one end lie the TT-BT types that are prone wish I could make these jet-setting leaders in leprosy to to DTH, and bacilli are but rarely encountered with ease; suffer the pain of leprous neuritis! And it is not just the at the other extreme are the so-called multi-bacillary pain. The most unacceptable aspect of it is the muscular leprosy, the borderline lepromatous and true, or polar weakness, the sensory paralysis and a continuing, lepromatous leprosy. In our country de novo lepromatous smouldering pathology in the nerves that I strongly feel are vary rarely encountered (I have seen only two so far in becomes a life-long association. my over 30 years of dealing with leprosy), and are quite deceptively normal looking unless when you take a skin smear that is highly positive. Almost all the BL- LL cases that we see had started with tuberculoid, or indeterminate disease, and have downgraded to BT or BL-LL

I cite here a small passage of an Editorial article from a group of Latin American Leprologists-"From the point of view of evolution, it is important to bear in mind that

3

peripheral neural damage and its side-effects frequently follow a course which is independent from the systemic manifestations of the basic disease. This means that in 'leprologically inactive' patients a progressive exacerbation of their neuropathies may occur ... from the practical point of view, this implies that 'dermatological discharge' does not necessarily coincide with 'neurological quiescence' of the disease" (Int. J. leprosy, 1983: 51: 576-586). Charosky, Gatti and Cardoma are but a small group from a vast pool of highly experienced leprologists from South America that base their observations on meticulous clinical and pathological observations over long years on large number of patients. I have a feeling that their observations and conclusions are ahead of time. Unfortunately these Latin Gurus of leprology are hardly ever referred to in contemporary literature, leave alone be seriously considered. Fortunately.-due to the presence of clofazimine in the regimen for multi-bacillary leprosy, Type II reactions (ENL) occur but infrequently with somewhat diminished severity. But the regimen for PB leprosy does not enjoy this privilege of clofazimine. As mentioned earlier, a large majority of our MB cases arise from Immuno-clinical downgrading of PB leprosy, and it should be quite obvious that these patients should also receive the reaction sparing benefit of clofazimine. I now hear that the WHO is looking for a suitable group/institution to initiate a study on the role of clofazimine in type I (DTH) reaction in PB leprosy. It was in a WHO-sponsored DGHS organised workshop at Jamshedpur in 1983 that this author showed the value of clofazimine in preventing type I reactions if it was used as a routine. Clofazimine was recommended as a preventive of type I reaction, not as a cure after letting it develop. Unfortunately, when the type I reactions do set in, clofazimine alone usually does not help; it has to be supplemented with corticosteroids for a suitable duration, .i.e., until clofazimine asserts itself-as its effect is slow in coming. I have been treating hundreds of cases at our small pay clinic essentially without DDS; it has to be mentioned that the cases that gravitate to our clinic are mostly MDT (WHO) failures. I do not want to invite trouble of a type I reaction-I use clofazimine as a compulsory component of PB treatment. Against all the incessant publicity of the WHO regimen's virtue, I cite the sane voice of a late veteran, Dr. Stanley Browne, in the context of ENL reaction of MB leprosy— "While stopping dapsone may not always result in rapid improvement in ENL, it is incontestable that resumption

of dapsone therapy in such patients, even in minute doses, will often precipitate a recurrence of the signs and symptoms of ENL" (Int. J. Leprosy, 1967: 35: 395-403). This was 30 years ago, pointing an accusing finger at DDS on causation of reactions. Until the 1960s, we were somewhat helpless, as DDS was the only drug available for leprosy. That does not apply now, and there is no need for the whole programme to be straitjacketed into just one regimen proposed by the WHO after very limited hospital-based studies. DDS can be easily substituted with clofazimine in PB leprosy, and temporarily or permanently withdrawn from the MB regimen as there are two other drugs in the regimen. The whip, reaction or no reaction, DDS must be continued in full dosage, might as well mean: deformity or no deformity, DDS must be continued. Now, deformity is often a very deceptive phenomenon in leprosy. It may, or need not be a spectacular development, meaning, developing with obvious visual impact. One often finds the telltale wasting of the hypothenar or thenar eminences, or the grooving of the dorsum of the hand due to the slow paralysis of the interossei. Paradoxically, it is the acute onset deformities that are more easily reversed and corrected, and there is very little one can do to reverse the deformities/disabilities that are late and slow in coming. However, in both situations, withdrawal of DDS, introduction of clofazimine in the PB regimen, or raising its dose in MB regimen, with a short course of steroids has almost always a salutory effect on checking the progression of deformity/neuritis, and its slow reversal, unless it has been more than one year in developing. At the Deoghar district MDT project, this author served as a consultant, which position he resigned after two years when the leprosy directorate objected to his modifying the regimen whenever called for. One of the numbers of consultants that followed me objected to this modified regine—by then all the MDs, superviors & PMWs were convinced of the usefulness of withdrawing DDS in reactive or nonresponding patients. When the concerned NMS (non-medical supervisor) explained how reactions could be rapidly resolved with DDS withdrawal, the consultant advised introduction of steroids without suspension of DDS. The NMS said something that silenced the said consultant—Sir, your advice is like bandaging a person after thoroughly bashing him up. Needless to say, all the staff in Deoghar district now

follow this modification-no 'expert' could change their conviction. So where does ethics stand here? Be ethical and obedient, or change course under an expert guide? I have always questioned the need for consultants if consultants were simply to follow a set pattern for all situations. Then you do not need a consultant, you need a manager!

Therapeutic Intervention in Children We had followed the clinical evolution (as relevant to leprosy) of hundreds of children in our Jhalda study areas. Lesions of leprosy developing in them were recorded, but we never put them on treatment but they were put on observation with physical check-ups every 3 months. We never even told their parents about their leprosy. Every child resolved his/her lesion spontaneously. Even if a stray child was diagnosed by the leprosy Mission staff who run the leprosy control unit in Jhalda, we would counsel the parents not to give the dapsone, Because of our very frequent visits, and our generally taking care of their minor medical needs also, the parents cooperated, and neither they, nor we had any occasion to regret our intervention. The rationale is simple. Children in these areas almost universally develop self-healing minor lesions, and this inconsequential early lesion is like an immunisation. Why intervene with treatment and interrupt this immune process? Medical ethics dicates any case diagnosed as leprosy must be put on treatment. Rationality dicatates that the child patient (one can hardly call the child a patient, that small spot is a part of living) be allowed to evolve with his early and limited leprosy under the watchful eyes of the health worker, ready to intervene if there is any sign on non-containment. Which is ethics: compulsory treatment, or non-intervention under adequate surveillance?

Elimination of Leprosy by 2001 AD? Under the pressure of the WHO, and the World Bank, the Government is now frankly resorting to grossly unethical, if not illegal practices. They have to meet the deadline to show elimination of leprosy, meaning reaching a prevalence of one case or less per 10,000 populations by the year 2001 AD. The World Bank has advanced a loan of over 3 billions of rupees on this assertion. How do they propose to achieve this miracle? Prevalence is being

projected as a better index of success than incidence, or crude new case detection rates (NCDR)! And how is this reduction of prevalence, and of course NCDR, which currently, even by official estimate's is a staggering 450,000 cases annually, or around 4.5 per 10,000 population, is to be achieved? Give a one day regimen to PB cases, and a 28 days all-bactericidal regimen to all known MB cases and declare them cured, and remove them from the register! Never bother to look for what happens to them, the bacilli they have been harboring, or the disease they have been suffering. Has any infectious disease been controlled/eradicated by chemotherapy alone? Now, is that what you call ETHICS? Don't do any survey to' detect cases, don't do any absentees retrieval; or any post-MDT surveillance. Because they all have been cured! About the deformed, the ulcerated, the economically and socially displaced, that is not the job of the leprosy directorate to bother about any way! Normally, the NGO working in leprosy have been catering to the needs of about 15% of the patients. By, and after 2001, they are going to have to shoulder the whole burden of the problem, because after 2001, apparently leprosy will have been eliminated-a few straggling cases that may have escaped the dragnet will be treated in an integrated set up. Who are they deceiving, these International experts? The count down has begun! By 2001, they are sure to show less than 100,000 cases in the country. Any group of leprologists/epidemiologists that have looked at the situation with a dispassionate view in any area undergoing MDT have come up with a 3-4 folds rate of prevalence over that shown in government returns. What we do? Pray or fight? There are other areas, notably the combination of drugs in the MDT regimens that we follow, where the WHO is not ready to concede that mistakes have been made in their undue haste to 'eradicate' leprosy. I propose to write another piece highlighting these inconsistencies, and suggesting alternatives.

ANNOUNCEMENT

Medico Friend Circle Annual Theme Meet Theme

:

Resurgence of Infectious Diseases

Date

:

December 27-29, 1997·

Venue

:

Sevagram, Wardha

The topic will be discussed under the following headings: 1.

International perspectives.

2.

National perspectives

3.

Case studies of local responses

4.

Public Health system response

The diseases focussed on are: 1. Malaria, 2. Dengue, 3. Plague, 4. Kala-azar, 5. Cholera, 6. HIV, 7. TB We would invite papers on broad perspectives and case studies of local responses to individual problems. Kindly send papers to:

Dr. Anand Zachariah, Medicine Unit I, CMCH, Vellore, Tamil Nadu 632 004.

Background Papers 1. Resurgence of Malaria —A case study 2.

Re-emergence of Kala-azar and Indian Society —A Case study

Ravi Narayan

Prabir Chatterjee

3. Dengue haemorrhagic fever in Delhi

Yogesh Jain & Sathyamala

4. *Drug resistance and irrational therapy

Delhi Group

5.

Anti-malarial policy system and resurgence of infectious diseases

J.P. Mulliyil and Madhukar Pai

7. *Resurgence of TB

Thelma Narayan

8.

P. Mohan Rao

"Of Cholera and Post-Modern World" EPW August 22, 1992

9. HIVIAIDS

Anand Zachariah

10. Contextualising Plague-A reconstruction and an analysis EPW, November 19, 1997 P 2981-2989.

Imrana Qadeer.

11. Epidemiological Overview of Infectious diseases, resurgence in India

Pankaj Mehta, Manipal Hospital, Bangalore

12. Rethinking Public Health : Food, hunger and mortality decline in Indian history (Presentation at School l.1f Social sciences, JNU)

Sheila Zurbrigg

13. Resurgence and Children

Sanjeev Lewin, St. John's Med. College, Bangalore.

14. *Market economy and infectious disease resurgence

Requests sent to Ghanshyam Shah) CSCM,

Economic impact of resurgence of infectious diseases

Rama Baru) JNU and Abhay Shukla, Pune

(If the above do not agree, to plan modification of "Communicable Diseases: Costs and expenditure", RJH Vol. 2 No.1 Jan-Mar 1996) 15. Urbanisation Agricultural development Ecological changes and resurgence of Infectious Disease 16. Community participation in vector Borne Disease Control: Facts and Fancies, Ann. Soc. Belg. Med. Trop. 1991, 71 (Suppl-I) 233-242, 17. Clinical Re-appraisal series

Dr. Vanaja, Bangalore

Dr. P.K. Das Vector control Res. Centre, Pondicherry.

C. Sathyamala and Yogesh Jain

Articles of general critique 1. From Philanthropy to human Rights. 2.

Amar Jesani

Social Medicine for Holstic Health. An alternative response to present crisis. Rudolf C. Heredia. EPW Dec. 1, 1990. *Not yet confirmed

Resurgent Tuberculosis in New York City: Human Immunodeficiency Virus, Homelessness, and the Decline of Tuberculosis Control Programs Karen Brudney and Jay Dobkin*

Introduction

Methods

Reported tuberculosis (TB) cases in New York City have been increasing since 1979 after decades of steady decline. The epidemic of infection with the human immunodeficiency virus (HIV) is of tern blamed for this resurgence. Although a substantial part of the increase may well be due to concomitant HIV infection, significant social, economic, and historical factors contributed to increasing tuberculosis rates several years before the full force of HI V infection was felt. The growth of homeless ness among urban drug abusers in the 1980s paralleled the spread of HIV infection, greatly complicating tuberculosis treatment and probably promoting further spread of this infection. In this report we document the enormous treatment failure rate caused by noncompliance among patients with tuberculosis and at high risk for HIV infection. We trace the decline in TB control programs that materially contributed to the failure rate, and we suggest some new (and old) solutions.

All inpatients at Harlem Hospital Centre with suspected or confirmed TB were evaluated by one of us. Only patients with culture-confirmed Mycobacterium tuberculosis were included in this analysis.

Central Harlem has been the area in New York City with the highest rate of TB since before 1960. It has seen a more dramatic increase in TB than any other area in the city during the past decade (table 1). It has also been 'an area of unemployment, poverty, and high rates of drug and alcohol abuse and the acquired immune deficiency syndrome (AIDS). During the past 10 to 15 years, the scourge of homelessness has been particularly brutal. in Harlem, with streets' of abandoned buildings and empty lots turning certain areas into "no-man's land", and two massive shelters for the homeless earning notoriety as breeding grounds for crime and the spread of infectious diseases. Because of the concurrent increase in AIDS and tuberculosis, we prospectively studied all patients with TB admitted to Harlem Hospital, a 700-bed public hospital, from January 1, 1988 to September 30, 1988. Our purpose was to ascertain and document factors that might predict loss to follow-up, as well as to determine a more exact rate of HIV infection among those with TB.

The detailed interview included information on the patient's previous tuberculosis history, current housing situation (what type of dwelling it was and who paid for it, current address, and whether there was heat or hot water) employment status, alcoholism, drug use, sexual partners, and transfusion history. Charts of patients, whose diagnosis was made after discharge or death, when a specimen whose smear had been negative subsequently grew M. tuberculosis, were reviewed. All patients were encouraged to undergo HIV testing, which was performed, after informed, consent, by the standard ELISA method with western blot confirmation. Patients with acknowledged HIV risk factors who refused HIV testing were deemed HIV positive if severe oral candidiasis was documented on examination by one of the authors. Each patient was given an appointment to the Harlem Hospital chest clinic at discharge. Patients with symptomatic HIV disease were also given an appointment to the infectious disease clinic. At 4-month intervals, the records of both clinics were searched to determine follow up frequency. All patients who failed to keep clinic appointment were entered into the hospital's computer system to determine whether they had come to any other clinic or had been readmitted to the hospital. At the end of 8 months and again at 15 months, a list of all patients lost to follow-up was compared with the New York City Department of Health Bureau of Tuberculosis computerized registry to determine whether patients' had been admitted to other hospitals, entered treatment in another chest clinic, or died.

Background paper for the MFC Annual Theme Meet, December, 1997 * Reprinted from the Journal of Public Health Policy, Winter 1992, p

435—450

Table I

Sexual Partner: Patient who had heterosexual contact with a

Tuberculosis Rates 1969-1989*

partner in an HIV risk group. Crack abuser: Patient who denied all HIV risk but admitted to smoking "crack" cocaine.

Year

Central Harlem

1969

121.2

1970

135.0

1971

131.7

1972

123.0

New York City 36.4 32.8 32.6 28.8 26.6

1973

103.3

1974

105.5

1975

105.5

1976

75.4

1977

64.2

1978

52.2

25.6 27.2 27.3 21.1

United States 19.37 18.22 17.07 15.79 14.77 14.25 15.95# 14.96 13.93

17.2

13.08

20.1

.12.6

Homeless: Patient who lived in the streets or one of the shelters for the homeless. Unstably housed: Patient who had lived in rooming house for less than 6 months, stayed with someone but paid no rent, or lived in a drug treatment centre and had no plans for future housing. Unemployed: Patient who had not worked for 1 year or more. Sporadically employed: Patient who had worked occasionally during the preceding year. Employed: Patient who had worked until developing symptoms of TB, or elderly patient employed for more than 20 years and retired at the time of TB diagnosis. Alcoholism: Daily consumption of more than three beers, one bottle of wine, or 1/2 pint of hard liquor or a history of alcohol-related illness (upper gastrointestinal bleeding, alcohol-related seizures, pancreatitis, or cirrhosis).

1979

50.9

19.9

12.2

1980

78.6

22.4

11.9

1981

79.9

22.5

11.0

1982

104.0

23.4

10.2

1983

109.0

23.0

9.4

AIDS-related complex (ARC): Patient with HIV-related symptoms not meeting CDC, AIDS criteria.

1984

90.7

26.0

9.3

Results

1985

110.9

31.4

9.4

1986

130.4

31.1

9.4

1987

134.9

32.8

1988

158.9

36.0

1989

169.2

9.1

9.5

* New cases per 100,000 population. # National reporting criteria changed effective 1975.

Definitions Intravenous drug abuser (IVDA): Patient who used drugs intravenously at anytime in his or her life. Homosexual: Men who have had sex with men.

AIDS: Patients meeting Centres for Disease Control (CDC) criteria for AIDS.

Tuberculosis was diagnosed in 224 patients from January 1, 1988 through September 30, 1988: The group was predominantly male (79%), with high rates of alcoholism (53%), homelessness or unstable housing (68%), and unemployment (82%) (Table 2). Of 193 patients on whom the information was available, 50 (26%) had been previously treated for TB, and nearly all of them admitted that they had never completed treatment. Over two-thirds of the patients reported HIV risk factors, and 80% of those tested were positive for HIV antibodies (table 3). Among crack abusers 49% were women compared with only 21% of the total study population. Of six crack abusers who denied risk factors, three were HIV positive. Of the 66 tuberculosis patients without HIV risk, nine of 24 who consented to testing (38%) had positive HIV antibody tests.

Table 2 discharged patients, 99 never returned for outpatient followup or renewal of medication, and an additional 49 failed to complete 3 months of treatment. A total of 19 patients (11%) were cured, died of other causes while on treatment, or remained on therapy at the end of the study period.

Demographic Features of 224 Consecutive Tuberculosis Inpatients Feature

%

No.

Male

79

177/224,

Alcoholic

53

103/196*

Table 4

Homeless

45

85/189*

Unstably housed

23

43/189*

Compliance and Outcome of 178 Patients Discharged on Tuberculosis Treatment

Stably housed

32

61/189*

Unemployed

82

171/208*

AIDS/ARC

40

89/224

No.

(% )

Cured

7

(4)

Age, mean years ± SD: 42.5±12.4

In treatment

10

(6)

*These denominators include only those patients on whom the information was obtained and 'are therefore less than the total 224.

Died of AIDS

2

(1)

Table 3. Tuberculosis Patients No.

(%)

Total

19

(11)

99

(56)

Noncompliant

HIV Risk Factors and Seroprevalence in 224 Factor

Compliant

No. HIV

(%)

Infected

No follow-up treatment Lost to follow-up <3 months treatment

49

(28)

IVDA*

124

(55)

88/97

(90)

>3 months treatment

11

(16)

Homosexual

10

(5)

9/10

(90)

Total

159

(89)

(1)

3/3

(100)

Sexual Partner 3 Crack abuser#

21

(9)

3/6

(50)

Denies risk

66

(30)

9/24

(38)

Total

224

(l00)

112**/140 (80)

*Includes two homosexual IVDA. # Denies all HIV risks but admits smoking crack cocaine.

** Includes six patients with HIV risk factors who declined HIV antibody testing but were deemed HIV positive by diagnosis of oral candidiasis.

A total of 48 patients (21 %) had extra pulmonary disease, of whom 27 had pulmonary tuberculosis as well, and 21 had only extra pulmonary disease. Of the 203 patients whose sputum grew M. tuberculosis, 104 had at least one positive smear for acid-fast bacilli (AFB), and 99 had only negative smears. A total of 178 patients were discharged, and 46 died during hospitalization. Of those discharged 89% failed to complete treatment (table 4). Of the 178

Within 12 months of discharge, 48 of 178 patients (27%) were readmitted with confirmed active tuberculosis at least once (table 5). Almost all of those discharged were again lost to follow-up, with 20% admitted a third time as of April 1989. In a multivariate logistic regression model, noncompliance was significantly associated with homelessness, alcoholism, and the absence of AIDS or ARC (table 6). Women were somewhat more likely to be noncompliant than men (97 versus 87%), and crack users were totally noncompliant. Female gender was borderline in significance as a factor contributing to noncompliance in the regression model. Noncompliance among patients with asymptomatic HIV infection was the same as among patients who were uninfected with HIV. Non-compliance among patients with ARC or AIDS, however, was significantly lower than among those without ARC or AIDS (75% [42/56] versus 96% [117/122]; x2= 15.5; p = 0.0001). Among the patients with AIDS or ARC who were lost to follow-up, 4 of 40, of 85% of the total, were homeless.

Table 5 Outcome of 48 patients Lost to Follow-up and Readmitted with Active Tuberculosis* No.

Outcome Died hospital

7

Remain in hospital

1

Discharged

40

Again lost to follow-up

35 #

Placed in TB homeless shelter

1

Follow-up < 30 days

4

*Culture positive #Of these patients eight were admitted a third time with active TB.

Table 6 Factors Associated with Noncompliance* Odds Ratio (85% CI)

p Value

0.08 (0.02-0.34)

<0.001

Homeless

2.61 (1.34-5.08)

<0.005

Alcoholic

4.57 (1.07-19.50)

<0.05

Male sex

0.12 (0.01-1.15)

0.06

Factor AIDS or ARC

*By multivariate logistic regression.

Historical perspective

effective outpatient program that would have $18 million/year to spend by 1973, given the projected closure of 500 beds by that time. The task force outlined the requirements of such a program; Clinic hours should be adjusted to the needs of the patient. Include trained residents of poverty areas in clinic and home care staffs. Provide domiciliary care and chronic disease care facilities and appropriate living quarters for TB patients who need them. Integrate the care of TB drug addicts and TB alcoholics in the developing community programs for addiction and alcohol control. Ten years after the Lindsay Task Force report was issued, amidst New York City's fiscal crisis, the nearly 1,000 designated TB beds were virtually gone, and the private sector now diagnosed more than one-half of all new TB cases, cutting the city's inpatient expenses even further (3). The combined city and state expenditures for the outpatient activities so strongly recommended 10 years earlier were less than $2 million. When the New York City TB rate increased in 1979 for the first time in decades, another task force was appointed, this time by the Council of Lung Associations of New York. In its report in 1980(4), this task force alleged: It must be strongly suspected that the increase in newly reported cases in New York City is in part the result of fiscal neglect of the TB problem in the State's largest city. The resurgence of the disease, a bitter reversal of the expected trend, is related to a failure of both health authorities and government at all levels to muster a public health program ....... At federal, state and local levels, public health funds allocated to TB are inadequate, in some instances so grievously inadequate as nearly to amount to dereliction and default on legal mandates.

In 1968, a special task force appointed by then Mayor John Lindsay published a comprehensive report detailing the strengths, weaknesses, and future priorities of the New York City TB Program. At that time, $40 million was being spent annually on TB in New York City in 21 health department district chest clinics, seven "combined clinics" operated jointly by the Department of Health and the Department of Hospitals, over 1,000 designated TB beds in hospitals throughout the city, and a small number of beds for New York City patients in New York State operated TB hospitals (2). Almost all the TB beds in New York City were in municipal hospitals; with the voluntary hospitals referring to these facilities patients with or suspected of having TB. The average cost per hospital day was $ 97:93 per patient on July 1, 1968, and a patient with TB had a average length of stay over 100 days (2).

New York City's fiscal crisis in the mid-1970s led to a drastic cut in appropriations for all public health programs, TB included. New York State progressively cut back its contract, which had accounted for 50% of the support for New York City's TB control activities, terminating it entirely as of September 30, 1979 (5). Federal support in the form of public Health Service monies decreased 80%, from a peak of $ 1.4 million in 1974 to $ 283,000 in 1980 (4).

The task force recommended earlier discharge, the elimination of 100 TB beds annually, and an expanded

In 1978, between $23 and $25 million was spent on TB in New York City, well below the $ 40 million being spent

annually between 1968 and 1973, and taking inflation into account, the reduction was far greater (4). Inpatient costs now comprised 93% of the total, compared with 80% in 1971. The dramatic decrease in beds and inpatient days had been accomplished, but outpatient expenditures not only did not increase correspondingly, they were in fact cut (4,5). In 1978, $1,630,000 was spent on TB "public health control" services, including all the TB activities of the New York City Department of Health, far below what had been spent, given the cut in inpatient costs (4, 5). (Calculating annual costs is impossible as the New York City Department of Health did not make budgetary appropriations programmatically until quite recently.) None of the outpatient services recommended by the task force 10 years earlier were being offered. The number of health department chest clinics had been cut from 22 to 9 (5). Neither public health nurse home visits nor health aide home visits were taking place since the mandated staff increase had not occurred. The number of contacts identified per TB case had dropped, and drug treatment programs to which patients reported daily for their methadone had received no impetus to screen or treat patients for TB despite the fact that the increased incidence of TB among intravenous drug users was known long before the appearance of AIDS (6).

The incidence of TB in New York City continued to increase during the next 10 years (table 1), with only minor additions in allocations to the public health control aspects of the problem (7-9). The federal government funded a new pilot project beginning in 1980, the Supervised Treatment Program (STP), in response to the increasing number of TB patients failing to complete treatment. A group of patients was identified who were clearly at high risk for treatment failure, including those who repeatedly failed clinic appointments, alcoholics, and patients with psychiatric problems that impinged on their ability to take their TB medication. The program mandated daily visits to the homes of patients who met these criteria, with direct observation of the patient ingesting his or her medication, and had an excellent success rate, with 90 to 95% of its patients completing therapy, cured (7). The federal government failed to increase funding to enable expansion beyond the initial pilot project level, and neither the state nor the city contributed significantly to enlarge its scope in the intervening years (l). By 1989, only five or six workers were assigned to this program, which therefore include

a maximum of 40 to 50 patients annually from the entire city, a fraction of those eligible. Furthermore, STP did not include the homeless, one of the criteria for enrollment being R permanent residence. As the homeless population increased during the 1980s, with crowding and unsanitary conditions in the shelters where masses of them were housed, the classic historical conditions for the spread of TB were recreated. Given this background, the advent of AIDS and the rapid spread of HIV among intravenous drug abusers, large numbers of whom inhabit the shelters, created a potentially explosive situation.

Discussion Compliance with medication is a universal problem among patients with asymptomatic and/or chronic diseases, long ago noted among hypertensive individuals (10). Once therapy has been initiated, tuberculosis usually becomes both an asymptomatic and a chronic disease (11, 12). Although the recommended duration of tuberculosis treatment in the United States was reduced to 9 months more than a decade ago, and to 6 months more recently, noncompliance has remained the major obstacle to eliminating what is essentially a curable disease (1315). Non compliance with medication in general does not correlate with education level, socioeconomic status, age, sex, or marital status (13, 16). Alcoholics, drug users, and the homeless have a particularly high rate of noncompliance and are likely to fail ambulatory treatment (17-20). Although it has been shown that a patient's beliefs about his or her illness, that is, the cause, the seriousness, and the likelihood that a prescribed treatment will help, can influence compliance (10); the relative importance of the illness to the patient, particularly when asymptomatic, depends on the stability of other factors in the patient's life. To a patient dependent on alcohol or drugs or unable to assume continuity of shelter, the importance of taking a pill or keeping a clinic appointment diminishes drastically, Housing status was clearly a very important predictor of noncompliance in our study, with patients who were frankly homeless and patients who floated from one rooming house to the next doing equally poorly, significantly worse than patients with stable housing. The patient population of this report reflects the increasingly publicized inner city housing crisis that has grown

unchecked for the past decade, as well as the chronically prevalent problems of alcoholism and drug use. Although central Harlem may be an extreme case, these problems are by no means unique to this community. Tuberculosis is a growing problem in impoverished populations, with concomitantly higher noncompliance rates in the other boroughs of New York City, as well as in Washington, D.C., Boston, and San Francisco (17-19, 21).

compromised hosts, such as diabetic or lymphoma patients, it is also compatible with primary infection or reinfections. One prospective study evaluating tuberculosis risk in HIVinfected methadone maintenance patients found that seven of eight active cases occurred in patients known to be purified protein derivative (PPD) positive (32), but no data were provided on the housing status or likelihood of reinfections or primary infection in the study population.

Homelessness has become the blight of urban life in the United States, and the publicly funded solution, mass ,The report by McAdam and colleagues of a 42.8% shelters, may have created more problems than it has tuberculosis infection rate in the clinic of one New York solved. Despite official denials by the agency that runs City homeless shelter points to the likelihood of spread of them of the existence of tuberculosis in the New York City tuberculosis within this shelter with its high HIV risk shelters, there is no doubt that the disease is well population, particularly since length of shelter residence established there. In a recent study, 1,853 homeless men there was independently associated with both infection attending a shelter-based clinic in New York were and active disease (21). Point-source spread of tubercuscreened for tuberculosis. The overall infection rate was losis in another homeless shelter has been documented by 42.8%; with 100 cases of active tuberculosis (6% of those phage typing the organisms (22), and is likely to occur in screened) (22). In a survey of all the shelters in the New similar settings. A 19% prevalence of Isoniazid (INZ) York City shelter system, 3% of 810 residents actually resistant tuberculosis among homeless patients recently admitted that they had tuberculosis and were in treatment reported from a New York City hospital raises the or supposed to be in treatment (23). ominous prospect of substantially greater difficulty in treating this already refractory group (33). The connection between HIV infection and tuberculosis was first noted among the newly arrived Haitain immigrant The resurgence of TB in the AIDS era is surrounded by population in the United States and subsequently described ironies. Increasingly potent anti-tuberculosis agents are in urban IVDAs (24-28). Given the high background rate powerless to overcome massive noncompliance. Tubercuof tuberculosis infection in both groups and the losis, among all the serious complications of AIDS, stands immunosuppressive action of HIV, it was predictable that out simultaneously as both the most curable and the most active TB rates would be high. contagious to the HIV negative population. Regaining Although it has been assumed that the rising rate of tuberculosis in New York City largely reflects co-infection with HIV and that active tuberculosis in HIV infected patients represents reactivation of old infection, a substantial amount of primary tuberculosis may also exist. This seems increasingly likely as more individuals with inadequately treated tuberculosis are crowded together with highly susceptible HIV-infected homeless people. The alveolar macrophage's ability to inhibit intracellular multiplication of the bacillus when first infected depends on its activation by immunologically primed CD4lymphocytes (29), and patients who are immuno-compromised are far more likely to progress from primary infection to active disease (30). The radiologic picture of pulmonary tuberculosis in AIDS patients is atypical of reactivation disease since cavitations is less common and middle or lower lobe infiltration more common (31). Although this pattern parallels the appearance of reactivation in other

control of epidemic tuberculosis will be difficult and will require effective approaches to hardcore issues also common to the AIDS epidemic: poverty, homelessness, and substance abuse. The AIDS epidemic has created severe financial stress on the health care system in many communities. It would be tragic mistake to divert vital resources to AIDS activities from essential public health programs like tuberculosis control. In combating resurgent tuberculosis in New York City, long abandoned strategies will need to be reinstituted for homeless and non-compliant patients: prolonged initial hospitalization, residential TB treatment facilities, and aggressive community-based supervision. Strong positive incentives will have to be offered, both because they have demonstrated efficacy among noncompliant substanceabusing patients in the past, and because, however costly, they will ultimately be less costly than the

multiple hospitalizations documented in our study population (19, 34, 35). Mandatory confinement, erroneously promoted by some to control AIDS, may in fact be needed to effectively treat some recalcitrant TB patients. As our data demonstrate, noncompliance with treatment for tuberculosis is both massive and predictable. Identification of patients likely to be noncompliant during initial hospitalization is possible and should be coupled with an aggressive supervised treatment program. Unfortunately, the needed resources and, more importantly, the needed commitment for such an effort have yet to appear.

14.

Addington, W.W. 'Patient Compliance: The Most Serious Remaining Problem in Control of Tuberculosis in the U.S.," Clin. Chest Med. 76 (1985) : (Suppl: 741-43)

15.

Reichman,

144(1991): 745-49.

2.

New York Lung Association. Report of the Task Force on Tuberculosis in New York City, 1968. New York, 1968.

3.

New York Lung Association, Tuberculosis in New York City 1977. New York, 1978.

4.

(Ref. is missing)

5.

New York Lung Association. Tuberculosis in New York City 1978. New York, 1979.

6.

7.

Reichman, L.B., Felton, C.P., and Edsall, J.R. "Drug Dependence, a Possible New Risk Factor for Tuberculosis Disease," Arch. Intern. Med. 319 (1979); 337-39. New York Lung Association. Tuberculosis in New York City

8.

New York Lung Association. Tuberculosis in New York City

9.

New York Lung Association. Tuberculosis in New York City 1984-1985, New York, 1987.

10.

Eraker, S.A, Kirscht, J.P., and Becker, M.H. "Understanding and Improving Patient Compliance," MMWR 36(1987): (Suppl. 1:1-15).

11.

Fox, W. "Compliance of Patients and Physicians: Experience and Lessons from Tuberculosis. I," Br. Med. J. 287 (1983): 33-37.

12.

Fox, W. "Compliance of Patients and Physicians: Experience and Lessons from Tuberculosis. II," Br. Med. J. 287 (1983): 101-5.

13.

Sbarbaro, J.A "Public Health Aspects of Tuberculosis: Supervision of Therapy," Clin. Chest Med. 1(1985): 25363.

Nations,"

17.

Centers for Disease Control. "Tuberculosis Control among Homeless Populations," MMWR 36(1987): 257-60.

18.

Slutkin, G. "Management of Tuberculosis in Urban Homeless Indigents," Public Health Rep. 101 (1986): 48185. Yeager, H., and Medinger, A. "Tuberculosis Long-term Care Beds. Have We Thrown Out the Baby with the Bathwater?" Chest 90 (1986): 752-54.

19.

20.

Dudley, D. "Why Patients Don't Take Pills," Chest 76(1979): (Suppl: 744-79).

21.

McAdam, J.M., Brickner, P.W., Scharer, L.L., Crocco, J.A, and Duff, A.E. "The Spectrum of Tuberculosis in a New York City Men's Shelter Clinic (1982-1988)," Chest 97 (1990): 798-805.

22.

Nardell, E., McInnis, B., Thomas, B., and Weidhhas, S. "Exogenous Re-infection with Tuberculosis in a Shelter for the Homeless," N. Engl. J. Med. 315(1986): 1570-75.

23.

Struening, E. "A Study of Residents of the New York City's Shelter System." Report submitted under contract to the New York City Department of Mental Health, Mental Retardation and Alcoholism Services, June 1986. Revised April 1987.

24.

Pape, J.W., Liautaud, B. Thomas, F., et al. "Characteristics of the Acquired Immunodeficiency Syndrome (AIDS) in Haiti," N. Engl. J. Med. 309 (1983: 945-50.

25.

Pitchenik, AE. Cole, C., Russell, B.W., Fischl. M.A. Spira, T.J., and Snider, D.E. "Tuberculosis, Atypical Mycobacteriosis and the Acquired Immunodeficiency Syndrome among Haitian and non-Haitian Patients in South Florida," Ann. Intern. Med. 101(1984): 641-45.

26.

Sunderam, G., McDonald, R.J., Maniatis, T., Oleske, J., Kapila, R., and Reichman, L.B. "Tuberculosis as a Manifestation of the Acquired Immunodeficiency Syndrome (AIDS)," JAMA, 256(1986): 362-66.

1982. New York, 1983. 1983. New York, 1984.

Developed

Hulka, B.S. Cassel, J.C. Kupper, L.L., and Burdette, J.A "Communication, Compliance and Concordance between Physicians and Patients' with Prescribed Medications," Am. J. Public Health 66 (1976): 847-53

New York City Department of Health. Tuberculosis in New York City 1988. New York, 1990.

in

16.

REFERENCES 1.

'Compliance

Tuberculosis 68 (1987): (Suppl: 25-9). .

Acknowledgments; The writers thank Ms. Julia Farrell for expert assistance in preparation of the manuscript. This article is reprinted, with permission by the American Lung Association, from the American Review of Respiratory Diseases

L.

27.' Pitchenik AE., Burr, J., Suarez, M., Fertel, D., Gonzalez. G., and Moas C., Related Disease among 71 Consecutive Patients in whom Tuberculosis was Diagnosed," Am Rev. Respir. Dis. 135 (1987): 875-79. 28.

Hewlett, D., Duncanson, F.P., Jagadha, v., Lieberman, J., Lenox, T.H." and Wormser, G.P. "Lymphadenopathy in an Inner-city Population Consisting Principally of Intravenous Drug Abusers with Suspected Acquired Immunodeficiency Syndrome," Am. Rev. Respir. Dis 137 (1988): 1275-79.

29.

30.

Sathe, S., and Reichman, L.B. "Mycobacterial Disease in patients Infected with the Human Immunodeficiency Virus," Clin. Chest Med. 10 (1989):445-63. DesPrez, R., and Heim, C.R. "Mycobacterium tuberculosis," in Mandell, G.L., Dounglas, R.G., and Bennett, J.E., eds. Principles and Practice of Infectious Disease. 3rd ed. New York: Wiley, 1990: 1877-1906.

31.

Pitchenik, AE., and Robinson, H.A. "The Radiographic Appearance of Tuberculosis in Patients with the Acquired Immune Deficiency Syndrome (AIDS) and Pre-AIDS," Am. Rev. Respir. Dis. 131 (1985): 393-96.

32.

Selwyn, P.A, Hartel, D., Lewis, V.A, et at., "A Prospective Study of the Risk of Tuberculosis Among Intravenous

Drug Users with Human Immunodeficiency Virus Infection," N. Engl. J. Med. 320 (1989):545-50. 33.Pablos-Mendez, A, Raviglione, M.D., Ruggero, B., and. Romox-Zuniga, R., "Drug Resistant Tuberculosis Among the Homeless in New York City," N. Y. State J. Med. 90 (1990): 35155. 34.

McDonald, R.J., Memon, A.M., and Reichman, L.B. "Successful Supervised Ambulatory Management of Tu-

berculosis Treatment Failures," Ann. Inter. Med. 96 (1982): 297-302. 35.

Schieffelbein, C.W., and Snider, D.E., "Tuberculosis Control Among Homeless Populations," Arch. Intern. Med. 148 (1988): 1843-46.

•

Dengue mosquitoes make an appearance in High Court The dozen-strong contingent of Aedes aegypti, dead and alive, were produced before the divisional bench Express News Service New Delhi, May 1

AEDES aegypti, or the dengue-causing mosquitoes, which last year claimed over 400 lives in the capital, finally made an appearance in Delhi High Court today. Brought before the bench of acting Chief Justice Mahinder Narain and Justice S.K. Mahajan by the scientists of National Institute of Communicable Disease, the dozen strong contingent of Aedes aegypti were brought to the court in six test tubes. . Besides the NICD scientists, hordes of MCD doctors also accompanied them. The larvae-eggs which produce this deadly mosquito-was also produced before the court in a polythene bag. They were produced before the court on the directions given yesterday, when the bench had expressed its desire to have a look at the deadly mosquito and its larvae. Both the judges inspected the Aedes aegypti-both dead and alive-with the help of magnifying glass before hearing the resumed arguments by the MCD counsel on the measures taken by the civic body to check the spread of dengue fever this year. Apparently dissatisfied with the claims of the civic body about the measures and action plan, the bench which suo moto initiated action against the local bodies, noted: "Strangely, the government action plan, submitted by the Union Health Ministry and the MCD did not mention any awareness campaign."

Coming down heavily on the MCD, the bench also asked the authorities to involve the information and broadcasting ministry in giving wide publicity to the preventive measures against dengue. During an hour-long argument on various scientific and civic causes which lead to its breeding, Dr. S K Sharma of the MCD informed the court that about 32 catching and two mobile vans are operating round-the-clock in the city to prevent its breeding. He, however, denied to disclose their locations and said that "If we disclose the catching stations, there is possibility of furnishing false information by the MCD staff." When the bench asked whether the MCD has identified the area where dengue mosquitoes and what action had been taken so far to prevent the disease, Dr Sharma said that "we can do nothing without the co-operation of the people." Dengue mosquitoes breed in any water-catching or storage container in shaded or sunny place including barrels, drums, jars, pots, plant saucers, tanks, cisterns, bottles, tins, tyres, pans, roof gutters, drip pan of refrigerators, drains, cement blocks, cemetery urns, bamboo stumps and other places where water gets collected.

• Source: Indian Express, 2.5.97

CONTENTS • Immunity, Ethics & Control of Leprosy • Resurgence of Infectious Diseases and the Indian Society

Author B R Chatterjee

Resurgent TB in New York City ......... .

• Dengue Mosquitoes make an appearance in High Court

1

5

A Zachariah, .M Pai, P Chatterjee

• MFC Annual Theme Meet, 1997 (Announcement) •

Page

6&7 K Brudeny and J Dobkin

8

Indian Express

15

The Medico Friend Circle (MFC) is an all India group of socially conscious individuals from diverse backgrounds, who come together because of a common concern about the health problems in the country. MFC is trying to critically analyse the existing health care system which is highly medicalized and to evolve an appropriate approach towards developing a system of health care which is humane and which can meet the needs of the vast majority of the population in our country. About half of the MFC members are doctors, mostly allopathic,' and the rest from other fields. Loosely knit and informal as a national organization, the group has been meeting annually for more than twenty years. The Medico Friend Circle Bulletin, now in its twenty first year of publication, is the official publication of the MFC. Both the organization and the Bulletin are funded solely through membership/subscription fees and individual donations. For more details write to the convenor.

Convenor's Office: Vijay Jani, 34 Kailash Park Society, Near Water Tank, Akota, Vadodara, Gujarat39OD20. Editorial Office: Sathyamala, B-7, 88/1, Safdarjung Enclave, New Delhi-110029. Subscription Rates: Inland (Rs.) Annual Life Individual 50 500 Institutional 100 100 Asia (US $) 10 0 Other Countries (US $) 15 100 Please add Rs. 10/- for outstation cheques. . 150 Cheques/money orders, to be sent in favour of Medico Friend Circle, directed to Manisha Gupte, 11 Archana Apts., 163 Solapur Road, Hadapsar, Pune-411028.

Edited by Sathyamala, B-7, 88/1, Safdarjung Enclave, New Delhi-110029; published by Sathyamala for Medico Friend Circle, 11 Archana Apts., 163 Solapur Road, Hadapsar, Pune-411028. Printed at Colourprint, Delhi-l10032. Views and opinions expressed in the bulletin are those of the authors and not necessarily of the organisation.


