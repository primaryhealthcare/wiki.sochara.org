---
title: "The Problems With RNTCP (DOTS), What Exactly Is At Stake"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled The Problems With RNTCP (DOTS), What Exactly Is At Stake from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC250-251.pdf](http://www.mfcindia.org/mfcpdfs/MFC250-251.pdf)*

Title: The Problems With RNTCP (DOTS), What Exactly Is At Stake

Authors: Sridhar

medico friend circle bulletin 250 251

January-February, 1998

Emerging Infectious Diseases HIV Disease in Kheda District Anurag Bhargava*

1989: A prestigious teaching hospital in New Delhi: An expatriate is admitted with opportunistic infections and profound wasting. He looks little more than a skeleton. A faculty member exclaims gleefully "AIDS is here!” 1991: University Hospital, Lusaka, Zambia: 'On a visit there I see their Medicine wards. More than 50 % {Jf the patients are in various stages of HIV disease, with tuberculosis, cryptococcal meningitis, Kaposi's sarcoma, The hospital has very little to offer **(1t ran out of insulin recently- Structural Adjustment ?). The house officer calmly signs 5 death certificates. The lady of the family I am staying with, who teaches in a school, says that at least one child drops out every week orphaned by HN. 1997: Karamsad, 41 patients have tested seropositive this year. At the moment of writing this article, I have discharged· a couple with HIV disease and have 3 other such patients. As I write this, I have a feeling that a whole decade in which we could have done something to prevent an epidemic of this scale developing in India has been lost. As it gathers its own Malthusian momentum, destroying families, it exposes the real face of our society, warts and all; the poverty, the ignorance, the profound vulnerability of the poor and of women, our rapacious medical establishment and its devil-may-care' attitude.

The statistics on the next page give us the magnitude of the problem as it has been observed at P. S. Medical. College, Karamsad but the statistics are, to use a cliche, faceless. The problems multiply manifold as we deal with individual patients. The stark truth is that in this part of India, BIV is partly an iatrogenic disease' with a devasting impact on families. . 'The following is a true account of some patients affected by HIV, albeit sketchy. I cannot adequately convey the fears, the anxiety, the intense turmoil they undergo as they bear the burden of coping with a disease, which is not only incurable, but brings about in its wake stigma, rejection and: disruption of relationships. •

Sulekha *** was 24 years old. She had been married for 5 years to Dinesh who had lived for many years in Uganda, and had also married a Ugandan national in order to get a liquor permit. He died a year before Sulekha developed constitutional features, and signs

* Dept. of Medicine, PS Medical College, Karamsad. The opinions expressed in this paper are strictly those of the author and do not reflect the opinion and policies of the Charutar Arogya Mundal and its institutions. ** In 1997 the situation hardly seems better. Test kits for HIV are sc tree and. when available, are reserved for screening blood for •

3

transfusion. (continuing Structural Adjustment?). . * * * The real name of all the patients nave been concealed to protect their identity.

suggestive of advanced HIV disease with lymph node TB. She weighed only 25 Kg., could barely sit up and yet always had an impish smile. When I examined her, her father's eyes grew moist. It was the first time in six months that any physician had touched her, he said. She had given birth to a male child who was seronegative. Quite promptly, the child was taken away by the 'in-laws' for rearing away from his mother who was denied till her last day, any contact with him. She died at home, crying for him, but by the time the 'in-laws' could be persuaded, it was too late. She had to bear the humiliation, till the end, of having infected their "innocent son". Dinubhai who is 28 years old was admitted with pneumococcal pneumonia in septic shock. Only one month earlier, he had been hospitalized in another hospital for a similar pneumonia. He had history of only one promiscuous contact 5 years ago in Bombay. He recovered from his pneumonia, was diagnosed as having early HIV disease. He is otherwise well. One of the interns/nurses apparently blurted out his disease status to one of his 'friends'. Since then all hell has broken loose. He was thrown out of his job and wherever he does manage to land another, word gets around. He has been ostracized by his friends. Jobless, ostracized, worried about his parents, and a sister to-be-married, he keeps approaching me to either get him a job or some poison, to end it all. I see him sometimes, standing alone around a corner in Karamsad, looking utterly lost in a world which has suddenly declared him an alien. Am I to blame? Aslam is a 24 year old who was transfused with 6 bottles of blood for G6PD (?) induced hemolysis, 3 years ago. He was engaged to be married only 3 weeks before he came to us and was diagnosed as having HIV disease with lymph node TB. He improved on treatment, but was lost to follow up, and was recently admitted with symptoms suggestive of Pneumocystis carinii pneumonia. He was terrified to even hear that he had lymph node tuberculosis. The brother pleaded with us not to reveal to him the nature of his illness. I pleaded with his brother not to get him married. The HIV epidemic is like a fire burning in the living room carpet. We are observing the speed at which it spreads the noxious fumes and the particulate content in the smoke. We must intervene to help put 1 out the fire. (KPWJ McAdam.)

Table 1* HIV SEROPREVALENGE 1991-1997 Category

No. Screened

Voluntary Donors

No. Positive Seroprevalence

24,953

20

0.08%

4,809.

130

5.4%

Patients (Medical, Surgical, Obstetric)

Table 2* HIV SEROPREVALENCE IN VOLUNTARY DONORS: YEARWISE RESULTS YEAR

TOTAL TESTED

HIV POSITIVE

PERCENTAGE

1991

55

—

1992

4711

02

0.04%

1993

5834

03

0.05%

1994

6153

04

0.06%

1995

4459

07

0.16%

1996

1962

01

0.05%

1997

1779

03

0.16%

—

TABLE 3*

HIV SEROPREVALENCE IN PATIENTS: YEARWISE RESULTS YEAR

TOTAL TESTED

HIV POSITIVE

PERCENTAGE

1992

65

—

—

1993

300

08

2.66%

1994

1647

33

2.0 %

1995

1298

30

2.31%

1996

964

30

3.11%

1997 (Upto September)

620

41

6.66%

-

TABLE 4* PROBABLE MODE OF HIV TRANSMISSION Total cases studied: 58 MODE

MALE

FEMALE

TOTAL

BLOOD TRANSFUSION

11 (18.9%)

24 (41.4%)

35 (60.3%)

SEXUAL EXPOSURE

06 (10.3%)

01 (1.7%)

07 (12.0%)

UNKNOWN

10 (17.2%)

06 (10.3%)

16 (27.5%)

*. These tables are derived in part from data presented by Dr. Sunil S. Trivedi*, Dr. J.D. Lakhani**, and Dr. Hita Shah *** in a paper titled' "HIV' Seropositive cases in and around Karamsad - Observations of a Rural Medical College", at the National Conference on HIV/AIDS organised by the Indian Society of Health Administrators, Dec 2-4, 1996, at Bangalore. * **

Associate Professor, Dept. of Microbiology, PSMC, Karamsad. Professor & Head, Dept. of Medicine, PSMC, Karamsad.

*** Assistant Professor, Dept. of Dermatology, PSMC, Karamsad.

•

Babubhai underwent an appendicectomy 6 years ago. He was transfused 1 unit of blood 'pre-operatively'. Now he has HIV disease with tuberculosis. His wife too is seropositive.

•

Sonal was given 1 unit of blood for 'weakness' at a private nursing home 4 years before her marriage. Her whole family is-seropositive including a 2 year old son. Her husband promptly left her. She has since then died.

•Trupti's husband was different. She was transfused around childbirth but her husband and child are seronegative. After her death due to a pneumonia, her husband came, shaken and trembling, requesting us for a HIV test which he had earlier refused.

Jashubhai was transfused 2 units blood, again without a clear indication, 5 years ago. He was detected as having HIV disease here on developing a severe cutaneous drug eruption 2 years ago. His wife still cares for him lovingly. She herself requested testing, since they had unprotected sex 3 months ago. They could not get condoms, she shrugged and said.

3. Women seem to be particularly susceptible with their high prevalence of anemia which often gets aggravated by pregnancy and pregnancy related complications. A number of women were transfused around the peripartum period, or in the past during abdominal hysterectomies which again lacked sufficient documentation, as being really indicated. It is the considered opinion of the better gynecologists in this district that an inordinately large number of hysterectomies are done without a valid indication. In fact one of them quipped "Kheda district is sure to achieve the goal of 'Hysterectomy for. All by 2000 AD '" If blood transfusions are abused by private practitioners, hospitals do not use them rationally either. It might be

We're all going to go crazy, living this (AIDS) epidemic every minute, while the rest of the world goes on out there, all around us, as if nothing is happening, going on with their own lives and not knowing what its like, what we're going through. We're living through war, but where they're living its peace time, and we're all in the same country. 2

These stories are illustrative of the tragedies being enacted in so many young lives being cut short by the virus. These stories also illustrate the evolution of HIV/AIDS in India, from an exotic disease with a foreign origin, to a largely metropolitan phenomenon with a heterosexual pattern of transmission, to an indigenous disease with our own peculiarly indigenous mode of transmission (blood

transfusions, truck drivers etc.). Based on our observation of the epidemic in Kheda district, some inferences can be drawn, some of which find mention in the accompanying statistics. 1. Apart from unsafe sex, transfusion of 'unsafe' blood by practitioners, procured from paid donors, constitutes a major risk factor for HIV transmission in Kheda district (> 60% of clinical cases studied has history of a transfusion). 2. The need as such for transfusions at all seemed questionable in many of our patients. It seems that a trend parallel to that of giving I.V fluids without indication is that of giving blood without a valid indication.

(Larry Kramer)

Pertinent to mention a study which analysed the blood transfusions ad4 ministrated in a Delhi hospital . It was found that almost 80% of the transfusions given were single unit transfusions. If just a single unit sufficed, the need for giving blood at all can be justifiably questioned. Use of alternatives like plasma expanders could be safer than giving blood which in the present scenario can never be considered 100% safe.

Caring for patients with HIV disease is turning out to be a very demanding act, professionally and emotionally. There are many contentious issues, scientific as well as ethical, which keep cropping up in the care of HIV patients.

Consent As a practical reality, in our hospital, all patients who are in the surgical services are screened for HIV infection before planned major surgeries. In our medicine unit only patients with suggestive clinical symptoms and epidemiologic risk factors are screened for HIV antibodies. I feel, whether preceded by pretest counseling or otherwise, two things should not be allowed to occur as a result of the testing: • Breach of the patient's confidentiality. • Denial of care in any form. My personal view is that identifying HIV positive individuals is important for the following reasons. a) The risk of having a HIV positive family may be

diminished by advice regarding contraception, and family planning. b) Even in a country like ours, where anti-retroviral therapy is unlikely to be afforded by the majority of patients, identification of the HIV status might, be of benefit to the patient (provided the physician provides rational and empathic care). It would provide the opportunity to provide prophylaxis and treatment of major HIV-related opportunistic diseases including tuberculosis. Knowledge of the patient's status alerts the clinician to the possibility of atypical presentation of common infectious diseases, e.g., tuberculosis, and to the possibility of unusual causes of common problems like community acquired pneumonia, e.g. P. carinii. Treating these intercurrent problems early is very important, as will be discussed later. Counseling As physicians we counsel, or are supposed to counsel all our patients, be their disease asthma, tuberculosis, or the like; i.e., we should be able to provide psychosocial support, as well as disease-related information to enable our patients to take decisions and act on them. Because of the personal and social consequences of HIV infection, this term has acquired a special significance, and if may say so, a 'mystique'. In the context of HIV/AIDS the goals of counseling are to support the ability of infected persons and those who care for them to cope with the stresses of HIV/AIDS, to discuss various aspects of HIV disease and to help, prevent transmission of HIV to others. 5 We do post-test counseling in association with the psychiatrists. To be quite frank, we find pretest counseling practical, largely with spouses of HIV positive patients. The greater visibility of HIV disease in the West leads to a greater anticipated reaction of the patient there to the test result. In the population we see (more than 70% of our patients come from a rural background), awareness of patients about HIVIAIDS is low. Initial reactions to disclosure are not often dramatic. Many patients cannot grasp the concept of a chronic insidious and incurable infection" with such profound implications for their lives. Confirmation of diagnosis - To blot or not to blot There seems to be some confusion, that a, Western blot is necessary for confirmation of the diagnosis of HIV

infection. This is simply not true. The ELISAs now being used are 2nd and 3rd generation ELISAs which employ recombinant antigens/synthetic peptides of the virus, rather than a viral lysate, which was used earlier. These have a very high sensitivity and specificity. The WHO recommends that a serum found to be reactive on the first assay (ELISA or rapid/simple assay), should be re-tested using a second ELISA or rapid/simple assay based on a different antigen preparation and/or different test principle. An additional sample should be tested from all persons newly diagnosed to be seropositive, on the basis of their first sample to eliminate a lab or clerical error 5. A Western blot is required only if the two tests differ in their result or when a borderline result is obtained from either test. Facilities for doing a Western blot exist in very few centers in India. This so-caned 'lack of ' confirmation by Western blot is used often in the press to further the argument that reports of increasing prevalence are lacking in authenticity. Confidentiality This should be protected at all costs. This very important issue gets casual treatment in-the hospital setting. The patient's seropositivity (which has most often been determined without consent) is prominently displayed on case sheets. At least one patient was apprehensive that his village people would know about his disease through such means. After having taken the nursing staff into confidences, we do not display the HIV test report in any manner on the case sheet. But maintaining the patient's confidentiality in a hospital is a difficult task, with all labs, demanding that the patient's HIV status be mentioned on the investigation form, for the sake of taking extra precautions. The concept of employing universal precautions at the bedside, and in the lab is hard to enforce. The concept that a medical professional faces greater danger in handling casually a sample whose HIV status is unknown, than in handling a sample from a HIV positive patient, is also somehow only grudgingly accepted. 4

Care of Patients This ranges from no care (most private practitioner) to care only till the patient's status becomes known (most govt. hospitals including some large teaching hospitals). Then the patient is left to fend for himself. And an oft repeated statement one hears is, why should one treat" HIV disease at all with its invariably fatal outcome. It is not only the lay people but also students and

medical professionals, who take diagnosis of HIV infection to be synonymous with AIDS. In a case in Goa, Dominic D'souza who was a voluntary blood donor and was found to be seropositive was incarcerated for 64 days although he was asymptomatic. The concept that HIV causes a whole spectrum of disease, ranging from asymptomatic infection, early disease, to advanced disease (AIDS), does not seem to be understood. HIV infection per se, is at the present incurable, be it in Karamsad or in Los Angeles. What anti-retroviral therapy does achieve is slowing the progression of disease. It has been estimated that AZT therapy alone prolongs life by a year I. The fact is that the period before the patient develops AIDS can be made more productive by tacking the patient's common problems of tuberculosis, oral candidiasis, and by giving prophylaxis for opportunistic pathogens like P. carinii (which is relatively cheap and effective). In fact 53% of patients of HIV/AIDS reported in a autopsy study from West Africa, died of potentially preventable or treatable conditions; Tuberculosis (32% of deaths) and bacteremia being 6 the foremost. These facts should provide a strong argument against practicing therapeutic nihilism in HIV patients. Tuberculosis is one of the most common and important opportunistic pathogens in HIV/AIDS, which may present with atypical manifestations, and undiagnosed tuberculosis is a major killer. Recognising and treating HN associated tuberculosis is especially important, since I feel that anti tubercular treatment is possibly the only anti-retroviral therapy we can ever afford for our patients. Tuberculosis by causing increased retroviral transcription (mediated by cytokines like Tumor-necrosis factor), accelerates the rate of progression of HIV disease. Thus HIV infection predisposes to tuberculosis, while tuberculosis in turn accelerates the rate of progression of HI V disease. The estimated risk of developing tuberculosis in tuberculin positive individual is around 10% per lifetime7. In contrast to this, it:' patient who is HIV positive, the risk of develop' g tuberculosis is of the order of 10% per year7. Given at the prevalence of tuberculin positivity in India is around 50%, it is likely that most of our patients with HIV disease will ultimately develop tuberculosis. This is borne out by our experience, where tuberculosis is the commonest HIV associated disease in our patients. The greater propensity 'or HIV infected patients to harbor multi drug resistant mycobacteria can spell disaster for the already bad problem of TB in India.

It gives me great satisfaction to find some of our patients going back to their work as a result of treatment working in the fields, driving rickshaws. It is another matter that their truce with the virus may often be short lasting. A number of patients, especially those with more advanced disease, are 'lost to follow up'. The care which is most often denied to ~IV infected persons is obstetric and surgical where the risks perceived to the doctors are the maximum. In Delhi, in an infamous episode, inspite of much advanced notice, a Prof. of Obstetrics (who had attended many a seminar on HIV related obstetric care) refused to organize the delivery of a HIV infected woman on grounds of in adequate facilities. It is another matter that these matters are never pressed too hard. Discharging the patient seems so much more convenient to the administration and doctors alike. It is here, in the denial of care, that the whole idea of testing without consent becomes totally unethical. An issue which arises in the care of these patients is the need for isolation. At least as far as medical care of these patients is concerned, I fail to understand the grounds for discharging them, or the need for any special facilities. Blood and body fluid precautions need to be observed, and disposal of infected waste should be proper. These precautions ought to be observed for all patients as such. HIV patients pose no special risk to other patients in a general ward, if the Hospital does not display high risk behavior (using unsterile needles, improper waste disposal). I am fully aware that in public hospitals, where everything is in short supply except patients, this is like asking for the moon. But I believe that apart from the lack of funds, it is the 'chalta hai' attitude, which is responsible. I have seen very busy mission hospitals taking scrupulous care in these matters inspite of a resource crunch. A study from KGMC Lucknow, published in an American journal, concluded that it was not only anti-tubercular drugs which caused hepatitis in patients with tuberculosis; 35% of 40 children who developed acute hepatitis on therapy, had evidence of hepatitis B, which was parenterally acquired in the 8 hospital. . Competence The task of managing patients with HIV disease in India is very difficult, given the state of knowledge of the large number of practitioners about the disease, the irreluc-

tance in caring for these patients and given the limitation of diagnostic facilities. Our track record of microbiologic diagnosis in common pathogens leaves much to be desired. In India, it is difficult to get reliable data on bacteriologic profile of pneumonias, diarrheal diseases, (with few exceptions). Our track record with less common pathogens is even less satisfying, as the plague epidemic so amply illustrated. How one to manage patients whose diarrhea is may be caused by such apparent exotica as cryptosporidia, isospora, whose pneumonia might be due to Pneumocystis carinii, or whose fever may be due to Mycobacterium avium-intracellulare complex? There are very few centers in India which even do mycobacterial cultures. In this situation, treatment of patients in most settings is likely to be largely empirical (which may however be rational, given our limitations). I had a recent experience which illustrates this. Aslam was recently admitted with features typical of P. carinii pneumonia, fever with dry cough, increasing dyspnea, a lung normal on auscultation, X-ray showing bilateral alveolar and interstitial opacities; and severe hypoxemia on blood gas analysis. Sputum (including induced sputum) was negative for P. carinii, but we did not have all the required stains, and bronchoalveolar lavage was not available. But he improved dramatically on high dose cotrimoxazole and steroids. So far so good. But what do I do for Maheshbhai, who has persistent fever and painful l swallowing? His CD4 count is 63/mm: . Testing for mycobacterium avium intracellulare infection is only available at Bombay. I have very little to offer him now. An investigation like CD4 count which is used in the West to stage the disease and plan therapy is either not available at most places, or available at an exorbitant cost (Rs, 10001- per test in Ahmedabad). Routinely we use an absolute lymphocyte count for staging which correlates broadly with the CD4 count.

Drug

Cost per day

Duration

(Rs.)

Zidovudine

200 mg t.i.d.

150.00

Lifelong

Fluconazole

200 mg o.d.

42.00

Lifelong

Clofazimine

100 mg o.d.

1.50

Lifelong

Ethambutol

800 mg o.d.

2.20

Lifelong

Clarithromycin

250 mg. b.i.d.

70.00

Lifelong

Co-trimoxoazole

DS o.d

1.80

Lifelong

Ciprof1oxacin

750 mg o.d.

9.00

Lifelong

Cost per day = Rs. 276.50

The annual cost of treating a patient with AIDS in the U.S. 9 has been computed to be around $ 35,000 .

Control HIV is a virus which thankfully has limited modes of transmission. Prevention of HIV infection should theoretically be easier than prevention of water and air borne pathogens. HIV/AIDS is spread as much by human behaviour and ignorance as by the virus. And the roots of that behaviour lie also in the kind of effects resulting from our kind of economic development: large populations of migrant labour existing in our cities with breakdown of traditional family structures. We cannot modify that behaviour but the least we can do is to ensure that the behaviour is an informed one. We are engaged in a perpetual debate whether sex education should be introduced in schools. But what about colleges? The National AIDS Control Organisation has established surveillance centres since its inception in 1987, and has been responsible for spreading awareness about HIV/AIDS. The media messages have spread information as well as panic. There have been too many projections of the "every third woman in Bombay will be HIV +ve by A.D." kind Panic just leads to even greater stigmatization of the HIV infected.

Cost At the outset, I would like to say that treatment of HIV disease (especially in its advanced stages) is something we simply cannot afford, even in the best of times. With the kind of patients I am managing, it seems HIV is predominantly a poor man's disease. Our hope lies solely in prevention. To give an example, let us compute the cost of care for Maheshbhai (if we were to follow text-book recommendations) who has had esophageal candidiasis and who may be considered to have mycobacterium avium intracellulare infection.

There has been no large scale initiative on HIV transmission through commercial sex, and until recently there were no legal directives on safe blood banking in India. I could hope that what has been described in this part of India is a local aberration, but the reality is different. Half of 10 India’s demand for blood is met by paid donors . In a study from Ahmedabad, paid donors reported no condom use and no knowledge about HIV in general, and 50% reported frequent contact : with commercial sex

11

Workers 86% of blood donors screened in a study reported from Bombay were HIV positive and 48% still 12 donated blood despite being infected with HIV . The government's and the medical community's (of which I am a part) response to these horrifying figures has been weak and tardy. On the .government's side, the blood bank trade still flourishes in an unregulated manner (although some action has been taken in the last one year in the metropolises, as a result of the Supreme Court directive). While educational efforts about spread of HI V emphasise the sexual mode, the dangers of blood procured from commercial/unlicensed blood banks is not highlighted adequately. The medical community's role has been partly to help the fire to spread by use of commercial blood banks, by giving blood to patients often without valid indications, and by allowing conditions in our hospital to exist which makes them HIV transmission prone. The more learned amongst our fraternity have been documenting the rate of spread of the fire and measuring the noxious fumes and the particulate matter generated. The only legal action which has woken the government from its slumber was initiated by Common Cause.

The picture I have painted is bleak, but there is little to cheer about than the spirit of some of my patients and their relatives. I have seen a few devoted couples, young and old, bearing well the strain; doting brothers who are willing to spend much beyond their modest means, and patients who still show an enormous capacity to bear misery with a generosity of spirit (however misplaced it many seem to me). I wanted one of them who had some documents, to sue the blood bank involved. He would rather let things be. Moreover, it was a question of someone's livelihood, he said. I hope I have been able to show a part of the face of HIV in India: a society where any responsible discussion on sexual behavior is taboo, where patients with sexually transmitted disease float from one quack to another, where requests for blood donation arouse instant disapproval, where practitioners transfuse blood procured from commercial blood banks on very slim indications; a society which provides fertile soil for the growth of HIV. We are reaping its grim harvest. The tragedy is that the rest of the world (sub-saharan Africa excluded) may be able to contain their own epidemics in the time to come.

If what we have seen here could be generalised, HIV affects the marginalised of our own society. There are few rich men here, there may not be any sports or movie stars with HIV/AIDS, I suspect, to give the disease some respectability and face they can feel some sympathy for, and no poets to articulate their agony. The middle class which is getting increasingly well informed may not have it 13 on their agenda. To paraphrase a WHO publication , it may soon be that the powerful may lose interest, and the powerless will have no choice. Acknowledgements: I am grateful to prof. Nivedita Desai, Dean P.S. Medical College, for her kind permission to publish this work. I am grateful to Drs. Sunil Trivedi, J.D. Lakhani, Hita Shah for permission to quote from their paper. My thanks also to Mr. T.A. Manavalan and Ms. Alka Gosai, for their help in, the preparation' of the manuscript.

References 1.

K.P.W. J. McAdam in Tuberculosis: Back to the Future. Edited by J.D.H. Porter and K.P.W.J. McAdam. 1994; John Wiley & Sons Ltd.

2.

Medical Wit and Wisdom. Jess M. Brailler, 1993; Running Press, Philadelphia.

3.

Kasongo Z. Impact of HIV on surgical care. Lancet. 1997; 349 (suppl III): 19.

4.

Gupta AK, Doda V, Kapoor A. Blood transfusion trend in a Delhi hospital-a report. Ind. Journal of Internal Medicine. 1995; 5(7): 66-71.

5. 6.

AIDS in Africa: A Manual for Physicians. 1992; WHO, Geneva. Lucas SB et al. The mortality and pathology of HIV infection in a West African city. AIDS. 1993; 7: 1569-1579.

7.

Telzak E.E. Tuberculosis and Human Immunodeficiency Virus Infection. Medical Clinics of North America. 1997; Vol 81, No.2.

8.

Kumar A. Misra PK, Mehotra R, Govil YC, Rana GS. Hepatotoxity. of Rifampicin and Isoniazid: Is it all drug induced hepatitis? American Review of Respiratory Diseases. 1991; 143: 1350-1352.

9.

Hellinger FJ. Forecasting the medical care costs of the HIV epidemic: 1991-1994. Inquiry 1991; 28: 2861-6

10. Kandela P. HIV banks (news). Lancet 1991; 338: 436-437. 11. Bhattacharya RD, Dalwadi V, Parekh V. A study of the commercial blood donor system in Ahmedabad (India) and implications for HIV control <Abstract # PoD 5165). VIII International Conference on AIDS, Amsterdam, July 19-24, 1992. Vol 2, p. D414, 1992. 12. Bhimani GV, Gilada IS. HIV prevalence in people with no fixed abode-a study of blood donor ship patterns and risk determinants (Abstract # MoC 0093). VIII International Conference on AIDS, Amsterdam, July 19-24, 1992. Vol 1, p. M023, 1992. 13. AIDS: Images of the Epidemic. 1993; WHO Geneva.

•

The Revised NTCP Prabir Chatterjee Dept. of Community Medicine, CMCH, Vellore

The Revised National Tuberculosis Control Programme (RNTCP) is being implemented in one fourth of the country (among 271.25 million people) and involves: •

Only 6 month Short Course Chemotherapy

• •

Fully intermittent regimes (given only 3 days a week) Directly Observed Treatment (DOT, where health workers administer medicines). Drug supply assured by taking a loan from the World Bank.

•

Problems: 1. Short course chemotherapy is much costlier than Standard regimes (12 month course or longer). A supporter of SCC point out that case holding is lower and relapses are more frequent with SR. 2. Very few intermittent regimes for sputum positive pulmonary tuberculosis have been successful without Streptomycin. The few successful non-Streptomycin "intermittent" regimes start with at least an induction phase of 15 days of daily therapy. For instance, the WHO recommends 2 HRZE / 4 H3 R3 for sputum positive cases as a possible alternative to 2HRZ.E /4 HR or 2HRZE/6TH or 2HRZS /4 HR. Most studies of intermittent regimes have been confined to cities (Hong Kong, Bangalore, Madras / Tambaram, Denver). Two studies do not actually specify whether participants were from rural or urban areas: One in Tarrant County near Fort Worth, Texas, and one in East Africa with the central point being Nairobi, Kenya. Travel and supervision are likely to be difficult in rural settings. Intermittent regimes are supposed to be fully supervised (DOT/ directly observed) even in U.S.A. (see the American Thoracic Association guidelines). 3. Directly observed treatment has mostly been tried in America (except for direct observation of an older Standard Regime in Madras in 1963).

DOT is of two types A. Clinic based: The R-NTCP follows this model ("PHI" based) with fortnightly visits by the patient to the PHC (Primary Health Centre). Thus the patient travels 12 times to a block town where the Primary Health Centre). Thus is situated, losing 12 working days and spending 24 bus fares. This would cost him around Rs. 350/-. Of course this is assuming that the patient reaches the PHC without any attendant. ,. In the American model (Denver 62 dose model) each dose of medicine is supervised. In Indian terms this would cost a patient Rs.2, 000/-. B. Patient based: In Baltimore, a Case Management team with two nurses supervises about 30 cases. This would mean an outlay of Rs. 600/- per course per patient (12 days salary for a MPHW) by the control programme in an Indian setting. The Bangladesh model involves a village TB worker, who could be paid around Rs. 100/- per month per patient supervised. (This is not in fact paid). Thus if there are 4 TB patient a year in a village of thousand people, the village TB worker could get a stipend of Rs. 200/- per month. Calculated per patient per course this too comes to Rs. 600/-.

Conventional (non-supervised) SCC involves only 6 clinic visits (treatment is self administered every day) and thus the expense to the patient is Rs. 175/-. Supporters of DOT point out that relapses, deaths and poor case holding make conventional self administered daily SCC 20%) costlier than its face value. Thus R-A really costs Rs. 1, 250/- while R-NTCP category-I costs Rs. 1,100/-. This of course misses the fact that category-I R-NTCP is itself not fully supervised and when the cost of a village TB worker or Case Manager approach is included it comes to Rs. 1,350/-. Also if one looks at it from the patient's point of view.

*Acknowledgment: A million thanks to Jeeva who did the typing

•

Village TB worker is the cheapest method

•

SCC self administered costs Rs. 175/- extra:

•

R-NTCP costs Rs. 350/- more than the cheapest method.

•

62 Dose Denver method costs the patient Rs. 2,000/- extra.

REGIME

DRUGS

OVER

a MPHW in their subcentre? One wonders about tracing patients in 24 hours. Will DOT be quite as successful in India? There are of course often other questions - whether a TB Control Programme is supposed to be controlling the patient? Or, whether a National TB programme should be seen as the basic right of every citizen who has the disease? The right to get TB drugs and cure versus the offence of not being at the PHI or at home when you are struggling to earn your daily bread.

TOTAL REMARKS HEADS

R·NTCP CATEGORY I

2 E3 H3 R3 Z3/ 4 H3 R3 Rs.750/-.

CONVENTIONAL 2 EHRZ 16TH SCC R-A Rs.8501-. 62 DOSE DENVER

12 VISITS Rs.350/-.

Rs.1100/-.

NOW IN USE IN 1/4 OF INDIA.

Rs.1025/-

USED IN REST OF COUNTRY

Rs.2750/·.

MUCH QUOTED AMERICAN MODEL

Rs, 1350/-.

MODIFIED CASE MANAGER

6 VISITS Rs.175/-.

0.5 SHRZ/ 62 VISITS 1.5 S2 H2 R2 Z2/ 4 H2 R2 Rs.750/-. Rs.2000/-.

BANGLADESH 2 E3 H3 R3 Z3/ (VILLAGE TB 4 H3 R3 WORKER) Rs.750/-. MODIFIED

12 MPHW DAYS Rs.600/-. OR STIPEND

S= Streptomycin

Previous number = no. of months

E= Ethambutol Z= Pyrazinamide H= Isoniazid R= Rifampicin

eg. 3 H = 3 months Isoniazid Number following = times a week eg. H 3 Isoniazid 3 times a week

R-NTCP without a village TB worker or a Case Manager approach is not as attractive to the patient as SCC self administered. However, R-NTCP is cheaper than buying the drugs from a quack who visits the village. Quacks are quite common and are involved in a large part of TB treatment in India today for better or for worse. 4. DOT as practised in U.S.A. has some clauses that have been called "intrusive": •

legal mandate to adhere to medical therapy

•

Involuntary hospitalization or (rarely) incarceration if compliance is not achieved.

•

Pharmacy Reporting Ordinance which makes it compulsory for pharmacists to report every dose of TB drugs dispensed.

•

Failure to administer a dose to be reported within 24 hours so that patient may be located.

A practical question- would it be considered polite to insist on tracing the District TB Officer within 24 hours in an Indian town? Or a PHC doctor in a block town? Or

5. Even in the U.S.A. the rise in TB was attributed in part to •

Physician mismanagement of cases.

•

Reduction in funding for public TB control Programmes

6. Let us suppose that the World Bank loan does ensure regular drug supply in 'the PHCs. •

Who supplies these drugs and thus profit from the loan?

•

Are these drugs being bought at Indian rates or foreign ones? A 600 mg. Rifampicin capsule costs $1 (Rs.30/) in U.S.A. but only Rs.7/- in India.

•

If purchases are being made outside India - has this affected the smaller Indian manufacturers in the states where R-NTCP is implemented?

•

When the loan gets over -who will pay for the drugs for the National TB programme? Or will supply become irregular again?

In short, is the improvement in drug supply sustainable? 7. Studies under field conditions in India and the District TB programme monitoring reports show that treatment completion is as low as 33% (with smear negativity reaching 75%) and 53 % (with resultant smear negativity of 96%). Alternative methods such as using dais, without giving any incentives, increased completion to 6&% (with a smear negativity of 86.9%). We seriously need to take' these Indian figures into account when planning a National TB Programme. Even in the U.S.A., the national statistics of completion do not come anywhere close to the model ones.

Conclusion: •

The major problem in India has been irregular supplies of TB drugs.

•

R-NTCP is not an ideal DOT programme.

• Neither is it very attractive to Indian patients •

American DOT methods are unsuitable in rural India

•

A dai or village worker method would be most relevant in India.

Intermittent Short Course Chemotherapy Studies PLACE

INDUCTION CONTINUATION

RELAPSES EFFICACY

Bangalore

2 EHR

4 H2 R2

10%

71.5%

MADRAS

2 S3 R3 H3 Z3 2 S2 R2 H2 Z2

4 H2 R2 & 4 " OTHER REGIMES

6.2% 7.9%

81%

2 S2 H2 R2Z2 2EHRZ

4 H2 R2 6 TH

NTI

EAST AFRICA 2 S H R Z 3 DENVER

CURE RATE 90% CURE RATE 96%

4 SS2 H2 Z2

1 H S E (OR PAS) 18 H2 S2 OR 2 H S E OR 18 H2 E2 OR 3 H S E OR 18 H2 S2 E2 TILL NEGATIVE CULTURE

62 DENVER 0.5 S H R Z I 1.5 S2 H2 R2 Z2 4 H2 R2 5 HONG KONG 6 H3 R3 Z3 83 E3 6 H3 R3 Z3 83 6 H3 R3 Z3 E3 6 H3 R3 S3 E3 6HRZE 1991 HONG KONG *

4S3 2Z3 6H3 6R3 4S3 4Z3 6H3 6R3 4S3 6Z3 6H3 6R3 No S 6Z3 6H3 6R3

1.3% <BEST) 4.4% (NO 8M) 10.3% (NO PZA) NOT INTERMITTENT 3% 3 TO 6% 1 TO 6% 4 TO 9 %

* There were no significant differences between regimes. However, in view of the failures during 6Z No S treatment, the Hong Kong Chest Service continues to give a fourth drug, usually Streptomycin, during the first two months of intermittent six month therapy in sputum positive patients. Further studies on the value of a fourth drug are desirable. References 1. Tuberculosis Rom and Garay: Little Brown New York 1996 2. Text Book of Pulmonary and Extra pulmonary Tuberculosis Satya Sri 2nd Edition : Interprint - New Delhi 3. Controlled Trial of 2, 4 and 6 months of Pyrazinamide in six month 3 times weekly regimes for' smear positive pulmonary tuberculosis - results at 30 months. Hong Kong Chest Service I British Medical Research Council. American Review of Respiratory Disease; 1991; 143:700 - 706 4. Five year follow up of a Controlled trial of 5 six months regimens of Chemotherapy for pulmonary tuberculosis. HKCS / BMRC Am

Rev Respir Dis 1987; 136: 1339 - 1342 5. Final Report of 3 Denver Studies Sbarbaro I Catlin I Iseman. Am Rev Respir Dis 1980; 121: 172 - 174 6. A 62 Dose six month Therapy for Pulmonary and Extra pulmonary Tuberculosis Cohn/Catlin/Peterson et al. Ann Int Med 1990; 112 : 407 - 415 7. The effect of Directly Observed Therapy on the Rates of Drug Resistance and Relapse in Tuberculosis Weis/Slocum/Blais et al N E J M 1994; 330: 1179 -1184 8. Controlled clinical Trial of 4 six month regimens of chemotherapy for pulmonary Tuberculosis Second East African! BMRC Study Am Rev Respir Dis 1976; 114 : 471 - 475 9. Cost effectiveness of directly observed versus self administered therapy for tuberculosis. Moore/Chaulk/Griffiths/Cavalcante/ Chaisson. Am J Respir Crit Care Med 1996; 154: 1013 - 1019 10. Tuberculosis control in Resource poor countries: alternative approaches in the era of HIV. De Cock I Wilkinson. - Lancet 1995; 346 : 675 - 677 11. Status of Short Course Chemotherapy under NaflonalTu1lefcufosls Programme. Suryanarayana I Vembu /Sathya Narayana/ Raj Lakshmi. Ind J Tub 1994; 41: 211 - 221 12. Operational Feasibility of an unsupervised Intermittent Short Course Chemotherapy Regimen at the District Tuberculosis Centre. Jagota / Gupta /Sreenivas / Parimala / Chaudhuri. Ind J Tub 1991; 38: 55 - 62. 13. Fully Intermittent six month regimens for pulmonary Tuberculosis in South India. Rani Balasubramanyam. Ind J Tub 1991; 38 : 51 53 14. Short Course Chemotherapy for Tuberculosis in Children Varudhkar. Ind J Paediatr 1985; 52: 593 - 597 15. Eleven years of Community Based Directly Observed, Therapy for Tuberculosis. Chaulk I Moore - Rice I Rizzo I Chaisson. JAMA Sept 27 1995; 274 (12): 945 - 951 16. Treatment of Tuberculosis and Tuberculosis infection in Adults and Children American Thoracic Society. Am J Resp Crit Care Med 1994: 149 : 1359 -1374, 17. Treatment of Tuberculosis - guidelines for National Programmes. WHO GENEVA 1993 18. Tuberculosis Control in India Edi. Chakraborty/Daniel/Chaturvedi. Action Aid 1996 - Bangalore 19. Results of Treatment with short course Chemotherapy used under field conditions in District Tuberculosis. Programme. Chaudhuri/l Jagota/Parimala Indian J Tub 1993; 40: 83 -89 20. Evaluation of. National Tuberculosis Programme Baily. Indian J Tub 1997; 44 : 61 -66 21. Evaluation of N J P (Editorial) Nagpaul Indian J Tub. 1997; 44: 59 - 60 22. An alternative method of providing supervised short course chemotherapy in District Tuberculosis Programme. Jagota/ Balasangmeshwara/Jayalakshmi/Islam. Indian J Tub 1997; 44: 73 77 23. The Revised National Tuberculosis Programme. Ashtekar S M F C - B 1996; 232 - 233 : 1-5 24. Tuberculosis control; Did the programme fail or did we fail the programme? Dujardin/Kegels et al (Editorial). Tropical Medicine and International Health' 1997; 2 (8) : 715 -718

The problems with RNTCP (DOTS) What exactly is at stake? Sridhar Sewa Rural, Jhagadia, Gujarat

The National Tuberculosis Control Programme (NTCP) was recently revised .to become RNTCP. The major change was the introduction of a set of supervised, short course regimen for treatment of tuberculosis, known the world over by the acronym DOTS, Directly Observed Treatment, Short-course. A lot has already been said about DOTS, mainly in literature concerning tuberculosis, and particularly in a series of articles published by VHAI in different fora over the last few years, the latter focussing on the potential and actual problems in implementing DOTS in India. I attempt to present the more relevant facts of the case, drawing some conclusions as I see them. In doing this, I assume the reader is familiar with tuberculosis as a disease, and with the structure and function of the NTCP. The issues raised so far mainly concern aspects of cost, appropriateness of treatment regimen prescribed, and operational feasibility.

A. Cost The issue of cost is probably best considered in its different components for purposes of greater clarity - the drug costs, transport costs for patients or staff, programme implementation costs, and the overall cost-effectiveness of the programme. Drugs Cost per Average Adult Patient with Different Current Regimen: Regimen

1. Drug costs: This again is better understood at two

different levels or perspectives: a. A comparison of the costs of treatment regimen of the NTCP and the RNTCP: The drug costs of the regimen currently used / recommended are computed in the table below (DOTS and lion-DOTS). Each regimen is indicated for a particular kind of disease or patient (diagnosis based on sputum or X-ray, fresh detection or relapse, regular or chronic defaulter, etc.). The most commonly used regimen for sputum positive patients are RA (non-DOTS) and Category-l (DOTS). Where facilities for sputum examination are reasonably decent, about 50-70%. of all patients of pulmonary tuberculosis should go on to one of these two regimen, depending on whether his/her place of residence falls in a DOTS-covered area or otherwise. As can be seen, there is not much of a difference in the cost. Similarly, RB and Category-2 (for smear positive relapses / retreatments) are equivalent (meant for X-ray based diagnosis of pulmonary tuberculosis), and their costs are also comparable. R1 and Category-3 are also equivalent in the sense that they are meant for patients diagnosed on basis of X-ray. R1 is also used for new smear positive cases in areas where SCC is not available, where it should be compared

Description Duration

Non Supervised Regimen Standard (“long course”) RJ STH for 2 mo. then TH for 10 mo (all daily) TH for 12 months (all daily)

R2 Short-Course (SCC) RA

HERZ for 2 mo. then TH for 6 mo tall daily)

RH

HSRZ for 2 mo. daily then SHR twice weekly for 4 mo

Directly Observed Therapy-Short-Course (DOTS) Cat J Cat 2 Cat 3

HERZ thrice weekly for 2 mo. then HR thrice weekly for 4 mo SHERZ thrice weekly for 2 mo. then HERZ thrice weekly for 1 mo. then HER thrice weekly for 5 mo HRZ thrice weekly for 2 mo. then HR thrice weekly for 4 mo.

Cost (Rs.)

12 months

472

12 months

(06

8 months 6 months

555 1099

6 months 8 months

446 937

6 months

383

(R=Rifampicin. Z = Pyrazinamide. E = Ethambutol. S = Streptomycin (injectable). H = INH. T = Thiacetazone). (All regimens are recommended by the National TB Institute. and implemented in the NTCP. The exact doses of drugs arc not mentioned but will differ slightly from regimen to regimen. In calculating costs of regimen the exact number of doses recommended has been followed. The duration of non-supervised regimen may extend by about a few weeks in practice when patients delay drug collections. As a rule an 80% drug-collection rate is considered "completed treatment") These drug costs have been computed using approximate current lowest market rates in April-June 97 of various drugs which are as follows: Rifampicin Rs 3.10/-50mg. Pyrazinamide Rs. 41l500mg. Ethambutol Rs. 1.60/800mg. Inj. Streptomycin Rs. 6.101750 mg vial. INH Re. 0.24 each 300mg. and INH + Thiacetazone combination Re. O.29/adult tablets. It should he a safe assumption that the cost for the government would not exceed this given reasonably adequate management. The Chinese government reportedly procured antituberculous drugs at about a sixth of the market rate by coordinating purchases. Even if we are able to achieve something similar in India the comparative picture described above should hold.

to Cat-1 of DOTS. In either case, R1 turns out to be more expensive, even if one does not consider the" cost"," of administering streptomycin. As an inferior alternative in situations where it is not possible to use streptomycin, NTCP allowed ethambutol to replace streptomycin, which drastically reduces the cost of R1 to about Rs. 200. Except for this situation, DOTS regimen prove to be less expensive than equivalent NTPC regimen despite using more expensive drugs, mainly by virtue of being administered only thrice a week throughout the duration of the treatment. R2 is the "dump", at a practical level common to all areas, which is offered to patients refusing other treatment, or unresponsive to any of these (there can be ethical questions here, of course, over the appropriateness of this "public health" approach to such patients). There are some differences in the assignment of patients with extra pulmonary tuberculosis to different regimen, but these constitute a minority «10%) of the whole TB load. b. Arguments are being put forward questioning the need of short-course chemotherapy per se (as compared to standard long-course therapy). The question of relative efficacy is dealt with briefly below. As to the cost, as mentioned above, the cost ofR1 (the standard treatment) is still more than that of Category 3 (a SCC regimen). Only a rifampicin- or pyrazinamide- free regimen like the alternative R1 or R2 would cost much less. The relative efficacy of this is also discussed below. 2. Transport costs: In cost terms, this is a question of the cost of the health worker reaching the patient's home twice weekly for the duration of the treatment plus the bimonthly visit of the patient to the PHC/TB centre under DOTS

versus the cost of the patient coming for his fortnightly / monthly collection once a month to the PHCITB Center under non-DOTS Here" only the travel costs of patients coming to a center for collecting drugs is the real cost, since it is envisaged that DOTS will utilise the existing network of health workers for reaching drugs to the patient during their routine field visits - at no extra cost. It is obvious that, if we assume the programme will function as envisaged, it will in fact reduce the cost burden on the patients, since they will now have to visit the centers less frequently than under non-supervised regimen. We will visit this assumption later, below. 3. Programme implementation costs: Costs of diagnostics are comparable. Apart from the costs of initial training and orientation of different categories of workers, there is minimal extra on-going cost of implementation, since the brunt will be borne by health workers of PHCs, and they will have to cover no more than 3-4

patients in the average village. Again, the assumption is that they will. (The experience of pilot studies in India suggests that heavy extra financing was needed, but we can safely assume that this anxiety to show results will not last till the programme phase, and the overheads will never be incurred). 4. Overall cost-effectiveness: If DOTS is successfully implemented, and if it indeed achieves the 80% plus cure rates being reported from pilot projects in India and abroad, there is little to" fear on this account. On the contrary, there will be major cost saving. A4etailed cost effectiveness study for DOTS in India has been made by Ravindra Dholakia of IIM-A (published by WHO, 1997), projecting an effective rate of return from DOTS of about 16%, or equivalent to at least $550 million per year. The World Bank hails DOTS as "one of the most cost-effective of all health interventions .... ' even at 50 times the cost the DOTS strategy would still be extremely cost-effective". Accolades indeed. The main reason for the high cost-effectiveness is the projected effectiveness: a cure rate of at least 85%, case detection of at least 70%, and drastic reductions in risk of new infection, relapses and mortality. Again, the only major assumption is, this will happen. The question of cost, in fact, must probably be turned over on its head: we should ask, not whether we can ever afford DOTS, but why we cannot commit the Rs.500/- or so in drug costs per patient, which could break the back of this disease that affects as many people in. India each year as in all the world (excluding China, which has half as much TB as we have) put together? Can we really not raise such resources? The problem is that for decades, we have not raised enough to ensure that the NTCP got the drugs it needed. What has happened to change the situation? Apparently, funds are available now because of DOTS, because DOTS is considered fund-worthy.

B. Issues related to appropriateness: Clinical Efficacy I Safety of Regimen and drug resistance A large number of trials have been conducted the world over, comparing new regimen against well-established ones. I have not looked at the entire literature, and I do not think it would be a simple matter to say which would be the "most effective" or "best" regimen even if one did an exhaustive literature search. Going by what review articles and text books seem to conclude, certain general guidelines are available: • The duration of chemotherapy can be shortened mainly by use of bactericidal drugs like Pyrazinamide or Streptomycin in the intensive phase and Rifampicin as much as possible. • Use of multiple drugs in the intensive phase ensures killing of most of the bacteria —a sterilising effect,

that overcomes any single-drug resistant organisms present. This also cures significant numbers of those who default after 2-3 months - the commonest phase for default. • The efficacy of regimen using Rifampicin till the end is more than those using only in the intensive phase. • The toxicity can be minimised without significantly affecting efficacy, using intermittent regimen thrice or twice weekly instead of daily. • Directly observed therapy; particularly when used with legal regulatory mechanisms can achieve near complete cure rates. • Under test conditions, un-supervised regimen reach an efficacy of 70% (cure rate), with short course regimen doing significantly better. Under field conditions in the government programme the cure rates in India have been in the range of25-30% ("standard" or long course chemotherapy) or 40-45% ("short course" Chemotherapy). Under programme conditions in NGO projects, the best cure rates are in the region of 70%, with 45-60% being more usual. • Regimen containing thiacetazone are the cheapest, but least tolerated. • Intermittent regimen should be trusted only when supervised. • Multi-drug .resistant TB is still relatively rare in India (up to 2-3% in limited pockets). • Untimely total drug withdrawal, if abrupt (as in default), is probably far less likely to induce drug resistance than when patients keep changing doctors and regimen, or when doctors add-on or subtract one drug at a time from an on-going regimen. Even if no one regimen is the "best", a number of regimen based on above principles can be expected to be reasonably effective. As such, there is no definite ground for doubting the efficacy of the WHO-sponsored DOT regimen (especially as compared to other regimen). Since the DOT regimen use intermittent dosing, the toxicity should .00 less than in the case of daily dose regimen. The assumption of efficacy, of course, is that it will work in field conditions. The other question that can be asked is whether SCC is any better than conventional regimen lasting a year or more. Common sense tells us that any patient will prefer a shorter course of swallowing pills. Surprisingly, the difference in completion rates between SCC and conventional regimen is not as great as one would expect. This is because, as mentioned above, the bulk of default occurs at the second and third months of treatment, affecting both alike. Here again, SCC should do better, since SCC

defaulters are at least theoretically less likely to relapse than those on fewer drugs in the intensive phase. The only case that remains to be understood is the relative efficacy of the alternative R1 (2ETHl10TH) and SCC, since the former is significantly less expensive. To the best of my knowledge, few authorities today will doubt the greater efficacy of regimen using rifampicin and pyrazinamide. (However, please also see Dr. Jhunjhunwala's paper presented in VHAI's National Consultation on Tuberculosis in 1994, Effectiveness of the RNTCP. He attempts to trace the basis of NTCP's acceptance of the greater cost-effectiveness of SCC as compared to standard regimen, and points to the possibility of undue bias).

C. Operational Considerations Operational considerations arise both at the level of improving diagnostics to increase the proportion of smear positive patients, an avowed objective of RNTCP, and in terms of the feasibility of DOTS under programme conditions. 1. Improved Diagnostics: There are several stumbling blocks in improving diagnostics. Microscopes of the required quality are simply not available in a large proportion of PHCs. Laboratory technicians are often not posted or not available sometimes because there are not enough funds available to fill up posts. Most new recruits are utterly inexperienced in the preparing of good quality smears which are a must for a satisfactory diagnosis. It is not uncommon for problems with laboratory supplies to hold up work. Similarly, servicing and maintaining microscopes is a headache. Questions of sincerity of work are well-known. It is difficult to envisage a situation where all this can improve under programme conditions even if pilot studies have done well. Fortunately, there are no proposals to increase availability of X-ray units! 2. The feasibility of DOTS under programme conditions: (Adequate supply of drugs is probably the most important question here, and it would be easy to give reasons why drugs would not be available. However, I will avoid getting into that area, and go ahead assuming this will be taken care of since, otherwise, there would be no point discussing any other issue.) Since DOTS regimen is not intrinsically more expensive (than other currently recommended SCC regimen), or any less efficacious in theory, the major element that remains to be understood is the operation feasibility. Will it work under programme conditions? We may try to find answers to this from three sources: a. The experience of other countries using DOTS and the applicability of this experience to the Indian situation.

b. The limited experience with WHO-sponsored DOTS in India. c. Our horse-sense. a. The experience of other countries: Since 1995, the WHO has been publishing annually "The WHO Report on the Tuberculosis Epidemic", (to the best of my knowledge). Three reports have so far appeared. They are not "official" WHO documents but are slickly produced, meant for publicity, and devoted to one theme - "To stop TB, use DOTS". They contain signed statements by the Director General of WHO, and the director of WHO's Global TB Programme, including reports of progress of different countries and regions (in the style of UNICEF's annual The State of the Health of Children). Every report has a sizeable section on funding aspects, and openly exhorts "big" donors to come forward and fund the programme. Since the entire thrust is on DOTS, we can safely assume that we will find the best arguments for DOTS in these reports. Indeed, the commitment to the cause of TB is touching, and undoubtedly based on sincere belief that TB must be stopped. The DOTS success story starts from Tanzania through the efforts of a Dutch doctor, Dr. Stryblo who himself suffered from TB, and then decided to take the bull by its horns. He persuaded the Tanzanian government in 1977 to introduce DOTS (2HERZ / 4H2 R2) in two districts, and upon success, in the rest of Tanzania, and later in Nicaragua and seven other African countries, all successfully (figures not given except that cure rate went up from 43% in 1983 to 90% in 1990 in Tanzania) going on at present. (At another point in the report, we have a list of countries where the majority of patients are currently on DOTS: Algeria, Benin, Botswana, Cote de Ivorie, Guinea, Lesotho, Malawi, Mozambique, Nicaragua, Peru, Senegal, Tanzania and Vietnam). The other success story comes from New York City (and, going by other paper and reports, a number of large US cities). Beginning with compliance rates of 11% (in Harlem hospitals) in the '80s, New York began serious use of DOTS in 1992, and has reported reasonable success. Denver and Baltimore are other cities that successfully implemented DOTS, under circumstances and using intervention strategies that may not be applicable to our situation. The success story most likely to be of interest to us is that of China. Following "privatisation" of its barefoot doctors in early 1980s (thereafter they were not financed by government, but were required to raise funds from their clients, the people), the government health department neither controlled nor funded the treatment of TB (a story with heart-rending parallels in Eastern Europe and erstwhile USSR), with the result that TB patients had to

fend for themselves with barefoot private practitioners who had no real interest in them. This led to a 'rapid deterioration in the TB situation, which was jointly \ reviewed by the WHO, the World Bank and the Chinese government in 1990. DOTS followed, with the government borrowing money for drugs from the World Bank and providing cash incentives to the barefoot doctors for detecting new cases and for cases completing treatment. The pilot projects covered 2 million populations and achieved over 90% cure rates. The barefoot doctors constitute the "outreach" primary health care system in China which has had a better success over the years in many health areas than we have had in India. Three "latest" success stories are from Guinea, Peru and Bangladesh. Apparently, none of them had a National TB control programme or policy, but put up one with-the help of the WHO and the World Bank very recently, the backbone of which is DOTS. All report 80% cure rates in regions where DOTS is being used. Similar successes are heard of in Vietnam. The Indian pilot projects in Delhi, Bombay and Gujarat (Mehsana District) are also quoted as "doubling their previous cure rates". While pilot projects succeeded in Bangladesh, a string of practical field-level problems, and the question of funds, are probably slowing down the programme there, despite strong foreign financial and technical inputs. The 1997 report over the TB epidemic lists 13 countries as carrying the main burden of TB, accounting for 75% of all TB cases. India heads the list with an estimated incidence of about 2.1 million annually, followed by China (1 .m), Pakistan, Bangladesh, Thailand, Indonesia/Philippines, Brazil, Mexico, Russia, Ethiopia, Zaire and South Africa. In ten of these countries, there is no National TB Programme or policy (only India, Russia and Thailand have long-established National Programmes for TB control). DOTS has just gone past the pilot phase in only 5 of these countries, the maximum population covered by DOTS being in China (1, 60,000 patients covered) -. It may be noted that none of these countries figured in the list where majority of patients are covered by DOTS. In essence therefore, all success stories are from either urban centres or from intensive pilot projects. The success under programme conditions is yet unknown, and definitely not from situations similar to the Indian. b. The Indian Experience with DOTS: Unfortunately, at the time of writing this, I have no access to first hand documents or information about the Indian pilot -" projects testing out DOTS. I do not have access to. Dr. Bannerji's or VHAI's expert committee's detailed critique of the RNTCP, only to the summary version of the expert committee report, and articles in HFM

(besides the report on the National Consultation on Tuberculosis of1994). These documents do give us some idea about how DOTS gained backdoor entry into India, by passing available expertise, and how the pilot projects are heavily supported and their reports inconsistent. The pilot studies are very recent, and I do not know if they have been published. Since the VHAI documents I have are widely available I will not comment on them, except to say that none of them are appreciative of DOTS. I will only add a bit of information that may amount to little more than hearsay. I have been trying to keep abreast of the progress of DOTS through officials connected with the Revised National TB Programme. My most frequently repeated question has been, "Is DOTS really working?" I am told that the only rural pilot project (Mehsana, in Gujarat) ran into serious problems, to the point where district health officials were repeatedly taken to task by "teams from Delhi" for poor performance, despite intensive monitoring from all levels. The project was heavily financed, with TB workers even being provided motorcycles to help supervision. There were also cash incentives. "How is the programme working in other areas where it has now been begun?" Well, so-so where the District Tuberculosis Officer is sincere and energetic, and very poor in most other places. (This is from informal chats among DTOs in a National TB Conference). Most of the health functionaries from the district level down are not enthused about DOTS. All this may be dismissed as ranting of disgruntled government officials, but it sounds quite canny and real. (VHAI's report on National Consultation on Tuberculosis has a number of articles exploring limitations of DOTS, and Dr. Jhunjhunwala's paper quoted above has a detailed analysis of the performance of the New Delhi pilot project). c. Our horse-sense: what do we feel about feasibility from what we know about ground realities: Similarly, our experience with how our existent PHC system functions should warn us that DOTS may not work. The argument used is, after all each worker has to look after a very small number of patients, so it is no real burden. This is true of multipurpose workers who do go to their field areas regularly and work sincerely, but what proportion do? And how many of them have been regularly meeting TB patients under the-existing programme anyway? How many Anganwadi workers or dais can be motivated to regularly look after some TB patients each, when their ability to perform in their own fields is constantly under doubt? In any case, why should they do "TB work"? What other disease or health problem has the system tackled on an' ongoing regular basis,

effectively? Why should it successfully happen in the case of TB, which barely enters the PHC system's field of vision? At best this is wishful thinking. That anyone in the health department believes this will happen cannot be true. At any rate, not for rural settings. I want to believe, I do, with all my heart, that we have a solution for TB. But I do not want to believe fairy tales.

What is at stake? There is in fact, one cause for real worry: By disturbing the present system in which at least a quarter of TB patients regularly come and take their drugs away, we are on one hand depriving these patients of treatment without being able to guarantee delivery of drugs to the doorstep (is this ethical?), and on the other, wiping off at one sweep the hard-earned gains of many years - in terms of patients taking responsibility for themselves and their disease. In our anxiety to achieve technological success, are we not reversing the process of empowerment that the existing programme engendered? And replacing it with a programme that is unlikely to solve the problem? This probably, is really what is at stake. The DOTS approach is indeed attractive, and one wishes it would work, but is it really going to? And it is here that the "new imperialism" hurts most — why should the anxiety of what will happen to New Yorkers in the Global TB Epidemic be allowed to disturb and distort a well-established TB programme, that was crying not for change, but to be given a fair chance — a sincere implementation, enough funds for drugs, freedom to adapt to local conditions? Is it too much, under the aegis of WHO, committed to support Primary Health Care, to ask, for funds with which to design and implement our own TB control programme? In a way, we are reaping the harvest of our own sins: had we managed our house better, been sincere in implementing our TB programme (or more precisely, our primary health care programme), it may have been us today, telling the world how to stop TB, just as we told the world once upon a time that sanatoria were out and domiciliary treatment was in. We would at least have been more fund-worthy. To conclude, we can safely predict that DOTS will not work the miracles it is said to. But it does seem to have done one good: brought TB back onto the agenda of our health planning.' We can now rethink the TB control strategy without feeling anachronistic. Since funds are hopefully to be available more readily, we must find ways to maximise their value for TB control. It may take 2-3 years for all to realise that DOTS is getting us nowhere. We have that much time to think and plan.

•

Views and opinions expressed in the bulletin are those of the authors and not necessarily of the organisation.


