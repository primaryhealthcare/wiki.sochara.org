---
title: "Rural Health Insurance Schemes, Sevagram (India) Experience"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Rural Health Insurance Schemes, Sevagram (India) Experience from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC177-178.pdf](http://www.mfcindia.org/mfcpdfs/MFC177-178.pdf)*

Title: Rural Health Insurance Schemes, Sevagram (India) Experience

Authors: Jajoo Ulhas

177-178 medico friend circle bulletin Nov / Dee 1991

FINANCING FOR PRIMARY HEALTH CARE- SEVAGRAM (INDIA) EXPERIENCE FROM VOLUNTARY SECTOR U. N. Jajoo INTRODUCTION

Economies of health care have gained special attention in recent years. Traditionally, health care belonged to the social service sector and therefore had to be administered by the State, as a welfare measure in India. The Investment by the State In health care Increased rapidly in the seventies. The State has been rethinking about, health finance and has been talking of alternatives. One of the alternative since the 6th Five Year Plan has been the State's open support to private health sector. As a result of unprecedented developments in regards to the new medical technology, the corporate Interests In health care has been glowing rapidly in last few years. The other alternative has been the privatisation of public provisions i.e. handing over public ,Institutions-to private bodies, permitting government doctors to Indulge In private practice and more recently In Maharashtra state, charging "fee for service" In government health Institutions. The trend has reflected in medical education_ too, by privatisation 'Capitation Fee' medical colleges. The consumer-households have to bear more and more burden by making direct payments (or through private Insurances) towards basic health care services in such capitalistic economies. An aura is created that it is Impossible for the State to sustain primary health care .cost within available resources. The bogee can be raised essentially due to our ignorance on two counts. Firstly, though Shore Committee recommended a desirable minimum, the financial outlay of the same could not be worked out because the State has' not Implemented all the requirements. Secondly, all is not just with regards to distribution of centralised State health resources. We live in a society where all are equal but some are more equal than others. We attempt here to extrapolate the expected primary health care cost from the experiences of voluntary health sector In Sevagram, India.

MATERIAL AND METHODS: The Kasturba Hospital, Sevagram (India), has developed a primary health care service to village in the vicinity.

It is an egalitarian, acceptable, accessible, affordable, effective, participatory, accountable and holistic health care delivery system for the rural poor…… (1) The micro experiment of the kind In Sevagram was set up In search of alternatives not merely for health financing but primarily towards more just health care. The Kasturba Hospital, Sevagram Is 501 bedded hospital attached to a medical college. A private trust-Kasturba Health Society- runs this college and hospital and shares 25% of the expenditure, while the rest 75% funds come from the State and Central government. Since the hospital is utilised for teaching undergraduates and postgraduates, all norms with regards to staff pattern and facilities, laid down by Medical Council of India for teaching and research, are fulfilled by the hospital. The cost of running such a specialised health care hospital is expected to be much more than what one would expect from a purely service oriented hospital. An area encompassing 23 villages (19,500 population), within a radius of 15 kilometers from Sevagram Is covered under the rural health Insurance scheme run by the Kasturba Hospital- a fact that speaks of credibility the hospital services enjoy amongst the rural folk. AII out-door medical consultations from the project area do not reach Kasturba Hospital and prefer to go 'to local private practitioners to get a tonic or prick of their choice - a demand which we do not comply? For door-step services in the project area, we have employed Innovative strategies so as to improve efficiency by utilising less formally skilled but well trained local human power. We adopt annual pulse Immunisation (2) and maternal risk surveillance strategy which does not necessitate rural location of ANM. The mobile health van staff (one ANM and a social worker) are placed at the base hospital and visit all the 23 villages for outreach services. OBSERVATIONS: The Kasturba Hospital is a referral hospital and deals with all illnesses, barring those requiring super specialisation' like cardiac/neuro/vascular surgery and haemodialysis.

The figures of last 5 years reveal an average hospital stay 01 a patient to be around 9 days and bed-occupancy of hospital of around 75%.The hospital provides drugs, material and food required during the hospital stay. Since Kasturba Hospital caters to referred patients from the entire district and also patients from nearby Andhra State, the pattern of hospitalisation from the project area qualitatively differs. All Indoor hospitalisations from project area occur at Kasturba Hospital. The indoor hospitalisation from the project area has an average stay of 7.5 days. The data of last 10 years reveal that out of every 11 people one utilise Indoor hospital service in a year. Two thirds of the indoor hospital load is shared by unforeseen and emergency illnesses while one third are foreseen hospitalisations like normal delivery, cataract, hernia, hydrocele etc. The outdoor service utilisation from the project population has a frequency of one attendance per person per year. The hospital billing pattern is not based on systematic cost analysis but follows a,' rough estimate of the market value. The hospital bill therefore cannot be relied upon for the health financing calculations. We have used total recurring expenditure for this purpose. FINANCIAL ANALYSIS: The data for the last 5 years (Table I) reveals that the hospital cost per bed per day has risen from As.65 in 1986-67 to As.130 In the year 1990:91. The salary head expenditure has shared the maximum chunk and ranged from 61 to 74% while the material requirement (drug, food etc) ranged between 20 to 31% of the total cost. The recurring expenditure on maintenance, office, travel etc. did not exceed 10 percent. With an average hospital stay of 9 days per patient, the hospitalisation cost works out to Rs.585 in 198687 to As.1170 in 1990-91. However, these figures cannot be extrapolated to a service oriented hospital simply because it Includes expenditure incurred on non-clinical doctors and staff who work for the medical college and stipend for postgraduate resident students. After pruning off the salary head accordingly, the cost analysis for a service oriented referral hospital ranges from Rs.54 in 1986-87 to As.106 per bed per day In the year 199.0-91.11 one keeps in mind that for every 11 people insured, one person utilised Indoor facility for 7.5 days in a year from the project area, the hospital cost works out to As.405 per patient in 1986-87 to'As.795 in the year 1990-91.1n terms of per capita cost the figures would be As.37 in 1986-87 escalating to Rs.72 per capita per year In 1990-91.The Indepth analysis further reveals that one had spent, in 1986 through 1990,54 to 68%on humanpower,24 to 37% on materials and 6 to 12% on maintenance etc. It must be highlighted that financial analysis of this kind is an over-estimate for a service oriented primary health care base hospital due to the following: i) A hospital linked to undergraduate and postgraduate medical training and research is heavily equipped by human power (doctors and

non-doctors) and material as such an educational institute has to meet the recommendations of the Medical Council of India. ii) With complete utilisation of available beds (75 to 100%) the overall cost is expected to drop by 1/4th and iii) The expenditure to “material” head includes the cost of food provided during hospitalisation. The expenditure during 1990-91 on outreach door-step health services at Sevagram project works out to be As.4.90 per capita per year (Table II).It includes As.0.88 for village health worker's drug kit,Rs.O.75 for transportation of mobile 'health team,As.1.85 towards salary of mobile health team members and As.1.42 on 'village health worker's remuneration. CONCLUSIONS: At 1990-91 prices, for good quality and just primary health care services, a provision of Rs. 77 per capita per year. (Rs 72 for hospitalisation and Rs.5 for door-step s ices) would be more than enough, provided the drain does not dry up by the time it reaches the 'Have Nots. One would expect at least one third of the total amount (As.25 per capita at .1990-91 prices) to be spent on drugs and material. With expected hospital stay of7.5 days and hospital admission rate of 90 per thousand per year, the base hospital would require 2 beds per thousand populations. It is interesting to note that the Government of India spent around As.90 per capita in the year 1990-91 on State health services (3), an amount enough to develop a just primary health care service. However, only Rs.2 per capita is provided to primary health care set up (Primary Health Center and Aural Hospital) towards-drugs and material cost. The abysmally low and partisan misdistribution of State resources speaks of what ails the system.1t is not the paucity of resources but a step motherly treatment to rural folk _that lies, behind the malady. SOME GUIDELINES FOR NATIONAL HEALTH SERVICES: The Sevagram experience throws light on the following guidelines for national health services:1) It is possible to offer just primary health to all within the existing resources, provided funds are locally available and locally governable. The decentralised set up, say up to the district level, enjoying autonomy in planning locally will tread a long way towards egalitarian health services. 2) The expertise of medical college hospital, if utilised to govern and supervise. State health care delivery, can divert the expenditure on medical education for improving primary health care services. 3) The Primary Health Centre (promotive & preventive role) if upgraded for curative role, will cut down unnecessary duplication of expenditure on rural hospitals (curative role in the present set up) and in addition make the services acceptable and accessible to rural poor. It would provide an opportunity to utilise credibility earned through curative services for facilitating preventive/promotive programmes.

4) The privatisation of public health services offers an opportunity for misutilisation of State health resources for private Interests. The avoidable drain must be plugged. REFERENCES: 1. Jajoo U.N.: Rural Health Insurance Scheme, Sevagram (India) Experience:

World Health Forum. (Sent for publication) 2. Jajoo U.N. et al: Annual Cluster (pulse) immunization experience: in villages near Sevagram, India: Journal of Tropical Medicine and Hygiene: 86; 277-280 (1985) 3. Duggal Ravi: State Health Financing and Health Care Service in India: Personal Communication from the Foundation of Research in Community Health, 84-A., R.G. Thadani Marg, Worli, Bombay 400018, India.

TABLE I COST ANALYSIS (Recurring). KASTURBA HOSPITAL.SEVAQRAM. ANNUAL EXPENDITURE (in Rupees).

BED STRENGTH: 501

Head of Expenditure * SALARY - Hospital Doctors -College Doctors -Student doctors -Non-doctor staff

1986-87

1987-88

1988-89

1989-90

1990-91

18,55,989 9.04,692 10,61,496 37,20,980

19,72,826 10,00,607 14,01,796 44,18,180

11,26,563 10,85,478 14,23,820 48,06,505

51,00,134 16,16,257 20,58,567 64,36,640

40,66,674 18,85,316 24,95,548 .79,31,103

32,55,719

34,31,837

42,26,261

41,85,029

53,55,059

9,76,981

14.02.697

11,26,088

11,16,685

16,73,491

TOTAL

1,17,75,857

1,36,27,943

1,37,94,715

2,05,13,312

2,34,07,192

Annual bed occupancy

67.4% 9.1 399 45.1

75.7% 9.1 432 50.9

76.4% 8.9 446 52.7

71.3% 9.0 388 48.1

75% 8.9 426 51.5

Cost per bed per day in medical college referral hospital

65.5

76

76.5

114

130

Cost per bed per day In service oriented hospital

54

63

63

94

106

Cost per indoor admission In service oriented hospital, for an average stay of 7.5 days

405

472

472

Annual per capita cost (1 admission per 11 people)

37

43

43

* MATERIAL & SUPPLY (Drug, food, other)

* MAINTENANCE & REPAIRS, OFFICE &

MISCELLANEOUS.

Average hospital stay Average OPD per day Average Indoor admission / day

705

64

795

72

TABLE II EXPENDITURE ON PERIPHERAL HEALTH SERVICE.HEALTH INSURANCE SCHEME KASTURBA HOSPITAL SEVAGRAM

YEAR 1990-91

POPULATION COVERED

VILLAGES 19,457

CATERED 23

HEAD OF EXPENDITURE

ANNUAL EXPENDITURE (In rupees)

PER CAPITA EXPENDITURE

*SALARY ANM (Rs.1800/month)

1.85

21,600 14,400

Social worker (Rs.12002/month) Village health worker (average.Rs.100/month)

27,600

1.42

* VILLAGE HEALTH WORKER'S DRUG KIT

17,250

0.88

(30 kms/day for 7 times a year, per village @ Rs.3/km)

14,490

0.75

TOTAL

95,340

4.90

* TRANSPORT

RURAL HEALTH INSURANCE SCHEME, SEVAGRAM (INDIA) EXPERIENCE JAJOO U.N. The Kasturba hospital, Sevagram, Wardha is attached to a medical college and has helped to initiate an outreach health programme to the villages nearby. The health Insurance scheme has evolved to its present form through a series of changes and adaptations which were largely based on the experiences of past approaches and their failure (1, 2, 3, 4). The realisation that blind charity corrupts the people compelled the organisers to develop a strategy that attempts towards community involvement, which at present expresses in the form of financial contribution from the community and people's participation in decision making and in supervision of village health worker's (VHW) performance. HEALTH INSURANCE CONTRIBUTION

The contributions towards health Insurance are made mostly in kind because it is easier for the villagers to contribute Jowar- Sorghum at the harvesting time. This makes into a village –fund to be utilised to support outreach health programme, and other development activities. The fund acts as a prepayment scheme, subscription entitlements include free primary health care and subsidised referral care. The observation that the village is not a homogeneous community and that more availability of health care facility does not necessarily make it accessible to the poorest

section paved the way for collecting health insurance contribution according to capacity. The purpose of collecting village fund was not to raise financial support to the outreach programme but to inculcate and generate a demand for qualitative service since they have paid for it. In fact, it was a tradition among villagers to collect voluntary contribution graded according to capacity, be it for a temple, a religious village function or a sport competition. At present, the health insurance contribution from the lowest income group (landless labourers) is 8 'payali' of Sorghum per family per year {equivalent to Rs; 16/- at current rate).The land owners contribute in addition 2 'payali' per acre of land holding. The collection is done by the village health worker at a prescribed site on a preinformed day. Those who fail to enroll themselves on the said day cannot avail the health insurance facility for that year. The village, fund covers the drug cost of the village kit, transportation cost of the mobile team and remaining balance goes to the VHW for his/her remuneration. STRATEGY The hospital adopted the village only if 75% or more of the poor community agreed to

enroll under the Health Insurance scheme. We regarded It to be a sign of acceptability of health scheme over the years.1f the membership dropped below 75% in a year during anyone year, the scheme for the village was withdrawn. The drop in membership offered an occasion to look back, open up dialogue with the people and take corrective action. In our experience, drop in enrolment occured due to strong political polarisation in the village where warring groups did not come together, and we got identified with a political faction. In a village

where we consciously avoided getting identified with partisan politics, the project withdrawal due to lack of support usually had the effect of motivating the community to reorganise and in most cases reinstate the Insurance scheme. The alternatives in operational field research (one that needs less human power and offers better quality services) were worked out for community vaccination and maternal care. The annual pulse Immunisation strategy(5) needs only four village visits a year and achieves herd Immunity (95% adequate Immunisation), while the maternal surveillance strategy picks up risk mothers at the earliest and requires three village visits a year. The outreach village visits are conducted from the hospital base. A team comprising of one ANM and one social worker can look after 20 villages on a regular basis. The location of the hospital and its approachability (by road) we found, was an Important consideration in people's view point. It did not matter for the villages within five kms radius. For a distant village we received offers of village adoption only if there were regulate road-way services plying to Sevagram. THE BENEFITS To the Insured persons, hospital offers free indoor treatment for unexpected Illness and 75% subsidy for planned/expected episodes of III health such as normal pregnancy, cataract, hernia etc. The free indoor service (the service that matters to the people) assures accessibility of hospital to the poor. The outdoor service is available at 75% subsidised charges. The nonmembers are free to avail medical facilities but at full hospital charges. The mobile health team (ANM, Social worker, VHW) provides maternal child health care services at the door-step. THE HUMANPOWER ROLE The VJ:!W provides symptomatic drug treatment, offers preventive and promotive care with the help of visiting health team members, refers hospitable patients and acts, as a link between the hospital and the community for other village development activities. The ANM with the social worker organises peripheral visits for vaccination and also provides maternal-child health care. They are the ones who follow all indoor admissions and see to it that the commitments are fulfilled. The doctor in charge has the role of supervision, treatment of patients in the hospital, conducting education slideshows, coordinating

village meetings late in the evening for health/non-health development activities and training VHW as per need. THE DECISION MAKING A village meeting (Gram Sabha) is held every year before Jowar collection. The meeting is coordinated by the doctor Incharge and attended by ANM, Social worker and VHW (the mobile team members). It is an occasion to reflect upon performance of the health delivery system. The meeting often stormy- serves dual purpose of evaluating performance of health structure and enacting disciplinary action on Irregularities committed by villagers themselves. The village meeting helps to facilitate communication between the health team and beneficiaries on one hand while on the other it helps villagers to command control an VHW and the health team. On occasions the Gram Sabha has also decided to change their Village Health Worker. THE FUNDING The hospital is funded by the Government to meet its 75% requirement. The remaining 25% is received from Kasturba Health Society (voluntary organisation that governs the college and hospital) through hospital charges and donations. The outreach doorstep services are totally supported through the village fund. THE ACHIEVEMENTS 1. Acceptability & affordability to the poor

Acceptability of health Insurance scheme reflects from the fact that more than 75% of the villagers have enrolled themselves over the last 10 years in the scheme. The poor section of village community joins readily than the economically better ones. We no more require “Cinema" for health education. The villagers attend meetings and mutual discussion, we found, was a better communication tool. 2. Accessibility and quality The community survey undertaken In the Insured villages reveal, but for unexpected and compelling reasons, all indoor hospitalisation invariably occur I n Kasturba Hospital. It speaks of the accessibility and credibility with regard to quality care that the hospital provides. 3. Effectivity There is no vaccine preventable Illness (measles, poliomyelitis, diphtheria, whooping cough tetanus) reported in children and in mothers after mass Immunization, by annual pulse approach, was undertaken. There is no maternal death in last 10years. The perinatal mortality has shown a steep fall. 4. Awareness building, participatory and accountability The stormy encounters during yearly village meetings asking to fulfill commitments that the scheme offered to the beneficiary

population, tells us the awareness of rights Inculcated deep in the minds. The village meetings make health providers answerable to people's audit. 5. Holistic Health The health team having earned credibility over years is now placed In a privileged position and Is regarded as guide I counselor for wider dimensions of health which deal with priority needs of people e.g. drinking water facility, Irrigation facility, Income generation programmes like dairy development, horticulture, sericulture, antiliquor programmes etc. THE DREAM STILL ELUDES US Though achievements are encouraging, the dream still eludes us. We succeeded in reducing natal and perinatal mortality by making hospital services accessible but preventable deaths during delivery and immediate post-partum period do occur. The analysis revealed that prevention of these deaths was beyond our remedial measures e.g. most of them due to lack of communication facility and transport services in emergency situation. The under-five deaths consequent to malnutrition, particularly in female babies, continue to occur. The nutritional education given to mothers, in low socio-economic situation, is far from feasible. The prolonged hospitalisation of severely malnourished babies Is too much demanding for the mother who cannot afford to lose her dally wage. The concept that this Illness is caused by evil spirit's wrath still hangs over the minds of people. The neglect of female baby turns out as the last straw on the camel's back. We too find ourselves helpless before the 'evil' spirit of poverty. The long and forceful health education drive notwithstanding, the mad demand for tonic bottles and Injection continue to hammer us. When we do not comply, villagers satisfy themselves by going to private practitioner in the vicinity. Though we could put a step forward and Inculcate community Involvement, active community participation on health Issues eludes us. We consider it to be natural on three counts. Caution In approach to community financing

Firstly, health stands at the lowest rung of priority need. Secondly, the need is short lasting and lastly the need is felt by Individuals and not by the community as a whole, at a given point of time. The community action on health Issues, we found, hard to emerge. The dole of egalitarian health care by an above down approach is not and should not be the philosophy of primary health care. The people should own their health services. Given the prevailing modes of production, structure of society and distribution of political power, such declarations sound very unreal. The microexperiment like that In Sevagram, therefore, cannot boast to create an oasis wherein the right to health can be exercised from below. Within the given social limitations, it has attempted to raise a replicable model. THE WISDOM The strategy of community financing (prepayment for health Insurance) according to capacity has paid the following dividends. 1. It Increased accessibility of basic health services and promoted greater concern for health in the community. 2. It ensured that services were acceptable and responded to the priorities as judged by the community. 3. It generated the concept of right to demand a qualitative health care by the beneficiaries and kept the service providers on their toes. 4. It offered an alternative payment mechanism to persons who need service but were unable to pay i.e. risk sharing. 5. It stimulated self confidence, organisational ability and paved the way for other development activities through community involvement. The literature (6, 7) however, raises few cautions n approach to community financing which we found being Inapplicable In our setting. Our Experience

1. It does little to promote equality, can place great burden on the poor/sick and suffers from 'adverse selection'.

1. The principle of contribution according to

2. It covers small proportion of cost.

2. The primary health care is the constitutional right of

3. It lacks stability of revenue and needs a high

degree of external support to mobilise and sustain community efforts.

capacity, but services according to need developed an egalitarian system which does not suffer from 'adverse selection'. every citizen. To cover the entire' cost from the community should never be the primary concern of pro-people scheme.

3.

The desirability & affordability of the scheme 4. It favours creation of those kinds of health

facilities for which there is high local demand rather than meeting professionally perceived needs.

5.

It carries the danger of excessive use of facilities

6. It is not easy to 'sell' among poor people

offered stability of revenue and willing community involvement on sustainable basis.

4. Since the hospital has to raise 25% of expenditure from a highly subsidised or free Indoor services, 'high tech' medicine, unless cost effective, cannot become the professional choice. Since 75% of funds are assured to the professionals, they do not succumb to the unjust local demands like that of tonic or pricks.

5. The facilities are primarily controlled professionals and not by the people thus avoiding their excessive use. 6. The desirable and affordable scheme does not require to be sold.

7. It benefits the community more than

individuals there may be reluctance to participate.

7. Our Insurance scheme benefits Individuals more by sharing risk and thus finds no reluctance to participants.

THE HIGHLIGHTS

a) The Insurance system evolved at Sevagram was a tool to develop an egalitarian and just health care delivery system and not an attempt towards selfreliance. Those who preach of self-reliance in health care should first answer the following questions. i) What is the minimum financial need to raise a just and equality primary health care programme? ii) What percentage of poor man's Income should be spent on primary health care? iii) What proportion of total expenditure should come from the State and private resources? b) The empowerment of people is the key towards propeople health service. The smallest participatory unit in a decentralised structure would be, we found, no less than a Gram Sabha (Village Council). The decisions therein should be taken by no lees majority than say 75% and presumably by consensus. c) The unregulated private sector is exploitative. A good quality social security (like our health Insurance 8cheme) has the potential of taking clients from the private sector and be cost effective to the beneficiary. GUIDELINES FOR NATIONAL HEALTH INSURANCE SCHEME 1. Primary health care services to be accessible and affordable to the poor, must be socially financed (i.e. pay ment by risk sharing) and curative service be provided free 10 the beneficiary. 2. For the goal of health care to all, the social finance will have to be raised according to the capacity of Individual family to contribute (be that by direct or indirect taxation) and benefits are offered according to the need. 3. For nationalised service sector to be directly accountable to the people they serve, the people must be empowered. The power flows through financial control and by performance evaluation of 8ervants. The local public body in a decentralised set up is trusted to undertake this responsibility.

4. The private sector (professionals, drug Industry, investment industry) needs regulation. For the hospitalised service the private sector may be involved on a fee for service basis. The out patient services may be opened up to private sector but within universal medical Insurance financed by the local government. No private medical Insurance be allowed.

REFERENCES Jajoo U. N. : Whim the search began: MGIMS, Sevagram Dr. Nayar. S 1984. Jajoo U.N. : Health is not villagers chief priority World Health Forum: 4:385-387 (1983) Jajoo U.N. : Health education alone can do little : World Health Forum; 8:220222(1985) Jajoo U.N. Gupta. O.P. Jain A.P. : Rural Health Services: Towards a new strategy? : World Health Forum; 6:150-152 (1985) Jajoo U.N. et al. : Annual cluster (pulse) Immunisation experience In villages near Sevagram, India: Journal of Tropical Medicine and Hygiene: 88:277-280(1985). Brain A.S. and Dua. A.: Community financing in developing countries, the potential of the health sector: Health Policy and Planning: 3 (2)", 95 108, 1988 Stinton W.: Community Financing of Primary Health Care: Washington D.C.: American Public Health Association, 1982.

ENDING THE UNDERFINANCING OF PRIMARY HEALTH CARE Ravi Duggal Before we discuss the fiscal Issues related to primary health care (PHC) It Is Important to clarify what we understand by PHC. It Is not what Is practiced In the country today: A Primary Health Center(PHC) for 30,000 population (that too only In a few States) with six beds for never-turning -up maternity and emergency cases, one doctor (2 In older PHCs),a few Inadequately trained paramedics for outreach services through sub-centers(about 5 per PHC) mostly doing family planning promotion and case-netting; let alone the paramedics meeting the routine curative demand at the sub-center or door step, even the PHCs don't attract clientele for curative care. The PHC we have in mind is closer to what the Bhore Committee had recommended on the eve of our political independence (Bhore, 1946). It was to be a universal comprehensive health care service freely accessible to all without any cost to the user, with the lowest health care delivery unit serving a 10,000 to 20,000 population with 75 beds, 6 doctors (Including medical, surgical, and ob. gy. specialists), 6 public health nurses, 2 sanitary Inspectors, 2 health assistants, 20 nurses, 3 hospital social workers, 6 midwives, 3 compounders and others. With this structure, curative, preventive and promotive services would be provided as an integrated service by full-time salaried staff who would reside at the PHC site. Hence, when we talk of PHC we mean his minimum decent standard universal health care system in both rural and urban areas, very different from the existing PHC In rural areas and hospital-based services in urban areas. Health Care Financing

Besides the network of public health services in both rural and urban areas there is a vast spread of a variety of private health care services (see Jesanl1991 and DuggaI1991).while the former are financed from the State revenues, the latter are directly financed by the households. The total health care expenditure in the country is estimated at over 6% of the gross national product and only one-fifth of this is by the State (Duggal and Amin, 1989). This shows that the health expenditure In India Is of a similar intensity when compared to even the developed countries. However, unlike the developed countries the public finance component of the health sector constitutes a much smaller proportion In India -20% In India as compared to 41% In USA,87% in UK,91%ln Sweden and 77% In Germany (Schieber and Poullier,1990). The financing of the private health sector by households goes directly to practitioners, hospitals and the drug industry and trade-this money is predominantly fetching curative care and diagnostic and related services; preventive and promotive care is Insignificant. To what end are public finances in the health sector directed? Public health services are vastly different in the urban and rural areas. In the urban areas, hospital services and medical education are the priority Items of State health care spending. Thus, even In Maharashtra, which has one of the best developed rural health care Infrastructures, the share of the urban health sector, is 57%, of which 49% is on hospitals services and 40% on

medical education. The smaller proportion of funds left over for rural health care which has to cater to 65% of the State's population is hogged away by family planning (22%) and national disease control programmes (71 %).Curative care does not even get lip sympathy (4%) (Calculated from Performance budget 1990-91 for the year 1988-89, actual). Today, in the country as a whole Rs.57, 000 million Is spent by the Central and State governments on health care services (excluding water supply) (GOI, 1990).Of this, 24% goes for curative care (hospital and clinical services) and of this 84% is spent In urban areas and only 16% in rural areas when the residents in the two areas are 25% and 75% respectively. In addition local governments in urban areas spend an estimated Rs.14, 000 million (Estimated from NIUA, 1989). Financing primary health care At the national level 'only a proxy estimate of expenditure on primary health care can be made. Primary health expenditure is that which is incurred through PHCs and subcenters. Under the national accounts the subheads that include this expenditure are: (a) The disease control programmes (b) Rural family welfare services (including proportionate compensation for sterilization) (c) Minimum needs program (d) Proportion of 'medical relief' Besides there is also expenditure on rural water supply and sanitation which we will not consider here because though this Item Is accounted under the health sector, the expenditure is incurred by ministries other than health in most States. On the basis of the latest data available this PHC component works out to 25.5% of health expenditure (excluding water supply).This means, the direct expenditure on rural health care services (synonymous with PHC In India) is one fourth of total health expenditure when three-fourths of the population lives In the rural areas. Object-wise breakdown of data from Maharashtra state (this is not available at the national level) for 1988-89 shows that 66% of this expenditure is on salaries and only 9% on materials and supplies (see Table No.1). So what does a typical PHC (In Maharashtra) serving 30,000 population get in terms of resources to deliver PHC? Each PHC and its sub-centers get approximately Rs. 700, 000. Of this roughly Rs.400, 000 comes under the national disease programme (malaria, leprosy, tuberculosis, blindness etc.), Rs.200, 000 under family planning (100% sub-center funding),

Rs.100, 000 for salary of one medical officer (if there is a 2nd doctor his salary would be under disease programmes) and for medical relief (curative care).The object-wise distribution of this expenditure and the per capita cost Is given In Table NO.2 What do we conclude from the above? Firstly, given the current prices of goods and services the resources made available to the PHC are grossly Inadequate. What the PHC gets from the national public health expenditure (excluding water supply) Is only one-third of the resources on a per capita basis (the remaining of course going to the urban areas). Secondly, the mix of services available at the PHC and its sub- centers is highly curative-prejudiced i.e. so called preventive- promotive oriented. Thirdly, salaries take away over two-thirds of the resources i.e. effective benefits that can accrue to people is very small, hence very few people from the catchments population benefit. Fourthly, field level experience shows that the largest proportion of time of all health service employees is spent on canvassing for family planning programmes and fulfilling its targets of cases. Fifthly, the staff composition to the population It Is supposed to serve is grossly inadequate if a universal comprehensive health care service has to be delivered. And sixthly, health care priorities are imposed from above and hence the programme components have very little relevance for local situation of the PHC. In addition there are other problems, which we will not enlist here, that prevent the functioning of PHC through the PHC and its sub-centers. What can be done to remedy the situation? Today the national public health expenditure is about Rs.90 per capita (for fiscal year 1990-91).Onefourth of this is on water supply and sanitation, over which the health ministries have no control. The health ministries hence have Rs.7.50 per capita at their disposal. We have also seen that roughly one third of this goes for rural health care under the present PHC structure and that it is very inadequate to meet the demands of the rural population. How do we remedy this so that the underprivileged masses get access to a health care system of their choice? There can be various solutions ranging from the rabidly radical to the opulent liberal. We are presenting here a macro fiscal solution in the context of a structure of a demo cratic decentralised (local government) set up. This suggestion does not emerge out of any specific research endeavour and neither is it a figment of the author's Imagination. It is based on a limited experience of the author's understanding of various health care systems globally and the field experiences In the Indian (specifically Maharashtra) context. It is being suggested in the hope that a debate will take place both within the mfc and outside. The suggestion is that the available resources (Rs.67.50 per capita) should be distributed equally among all health delivery units on the basis of the population it serves (This does not justify that the present resources allocation is adequate). Hence in the given set up if the PHC is serving a 30,000

population then it must be given Rs.67.50 per person i.e. a total of Rs.20.25 Lakhs instead of the present Rs.7 lakhs; i.e. nearly thrice of what it gets presently. The question here is who would be responsible for the use of these funds. As mentioned earlier these funds will have to be administered under a local government. The latter, being representatives of the local people, would plan their own health services based on their needs? Of course! Politics does not work like that in practice. There will be vested Interests (Including the private health sector) out to grab this money: misappropriation will still be there. But there will be a difference. All this will take place under the local people's eyes. It will be within their sphere of the world and hence they will be in a better position to question it and raise Issues about it, and demand greater accountability. The other question is will such local government be able to organize health care services? They might decide to spend half the amount on constructing a 50 bedded hospital building and some local politician-cum-contractor might make a couple of lakh rupees on it. Some other unit might decide to appoint one doctor for every 2000 population. Another unit may want to strengthen the existing Infrastructure by improving materials and supplies allocations. Still another unit might do something that our Imagination is unable to for see. The point we are trying to make Is that if State revenues (collected from people) are returned to them in an equitable ratio and they are left to plan 'democratically' their own political economy then with all controls for development vesting with them they will have to assume responsibility and demand accountability from their elected representatives-and they will be In direct contact with the power brokers to do this, unlike as at present where the power center Is the State and national capital. What will be the Impact of such a macro fiscal policy? (Please note that such a macro policy should cover the entire' development sector, i.e. besides health, education, agriculture and rural development, employment programs, small irrigation projects, drinking water supply, social welfare etc.) Firstly, the power equation will slide to lower levels and political struggle at the micro level will become more intense. Secondly, the bureaucracy at the Central and State Capitals and at regional and circle headquarters will have to shed its top heaviness-this is the central principle of decentralization. It must be added here that the overgrown bureaucracy will be the first bottle-neck in accepting such a solution. Thirdly, the urban areas, especially metropolitan cities will get far lesser resources in this arrangement and the city dwellers are a vocal lot. But decentralization of larger municipal bodies by having neighborhood councils for every 'n' population (say 30,000 to 50,000) should to some extent lessen the reaction of the urban folk. It is necessary to see that in development policies urban and rural areas are not differentiated-for example, having PHCs In rural areas and hospitals in urban areas. Fourthly, there will still be some structural changes In

local government functioning, Suppose, for the sake of the present discussion (in reality we will have to work out a model),we say that In rural areas the local council should be for every 20,000 population (10,OOO In desert and hilly areas) and In urban areas for every 40,000 population (60,000 in densely populated cities).This will not mean that all sectors devolved to the local government will be planned in the confines of Its population jurisdiction. Each sector has its own economy of scale and this will have to be kept in mind. Therefore, the local government may be able to have a 30 bedded hospital for primary care, maternity etc. at the 10,000 population level but a 100 bedded referral hospital with some specialist care may be possible only at the 100,000 population level. Hence a few local councils will have to come together to appropriately locate the referral center, share its costs and manage Its functioning the case of education, primary schools, secondary schools, high schools, colleges etc. will be planned according to its own economy of scales. So on and so forth. Thus for the purpose of collaboration and coordination higher level bodies of government ~He necessary, and therefore, under decentralization they don't become redundant they have a different and more useful role to play. But the principle should be that higher level bodies should evolve from below to meet the demands and needs at the people’s level. Finally, having-stronger local governments with larger fiscal and administrative autonomy would enable a large participation of common people in the, political, process and hence help empowerment of people. This wills in turn facilitate accountability of providers and Institutions to the people and also make social audit a reality.

References

1. Bhore, Joseph 1946: Health Survey and Development Committee Report, GOI, Delhi. 2. Duggal, Ravi and Amin, Suchetha, 1989: Cost of Health Care, FRCH, Bombay. 3. Duggal, Ravl, 1991: Private Health Expenditure In mfc bulletin 173-174, (July/Aug) 4. GOI, 1990: Indian Economic Statistics-Public Finance December 1990, DEA, Ministry of Finance, New Delhi 5. Jesani, Amar, 1991 :'Size of Private Sector In Health Care', in mfc bulletin 173-174, (July/Aug) 6. NIUA, 1989 : Upgrading Municipal Services, NIUA, New Delhi. 7. Schieber J. and Poullier J.P., 1990 : 'Overview of International Comparisons of Health Care Expenditure' In Health Care Systems In transition: OECD Social Policy Studies No.7 OECD: Paris.

Available Now! Medical Education : Re-Examined An anthology of papers and articles taking a critical look at our medical education and suggesting alternatives on some aspects. Available from: Price Medico friend circle Paper back – Rs 35.00 34, B Noshir Bharucha Marg, Hand-cover – 100.00 BOMBAY 400 007. Table I : Rural Health Services Expenditure In Maharashtra : 1988-89 * (Rs. In Million) Curative Services

Disease PHC Programmes MOs

Family Planning &

Immune. Total

29.20

582.27

61.09

149.12

Salaries

18.72

365.86

59.58

90.95

Material & Supplies 7.07

40.97

31.90

* In addition to the Programmes listed In this Table, rural areas health expenditure also Includes (for the same year) Rs.335.55 million on Minimum Needs Programme (setting up new or upgrading health Infrastructure) and Rs.114.51 million on 'compensation' paid for family planning acceptance and promotion. The total rural and urban health expenditure by the 1.Medical Services 2.Dlsease Programmes 3.Family Planning & Immunization

100,000

78.40

14.50

7.10

400,000

63.00

7.10

29.90

200,000

70.00

12.10

17.90

State government in 1988-89 was Rs.3, 531 million (excluding water supply, but Including Rs.577 million 'for health administration. Source : Performance Budget 1990-91 Public Health Department, Government of Maharashtra, 1990 Table 2 : Expenditure of a Typical PHC -1990 Prices (Maharashtra) Total Funds (Rs.)

Total Per capita (Rs.)

% Salary

% material & supp. % other

700,000

67.20

9.60

23.20

23.33

15.58

2.24

5.41

MEDICO FRIENDS CIRCLE BULLETIN Editorial Committee: Abhay Bang Anil Patel Binayak Sen Dhruv Mankad Dinesh Agarwal Padma Prakash Sathyamala Vimal Balsubrahmanyan Sham Ashtekar Anita Borkar (Editors) Editorial Office: C/o Abhivyakti. 41, Anandvan, Yeolekar Mala, Nasik-5

Inland (Rs.) a) Individual b) Institutional Asia (US $) Other Countries

Annual

Life

30 50 6 11

300 500 75 125

Please add Rs. 5 to the outstation Cheques. Cheques/M.O. to be sent in favour of MFC (Address, Dr. Anant Phadke, 50, LIC Colony, University Road, Pune 411 016, India.) Published by Sham Ashtekar for MFC and Printed at Impressive Impressions, Nasik.

Subscription/Circulation Enquiries:

Typesetted by Lagu Enterprises, 1, Shanti Niketan, Mahatma Nagar, Nasik – 4000 447.

Anil Pilgaonkar, 36-B Noshir Bharucha Marg, Bombay – 400 007

View and opinions expressed in the bulletin are those of the authors and not necessarily of the organisation

Subscription: Rates:


