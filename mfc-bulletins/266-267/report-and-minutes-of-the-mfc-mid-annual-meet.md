---
title: "Report and Minutes of the mfc Mid-Annual Meet"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Report and Minutes of the mfc Mid-Annual Meet from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC266-267.pdf](http://www.mfcindia.org/mfcpdfs/MFC266-267.pdf)*

Title: Report and Minutes of the mfc Mid-Annual Meet

Authors: Madhukar Pai,

medico friend circle bulletin 266 267

Nov-Dec. 1999

Health Implications of Ageing Udaya Shankar Mishra

Ageing: An Overview The phenomenon of ageing conceived in terms of chronological measurement has become an area of demographic research and studies of ageing in human population are of quite recent origin. Rapid social and economic changes are expected to have serious implications for the circumstances under which the future elderly will live. These socio-economic changes consist of family nucleation, average smaller number of children per couple, greater longevity, physical separation of parents from adult children as a result of rapid urbanisation and age selective rural-urban migration and also the changing values of the younger generation against the older one. Population ageing is an obvious consequence of the process of demographic transition. Being ahead in the process, the developed regions of the world have already been experiencing this situation and the developing world is well on its way to face a similar scenario. 'Though the proportion of elderly persons (defined in terms of aged 60 and above in a population) is low in some of the developing countries, the numbers of elderly persons in absolute numbers are more because of the large population base. , Projected increase in both the absolute and relative size of the elderly population in many third world countries is a subject of growing concern for social policy (Treas and Logue, 1986; Grigsby 1993; World Bank 1994). These increases are the result of changing fertility and mortality regimes over the last forty to fifty years. The combination of high fertility and falling mortality during much of the

present century ensures that there will be large and rapid increases in elderly population as successively larger cohorts enter the older ages. Further, the recent sharp declines in fertility ensure an increasing share of future elderly. Given that these demographic changes have been accompanied by rapid and profound socioeconomic change, cohorts have differed in their experience as they have aged. Despite growing awareness of recent and impending increases, few empirical assessments have been made of the accompanying compositional changes that might be expected in these elderly populations or to the historical and dynamic aspects of cohort succession which give rise to these changes. With the background of transient fertility and mortality trends, it is worthwhile to preface our discussion with an account of the structure and magnitude of the elderly. The number of elderly in the developing countries has been growing at a phenomenal rate to the extent that in 1990 the population 60 years old and over in the developing countries exceeded" that of the developed countries (estimated world total of 490 million). By 2030, that number will triple to 1.4billion. Most of this growth will take place in developing countries and over half of it in Asia (World Bank 1994). Obviously, the two major population giants of Asia, namely India (Irudaya Rajan, Mishra and Sarma, 1999) and China contribute a significant proportion of this growing elderly in future. Centre for Development Studies, Trivandrum. A Major part of this paper has been extracted from the book India's Elderly: Burden or Challenge, Sage Publications, 1999.

A detailed account of growing elderly population in India and its states (See Appendix Table A1) has been included. Ageing and its Health Implications The changing age structure of the population results in different patterns of ageing. Consequently, the pattern of death and disability due to ageing in a population would also vary. This ageing pattern causes differential health risks in a population as a result of the epidemiological transition process. Secondly, extended years of life unaccompanied with better physical well-being during later ages poses a greater demand for health services. Often, a high price is paid for ensuring disability free old age which is a result of postponing death with improvement in mortality. The following table shows the trend in expected years of life that an Indian will have beyond sixty years of age. Given that the expected years of life beyond sixty years is increasing, it will be essential to make these years free from disability. In fact, according to-Murray and Lopez (1996) a quarter of this extended life will be with disability of one kind or the other. Also predictions are made with regard to these disabilities on account of non-communicable diseases mainly cardiovascular, neuro-psychiatric, sense organ and respiratory related symptoms. Therefore, in order to make these additional years of life more productive and healthy, there is a need to plan and provide for an increasing burden of chronic disease morbidity, some of them rather debilitating for the elderly. Table l: Expectation of life at ages 60 and 70 for India.

Year

Male e60

e70

Female e60

e70

1971

13.80

8.57

14.75

9.10

1981

14.25

8.83

15.31

9.42

1991

15.01

9.27

16.23

9.97

2001

15.74

9.70

17.05

10.45

2011

16.29

10.03

17.75

10.87

2021

16.75

10.32

18.18-

11.14

Demographic Transition and Epidemiological Transition The demographic transition described is accompanied by changes .in the pattern of disease- the epidemiological

transition first defined by Omran (1971). In the past, as nations underwent social and economic transformation, improvements were gradually reflected in changing pattern of disease, for instance, through control of infectious and parasitic diseases contributors to early mortality. However, the experience of recent decades has shown that developing countries are now undergoing changes-in disease patterns, even in the absence of socio-economic development. This is largely due to impact of medical technology, A patient with tuberculosis for example would have died at a young age in the past is now likely to survive due to availability of effective treatment. The same could be said of a child living in a slum who will not experience infectious diseases such as measles or poliomyelitis due to availability of vaccines. The control of infectious diseases in the developing world has taken place more rapidly. For example the share of infectious and parasitic diseases causing death has been replaced by deaths due to cardiovascular diseases and neoplasm’s (Kalache, 1991). Though, some of these infectious diseases like pneumonia and dysentery remains widespread, they cause fewer deaths. On the contrary, disease causing agents like cardiovascular, intestinal and respiratory infections are of greater importance in terms of morbidity or potentials years of life lost. Trends in the duration of life lived with disability that accompany the epidemiological transition have been subject to extensive debate (Crimmins, 1990; Murray and Chen, 1992). There are three types of theories or postulates put forward to explain the changes in disability that accompany mortality decline. Fries (1980, 1989) and colleagues (Fries et.al. 1989, Leigh and Fries (1994) argue that with improvement in survival, the prevalence of disability will decline and therefore the proportion of life span lived in a disabled state will decrease. This theory is often called 'compression of morbidity hypothesis'. Conversely, an alternative set of theories have been proposed predicting that the proportion of the life span lived in disability will increase as mortality declines. Gruenberg (1977) suggest that as the survival of individuals with chronic conditions such as Down syndrome improves the prevalence of-these conditions would rise. Other authors (Alter and Riley, 1989; Feldman 1983 and Shepard and Zeckhauser, 1980) also forecast an increase in the prevalence of disability. According to them medical interventions improve survival for more frail individuals who will subsequently experience higher incidence rates of disability. More recently 01shansky et al (1991) further refined the expansion of morbidity hypothesis. A third theory (Manton, 1982) which shares element of both these view

points, predicts that the progression of chronic diseases to severe disability will be slowed leading to a decline in the prevalence of severe disability but arise in the prevalence of mild disability; the later would occur due to decline in mortality.

a feeling of dissatisfaction among the elderly with regard to the provision of medical aid. This report also mentions that the sick elderly lack proper familial care and at the same time public health services are insufficient to meet the health needs of the elderly.

Health Concerns of Elderly

We have made an attempt to assess the health situation among Indian elderly with data from the National Sample Survey (1991) and the Ageing Survey (1993), carried out by us in four major states as a part of the study on elderly in India (more details, see" Irudaya Raj an, Mishra and Sarma, 1999; Irudaya Raj an, 1993). The two major health concerns among the elderly are their disability/impairments and chronic disease prevalence. Also, their lifetime habits along with preference for type of treatment have also been analysed. These parameters are also examined with respect to three basic characteristics e.g. age, sex and marital status to understand the differential therein according to such characteristics.

Health problems are supposed to be the major concern of the elderly as older people are more prone to suffer from ill health than the younger ones. It is often claimed that ageing is accompanied with multiple illnesses and physical ailments. Besides physical illnesses, aged are more likely to be victims of poor mental health which arises from senility, neurosis and extent of life satisfaction. Health therefore occupies prominence in any study of the elderly. In most of the primary surveys, the Indian elderly in general and their rural counterparts in particular are said to have some health problems. Nandal, Khatri and Kadian (1987) found a majority of the elderly were suffering from ailments like cough, poor eyesight, anaemia and dental problems. The proportions that are ill among the elderly is found to be increasing with advancing age and the major physical disability is in terms of blindness and deafness. Shah (1993) in his study of urban elderly in Gujarat too finds deteriorating physical conditions among two thirds of elderly in terms of poor vision, hearing handicap, arthritis and loss of memory. An interesting observation made in this study was regarding the proneness of sick elderly towards availing treatment from private doctors. Besides physical ailments, psychiatric morbidity is also prevalent among a significant proportion of the Indian elderly. An enquiry in this direction indicates that a mental illness starts beyond the age of sixty years. While distinguishing between the functional disorders and organic disorders, it was noted that functional disorders are more common compared to organic an disorder which occurs beyond 70 years of age. The National Sample Survey (1991) indicates that 45 per cent prevalence of chronic illness among the elderly and joint pain and cough were reported to be the most common health problems. The other diseases among the elderly found in the survey include blood pressure, heart disease, urinary problem and diabetes. A major cause for mortality among the elderly was respiratory disorders in rural areas and the disorders of circulatory system among the urban elderly. Another rural survey reported around 5 percent of the elderly as bed ridden and another 18.5 per cent as having limited mobility. Given the background of the prevalence of ill health and disability among the elderly, Vijay Kumar (1991) reports

The National Sample Survey (NSS) provides information specifically on the physical mobility as well as chronic disease prevalence. It is found that 44-47 aged males per thousand reported physical immobility as against 67-68 aged females per thousand at the all India level (Table 2). The male-female disparity in the proportion of persons with physical immobility observed at an all India level is also reflected both in rural and urban sectors. Physical immobility seems to be a function of age. Further, it is found that around 60 per cent of physically immobile among the elderly are in the old-old (70+) category.

Table 2: Proportion (per 000) Physically Immobile Aged persons by sex and residential Background, India.

Residence

Rural

Male

Female

Total

44

68

54

47

67

55

Source: National Sample Survey, 1991.

As can be seen from the Table 4, the proportion of aged persons with chronic disease varied between 443 to 455 per thousand persons at the All India level. This proportion among the aged males and females were of the same order in most of the states. With regard to the prevalence of chronic disease among the elderly, the problems of joints (46.96 per cent) followed by cough or Asthma related

complaints (34.37 per cent) were found to be prominent. Though there remains no wide difference with regard to prevalence of chronic illness between rural and urban areas, the pattern of disease prevalence shows some differential (Table 5). For instance, heart disease, diabetes and blood pressure are more common among urban elderly compared to their rural counterparts. Such differential pattern of chronic disease between sexes does not seem significant except the female elderly sharing a little greater risk of being chronically ill.

Table 3: Percentage Distribution of Physically Immobile Elderly by Age, Sex and Residential Background, India.

Rural

being experience of the elderly in parts of India. The self rating of the health status of elderly is presented against their individual characteristics (Table 6). Though such a biased assessment based on self rating may be subjective or relative, it has been considered as an assessment of health status of the elderly. This self assessment of health status was in three categories namely healthy, fairly alright and unhealthy and it was found that around two third of the elderly reported to be fairly alright as against ten percent who reported as unhealthy. The frequency of declaring themselves unhealthy increases with age and by the age of ninety, fifty percent of them reported themselves as unhealthy. Also, there appears no striking difference on the rating of health status by sex. Table 5: Percentage Distribution of Type of Chronic Diseases among the Indian Elderly by Age and Residential Background.

Urban

Age

Male

Female

Total

Male

Female

Total

60-64

22.71

19.93

21.30

19.10

18.07

18.59

65-69

20.88

19.88

20.37

24.66

19.83

22.34

70+

56.41

60.19

58.33

56.24

62.10

59.17

Source: National Sample Survey; 1991.

Type of Chronic Disease Age Cough Piles Problem Blood Heart Urinary Diabetes of joints Pressure disease problems Rural

Table 4: Proportion (per ‘000) of Aged Persons having Chronic disease by Sex and Residential Background, India.

Residence

Male

Female

Total

Rural Urban

451 443

448 455

450 448

60-64

35.6

3.37

46.07

6.47

3.26

3,20

2.04

65-69

33.78

2.98

48.50

6.22

3.53

3,21

1.78

70+

33.69

3.35

46.65

6.52

4.33

4.05

1.41

60+

34.37

3,25

46.96

6.42

3,74

:3.53

1.73

60-64

24.16

3.65

38.37

18.53

6.44

3.20

5.65

65-69

24.25

3.80

38.70

17.65

6.88

3.10

5.62

70+

24.95

3.38

39.16

16.60

5.93

5.23

4.76

60+

24.52

3.58

38.79

17.48

6.34

4,02

5,27

Urban

Source: National Sample Survey. 1991.

Health Status: Some survey results from selected states

Source: National Sample Survey

This section makes an attempt to assess the physical health 1991. and mental health of the elderly together with their preference in use of medical services (more details, see Irudaya Rajan, Mishra and Sarma, 1999). For this we have In order to identify health problems faced by the elderly used the results of the Ageing Survey conducted by us as a they were asked 'were you sick at any time during the last part of the larger study on ageing sponsored by 'Economic one week/one month/one year?'. In addition they were also and Social Commission for the Asia and Pacific, Social asked if they had a problem that they suffered from Development Section, Bangkok. This Survey covered four continuously. This information was analysed with respect Indian states namely Tamil Nadu, Kerala, Gujarat and to the age and sex of the respondent. In this regard, 35 per cent of the surveyed elderly reported having some or the Karnataka. other perennial health problem which seems to increase in This exercise has been carried out using the demographic proportion with increase in the age of the respondent (see terms of reference. Therefore, its precision from Table 7). This phenomenon is not found significantly epidemiological/medical perspective may not be high. different between sexes except in the young-old category However, I present these findings as indicative of the well

where females seem to be having an advantage over the males. Based on the information on sickness during last one week, one month and one year, the sickness prevalence with a week reference period is measured at 11.3 per cent and the same with reference of one month and one year is found to be 28.5 and 26.8 per cent respectively. With regard to the incidence of being bed ridden during last one year, around fifty percent of the male elderly and 60 percent of the female elderly have positive responses. This indicates the state of health among the elderly and the severe morbidity which some times affect them. Instances of being bed ridden are more frequently reported by old-old category.

Table 6: Percentage Distribution of Elderly by Self Assessment of Health Status & individual characteristics. Individual Characteristics

Healthy

Fairly Alright

Unhealthy

Age

Sex

Table 7: Age-Sex Distribution of the Elderly by perennial health problems and being bed ridden during last one year. Perennial health Problems

Reported to be bed ridden during last one year

Age

Male

Female

Male

. Female

60-64

30.4

26.6

42.9

55.7

65-69

36.7

35.5

53.1

56.2

70-74

34.9

45.9

48.3

62.1

75-79

50.5

36.0

57.1

61.4

80"84

54.1

42.5

69.6

70.4

85-89

60.0

60.0

72.7

62.5

90+

33.3

55.6

50.0

83.3

All ages

35.2

34.6

49.3

58.7

Source: Ageing Survey

60-64

28.6

65.5

5.9

65-69

22.9

70.6

6.5

70-74

21.8

64.7

13.5

75-79

21.5

59.9

18.6

80-84

11.7

66.2

22.1

85-90

3.3

53.3

43.4

90+

6.7

46,7

46.7

Total

24.4

66.1

9.5

Male

24.7

66.6

8.7

Female

24.0

65.4

10.6

NM

26.0

60.0

14,0

CM

28.1

64.4

7.5

W/D/S

18.0

69.5

12.4

Marital Status

-Source: Ageing Survey

Disability/Impairments Certain disabilities like impairment in vision, hearing and movement are common consequences of deterioration of muscles and senses in old age. For evaluation of the prevalent extent of disability/handicap among elderly, the three aspects enquired were regarding the problems with vision, hearing and walking. The reported number of cases' having problems on these counts are tabulated by age and sex. Following a listing of disability conditions, the extent of

Table 8: Percentage Distribution of Elderly by different Handicaps. Age

Type of Handicap Vision Hearing-

Walking

60-64

28.9

7.8

11.9

65c69

32.8

8.9

15.7

70-74

35.6

12.0

19.9

75-79

45.3

19.8

29.7

80-84

54.5

31.2

44.2

85-90

66.7

33.3

43.3

90+

60.0

66.7

73.3

All Ages

33.7

11.1

17.4

Sex Male

35.4

11.3

17.6

Female

31.4

10.9

17.1

Source: Ageing Survey

use of aid to, overcome such deficiencies and their effectiveness was also enquired into. Among the three major handicap faced by the elderly, handicap in vision seems to be prominent with one third

of the elderly having poor eyesight. (See Table 8) Following this, mobility and hearing handicap account for 17.6 percent and 11.1 percent of the surveyed elderly. These handicaps become more and more frequent with increasing age. A sex specific description of handicap prevalence puts females better compared to the male elderly. With regard to the extent of use of aid to overcome these handicaps, it is found that 27 per cent of those having vision handicap use spectacles and the same for those with hearing and walking handicap is 2.3 per cent and 8.6 per cent respectively. Further the effectiveness of the aids used is pretty high (i.e., 67.2 per cent) in case of vision handicap, whereas aids used are reported to be least effective in case of hearing impairment followed by walking. To assess the individual's physical ability, opinion was sought with regard to their traveling without depending on somebody. And in this context, males' physical ability seems to be more compared to the females, but this could also be reflective of gendered norms regarding appropriate female behaviour. Lifetime Habits Individual health condition ought to have some bearing on one's personal habits and practices in relation to diet, exercise, occupation, sleep, smoking, drinking and chewing etc. Among the personal habits enquired, use of tobacco <in any form seems to be prominent. Some of these habits have a linkage with traditional practice. Also there remains a subjective differential in these habits between sexes which perhaps can be attributed to the social acceptability, As regard the frequency of eating, the surveyed elderly are reported to be eating on an average three times a day which is more or less same for both the sexes. Also the extent of appetite reported by the respondents reveals that 40 per cent of them do not have proper appetite. In addition, 36 per cent of the female elderly were found in the vegetarian category against the same being around 22 per cent for their male counterparts. Preference for vegetarian status seems to be increasing with increase in age as more than sixty per cent of the elderly in later ages are reported to be vegetarian. No doubt, the eating frequency per day shows a decline over age of the elderly. Similarly, the average number of sleeping hours reported comes between 7 to 8 hours a day. This too is not very different between males and females. Use of health services The extent of use of health services is an indirect determinant of their access to health care. The frequent health

problems among the aged calls for a regular utilisation of health services. Hence our interest lies in answering questions of the kind e.g. How do they cope with their health problems? Do they get adequate treatment? Which system of treatment do they prefer? And who pays for their medical expenses? etc. In case of sickness, ninety per cent of the respondents irrespective of sex stated that they had consulted a doctor: And the most popular system of treatment preferred by the Indian elderly is the Allopathic system which is adopted by nearly ninety per cent of them. The rest ten per cent of the elderly adopt either Ayurvedic or Homeopathic system of medicine. Further the most common source of health service utilisation is reported to be from the Government followed by the private clinics and hospitals. Summing up Assessment of health status among the aged is commonly in terms of self rating, though it is considered subjective to evaluate an individual's health in terms of his/her self assessment. It is suggested in literature that self rating of health is an excellent choice for measuring health status especially in surveys. The elderly population in India is growing and these increasing numbers need to be provided with adequate health care. There is a preponderance of chronic morbidity conditions among the elderly, with joint/arthritic problems and respiratory ailments among a majority of them. There is an urban-rural difference in this proportion of elderly having circulatory disorders like hypertension and heart disease. While most of them seek health care, it has been observed that for most common impairments associated with ageing like vision, hearing and mobility, only about two thirds used effective aids to overcome vision impairment. In the case of hearing and mobility, aids were used minimally by the elderly and their effectiveness were also limited. There is a need to make provisions not only for health needs but also for aids required to improve vision and hearing, which would improve the quality of life of the elderly.

Announcement Next Annual Meet of MFC New Challenges for Health in the year 2000 Date: 27-29 Jan., 2000 Venue: Yatri Niwas, Sewagram, Wardha. Contact: Convenors' office.

Appendix

References

Table Al: Demographic Profile of the Aged, 1991-2021 States

Year

Population( 000's)

Percentage

Treas, J. and Logue, B. (1986) Eco nomic Development and the Older Population. Population and' Develop

Sex ratio

ment Review, Vol. 12(4), pp.645-673.

to total

(70+)

Grigsby, J. (1993) Paths for Future Population Aging, The Gerontologist, Volume 31, No.2, April, pp.195-203. World Bank (1994) Averting the Old Age Crisis: Policies to Protect the Old and Promote Growth, Oxford Univer sity Press, Oxford. Irudaya Rajan, S. 1992a. Growing Old Gracefully. Chapter II in Nair, K.R (ed). 1992. Geriatric Neurology. Pub lished by Trivandrum Association of Neurology, Trivandrum.

population (60+) Andhra Pradesh Arunchal Pradesh Assam Bihar Goa Gujarat Haryana Himachal Pradesh Jammu & Kashmir Karnataka Kerala Madhya Pradesh Maharashtra Manipur Meghalaya Mizoram Nagaland

Orissa Punjab

Rajasthan

(Table contd. next

(70+)

(60+)

(70+)

(60+)

1991 2021 1991 2021 1991 2021 1991 2021

4306 11469 37 134 1186 3355 5227 13451

1425 4475 12 49 448 1153 1803 5123

6.47 11.63 4.23 7.02 5.29 9.34 6.05 8.11

2.14 4.54 1.42 2.57 2.00 3.21 2.09 3.09

102 104 83 86 81 90 86 100

106 110 90 85 76 89 85 99

1991 2021 1991 2021 1991 2021 1991 2021 1991 2021 1991 2021 1991 2021 1991 2021 1991 2021 1991 2021 1991 2021 1991 2021 1991 2021 1991 2021 1991 2021

74 217 2540 6791 1230 2511 402 744 432 1111 3041 7770 2549 5771 4254 9704 5453 14019 109 310 82 264 34 108 65 190 2217 5026 1532 3558

27 88 966 2515 528 905 164 286 163 430 1149 3020 1006 2324 1583 3594 1934 5427 40 116 29 92 12 38 28 73 794 1935 625 1410

6.34 14.51 6.15 10.33 7.47 8.72 7.79 9.58 5.78 8.15 6.76 11.18 8.77 15.63 6.43 7.75 6.91 11.72 5.94 10.16 4.62 6.73 4.93 8.61 5.40 8.32 6.98 10.60 7.56 11.11

2.27 5.90 2.34 3.83 3.21 3.14 3.18 3.69 2.18 3.15 2.56 4.34 3.46 6.30 2.39 2.87 2.45 4.54 2.19 3.80 1.64 2.35 1.77 3.01 2.31 3.19 2.51 4.08 3.08 4.40

131 109 107 99 93 90 89 114 77 99 101 101 115 117 98 98 101 100 88 100 83 101 96 95 71 95 99 99 83 95

141 Irudaya Rajan, S. 1992b. Aging in In: 117 Dia. Paper presented at the Workshop 113 on Development of Comprehensive Na Tional Policies on Aging: Life-long 105 Pre" 81 Paratory Measures including Social 91 Security, organized by the Social De 87 Velopment Section, United Nations, 120 Bangkok, held at the Social Develop 75 ment Section , United Nations, 104 Bangkok, 29-31, October. 105 Irudaya Rajan, Sand U S Mishra. 102 1997. Population Aging: Causes and 121 Consequences. Chapter 12, Pp. 222 130 236 in K C Zachariah and S Irudaya 100 Rajan (eds) 1997. Kerala's Demo105 graphic Transition: Determinants and 105 Consequences. Sage Publications, New 106 Delhi. 92 Kumar, S. Vijaya.1991. Family Life 102 and Socio-Economic problems of the 86 Aged. Delhi Ashish Publishing House. 102 Nandal, D.S., RS. Khatri and RS. 105 Kadian. 1987. Aging problems in the 99 structural context. In M.L. Sharma 69 and T.M. Dak (Eds, Aging in India, 93 Ajanta publications (India) New Delhi, 98 pp. 106-16. 104 National Sample Survey Organisation. 78 1991. Socio-Economic Profile of the 99 Aged persons: NSS 42nd Round (,July

1991

2666

917

6.06

2.08

98

103 1986 -June 1987) Sarvakeshan, Vo1.15

2021

6488

2422

7.59

2.83

99

105

No.2. New Delhi.

States

Year

1991

Sikkim Tamil Nadu Tripura Uttar Pradesh West Bengal Andaman & Nichobar Islands Chandigarh Dadra Nagar Haveli Daman & Diu Delhi Lakshadweep Pondicherry All India

Population ( 000's)

% total population

Sex ratio

(60+) (70+)

(60+) (70+)

(60+) (70+)

19

6

4.59

1.51

75

78

2021 1991 2021 1991 2021 1991 2021 1991 2021 1991 2021 1991 2021 1991 2021 1991 2021 1991 2021 1991 2021 1991 2021 1991

63 22 4073 1408 10261 4195 192 87 422 146 9250 3403 19083 7256 4087 1500 11495 4176 10 3 53 19 29 11 136 48 6 2 23 8 6 2 17 6 444 154 1764 606 3 1 8 3 56 21 144 56 55606 20252

7.98 7.29 14.30 6.96 8.59 6.65 7.10 6.00 11.62 3.55 12.13 4.52 15.71 4.40 8.35 6.32 11.41 4.71 11.29 5.22 10.19 6.90 12.60 6.58

2.82 2.52 5.85 3.17 2.98 2.45 2.70 2.20 4.22 1.18 4.32 1.73 5.53 1.24 2.95 2.43 4.30 1.63 3.88 1.68 4.00 2.55 4.88 2.40

85 92 106 97 97 81 97 96 91 67 79 83 81 122 103 150 107 85 81 92 110 108 104 94

84 90 110 100 103 77 97 96 96 78 68 87 79 140 115 168 123 86 82 93 116 105 112 93

2021

136459 52018

9.87

3.75

99

103

Source: Irudaya Rajan, Mishra and Sarma, 1999 Irudaya Rajan, S, U S Mishra and P S Sarma. 1995c. Living Arrangements among the Indian Elderly Hong Kong Journal of Gerontology, Volume 9, No 2, Pp, 20-28. Shah, V.P. 1993. The Elderly in Gujarat, Department of Sociology, Gujarat University, Ahmedabad (Mimeographed). Murray, C.J.L and A.D. Lopez (1996) 'Alternative Visions of the Future: Projecting Mortality and Disability, 1990-2020.' in Murray and Lopez (Eds), The Global Burden of Disease, Harvard School of Public Health. Crimmins, E.M. (1990) 'Are Americans healthier as well as Longer Lived? Journal of Internal medicine, vo1.22, pp.89-92. Murray, C.J.L .and L.C. Chen (1992) 'Understanding Morbidity Change' Population and Development Review, Vo1.18 (3), pp.481-503. Fries, J.F. (1980) 'Aging, Natural Death and the Compression of Morbidity' New England Journal of Medicine, Vo1.303 (3), pp130-135. Fries, J.F. (1989) 'The Compression of Morbidity Near or Far?' Milbank Quarterly, Vo1.67 (2), pp.208-232.

Fries, J.F. et al (1989)' Health Promotion and the Compression of Morbidity' Lancet, pp.481-483. Leigh, J.P. and J.F. Fries (1994) 'Education, Gender and Compression of Morbidity' International Journal of Aging and Human Development, Vo1.39 (3), pp.233-246. Gruenberg, E.M. (1977) 'The Failure of Success' Milbank Quarterly/Health and Society, vol. 55, pp.3-24. Alter, G. and J.C. Riley (1989) 'Frailty, Sickness and Death: Models of Morbidity and Mortality in Historical Populations' Population Studies, vo1.43, pp.25-45. Feldman, J. (1983) 'Work Ability of the Aged under Conditions of improving Mortality' Milbank Memorial Fund Quarterly/Health and Society, vo1.61, pp.430-444. Shephard, D. and Zeckhauser, R. (1980) 'Long Term Efforts of Interventions to Improve Survival of Mixed Populations' Journal of Chronic Diseases, vo1.33, pp.413-433. Olshansky, S.J. et.a1. (1991) 'Trading off Longer Life for Worsening Health: The Expansion of Morbidity Hypothesis' Journal of Aging and Health, va1.3, pp.194-216. Manton, K.G. (1982) 'Changing Concepts of Morbidity and Mortality in the Elderly Population', Milbank Quarterly/Health and Society, vo1.60, pp.183-244. Omran, A.R. (1971) 'The Epidemiological Transition: A Theory of Epidemiology of Population Change' Milbank Memorial Fund Quarterly, Vo1.49. . Kalache; A. (1991) 'Aging in Developing Countries' in M.S.J. Pathy (ed) Principles and Practice of Geriatric Medicine, Second Edition, C. J. Willey.

•

The Pulse of Polio Eradication Prof. T Jacob John The clinical purpose of immunization is to protect the individual against a particular disease. We-aim for such protection irrespective of who else in the community is, or is not, immunized. We do not worry whether the immunization protects against infection per se or not, since infection per se may be silent unless it manifests as recognizable clinical disease. And clinical vaccine efficacy is measured by the degree of protection against clinical disease. Epidemiologists have recognized that immunization can also become a tool in public health. The public health purpose of immunization goes beyond the clinical purpose. For clinical need, the immunological modification of the individual must be such that the final outcome of exposure, or even infection, does not progress to disease. For public health need, the immunological alteration of the individual must be such that the final outcome of exposure, or even infection, does not progress to the amplification of the microbe leading to transmission to another susceptible person. The clinician wants the immunized person protected from disease. The public health system wants the contacts of the immunized person freed from the risk of infection from that individual. This latter effect is the basis of the 'herd effect' of an immunization program. For instance, oral polio vaccine may protect a child from paralytic poliomyelitis after receiving a few doses of the same-may be 2 or 3 in a few, 5-7 in the vast majority. Such children, protected by the presence of antibody, do not allow the spread of poliovirus from the gastro-intestinal tract to the central nervous system. It appears that such clinically protected children continue to be susceptible to gastrointestinal tract infection by poliovirus, its amplification and its transmission. In other words, OPV affords very little mucosal immunity; hence very poor herd effect. Dr. Albert Sabin himself had shown that OPV worked best when given in mass campaigns. He believed that the vaccine viruses got transmitted to contacts; hence better vaccine efficacy. We know that the incidence of polio drops to zero or near zero for a period following an epidemic. Perhaps this is due to the infection reaching 100 per cent of susceptibles; perhaps not 100 per cent, but a very large proportion. What would happen if we mimicked an epidemic of OPV infection by feeding it to a very large proportion of under-fives?

Perhaps we could achieve high immunization coverage also, if we gave OPV, three times, at monthly intervals to the under-fives simultaneously. This was tested in Vellore in 1981. We had established polio surveillance for 2 years before the 3dose OPV campaign which we called 'pulse immunization'. The outcome was astounding. As against the expected 4.2 cases per month, 9 months passed without a single case. We surmised that transmission had been broken. The third dose coverage reached was only 65 per cent. This is the model of 'pulse immunization'. We repeated pulse immunization the next year with similar results. In the 2 years, altogether there were 9 cases, with no cases in 18 months and 6 months with 9 cases; the anticipated disease burden, was 100 cases with no month without cases. By this time, we began getting warnings not to conduct immunization campaigns any more; the Municipal Health Officer who cooperated with us was transferred. Obviously, OPV has some herd effect, hence some mucosal immunity. When such properties are built up simultaneously, there might be a rapid (but temporary) and steep fall in the proportion of children who are susceptible to infection by polioviruses. That might be enough to break transmission. There would be no poliovirus infection until re-introduced. This is my interpretation. If the pulse immunization stops short at 2 doses, the fall in the proportion of susceptibles may not be sufficient to break transmission; it only slows down transmission, Having failed to achieve break in transmission using 2dose annual pulse immunization, 3-dose pulse would have been logical; but if 3-dose pulse succeeded, there might be a 'loss of face' for those who made the 2-dose policy and for those who advised the policy matters. This is the background of the decision to give 4-dose pulses. As National Consultant on Polio Eradication (since mid1999) I was promised that in August 1999 the WHO experts would meet and consider if 3-dose pulses would be acceptable. That meeting did not take place. There is politics even in polio eradication.

•

Emeritus Medical Scientist of the ICMR, Department of Clinical Virology. Christian Medical College Hospital, Vellore, 632 004. Acknowledgements: Author is thankful to Mr. N. Swaminathan, Librarian. CHTC. Christian Medical College. Vellore, for word processing this manuscript.

Experience of Menopause among rural women Study from south India Rita Isaac Health problems of the old are becoming a major issue of late because of the rapidly growing elderly population. Moreover women have a longer life expectancy when compared to men. As a result very soon in most countries the elderly women will out number elderly men. Older women constitute a distinct population and their health needs are different from those of younger women. All women who live up to 50 years or more go through a period of transition from the reproductive to non-reproductive stage of life, within which menopause refers specifically to cessation of menses . What are the health repercussions of this event? Objectives This study was carried out in order to measure the prevalence of menopausal symptoms among women in rural South India and to see what extent these symptoms are modified by selected risk factors such as: • having an unaffectionate husband and an indifferent father to her children; • no partnership in decision making at home; • not supported by her husband; and also to study women's attitudes and beliefs about menopause. Materials and methods A sample of 100, currently married, post-menopausal women from the age of 40 through 49 years, were chosen from 7 purposively selected villages in Kaniambadi block, Vellore District, Tamil Nadu, for this study. To differentiate menopause related effects from concurrent ageing effects, an age-matched comparison group of 100 premenopausal women were also chosen. The study design was cross sectional and the information was collected using a questionnaire designed for in-depth interviews with a structured method of, recording responses. Results The study revealed that a significant proportion of postmenopausal women experienced vasomotor symptoms such as hot flushes(42%) and night sweats (38%), urgency of micturition (18%), symptoms suggestive of depression (29%), multiple somatic symptoms (58%) as compared to,

pre-menopausal Women where the corresponding proportions are 27%, 16%, 8%, 18% and 31% respectively. However in this study we tried to identify the other possible aetiological factors to account for the reported elevations of symptoms. Stratified analysis ,using Mantel-Haenszel technique to adjust for the effect of ageing on symptoms such as depression and multiple somatic symptoms showed that ageing does not contribute towards the elevation of symptoms. (Crude Odds ratio 1.86 adjusted OR 1.86, crude OR 3.07 adjusted OR 2.9 respectively). Poor perceptions regarding relationships within the family had a stronger association with depression than menopause (OR =3). However, after controlling for the potential confounding effects of risk factors by logistic regression analysis, odds ratio (OR) for depression among post-menopausal women was still high (OR =2, CI .9578 to 4.2). It was interesting to note that despite the significant menopausal symptoms experienced during the post-menopausal period, almost all of them felt menopause is very convenient in village living conditions and lifestyle. However, many of them (69%) complained of diminishing ability to work following menopause, but they related it to lack of blood and the ageing process. It is noteworthy that 54 percent of the post menopausal and 32 percent of the pre-menopausal women are currently not sexually active for reasons like lack of privacy, not accepted in their culture and because of grown up children, which are not related to menopause. Seventy four percent of those sexually active among post-menopausal women and 35 percent among pre-menopausal women did not enjoy sex and some found it intolerable. Conclusions This study showed that a significant proportion of rural women experienced symptoms associated with menopause. Menopause appears to affect the overall quality of life of rural women. However, rural women perceive menopause as a part of ageing process which has its own advantages and disadvantages. They have learned to accept it with grace.

•

Christian Medical College. Vellore 632 004 (Research done as a part of MD training in Community Health)

First Contact Care: Flogging a Dead Horse? Sham Ashtekar

I have a feeling I am writing on a subject that most health people have given up for various reasons- the CHW and First Contact Care (FCC). Is there going to be anything like the CHW in the post 2000 health system of India? Or is something like that necessary at all? 'Lip sympathy' on this issue abounds but of convictions or efforts there is little. Before getting into the issue, it is necessary to distinguish FCC from the Primary Health Care which is the larger framework for health efforts including health systems and hospitals, nutrition, drinking water, environment and all that goes into making health a reality at national levels. FCC thus is a subset of the PHC framework, not an overlapping or substituting phrase. Our group prefers the use of the phrase 'First Contact Care' for the village/community level comprehensive health care facility through trained workers. Can we do without FCC? I am one of the few adherents of a conviction that in a country that largely lives in its 5lakh villages and its doctors piling up in cities and towns, a well-trained health professional at the village level is still relevant for most health work. I am convinced that the health system of India is without its foundation if a village-based health professional is missing. Sadly most states in India except for MP and in some way Punjab have done little to save the dying breed. I saw functioning FCC systems in China, Philippines, and Thailand with country specific variations. In China it is already a third generation of barefoot doctors with three-year courses in institutes. Brazil introduced Community Health Workers as late as 1995 despite being a 70% urbanized nation and being far richer than India is. What is so special about India that we think that we can do without a reasonable FCC? As a group working on PHC in Maharashtra, we have been arguing about the need and possibility of FCC in the state for the last four years with a definite proposal for weaving a FCC network in the existing set of realities. Our experience has been that it is-very difficult to convince the government about FCC when it otherwise vows to provide primary health care every other day. There are several conceptual and operational hurdles, which I am listing here below. Is FCC a state's responsibility? If yes, at least Maharashtra

state is already doing it because the CHW is there on the expenditure lists and to add to that we have now the Pada health worker (heath messenger). The ICDS was already there in 1979. If you say that state is not responsible anymore, what about the legal problems involved in establishing FCC, the connection on national health programs, and the linkages with public health system? The medical bodies are vehemently opposed to the use of medicines in the hands of FCC workers-like what happened in Punjab. Even the private doctors without as much as a degree or training are opposed to it. A health minister who happens to be a doctor, is no less opposed to FCC since he is not willing to 'hurt the medical system' and the 'sanctity of the five year course'. Any non-doctor health minister is simply afraid of stirring the beehive of vested interests. Congress did not like the idea and BJP cannot hurt middle class interests. The result is that no health minister is interested. Come to health activists, the apathy continues. An eminent health activist told me that it is a bygone idea and 'it is twenty years late'. May be this is an honest position, but we see every health project using the CHW to do errandsin almost every project that you speak of Reproductive Health (RH), ARI, even income generation, micro credit, sustainable farming (sic) and so on. But when it comes to FCC infrastructure, it is another matter, something bygone and out of fashion. I am particularly worried by the fact that the RH agenda is being pushed despite the knowledge that there is simply no FCC system in most states and many NGOs are lapping up the RH initiative no matter what is the state of the general FCC system or the lack of it. Crores of Rupees were spent on an RH pilot project in. Nasik district by the government without any impact on RH ground realities. How can one think of such sensitive programs without someone to care for down there at the village! It will be naive to believe that FCC is a simple matter, indeed formidable forces are arrayed against it. Politically it is not an issue or popular demand. Doctors are generally against sharing the cake with anyone; politicians keep either a safe distance or ensure interests of the private medical community. Bureaucrats are uneasy with the idea of such 'loose participatory programs'. So it is nobody's baby any more.

•

Report and Minutes of the mfc Mid-Annual Meet (including report of PHC cell and women & health cell meets) July 28-30, 1999 at Sewagram, Wardha Report prepared by Madhukar Pai, MFC Convenor

The mid-annual meet of MFC was held between 28-30 July 1999 at Sewagram. 26 friends attended the meet. The meet was essentially a meeting of the cells of MFG. On 28th the PHC (Primary Health Care) Cell and ID (Infectious Diseases) Cell met together. On 29th the Women & Health Cell met. Business matters and organisational matters pertaining to the next annual meet were discussed on so- July. PHC and ID Cell Meet Since Yogesh Jain (convenor of ID Cell) was not present, Sridhar convened the meeting of PHC Cell and ID Cell together. There were 5 presentations made with background papers circulated. These were: 1. Some critical issues in the epidemiology of hepatitis B in India by Anant Phadke and Ashok Kale. 2. The case for intradermal route hepatitis B vaccination by Ashok Kale and Anant Phadke. 3. Zoonotic: a neglected route of tuberculosis by Ashok Kale and Shailesh Deshpande. 4. Zoonotic aspect of dog tuberculosis by Ashok kale. 5. PHC Collective: "Gramin. Lok Swasthya Yojana" an election manifesto note. Hepatitis B issues Anant made a brief presentation of the Hepatitis B issue. This was discussed as a case study for evaluating newer vaccines. As more and more new vaccines become available, there is pressure to include them into the UIP (Universal Immunization Programme). There is a need to evolve an overall strategy or approach to evaluating any new vaccine. Cost effectiveness analysis should be a part of this strategy. A vaccine may be good but is it cost effective? The presentation was based on a. detailed literature review and analysis of sequelae of hepatitis B infection done by Yogesh Jain and a group from Delhi and a cost effectiveness analysis and critique by the Pune group. Anant's presentation focused on 4 issues 1. The prevalence of hepatitis B in India is not as high as it has been made out. Many papers quote the figure of 4-

5% carrier rate. This figure is highly exaggerated because some studies suffer from' selection bias (done on unrepresentative populations like blood bank donors) and some do not account for the fact that the screening test used can give false positive results and therefore the positive predictive value could be quite low in low prevalence situations. There are also issues of flawed statistical methodologies. After accounting for all these problems, the true carrier rate could be only 1.42% 2. Many studies do not account for the infectivity of the carrier. Those who are Hbe Ag positive are more infectious. If this is taken into account, then the magnitude of the problem is further reduced. 3. The occurrence of sequelae after hepatitis B infection again may not be as common as reported. 4. Cost effectiveness analysis estimates that to save one life-year (from hepatitis B related problems), for the infant group, around Rs, 2275/- have to be spent.' This does not make it a very cost-effective vaccine and therefore inclusion into the UIP has to be reconsidered. 5. Intradermal vaccination may be more cost effective and some studies have shown that this is' an effective route. The cost could be reduced to one-fifth as compared to the regular route. This topic needs to be further studied as it can have a great impact on cost reduction. Discussion after the presentation was intense. Prabir questioned the wisdom of a national level policy if each state had widely differing prevalence rates of 'Hepatitis B. Issues like how does one decide when a disease is a public health problem was raised. The importance of accounting for the limitations of the screening test used was brought out by another example quoted by Madhukar: how the estimates of cataract blindness could be overestimated because screening test limitation were not accounted for. Prabir raised the issue of iatrogenic transmission of Hepatitis B because of unsafe practices during UIP vaccination (same syringe being used for many vaccinations). Suggestions were offered for further analysis: use of Number Needed to Treat (NNT) principle to compute the number needed to prevent Hepatitis B disease; study cost per death averted, attributable risk, etc, the issue of alcoholism and its impact on cirrhosis in our country was discussed.

If Hepatitis B disease can add to alcoholic hepatitis burden, then vaccination may be a good strategy, given the fact that alcoholism is so common in our country. Zoonotic TB Ashok Kale presented the issue of Zoonotic transmission of tuberculosis. He outlined the historic aspects of TB control in the rest of the world: most countries had made an effort to control bovine TB and wherever human TB had been controlled, in such places animal TB had also been controlled. If humans could acquire TB infection from animals (cows and dogs), then control of human TB without controlling animal TB could be impossible. Ashok Kale reviewed the studies that showed some association between canine TB and human TB. Most of these were old studies and not much Indian data was available on this subject. Most Indian TB experts believed that bovine TB was not the same as human TB strain and therefore it was unimportant. Moreover, Pasteurization was expected to prevent bovine TB. Ashok Kale pointed out that pasteurization cannot prevent transmission through the respiratory route. Animals suffer from pulmonary TB and though they do not cough, they can still transmit T'B. In our country, dogs are very commonly found in close contact with humans. As dogs are equally susceptible to human and bovine TB strains, they may be operating as a source of TB infection in India. These ideas were radically new to most members and there was a heated discussion on the importance of animal TB. Sridhar pointed out that most of the studies were old and newer data is required to understand the problem better. Ritu wanted to know if there was any Indian data on this. Anant mentioned that National TB Institute has failed to culture bovine TB strain from human sputum positive cases. Kale pointed out that there was apparently a proposal for a national animal TB control program in India during the 1960s but it did not materialize. It was felt that in India, this Zoonotic transmission is quite possible because of close contact between humans and animals. There was some discussion on the role of molecular epidemiology in studying this issue further. PCR and other molecular techniques could easily type the exact genome of the strains and thus it could be possible to study whether humans who suffer from TB could be harboring animal strains or vice versa. It was also pointed out that most of the studies quoted did not have comparison or control groups and therefore drawing conclusions from them was difficult. Moreover, Koch's postulates

have to be proved before it can be accepted that animal strains cause human disease. It was left that if Zoonotic transmission was important, then urban-rural differences in TB prevalence must be obvious. The group felt that this was a' topic worth doing further research on. The Pune group assured their support to anybody taking up the issue for study. PHC Collective: "Gramin Lokswasthya Yojana", an election manifesto note The PHC Collective is a group that arose as an' offshoot of the PHC Cell of MFC. To address the issue of no availability of medical care in rural areas, they have mooted the idea of a village level upgraded community health worker (UGCHW) or Bharat Vaidya. This concept is needed urgently because most governments have given up on the CHW idea. A paper on this idea is now being 'circulated among political parties with the hope that they will take up the idea and include it in their election manifesto. Sham Ashtekar presented the idea of UGCHW and there was a discussion on this. He said that since most CHW programs have collapsed, can this concept take up its place. Open universities could train these workers and Mahila and gram Panchayats can support the initiative. People can pay a small user fee for health care. Not much funding is required from the government. The government needs to issue a notification that a list of essential drugs can be used by these workers. The Maharashtra Medical council, for example, has a provision for this. These workers can then function as first contact care providers at the village level. There was a discussion on this concept. The issues raised were: since more drugs would be available with the UGCHW, they would need better skills at diagnosis and treatment; if the UGCHW is not linked up to better referral systems, then it would suffer from the same problems as earlier; how is the training for this new worker going to be better than what the earlier CHWs received; who will train and who will monitor their work? What kinds of diseases are these people expected to diagnose and manage? Ulhas raised the issue that instead of training a selected person from each village, can we not train a lot of interested people (like mothers) in managing common simple problems and make the drugs available over the counter? Will this not be more effective? Anant raised the issue of potential for misuse of drugs and malpractice. The concept of user fee was also a form of privatization. How will this matter? Sabala raised the issue that upgradation is being seen in a very narrow allopathic sense (increasing the

number of drugs to be used the UGCHW), but alternative forms of therapy are not being considered. Anant felt that training for these new workers can be done very well with manuals (available) and aids like fever diagnosis charts, etc. Ritu raised the issue of social control and accountability of these workers. There was a broad consensus that this initiative from the PHC Collective was welcome and the PHC Cell of MFC would support the initiative. Members were requested to follow up the matter with their local and regional political parties, to convince them to include the idea in their election manifestos.

Women and Health Cell Padmini convened a meeting of the Women & Health Cell on the 29th. Within the overall framework of the theme of 'Challenges for Health in the Year 2000', the W & H Cell discussed the issue of W omen, Work and Health. The meeting began with a presentation on studying the theme of 'work-related health outcomes in a context where work, particularly for women, is multi-dimensional in nature not only within the household-but also outside the household. Researchers attempting to study occupational health issues from a social science background can only record the perceptions of workers about their health and whether and to what extent (in the workers' perception) their ill " 'health I$' associated with their work. In the absence of any medical data with medical persons to corroborate such perceptions, this 11.1 turn precludes the possibility of any causality being established between work/occupation and specific ill-health/disease. Giving examples from her own work in the area, Padmini highlighted the problems faced in the field work where most official institutions-e-legal, medical, etc., hardly functioned and where even minimal data on conditions of employment were not available. Also, doctors working on occupational health quote only those papers published in clinical journals; hardly any attempt is made to study social sciences work done on occupational health. Padmini's introduction was followed by four presentations: 1. Meena presented a case study of home-based beedi workers and their occupational health problems. 2. Millie presented 'her work on health problems of worn en working in the .tannery industry. 3.Neha presented her study on impact of globalisation on the living environment of women in Mumbai slums. 4.Madhukar- presented his study on high rate of caesarean' section an affluent section of Madras city and-the factors influencing high caesarean rates.

Meena Gopal gave an overview of the conditions under which women workers in the beedi industry toiled relentlessly. She described how home-based work can lead to less sleep, poor eating habits, altered consumption patterns and priorities within the household; etc. Millie Nihila sketched out the organization of the leather tanning industry to give an idea of the range and intensity of the work done by women and children. Tanning is a hazardous industry and women are not supposed to work in tanneries. But 30% of the workers in tanneries were women, and most of them are contract laborers and not shown as 'workers.' Women workers do tasks like cleaning, carrying hides, unhearing, drying, trimming, etc. and they are prone to a wide range of health problems like skin disorders, and respiratory ailments. There was a discussion on' the importance of judicial activism in addressing the tannery industry problem. The issue of women not being categorised as workers was an important one. Unless that is done, the situation would not improve. Neha Madhiwala's presentation on women's work in a slum setting brought out issues relating to how globalisation can have an impact on women's living environment! And health. Women in such settings report a lot of morbidity but many episodes are untreated. The profile of work is shifting: women are involved in different occupations during their life time. A woman can typically be associated with 6-7 different jobs in a lifetime. The reasons why women quit work is also difficult to identify, Work is not generally perceived as 'job'. The factors that might force women to work may be very different from the factors that apply to men. Hard work during youth and neglect of health problems during younger days could lead to morbidity at a much later date when the women are old. At that time, they go to many places seeking health care. Neha posed the question that if definition of 'work' it self is changing, then how does one study occupational health? From these three presentations, the' definition of 'work' itself for women came out as being highly problematic given the varied spaces that women have to traverse at a point in time and over time. Whatever be the space from which women operated- private/home as in the case of the beedi industry and slums or public space or in the case of the leather industry-mainstream economics continues to marginalise women and women's work by; recognising and/recording most women as workers, and thereby denying them a whole range of benefits. The few

women who are part of the mainstream had to contend with lower status of employment followed by low pay and denial of promotions. In short, the presentations brought out in different ways the deteriorating qualities of the environment under which women are forced to 'work'. Madhukar made a presentation on the high caesarean section rates. When indicated, a caesarean section (CS) is life saving. Thanks to its role in reducing maternal and perinatal mortality, it is an indispensable part of obstetrics. However, if CS are done too often, what is the potential adverse impact? How much is too often?' While there is no universal agreement as to what should be the right number, a rate of about 10% could account for medical indications. In recent times, rising CS rates in many countries has raised concern. For example, in countries like USA, China and Brazil, CS rates have increased from about 5-15% to 25-30 % in just 2-3 decades. In the UK, this is said to be the 'most urgent crisis facing obstetrics.' Alarmed by these trends, many countries are actively trying to curb this rise. The government of USA has set a goal of reducing CS rate to 15(7r by 2000. Why is this trend alarming? Though CS are much safer today than in the past, CS carry risks to mothers and babies: risks of maternal mortality, maternal morbidity, longer hospitalization, higher healthcare costs, and injury to the baby. The risk of maternal mortality and morbidity after caesarean delivery is 8-12 times higher than after normal delivery. The negative psychological impact of caesarean section on mothers has also been studied. Medicalisation of normal childbirth and unnecessary medical interventions also raise ethical issues. In India, we do not know our national CS rate. A few studies and anecdotal report suggest that CS rates in urban areas are very high there has 'been very little research on CS rates and its adverse impact. The issue is rarely discussed in hospitals and medical institutions in India. Obstetricians in India have not shown any concern about this nor have they attempted audits. In one of the first studies of its kinds in India (being published in the National Medical Journal of India), Madhukar and his colleagues estimated the CS rate in a selected area of Madras City. It was as high as 45%. On purely scientific grounds, such a rate is impossible to justify. The study also showed that CS might be adversely affecting breast feeding. Babies born by CS were initiated on breast feeding late, given Prelacteal feeds more often, and given colostrums less often when compared to babies delivered normally. This adverse impact of CS on breast feeding has

has rarely been identified as a deterrent to breast feeding. There is an urgent need for research and advocacy on this issue, particularly to encourage the obstetric community to audit CS and explore strategies to curb its rise. Women's groups should be encouraged to independently audit CS rates to ensure that social issues (like women's choice in deciding on the mode of delivery, their right to be counseled about the risks of CS) will be explored. For advocacy, audit, and impacting on policy, we need studies, particularly on reasons behind rising CS rates, providers' opinions, and women's perceptions. Madhukar's presentation was followed by a long debate on the issue. Anant and Sham felt that unnecessary medical interventions were very common and CS was not the only one. In many areas, hysterectomies are being done unnecessarily, that too by unqualified people. Anant described the Pune experience of some surgeons wanting to charge the same fees for both vaginal and CS deliveries; he felt that such experiments are needed to solve the problem. It was also pointed out that lack of support from mid-wives, and declining skills in instrumental delivery could be reasons for rising CS rates. The issue of doing too many CS in teaching hospitals just because residents want more "cutting chances" was also discussed. Preparation For the theme meet in January 2000 The theme for Jan 2000 meet will be "New Challenges for health in the year 2000". The dates of this meeting will be 27, 28, 29 of Jan 2000. It will be held at Sewagram. Both the PHC and Women & Health Cell met on the third day to discuss the theme meet and papers or presentations were planned for the meet. Minutes of the Extraordinary General Body Meeting, held on 30th July 1999 An Extraordinary General Body Meeting was held on 30th July 1999, to discuss the issue of the Bulletin. Members expressed concern that the Bulletin has not come out for the past 6 months. The letter by Sathyamala was read out and -discussed. It was decided that no major decisions about the bulletin could be ta-ken till the next AGM (to be held in Jan 2000). Until such time, the present editor could be requested to continue. A resolution about shifting the editor's office to Bilaspur was proposed and accepted. The group members were requested to contribute articles to the Bulletin. It was hoped that the coming annual theme meet background papers would ensure enough papers for the next few issues of the Bulletin.

•

CONTENTS Author • Health Implications of Ageing

US Mishra

• The Pulse of Polio Eradication

T J John

• Experience of Menopause among rural women

R Isaac

• First Contact Care: Flogging a Dead Horse

S Ashtekar

• Report of mfc Mid-Annual Meet

M Pai

Page 1 9

10 11 12

The Medico Friend Circle (MFC) is an all India group of socially conscious individuals from diverse backgrounds, who come together because of a common concern about the health problems in the country. MFC is trying to critically. Analyse the existing health care system which is highly medicalized and to evolve an appropriate approach towards developing system of health care which is humane and which can meet the needs of the vast majority of the population in our country. About half of the MFC members are doctors, mostly allopathic, and the rest from other fields. Loosely knit and informal as a national organization, the group has been meeting annually for more than twenty years.

a

The Medico Friend Circle Bulletin, now in its twenty fourth year of publication, is the official publication of the MFC. Both the organization and the Bulletin are funded solely through membership/subscription fees and individual donations. For more details write to the convenor. .

Subscription Rates:

Inland (Rs.) Individual Institutional Asia US $) Other Countries (US $)

Annua l 100 200

10 15

Two years 175

Five years 450

350

925

Life 1000 2000 100 150

Cheques/money orders, to be sent in favour of Medico Friend Circle, directed to Manisha Gupte, 11 Archana Apts., 163 Solapur Road, Hadapsar, Pune·411028. Please add Rs. 10/- for outstation cheques. Convenor's Office: Anand Zachariah, Madhukar Pai, Prabir Chaterjee; C/o Anand Zachariah, Medicine Unit I, CMCH, Vellore, Tamilnadu, 632004 Editorial Office: Sathyamala, B-7, 88/1, Safdarjung Enclave, New Delhi-110029. Edited by Sathyamala, B-7, 88/1, Safdarjung Enclave, New Delhi-110029; published by Sathyamala for Medico Friend Circle, 11 Archana Apts., 163 Solapur Road, Hadapsar, Pune-411028. Printed at Colour print, Delhi-110032.

Views and opinions expressed in the bulletin are those of the authors and not necessarily of the organisation.


