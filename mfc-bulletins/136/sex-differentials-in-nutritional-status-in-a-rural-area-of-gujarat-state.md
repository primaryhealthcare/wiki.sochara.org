---
title: "Sex Differentials In Nutritional Status In A Rural Area Of Gujarat State"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Sex Differentials In Nutritional Status In A Rural Area Of Gujarat State from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC136.doc](http://www.mfcindia.org/mfcpdfs/MFC136.doc)*

Title: Sex Differentials In Nutritional Status In A Rural Area Of Gujarat State

Authors: Visaria Leela

﻿

	136

medico friend circle bulletin


JANUARY 1988



Sex Differentials in Nutritional Status in a Rural Area of Gujarat State: An Interim Report PA RT-I

Leela Visaria



Women are regarded as both biologically stronger and physiologically superior to men. Biologically, the presence of a pair of X chromo­some protects women against chromosome linked recessive disorders and makes them less suscep­tible to infectious diseases (1). Physiologically, women are reported to be more efficient than men; for a given quantum of work, they require some­what less protein and energy than men(2). Other things being equal, these innate differences would result in lower female mortality compared to that of men; this situation is observed in most parts of the world today. In the developed countries, the sex differences in mortality has been widening. By 1983, a difference of 7 to 8 years in the life expectancy at birth between males and females was not at all uncommon and was reported by USA, France, Finland and Australia (3). The once common maternal deaths have been virtually eliminated. At the same time, the biological disad­vantage of men is aggravated by a stressful life style and accidents, which account for a significant proportion of deaths. The observed high male ­differences in mortality at ages 35-75 in countries such as Finland, France, USA and USSR are attri­butable to the higher incidence of cardiovascular and respiratory diseases (including lung cancer) &  accidents among men (4). In sharp contrast to this general pattern is the situation reported by the populations of the Indian subcontinent where males enjoy lower mortality than females almost from birth until about the end of the reproductive period of the later.

This has been an important factor contributing to the anomalous excess of males in the population reported by the censuses for nearly a century now. The age specific death rates based on the recent large data sets such as the Sample Registration System in India have confirmed the excess female mortality suggested by the earlier estimates 'of life expectancy at birth and other ages, based on the census age data. This paper is a preliminary examination of the data on birth weight and on nutritional status or weight gain among children under the five years of age available from a research-cum-action project in a rural area of Kachchh district of Gujarat state, to see. whether and how far these support the widely held hypotheses about differentials in treat­ment between young boys and girls in terms of food allocation and health care, being a major cause of higher female mortality.

The Study Area The study area was selected because of the scope for collaboration with an NGO (non-govern­mental organisation) based in Ratadia village in Mundra Taluka in Kachchh district. The NGO, named Shri Sangh, is led by two health professio­nals, actively involved in multi-faceted develop­mental work while employed by the Panchayat hospital with an attached maternity home. The project area, spread over 25 villages, covers about 3400 households with a population of 17,000. Following a pretest in April 1985, a benchmark survey of all the households was carried out during June-July 1985. The region is very heterogenous in caste composition and also has a long tradition of out migration to Bombay and other urban centres and even abroad, to East Africa in earlier decades and recently to the Gulf countries. This is reflected in the sex ratio of population in rural areas of Kachchh district, 975 males per 1000 females in 1981, was 'lowest in the State, compared to 1043 for Rural Gujarat.1

The action programme involves a careful re­cording of all the pregnancies, births and deaths as well as monthly monitoring of the weight of all the children below five years (or sixty months) of age. (Measurement of height began in October, 1986). The growth monitoring activity began on October 2, 1985 in five villages (total population, 2490; number of under-five children, 374). It was expanded to four more villages in May 1986 (total population 3509, number of under-five children, 414). The tenth village was added in August 1986. In addition, we have data on births that occurred in the maternity home since 1980.

Sex Differences in Birth Weights . Out of about 200-225 deliveries occuring in the Ratadia maternity home each year, between 60 and 70 percent are to the mothers who come from the project area.2 The hospital maintains records of all the deliveries with details about the caste, education, occupation of the parents along with the sex, parity and the birthweight of each baby.3 Over the six years 1980-85, recorded birth-weights are available for almost 800 babies from the project area villages. Admittedly, the information on the gestational age of the fetus is not accurate. Most of the women come to the hospital for the first time at the time of delivery or register their names just a couple of weeks before the date of delivery. Therefore, the information on the time elapsed since last menstruation, given by the women or their relatives, is accepted at its face value. The hospital did not until recently ascertain the gesta­tional age of the fetus independently through measurement of the fundus height. Interestingly, the percentage distribution of children of various caste groups according to their birth weight Shows that both the mean and the distribution are similar to those observed in many other Indian Studies (cited in Visaria, 1985)4. About 25 per cent of the children (22 percent of boys and 28 percent of girls, born in our project area during 1980-85, had a birth weight of less than 2500 grams. The inter-caste variations in this percentage are very small except that more than 50 percent of the Harijan children (45 percent of boys and 56 percent of girls) had birth weight of less than 2500 grams. Such babies are considered "high risk" according to the WHO standards. However, the Indian pediatricians report that "full-term" babies weighing between 2000 and 2500 grams can survive with minimum inputs. As noted above, the data do not include any in­formation on the duration of gestation, but about 5 percent of all children (4 and 6 percent of boys and girls, respectively), and 15 percent of the Harijan children (8 percent of boys and 23 percent of girls) had a birthweight of less than 2000 grams and were "at risk" according to the Indian "stan­dards" as well.5 The observed sex difference in birth-weights in our project area was similar to that in the data for the reference population of the National Center for Health Statistics (NCHS). On an average, girls weighed about 200 grams less than boys at birth but the differencE3 was not statistically significant. The inter-caste differences also do not seem to be significant. The limited number of observed births, might be the likely explanatory variable. Yet, surprisingly, among the Barot babies, the average birth weight of girls exceeded that of boys by 110 grams. Unlike other women in this region, the Barot women tend to be tall and better­ built, but the observed difference is certainly not statistically significant.6

On the whole, boys do not begin life with any marked advantage over girls in our project area. We shall next examine whether and when in the course of next five years, clearly identifiable dis­advantages emerge which adversely affect the girls.

Dynamics of the Child Population Under Observation In our programme of monthly weighing of children, the base population changes every month. Apart from the loss of some children because of deaths, some children cross the stipulated age limit; some migrate from the region permanently with their families or are temporarily away; they are not weighed. Some children may not be brought for weighing because of illness or because parents are busy or for any other reason. The changes in the denominator as well as the numera­tor need to be monitored every month to assess the proportion of children weighed and the reasons for the non-weighing of others.

.

..,

2















						











4


by weight; but a higher pm portion of girls (12.3) than of boys (4.7 percent) fell in the "at risk" category. The birth weight of a large majority of children puts them between the mean and the three standard deviations curves, and their growth pattern broadly follows the standard curves.

4. After the age of six months, however, falter­ing of growth begins among both boys and girls. The percentage of children below the three stan­dard deviations curve rises to 20-25 percent or more from about six months upto four years of age, with a relatively small sex difference. A proper feeding of the children during the post-weaning period as well as managing the weight loss or lack of gain due to infectious diseases seem to pose a problem.

5. After' the age of 3 years, a markedly higher percentage of girls, than of boys falls below the three standard deviations from the mean curve. However, given the small number of children, the sampling error is large and the observed sex differences are not statistically significant.

6. As indicated in Table 2, on an average, we have 4.7 pairs of observations of weight per child. About 65 per cent of the observed cases of both boys and girls relate to weight gain. About 23-25 percent of weight observations indicated loss of weight between two successive months. The extent of seasonality in weight gain or loss, is yet to be explored.

Another way of looking at the serial weight measurements (discussed by Jelliffe 1966) brings some of the above observations in a sharper focus. Ideally one needs a minimum of one year's weight observations in order to calculate the average monthly weight gain by age. However, to obtain a larger number of observations in each group, weight data for five villages (where the programme was launched in October 1985) and those for four villages (where it was launched in May 1986) are pooled. The data for five villages based on 11 months' observations show a sex and age pattern very similar to that evident from the pooled in­formation as for nine villages. The data indicate that the total weight gain of the project children upto five years of age was 68 percent and 77 percent of the mean standard of the NCHS for boys and girls, respectively. Interestingly, girls faired better than boys in relation to the standard from the age of 6 months onwards. At every age, the difference in the weight of boys and girls in the projects area is smaller than observed in the NCHS reference data. The data also indicate that the weight gain of the project area children was short of the NCHS standard more during the first year of life than during the next four years. Within the first year of age, the growth shortfall was somewhat more during the second half or the post-weaning period; the difference was particularly marked in the case of girls.


Notes:


(refers to numbers in small print) 1. Besides rural Kachchh, an excess of females in the population was reported by only one other area-rural Valsad district (Sex ratio of 993 males per 1000 females) according to the 1981 census.

2. According to our baseline survey, nearly 30 percent of the births of the previous one year in the project area took place either at Ratadia hospital or at a similar institution elsewhere. This figure is quite high for a rural area of Gujarat. In rural area of Gujarat State as a whole, the Sample Registration System has reported less than 13 percent of the births to be occurring in an institutional setting during 1982 and 1983 (SRS 1982, 1983).

3. The hospital nurse generally weighs the new­born on a spring baby weighing scale which is calibrated for 50 gms. 4. Prima facie, the implied sex ratio at birth of 1154 boys per 1000 girls appears implausible and needs to be investigated. 5. An effort is under way to trace each of these children to find out how they have fared later in terms of their weight and survival.

6. Barots in Kachchh are largely engaged in animal husbandry and maintain large herds of goats and sheep. They also own some milch cattle as well as land which is generally un irrigated. The Barot women are quite active in tending the cattle and also appear to enjoy decision­-making power in household matters including marriage.

7. For a discussion of the NCHS data, and their advantages over other similar large data sets, see : (Waterlow, 1977). (Contd. in the next issue)

5

Book Review Health and Family Planning Services in India An epidemiological, socio-cultural and political analysis and a perspective; D. Banerji; Lok Paksh, New Delhi 1985

This book is a critical overview of the develop­ment of Health Services in India since Indepen­dence and is a culmination of years of effort by the author, of attempting to provide a framework of social analysis for the numerous, often ad hoc and empirical, health service policy options that health planners have pursued. At the outset, therefore Prof. Banerji needs to be congratulated on this voluminous effort.

Over the last two decades he has emerged as a sort of devil's advocate for health planners in India. So a book such as this would be welcomed both by those who share many of his views and those who find them irksome.

The book is divided into six main sections. The first section deals with the broad socio­political context of health service development in India and provides a historical view from 'colo­nial times' till the health policy declarations of the 1980's. In this section the dichotomy between the needs of the masses and the elitist orientation of the evolving framework of institutions, techno­logy and manpower development is brought out. Prof. Banerji's assessment of this dichotomy is known to most of us through his earlier writings many of which have also featured in mfc bulletins and the first two anthologies. However some new issues are highlighted.

The health policy options of the national move­ment make interesting reading. The conclusion that recent policy shifts indicate that there are 'forces of democratization within the country' that are impelling the leadership to promise a better deal for the people in future, the elitist orientation of 35 years of health planning notwithstanding, is significant especially when many critics would pass these off as populist aberrations. One wishes that Prof. Banerji would provide more substantia­tion for this interesting hypothesis. The sections entitled the Relevance of vertical programmes(which actually analyses the irrelevance) is a good resume of the history and contributory factors which ultimately lead to the acceptance of the horizontal integration approach and the MPW scheme. Prof. Banerji's undiluted affection for the socio-epidemiological foundations of the National T B programme continues, failing once again to explain why the same approach did not develop further or get accepted as the basis of other pro­grammes as well.

The influence of social and political forces and the intervening role of foreign agencies on the evolving dynamics of the Family Planning Services in India are well highlighted and the need to make a more thorough study of regional demography stressed. However the conclusions that the mana­gers of these programmes should have a wide range of interdisciplinary competence is not backed by adequate indication of, what the components of such competence could be. Section Four deals with, the evolution of the primary Health Centre, CHW scheme, MCH Services and Medical Care institutions. The chapter on social science inputs and the review of their contribution as well as the inadequacies of the attempts of social scientists to modify the prevailing ethnocentric world views, is a key chapter. While most of the other chapters in this section are a bringing together of many of his earlier writings one had hoped that chapters like the Analysis of Primary Health Care in India and even more so the one on 'Research and Deve­lopment' would have more substantiation.

Section Five deals with intersectoral programmes for health and covers a wide range of issues like the evaluation of ICDS scheme, the political eco­nomy of nutrition, community participation in sanitation programmes, socio-epidemiological issues in water supply programmes, environmental conditions in industry and the Kerala case study. While all of these are key issues the overviews are too superficial lacking both contextual informa­tion or references, thus verging on journalistic accounts rather than the rigorous approach that such a scholarly perspective demands.

Section two and three deal with, National Communicable disease programmes and the National Family Planning programmes. Here the somewhat adhoc foundations and development are outlined but one is uncomfortable with the rather uncritical use of available health statistics from various government reports.

The final chapter on Health Planning and Alter­native approaches leaves one with rather mixed feelings. The ICMR / ICSSR report gets a thorough critique, as usual. highlighting its contradictions and inadequacies. The innovations and alternate approaches arising from the increasing number of non-governmental voluntary agency projects are dismissed somewhat perfunctorily as irrelevant.

6



.­ But what follows as the suggested alternative approaches to health service development is a series of exhortations like starting with the people; democratization of the community; a systems approach; involvement of democratic institutions at various levels of the administration; readjust­ments in the culture of hospitals and so on with little detail of how to actually go about doing it. Many of us in mfc would definitely agree with the philosophical content of those exhortations-our pamphlet is full of them already! However what many of us would have hoped from such a book beyond its critique of the present situation and policy would have been a series of more concrete policy options that could be explored at various levels of the existing services, so that more mean­ingful formulations of policy would emerge in the future.

Maybe it is unrealistic to expect that these details would be forth coming in a book, which is a Himalayan overview of the situation in India. Perhaps it is left to all of us who are building approaches, attitudes and action from the grass roots in the community-to outline these compo­nents over the next few years from the successes and failures of our action. Prof. Banerji's book can therefore be considered a prophetic signpost in the direction of the evolving community health in India. An important milestone no doubt, but equally and clearly an indication that much home­work still needs to be done by all of us who claim to be committing our energies to building a more relevant health service and medical education system in the country.

While one welcomes the formation of a forum like Lok Paksh-a publication forum avowedly committed to take the side. of the oppressed and dispossessed-one cannot but help take up issue with the forum for the cost of its first publication. Three hundred rupees in the Indian context empha­tically determines who can and cannot have access to this book. This book may well remain in the dust-filled shelves of centres and institutes where 'the lag between the socio-cultural aspira­tions of the people and their articulation by the political leadership and health teams' are already well established.

	Ravi Narayan, Community Health Cell, Bangalore. (Contd. from p. 8)

'indigenous' use for this drug is for the postpone­ment of menses (menstruating women being consi­dered a taboo during festival occasions). Since EP drugs can bring on or delay uterine bleeding 'symptomatic' relief is being provided without the need to diagnose or treat the cause. The patient is happy, albeit for a little while, and the doctor is saved from the laborious process of diagnosis. It is no wonder that the medical community that generally prefers to sit on the fence, as amply illustrated by the case of sodium thiosulphate therapy to the Bhopal gas victims, is rising up in arms, taking the effort to write letters to the Drug companies and the Drugs Controller as well as spending precious time off from private practice to give evidence in the Public hearings in defence of this irrational and hazardous drug!

Whatever be the final verdict on the EP drug, the campaign against it has succeeded in smoking out the vested interests in the medical community and has brought out into the open the collusion between the Drug Companies and the Medical Establish­ment in un-equivocal terms.

—Sathyamala

For those of you who are interested in the Technical case against EP drugs, a drug infor­mation pack is available from 'Growth Initiatives', C-4-78/2 Safdarjung Development Area, New Delhi-110016, at the cost of Rs. 25/ - postage inclusive.


BHOPAL: Nature of Health Damage Unveiled

Published by : No More Bhopal Committee, West Bengal, Price Rs. 5.00

Available at : New Book Centre and Peace Pub­lishing House, 14 Ramanath  Majumdar Street, Calcutta- 700009



7 From the Editor’s Desk

	Last year, as directed by the Supreme Court, in the case of Vincent Panikulangara versus Union of India & Ors, the Drugs Controller held four Public hearings to decide on the ban of the high fixed dose estrogen-progesterone combination drugs, popularly known as EP drugs.  In 1975, following the decision by the Australian govt. to ban EP drugs, the Who informed all its member countries of this decision and advised that these drugs had no indication in gynae practice and because of the possible association with congenital malformation when given in the first trimester of pregnancy, were to be considered non-essential and hazardous.  In response to this, the Drug Controller’s office took the soft option of issuing a notification to all the manufacturers that package inserts and promotional literature should carry a warning against its use in pregnancy.

In 1980, the issue was taken up in a major way by several Health groups, Women's and Consumer organizations in India who focused on the widespread misuse and the potential teratogenicity of the drug. Since then a protracted battle has been on between these groups and the companies manufacturing EP drugs. The resolution of the issue is now not a matter of settling the question of the drug's safety and efficacy alone, but would form an indication of the balance of power between the Drug Companies and the Medical Community on the one hand and the emerging People's Health Movement on the other.  

	In the Public hearings held by the Drugs Controller, doctor after doctor (Allopaths with a long list of qualifications) spoke up in favour of the drug. The Drug companies (Organon and Unichem) submitted as evidence testimonials from allopathic medical practitioners who in their opinion "felt" that the drug was absolutely safe as they, in their long years of practice, had not seen even a single case of congenital malformation with EP drugs and who, again in their opinion, felt, that the ban of this particular combination would lead to a complete collapse of gynae practice.  More recently, in the information folder that Unichem is circulating, entitled ‘Facts and EP Combinations’, many eminent gynaecologists have lent their names in support of this useless drug.

	In contrast to these 'character certificates' issued by the eminent gynaecologists, in the medical text books (including the one written by C S Dawn, a vociferous supporter of EP drugs at the Public hearing), and current medical literature, cited as authoritative sources by the groups fighting for the ban, EP drugs do not find a place. In the 1977 edition of Chamberlain's 'Contemporary Obstetrics and Gynaecology', a passing reference is made: "Substitution therapy in long standing secondary amenorrhoea has also been used (Zondeck 1942, Finkler 1944). A two day treatment of daily injections of estradiol benzoate and progesterone was followed by menstrual bleeding".

	Provision of symptomatic relief is the foundation on which the highly competitive private medical sector exists. The quicker the relief, longer the doctor-client relationship, is the maxim that every 'good' private practitioner adheres to. Hence we see examples of 'uses' for drugs that may have become obsolete or are not to be found in literature as an indication an example being the prescription of anabolic steroids to help Paediatric victims 'gain weight'. The widest 'use' that private practice has found for EP drugs is in what is euphemistically termed menstrual regulation. Under this indication, the drug is being prescribed for primary amenorrhoea, secondary amenorrhoea, dysfunctional uterine bleeding, irregular menstruation as well as for inducing abortions, A truly

(Contd. on page 7)


