---
title: "Sex Diffrentials In Nutritional Status In A Rural"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Sex Diffrentials In Nutritional Status In A Rural from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC137.doc](http://www.mfcindia.org/mfcpdfs/MFC137.doc)*

Title: Sex Diffrentials In Nutritional Status In A Rural

Authors: Visaria Leela

﻿


137

medico friend circle bulletin

FEBRUARY 1988



Logistic Support and Facilities for Primary Health Care (Crucial Role of Physical Accessibility)

Ashish Bose

In the rhetorical discussions on the Alma Ata Declaration of Health For All By 2000, not enough attention has been paid to the crucial role of physical accessibility. The Alma Ata Declaration (1978) does show awareness of the fact that "the success of primary health care depends on ade­quate, appropriate and sustained logistic support.." and recommends that "government ensures that efficient administrative delivery and maintenance services be established reaching out to all primary health care activities at the community level.... That the government ensures that transport and physical facilities for primary health care be func­tionally efficient." "The global strategy for health for all by 2000" adopted by the World Health Assembly in 1979 indicates that "the main thrusts of the strategy are the development of the health system infrastructure starting with primary health care for the delivery of country wide programmes that reach the whole population." (WHO, 1981 : 12). In the international literature much is made of the Kerala model. Several Sophisticated statis­tical exercises have also been made on the impact of literacy, education, status of women, higher age at marriage and related factors on the fertility pattern. However, there is no evidence that these exercises have taken adequate note of the human settlement pattern and the access to health and education which the state of Kerala has, by virtue of its unique settlement pattern where one can hardly distinguish a rural area from an urban area. This is partly a function of density of population.

But a number of historical, geo­graphical, political, economic and other consi­derations have influenced the settlement pattern of Kerala. For example, according to the 1981 Census, in Kerala, 90.3 per cent of the rural popu­lation was enumerated in villages with population of 10,000 and over. The comparable figure for Bihar was 4.2 per cent; Madhya Pradesh 0.1 per cent; Rajasthan 0.9 per cent; and U P 0.7 per cent. In terms of the number of villages (in Kerala) 74.3 per cent of the total number of rural settlements belong to villages with population of 10,000 and over. The comparable figure for Bihar is 0.3 per cent; Madhya Pradesh negligible; Rajasthan 0.1 per cent and U. P. 0.1 per cent. These figures bring out the sharp contrast between Kerala on the one hand and large states like, Bihar, Madhya Pradesh, Rajasthan and U P (which will be referred to subsequently as BIMARU states), on the other.

As is well-known, in our strategy for primary health care, we have more or less adopted a blanket approach throughout the country (though there are some minor modifications in tribal and hill areas) by laying down norms for the establish­ment of primary health centres, sub-centres, etc.

The computational work in connection with this project was done at the Computer Unit of the Institute of Economic Growth. The author ac­knowledges the assistance given by K. Lal, Pro­grammer and Jatinder Bajaj, Senior Research Analyst Population Research Centre. 








Sex Differentials in Nutritional Status in a Rural area of Gujarat State

PART II

Leela Visaria

Medical Intervention in the Event of Fatal Illness

A review of studies and our own data on nutri­tional status provides little clear evidence of a discriminatory behaviour towards girls. The question then arises; are there differentials in the prevalence as well as the incidence of infant and childhood illnesses? Unfortunately, the relevant information on morbidity patterns in our field area is not yet analysed. However, what is available is the information on infant and child mortality, from our efforts at continuous recording of vital events. We have tried to ascertain from the parents of each deceased child, the symptoms preceding the death of the child, the medical help sought (if any) along with the names of the practitioners and wherever possible the actual treatment. One of the doctors attached to the project in Ratadia has examined these records to indicate further whether and to what extent the cause of death can be diagnosed from the reported symptoms.

The data relating to the infant and child deaths during the 15 months since our benchmark survey conducted during June-July 1985-upto Septem­ber 30, 1986 are summarized in Table 3. The number of female infant deaths was two and a half times that of male infants. But, yet, because of the small number of events, it is difficult to generalize about the sex-bias in the incidence of fatal illnesses or in the utilization of medical help prior to death. However, diarrhoea, infections of respiratory tract and post-measles complications account for close to 80 percent of all deaths. The remainders were due to congenital problems, prematurity or sheer "wasting away", presumed to be due to chronic infection.

Interestingly, the sex bias, which is believed to exist in the seeking of medical help, does not seem to be extensive here. In the case of three female child deaths, and one male child death the family had relied on home remedies only; the percentages of these deaths to total child deaths were 17.6 and 14.3, respectively. Yet, overall, young girls were also taken to the "doctors" outside the village, when there was no

"Health Medical Practitioner" within the village. (Out of nine villages for which data on death are presented, six had no "doctor" within the village, their resi­dents had to travel about 5 to 7 kms. to reach a village with a medical practitioner. Each village is connected with Ratadia by bus communica­tion, with at least one service per day.) In the case of four girls, parents even went to the district head quarters (Bhuj) and to another town (Anjar) to consult qualified trained doctors. Perhaps, our study region is a typical relative to other parts of the country. The contacts with urban areas as well as outside world have perhaps influenced the perceptions of parents about seeking help in the event of illness. Yet, given the wide sex differentials in mortality, one cannot help wonder whether parents seek medical help for their daughters when it may be too late to do much to save their life.

Discussion On the whole, we cannot identify any clear sex-bias in health care or food allocation in our project area in Kachchh. Yet, the difference in the number of boys and girls succumbing to death appears quite so that one has a feeling that some­where something goes against girls in the process of growing up. The one dark area to which we referred earlier is the timely recourse to medical help provided to girls. Is it sought too late? In the instance of two girls (both belonging to the Rajput caste) we were told plainly that only home remedies were tried because they were girls. Strangely enough, one of the girls was a second parity child; the earlier daughter born to the mother also had died in infancy. The other girl was a tenth parity child, with two surviving sisters and six surviving brothers. (Another daughter had died earlier). In the case of a third child (fifth parity, with two surviving brothers and one surviving sister) belonging to a scheduled caste, poverty was apparently the reason for not seeking help from a private practitioner in the nearby village. While help at Ratadia is free,

4




­

Health Effects of Low-Intensity Warfare in Nicaragua: Results of a Pilot Study



The Nicaraguan Health Study Collaborative at Harvard, CIES* & UNAN*



"Low-intensity warfare" (LIW) is currently being promoted as a legitimate tool with which to advance foreign policy interests in Nicaragua. To understand the effects of LIW on the health and welfare of the civilian population of Nicaragua, we conducted a pilot study in two communities Acoyapa, in an active zone of war (Chontales Region) and EI Ostional, outside the war zone (Rivas Region). A broader objective of the study was to develop a generally applicable method for assessing the human consequences of LIW in other regions of conflict. A means for quickly assessing the needs of populations in regions of low-intensity conflict is essential if health pro­fessionals are to respond to the growing number of people living in countries stressed by this type of warfare.

This attack on production results in a loss of access to arable land, while stimulating the concomitant flow of rural civilians to population centers. Serious consequences of this disruption in the social structure of Nicaraguan communities are increased stress on the provision of food, access to health care and education. LIW as a foreign policy tool is therefore in direct contradiction to the goals of public health.



* CIES is the Center for Investigations and Studies in Health of the Nicaraguan Ministry of Health * UNAN is the National Autonomous University of Nicaragua

In a random sample of 88 households, a standardized questionnaire and psychological instru­ment were administered to the female head of the household. Height and arm circumference measurements were taken on children under 6 years of age to assess nutritional status, and vaccination status was determined. Community leaders, health workers, and teachers were syste­matically interviewed, and focus groups of the interviewees were convened. Local death records and regional morbidity and mortality data were reviewed.

The two communities studied differed in popu­1ation size, but were comparable in socio-demo­graphic indicators. The proportion of the popula­tion in each community having electricity, a latrine and running water was the same. Thirty-five percent of the households sampled in Acoyapa had experienced direct attacks or threats by counterrevolutionary forces, compared to 6% in EI Ostional. A significant difference was found in the rate of persons leaving agricultural production (18 % in Acoyapa and 2 % in EI Ostional). Dietary protein consumption vaccina­tion coverage and school attendance rate were all significantly lower in the active war zone community.

These findings suggest that UW in Nicaragua has the result, and probably the intent, of des­troying the agricultural production base of the civilian population by direct physical attack.

New International Newsletter on AIDS

Appropriate Health Resources and Technologies Action Group Ltd. (AHRTAG) is publishing, on a quarterly basis, a new newsletter focusing on AlaS, from Nov 1987. The newsletter will be similar in style and format to AHRTAG's existing publications 'Dialogue on Diarrhoea' and 'ARI News'. The newsletter aims to provide practical information, presented in a clear, straightforward language, about prevention and control of H IV infection and AIDS in the context of other primary health care activities.

The newsletter will be available initially in English, and in 1988, in French. Copies will be free of charge to readers in developing countries. A small annual subscription fee of £ 5.00/$ 10.00 will be charged to readers in Europe, North America and Australia. Write to AHRTAG, 85 Marylebone High Street, London WIM 3DE.

7




