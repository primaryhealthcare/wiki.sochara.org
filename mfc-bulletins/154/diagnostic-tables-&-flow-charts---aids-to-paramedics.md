---
title: "Diagnostic Tables & Flow Charts : AIDS To Paramedics"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Diagnostic Tables & Flow Charts : AIDS To Paramedics from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC154.pdf](http://www.mfcindia.org/mfcpdfs/MFC154.pdf)*

Title: Diagnostic Tables & Flow Charts : AIDS To Paramedics

Authors: Ashtekar Sham & Ratna Ashtekar

154

Medico Friend Circle Bulletin August 1989

DIAGNOSTIC TABLES AND FLOW CHARTS: AIDS TO PARAMEDICAL TRAINING PROGRAMME SHAM ASHTEKAR AND RATNA ASHTEKAR

Training programmes for paramedical personnel and village level health care workers, both in the state and private sector constitute an important area of activity towards the 'Health For All' strategy in the context of rural health care in the developing countries. Because of the direct exposure to the people and because of the poor doctorpopulation ratios in the rural areas it seems that they will not; only be carrying out their present assignments but should be required to take up various other duties, mainly of curative nature, both complementary and alternative to the medical services. But somehow the paramedical cadres seem to have been harnessed entirely to the preventive promotive programmes (even ignoring that curative services offer a very good spearhead for advancing comprehensive care) thus, lowering their de facto position to mere auxiliaries. Lately there seems to be some awareness on this issue and the literature prepared for paramedical training in the last decade is seen to carry sections on curative services, though actual practical training is only notional. The VHG (village health guide) scheme in India also could not escape this pattern. As for the actual syllabi in curative services there seem to be a number of unresolved issues that include identification and understanding of ailments, selection of remedies, margin of risk, linkages with referral services and official and elite postures towards this 'indulgence in medical enterprise' by the paramedical personnel. This discussion is limited to only two of these issues: Classification of ailments from the point of view of paramedical training-programme-potentials and identification I diagnosis of the ailments in view of paramedical training programme. The training literature for paramedical cadres is replete with understatements and overstatements on the role of paramedical and this article attempts to suggest some steps in this direction also. Classification of ailments/diseases in the context of the potential of the paramedical programmes: The essentials of such classification should be: a) Overall feasibility to diagnose the condition; b) Overall feasibility of treating the condition in his/her situations; c) Overall risk involved in the condition itself as well as its management in the circumstances, and; d) The prevalence factor.

Considering these factors, a 'feasibility classification' can be worked out that should help to ascertain the limits for the effort, referral end risk that these cadres can take, It would take a lot of quantitative studies to decide the actual weight age to be given to each of these factors and a lot of variation is expected with local conditions as well as the overall status of paramedical infrastructure and the technology, transport and referral back-up available to them from country to country. The classification shown in table-1 is prepared with the general Indian background and should only assist in illustrating the case. This classification should also help to decide the 'package of curative practices' mandatory for the paramedical training programme. A case in point is snakebite. Conventionally, the paramedical person is simply asked to offer first-aid to the victims and usually this includes reassurance, bleeding out the poison and stopping the spread by a tourniquet. With the rationale of this classification it can be argued that the risk involved in giving Anti-snake venom injection at the periphery is surely much les than transporting the victim without it; and therefore this procedure should find a place as a first aid measure in paramedical curricula in view of the sizeable mortality because of snakebite and with the kind of transport infrastructure we have But the ultimate listing and grouping of ailments must be subject to policy decisions at the responsible level.

Identification/diagnosis of ailments/conditions With the existing training programmes, this area is a relatively abandoned one, despite the fact that diagnostic flow-charts appear in some works. It is time that we abandon simplistic and symptomatic entities and confronts the need to attempt a reasonable degree of break-up for symptoms if one is serious about linking up the first-contact care with the referral care, with rational roles for both the tiers. Therefore use of flow-charts is a welcome sign. For reasons of visual clarity and linearity in decisions, flow-charts offer a great device. But there are some hamstrings for the use of flow charts which need careful examination. The accompanying diagram shows the flow chart we have prepared for the presenting symptom of 'Fever in Adults'. The main theme here is to split the spectrum of fever conditions by an important qualitative variable one of which is 'cough' in this case. Further break-up is made by using important variables for both 'coughs' & 'non coughs: The nature of these subsequent variables is both qualitative and quantitative and in reality, the diagnosis of many of the listed conditions depends upon many other variables not shown in this flow-chart. As a result, a number of problems arise that question the worth of flow charts and the main difficulties arise from over diagnosing or missing the condition (Type I and Type II errors) Here are a few examples from the flow-chart: i) The case for hepatitis banks on yellow sclera which essentially occurs after the serum bilirubin crosses a certain threshold. Other criteria like nausea vomiting, right hypochondriac tenderness, anorexia etc can sufficiently suggest hepatitis even in absence of a distinctly yellow sclera.

ii) Neck stiffness/rigidity is not so easily decided and meningitis is too important a condition to be missed or over diagnosed just because the test is found to be (false) negative or (false) positive. iii) In situation like 'common cold,' element of URI is added later on in few cases so that 'cough' would appear after some days. In a flow-chart this time-scale can be accounted for.

In all these cases, making a diagnosis by exclusion of a few things and detecting presence of a 'special' criterion is attended by errors and in the long run this would jeopardise the fidelity of the paramedical cadres, the viability of which is so much important in all the developing countries.

Alternative device: Diagnostic Table. With this background of problems encountered with a flow chart, a diagnostic table can be viewed as an alternative device or even better as a referral text, to be used in combination with the flow-charts... Table-2 is an attempt to prepare one such model table. The main theme is to present a more comprehensive picture by accounting for more variables and the variation in their frequencies. Thus from this table it is seen that typhoid fever always brings headache, body ache and continuous high fever while chills do appear sometimes and that anorexia is common. This collective information read with the specials features like relative bradycardia and prostration should help to decide about typhoid fever with much more certainty. The fact that abdominal pain appears late (suggested by the letter 'L' in the table) in the episode adds a time scale variable to the information in the table. Thus the diagnostic table has an inbuilt flexibility and comprehension greater than what a flow chart can offer without being too intricate for the purpose. The possibility of either over diagnosing or missing a condition must necessarily be less with the diagnostic table than a flow-chart. (Fever is perhaps the most complicated phenomenon in the array of presenting symptoms here). Further diagnostic tables can offer comprehensive information about the clinical picture of a condition in a single line taking into account qualitative, quantitative and time scale variations of a symptom size in the given direction & can accommodate local epidemiological changes with great ease. However, a general usage of such tables and flow charts must await trials in actual field conditions and for various cadres differing in levels of education. In general it can be said that a combined presentation (diagnostic table coupled with flow-charts) should answer the needs for both clarity and comprehension that the training demands. Accounting for many variables and variation is essence of computer-aided diagnosis but with the tables of this type one can conveniently use and present clinical information to suit the circumstances of paramedical training programmes. **

Table- 1

CLASSIFICATION OF AILMENTS FOR PARAMEDICAL TRAINING PROGRAMME Group of ailments

Diagnostic Feasibility

Treatment Feasibility

Safety Prevalence Factor Factor

Ailments

1. Minor ailments

""

""

""

""

Common cold, minor cuts, headaches, constipation, fungal infection, scabies etc.

2. Major ailments

"'

"'

"'

"'

Diarrhoea, dysentery, URTI, malaria, otitis media, vaginitis, hyperacidity hepatitis, etc.

3. Serious ailments

"

"

"

"

Pneumonia typhoid, fever Acute abdomen Diphtheria, tetanus etc.

meningitis,

4 Important chronic " conditions that need early detection and health education.

"

"

:

Tuberculosis, leprosy, filariasis cancer, etc.

5. Acute emergencies ' referral

'

'

' Snake bites, Burns, that need first-aid and Severe dehydration; major accidents specially involving brain, chest abdomen and haemorrhages etc.

Note: 1) The examples under 'ailments' heading are not a compete list but only a few cases for illustration. 2) The difference between category 3 and category 5 is that in category 3 (serious ailments) there is little scope for first aid while in the later first aid can often save the patients. 3) The diagnosis of, say snakebite, is easy but that of its effects is difficult and hence the overall diagnostic feasibility is poor. 4) Category 1 can be safety attended [by the paramedical, while category 2 should be attended with caution watching for indications for referral. Category 3 should be immediately referred to medical experts. Category 4 needs high suspicion index for early detection supervision over the treatment and health education and category 5 assisted with first aid before sending for expert care.

Dear friend, This is in response to Anant Phadke's calculations and recommendations regarding iron supplementation in pregnancy. (MFC bulletin, May '89). The recommendations for the national programme were made not with a view to improve the general nutritional status or even the widespread problem of anaemia. The aim is to improve the iron status quickly, to substantially reduce the maternal mortality quickly, much of which is believed to be the result of complications of anaemia. The aim is to raise Hb levels to 10-12 gms /dL The average in a normal female population is not 14.5 gms /dL but 12 gms/dL) With. a deficit of 2 gms (Hb S gms as Anant has taken) and a blood volume of 4 litres, the deficit is SO gms Hb, that is 50 X 3.4 = 272 mg iron. Since no woman is totally devoid of iron stores, replenishment is taken as 50%: that is 500mg and not 1000 mg. The daily total requirement during pregnancy is 4.5 mg and additional requirement is mainly needed in the last two trimesters of pregnancy and this works out to 900 mg Thus the total iron needed would be 272 + 500 + 900 = 1672 mg or about S mg iron per day for the last 200 day. Even if the diet were to provide only 20 mg (the lowest intake) and with only 10% absorption, the woman gets a minimum of 2 mg iron from diet. She needs an extra 6 mg per day or 1200 mg for pregnancy. The iron tablets with 60lmg elemental iron and 20% absorption will provide 12 x 100 =1200 mg. Instead of providing 6 mg for 200 days, 12 mgs are being given for 100 days because most of the women who ever seek antenatal care come to MCH centre only during the last trimester of pregnancy. The reasons of this are well known and also discussed by Manisha Gupte in the same issue of the bulletin. We must remember this is a national programme aimed at helping the maximum nUJ1lberand does not take in to account individual cases. The tablets are to be doled out by ANMs. Any individual case that needs earlier attention has to be taken care of by the medical officer and nothing prevents him/her from giving fersolate tablets from the general stock. — Kamala Jayarao ** Book Review NOTIFIABLE DISEASES: SIGNS, SYMPTOMS, DIAGNOSIS. Prepared by Vijay Kanhere, Society for participating research in Asia, 45, Sainik Farm, Khanpur, New Delhi 110062. Unpriced. pp 36 "The incidence of notifiable diseases recorded by the factory inspector is insignificant, less than a few hundred. It does not mean that working conditions in India are extremely good and few workers are affected by the notifiable diseases", begins the introduction of this very useful compilation of signs and symptoms of diseases of

work. Notifiable diseases ar9 those occupation related diseases which the employer and / or the treating doctor is / are required by law to notify to the factory inspector but rarely do. The state of affairs is quite bad as far as organised sector is concerned but it is dismal in the unorganised sector Workers employed in the handloom industry, small tanneries, Sindoor manufacturing, quarrying, slate making industries and so on suffer and did of these diseases unnoticed and unnotified: A part of the responsibility of such a situation lies with the medical profession. No recognition of an occupational problem in a patient is a consequence of - besides other factors-the lack of the training of the physician to do so. Though more often than not it is factors such as a callous attitude towards a worker patient, a cursory clinical examination and an irrational faith in shot-gun therapy are at work. However, it cannot be denied that during an undergraduate training of the doctor, occupational diseases receive little attention either during the lectures or during the 'clinics'. This Topic is dealt with separately and at some length, only under preventive and social medicine, a non-clinical subject. Thus clinically a medical student remains ill-informed on this topic. It is for those of the medical profession and also for those involved with the problems of the worker's health who have been exposed to such problems but feel ill-equipped even to suspect the culprit, that these reference sheets will come in handy. Organised on the basis of the diseases specified as Notifiable Diseases under the schedule (Factories Act, 1948,) the presented in a tubular the information form-the tables showing the industry of occurrence signs and symptoms and the diagnosis, forms a useful guide)o locate a particular health problem. The diseases (strictly speaking. health problems) covered include those caused by compounds of metals such as mercury, lead, chromium and arsenic, other chemicals such as nitrous oxide, carbon disulphide, benzene, halogens and halogen derivatives of hydrocarbons. Also included are specific occupational diseases like silicosis, byssinosis, asbestosis and pneumoconiosis. As a useful addition, a sheet on exposure limits as suggested by the international Labour Organisation is given at the end. Despite its usefulness as a reference manual the hand Book in its present from suffers from one drawback. It is not like that it appear (s) more complicated and technical and thereby less useful to workers"... as the introduction indicates. More than that the information is not presented in the form that takes a clinician's view of the patient. When facing the patient a clinician does not start with the causative agent and then looks for the signs and symptoms but the other way round, To be more specific, the information should be presented in a manner wherein certain symptoms/signs are correlated to diseases causative agents and the industries where they are used. An example on page 23 (eczematous lesions) serves to drive this point home. Again finally, it lacks the necessary illustrations-photographs or the line drawings of typical symptoms, pathological changes, radiograph and so on-to become a complete handbook of occupational diseases for physicians. The above mentioned is being suggested as an addition to what already appears and not as substitute for it. However, all this is not to belittle the significance and the usefulness of these sheets. After all as the introduction mentions, 'it is just a beginning.' we can look forward to others to follow. — DHRUV MANKAD **

MEDICO FRIEND CIRCLE BULLETIN Editorial Committee: Abhay Bang Anil Patel Binayak Sen Dhruv Mankad Dinesh Agarwal Padma Prakash Sathyamala Vimal Balsubramanyam SP Kelantri, editor

Views and opinions expressed in the bulletin are those of the authors and not necessarily of the organisation


