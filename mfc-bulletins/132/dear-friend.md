---
title: "Dear Friend"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Dear Friend from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC132.doc](http://www.mfcindia.org/mfcpdfs/MFC132.doc)*

Title: Dear Friend

Authors: Warekar

﻿­

­

'­

132

medico friend circle bulletin

SEPTEMBER 1987

....


The Epidemiological Approach: Its Elements and its Scope

Ritu Priya




Epidemiology is defined as the study of the dis­tribution and determinants of disease frequency in man (1). This involves study of its distribution in time and place, in terms of its quantity, in terms of the persons affected, their age and sex distribution etc. The determinants of disease are the causal factors-factors involving the agent, the host and the environment and their Inter-linkages in the 'web of causation'.

The Approach Epidemiology is basically an approach to studying any health problem of a population, commonly applied to studying the causality of disease. The approach primarily involves studying the problem as it exists in the population, in all its various aspects, and then using this information as the basis for understanding the causation of the problem; very often with the additional perspective of trying to identify means to prevent or control or otherwise effectively deal with the problem.

Principles underlying the approach are : (l) It is a comprehensive, holistic approach; taking into account the various dimensions of the problem-physiological, pathological, demographic, environmental. Based on the understanding that human disease is related to man's environment, it involves study of the population's physical, chemical, biological, social, economic and cultural environment. It thus deals with both medical and social sciences and uses the tools of both. Its field of enquiry will extend "from the molecule to the social group, from the prevalence of sickle cell trait to the conditions of industrial morale,"(2).

(2) It deals with large populations and not with individuals. Thus, for example, the difference in deal­ing with the natural history of a disease. In an in­dividual, natural history of a disease connotes the process of pathogenesis of the disease-from pre­-pathogenic phase, to incubation period, to early discernible lesions, to advanced disease. Epidemiology studies this in order to explain the behaviour of the disease in society, its distribution over time, its seasonal variations, Its geographical distribution, the characteristics of persons affected, the conditions for variations in natural history in individuals etc. It is interested in "community diagnosis' just as clinical medicine is interested in diagnosing the health problems of the individual. It is a' reconstructive science' in contrast to the laboratory approach of studying minute components of the whole.

(3) It depends upon the quantifications of pheno­mena using statistical concepts and tools. This helps in a systematic and 'objective;' collection and representation of data, in drawing inferences and establishing associations, in generalizing from partial data, and in knowing the possible degree of error in one's deductions.

The health status of a population is measured in terms of morbidity and mortality rates. Causal relationship between events is established by proving a statistically significant, statistically determined association between them and then substantiating this association with other evidence like time sequence, strength of association, existing knowledge about the mechanisms involved, or by direct experiments.

(4) It tests its hypotheses by experiments, even on human populations, in addition to using the various tools used by other disciplines. The 'epide­miological experiments' involve comparisons between a study and a control group or between two groups subject to conditions differing only in the 'one factor under study. These groups may be created by natural conditions or by direct, planned intervention.

(5) It deals with dynamic processes and therefore has to keep a constant track of changes in disease patterns, in health status, and in the various factors influencing them. To be able to do this it has also to keep testing and altering its specific tools, evolving new tools, and seeking out new indices of health and disease.

Epidemiology can help in :

(1) Understanding disease processes in popula­tions-the history of the rise and falll.1f diseases in specific societies and changes in their character. This will help in understanding present health prob­lems and in making useful projections into the future. For example, McDermott (3) uses the morbidity rates of various infectious diseases in the USA at the begin­ning of this century to show that these diseases had begun to decline even before specific therapeutic and preventive measures for them had been discovered. He explains this trend by the general improvement in socio-economic conditions and thereby rise in the standard of living. This understanding should help in building our overall perspective towards preven­tive measures, emphasising the importance of socio­economic measures over medical technology based solutions.

(2) Identifying causes of a disease and establishing their relative importance (multiple-cause theory) by studying the incidence in different groups, defined in terms of their composition, their inheritance and experience, their behaviour and environment. This knowledge will help in evolving means of treatment and of undertaking the various levels of prevention.

(3) Planning, organising and monitoring health services by diagnosing the health of the community in terms of incidence, prevalence and mortality; to define health problems for community action, and their relative importance and priority; to identify vulnerable groups needing special protection.

Studying the working of health services with a view to their improvement. Operational research translates knowledge of community health in terms of needs and demand. The supply of services IS des­cribed and how they are utilised, their success in reaching standards and in improving health ought to be appraised. All this has to be related to other social policies and to resources.

(4) Furthering the knowledge of individual di­seases and of medicine as a whole by completing the clinical picture of diseases and describing their natural history on the basis of study of a large number of cases of all kinds, and by following the course of remission and relapse, adjustment and disability (by detecting early sub-clinical disease and relating this to the clinical and by discovering precursor ab­normalities during the pathogenesis) ; identifying syndromes by describing the distribution, association and dissociation of clinical phenomena in the popu­lation; estimating individual risks and chances, on average, of disease, accident and defect; testing hy­potheses and techniques emerging from laboratories and clinics about causality, about risk factors, about effectiveness and side effects of new drugs etc.

Some Potentials and Limitations in the Scope of the Epidemiological Approach

(1) All 'Disease' and no 'Health': Having started as the study of epidemics per se, epidemiology has extended to the study of disease in non-epidemic times and to chronic diseases; and from the study of infectious disease to those non-infectious in na­ture. Having thus been concerned primarily with disease, a further advance in its scope is study of the positive components of health and its determinants. This also involves redefining 'health'. There are some who think otherwise. While ready to concede the application of epidemiology to 'other biological processes, including growth, multiple pregnancy, sex determination, intelligence, and fertility", they do not think it important enough to study 'health.'

It is sometimes suggested that epidemiology should also be concerned with the positive components of health implicit in the definition used by the World Health Organisation. According to this definition, "Health is a state of complete physical, mental and social well-being and not merely the absence of disease or infirmity".	However, the number of widespread and serious diseases of which the etiology is unknown is more than sufficient to occupy epide­miologists for many years to come. Concentration of effort on these diseases appeals to be indicated by the urgent need for knowledge leading to their prevention, as well as by the practical difficulties in quantitative investigation of concepts that have not been defined in clinical, pathologic, or other operational terms.

.....

2

'" Even a 'progressive' book like Morris 'Uses of Epidemiology' (2) does not think of positive health as worth mentioning, Besides the above view, epi­demiology appears to have an inherent limitation in studying 'health', because of its basic reliance on sta­tistical measurements, Disease and death are easier phenomena to quantify than health. Health remains more qualitative than quantitative. May be it is possible to evolve indices of 'positive health' and attempts should be made in that direction. Nine­teenth century medicine, by contrast, with its empha­sis on the normal functioning of an organic structure, required a knowledge of physiology for its practice. It was on this view, the life sciences in the nineteenth century were built, not on the comprehensive and transferable nature of biological concepts, but on the opposition of health and illness.



Turshen (3) shows how the clinical picture of health dominates disease and the individual IS placed at the centre, and health suffers. He argues that this has come about as a result of development of the capitalistic mode of production in the industrialized countries.

He explores the limits of the clinical paradigm that has defined disease and health for centuries. This paradigm takes individual physiology as the norm for pathology (as contrasted with broader social conditions) and locates sickness in the in­dividual's body. A typical nineteenth century va­riant held that every illness was the disturbance, exaggeration, diminution or cessation of a corres­ponding normal function. In this view treatment readjusted the body until its physiological norm was restored, a mechanistic approach that reduced the body to a machine whose organs could be discretely examined and regulated. Implicit in this notion was the concept of health as the absence of disease. No positive concept of health was advanced.

The clinical perception of disease could not have emerged in the nineteenth century if the science of quantification had not been developed earlier, since it depended on operational verification by measure­ment, clinical study and experiment, and evaluation according to engineering norms. The medical quantifiers of the nineteenth century placed sickness in the center of a medical system that was a mecha­nised framework for the investigation of the mecha­nical troubles of the human body.

Foucault, in his study of the origins of modern medicine, makes the interesting observation that, until the end of the eighteenth century medicine was more concerned with health, with qualities of vigor, suppleness and fluidity that were lost in illness and had to be restored, than with normality, an analysis of regularity, the search for functional deviation, and the return to an equilibrium,. Foucault suggests that from this early concern for health there followed not only an interest in nutrition but also the possibility of self-help, since the sick person could treat himself or herself by following a certain diet.  

Social and preventive medicine extended the clinical model in the direction of health, expanding its application from the individual to his or her family and immediate environment. Environmental sani­tation reflects a further extension to the wider phy­sical milieu: environmental sanitation is the study of disease based on bourgeois epidemiology, i.e., the classical triad-host, disease agent, and environ­ment. It is in no sense a study of collectivities. Insofar as these disciplines remained dominated by the clinical model, none seems to grasp the notion of collectivity, without which there can be no ade­quate definition of health.

A medical paradigm that is not holistic and col­lective produces only an inexact and inadequate body of medical knowledge.

This suggests an additional proposition-that medicine's failure to develop a positive definition of health results from the individualistic and ideological bias that pervades medical research and medical practice, structural relations between practitioners and patients, shapes the approaches selected for treatment (eg. chemical or surgical intervention) and the technology employed, and rejects the initiation of collective social action by communities.

The closest 1 have ever come to finding a positive definition of health is the following formulation: Marx regarded as the aim of the socialist movement "a society, in which menliberated from the 'alienations' and 'mediations' of capitalist society, would be the masters of their own destiny, through their under­standing and control of both Nature and their own Social relationships."

Thus while one mayor may not agree with all his propositions, one cannot deny the need for a change in the basic perspective of medical science today, specially in the light of even the 'establishments' professions of emphasis on community participation and the attempts by 'non-establishment' to initiate community action in health.


3

',. (2) 'Holistic', but no 'Political Environment’: Epidemiology has from the beginning had the basic understanding that human disease is related to the human environment. The environment has from the earliest (even Hippocrates) been taken to include the physical and geographical, the biological and the socio-cultural (food habits, addictions, phy­sical exercise and labor). It still remains primarily restricted to these (and in fact even the socio-cultural aspect is taken in a very limited sense). But with its holistic approach it can, and must, cover more and more the varied and complex social economic and political dimensions of the human environment.

Attempts are being made in recent times to add to the dimensions under purview of epidemiology by naming new aspects like 'medical ecology' (the study of human disease in relation to physical, bio­logical and social environment) and studying the 'political economy of health' and the 'political ecology of disease.'

One can start with the understanding, as Turshen (3) does, that, "the theoretical assumptions on which medicines is based are subjective	"Both scienti­fic and medical knowledge depend on material production and reflect the social organisation of that production, not on historical objectively". It fol­lows then "that contemporarily medical definitions of health and disease are inadequate because they are abstraction derived, for specific historical reasons, from the clinical study if the individual". The study of these social, economic and political reasons thus becomes necessary for understanding the existing medical perspective and for evolving the 'new' per­spective.

The definitions are inadequate expressions of the relation of medical states (illness) to reality since individuals are not clinical entities. In reality the hu­man essence is the product of an example of social relations. The clinical model does not encompass the social relations of the individuals it studies, even at its most progressive limits (3).

The idea that the human environment is a complex interacting web has been accepted in the biological and social sciences since the time of Darwin. Use of the concept entails analysing natural phenomena in the context of their total environment. This theory of holism rarely directs studies of human eco­logy, because many hidden assumptions preclude the consideration of cardinal social and political factors. Enzensberger, in his critique of ecology, points out how recent studies (eg., the hasty global projections of the club of Rome) fail to consider the complexity of relations between people and their en­vironment. He traces this failure to the use of narrow biological methods in the analysis of problems that are broadly social. "For in the case of man, the mediation between the whole and the part, between subsystem and global system, cannot be explained by the tools of biology. This mediation is social, and its explication requires an elaborated social theory and at the very least some basic assumptions about the historical process."

Little attention has been paid to this social aspect of hygiene, especially since the scientific advances of the nineteenth century gave the practice of medicine a solid though theoretically narrow foundation.

Turshen asserts that the one branch of biology that has taken it up is medical ecology. He goes on to describe the conception of environmental factors according to medical ecology, but I would contend that they be considered a part of epidemio­logy and his description can be freely applied to epidemiology as a whole.

Medical ecology thus asserts a relation between environment disease, and man, but selects only bio­ logical and socio-cultural factors as relevant. It looks at the convergences of environmental and com­munity factors only within the person of the patient. At no point is it concerned with the collectivity as such. By dismissing political and economic factors as irrelevant, it suffers from a failure to consider the relation of people to their environment in all its com­plexity. As with ecology and biology, the methodo­logy of medical ecology is too limited to solve the problems of public health. It is constrained by the individualistic and ideological bias of the clinical paradigm which medical ecology reflects.

These points can be Illustrated with a brief example from Vietnam. In discussing the influence of culture on human disease occurrence in northern Vietnam, J.M. May, a prominent medical ecologist, wrote in 1953: "From the water the people get their food, also their cholera, their dysenteries, their typhoid fevers, their malaria; from the earth they get their hookworm; from the crowded villages they get their plague and typhus; and from the food their protein deficiencies, their beriberi." 

May, who worked as a surgeon in the French colonial service, recognized the direct influence of scarcity and starvation on the pathology he described but dismissed any examination of their causes: "We will not discuss here

"

4

,.-­ the fantastic edifice of mortgages and debts which rises above the fraction of an acre of land on which family life is built. Nor shall we describe the land tenure laws and customs that have resulted in the reduction of the size of property through the years to insignificant proportions." To discuss and describe in these circumstances was dangerous, for no intelligent observer could escape the conclu­sion that the origins of indebtedness and land tenure laws were the key to the ecology of disease in Vietnam. The etiology was no 'cultural maladjustment'; It was the dislocation of the Vietnamese political economy by French colonialism, which imposed a system of land classification and taxation that impoverished the peasantry. Medical ecology could not take political and economic factors into consideration without challenging the legitimacy of colonial rule.

As the above example clearly shows study of so­cio-economic and political factors is important for understanding the existing conditions of health of people and the condition of medical science and health services. And this understanding is!. an impera­tive for future developments in these fields.

The Hazards Bulletin

A bi-monthly, will carry:

* technical, medical and legal information

* methods to monitor and combat hazards

* directories, expertise, easy references, educational programmes * in-depth case studies * news reports * your contributions

Subscription: individuals Rs. IS/­Institutions Rs. 30/­Foreign-US $ 10.

Conclusion

If we accept the two propositions stated above, that of incorporating 'health' within the purview of epidemiology and that of incorporating socio-eco­nomic and political conditions as part of 'human environment', we can easily substitute the word 'problems' for 'diseases' in the five uses of epidemio­logy stated above. The epidemiological approach can then be applied to all dimensions of the health field. In fact the basic elements of the approach can be applied to study the various aspects of society at various levels. It all depends on what parameters one defines for 'health'. Inherent limitations to this application of the epidemiological approach will definitely emerge and it cannot be the only means of understanding the complexities of society but it can definitely be one of the many tools in an attempt to do so.

Subscriptions to be sent to : The Hazards Bulletin, 2/32 Trimurti, Chunabatti, Bombay-400022

References

1. MacMahon, D. and Pugh, T.P. (1970) : Epi­demiology : Principles and Methods, Boston, Little Brown.

2. Morris, J.N. (1964): Uses of Epidemiology, Williams. Other fields studying society too seem to share the 'clinical' concept and its impact is seen in other modern social institutions and organisations as well. As Morris (2) says "One of the urgent needs of society is to identify ways of healthy living, the wisdom of body and mind and the principles of social organisa­tion that will reduce the burden of disease and improve the quality of life. The quest for this knowledge is the main use of epidemiology".

3. Turshen, M. (1981): The Political Ecology of Disease!>, Health Bulletin, No.1, pp. 1-38.

4. McDermott, Walsh: Demography, Culture and Economics and the Evolutionary stages of Medi­cine in Human Ecology and Public Health. Kilbourne E.D. and S.


5


Dear Friend, clinical practice, a theme I touched upon ear­lier in this column (mfcb 130).

Prashant’s question put to Amar Jesani (mfc 130) provides an appropriate clinical situation wherein the difference between a purely clinical approach and a community health approach can be demonstrated. J therefore, takes the liberty of responding to it.

Dhruv Mankad, Nipani.

Apropos Gloria Burret's article "Paralytic Poliomyelitis" (mfcb 130) I would like to have the following information: Prashant's question contains within itself an ans­wer from a purely clinical view point. A patient (the woman facing oppression) presents herself with a malady (pregnancy, may be having a female fetus) to the clinic. What should a Doctor do? Prashant's clinician genuinely moved by her misery advises Sex Determination test and if she finds a female fetus, does an abortion, feeling satisfied at having successfully relieved the patient of her malady through medical intervention. Instead, a socially sensitive clinician adopting a community Health approach would ask "Has the woman really been relieved of her misery by this medical intervention? How long would I go on advising SD/abortion, every time she comes with a pregnancy? What IS the guarantee that she will not be harassed for having borne a female fetus in the first place, abortion or no abortion? Proceed­ing on these line& s/he will come to result that the harassment of the woman due to bearing of daughters only, is a Social problem and not a Medical one at all, wherein medical intervention can only be partially effective or as in this case, may even be a dangerous short cut. It would perpetuate her misery without giving her any advice on the ways and means of ob­taining freedom from her oppression. This is because a purely clinical approach sees getting rid of her female fetus as her 'need' while a community health approach perceives it as the need to be relieved of harassment. Therefore, either advocating SDjabortion (in case of a female fetus) or giving her a long lecture on medi­cal ethics can be the appropriate way of intervening here. Her need can only be met by referring her to a women's group able and willing to share her misery and to make a collective effort to help her struggle against oppression.

Medical intervention while being useful as a pal­liative measure in other such 'medico-social' problems can turn out to be counter productive in this parti­cular situation as it may even perpetuate her harass­ment of undergoing multiple abortions till a male fetus is born.

I. The exact data to suggest that paralytic polio has rising incidence (fresh new cases/WOO popula­tion) in India. The data should include incidence 10, 20, 30 years ago and the present incidence to show that there is rise over a period of time per thousand susceptible population. Data naturally have to be community based rather than hospital based.

2. There has been a plea raised from various quarters that the number of polio vaccine doses should be increased from three' to five or more because the efficacy of the vaccine is low in our country. I would like to know whether the accepted formula Vaccine efficacy= Attack rate in the unimmunized Attack rate in the immunized x 100 Attack rate ID the unimmunized 

has been applied by those wh0 are advocating the raise on an all India basis.

3. The author has quoted Jacob John's article to say that partial coverage through vaccine causes increasing incidence. This j", according to John, because of retardation in the virus circulating and that retardation IS due to (a) improvement in hygiene (b) because immune individuals are poor transmitters of the virus. Improvement of hygiene has not ap­parently occurred in this country. No data suggests this conclusion. As regards immunity gained through vaccination, less than 6.1% of the population had been immunized in the year John's article was published. It may be presumed that this 6.1% population belonged mostly to the larger urban areas. Would such a small number residing in specifically located areas be res­ponsible for retardation of virus circulation in a country as vast and as far flung as India? If it in­deed did retard circulation of the virus, would such an eventuality reduce the incidence or increase it? During epidemics, administration of vaccine is ad­vocated to reduce the incidence of polio why?

­

-- ­ This instance illustrates the need and the possibility of adopting a Community Health approach in one’s 

Warerkar, Solapur

6 ­

Injury Prevention and Basic Preventive Strategies

Dinesh Mohan

­

­





The injury problem in developing countries looks so complex that many tend to throw up their hands in helplessness. It is often recognised that problems exist and they need attention. But the most that is done is to put up posters and billboards exhorting their readers to behave more responsibly. The pro­blem remains unsolved. This is partly because there are many myths prevalent regarding the control of injuries. Most people think that injuries are mainly the problem of rich countries. This is not so. The data available from developing countries suggest that in every sphere of activity the proportion of persons killed or injured is similar to or higher than that in industrialized country. Another myth is that education, propaganda, and law enforcement can be the most effective tools in injury control. This also does not seem to be borne out by many studies around the world. The whole problem can be best understood if we are reasonably clear about the concepts which follow.

"Accidents" and injuries are not "Acts of God" It is vital first step to realize that the occurrence and outcome of events which may cause injury are predictable and subject in many cases to human control. Often an injury can be prevented even where an accident cannot. In a motorcycle crash, the occu­rrence and severity of head injury depend on whether a helmet was used and on the quality of helmet used. Fires and explosions in the kitchen are redu­ced drastically when safer cooking methods are used. Children do not fall out of windows which have proper screens. Similarly, even the so called natural disasters are not really 'natural' If they were, then the effects of floods would be the same in the rich and poor countries. It is rare to see thousands made home­less in the US, but it is a yearly ritual in India. Even in India, it is the poor who seem to be more adver­sely affected by floods, and storms than the rich. Therefore, how a physical event influences human beings is largely influenced by the human beings themselves. Even the occurrence of the physical event itself is very often a result of man's activity. For example, floods may be caused by deforestation, faulty designs of dams, blocking up of drainage in cities, etc. Therefore man has a great deal to do with whether or not accidents and disasters take place and how these events affect us. We can design our environment and products such that the incidence and effects of accidents and disasters are minimised There is no difference between injury and disease Injury is a disease that results from acute exposure of the body to physical and chemical agents. There are no basic scientific distinctions between injury and disease. When one drowns, one may die because of fluid in the lungs which prevent exchange of oxygen between air and blood. The cause of death in pneumonia is the same. Any infectious disease may cause fever, pain, disability, or death. Injuries do the same. Therefore, the concept of injury is coextensive with the concept of disease as illustra­ted by the following table.

Comparative epidemiology of Malaria and Skull/ Fracture

 Pathological Host condition Agent Vector/Vehicle Interaction Malaria Man Plasmodium Mosquito Bite Skull fracture Man Mechanical   Motor- Energy          cycle Crash

Further if injuries are viewed as diseases, the com­munity may stop viewing them as events resulting primarily by carelessness. Long ago we learned that it does little good to blame the victim of a disease for being sick. For example, when a patient goes to a doctor with malaria, the doctor does not blame the victim for not killing the mosquito before it bit him. The most effective disease control measures often consist of modifying the environment, not the behaviour of the individuals, to make contracting the disease less likely. Up to now, our efforts at injury control have often been retarded by a preoccupation with fixation of blame. This has led to repeated attempts to prevent injuries by changing the behaviour of their potential victims. Such attempts are usually costly, not often successful, and have added to the public's sense that injuries are an unavoidable evil. In general, the same principles used in disease control may successfully be applied to injuries. All/Injuries cannot be prevented Most efforts to reduce injuries are termed "accident prevention" campaigns. We should be clear that accident prevention is just one aspect-and not often the most rewarding one-of a much longer range of the countermeasures used in effective injury control programme.

7 ,

This is because, making mis­takes is very 'normal' and	not 'abnormal'.	It is normal for professional drivers to be distracted during some periods of their long driving hours; it is normal for cooks to be day dreaming at some point in the kitchen; it is normal for a factory worker to make a mistake when he thinks of the hundred problems at home; and it is normal for children to do the unexpected and hurt themselves. In short, we will never eliminate carelessness, absentmindedness and even neglect in day to day activity. However, by designing our products and environment to be more tolerant of these normal variations in human performance, we can minimise the number of resulting accidents and injuries. Accidents result from a temporary imbalance bet­ween an individual's performance and the demands of the system in which he is functioning. They can be prevented by an alteration in either but most effectively by focusing on the system and not on the user the user. In many areas of public health we understand this very well. We know that drinking water should be purified at its source; it is unreasonable to expect everyone to boil water before drinking it. Ironically, it is quite common to create a product or environ­ment which is likely to cause injury, warn the user to be careful, and blame the user if a mishap occurs. We would never tolerate a person who introduced cholera germs into a city water supply and then asked every citizen to boil water before drinking it. But this we do all the time to as far as injuries are concerned.

Injury Control measures can be developed system­atically Even if one considers injuries to be a health pro­blem, very often it remains difficult to think of all the possible counter measures because the problem

R.N. 27S65J7 appears to be too large and un-wieldy. It is always easier to work in a step-by-step manner. One useful approach is to consider each injury problem as resulting from an interaction between several disc­rete factors occurring over distinct phases in time. This can be done if we divide all time into three categories: before the injury producing event, during the event, and after the event. The physical universe can be divided into these factors: man, the device under consideration, and the environ­ment which consists of everything else. These can be used to create a 3 x 3 matrix as shown in the table.

 
Injury Matrix

 
Factors

 



 Phases Human Vehicles & Physical, &
 

equipment Socio-economic environs. Pre-event 1 2 3 Event 4 5 6 Post-event 7 8 9

(From: 'To prevent Harm", Insurance Institute of Highway Safety, Washington DC)

In developing a program for injury control measures for a particular injury problem we can go systema­tically through each cell of the matrix and think of all possible counter measures applicable to that cell. (In classifying an intervention according to time, it is the point, at which an intervention exerts its effect, not the time at which it is undertaken, which is considered). The usefulness of the matrix is as a tool for generating ideas. At this stage, every possible strategy should be documented and noth­ing held back because of political or financial consi­derations. After all the possible countermeasures have been listed, injury control experts and policy makers can select those which are most feasible, effective and acceptable politically.

Editorial Committee: Anil Patel Abhay Bans Dbn1vMankad Kamala S. Jayarao Padma Prakash Vimal Balaaubrahmanyan Sathyamala, Editor

Views and opinions expressed in the bulletin are those or the authors and not necessarily of the organization. Annual Subscription - Inland Rs. 20.00 Foreign: Sea Mail US S 4 for all countries Airmail: Asia- US S 6; Africa & Europe Canada &: USA - US $11 Edited by Sathyamala. B-7/88/1, Safdarjung Enclave, New Delhi 11 0029 Published by Sathyamala for Medico Friend Circle Bulletin Trust, SO UC quarter University Road, Pune 411016 Printed by Sathyamala at Kalpana Printing House, L-4, Green Park Extn. N. Delhi 16 Correspondence and subscriptions to be sent to—- The Editor. F-20 (GF), Jungpura Extn., New Delhi-ll0014.

...



