---
title: "Food and Nutrition in India: A Public Health Perspective"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Food and Nutrition in India: A Public Health Perspective from medico friend circle bulletin. You can find the original in the PDF at [MFC348-350.pdf](MFC348-350.pdf)*

Title: Food and Nutrition in India: A Public Health Perspective

Authors: 
C Sathyamala


medico 348- friend 350 circle bulletin August 2011 - January 2012

Exploring a Roadmap for Health Care for All/UAHC (Framework Paper for the 39th Annual Meeting of the medico friend circle) - Organizing Committee of the 39th mfc Annual Meet

Background The medico friend circle has been discussing the theme of Universal Access to Health Care (UAHC) for nearly the last two and a half years. At the Mid annual meet (MAM) held in Pune in July 2009 it was decided that it was very important for a group like mfc to move beyond only providing critiques of various aspects of the present system to actually evolving a framework for Universal Access to Health Care. Given the increasing inequities as well as the continuing lack of access to health and the resources for health, as well as the further privatization on a sector wide scale, it was decided to develop and propose a broad plan for Universal Access to Health Care Health Care for All. (UAHC/HCA). Towards this it was decided that instead of preparing for a year for a theme for the Annual mfc Meet, as is the usual practice, we should work on the same theme (UAHC/HCA) for two annual meets. It was also decided that the second meeting would be held on a much larger scale than usual, when at least the broad contours of an mfc position/plan of UAHC can be explored and discussed. Thus we had an annual meeting in January 2010 focusing on the health and health-care situation in India as it is now and highlighting the gaps, that need to be filled in, and barriers to be overcome in order to reach the goal of UAHC/HCA. Through 2010 we met in groups, twice, including the mid-annual meeting in Sewagram in July 2010, to plan out the second annual meeting on UAHC/ HCA that aimed to evolve, in broad strokes, an “mfc framework” of UAHC. We had very intense, rich discussions during the annual meeting in Nagpur in January 2011 on ‘Towards UAHC/HCA: a realizable dream’. In this meet, in addition to a larger number of mfc members we also had the participation of some of the members of the High Level Expert Group (HLEG) on UHC appointed by the Planning Commission. The background papers of this meet and the report including

the summary of the consensus have been published in the mfc bulletin and are also available on the mfc website (www.mfcindia.org). At the end of this meeting it was felt that we should continue on the theme for another year (given the amount of work that has already gone in) but focus specifically on the current context in India, namely the National Rural Health Mission (which is in its last year in 2012) and on road-map for UAHC starting from the reality of the post-NRHM scenario. The Annual Meeting of January 2012, “Exploring Road-map to UAHC”, would thus focus even further on the specificities required and the road map envisaged. Further there were also many unanswered questions/grey areas that came up during the annual meeting in January 2011 that were identified in the mid-annual meeting in July 2011 as those which could form the basis of discussions at the Annual Meeting of January 2012. Both health activists as well as corporate heads are talking equally passionately about Universal Health Care. This fact should alert us of the ways in which private capital seeks higher profits from the health industry, which includes supporting such concepts. Hence a very clear people-centric approach to UAHC has to be formulated by mfc. This concern was echoed in the discussions we have had so far and is crystallized in the papers by Ritu Priya published in the Feb-Jul 2011 edition of the mfc bulletin.

Conceptual and Contextual Terrain of Universal Health Care Ever since the Annual Meeting at Nagpur, there have been a number of significant interventions/ publications on the concept of Universal Health Care. These include the Lancet series on India, the Report of the High Level Expert Group (HLEG) of the Planning Commission, the

2 approach paper of the 12th plan document on health. In November 2011, the Jan Swasthya Abhiyan, discussed UAHC during its extended National Co-ordination Committee Meeting and came up with its approach to UAHC and the developments related to it. Similarly the fact that the NRHM will be completing its 7-year period and is poised to evolve into the National Health Mission which is to include urban health care and the Prime minister announcing that like the last five-year plan could be considered an “Education plan”, and this plan would be a “Health plan”; it is clear that health is clearly on everybody’s agenda. While it is beyond the scope of this concept note to delve into the details of these individual approaches, a reading of all three ‘approaches’ show that each of them addresses major issues in areas of financing, provisioning and governance and there does seem to be some level of overlap/ consensus between specific aspects of the approaches. Given that moving from the present state of affairs to a Universal Health Care system will require system wide as well as social and political changes, what emerges from these various papers is a range of approaches to the problem. This exercise is done specifically with reference to the “mfc model” (still evolving) with a hope to identify some of the broad areas that need to be discussed in the forthcoming annual meeting. (A more detailed exercise comparing the different approaches will be circulated as a background paper for the annual meet). In the next section we will present some of the major areas discussed by the different papers and highlight some of the areas of overlap and divergence. In the section after that, we delineate how the mfc model differs from these and map out some of the major emerging themes of the mfc model and specific questions that will guide the discussion at the annual meet.

Major Aspects Discussed in the HLEG report, the Lancet – Call to Action and the 12th Plan Approach Paper on Health Financing • The HLEG suggests that there should be a predominantly tax based financing of the proposed UHC. There seems to be a clear recognition that private sector insurance based financing will not be able to achieve the goal. The HLEG is very explicit about this, stating, “Use general taxation as the principal source of health care financing” (Recommendation 5.1.3), and “Do not use insurance companies or any other independent agents to purchase health care services on behalf of the government” (Recommendation 5.1.9).

mfc bulletin/Aug 2011-Jan 2012 • The Lancet piece makes the boldest demands with a demand for 6% GDP as public financing with 15% of the taxes being earmarked for health (Box on key messages on the first page of “Call to action”). • The 12th Plan approach paper however merely talks about the 12th plan to “explore the possibilities of introducing a government funded health insurance plan for every citizen along the lines of the RSBY, which is currently limited to the poor and for certain select groups. Insurance under the plan will focus on both preventive and curative services. The premiums should be contributed by the beneficiaries and their employers”. • There is a general idea in both the HLEG and Lancet article that the various schemes/social insurance etc. need to be consolidated into a common pool. The HLEG states, “All government funded insurance schemes should be integrated with the UHC system” (Recommendation 5.1.11). Similarly, the Lancet series states, “Merge all the existing health insurance funds” (Panel 4 in the “Call for Action”). Whereas the 12th plan document does not specifically mention anything on this issue. Provisioning • Both the Lancet and the HLEG share a broad consensus that the public health system needs extensive strengthening Thus, the Lancet series says, “Health care should be provided through the diverse providers who are already active in health care, with substantial strengthening of the public health-care delivery system...”. (Pg 4 Call to action). The HLEG calls to, “reorient health care provision to focus significantly on primary health care.” (Recommendation 5.2.3), “Strengthening of district hospitals.” (recommendation 5.2.4). “Ensure adequate numbers of trained health care providers and technical health care workers at different levels – giving primacy to the provision of primary health care.” (Recommendation 5.3.1). The 12th PAP, however, is not as explicit. In fact in the section on Urban health it states, “(public health) infrastructure cannot be based on mechanical application of population based norms since many people in urban areas have access to private medical care” (p. 120, 12 PAP). • The Lancet paper calls for an Integrated National Health System where all providers will need to register with a National Accreditation Authority. (Pg. 1 Call to Action), and the HLEG calls for a strengthened public health system with ‘contracting- in’ of the private sector where

mfc bulletin/Aug 2011-Jan 2012 needed/to complement provisioning. The 12th PAP merely envisions the PPP as a finance generating arrangement. • There is a consensus among the HLEG and the Lancet on the need for defining standard treatment guidelines as well as the costing of the various interventions and that prices be strictly regulated. They also talk of strengthening the regulation of the private sector. However the 12th PAP is silent on this aspect of regulation. • There seems to be an increasing voice for the strengthening of the production capacity of the public sector undertakings for drugs and vaccines sector. The HLEG states, “Strengthen the public sector to protect the capacity of domestic drug and vaccines industry to meet national ends.” (Recommendation 5.5.3). Similarly the approach paper for the 12th plan states, “PSUs which have manufacturing capabilities, can play an important role in ensuring reasonably priced supply of essential drugs and they should be strengthened for this process.” (Point 9.23 pg. 119) Governance1 • One area where there is a broad consensus among all the three papers is that communities need to be more involved in monitoring and planning in the health system. Community monitoring/ accountability is clearly mentioned in all the three documents. While the Lancet paper talks about, “Creating systems for accountability of local health – care services to fully empower civil society groups and creating a decentralized governance structure that responds to local needs and is accountable.” (Panel 4 Call to Action). The HLEG is more explicit in this aspect calling for a range of community structures and processes like Participatory Health Councils and Health Assemblies etc. (Recommendation 5.4.1, 5.4.2). Similarly, the 12th PAP talks about, “the introduction of an accountability matrix - with community based data collection and monitoring as being critical”. (Point 9.7 pg 115) • Both HLEG and the Lancet paper suggest the formation of a number of newer bodies to oversee the development and implementation of the various components of the system and improve the governance of the health system. The HLEG recommends the following agencies: National Health Regulatory and Development Authority, National Drug regulatory and Development Authority, National Health and Medical Facilities Accreditation Authority, Health System

3 Evaluation Unit and National Health Promotion and Protection Trust. (Recommendation 5.6.5). The Lancet series talks about the setting up of: “Integrated National Health System, national health regulatory agency, a fully autonomous council that compiles and synthesizes relevant information to develop guidelines for evidence based health care and its assessment.”(From Call for Action). In terms of specific significant ideas in each of the documents, the following are notable: • The HLEG also envisages a district level information center “Jan Sahayata Kendra” for dissemination of all types of information for the people.(Recommendation 5.4.5) • Significantly, the HLEG talks about the setting up of a grievance redressal mechanism at the district level. (Recommendation 5.4.5) • While the HLEG talks about a predominant tax based financing - it clearly notes that no private sector body should be involved in the handling of these funds and that the primary purchaser of all services should be the government. (Recommendation 5.1.9) • The HLEG also envisages the district hospitals developing into nodal centers where a whole range of educational activities for professionals, para-medics as well as community based workers is hosted. (Recommendation 5.2.4). • There is detailed discussion on the role of information and research as well as the development of standard treatment guidelines in the Lancet paper. (Both in Box on page 1 as well as in Panel 4). · Very significantly the 12th plan document faults the government for filling a large number of posts through the mechanism of contracts. They talk instead of building up the overall capacity of the health system through the increased intake of permanent cadre. (Point 9.29 pg 121).

The MFC Approach: Critical Areas of Divergence While agreeing with the broad ideas of a unified system of financing and the fact that it should predominantly be tax based, the idea of an integrated health system that includes the private sector providers and that communities need to be integrally involved in all steps of the governance of the health system; the mfc sees certain critical divergences from the overall framework of the other three models. This will be discussed below.

4 Critique of the Present Model The mfc has embedded in its framework a critique of current pathogenic developmental model which has meant a double burden of diseases – older health problems like malnourishment, TB, malaria have continued and in addition there is the high incidence of ‘newer’ diseases arising out of stress, pollution, addictions, violence, etc. Any Public Health approach must try to curtail this high disease burden and hence must question the pathogenic model of development. Secondly, there has been no regulation of the quality/ quantity of private medical sector, majority of the private producers being in the business of money making through irrational, excessive medical interventions at the expense of the patients. The private sector will need to be effectively regulated and to some extent ‘socialised’ in some form, to become compatible with a genuine system for UHC. The mfc approach believes that just providing large scale, increasing public funds to an irrational, over-medicalised, ever-expanding, profitseeking industry may not be the best way forward. A People-Centered System The mfc approach goes beyond the concept of community or people’s ‘participation’ to the concept of ‘people-centeredness’. Thus the central question is not how people can get involved in the health system but rather how the health system can respond to the emerging needs of the people in ways that are relevant, accessible and respectful. The mfc approach thus starts from the concept of self-care and the critical question is how does the health system ensure support to self-care? Similarly, AYUSH and other local folk remedies are not seen merely as ‘gap fillers’ in the present system, but are seen as contributing to the overall health and therefore, need a respectful and comprehensive approach. Involving the Private Sector There is a broad consensus on the involvement of the private sector in the provisioning of services in the system. The exact method / mechanism, however, needs clarification. This is also one of the major bones of contention of the model proposed by / being discussed by mfc. While the other groups merely stop at “contracting - in” of the private sector – the mfc talks about progressively socializing the private medical sector. How will it differ from the envisaged integrated national health system of the Lancet series? We need to take into account the fact that in today’s world and especially in the health system, private capital actually adapts to various institutions to maximise accumulation2 and many times in counter-intuitive ways. Similarly the mfc has suggested a hierarchy in

mfc bulletin/Aug 2011-Jan 2012 prioritizing the involvement of various types of healthcare providers - starting with public sector, then charitable, not-for profit hospitals and only then the corporate hospitals. This whole conceptualization needs more detailing - as it will emerge as a crucial aspect in the regulation/control of private sector as well as in the socialization of the health sector. Civil Society vs the People The mfc approach also makes a clear differentiation between civil society and the people, clearly recognizing that civil society when taken as NGOs alone cannot be considered as ‘representatives’ of the people. A much wider range including most centrally the panchayats need to be taken into account. Equally importantly, we recognize that people imbibe the values of ‘commodification’ that form the practice of the system. Thus, there may well be a difference between what people ‘demand of a system’ (based on their assessment of what that system can offer) and what the people’s values and needs really are. Similarly the way in which people may finally choose to ‘demand’ UHC and ‘own’ our various concepts and approaches depends on how well the model fits in with the lived reality of the people The Way Forward One of the key aspects of the mfc approach is to see the evolution of a Universal System as an intensively political process. There needs to be massive mobilization, mapping and networking with the whole range of pro-people forces including mass organisations, social movements, trade unions, health sector employee associations, sections of rational doctors, consumer groups, etc. Amere introduction with various technical interventions without the parallel social and political change will merely lead to co-option of the various ‘solutions’ by the elite.

Emerging Questions for Discussion at the Annual Meeting Based on the above analysis and the ongoing discussions, we pose a few questions in this section which will indicate the broad focus of the Annual Meet of 2012. Due to time constraint, only some of the key questions would be taken up for discussion during the mfc meet. Provisioning • What would be the mechanism for identifying, accrediting, inducting and monitoring the services of the public sector and private sector at various levels of health care provisioning?

mfc bulletin/Aug 2011-Jan 2012 • What would be the mechanism to evolve Standard Treatment Guidelines, especially including focus on self-care and AYUSH which the mfc sees as crucial components in any UAHC system? • What would be the mechanism to decide on the level of technology/diagnostics and other aspects of care at each level? How would these be up-dated? Again how does this take into account our focus on self-care and AYUSH? • What are the mechanisms for ensuring the provisioning of universal quality care to marginalized populations like homeless, ragpickers, street children, construction and brick and lime kiln workers, sex workers, and other temporary migrants, etc.? • What are the mechanisms for ensuring the provisioning of universal quality care to people with special needs like those with disability, mental health issues, non-normative sexualities, etc.

Human Resources • What is the possible national UHC model for urban rural localities for HHR estimation? What should be the ratio of nurses to doctors? • What would be the HHR requirement of medical, nursing and paramedic personnel including public health nurses, specialist nurses, dentists, physiotherapists, optometrists, audiometrists, etc.? • Unlike the current situation, will the body entrusted with regulation of the private practioners be an autonomous body with staff appropriately trained for this work? If yes, how will it be constituted and run? • What is the envisaged role of the ‘three-year doctor’, the ‘nurse practitioner,’ etc. and the community health worker in this system? • What are the current HHR problems in the Public Health Services- about placement, transfer, remunerations, anomalies between contractuals and permanent, motivational, clinical, professional trainings, eligibilities for posts? • What would be the performance appraisal system for competency testing, quality assurance, managerial tasks which can lead to promotion (right now it is entirely seniority based ?) • How to 'recycle & redistribute' the existing HHR? How about absorbing practicing doctors? • What about the educational infrastructure for HHR, also the CME: planning, teaching infrastructure, educational training to the teaching staff, curriculum building mechanisms

5 • What are the legal arrangements required for various levels – licensing, practicing, dispensing, contractual appointments, wages, all laws related to pharmaceuticals, registration, establishment, protection, medico-legal issues, ethical issues, professional conducts, reporting systems, medical insurances, etc. • What should be the financial layout for HHR: regulatory, monitoring, educational, training, evaluating, quality assurance, etc. • What are options for integration of different systems of medicine – in terms of infrastructures, in managing HRs, in medical education and training, in competencies, etc.? • How do we tackle the “unregistered practitioner” problem in HHR framework? What about the unregistered but informally or formally trained nursing staff? Financing • What would be the total health care expenditure for UAHC in India? How do we assess Ravi Duggal’s estimate of 4.5% of GDP, and the HLEG’s estimate of 3% of GDP? Would it not be better to talk in terms of per capita health care expenses at constant prices because it would be based on estimates of actual expenditure needed for UAHC? NCMH had estimated in 2004 that to provide comprehensive care, it would require Rs. 1200 per capita annually. Do we have the details of this exercise? How rational, realistic is this estimate? Has there been any discussion on this estimate? Is it possible to take this estimate as a benchmark and prepare an improved estimate for 2011-12 at constant prices and current prices? Can we have some reasoned estimate of the situation of health care financing in 5, 10 years from now if we are to march towards UAHC in 10 years? Translated into Public Health Care expenditure as proportion of GDP, what would be the target for 5 and 10 years? Translated into Government’s Health Care expenditure as proportion of it’s total budget, what would be the target for 5 and 10 years? • What about the fund flow system in public health infrastructure? Financial sanctions for non-salary expenses of the health dept. come quite late; from October onwards and an important part is released during last couple of months of the financial year. This is an important barrier to full utilization of budgeted amounts and to ensuring adequate, regular supplies to the Public Health Facilities. What steps would be needed to ensure timely release of the budgeted funds?

6 • Learning from the experience of the developed countries, who find it difficult to cope with progressively increasing, limitless demand for health care expenses, what measures would be required in India to prevent this situation? How can unnecessary or wasteful expenditure be curbed? • What is the basis of the HLEG’s contention that 70% will be spent on Primary, Health Care? Any details of the exercise of the estimated health care expenditure’s break-up into expenditure for primary, secondary, tertiary health care? • Health-care is a state subject in India’s constitution; 80% of the government health care expenditure is by the State governments and yet state governments have very little powers for taxation. How will this contradiction be resolved? • Starting from a Public Health Care expenditure of Rs. 500 per capita today, how much would be required to move towards the goal of UAHC in 5 years and 10 years? Compared to the increase in the Central govt. health care budget due to NRHM, how much increase would be needed in 5 and 10 years to reach a budgetary healthcare allocation to reach the goal of say Rs. 1500 per capita, and Rs. 2500 per capita during coming 5 and 10 years? • What are the mechanisms through which the existing low tax/GDP ratio of 14% can be increased to more than 25%, taking note of the fact that India has a large unorganized sector including agriculture? What has been the experience of the ‘turnover tax’ on trading of securities, shares? • How much fund would be required to ensure ’medicines for all’? How much for Primary Health Care, how much secondary and tertiary care? What is the basis of the estimate by NCMH that for Primary Health Care a provision of Rs. 50 per capita is adequate? (Currently the government expenditure for medicines is about Rs. 4 per capita for PHCs and around Rs. 30 per capita in total) • What would be the overarching health authority to manage funds? At national, state levels district and municipality levels? Would the funding be on the basis of block per capita budgeting, plus some additional allocation for special needs? • What mechanism would be needed for reimbursing private doctors who opt for the regulated private health care under the UAHC system without bureaucratic hurdles and corruption? Governance3 • How do we conceptualize and concretize a peoplecentered health system and what are the specific

mfc bulletin/Aug 2011-Jan 2012

•

•

•

•

•

•

•

•

principles, institutions and processes that we need to introduce into the present system to move towards such a system? How does one institutionalize the incorporation of inputs from the Village health and Sanitation committees into the evolution of the district health action plans and then further into the State Health Plans? How does one ensure the voice of the marginalized like homeless, rag-pickers, street children, construction and brick and lime kiln workers, sex workers, and other temporary migrants, those with disability, mental health issues and non normative sexualities etc. is heard? How does one institutionalize the concept of community-based monitoring and the collection and discussion of community based data? How does one ensure that it sustains? How would a system be evolved that will ensure the answerability and transparency of the health system to the people at all levels of policy making, priority setting, implementation and evaluation of any health related program/process to the Panchayat system and to communities as a whole? What institutions are required to initiate a process of consistent feedback for building up a consensus between the representatives responsible to design and implement policies and programmes, to oversee and monitor its effectiveness and responsiveness and those who receive the services? How would the issues of inter-sector cooperation/ co-ordination both at the departmental as well as the field level be addressed? What should be the mechanism that is inclusive of the bureaucracy, the representatives and civil society and its various facets – gender, societal and regional? What should be its powers and responsibilities? What is the legal framework for defining the health rights of the service receivers in the private and public sectors as well as the grievance redressal system and regulation of quality? What should be the legal framework and mechanism for specific rights as citizens from public health infrastructure about preventive, promotive and rehabilitative as well as the regulatory actions? What should be the principles and guidelines of private health care mechanism being a peoplecentric (patient-centric to be precise for private hospitals) What should be the specific responsibilities of citizens to ensure and respect the other citizens’s health rights?

mfc bulletin/Aug 2011-Jan 2012 • What should be the enforcing mechanism for people’s health rights so that they have legal obligations? • How would the discourse on the people’s orientation of the health system take shape? How would it be converted into political mobilization and then into political commitment in the long run? What should the mechanism/political institutions to mobilize and sustain its vibrance be? • How would the cynicism of the people with the present elite dominated systems and about the faltering, corrupt public health services be overcome? • How would the present state where the commodification of the health sector services and products has created a false sense of what is “good quality health care” among the people be overcome? • How would it be ensured that there is a freedom to choose the health care provider? What mechanism should be created so that the choice is rational but flexible? • How would there be institutionalization of the dialogue/multi-logue between various systems of medicine in the evolution of a Universal Health system and more importantly between the people and the various systems of medicine and the health system? How does this feed into the issue of choice of ‘pathy’ during the evolution of a truly peoplecentric health system providing Universal Health? (All the questions are included.)

Conclusions There are some issues like medical education, medical research, the type and nature of the HMIS etc. that have been alluded to, to some extent in the HLEG or the Lancet paper but have not been discussed in mfc. Similarly the issues like medical tourism, clinical trails and research, conflict and public health, surrogacy etc. need to be discussed at some point. We hope that the discussions in the upcoming annual meeting will move us further on the path of a concrete set of suggestions, aspects of an alternative and truly people centered health model. And more importantly chart out a road map on reaching this ideal from the morass of the here and now. We of course have no illusion that UAHC is going to come as we would like it. The kind of UAHC that we want would require huge socio-political mobilization around a truly pro-people road-map. But putting forth a mfc view of UAHC and road-map for it can be part of the counterhegemonic people’s politics. Hence the relevance of

7 having discussions on ‘road-map for a realizable dream’. Footnotes 1

Suggested definitions for Good Governance are: Governance means exercising of power or authority– political, economic and administrative - to design and implement the broad vision of using our common resources and services provisioning to all the members of the society. This would mean mechanisms, processes and institutions which the citizens or groups can use to articulate their interests exercise their rights, carry out their re sponsibilities and settle their differences. Good Governance means doing so competently in a way which is open, transparent, accountable, equitable and responsive to people’s needs 2 Because Health constitutes a politically unique terrain, it has been noted that, “Privatization and corporatization can meet bitter resistance and that the health industry maneuvers very carefully when trying to extend its avenues of accumulation. This side of the sector is best captured by the idea of ‘accumulation by institutional adaptation”. In this conceptualization an institution is “the crystallization of power relations and class struggle...”, and adaptation means “enabling an environment in which agents of the health industry can gain the most from existing institutional parameters, as they change over time”. (Loeppky, Certain Wealth; Accumulation in the Health Industry, Socialist Register 2010) 3

Some principles of Good Governance:

Political Principles 1. Good governance is based on representative and accountable form of government. 2. Good Governance requires a pluralistic society with freedom of expression and association. 3.Good Governance requires good institutions – set of rules governing the actions of individuals, groups and organizations and the negotiations about differences in them. 4.Good Governance requires primacy of law maintained through competent and independent enforcing mechanism and overseen through an impartial and effective legal system. 5.Good Governance requires a high degree of transparency and accountability in public and organizational processes. A participatory approach of service deliveries particularly of public services is important to be effective. Economic Principles 1. Good Governance requires policies which promotes broad based, inclusive economic growth. It requires economic policies which allow economically free but socially regulated dynamic entrepreneurship to grow. 2.Good Governance requires social policies for both private and public sectors which ensures poverty reduction and ecological balance. 3.Good Governance requires incremental development in intellectual and physical abilities of human resources through policies and institutions for quality education, health care and other services.

8

mfc bulletin/Aug 2011-Jan 2012

Financing the Cost of Universal Access to Healthcare - Ravi Duggal1 Taking forward the discussion from the January 2011 mfc meeting at Nagpur and the discussion during the provisioning and financing sessions, it was felt that we need to work out the nuts and bolts of the costing and financing of UAHC. This paper will try to make further sense of the numbers presented at the Nagpur meeting within a strategic framework for financing UAHC. Before we engage with financing and costing numbers it is important to be clear about the volume and character of provisioning to which we want to provide access. Reviewing the discussions based on my provisioning paper and other contributions during the Nagpur meeting, I am suggesting below a brief profile of the “package” that should be delivered to start with on a course towards progressive realization. Primary Healthcare 1. Family Practice and Epidemiological Services: This is the base of the healthcare system. It is a combination of family medical practice and public health which is delivered to a unit of an average 500 families (ranging perhaps from 100 in sparsely populated areas to over 1000 in dense habitations), assuming that on any given day between 2 to 3 percent of the population seeks primary care – preventive, promotive and curative. For this an epidemiological station (an upgraded version of the current PHC) for between 10000 to 50000 population would be needed with a core staff of a public health professional, paramedics, community health workers and other support staff to manage public health needs of its area, including outreach through subcentres and an appropriate number of clinicians based on the population load, on an average one per 500 to 1000 families, who would engage in family practice either as employees or contracted in practitioners under a regulated capitation system of payment similar to the UK NHS, and not allowed private practice. All families and individuals in this unit’s coverage area will be enrolled under the UAHC and will be entitled to all primary care services (allopathic with progressive integration of AYUSH) – curative, immunization, maternal care, contraceptive, dental, ophthalmic, mental health, counseling, health education, environmental and public health, disease surveillance and control, rehabilitation and occupational health, pharmaceutical and basic diagnostic services, ambulance and mobile health services etc. The FMP could be an allopath or an Ayush practitioner, the latter with crash training in allopathic medicine, who would be located in each subcentre of the epidemiological station area. The head of the epidemiological station 1

<rduggal57@gmail.com>

would be either an allopath or Ayush doctor who also has training in public health. 2. First level Referral Care: Presently we have the model of CHCs as first referral units but most are non-functional due to poor human resource availability, especially of specialists. Since this infrastructure with 30 beds is already there in most places it should be retained but I am suggesting a dovetailing of this with a block level (more likely to get specialists) basic hospital of atleast 100 beds (depending ofcourse on the population size but approximately one bed per 1500-2000 population). This means that the base referral unit would be the block level hospital and the CHCs (about 3 per Block) would be its ancillary units providing integrated referral and basic specialty services. The block level (in cities the Municipal ward with a single 150-bed hospital, that is no CHCs) would be the health district which will have a local health authority (a sub-unit of the National and State health authorities) that would administer and govern the basic healthcare system of the area. For each basic specialty there should be atleast two specialists employed or contracted in (those contracted in to the UAHC would commit full time and would not be allowed private consultative practice) and progressively numbers could be increased as they become available towards a goal that each CHC has all basic specialists and the block hospital gradually gets upgraded for higher level referrals. Secondary and Tertiary Care The district and large city hospitals 500-2000 beds, including the teaching hospitals, would provide higher levels of care (about 1 bed per 3000 population). Where public hospitals are not available in adequate numbers private hospital beds would be contracted in through a regulated purchase agreement for exclusive service under the UAHC. The entire system would be regulated and would have a gate-keeping system through a strict referral mechanism. All non-emergency cases would only come as referrals from the primary healthcare level. The above is not a complete description but only an outline to facilitate the costing of such a UAHC. However, it needs to be developed on the lines of the UK NHS as shown in the following diagram but modified to suit the structures and needs within the country. A. Working out the Cost We are not confining this calculation to the 2-3% GDP commitment of the UPA but working out the cost based on the basic healthcare package defined above. This would of course be a staggered development but we are

mfc bulletin/Aug 2011-Jan 2012

confining it to the 12 th Plan period assuming that reasonable political commitment would be mobilized to get this core package up and running before the end of the 12th Five Year Plan – so basically what we are saying is that the 12th Plan resources should be coterminous with this costing. Further the costing is being worked out on the basis of an average unit as defined above – 750 families for a FMP unit, 30000 population and 10 beds for an Epidemiological Station or PHC, one bed per 1000 population for the basic hospital and CHCs, and one bed per 3000 population per district/tertiary hospital (overall one bed per 600 population)– but on the ground will depend on the population covered and the density of an area, with unit rates of payments being adjusted for sparse population (higher rate) and dense population (lower rate). The costing is being done at 2011 prices and a population base of 125 crores and GDP of Rs 7,500,000 crores. Primary Care Cost

9

Rationale: •

Staff composition for each PHC-FMP unit to include 5 doctors (one coordinating the ES-PHC and 4 clinicians located at subcentres and the latter paid independently as mentioned in (1) above), 1 PHN, 2 nurse midwives, 8 ANMs (females), 4 MPWs (males), 2 pharmacists, 2 clerk/stat asst., 1 office assistant, 2 lab technicians, 1 dental asst, 1 opthalmic assistant, 1 physio assistant, 1 counsellor, 3 drivers (2 for ambulance), 1 cleaner and 20 CHWs etc. Doctors and nurses may either be salaried or contracted in on a capitation basis as in the NHS of UK. The curative care component should work as a family medical practice with families (100 - 1000 depending on density) being assigned to each such provider.

•

•

Average of 10 beds per PHC, 2 ambulances, pharmacy, basic diagnostics, ophthalmic, dental, physiotherapy and counseling units, 4 subcentres, etc. Average rural unit to cover 20,000 population (range 10-30 thousand depending on density); average urban unit to cover 50,000 population (range 30-70 thousand population depending on density)

•

All consumables, POL etc, except drugs included

1. FMP, one per 750 families @ Rs. 1000 per family per year, including overheads. For 25 crore families 330,000 FMPs needed = Rs 24,750 crores Rationale: Average net income of between Rs. 5 and 6 lakhs per year and Rs. 1.5 to 2.5 lakhs as overheads, maintenance, assistance etc. of the clinic. The clinic would preferably be located at the subcentre and the ANM and MPW of the subcentre (paid by the ES) would support the clinic for preventive, promotive and outreach activities. 2. Epidemiological Station for 30,000 population @ Rs.350 per capita, including PHC, subcentres, CHWs, ambulance services, basic diagnostics, public health programs, 10 beds etc. 42,000 ES needed = Rs. 44,100 crores

3. Pharmaceutical services for primary care (FMP and ES) @ Rs. 60 per capita per annum = Rs 7,500 crores. Rationale: Based on NSSO data and the debates we have had over the past year and assuming that generics will be used mostly, and bulk and rationalized buying and elimination of retail purchases will reduce substantially the cost of drugs to the state. 4. Basic Hospitals at Block and CHC @ 200 beds per block unit (avg population 200,000 with 1 bed per 1000 population) @ Rs. 750,000 per bed per year. Beds needed 1,250,000 = Rs 93,750 crores.

10 Rationale: Based on costing studies/budgets of public and private hospitals 2 (non teaching and nonmultispecialty). This includes all costs like salaries/fees, diagnostics, medicines and other consumables and admin and maintenance costs, ambulance services etc. To begin with atleast two specialists for each of the six specialties (physician, surgeon, Ob & Gynaec, paediatrician, ophthalmologist, orthopaedic), 1 physiotherapist, 1 dentist, 1 anaesthetist, 2 MOs per CHC, 6 MOs at Block Hospital, 40 nurses for the Block, 6 counsellors/socialworkers and the other requisite paramedics and support staff needed in such hospitals. Secondary and Tertiary Care Cost

mfc bulletin/Aug 2011-Jan 2012 and accountability these can be substantially negotiated down, closer to 1 million per bed for teaching hospitals, 0.75 million for the larger general hospitals and 0.6 million for the Block level hospitals. Further the new investment capital spending of 15% would be restricted to the 12th Pan and if proposed infrastructure is in place then in later years this would be much lower. A summary of Costs is given below: Primary Care cost Rs. 170100 crores (including first referral hospital) Secondary/ Rs. 62550 crores (including Tertiary care Medical and health education)

District level and city hospitals @ 1 bed per 3000 population @ Rs 15 lakhs per bed per year, including medical and health education (doctors, nurses and paramedics). Beds needed 417,000 = Rs. 62,550 crores.

Total Health Services Cost =

Rs. 232,650 crores (Rs.1860 per capita or 3.10% of GDP)

Add 2% Medical Research

Rs.4,653 crores

Rationale: Based on costing studies/budgets of teaching and multi specialty public and private hospitals3. This includes all costs like salaries/fees, stipends, diagnostics, medicines and other consumable, admin, maintenance, education, training, etc. The staff composition as per existing teaching and district hospital norms – the suggestion is that all district hospitals should eventually become teaching hospitals so as to decentralize production and improve retention within districts. The range of costs will vary according to the mix of services provided and could range from 10 lakhs per bed to 25 lakhs per bed as unit cost.

Add 2% audit and info mgt

Rs.4,653 crores (including accreditation/ standards monitoring) Rs.11633 crores (costs of the health authorities and system management)

A note of caution about the costing worked out. In working out the hospital costing I have erred on the higher side since market pricing has been factored. Under a public monopoly and use of rational therapeutics, strict audit

Add 5% admin/ governance TOTAL Recurring Add 15% new Capital investm’t

Rs.253,589 crores (Rs. 2029 per capita or 3.38% of GDP) Rs.38038 crores

Add 10% amortization

Rs.25,359 crores

Add 5% Contingency

Rs.12,680 crores

Total Health care

Rs. 329,666 crores (Rs. 2637 per capita or 4.39% of GDP)

B. Financing Strategy 2

Cost studies and budget analysis done by CEHAT, and the author, of small private hospitals, CHCs, cottage hospitals. 3 Based on budgets of Mumbai (KEM and GS Seth), Delhi (Safdarjung and RML) and Maharashtra public hospitals (district and teaching), charity teaching hospitals (Sewagram and Sathya Sai) as well as of corporate hospitals like Apollo Group, Fortis Group, Max Healthcare, CEHAT cost study etc. It is interesting to note the huge variations in per bed cost across various hospitals. Apollo net of profit 1.8 million, Fortis net of profit 1.5 million, Max net of profit Rs 3 miilion, Pune multispecialty hospital (CEHAT study) net of profit Rs.1.4 million, Safdarjung Hospital and Vardhaman Medical college Rs. 1.9 million, Ram Manohar Lohia 2.5 million, Maharashtra district hospitals 0.4 million, Maharashtra teaching hospitals 0.6 million and surprisingly the KEM and GS Seth Hospital in Mumbai just 0.5 million, and another surprise the AIIMS being a whopping 3.6 million. The charity oriented and ethically operating hospitals are very interesting – Sewagram has a per bed cost of Rs. 0.76 million (with 17% income coming from patients) and the Sathya Sai hospitals only 0.68 million per bed (zero cost to patients and some voluntary labour by devotees).

The resources for this will come mainly from general tax revenues. At present tax revenues are not adequate (only 17% Tax: GDP ratio) to support such a large budget for healthcare. Ideally for reasonable social sector financing the government should have a Tax: GDP ratio of atleast 25%. Within the present tax framework it is possible to raise the Tax: GDP ratio to over 30% if most of the tax expenditures (revenue forgone)for the corporate sector are done away with. The corporate lobby is too powerful to let go of this subsidy they get, and in fact with each year’s Finance Act during the past decade there has been a consistent increase and the latest figures indicate that the revenues forgone are almost equal to the tax revenues collected – indicating a potential for doubling the Tax: GDP ratio. Further, tax collection and compliance is also very poor – the organized sector employees and corporates pay their taxes but a large proportion of selfemployed and small businesses evade taxes and a substantial number of the political and business elite conceal incomes and/or export them illegally to tax

mfc bulletin/Aug 2011-Jan 2012 havens. This would require a much better and efficient tax administration and discipline and is estimated that it could add around 50% to the tax revenues collected. It is unlikely that the above situation would improve substantially soon and therefore in the interim other means of raising resources for the health budget would be required. Tax based financing would be the fulcrum of the UAHC and insurance of any kind should not be used, including the RSBY and Arogyashri variety which involve either an insurance company or government as insurer. A benchmark either as proportion to GDP or as a proportion of the total government budget or alternatively even as a per capita allocation which is linked to an appropriate price index must be created so that under-financing of the health sector does not happen. Of course this would need to be revised periodically as we move towards greater progressive realization for the commitment to the highest attainable standard of health as ratified by India in the ICESCR. Some suggestions for other resources given below: •

The potential for social insurance to contribute substantially is an option worth pursuing. The 200910 Labour Bureau survey estimates a workforce of 415 million persons but only 63 million or 15% are getting any social security benefits. There is scope for higher payroll deductions, mainly from employers. Presently the contribution of social insurance to healthcare spending is only 5% of the public health budget and about 70% is through public sector employment. The NCEUS which reviewed this potential has recommended the expansion of social insurance and one of the key mechanisms to raise resources is through cess on goods produced in the informal sector or even a cess on overall excise duties to finance social security. All such social insurance funds like CGHS, ESIS, Railways and P&T health funds, beedi worker, plantation workers, mine workers, construction workers, matahdi workers, head-load workers and many such small occupational group funds (the health/medical benefit components) both at national and state level should all be pooled into the National Health Fund along with tax funds

•

Another major source for raising resources is a comprehensive Financial Transaction Tax covering stock and commodity markets, futures trading, currency exchange, FDI investments etc. and with a 0.5% tax on the turnover nearly 6% of the GDP can be netted. This will add substantially to tax revenues

•

Apart from this sin taxes and cess on consumption taxes like VAT etc. which are increasingly being used in many countries could also be an option.

•

Assuring accountability for the subsidies given to the private sector under the Trust Act, land and other

11 capital, tax waivers etc. can also provide access to significant resources in the private health sector for public benefit. For instance, hospital beds under Trust/Society ownership is estimated to be at least 350,000 across the country and 20% of this that is mandated as social benefit would be 70,000 beds and assuming an annual running cost of Rs. 1 million per bed in value terms this benefit is worth Rs.7000 crores or an equivalent of 10% of public health spending in India. Apart from this there are other concessions given to private hospitals and other healthcare industry producers and against them there are agreements for social benefits which are not being honoured because of poor monitoring and audit by state agencies. For instance the deal between Delhi government and Apollo hospitals was to provide 30% of beds and OPDs free at Apollo hospital in Delhi but this clearly does not happen. To accomplish the above no great structural changes are needed. What is required is for the various departments like the tax revenue, charity commissioner, finance ministry, labour ministry, etc. to do its work efficiently and honestly. That is do their jobs as per the law. So raising resources is not the big issue in financing the UAHC; the big issue and challenge is restructuring the healthcare system – re-energizing the public health system with appropriate resources and good governance and accountability, and regulating the private health sector towards its socialization under the umbrella of UAHC. To do all the above an exceptional political will is needed. This is the key element as revealed by the recent Thailand and Brazil examples which we discussed at length at the 2011 mfc meet. For support of social sectors in India we presently have the NAC under Sonia Gandhi with a number of civil society stalwarts exerting the push. But in the last 8 years of NAC while a number of populist schemes have been mooted and have taken root they have failed to make a significant impact because they have failed to leverage significant budgetary increases – some substantial increase in Central government budgets may have happened during these years but most of these schemes are constitutionally state subjects and hence leveraging state budgets is more critical. The NAC certainly cannot do that. To promote political will we need champions in mainstream politics and that we do not find presently for the cause of health and healthcare. We may have one off initiatives like the recent free medicines one in Rajasthan or Chiranjeevi in Gujarat, Arogyashri in AP, the Assam health bill, etc. but these do not address system issues and hence are only populist schemes to help electoral politics. Assuming that some political will exists and the resources indicated above are made available then how do we strategize the financing to achieve UAHC goals? At the outset I would like to make it clear that the costing

12 I have worked out above and the resources needed is not for progressive realization. These resources are for a core package that must be made available right from the word go. This has to be a comprehensive approach, you cannot take parts of it and prioritize. That is why the emphasis on the political will. The 12th Plan must in principle commit to making these resources available within the period of the Five Year Plan (Rs. 1,648,330 crores (Plan + Non-plan) at 2011 prices for the five years of the Plan must be committed), strategize it and design the structural changes needed to make this work. All the resources from taxes, social insurances, cesses, etc. should be pooled and transferred to the National and State Health Authorities as the case may be and they would transfer these resources to the block health authority as per their requirement based on the units of provision in that region. As per the costing worked out above, the resources should be provided to the block level health authority for the areas under its oversight. This block level health authority would function as the key planning and budgeting unit with participation of providers, managers, elected representatives and civil society representatives of the region. This authority will also maintain the list of all families/households in the block and enlist them under the UAHC for which appropriate documentation (Health Entitlement card for example as suggested by HLEG) would be made. To facilitate its functioning this authority will also have a research and information management unit which will maintain all patient records, conduct local health related research etc. Further, for monitoring and social audits the Community Based Monitoring system being experimented under NRHM must be appropriately upgraded to work with the UAHC. For accreditation and standards there should be an independent national authority with its local branches (and perhaps this should be an authority that covers all social sector and development programs). C. Payment Mechanism and Fund Flow System The block health authorities who will get quarterly allocations from the national and state authorities will purchase healthcare services from each provider unit as per the cost schedules (detailed cost schedules for each allocation will have to be worked out). The provider units will have to provide detailed budgets of their units (this being done as part of the planning and budgeting exercise) to secure funding. The funds would be released as monthly advances and at end of each month a utilization statement along with all supporting documents (bills and vouchers) would be sent to secure the release of the next monthly installment. As far as possible all transactions should be done electronically and the effort should be to minimize cash transactions –

mfc bulletin/Aug 2011-Jan 2012 this would also improve the efficiency of accounting and money management. The funds from the central government will be pooled in the National Health Authority and those of the state governments in the state health authority, including the other special funds from social insurance, cesses, etc. The National Authority would send the respective share of the states from the National Fund as six monthly advances to the states. The state authorities would receive the budget demands as per the cost schedules of each unit from the Block Health Authorities and the funds would be sent to the latter as quarterly advances and at the end of each quarter a utilization statement would be sent to the state for release of the next installment. The provider units will receive monthly advances and at the end of each month would submit a utilization statement to receive the subsequent installment. As mentioned above all these transactions would happen electronically, with each oversight authority verifying periodically/randomly the veracity of the transactions. D. Conclusions Since lots of deficiencies of the system have to be put in order, especially infrastructure requirements, the Plan should make provisions for most of the capital expenditure in the first two years. Similarly lot of recruitments or contracting in agreements would have to be worked out in the first year itself. A special agency under the National and State Health Authorities would be needed to facilitate this restructuring and contracting in process. They would have to review the public health system, its various deficiencies in human resources, infrastructure, governance, etc. They would have to map out the functional units needed and review to what extent the public health system will be able to meet the demands. They would have to engage with the private health sector and negotiate their terms of engagement with the UAHC. For all this a national legislation for UAHC (and Private sector regulation) with its rules and regulations would be needed immediately to help facilitate this process. Similarly under the Health Authorities a technical research unit would have to be set up which would work out the detailed costing, pricing mechanisms, payment systems, audit and reporting systems etc. The NHSRC and SHSRCs could be upgraded to do all this preliminary, scoping, mapping etc. work so that the proposed UAHC is up and running within the first year of the 12th Five Year Plan. On the face of it one would say this is not possible to happen so quickly and we must have a longer term perspective. But I believe that if we don’t plan towards achieving this within the Plan period and generate the pressure and political will for that it will not happen even in the next 25 years.

mfc bulletin/Aug 2011-Jan 2012

13

Document

Strengthening the Drug Regulatory Mechanism in India (Draft Note for the Working Group on Drugs and Food Regulation for giving inputs for the 12th FYP, July 2011) - Anant Phadke and S. Srinivasan We put forth herein some suggestions about strengthening the drug regulatory mechanism in India keeping in view the Terms of Reference of the Working Group for giving inputs for the 12th FYP. These Terms of Reference for the WG-DFR are – 1. To review the drug and food regulatory mechanism in the country to ensure providing essential quality, safe drugs at affordable prices to all citizens and to provide wholesome food to all citizens in the country. 2. To review the incidences of antibiotic/anti-microbial resistance and suggest measures for rational prescription of drugs especially antibiotics.

of the fact that three years had lapsed from the acceptance of the PRDC report by the Government, no infrastructural improvement whatsoever in respect of personnel has occurred in CDSCO.”

3. To review and suggest measures for promotion of generic drugs.

1. Clinical trials and approval of new medicines, enabling the use of TRIPS flexibilities

4. To review the progress of pharmaco-vigilance programmes and suggest measures to strengthen the same. 9. To suggest modifications in policies, priorities under the drug and food regulatory framework during the 12th Five Year Plan. 11. To deliberate and give recommendations on any other matter relating to the topic

2. Periodic review to eliminate unsafe and obsolete medicines;

As a background, it may be noted that the Mashelkar Committee in its report in 2003 had taken a review of the action taken to fill the gaps which were identified by the earlier Pharma Research and Development Committee (PRDC) in 1999. The Mashelkar Committee had noted the following gaps in the implementation of the PRDC recommendations -

During the 12th Five Year Plan all aspects of the Drug Regulatory mechanism in India should be strengthened substantially by implementing the unimplemented part of the recommendations of the Mashelkar committee. We are however certainly not in favour of the recommendation of death penalty to the offenders. The strengthening of the drug regulatory mechanism would encompass regulation of

3. Quality of medicines 4. Promotion of medicines professionals and lay people

amongst

health

5. Prescription practices and use of medicines by health professionals (to be done by the regulatory bodies for the health professionals like the Medical Council of India)

Lack of performance management of systems; Inadequate administrative, professional and financial support, which hindered the opportunity of availing expertise from outside specialists, particularly in the field of new regulatory areas; - Lack of data base of drug products licensed by various State authorities in the country.”

To regulate all these aspects of the pharma industry, the Central Drug Safety Control Orgnaization (CDSCO) would need considerable strengthening. 1 1. STRENGTHENINGTHE REGULATION OF CLINICAL TRIALS, OFAPPROVAL OF NEW MEDICINES INCLUDINGVACCINES A. Clinical Trials, Approval of New Medicines A1. Review and Strengthening of Schedule Y: It is in this background, Schedule Y of the Drugs and Cosmetics Act dealing with approval of new drugs and clinical trials needs to be reviewed and strengthened in the light of recent experiences. Several provisions of Schedule Y may need to be harmonized with other codes of ethics. Specifically, giving legal “teeth” to the ICMR guidelines for research by merging it into Schedule Y need to be examined. IPR issues of data need to be examined consistent with India’s obligations under TRIPs and not ‘TRIPS Plus’. A2. Law for Regulation of CROs: Also of essence is a proper law regulating the functioning of CROs that takes on board the above concerns. These can be in addition to the rules of the Drugs and Cosmetics Act. Hence the current draft on regulation of CROs would need some additions.2

The Mashelkar committee had further noted that “in spite

A3. Increase in Human Resources: The CDSCO also

“The major gap areas were identified as: -

-

Inadequacy of trained and skilled personnel and infrastructural support at Central as well as State levels commensurate with their respective specialized roles and responsibilities and emerging challenges; Non-uniformity in implementation of existing regulatory requirements and policies;

-

Variation in the quality of enforcement;

-

Inadequate and disjointed drug testing laboratories scenario;

-

14

mfc bulletin/Aug 2011-Jan 2012

has very little technical staff to cope with challenging tasks of reviewing the increasingly mounds of data that are submitted for clinical trials and drug approval. Ongoing surveillance and safety studies require even more qualified human resources. The staff at CDSCO and at State level Drug Commissionerates needs to be increased many fold. Decision making needs to be transparent and as much of all data and decisions should be available for review in the public domain. Till such time when India has its own resources to generate clinical and review data, our pharmacological decisions necessarily need to be guided by the decisions of well-regulated countries like the USFDA, MHRA UK and similar bodies of Sweden, Australia and Germany (to cite a few). This must be seen as an exercise of the precautionary principle in the interests of the citizens of India. B. Regulation, Manufacture of Vaccines B1. India needs a well-debated, evidence-based national vaccine policy that clearly delineates kinds of vaccines to be introduced and sustained in India in the National Health Programme.3 To fufill this objective, the CDSCO will have to be considerably strengthened to harness expertise to be able to take decisions about the safety and efficacy of vaccines to be introduced in India.4 B2. Strengthening the Role of the Public Sector Units in Vaccine Manufacturing The need to revive and reinforce the Vaccine PSUs with the goal of retaining vaccine manufacturing selfsufficiency in the hands of the Government, and in the interest of national health security, cannot be overstated. The 12th Plan Goal would be to attain GMP standards in all PSU vaccine manufacture and complete selfsufficiency in all vaccines in the UIP.5 2. PERIODIC REVIEW TO ELIMINATE UNSAFE AND OBSOLETE MEDICINES 2.1 A Special Commission to eliminate irrational drugs and irrational Fixed Dose Combinations During the 12th FYP a capable, adequate mechanism needs to be developed to periodically, routinely, review the rationality and safety of the medicines marketed in India and to eliminate irrational drugs and irrational Fixed Dose Combinations. This review is needed if the objective of promotion of generic medicines is to be achieved. A special commission working in the in mission mode should be set up during the 12th FYP to clear the backlog of eliminating thousands of irrational medicines/irrational drug combinations.6 A related aspect is the examination of legal aspects of weeding out such medicines. Even though it is done on purely scientific and therapeutic considerations, stay orders are brought in and all good efforts come to a naught. This should be prevented by overcoming any legal lacuna that may be present currently. 2.2 Enhanced Pharmacovigilance: Periodic review of

medicines market in India would require good pharmaco vigilance. The existing mechanism will have to be considerably strengthened to have a nationally connected computerized system of relevant data collection and vigilance.7 2.3: An independent agency (independent of the CDSCO/DCGI, the apex licensing body) like the National Institute of Clinical Excellence (NICE) in UK needs to be set up in India to create a scientific basis for the decisions of the CDSCO/DCGI. 8· 2.4: Convergence of NLEM and National Formulary and Drugs in the Market and those Prescribed in State and Central Govt Institutions: A new National List of Essential Medicines (2011) and a new National Formulary have been released. These are welcome steps in the right direction. For these to have meaning, by and large primarily medicines in the NLEM and formulations in the National Formulary should be licensed for marketing and manufacture and only such formulations be made available in the country. To accommodate the need for medicines that are needed for rarer conditions, a separate list may be drawn up. Medicines approved for import and marketing in India should be guided by these considerations. 3. QUALITY OF MEDICINES 3.1 Time bound plan for implementation of the relevant recommendations of Mashelkar Committee There has to be a time bound plan for implementation of the relevant recommendations of the Mashelkar Committee, except the recommendation of death penalty. At the State level, several new food and drug laboratories need to be set up with continuous training and upgradation of staff. At least one major laboratory per State would be required and in the States where pharma industry is predominantly located, the appropriate number may be decided after finding the current and potential workload. The Mashelkar committee had recommended that there should be “1 inspector for 50 manufacturing units and 1 inspector for 200 sales units”. This should be implemented during the 12 th FYP by enhancing adequately the training facilities and the budget for appointment of these many drug inspectors. 3.2 Financial help for upgradation of WHO GMP/ Schedule M: During the 12th FYP, based on clear guidelines, financial and technical should be available to SSIs for upgrading their facilities to WHO GMP/Sched M standards. WHO GMP/Sched M standards should be part of the 5-year national goal 4. PROMOTION OF MEDICINES AMONGST HEALTH PROFESSIONALS AND OTC PRODUCTS During the 12 th FYP, there should be considerable strengthening of the process of eliminating misleading

mfc bulletin/Aug 2011-Jan 2012 unethical promotion of drugs by pharma companies to health professionals and of OTC products. A mandatory Code on the lines of the code available internationally prepared by the Health Action International (HAI) should be in place which would cover promotion of medicines both to the medical professionals and the lay-people. We attach a draft code worked (not attached in this issue of the bulletin - editor) out in consultation with various civil society groups like the All India Drug Action Network, Medico Friend Circle, LOCOST, Federation of Medical Representatives Association of India, the Lawyers Collective, etc. Such groups may be involved in the process of consultation in the drafting of a regulatory framework. During the 12th FYP, CDSCO the mechanism for redressal against misleading advertisements should be professionalized and should be made widely, easily accessible. This should be widely, consistently advertised across all media so that consumers can participate in the process of enforcement.9 5. PRESCRIPTION PRACTICES AND USE OF MEDICINES BY HEALTH PROFESSIONALS During the 12th FYP, following measures will have to be taken in this connection – •

• • •

•

Introduction of a national antibiotic policy that covers usage and treatment guidelines of antibiotics for humans and animals Antibiotic usage review audits in public and private hospitals A policy that all antibiotics to be sold only by generic names; Medical/nursing/pharmacy colleges, professional medical, pharmacy and nursing networks/ associations, schools and consumer groups need to be involved in finalizing the details; Study and adoption of replicable aspects of successful attempts by countries like Sweden, Australia for recovery of older antibiotics.

•

The resistance in case of TB and AIDS needs to be tackled on a separate war footing. DCGI’s reported resolve to insert a new Schedule called HX under the Drugs and Cosmetics Act with a view to preventing the misuse of antibiotics is to be welcomed. Representatives of doctors, chemists and civil society organizations should be consulted in finalising the details.10 The background and rationale of this measure is Rational Use of medicines, especially of antibiotics is essential to prevent antibiotic resistance as a public health problem. Regulation of prescription practices and use of medicines by health professionals is to be done by the regulatory

15 bodies for the health professionals like the Medical Council of India. However, other measures are also required. 6. MODIFICATIONS IN RELATED POLICIESAND PRIORITIES TOR 9 and 11 of this working group is: To suggest modifications in policies, priorities under the drug and food regulatory framework during the 12th Five Year Plan. To deliberate and give recommendations on any other matter relating to the topic. In this connection, the following is suggested •

A national policy for research priorities for drugs that need to be discovered considering mortality and morbidity patterns of India and how such research will be seeded and funded in the country.

•

All drug related matters should be under one ministry and not be divided between MOHFW and MOCF.

•

Price Regulation of all Medicines and not only essential medicines so that there is no incentive to market, costly, ‘me too’ medicines (see below) Provision of free drugs for all in the Government health systems all over India (The calculation of estimate follows this article – editor, mfc bulletin). .

•

•

•

Retention of regulatory/licensing for marketing and manufacture of biotech drugs under the DCGI/ MOHFW. Regulatory aspects on approval of biosimilars need to be clarified..

•

Patent matters need to be firmly delinked from the drug licensing process for manufacture and marketing.

•

Ensure India uses TRIPS flexibilities like CLs to the optimum (including CL for government use) and not be a signatory to any agreement that is TRIPS Plus. Conflict of interest declaration (especially with respect to the medicine/medical device industry) be made mandatory for all individuals, official and nonofficial, involved in policy making and those who are part of committees related to policies and laws.

•

•

•

A commission to institute reforms and restructure CDSCO in the light of new challenges may be considered.

Transparency in all matters related to regulatory decision making and the basis of decisions Of these measures we comment on pricing as it has a direct and immediate health and socio-economic impact on ordinary end-users. Price regulation of some kind is essential. This is now accepted in much of literature. Most advanced ‘free market’ economies have some form of price regulation/ subsidy/reimbursement schemes. 11 Indian pharma formulations industry is characterized by wide ranging prices for the same product and high profits, apart from

16 marketing unnecessary combinations and selling them. There are also other tragic consequences of the ignorance of susceptible users making decisions in distress, ‘advice’ of prescribers, and ‘marketing’ efforts of companies – all illustrations of the inherent asymmetries of health markets: the costlier versions of the same drugs are bought more, and irrational combinations sell more because the doctor says buy them. Several government bodies have recommended some form of price regulation even as every so-called developed country has some form of price regulation/ profit cap/reimbursement scheme etc. Annexure 1 Note on Regulation of Drug Trials in India for the Working Group towards 12th Five Year Plan Sama – Resource Group for Women and Health, June 15, 2011 As the medical research world becomes increasingly globalized, there is a need to make research both methodologically and culturally valid. An increasing number of pharmaceutical companies have started outsourcing drug trials to Contract Research Organizations (CROs) in developing countries; these now make up a specialized global industry focusing on the recruitment of and research on human participants. In October2008, the Drugs Controller General India (DCGI) stated that there were 582(registered) clinical trials being conducted in India, of which 72% were carried out by the pharmaceutical industry. Shifts in the very science of drug development have influenced the decision to increase participant recruitment, with India becoming an attractive destination for international, outsourced clinical trials. The weak Indian disease surveillance system provides little empirical evidence to establish the relevance of a particular study to the country’s health needs. Drugs or other interventions developed through research may be expensive and therefore unavailable to the public. Indian sites for the research are without any independent institutions for monitoring and auditing of drug trials. In the absence of such a mechanism, it is difficult to ensure that scientific standards are upheld, and that good research practices and required trial protocols are fulfilled. Eventually, the quality, practice, ethics and effectiveness of science itself may be compromised. There are many examples of drug trials that have taken place without proper protocols of consent. Without adequate and effective regulatory jurisdiction and systematic oversight, the reliability and validity of such research is jeopardized. Considerations of transparency along with protection of participant rights have to be made priority for policies that truly engage and respect the public. This is particularly critical in the context of drug and vaccine trials, placebo-based and comparator

mfc bulletin/Aug 2011-Jan 2012 trials and genetic studies that are often in violation of both the Declaration of Helsinki as well as the guiding principles laid down in the Indian Council of Medical Research’s (ICMR) ethical guidelines for biomedical research. India requires more substantial regulation, and effective implementation, that can institutionalize the highest standards of independent inquiry, good clinical practice, protocols, monitoring, and follow up, so that a strong and science-friendly policy framework can be put in place, to enable and empower medical research. In this context, regulation of these trials seems to be on shifting sands. Norms seem to be set to suit business interests rather than for the protection of the rights of research participants. Public health and medical priorities of the country are not considered. Some of the areas of concern in this respect are enumerated below. Issues in the regulation of Contract Research Organizations (CROs) • Clinical trials are con-ducted by contract research organizations (CROs) which are developing the infrastructure For trials by making inroads into small towns, identifying trial sites in small private hospitals and developing databases of potential trial participants. Medical professionals are given substantial incentives to recruit their own patients into clinical trials. This situation creates a major conflict of interest that threatens the well-being of patients. • Unlike the United States, where Contract Research Organizations (CROs) are codified in a Federal Register notification, in India Schedule Y of the Drugs and Cosmetics Act (the only present legislation for the regulation of clinical trials) does not make any mention of them, let alone lay out clauses for their regulation. Ambiguous Clauses, Ambitious (Profitable) Amendments • The Schedule Y amended in 2005, gives the DCGI arbitrary powers to waive off the need for trials in several cases. This combined with vague formulation of clauses has led to an ineffective law. • The recent amendment, allows for concurrent trials, doing away with the prescribed phase lag for trials for drugs developed outside India. However, these trials are not properly defined with the usage of extremely vague and ambiguous terms such as “adequate”, “necessary” etc. without any specifications clearly spelled out. • At present, the Drugs and Cosmetics Act does not allow the conduct of Phase I trials for drugs developed outside the country until at least one phase I trial has been completed elsewhere in the world. There is an additional waiver (based on discretionary powers of the DCGI) for trials on drugs indicated in life threatening/serious diseases or diseases of special relevance to the Indian health

mfc bulletin/Aug 2011-Jan 2012 scenario. However, there is an increased push to allow concurrent phase I trials in India (as is now permitted in the case of Phase II and Phase III trials). • The issue of allowing Phase I trials need to be also explored in the context of structural issues, including o Absence to public health makes people vulnerable o Regulatory gap o Ethics gap o Capacity gap – Huge lag between the number of trials and the development of capacities to actually carry out these trials in an ethically sound manner. • The earlier minimum requirement of number of subjects for phase III trials to be conducted for drugs approved in other countries was removed and made discretionary. • There are no specific, listed requirements for sites where clinical trials can take place such as availability of expertise and infrastructure to deal with unexpected adverse drug reactions, ICUs etc. • A major lacuna in the DCA is the lack of any mention of penalties or application of liabilities for those who violate the Act. Similarly, the Act does not lay down the technical details of the specific requirements at trial sites, membership and location of ECs. • Clinical trial data exclusivity that is being sought by foreign companies is another area of concern as it can delay the introduction of generics and increase the cost of medicines. Moreover, claims that clinical trial protocols and data are protected pose a serious threat to the tenets of transparency and hence accountability in clinical trials in India. • The content of Schedule Y of the DCA needs to be revised so that the stronger requirements and larger concerns of the ICMR guidelines and that of the Helsinki Declaration are reflected explicitly, forcing trial protocol documents in India to meet these requirements. Accountability of Ethics Committees • Since vulnerable participants are involved in clinical research, ethics committees must be independent and able to provide adequate oversight. However, many ethics committees are controlled by the institution and cannot act independently. Most of these members are untrained and may not be competent to assess ethical and scientific issues. There is also no clarity about the extent of their responsibility and their accountability. Can they be taken to court for failure to perform their fiduciary duty? There is no regulation of the ethics committees themselves. • Similarly, the lack of co-ordination between the various ethics committees in a multi-centric trial raise serious concerns of an absence of rigorous approval processes.

17 •

The current law in no way addresses the issue of the conflicts of interest amongst members of ethics committees and other aspects of drug trials Assessment of Adverse Events, their management and compensation for injuries in clinical trials • Reporting of adverse drug reactions and adverse events is dismally low in the open market post licensure • Over all needs in this regard – i. Clarity in law and guidelines ii. Awareness for all stakeholders & training iii. Advocacy to participants’ rights iv. Categorization of injuries v. The death issue vi. Integration of various fields – medical, legal, financial etc vii. Urgency • Several complexities related to compensations and insurance. Who takes the insurance? Which companies provide? If the insurance was to be taken for each participant individually, companies would charge higher premium, thus creating a ‘hurdle’ for research companies. These costs also need to be factored in to understand why India is becoming a hub. • The issue of follow up and compensation is also linked to the ambiguity on the legal provisions of establishing causality. Phase IV Trials • The DCGI has since June 15th, 2009 made it mandatory for any researcher who plans to conduct a trial involving human participants, of any intervention such as drugs, surgical procedures, preventive measures, lifestyle modifications, devices, educational or behavioural treatment, rehabilitation strategies as well as trials being conducted in the purview of the Department of AYUSH to register the trial in the CTRI before enrolment of the first participant. However several other research studies continue to go unregistered as ‘observation studies’, ‘operations research’, ‘demonstration projects’, etc • There is an urgent need for clarity on the distinction between phase IV clinical trials and other oft used terms such as post marketing surveillance, demonstration projects, observation studies and so on, that are not spelt out in the law at the moment.

Endnotes 2

The rationale for these measures is - The pharma sector has increased in a galloping fashion during last 20-30 years whereas the drug regulatory mechanisms have been woefully inadequate. The manufacture and export of medicines has increased to over Rs one trillion during the last decade. The number of clinical trials has increased to 900 as of June 2010 and the clinical trial industry revenues are expected to grow at a rate of 65 % per year. New drug approvals (223 in 2010) and the number of biotechs

18 and biosimilars are also increasing in number. There are more than 50,000 drug formulations in the market, majority of these are irrational medicines or irrational Fixed Dose Combinations. It has been repeatedly pointed out that drug promotion in India to the health professionals and to the lay people has to conform to standards and the prescription practices and use also needs auditing. 2 The rationale for these measures is - Clinical trials are the fountain of drug development. But there are safety issues especially when many trial participants are poor and illiterate and/or are not in a position to assert their rights and entitlements. There is need for balancing clinical excellence, ethical requirements and rights of clinical trial participants. Ways of legally regulating clinical trials and CROs (contract research organisations) must be strengthened even as we develop laws on liability to trial participants and of those in charge (including ethics committees). Ethical standards in India where a trial is being conducted should not be any less stringent than in the country where the drug has been discovered. [See also Annexure 1: Note on Regulation of Drug Trials in India for the Working Group towards 12th Five Year Plan, from SAMA.] 3 Y.Madhavi, et al (2010): “Evidence-based National Vaccine Policy”, Indian J Med Res, May, 131: 617-628. 4 The rationale for this measure: Vaccines are to be given to healthy, susceptible individuals and they are given to millions of people in India. The safety and efficacy of vaccines has to be far more strictly, scientifically assessed compared to other medicines. Recent experience of the debate around introducing new vaccines in India has shown that the matter is exceedingly complex and the technical capacity of the CDSCO will have to be considerably enhanced to be able to handle this responsibility. 5 The rationale for this measure - A strong Public Sector Vaccine Manufacturing capacity will also play an important role in regulating the vaccine manufacturing in the private sector. If the private sector refuses to or is unable to meet the national requirements in emergency situation or in other times, strong public sector vaccine manufacturing capacity would be an option to the meet the national needs and also induce/compel the private sector to fall in line. 6 The background and rationale of this measure is - A very large number of medicines in India do not have any generic name; they are available only as brand names. Many of these brands are irrational Fixed Dose Combinations, and top-selling, and are not recommended by standard medical textbooks/authorities and hence have no generic name. Unlike rational FDCs like oral contraceptive pills or co-trimexazole or Oral Rehydration Salts, these irrational FDCs have only brand-names. Hence ‘promotion of generic drugs’ implies elimination of these irrational FDCs. A survey of the 300 top-selling brands (as per the ORG/IMS list) with sales of $3.75 billion, revealed that they included medicines of uncertain efficacy, safety, such as ginseng, liver extract, Vitamin E, and nimesulide; irrational combinations of antibiotics, which lack therapeutic justification; and expensive congeners. (A study by LOCOST/JSS says 60 % of the top-selling 300 medicines are not in the National List of Essential Medicines, 2003.) Today in India there is no mechanism to review periodically and routinely the rationality and safety of the medicines marketed in India. Hence to clear the backlog, the task of going drug by drug would become too huge, to assess each of such medicines separately and it will decades to finish this work. Hence it is necessary to draw up broad criteria of drugs that can be weeded out and these may be applied retrospectively and prospectively, so that the country can start on a clean slate once again. Indeed this task needs to be taken by a special commission in mission mode. 7 Much pharmaco vigilance in India falls by the way side because of lack of adequately generated data. Hence to start with, specific

mfc bulletin/Aug 2011-Jan 2012 programs on drugs that are suspect in creating renal, respiratory, cardiovascular complications, drug-drug interactions, etc. will have to be taken up. Simultaneously a program of ‘medical transcription’ – putting prescription and diagnostic related data on computer amenable to generating useful information thereafter – will have to be taken in the public and private health facilities of major cities (as they are the ‘magnet’ for complicated cases) as well as from primary to tertiary in certain districts and certain states. The will have to be used to generate data on antibiotic resistance and on long-term surveillance. 8 This organisation would· • undertake independent appraisal/comparator studies, and make recommendations, on new and existing medicines, treatments and procedures • undertake independent studies, including meta reviews, for treating and caring for people with specific diseases and conditions • make recommendations to the Government of India, local authorities and other organisations in the public, private, voluntary and community sectors on how to improve people’s health and prevent illness and disease. • suggest cost cutting measures through studies on medicines, vaccines, medical equipment, and technologies • carry out impact studies of health policy measures and health impact of intersectoral policies. • formulate indicators of quality health care from GP level to tertiary levels. make recommendations on the content of advertisements and marketing literature of drug and equipment manufacturers 9 The background and rationale of this measure is - There is a great deal of misleading and unethical promotion of medicines in India. Doctors are misled about the indications, therapeutic benefits, side-effects and contraindications. Doctors and their organizations are indirectly bribed in various ways to make them party to irrational, excessive use of costly medicines Though the Medical Council has now put stricter restrictions on doctors about accepting gifts and favours from the pharma companies, MCI has no mandate over hospitals. Hence curbs are require d on pharma companies also to prevent misleading, unethical promotion. Lay persons are given a hugely exacerbated idea about the benefits of OTC (Over the Counter) medicines, with no idea about the contraindications and side effects.This misleading promotion also needs to be strictly restricted. (The definition of “OTC medicines” needs to be clarified to the public so that there is no room for doubt.) The voluntary codes by the pharmaceutical industry have been in existence for decades. However there has not been even one complaint or action taken recorded on the website of the association of pharmaceutical companies so far. Given the fact that many issues have surfaced over the years in the media, this indicates a discrepancy pointing to the need to safeguard the interests of the consumers in a more effective manner. The world over, many countries like the USA after long periods of trials with voluntary codes have moved to legally enforceable regulation of promotional practices as voluntary codes have failed abysmally. 11 For Medicine price mechanisms in other countries, see Chapter III of the Report of the Drug Price Control Review Committee, Dept of Chemicals and Petrochemicals, New Delhi, October 1999. For a more recent review of these see: Amit Sengupta, Reji K. Joseph, Shilpa Modi and Nirmalya Syam. “Economic Constraints to Access to Essential Medicines in India.” Centre for Technology and Development and Society for Economic and Social Studies in collaboration with WHO SEARO, 2008.

mfc bulletin/Aug 2011-Jan 2012

19

Budgetary Outlay at TNMC Prices for ‘Medicines for All’ Scheme for Public Health Facilities** Subject Head 1.

TN’s budget for medicines at 40 % utilization

A mount in R s 210 crores

2.

All India requirement at TNMSC procurement prices

3530 crores

3.

All India requirement inclusive of ad ditional requirement for the very poo r 20 % patients who are currently totally deprived Total A ll-India Requirement for 5 year Plan Period for medicines for PHFs

5735 crores

28675 crores

At Rs 5735 crores x 5

5.

a) At 50 % central contribution of running co sts

14338 crores

(50% of Rs 28675 crores)/5 = Rs 14338 crores/5 = Rs 2868 crores per y ear.

6. 7.

b) C apital Costs IT enabled Supply Chain system @Rs 5 lakhs per d istrict for all India W arehouses and related infrastructure like cold storage, storage racks @ Rs 2 crores per district for 631 districts c) Total Capital Costs all-India

31.55 crores 1262 crores

631 districts @ Rs 5 lakhs

4.

8.

9.

10. d) Center’s Contribution at 50% of (a) plus 100 % of (c)

1293 crores 15631 crores

R emarks Rs 210 crores1 for TN population of 7.2 crores as per 2011 census provisional figures. (In TN , out of patients seeking treatment, 40% go to PHFs, that is 40 % utilization) Extrapolated to patients seeking treatment from 121 crores population of India: Rs 210 crores x (121/7.2) This translates to 62.5 % increase in patients attending PH Fs*: 1.625 x Rs 3530 crores . See footnote.

At 1 0-12000 sq. feet per wareho use; and 631 districts @ Rs 2 crores.

Rs 14338 crores plus Rs 1293 crores

* Out of per 100 patients, currently only 80 seek treatment; 20 are out of the reach of both private and public health services. Out of these 80 patients, 32 (40%) go to the PHFs; rest 48 go to private practitioners. Due to the ‘Free Medicines for All’ scheme, it is assumed that all over India, like in TN, now 40% of patients will take treatment at PHFs. Secondly, now the 20 patients who were hitherto unserved, will also take treatment from PHFs. Thus out of 100 patients, now 52 instead 32 patients will take treatment at PHFs. Thus under this assumption that all these 20 hitherto unserved patients will also take treatment in PHFs, number of patients reaching PHFs will increase by 62.5% (52/32 x100). 1

Source: TNMSC, July 2011. This does not include Centre’s contribution for National Programme etc.

A Detailed Explanation of How the All-India Estimate of Rs 5735 crores was arrived at Under Key Assumption that out of 100 patients needing tt, 20 patients (20%) who are very poor and deprived are not reached at all. And of the rest 80 patients (80%), 32 (40%) seek treatment (tt hereafter) in government /Public Health Facilities (PHFs); and 48 (60%) seek tt in the private sector. The calculations remain same if we had assumed 20% of the population (say of TN or India) was unserved. 1. Total population of TN = 7.2 cr. And let the fraction of patients needing tt to total population be y. Therefore Total No of Patients needing tt in TN = 7.2 y crores. 2. Out of 7.2y crores, 20% are not reached in TN. That is 20% of 7.2y crores are not reached (or 0.2 x 7.2y crores). Therefore those who are able to access tt is 80% of 7.2y crores (or 0.8 x 7.2y crores). 3. Utilisation of the PHFs in TN is 40%. That is 40 % of (0.8 x 7.2y crores) = 32 % of 7.2y cr = 0.32 x 7.2y cr = 2.304y cr.

4. Total Outlay of TN = Rs 210 cr 5. Therefore per patient cost is = Rs 210 cr /2.304y cr = Rs. 91.145/y. 6. Total population of India = 121 cr. 7. And therefore assuming same y factor, there are 121y crore patients in India 9. Out of this No of patients seeking tt from PHFs = 32% of 121y cr = 38.72y cr patients (as in 3 above). 10. Cost of free supply for above = 38.72y cr x (Rs 91.145/y) = Rs 3530 cr approx. (A) 11. 20% of patients in India = 20% of 121y cr = 24.2y cr patients. 12. Cost of free supply of medicines for (11) above = 24.2y crores x (Rs 91.145/y) = Rs 2205.70 crores. (B) 13. Total of (A) + (B) = Rs 3529.13 cr + Rs 2205.70 cr = Rs. 5734.83 cr say Rs 5735 cr. This amount will reach through PFIs, (32 +20) % x 121y cr or 52% x 121y cr patients.

** A Draft Submitted to the MOH/Plng Commn Working Group on Drug Regulation, July 2011, S.Srinivasan and Anant Phadke. Please note some other estimates assuming that medicines need to be 15 % of the budgetary expenditure (now proposed at 3 % of GDP) have arrived at Rs 30,000 crores per annum.

1

20

mfc bulletin/Aug 2011-Jan 2012

Towards a Critical Medical Practice Some Thoughts Regarding Universal Access to Health Care - R. Srivatsan1

The stress in my paper is to understand health care in our neoliberal times. 1. What is the political and economic terrain health care is situated on and what are the constraints this terrain poses for a politics of health? 2. What is the historical context of ‘our’ approach, as doctors and medical practitioners to health care when seen against this terrain? 3. How do these affect the status and condition of patients and others who require health care? Indeed it is from the vantage point of a patient’s need for a cure to ailments that put them out of a day’s work that this paper is organized. The importance of this daily basis arises from the dominance of daily wages in this era of informal work. At the same time, the problem of the patient in the current terrain of medicine, as I will argue, is goes beyond cost, to risk and survival. Terrain of Neoliberal Health Care in Contemporary India Health care policy today is determined by the Planning Commission and medical experts, and is influenced by the medical and p har maceutical industr y, corporate hospitals and the insurance business. While most industries have this common originary framework of expertise finance and policy, the specificity of health care is that the effective point of application of health care policy, business and science is a person’s body, touching on her suffering, ability to live, work, earn and support a family. The second difference is that, while most enterprises justify their ventures in terms of profitability and utility, medicine does so in the name of the latest science. If it is a vaccine (HPV, HiB, OPV) scientific knowledge determines what needs to be prevented, technology determines the method, and statistical/ epidemiological studies, the target population. In ad vanced therapeutic car e under insurance (Arogyasri), specialists in corporate hospitals, 1

insurance business and policy makers offer the most advanced biomedical procedures to the poorest patients. At least two issues stand out: 1. The decision on what is necessary for health care is beyond the ken of the ordinary user; such a decision is taken today by government, industry and expert. 2. The therapeutic or prophylactic solution to any health problem is decided according to the norms of the best biomedical science. One criticism of medical care in neoliberal India proposes that we must go back to a state run health care system. The reasoning is that private industry’s profit motive corrupts medicine’s duty to the patient, causes catastrophic expenditure and lack of access. Critics argue that the state run health care system will be more equitable, effective and economical, therefore have more government hospitals, tighten the system, make doctors work where required. However, when we look at state run medical care after 1950 what do we see? We see a primary health care system that runs powerfully funded, centrally coordinated immunization, disease eradication and family p lanning p rogr ammes. T he state administration relinquishes curative care to the private practitioners and nursing home businesses that begin to appear. The need for everyday medical care remains poorly addressed in most places except in metropolitan centres. From the 1960s the government focuses on public health measures that improve national health statistics in the eyes of the international health fraternity and leaves clinical services to private industry. More broadly, Veena Shatrugna argues that food policy systematically seeks out the most economical solution to the problem of food, cutting scientific corners with respect to health and resistance to infectious and chronic diseases. There is a remarkable blindness with which upper-caste administrators find

The unabridged version of this paper draws on many of the insights gained in collaborating on the Introduction to Towards a Critical Medical Practice, in writing the paper on “Development and the administration of public health”, and in thinking about the different essays as part of the collaborative effort to edit the papers in the volume. See http://ifile.it/so8g6k4/R for full version. Email of author: <r.srivats@gmail.com>

mfc bulletin/Aug 2011-Jan 2012 economic solutions that override the diverse cultural and nutr itional wisd om of many d ifferent communities. Similarly, the system of vertically implemented immunizatio n and eradication programmes in India reflects an economistic, reductionist, utilitarian and caste-culturally distanced mode of thinking about public health. The history of governmental health care in India has left us without a viable, effective system of curing people of diseases that immediately cut into their daily wage. Would a better-funded health care programme result in a more equitable and effective system than before? The proposals for Universal Access to Health Care (UAHC) answer in the affirmative. However, there seems to be little reason to expect that a combination of vertical preventive programmes and advanced tertiary clinical services will solve problems of the kind represented by a common man or woman. In what follows, I will address the best-case scenario of free universal access to health care. To put it more clearly, UAHC as envisaged will not result in initiatives to focus on diseases and syndromes specific to our condition and it will certainly not put a brake on excessive medical technology and laboratory testing. The fundamental problem of medicine’s orientation is not addressed by universal access. Why? Government and Health Care in the West – Implications for India Recent historical studies have shown that medical science has a symbiotic relationship to the science and techniques of government. The administration of care to a ‘sub-normal’ population (the unemployed, the poor, the mad, those afflicted with leprosy, those suffering from a plague epidemic, etc.) provided a series of experimental sites in general administration. So ciety learned to d iscipline and go ver n its population to a considerable extent through its experience in administering medical care to control morbidity, epidemics and mortality, (and idle malingering). The technology of public health and the science of clinical medicine both grow in this symbiotic relationship with administrative practice in the nineteenth and early twentieth centuries. In the middle of the twentieth century, when this

21 interrelated configuration of government and medicine has to cope with the aftermath of World War Two, ano ther mutation takes place in that relationship, first in Great Britain. The Beveridge proposals formed the basis of the welfare state, and within it the National Health Service (NHS). Thus, a broad network of governmental health care with its specific emphasis on medical practice, education and research replaced the private practitioner. The other implicit aim of these policy directions was to drive the health care industry as a Keynesian macroeconomic stimulus to the weak post-war economy. While the NHS succeeds brilliantly, one influential assessment is that by the 1970s, despite the NHS being conceived as the administration of ‘a right to health’, a curious stagnation takes place: instead of people becoming healthier as a result of good quality care, they become passive consumers of health in a medical industry that thrives on their problems. Without necessarily agreeing with the last assertion, we may ask what this historical relationship between medicine and government implies for us? One, it implies that modern medicine is governmental in its structure and its perspectives and priorities are determined by statist concerns. The structure of medical knowledge uses techniques and perspectives of the sciences of the state (statistical reasoning, epidemiology, determination of effectiveness based on population studies as in EBM). The problems it chooses to address and the methods it adopts to address them have evolved in response to the needs of government. Two, the concern of the doctor for the individual patient is always expressed through the scientific perspective of the risks and consequences of disease to the governed population and to society as a whole. Biomedical cur ative str ategies are given overwhelming legitimacy in doctors’ minds by institutionalized research criteria. Doctors administer medicine in a governmental mode. This is a systematic effect o f scientific med ical knowledge and disciplinary training. The implication of this is that there seems to be very little hope for a fresh perspective within medical science on what is necessary for health care in our context as presented by patients on a day-to-day basis.

22 Three, medicine is deeply embedded in politics, economics and administration. Therefore, ‘advanced’ biomedical science is an ideological cover to push solutions to research problems determined within a framework specified by policy and investment. Governmental science policy and corporate medical industry find different historical routes to sponsor a research problem (e.g., unhealthy lipid profiles). Business invests in a medical strategy (e.g., statins) to solve this problem. The norms of medicine (e.g., lipid levels) are thus fully infused with policy and multi-billion dollar business interests. There is no reason why these biomedical solutions should be the best ones in all the contexts in which it will be applied. One of the impasses of medicine today is that we do not have strong criteria for medical research strategies for common illnesses in our country and for treatment approaches that are sensitive to cost and the individual context of the patient, while we have imported specialties that have limited utility and high cost. Four, the birth of specialty medicine (cardiology, neurology, endocrinology, pulmonology, etc.) occurs in this historical context after 1950. When medical care and research have macroeconomic implications that touch 9-14 percent of the nation-state’s budget, the scale, scope and cost of health care go beyond the reach of the individual patient. There also occurs post 1950, the tertiary care formatting of medical knowledge. Clinical care and treatments guidelines are defined in the mode of technology intensive tertiary care medicine in the context of universal health care. Any treatment that is defined for primary or secondary care for the non Western setting would be regarded as inferior to the gold standard of tertiary care. When these solutions to medical problems find their way to the Third World they are beyond governmental budgets too. This (as the technological horizon within which the privatized co rpo rate hosp ital comes into being) is the fundamental reason why we have ‘catastrophic out of pocket expenditure’ in India. Thus medical science and governmental policy as we know them are not promising candidates to find solutions to our medical problems. The reason is that they exist within a network of power relations that preclude such an objective. Medicine as it is

mfc bulletin/Aug 2011-Jan 2012 structured today is responsible more to the state and its indicators of health, business needs, and the guidelines of scientific practice and not the need and interest of a patient. This is the fundamental mismatch. However, if and when we begin to implement UAHC, and if by good fortune and excellent government the individual patient would not have to pay anything, why worry? And after all, given the abysmal level of health care, shouldn’t anything more only be good? What are the implications of this mismatch? Location of Patient under the Horizon of Universal Access to Health Care in India Firstly, there is the risk that the patient is exposing her body, livelihood and life to a medical action that she has no knowledge or control of. This lack of control is exacerbated by a complete absence of a family doctor who would help the patient mediate, assess the efficacy of, decide on, prepare for, and control the consequent risks of the intervention. Secondly, the governmental baseline pricing of packages (recommended by the World Bank in 1993 and by Amartya Sen more recently) will not address the major part of the problem. The emphasis on public private partnership frameworks, goals and targets of success in the neoliberal environment give us little assurance that the implementation mechanism will keep the patient’s interests primary. The danger is that the Indian government is looking at ways to forcefeed economic growth through opening the medical industry and hospital to large scale investment in medical sector that will grow any where up to 15% of the national income. What are the Implications of a Patient Location in this Terrain? Undefined Medicalization/Invisible Morbidity: When government and business come together to decide health policy, one danger is that there will be no protocols that determine what is or is not a health problem. For example, is lack of food a health problem that is to be solved by industrially packaged nutritive mixes, or by providing access to healthy food? Or, is the threshold of blood glucose, blood pressure or lipid profile to be reduced by 10 units to make it a healthier body or to increase drug sales? What actually is a healthier body? And the mirroring

mfc bulletin/Aug 2011-Jan 2012 question, what is an important disease that needs to be tackled? Who decides? One problem here is economic—what is the best way to spend tax money to achieve the well being of Indians? The deeper question is what is a priority as a health problem? What is not? Undefined medicalization and invisible morbidity are the two poles of mismatch between medicine offered and needed. Medical hazard and iatrogenic morbidity: The ‘discovery’ of new diseases, the finding of new health risks, and the introduction of tighter thresholds to known health parameters would lead to one level of medicalization. The second hazard is that of iatrogenic complications intro duced by, often unnecessary, medical intervention. Governmental benchmarking and insur ance comp ensatio n cannot cur b the overenthusiasm of medical and surgical enterprise. T he thir d closely related d anger is that this positioning of health care as the next stage in economic growth will find systematic links with the liberalization of clinical trials. In the haste to make markets succeed, the stage is comprehensively set for the vulnerability of different uninformed populations to medical experimentation, and to iatrogenic complications and morbidity, not to mention mortality, due to ill-controlled introduction of ‘scientific care’. Medicine’s contract: On our current horizon with and in spite of UAHC, the following spectrum of risks emerge for populations: as people for whose health problems there is neither visibility nor treatment; as patients treated by invasive pro ced ures with consequences they don’t understand; as consumers of medical drugs with poor comprehension of their long term effects; as participants in medical insurance co ntr acts that they have no co ntr ol over; as population samples of subjects for clinical trials. It is perhaps with these new connotations that we as health care professionals and activists need to complicate the meaning of the statistical term ‘populations’. These multiple connotations should in turn reverberate to the mantra about ‘demographic dividends’ that emerges around the year 2000: ‘India’s population is her strength’.

23 Some Tentative Proposals Some tentative directions: 1. Perhaps we need a community based process of determining the kind of medicine actually necessary, in order to both encourage and devise protocols for new medical practices and solutions. There is no clear articulation of what medicine is needed most desperately. At the same time, it is also clear that people do not know enough about medicine and health care to establish the proper direction right away. Therefore and however, it is imperative that community discussion, control and audit of health care begin. 2. Devising protocols to monitor and control medical practices would imply a research in medicine, not as biomedical science driving specialty medicine and tertiary care, but as effective cultural, political and economic practices of meeting health care needs in our context. We would need to learn from the kind of doctors who meet the needs of the community and gain its confid ence, even if they are pr ivate practitioners (or even ‘quacks’) in small towns. Underlying this is a reversal of emphasis on medical politics from one that is seen as an expert scientific discourse, to one that has an everyday significance where the layperson can speak. 3. It is clear that many large-scale interventions against government and business policy often are successful only when taken up through party politics and electoral commitment. So far, in independent India, routes of direct action and civil societal politics have had limited purchase and scope. It is also amply clear that elected representatives do not often represent the people who elect them. Yet generating a measure of accountability to that process would be an important aspect of a politics of health in the twenty first century. 4. Finally, it requires fresh thinking on the part of experts in medical education to draw the pedagogic consequences of a reorientation of medicine towards the patient’s needs in terms of a requisite medical curriculum. This reorientation is essential given the current thr ust in medical education to war ds equipping the student with the ‘most advanced scientific knowledge’.

24

mfc bulletin/Aug 2011-Jan 2012

Some Bark, Little Bite: The Draft National Pharmaceuticals Pricing Policy, 2011 - S. Srinivasan1

A new pharma pricing policy has been announced – the draft National Pharmaceuticals Pricing Policy (NPPP2011). The new policy declares that all 348 essential drugs (as per the new National List of Essential Medicines, NLEM 2011) will be under price regulation. The shift to essentiality as criteria, from market share, is to be welcomed. However the policy still leaves a lot of loopholes for non-essential and irrational drugs to be made. It also has made calculating ceiling prices of the many drugs not in NLEM a tedious, if not impossible, exercise. In addition to the NLEM 2011, top selling 300 drugs of the IMS could have been covered. The draft policy delinks the ceiling prices of formulations from the price of bulk drugs. Indeed, the arguments given in the draft policy for removing price control of bulk drugs do not make sense. Government should have kept the option of price control on bulk drugs in the event of cartelization or abnormal increases in price of bulk drugs. The latter may result in the scarcity of a particular essential drug formulation unless it is already overpriced relative to the cost of the bulk drug used. Worse, this may result in the bulk drug or the formulation not being made within the country. Secondly, using the WPI (Wholesale Price Index) to revise prices is not a good idea. It adds an inflationary element to the ceiling price automatically every year. The WPI (100 for base year 2004-05) for 201011 is 143.3. Most drug prices have not really increased 43 percent during the period. It would have made sense to have the ceiling price of a drug formulation tied directly to the related bulk drug price increase over the year.

Buttressing a Market Failure The arguments for totally relying on Market Based Pricing (MBP) of formulations apparently does not recognize the fact that there exists a wide range of prices in the market of the same formulation and that prescribers, and therefore patients, tend to place more value on the costlier brands of the same formulation. In medicines unlike say soaps or cars, the brand leader is also the price leader: such phenomena are a result of anti-competitive forces, indeed they signify a market failure of sorts. The proposals of market based pricing do not attack these

but in fact buttresses them by legitimizing higher prices. The key para in the draft policy is para 4.7: “The Ceiling Price would be fixed on the basis of Weighted Average Price (WAP) of the top three brands by value (MAT value) of a single ingredient formulation drug from the NLEM on per standard dosage basis.” The WAP idea means that it will end up legitimizing high prices especially if the top three brands are overpriced: top selling brands – with a few exceptions - would be of the costliest brands. That is the norm of the medicines sector, thanks to asymmetry between consumer and prescriber/manufacturer. It means that that regardless of overpricing and profiteering, if the market “accepts” it, the price is ok. Never mind the patient may get poorer in the process. It also legitimizes the mistaken notion that higher priced drugs are of better quality. In our case, for example, albendazole (see Table below) selling above Rs 12-13 per tablet (price of current market leaders) would be legit. Ceiling prices need to have a clear relationship with the cost of the raw material at least. The WAP formula has in effect no relation with the cost of raw material, let alone the cost of other inputs. Column 6 in the above table shows that the MRP to raw material ratio is about 3000 % to 5000 % in quite a few essential drugs. Should a Government legitimise such super profits? Most retail pharmacies do not keep cheaper versions because of lesser margins; eventually all lower priced brands will move towards the higher ceiling price even as ‘premium’ prices, including that of overpriced imported drugs like Novartis’ Glivec, will take a hit with the WAP formula. The draft policy gives a formula to discourage nonstandard dosages. The same thinking could have been applied to discourage irrational and unscientific drugs outside the NLEM. One can discourage irrational combinations, and attempts to circumvent the ceiling price, by taking the cue from the Pronab Sen Task force Report – from which many of the recommendations have been taken anyway – which says, “For formulations containing a combination of a drug in the NLEM and any

1 (A version of this article appeared in the Hindu Business Line, November 8, 2011. A more detailed critique of the policy has been filed in November 2011 in the Supreme Court in the ongoing pricing case of AIDAN, mfc, LOCOST and JSS vs Union of India.)

mfc bulletin/Aug 2011-Jan 2012

25

A Comparison of Medicine Prices (prices in Rupees)

Generic Name of Drug (1)

Unit

Chittorgarh Tender Rate (3)

MRP Printed on pack/strip (4)

TNMSC Prices (5)

(Column 4/ Column 5) (6)

Albendazole Tab 400 mg

10 tablets

11.00

250.00

4.55

54.94

Alprazolam Tab IP 0.5 mg

10 tablets

1.40

14.00

0.51

27.45

Amlodipine Tab 2.5 mg

10 tablets

2.30

23.00

0.41

56.01

Atorvastatin Tab 10 mg

10 tablets

9.90

65.00

2.10

30.95

Cetrizine 10 mg Diazepam Tab 5 mg

10 tablets 10 tablets

1.20

35.00

0.49

71.42

1.40

18.00

0.55

32.72

(2)

Note: TNMSC (Tamil Nadu Medical Services Corporation) prices are from its website, http://www.tnmsc.com/tnmsc/notification/ Drugs232.pdf, for 2011-12. Chittorgarh prices are of well-known companies and are at http://chittorgarh.nic.in/Generic_new/ generic.htm. Column (6) gives an idea of how many times the retail market MRP is in comparison to the TNMSC procurement price – the latter being almost near the cost of production.

other drug, the ceiling price applicable to the essential drug would be made applicable.” Sales tax and excise duty could be higher for drugs outside NLEM 2011 and zero for NLEM drugs. The draft policy could also take another recommendation from the Pronab Sen Committee: debrand, that is remove brand names, to ensure true competition among generics.

Welfare should be exempted. If by the latter government means vaccines, we do not see the reason for exemption as vaccine PSUs could be revived and their prices should be the ceiling price. Otherwise, it will encourage the corruption which is at present rampant in drug procurement and/or justify inefficient public procurement at high prices.

Inexplicable Exemptions

So what is a better pricing policy? That will be one that brings down the prices of overpriced drugs, that has some linkage to the actual cost of production, and therefore to the cost of the raw material, and does not legitimize overpricing of drugs. Nominally reducing the price of the top-selling brand is tokenism. A good starting point would be to take as reference price the prices of well-run public procurement systems and take a multiple, say 4 to 6, of the reference price as the ceiling price. The present WAP procedure will make the ceiling price 20 to 70 times the public procurement price – which is a bit rich.

The draft policy lists certain exemptions which again are inexplicable: all drugs costing less than Rs 3 per unit are to be exempt. This again legitimizes overpricing of drugs which cost around 10-20 paise and begs them to be priced near Rs 3/-. An example is cetrizine which costs less than 15 paise per tablet to make but the brand leaders are available near Rs 3/-. Why should this be condoned? Should much needed iron plus folic acid tablets which cost to produce less than 10 paise per tablet be given leeway to sell at or near Rs 3/-? Most retailers will give only a strip of 10 even when I need a couple of tablets only. We also do not see the reason why drugs which are part of Hospital Supply as maintained by M/o Health and Family Welfare; and drugs which are part of Public Health Products as maintained by M/o Health and Family

The draft policy gives the impression of a policy cobbled to satisfy perfunctorily the Supreme Court Orders of March 2003 and October 2011, one that will leave major players mostly unaffected. A policy with some bark and a little bite.

26

mfc bulletin/Aug 2011-Jan 2012

HHR for UHC: A Roadmap - Shyam Ashtekar1 with help of Tarun Seem

Overview of HHR in India

Table 1: HHR Situation in Some Countries

Optimal HHR (Health Human Resources) is a precondition to any good health system and favourable outcomes. We need the right HHR numbers, team composition, placement and supportive working conditions. However India’s public health system has been facing serious problems on all these fronts of HHR, more so in some states than others and in rural more than urban situations. The HHR is also largely (70%) segregated in the private sector, and hence is not available to the public health sector.1 The Indian HHR data sources are not reliable and updated, a large number of unregistered health workers are operating in private and rural sector and some of them get included in Census & NSSO data. I have liberally used the above-mentioned PHFI and NHSRC2 reports which are drawing from sources like MOHFW, Census, NSSO and WHO. The recent HLEG report on universal care, though not published, was available from mfc sources and has been helpful in updating and completing the larger picture of HHR, so also the Lancet paper by Mohan Rao et al has been helpful for cross checking and buttressing some issues.

1

<shyamashtekar@yahoo.com>

Source : NHSRC as per Endnote

The PHFI study group has given a comparative statistics of HHR and suggests that HW (Health Worker) density in India is about 1.9 (doctors and nurses) per 1000 people and the 12th FYP uses the same figure while advocating the need for 2.5 HHR per 1000. But if we include other health workers also, the HHR tally is 3.65 per 1000 population (see Table 1). The composition of HHR in the countries listed in table 1 varies considerably, obviously due to historical development of each health system. For instance, China has its million plus rural doctors counted in the HHR. USA seems to have a HHR-heavy health

mfc bulletin/Aug 2011-Jan 2012

27

system. India looks a minimalist on HHR in all the categories. The architecture of the ‘HHR pyramid’ and the relative size of various layers are all important and strategic to the making of a health system. For India it is already high time for important choices/shifts about the size and role of ‘primary HW layers’ in India or allow the drift to a specialist-centered health care system (which will come with its own demands of technology, costs and infrastructure). It is also pertinent that we have not given much role to ASHA as care providers.

HHR Norms, Estimating Needs and Shortfall As a caution, we must remember that HHR estimates can vary widely on the type of model we use for health care and the mix of HHR for each model. The HLEG model, submitted to the Planning Commission, may be finalised with modifications and can substantially alter the HHR needs and projections.

Fig 2: Health facilities model for a 30 lakh district: TH denotes a Tertiary Hospital with medical college

HHR is an ever expanding list and the simple doctor: population ratios are not enough. The Technical Report by PHFI (2008) using NSSO and Census studies explains the coding system for various HHR as shown below:

private: Public mix and urban-rural divide, it is difficult indeed to work out such norms for India. However, I have made some suggestions on the basis of a pyramidal model for UHC for a district as in Figure 2.

Allopathic physicians/surgeons - 2221; Health Professional (except nursing) - 2229

Basic UHC model for HHR needs Estimation

Dental Specialists - 2225; Ayurvedic physicians/ surgeons - 2222; Homeopathy physicians/surgeons - 2223; Unani physicians/surgeons - 2224 Nursing Professionals - 2230; Nursing Associate Professional - 3231 Sanitarians - 3222; Midwives - 3232; Pharmaceutical Assistants- 3228; Medical Assistants - 3221; Medical Equipment Operators - 3133; Life Science Technicians (Lab technicians); Dieticians & Nutritionists - 3223; Optometrists - 3224 Dental Assistants - 3225; Modern Health Associate Professional (except nursing) - 3229 Health Professional except Nursing - 2229 (80%); Traditional Medicine Practitioners - 3241 Faith Healers - 3242; The absence of ASHA and Anganwadi workers in this coding system is obvious. Each of these categories has several subcategories, for instance the allopathic doctors will include several branches of specialties. The specialist (SP) and Super-Specialist (SSP) categories include about 10 and 20 subcategories respectively and there are no norms about how many of which are necessary for the population. Adding the complexity of

I am suggesting here one model that integrates the 12th FYP suggestions3 of having a health subcentre in each panchayat cluster, and I support the PHFI/HLEG suggestion of also putting a Rural/Ayush doctor for a 5000 population under each subcentre. Assuming that a typical district has about 30 lakh population, with its main city having 10 L population and remaining population spread over about 1000 villages (and some small towns), I am suggesting a pyramidal model peaking in a tertiary hospital with medical college. I assume that the city will have almost similar health infrastructure like the rural clusters. I am for the present ignoring that 80-90% doctors are in private Sector and that there is some way of bringing them all in some public sector through contracts or PPPs or social insurance. This model builds on the current public health care model of India and includes two components hitherto missing (a) Urban health care system almost parallel to the rural one (b) a BRMS doctor or Ayurvedic physician at each existing SHC (5000 cluster) and a paramedic center at each panchayat (1000 population). Here is an estimation of HW needs of this model. I assume that super specialist (about 20 categories) will be available only in the tertiary hospital that is also a medical college which also has teachers in various pre and Para clinical branches. As for the urban model, I assume that there will be a TH (Tertiary Hospital with a medical college). With medical college, a Municipal hospital of 300 beds for 10 lakh

28

mfc bulletin/Aug 2011-Jan 2012

Table 2: Estimating health facilities for a 30 Lakh pop. dt.

specialists, super specialists, other docs and paramedics.

Tabl e2 : E stima ting hea lth f acilities for a 3 0 Lakh pop dis tri ct C a tegor y (pop se rve d) Beds Ur ba nuni ts R ur a l u ni ts T ertiary H ospital (30 L) 1 0 00 1 0 Di str ict /M uni cipal hos pit al 3 00 2 2 RH /W ar d H ospital (1 lakh) 30 10 20 20 50 P H C/U HC (30 00 0 ) 0 SH C (5 00 0) 0 20 0 30 0 P anchaya t (10 00 ) 0 0 70 0

The 882 population: 1 bed ratio arrived at in this model is about the public sector. The HLEG report desires that the beds in public facilities should be 2 per 1000 population, besides the private sector.4 It is not clear whether the HLEG report is ‘inclusive’ about the private sector hospital beds, which are currently about 3 times the public sector.

population of the city, a ward hospital for each 1 lakh population. An urban health center for 50,000 population and a health post for 5,000 population (subcentres). The rural structure, in addition to what we have today, should have panchayat level subcentres and a day care center at 5000 cluster. I have suggested two district level hospitals since one is already there and the second one accounts for 2-3 sub-district hospitals of 50-100 beds. The urban hospitals, including the medical college hospital add up to 1900 beds and rural for 1500 beds, hence about 3400 beds for 30 lakh people; yielding 1 bed for 882 people. The recommended norm is about 2 beds for 1000 People (PHFI). We have to account for some presence of private Sector even if in PPP domain, making about 2 beds for 1000 population. Table 3 is my construct from the 30 lakh district model and a projection for India. It offers 1 HHR for 251 population, hence 4 per 1000 and includes all health workers. Here I have also tried to work out the need of

From the numbers angle, it seems India has just enough doctors. There is a shortfall of 22 lakh paramedics including nurses. The urban sector will need 7.5 lakh USHAs. There is some confusion about the statistics of nurses: varying from 8 lakh (NSSO 2004-5), 12 lakh (MOHFW 2008) and 19 lakh as per the NCI website 2011.5 Therefore it is difficult to say if nurses numbers are enough or not. Besides we have a seriously skewed distribution of doctors and paramedics in favour of Southern and Western states, private sector and cities. Hence mere numbers cannot answer the need, it is how we place and utilize them. The HLEG report, available to the author from some sources (report yet to be published), uses a different and more exhaustive model for health care pyramid for a 10 lakh population, and recommends 3 ASHAs and 2 AWWs for a 1000 population cluster, and a rural doctor for subcentre. I am not clear if the HLEG report estimates only for its public sector model or for both private and

T a b le 3: H H R R e q u ir e m e n t s f or a 3 0- L a k h D ist ri ct an d P r oj e c t ion f or In d ia T h e D ist ric t M od e l H H R fo r 3 0L dis t

R ur a l

(A ) B e d s (p ub lic se c tor ) (B ) H H R - c a te go ry do c tor s SSP Sp M M-M O A y M O /B R M S D e ntis ts A dm in M O s T ota l do c tor s (C ) O th e r H W s N ur se s+ M id w ive s O the r P M W a r d A ss t e tc S up po r t s ta ff T ota l of oth e r H W s

15 00

U rb a n+ M ed c ol 19 00

0 24 0 28 0 41 0 10 0 10 10 40

In dia To ta l

34 00

po p pe r un it 88 2

C o un tr y ne e d 13 60 00 0

50 35 0 20 0 41 0 10 0 10 11 20

50 59 0 48 0 82 0 20 0 20 21 60

60 00 0 50 85 62 50 36 59 15 00 0 15 00 00 13 89

20 00 0 23 60 00 19 20 00 32 80 00 80 00 0 80 00 86 40 00

21 20

11 10

32 30

92 9

12 92 00 0

82 0 18 60 66 0

80 0 18 00 60 0

16 20 36 60 12 60 97 70

18 52 82 0 23 81 30 7

64 80 00 14 64 00 0 50 40 00 39 08 00 0

A ll H W s 11 93 0 25 1 47 72 00 0 A SH A s 36 00 83 3 14 40 00 0 (+ p lu s sign sho w s su rp lus H H R ) * * H L EG m a k e s dif f e re n t a s sum pti on s 1 1

HlEG: Final Report 2011: Section on HRH (To be published, curtesy mfc sources) 1

HLEG 2 01 1* *

A c tua l

2 09 09 1

67 67 56

sh or tf a ll *

NA 4 17 11 9 3 14 54 7

11 L a kh

20 L a kh

19 64 88 22 96 2

7 46 49

89 62 06

+ 3 22 06

82 35 88

4 68 41 2

23 27 6

6 24 72 4

84 68 64 17 43 07 0 90 00 00 0

3 02 89 30 + 7 56 00 0 0

mfc bulletin/Aug 2011-Jan 2012 public sectors, however it says that by 2025, only 80% of care will be free from OOPE.

29 Fig 3: Specialist types in a district study

Issues Regarding Doctors, Specialists, AYUSH & Modern Medicine Systems The category of doctors includes several subcategories and also the three systems of medicine (Allopathy, Ayurveda, Siddha and Homeopathy) in India. There are several serious issues regarding doctors. • The Medical Council of India (MCI body of modern medicine (MM) doctors) is unwilling to even consider the non-allopathic doctors while discussing doctor: population ratio. In reality the GP (General Practitioner) sector consists mainly of non-modern medicine doctors and this trend will complete the replacement of MBBS, thanks to the lure of technology bringing more monetary returns. BAMS and BHMS doctors quite often use modern medicines for various reasons but there is no valid legal permission for this, implying that their practice of modern medicine almost illegal. More and more BAMS doctors are working as paramedics and house doctors in private hospitals including so called ICUs on quite low salaries. The regulating bodies like MCI and CCIM (Central Council of Indian Medicine) have so far turned a Nelson’s eye to this problem. • India has a huge number of ‘quacks’ - about 25 lakh by one estimate - and this has stymied the problem of non modern medicine doctors practicing modern medicine. • Although we see Associations and bodies of modern medicine in denial mode about non-allopath doctors, the business nexus is growing from underneath thanks to cuts and commissions on medical bills. Among the modern medicine doctors, the PG category is increasingly dominant. In fact about half of the specialist categories have already become ‘primary care providers’(Gynec, Pediatrics, Ortho, Eye, ENT, skin, and dental) for better off clients. This has reduced the role for GPs/family Physicians in cities. This trend will grow as consultants will not entertain any patient not belonging to their specialty. This development will alter the desired population: doctor norms and make room for many more doctors. Some categories are already ‘super’ by now: joints, heart, kidney, liver, brain, spine, plastic surgery, hematology, endocrines, intensive care, infectious disease specialists, eye especially posterior chamber, imaging, gastrointestinal tract, urology, urosurgery, infertility, etc., have already reached the super status. We will have to factor these categories in

the HHR. Most super-specialties have subcategories of physician and surgeon (like a neurophysician and neurosurgeon). Even eye-specialty is now splitting into anterior and posterior chambers, not to speak of optometrists jostling for space. This subspecialization is hitting many branches. In small townships, we find only the 4-6 basic specialties- gynec, pediatrics, physician, orthopedician, eye, dentist and may be a skin specialist. This specialization tree is going to branch out. There is no formal arrangement of referral from one specialist to other in the private sector, and patient-clients have to negotiate each transaction afresh except when in IPD. The fees of super specialists (SSPs) are huge (Rs 5001000 per consultation) and much more for procedures. That brings a lot of earnings to SSPs and this has strengthened the private Sector in a manner that public sector will never attract SSPs. The clinical outcomes of SSPs are presumably far better than mere specialists. This has also made many ‘primary specialists’ irrelevant and underutilized. Unless specialists conform to technology and business model, one runs the risk of falling out of business. There is indeed a fierce competition. Many doctors migrate to developed nations - according to one study 54% of the AIIMS graduates leave the country, more so if they are from Open Category, still more if they have won some medals. 7 The Planning Commission cites WHO to show that about 100,000 Indian doctors work in the USA and the UK.8 The flip side SP and SSP concept has now a fragmented the ‘human patient’ into organs and systems with little attention to holistic health care. The family physician is in jeopardy. We might now need computer software to ‘reconstruct the patient in his/her fullness’ from profiles fed by various specialists and departments. The addition

30

mfc bulletin/Aug 2011-Jan 2012

to life expectancy has ensured that organ-specialists are essential to human life and civilization. There seems no escape from the medical industry no matter if you are poor or rich. UHC will have to account for this huge change.

The Quacks India has a huge quack problem (?), thanks to the (a) paucity of certified doctors in major parts of India especially North, and (b) lack of regulation of medical services. Viewed from the people’s need angle, quacks have served a crying need. There are various names, the most famous being the ‘Bengali doctor’. The practices are quasi-scientific since they get some kind of handson- training and have even some books to read. Since India has already a huge sector of ‘cross practice’, the quacks are only a notch away from the vagaries of the cross practice by registered Ayurveda and Homeopathy doctors indulging in modern medicine. The MCI estimate of quacks is around 25 lakh.9 They far outnumber the official doctors. MCI lodges occasional complaints but also admits that they are there because MBBS doctors are unwilling for working in rural areas. What is the answer? We must first answer the need in a rational and scientific manner. Training paramedics and equipping them with some essential primary medicines is the quickest and surest way of overcoming the problem. Putting an Ayurveda doctor or BRMS at every subcentre is another option. A mere ban is no answer. Trained doctors are unwilling to work in rural areas is a fact of life, and they perhaps cannot sustain in the average village of India. It is rightly argued that quacks can take a formal entry to BRMS courses or perhaps the legally trained paramedics. It is necessary to look upon quacks as a solution to a problem, not a problem by itself. The important concern is to prevent irrational use of medicines and exploitation of people. Legal and academic support is the best remedy. That MCI is even unwilling for BRMS courses is not a sign of pragmatism.

•

•

•

•

Nurses and Midwives India is severely short of nurses especially in the private sector. The update on numbers of nurses (close to 19 lakh) is a significant improvement on the 2008 figures of 13 lakhs referred in the HHR figure. • The equation for NMs/MWs (nurse midwives to midwives) is somewhat quirky. India will target for a 2 bed/1000 people density (hence 21 lakh beds and at least 7 lakh nurses. In view of our commitments to further provide 1 ANM per 1000 population (panchayat level); we shall need 5 lakh ANMs in the next FYP to add to the current figure of 5.75 lakhs.

•

•

We have already some 19 lakh registered nurses and midwives. Yet we find them in short supply, this needs further enquiry. Like the doctors, nurse’s distributions also follow the imbalances of Urban: Rural sectors and across states. HLEG report places total need of nurses at 2.53 lakhs and ANMs 8.14 lakhs. On the backdrop of this ‘surplus nurses’ it is not clear why the INC (Indian Nursing Council) asks for 2.4 million more nurses.1 The training institutes are clustered in 5 southern states (TN, Kerala, AP, Karnataka, and Maharashtra) with 57% of nursing institutes and 28 others states/UTs having 43% institutes. Out of the nearly 11.75 lakh working nurses, 54% are in the five southern states, with rest of India having 46% nurses. On the whole India has a shortage of nurses, with a nurse for 625 people. The very same states also have major share of doctors and hospital beds, hence the nurses will follow this pattern. However, even in these states, private hospitals often work without qualified nurses. Nurses from Kerala are found working in many metros. The quality of training in nursing institutes, especially private ones and of the ANM schools is questionable. Corruption in granting private ANM schools is rampart. Nursing council, unlike other councils, also conducts its examinations. Independent universities must be in charge of educational administration and examinations. Nursing councils grant licenses and also conduct exams, which is not

mfc bulletin/Aug 2011-Jan 2012 the best practice. Separation of these tasks is necessary. • It must be noted that countless small to medium private hospitals in India are operating without qualified nurses; this is partly due to HR shortages, and also due to unwillingness of owners to pay good salaries to nurses. Nurses prefer government jobs for reasons of better payment, security and safety. Lack of effective regulation of health facilities only adds to these factors. Graduate nurses get better jobs in corporate and overseas locations. • It is quite difficult to get qualified PG teachers for nursing institutes and this is a constraint on expanding training facilities. • It is possible to expand this HR sector with flexilearning strategies coupled with better assessment techniques. Without this, it is indeed difficult to expand the numbers.

Other Paramedics In most countries paramedics are those who respond to medical emergencies out in the field for the purpose of stabilizing the victim’s condition so s/he can be transported to medical facilities and function as an emergency medical technician (EMT). In India, this term is used loosely to define ALL allied health providers. In India the health delivery apparatus and its underlying ethos has been is entirely doctor-centric. The organization and methods of the delivery system have therefore developed along the physicians training, work and career path. Logically, the principle should be to allocate tasks to the workers with the lowest level of training and salary cost compatible with quality of care.

31 Council Act, 2003 both cover professionals who practice traditional systems of medicine such as Ayurveda, Unani or Homeopathy as paramedics. Precise statistics for AHP (Allied Health Professionals) Requirements in public systems are not available. Routine sources of information are fragmented and generally unreliable. According to the RHS bulletien2009, the country has a 59% shortage of Radiographers in the CHC’s, while a shortage 54% in case of Laboratory technicians at PHC and CHC. There is a shortage of 31 % in case of the Health worker (male) while it is 32% in case of Health assistant (female). As per WHO statistics, the Lab Tech per 1000 population in India is 0.02 where as in countries like USA, UK and China it varies from 0.16 to 2.15. These are figures based on in-position against sanctioned posts in the Public health system in the country and as such do not reflect the real picture. However as part of the UHC discussions, the following projections has been developed relevent to the year 2020: As per the International Standard Classification of Occupations (ISCO), Paramedical practitioners provide advisory, diagnostic, curative and preventive medical services for humans more limited in scope and complexity than those carried out by medical doctors. They work autonomously/with limited supervision of medical doctor. Broadly the AHPs are mandated to address the following needs: It may be useful to classify the levels of AHPs as (a) AH professionals (degree/PG) (b)AH provider (UG diploma) and (c) AH workers (Certificate Holder). This nomenclature will help fix salaries and career paths for AHPs.

The MOHFW is now working on the Paramedic initiative (called AHPs or Allied Health Practitioners) which has For too long Government of India did not even start the following components: (a) setting up the central apex action on the paramedic front. However some states body: National Institute of Allied Health Sciences or started paramedic bodies - Kerala, MP and HP. The Kerala NIAHS at Najafgarh to look after regulatory and paramedical council bill includes ECG Technician, EEC academic functions especially regard PG posts (b) Set Technician, EMC Technician, X-ray Technician, Medical up 8 regional institutes for AHS undertaking graduate Laboratory Technician or Ophthalmic Assistant as Para and PG programs (c) State training institutes will mainly Medical Technicians. The Madhya Pradesh Paramedical implement Grad and diploma courses. There are serious Council Act, 2003 and the Himachal Pradesh Paramedical issues regarding the academic administration of facult Table 4: Paramedic/Allied Health Professional (AHP) Requirements Category Required Available Additional Required Pharmacists (Allopathy) 1,36,869 20,967 1,15,902 Lab. Technician 1,36,869 12,904 1,23,965 Radiographer / DRA 37,681 1,867 35,814 O T Technician 46,563 NA 46,563 Ophthalmic Technician 66,478 NA 66,478 Physiotherapist 66,478 NA 66,478 Source: Dr. D. Thamma Rao, Public Health Foundation of India, New Delhi

32

mfc bulletin/Aug 2011-Jan 2012 Table 5: Categories for AHPs

Diagnostic

Cardiovascular technologists Medical and Clinical Laboratory Technicians Radiologic Technologists and Technicians Medical Services Dental assistants Emergency medical technicians and paramedics Medical assistants Non-direct care Dental laboratory technicians Medical appliance technicians Pharmacy technicians Rehabilitative Occupational therapists Speech-language therapists Respiratory therapits Community Related Community Based Rehabilitation Therapists Home based Care workers/Home Aides (ASHAS and AWWS??) will they come under UGC or NCHRH-Health universities? The resistance to open distance learning by AHP institutes smacks of elitism because it will keep thousands of working AHPs to suffer without any formal academic recognition.

Other Support staff The support-staff in clinical facilities, ambulance drivers, office assistants and data managers etc are necessary to improve the content and context of health care. They need good induction training and continuous support. This facility is currently available only for corporate hospitals. They have some hands-on-training, but will need better training in many areas to improve health care. And we also need to train new workers for the expanding health sector. The private Health sector generates a lot of employment in this, but their terms and working conditions are not favourable, many get payments below minimum wages. One way to improve this is certificate courses and better regulation of hospitals.

ASHA ASHA, the Accredited Social Health Activist of NRHM, is a recent addition to HHR (nearly 9 lakhs) on the backdrop of the 1978 CHW program that failed. As someone involved with the consultations about ASHA program, I get a feeling that GOI-MOHFW were wary of granting any worker-status to this ‘cadre’ and preferred the ‘activist’ nomenclature to avoid issues regarding use of medicines and salary implications. The Accreditation never came though some of them could enter the ANM cadres’ by local selection for ANM training. This leads us to the next question: who will be the authorized health

worker for India’s 6 lakh villages? If the 12 th FYP suggestion of having a subcentre in each panchayat/ 1000 population cluster with an ANMs is any clue, the ASHAmay be a disposable element in India’s HHR, a false step! The villages will have to wait to get a subcentre and an ANM for some time, but the ANM herself may not be able to deliver comprehensive services unless her role and tasks are redefined for comprehensive care. The ubiquitous quack and the semi-quacks therefore will hold the ground for some time to come.

Distribution of HHR Inter-State Variation Fig 4 represents the variation of doctors’ density across states based on Census 2001 estimates. In general it shows that Punjab has a high density (8-23 per 10000) of doctors, followed by J&K, Sikkim, Haryana, Maharashtra, Karnataka, AP, W.Bengal, Uttaranchal and Goa (range of 6-8 doctors per 10,000). The third group having 4-6 doctors per 10,000 includes MP, UP, Mizoram, TN and Kerala. The last group includes Orissa, Bihar, Jharkhand, Chhattisgarh, Rajasthan, Assam, Arunachal and surprisingly Gujarat. This picture nearly conforms to the number of medical colleges in each state except Gujarat. So also is the picture of nurses, which follows the doctors’ density and hence about 56% registered nurses and midwives are in the 5 southern states (Maharashtra, Karnataka, Kerala, Tamilnadu and AP). It is notable that Goa and NE states except Assam also have high density of nurses.

Uptake of HHR in Public: Private sectors

mfc bulletin/Aug 2011-Jan 2012 “Overall, the majority (70%) of health workers work in the private sector in both urban and rural areas. This pattern generally holds across health worker categories though there are some important exceptions.” (PHFI’s Technical Report on HHR 2008). The report further states that, “In contrast, around 50% of the nurses and midwifes in both urban and rural areas are employed by the public sector.” My study of Nashik district reports that out of 6,160 doctors of all types, barely 10% are in the public health sector. That gives a staggering 90% uptake by the private sector. This statistics includes unregistered doctors. This private: public sharing of doctors will vary from state to state. Also there is the factor of private practice by many government sector doctors that vitiates the statistics. The huge presence of doctors outside public sector poses a special problem when and if India will design its UHC. If the private doctors are left out of UHC, they will outsmart the very UHC by sheer weight of their numbers. On the other hand If UHC tries to include them by contracting-in arrangements, they are simply far too many (in some states) to be included and supported in UHC. This will also present selection problems.

33 public hospitals. Other doctors (mostly unqualified) abound in rural areas. Rural areas are not going to get qualified doctors except by some spillover, while the expanding urban areas are brimming with doctors. The ubiquitous small village as a unit of population is not going to get any doctor, even of the quack kind because of obvious economics of an average village if not its size per se. Therefore the essential village problem has to be solved with a trained paramedic with comprehensive equipment and in the panchayat framework. The ASHA has utterly belied our hopes on this account (or rather the planners have belied the hopes).

Some Legal issues In the context of HHR and using their capabilities, India has several legal issues to tackle. I am mentioning only some prominent ones here.

• The GP or family physician is not only losing the ground, but BAMS and Homeopaths are filling these positions (in lieu of MBBS doctors). Most of AYUSH GPs use modern medicines and going by existing acts, this is not tenable. It will be pragmatic to solve this problem by starting bridge courses in flexi mode followed by legal approval for limited use. Rural-Urban Till this is done, quality of care is in serious jeopardy. This problem is well known. As the PHFI technical report • The large number of other doctors in rural areas will states, “overall and across most health worker not vanish unless alternative services are made categories, typically 60 percent of the health workers are available. One way is to start paramedic programs present in urban areas. In contrast, only 28% of the (nurse practitioners or physician assistants) with country’s population is urban (Census of India, 2001).” legal license to use a short list of medicines. If we go by density of health workers per 10,000 • There are now turf battles between some specialties population, urban Health Worker density is four times and paramedics, especially between (a) as compared to rural areas. In our study of Nashik district, pathologists versus lab technicians, (b) the distribution of doctors is further evident, as post ophthalmologists and optometrists (c) doctors and graduate (PG) doctors are mostly in urban private sector, pharmacists. These need to be resolved in time and as also the dentists and Ayurveda GPs. clearly defined for regulation. In this study post graduate doctors are located in urban • The paramedic council is yet to appear at central private sector; nearly half the district population that is level, though some states have enacted legislations rural gets a minuscule of PG doctors and even less in its in this regard. There are issues regarding inclusion of some categories, the educational and Figure 5: The Urban Rural Divide of Doctors

34 institutional framework to support the paramedic systems. • The huge paramedic workforce in the private health sector is without legal training and may remain so unless the paramedic and nursing councils extend their helping hand through distance learning programs. However distance learning itself is abhorred by the medical and paramedic bodies.

HHR: Education, Training, CME Modern Medicine Medical Colleges India has roughly 270 medical schools, from which 28,158 doctors graduate every year. In all 52 private medical institutions have helped this rapid increase in medical education (Figure 4). In 1990, 33% of 135 medical schools were privately operated; nowadays, 57% are privately operated.11 The 5 southern states (Maharashtra, Karnataka, Kerala, Tamilnadu and AP) and Gujarat have together 35% of India’s population12 but have 172 (62%) modern medicine medical colleges, the rest of India has 105 (38%). Ayurveda and Homeopathy colleges may have a similar pattern. This variance leads to clustering and paucity of doctors in these states (except in Gujarat where there could be outmigration to Mumbai or other countries). The starting of All India Entrance Examination (NEET) has somewhat overcome this imbalance by way of nationalizing entry to UG and PG Medical Education, but states still have a large quota of 85% seats. The presence of private medical colleges has added a new twist to medical education in India, not so much because they are privately funded, but the very weakness of governance and regulation in India (even government colleges have several problems). Unlike engineering colleges, medical colleges require functioning hospitals with good occupancy ratio and rich clinical work exposure. This is often missing. The premium on PG seats (so also UG seats) is a huge problem, often touching Rs 2 crores for a radiology PG seat. NEET may be able to correct these issues by centralising the entry to all colleges. Many medical colleges cannot get enough teachers and cannot pay enough to hold teachers. Malpractices are rife about managing faculty positions, and there are websites that make open offers. MCI members get richer in this situation and scandals are quite possible in this situation. Dental education has similar problems and nursing and physiotherapy also has its share of the problems. Quality and ethics have taken a back seat in medical education. Since the doctors from government and private streams of medical education mingle eventually in the dominant private sector, there is no scope for difference in earnings, moral-ethical

mfc bulletin/Aug 2011-Jan 2012 practices or location preferences (rural-urban, etc). Hence there are three basic issues in medical education sector today: • Needy states should get enough medical colleges and • Quality medical education be developed as regards optimal content, relevance and ethics. • Issues about how to achieve AYUSH sector integration. MCI has relaxed the norms for medical education to suit opening of more medical colleges and increasing the number of UG-PG seats in colleges. There are some concessions for NE regions. The issue of BRMS education in states other than Assam, W.Bengal and CJ are still pending. The following points are important for reorganizing medical education for India: • Central assistance for opening medical colleges in needy states (and discouraging this in other states), so that all revenue divisions get modern medicine and Ayurveda medical colleges. The 12th FYP should help this cause. • Greater share of UG seats in NEET to backward states (but this is unlikely to happen). • Reworking of the syllabus of UG medical education to serve public sector institutes: PHCs/Rural Hospitals, etc. as also optimizing the curriculum from the existing heavy load that is often decided by choice of books rather than rational needs. It is welcome that textbooks by Indian authors are increasingly replacing Western textbooks, but there is a need for MCI to be officially approve this. • Fair share of PG seats (50%) to doctors having completed 5 years of rural/public services. The current PG selection process is loaded against candidates from health services. Integration of departments of medical education and health services will facilitate this process. • Integration of AYUSH elements in MCI stream as a compulsory subject (and vice versa) with formal evaluation. • Mandatory CME, formal test for credits and renewal of registration. • Integration of health services with medical education departments in all states, so that rural services are not at a permanent disadvantage. Segregation has damaged this cause. • Reservation of some UG seats (say 10%) for nursing professionals and BRMS. With a full HHR scenario and applying current salary rates, the share of various HHR categories is whon in Fig 6. The

mfc bulletin/Aug 2011-Jan 2012

35

HHR Institutions & Availability (NHP 2008 NHSRC HRH Division) Institute

Number

1. Medical Colleges

289

Annual uptake 32,815

2. Dental Colleges

282

22,650

Availability of HHR MOs 2,15,199 Spec. 1,52,437 Dentists 14,499

HHR: pop ratio

3. AYUSH Institutions

477

27,265

Drs. 70,202

4. Nursing Schools

1,620

62,647

Nurses 6,90,564

5. ANM Schools

329

6,502

ANMs 5,24,283

6. Health Worker Male 7. Pharmacy - Degree

102

5,334

2,28,946

1: 264 population – India; 1: 100200 - Europe 1,42,655 - 2nd ANM; 43,966 New SHCs No Registration from Council

241

13,400

1,25,915

India 1 : 1,840;

Phramacy: Diploma

523

31,543

1:1,667 Population-India 1:35 Lakh – Bihar; 1:18,812 Pondicherry 1:798 Population - India

Europe 1 : 2,300

8. Lab Technician

97

2,193

1,44,990

No Registration Council

9. Radiographers

33

410

36,628

No Registration Council

10. Ophthalmic Tech.

41

426

46547

No Registration Council

Total

4034

2,05,185

21,04,650

Courtesy: DR Thamma Rao & NHSRC

HHR: Cost Implications A simple calculation for the national annual salary bill for the HHR at reasonable rates compatible with 6th pay commission is given below: Table 6: HHR Costs Category (pop/HW)

Need for district of 30 lakh popln. 50

Country need 20000

Monthly salary in Rs 150000

12 months (in crore Rs) 3600

1

SSPecialists(60000)

2

Specialists(5085)

590

236000

80000

22656

3

MM-MO(6250)

480

192000

50000

11520

4

Ay MO/BRMS(3659)

820

328000

30000

11808

5

Dentists(15000)

200

80000

50000

4800

6

Admin MOs(1.5L)

20

8000

80000

768

All docs(1422)

2160

864000

0

0

7

Nurses+MidWives (929)

3230

1292000

30000

46512

8

Other Paramedics(1852)

1620

648000

25000

19440

9

Ward Asst etc(1115)

2690

1076000

12000

15494

10

Office Support staff(2500)

1200

480000

20000

11520

11

ASHAs(833)

1440000

3000

5184

Total annual salary

5800000

perks/travel etc Gross Bill annual A

India GDP2010

B

Ann HHR salary as % GDP

All India

Total

153302

Cr

15330 0

168633

7.50 lakh crore Rs

7500000 2.25

36

mfc bulletin/Aug 2011-Jan 2012

•

•

•

share of doctors is about 32% (add some perks etc). Nurses, paramedics, assistants etc come in that sequence.

•

HHR Management in India: Some Major Issues Given the variation across states in HHR management, only some broad guidelines are possible here. In general, working in the public sector should offer a benefit of better working conditions and reasonable salary and perks and protecting long terms interests of HR. I feel that there are several HHR issues to note in this regard, starting from lowly subcentres to medical colleges. The major issues in HHR are: Recruitment, transfers and promotions, salaries, working conditions, motivation, Compliance with rules and regulations, grievance redressal, etc. Profiling an all-India picture of these aspects of HHR, across various categories in the public sector involves a huge exercise. Apart from PHFI’s report on some states, there is no source on this. Broadly there are 5 sub-issues (apart from initial formal training) involved in HHR management: (a) selection and appointment, transfers, etc (b) In-service training and motivation (c) pays and perks (d) working conditions (e) administration and monitoring. The points that I can make from various interactions in some states include: • In Maharashtra, about 25% of Medical Officers at PHCs are from AYUSH sector, most of them end up in tribal or backward PHCs. • The contractual doctors and nurses get around less than 50% of salaries as regular staff get; the working contracts are far too unsatisfactory. • In many states health center doctors are asking for arrears of 6th Pay Commission, and in some states it is even difficult to give 6th Pay Commission salaries to health staff. • Bribes for appointment, transfers are common

•

practice in many states, despite the fact that doctors are unwilling to work in public sector, especially in the rural parts. Most doctors and nurses would like to have a transfer to taluka towns once their children grow to the schooling age. Many often shift home to the town and start commuting. ANMs and NMs have sparse career options and work on the same post for a lifetime. They must be provided upward mobility; career path and we should also pick up the right candidates from ASHAs. 50% of the MPW posts are not filled and in some states have a worse record. MPWs have played and will play crucial role in disease control programs. Therefore they deserve career paths and crucial role in health care. PG doctors working in RH get 30-40% less salaries compared to that in a medical college, with much adverse working conditions and poor career options. This relative deprivation robs them of any motivation for public service. Home visits are part of the ANM’s life and this model of work has sapped the strength of ANM. Things will improve only if every village gets its own subcentre.

How to bring the Private Sector HHR into ‘Public Domain/UHC’? This is the most tedious issue of UHC—how to bring the right mix of HHR in right places in ‘public’ sector for UHC. Since 70-90% HHR and 70% services are in the private domain, a major debate among health planners and the private health sector is expected and there is no easy solution. There are 4 broad choices: (a) ‘Nationalizing the health sector—the hard option and leaving little/no room for private operators to survive but join the public sector, (b) Expanding the public sector infrastructure 2-3 times, and ‘democratically’ forcing the private sector HHR into this new public sector by making the private sector unviable and unattractive - the competitive approach, (c) Contracting the private HHR for public services through PPP approach (d) Walking a long path for bringing both the existing sectors - public and private -under a SHI domain like in Germany with contributory payment from Government, families, etc. the Social Health Insurance (SHI) approach. How the medical doctors, especially the specialists and SSPs will perceive and respond on each option will decide the course of UHC in India. Also the political economy of the day has to throw up a suitable model for UHC with scope for federal adjustments. On the whole, the HHR

mfc bulletin/Aug 2011-Jan 2012 dense states may prefer a course that protects their doctors’ interests/incomes as state-payments could make a huge demand. On the other hand the States having poor HHR density, with central assistance may choose a UHC band that is more state run provided they get their HHR. Bringing the huge private sector HHR in UHC is going to be the real battleground. The state of governance in most states and health departments is miserable, and it is necessary to bring on substantial reforms before we can hope to lure/force the private sector HHR into a ‘public sector’ of some kind. The governance front is also no small challenge, but this is where we can and need to begin in the earnest.

37 HHR, locate gaps and clustering, also undertake utilization studies to find out service adequacy, gaps and needs. Prepare a national and state level registry of in-service HHR. Start Umbrella Medical colleges in each district locality, with modern medicine,AYUSH, and Nursing, Paramedic institutes in one campus to being about functional and systemic integration in training and in health care delivery. Integrate the Medical Education Department and Public Health Department in each state and affix responsibility of HHR and management in the district to each district medical college. It may be useful to integrate the local body medical services also under the same administrative umbrella; however this seems unlikely at this stage given the vested interest of local body HHR. Ensuring adequate nursing and other paramedics in each health facility, making online info available for all registered facilities. Publish guidelines for salaries of HHR in private sector, educate HHR about minimum wages for skilled and semi-skilled workers, trainees, etc. Also publish a code of conduct for various HHR categories, coupled with an appellate tribunal for HHR in both private and public sector. Special packages and incentives for HHR working in rural/underserved areas., some states can bridge their HHR gap with from other surplus states with mutually agreed special packages. Compulsory CME/CHE made available online and/ or face to face with credit accumulation.

•

•

National Council for HRH Draft Bill, 2009 This proposed bill aims to bring together all streams and levels of medical, nursing and paramedic education and training under one system and will authorize and regulate courses, institutes and have power over all universities engaged in medical education. The NCHRH is a welcome step since it will end disparities and facilitate coordination among various levels and streams. A press release in October 2011 says it has been cleared by the cabinet and will be tabled in Lok Sabha soon. Under this secretariat, seven departments, each for separate categories like Medicine, Pharmacy, Nursing, Dentistry, Rehabilitation & Physiotherapy, Public Health & Hospital Management and Allied Health Sciences, will be established and headed by a director for regulatory purposes.

Recommendations Since HHR is a precondition of UHC, I propose the following reforms in HHR: concepts, strategies, plan, etc. • Train paramedics/Nurse Practitioners to take over forthcoming panchayat health facility (1000 population), may be ASHAs can be retrained to take this with due training, accreditation and authorization: A million such paramedics are necessary. We must steer clear of the prevalent doctorcentric/dominated model. • BRMS orAYUSH doctor to be made available at each subcenter (5000 population) – one who is capable of using 40-60 modern medicine medicines and skills to handle epidemiologically relevant and common health problems. • AYUSH doctors to be made available in all levels of care under the same roof, so that there is no discrimination and bias about pathy choice and HHR streams. • Make available optional integration programs and legal registration for AYUSH doctors to strengthen primary care in public and private sector. • Prepare district level databases to size up and map

•

•

•

•

•

Endnotes 1

KD Rao, Bhatnagar Ashwini, HRH Technical Report #1, PHFI 2008, quoted from NSSO, p.22.

2

Thamma Rao, Human Resources for Health, NHSRC-HR division, New Delhi.

3

12th FYP Approach Paper: Planning Commission: p. 117.

4

High Level Expert Group (HLEG) on Universal Health Coverage – Progress Report - October 2010-January 2011 (unpublished) 5

Indian Nursing Council website: accessed11-9-2011

6

HLEG: Final Report 2011: Section on HRH (To be published, courtesy mfc sources) 7 High-end physician migration from India. Kaushik M, Jaiswal A, Shah N, Mahal A., Bull World Health Organ. 2008 Jan; 86(1):40-5. 8

Cross reference from article by Mohan Rao et al cited below in (11)

9 http://articles.timesofindia.indiatimes.com/2011-04-17/delhi/ 29427730_1_quacks-mci-board-traditional-medicine 10

Bull World Health Organ 2010;88:327–328 /doi:10.2471/ BLT.10.020510 11 Human resources for health in India. Prof Mohan Rao PhD, Dr Krishna D Rao PhD, AK Shiva Kumar PhD, Mirai Chatterjee MPH, Thiagarajan Sundararaman MD. The Lancet- 12 February 2011 ( Vol. 377, Issue 9765, Pp. 587-598 )

38

mfc bulletin/Aug 2011-Jan 2012

Food and Nutrition in India: A Public Health Perspective A Response to the paper by Deaton and Dreze, EPW, Feb 14, 2009 - C. Sathyamala1 Several studies have thrown up the trend of declining calorie intakes among Indian populations. Deaton and Dreze had triedto explain this phenomena in an EPW paper (Feb 14, 2009). We publish below Sathyamala's comments on the paper and a response by Dreaze and Deaton. - Editor, mfc bulletin In India, the overall nutritional status of the population which has shown little improvement in the last decade, with, as revealed in the National Family Health Survey 3, nearly half the children below the age of three undernourished, is a matter of grave public health concern. The paper by Deaton and Dreze (“Food and Nutrition in India: Facts and Interpretations”, EPW, Feb 14, 2009), is a recent attempt to explain the seemingly inexplicable findings, that this is happening at a time when the Indian economy is growing at “historically unprecedented rates” (p 42). Using national level secondary data, the authors try to find answers to a series of “puzzles” they identify to finally arrive at a “plausible hypothesis” which I would argue is questionable. While they comment on both the rural and urban situation, my response will focus on the rural. According to the authors, while there has been substantial reduction in poverty since the early 1980s, the real per capita calorie intake is declining (their emphasis; p 42), as also the intake of many nutrients with the exception of fat, whose consumption is “unambiguously” increasing. Let us examine the decline in food consumption without linking it with the alleged reduction in poverty. The data presented in their paper show that in rural areas, between 1983 and 2004-05, the mean per capita decline in calories from cereals was 295 calories, equivalent to approximately 74 gms of cereals1 (their Table 3, p44), and although fat consumption during this period increased only marginally by 8.3 gms (their Table 1, p43), it was not sufficient to offset the calorie deficit as it accounted for approximately 75 calories only, with the result that on an average, rural households were consuming around 200 calories per capita less in 200405 than they did in 1983 and approximately 350 calories less than the minimum requirement of 2400. On the other hand, the per capita protein intake, which in a cereal based diet largely comes from the cereals, in keeping with the fall in cereal intake, had come down by approximately 8gms. Thus the fall in protein intake was secondary to the fall in cereal intake. However, in 2004-05, at 56 gms, the protein content in the diet was adequate for an adult 1

<sathyamfc@yahoo.co.in>

with 56 kg body weight; so also was fat, at 35.4 gms, more than sufficient to meet daily requirements (Table 1 in my response). If people were eating less on an average, did it mean that they were spending less on food? During this period, as per the authors’ calculations, mean household expenditure on food did not come down and between 1987-88 and 2004-05, the rural households ended up spending per capita Rs 9.6 in excess (an increase of 12.1%) for consumption of 1,000 calories (their Table 2, p 44) without increasing their intake. The puzzle then is, where did this ‘extra’ money go since, according to the authors, the real value of per capita food expenditure remained unchanged2? This paradox, according to the authors, was because the rural households reduced their expenditure on ‘cheap’ calories such as cereals and shifted to the purchase of “more expensive calories such as edible oils, milk products and meat” (p44), thereby leading to a deficit in calorie intake despite spending the same amount. This statement is speculative as the authors provide no evidence to substantiate it, and moreover, it contradicts the figures presented in their paper (Table 1, p43), which show a marginal fall in per capita protein intake and only a marginal increase in fat. In fact, later in their paper, the authors, citing the NNMB data, admit that even pulse consumption (a relatively ‘cheaper’ protein food) has declined during this period (p 48). And strangely enough, this ‘switch’ to more ‘expensive’ food appears to be confined to only rural areas as the cost of 1,000 calories in urban areas remain more or less the same during this period. Even if this were to be true, as the authors aver, that the real value of per capita food expenditure has remained unchanged, it need not necessarily mean that the rural poor are moving away from cereal based diets; the shift could be, as the authors admit, from cheaper cereals (coarse grains) to more expensive cereals (wheat and rice) (Figure 7 ; p 51), linked to availability, association with social status and changed food ‘preferences’, through forced consumption by the public distribution system and the ICDS programme3. That such a shift has serious implications for nutrient availability can be seen from Table 1 which gives the nutritive value of some of the cereals. Weight for weight, the nutrient content of the millets can match that of rice and wheat, give a run for their money, and in some cases, for instance calcium in ragi or iron in bajra, win the nutrition race!

mfc bulletin/Aug 2011-Jan 2012

39

Nutrients per 100gm of dry weight

Rice milled

Table 1: Nutritive Value of Indian Cereals Wheat ragi bajra jowar Kodo Kutki flour (varagu) (samai)

Energy (cals) Protein (gm)

345 6.8

341 12.1

328 7.3

361 11.6

349 10.4

309 8.3

341 7.7

Fat (gm)

0.5

1.7

1.3

5

1.9

1.4

4.7

Mineral (gm) carbohydrate Calcium (mg)

0.6 78.2 10

2.7 69.4 48

2.7 72.0 344

2.3 67.5 42

1.6 72.6 25

2.6 65.9 27

1.5 67 17

Iron(mg)

0.7

4.9

3.9

8

4.1

0.5

9.3

Daily req* male/female/ pregnant/ lactating 60 gm/50 gm /65 gm/ 75 gm 20gm/20 gm /30gm/ 45 gm

400mg/400 mg/1000mg/ 1000mg 28mg/30 mg/ 38 mg/30 mg**

* Recommended daily requirement for a 60kg male and 50kg female irrespective of work intensity Source: Nutritive Value of Indian Foods, National Institute of Nutrition, Hyderabad (2007), pp.47, 98. The policy implication is clear that, both on nutritional grounds and on the relative ease of cultivation, serious attempts should be made to increase the production of coarse grains and a beginning could be made by making it part of the PDS. While thus far, the discussion was on mean intakes, looking at Calorie Engle Curves, the authors state that the trend of falling per capita consumption is at all levels of per capita household expenditure, and the percentage decline is more or less the same across all expenditure levels. Their “puzzle” here is, while it is not hard for them to understand why people at the top quintile of expenditure should consume less over time, why should those with the lowest per capita expenditure (the poorer households) consume less over time? The data they present tells a different story when seen not as percentage decline but in absolute number of available calories. Table 2 in my response presents the recomputed data from their Table 7 (p 47). Table 2 shows that for the bottom quartile, there was a minimal change in both calorie and cereal calorie intake (an increase of 44 calories if 1983 is taken as the base year or a decrease of 59 calories if 1987-88 is taken as the base year). This group comprises of 25 percent of our population, who get 83% of their calories from cereals, are eating so minimally to amount to a starvation diet (approximately 1,600 calories, or 66% of minimum requirement), that they can decrease their intake no further. In the second quartile, while there is an overall drop in calories by 107 there is an increase of 101 in cereal

calories (25 gms). This means that in this group, contrary to what the authors aver, there is a shift to ‘cheaper’ calories from more ‘expensive’ calories (such as oil and milk), with the freed cash used for purchasing more cereals, which are now more ‘expensive’ than before. In the third quartile group (whose calorie intake in 1983 was at the borderline of the minimum requirement of 2,400 cals), there is a sharp fall in overall per capita consumption by 185 cals, but a sharper fall in cereal calories by 308. This group by 2004-05 shifted below the official poverty line with a per capita consumption of 2143 calories, with a noncereal food supplying the 120 calories. This is the only expenditure quartile, whose intake ‘behaviour’ shows a minimal shift, if at all, to a non-cereal food supplying calories in their diet. The fourth quartile, which was well above the minimum requirement at 3,044 calories in 1983, shows a sharp fall of 523 calories, almost the entire amount (503 calories) being accounted for by a fall in cereal calories. This seems a little strange (and data suspect), particularly in the context of the increasing prevalence of obesity in the upper quartile population. Commenting on this, the authors state that, “for the quartiles, even the bottom quartile, the trend is either confused or declining” (p 47). The confusion is because these data refuse to fit the authors’ notion of a shift to ‘expensive’ calories. We now come to the core of the paper, the authors’ “plausible hypothesis”, that while, “real incomes and real wages, have increased (leading to some nutrition improvement4), there has been an offsetting reduction

40

mfc bulletin/Aug 2011-Jan 2012

in calorie requirements, due to declining levels of physical activity and possibly also due to various improvements in health environment. The net effect has been a slow reduction in per capita calorie consumption” (pp 42-43). How true is this? First, the authors’ assumptions regarding “epidemiological” environment having improved to such an extent that there is a substantial saving of calories, is debatable as the quantum of morbidity in rural areas, particularly diarrhoeal related diseases, has not come down appreciably despite the increase in number of hand pumps etc, (see Figure 1, my response). Secondly, if their argument were to be applied to the data presented (Table 2 , my response), it would imply that the poor (up to the third expenditure quartile, ie., 75% of the population), whose intake was already lower than the officially allowed minimum calories, in the last few years (since 1987-88 to be precise), having realised that their nutritional requirement had reduced even lower because of ‘energy saving’ devices such as the bicycle and motorcycle5 (the authors failed to add energy saved because of increasing unemployment ), and better ‘epidemiological’ environment, have voluntarily decided to curtail their ‘calorie’ intake. There are two issues here, calorie requirement going down as a physiological need and changing food consumption behaviour as an adaptation to changed circumstances. If, as per their assertion, the requirement has indeed fallen, then it should translate into a ‘healthy’ population on a reduced diet that was to begin with less than minimal. How does this show up in terms of nutritional status? The authors’, after several pages of elaboration on the merits and demerits of the various anthropometric measures and sources of data, conclude that undernutrition is widely prevalent in the country among men, women and children. In children, though a sharp

fall was seen in the proportion with severe undernutrition (weight for age below 3SD), much of this decline took place before 1996-97, and since then, there has been only a marginal change6. The proportion of children with moderate undernutrition (weight for age below 2SD) that showed some decline in the period up to 1996-97, now remains at 55% (NNMB data) and 46% (NFHS-III) 7. Hence, whichever source of data one has a predilection to8, there is no escaping the harsh reality, that chronic food insufficiency, as reflected in childhood undernutrition, is a reality for almost or more than half the population9. There is thus a complete disjuncture between the reality of undernutrition in the country and the authors’ “plausible hypothesis” of reduced calorie requirement. The authors’ assertion that the poor are voluntarily curtailing their food intake is because it is happening in the context of increased household expenditure which is taken to signify increased household income (emphasis mine), an assumption that has been questioned (Saith 2005). If people are spending more, and if money is not being spent on food, it appears that money is now being spent increasingly on other competing needs, given that according to the authors food costs have not gone up. What needs could these be that they are so much more pressing than food that the poor are ‘voluntarily’ starving themselves further to meet them. Could one such need be increasing medical care, which contrary to the authors’ claim is a consequence of both the deterioration of the ‘epidemiological’ environment and rising medical care costs due to privatisation, now the second major cause of rural indebtedness? The authors assume that if people are not eating adequately, then it should manifest as rates of ‘hunger’

Table 2: Changes in Calories and Cereal Consumption Across Expenditure Quartiles (data recomputed from the authors’ Table 7)

per capita calories consumed in 1983 Change in per capita calories (1983 to 2004-05) per capita cereal calories consumed in 1983 % cereal calories in total (1983) Change in per capita cereal calories (1983 to 2004-05)

Quartiles of Per Capita Expenditure Bottom Second Third quartile quartile quartile 1,580 2007 2328

Top quartile 3044

Plus 44 *

Minus107

Minus 185

Minus 523

1,309

1,589

1,738

1,974

82.8

79.2

74.7

64.8

Minus 50

Plus 101

Minus 308

Minus 503

* This will be minus 59 cals if 1987-88 is taken as the base year which may be a better way of looking at this as the calorie consumption has remained more or less the same for this sub-group

mfc bulletin/Aug 2011-Jan 2012

41

in surveys. It is no mystery why the data on self-reported ‘hunger’ does not correlate well with calorie shortfall. ‘Hunger’ is a physiological sensation regulated by a not fully understood complex feedback mechanism involving the neuro-endocrine system, with learned, cognitive components10. Poor people with chronic deficient food intake adopt several ways of ‘killing’ hunger and appetite as a way of survival11. It is perhaps time the use of the term ‘hunger’ is discontinued as a euphemism for chronic insufficiency of food. Surveys on self-reported ‘hunger’ thus end up with findings that, for instance in 2004-05, the rates in West Bengal (11.7%), Orissa (5.9%), Assam (5.5%), Bihar (3.2%) were above the national average (2.5%) followed by Chattisgarh (2.5%) and Kerala at (2.5%) at national levels which then allows the authors to draw a spurious conclusion that the “four states with high levels of hunger are all located in the ‘rice belt’ of eastern India” (p 45)! Apart from the fact that Tamil Nadu, though in eastern India and part of the ‘rice belt’ does not fit this picture (‘hunger’ levels 0.1%), and since the other ‘rice eating’ states are in the western part and register almost a negligible ‘hunger’ rates as well, it is not clear which is the determining factor, living in eastern India or ‘rice eating’12. Overall, one of the glaring shortcomings of the paper is the near absence of disaggregated data and the presence of sweeping generalisations such as the Indian diet being “frugal” (p 61).

The falling per capita cereal consumption in the country has been commented upon by other scholars who view it as a consequence of growing impoverishment in the country (Patnaik 2004). Deaton and Dreze, on the other hand, argue that this has little to do with impoverishment and more to do with self monitoring of the population (across economic categories), who regulate their calorie intake according to their ‘accurately’ perceived physiological requirement. The authors are constrained to put forward this hypothesis because of their compulsion to explain it in the context of high growth and, in their assessment, a decline in the levels of poverty in this period of growth. This is surprising considering how one of them had written unequivocally, just two years ago, that, “economic growth is not a dependable means of achieving rapid improvements in child development” (Dreze et al 2007). A chronically starving population is highly susceptible to disease. It is also this population that ekes out a living by selling its labour in occupations involving heavy expenditure of energy. While anthropometric data is only an indicator of such deprivation, from a public health point of view, in India today, the inadequacy of food intake is the single most important risk factor impacting the morbidity and mortality profile of the country. That the population under risk is seventy five percent (and rising), indicates an epidemic of unimaginable proportion which would not have been allowed for any other ‘disease’. In

Acute Diarrhoeal Diseases Cases 1991-2008 12000000

10000000

Cases

8000000

6000000

4000000

2000000

al

)

07

io n

20 08

Year

(p

ro vis

20

05 20 06

20

02

01

03 20 04

20

20

20

98

99 20 00

19

19

95

96 19 97

19

19

92

93 19 94

19

19

19 91

0

Data compiled from Central Bureau of Health Intelligence (GOI): National Health Profile 2006, p. 50; National Health Profile 2007, p. 51; National Health Profile 2008, p. 51.

Figure 1

42

mfc bulletin/Aug 2011-Jan 2012

this context the unfounded ‘hypothesis’ put forward by Deaton and Dreze would in effect relieve the state of its obligation towards meeting its citizens’ right to food and place the onus on the victims themselves. References Dreze J, Khera R, and Narayanan (2007): “Early Childhood in India: Facing the Facts”, Indian Journal of Human Development; 1(27), 377-388. Patnaik U (2004). “The Republic of Hunger”, Social Scientist 32(9-10), 9-35. Saith A (2005). “Poverty Lines versus the Poor: Method versus Meaning,” Economic and Political Weekly, Oct 20, 46014610.

Endnotes 1

Calories from cereal @ 4 calories per gm, and that from fat @ 9 calories per gm;

2

Not being an economist, I am not competent to comment on the assumptions that have gone into the authors’ calculations.

3

To this, one must add the observation that a ‘coarse’ grain such as ragi which was the staple diet of the agricultural labour in Tamil Nadu few decades ago, is now being reinvented as ‘special’ foods sold in fancy packets, for instance in Delhi, at Rs 100/- per Kg. 4 The authors’ “some nutrition improvement”, refers to the marginal increase in fat consumption as otherwise the data shows a deterioration of nutritional levels. 5

“Consider”, they say, “for instance, the fact that household ownership of a bicycle or motorcycle is a positive predictor of per capita expenditure and negative predictors of cereal consumption. This could simply reflect that people who own bicycles do not use as many calories in walking to work or to school, rather than the fact that ownership of bicycle indicates higher income and through higher income, lower cereal consumption” p55.

6 There is an attempt in their paper to obfuscate this fact as percentage decline has been presented from 1975-79 to 200405. If the period is split into 1975-79 to 1996-97 and 1996-97 to 2004-05, it becomes clear that there has been only minimal change in nutritional status in the latter period. 7 This was perhaps the impact of the several nutritional interventions for children, particularly the ICDS programme. The data indicates that such programmes are acting as ‘supplementary’ programmes and not as ‘complementary’ programmes. There is also a need to explore whether the emphasis on schooling and the consequent increase in schooling, particularly of girls, the care takers of younger siblings, is leading to their neglect. This is not to say that girls should stay back home to take care of their siblings but how can this need be addressed when such care may not be available. 8 The authors, with a less than adequate appreciation of the rigour that goes into collection of NNMB data, have dismissed it in favour of the NFHS or DLHS as being based on samples as against total population and only covering nine states in the country. In fact, where nutritional data is concerned, the surveys by NNMB are far superior and in fact should be expanded to cover the whole of India. 9

The authors’ discussion on stunting and wasting shows a lack of understanding of these measures, but a discussion on this here would divert attention from my central critique. 10

http://www.csun.edu/~vcpsy00h/students/hunger.htm accessed 21.4.09

11

Use of tobacco (chewing, smoking), adding water to increase food bulk, ‘ritual’ fasting, are just a few example. There is also a question of self respect which prevents people from admitting to ‘hunger’. 12 Could it be that rice eating when located in eastern India leads people to acknowledge more openly to their ‘hunger’ status?

Response to Sathyamala - Jean Dreze and A. Deaton1

In an earlier paper, we discussed the puzzling fact that calorie intake has declined in India in the last few decades, mainly but not exclusively among better-off sections of the population. We examined various interpretations of this decline, including the possibility that calorie requirements might have fallen (due, for instance, to improvements in the epidemiological environment and reductions in activity levels). We emphasized that this hypothesis (“declining requirements”) remains speculative and requires further corroboration. Dr. Sathyamala evidently resents this hypothesis, but she provides no evidence against it. On the contrary, the 1

< jaandaraz@gmail.com>

data she presents on the number of “diarrhoeal diseases cases” (Figure 1 in her paper) actually reinforces the plausibility of this hypothesis. Indeed, if the annual figures are divided by population and considered in per capita terms, as they should for our purposes, then it is evident that the incidence of diarrhoea has steadily declined in the last two decades – and this is entirely consistent with what we wrote about a possible improvement in the epidemiological environment. We thank Dr. Sathyamala for drawing our attention to this useful information. Aside from this, Dr. Sathyamala presents a long and confusing critique of the observation that, in rural areas,

mfc bulletin/Aug 2011-Jan 2012 food consumption patterns have shifted (to some extent) towards more expensive calories. She claims that we have not substantiated that observation. This is incorrect. The shift towards more expensive calories is evident from the fact that (1) calorie intake has declined, and (2) per-capita food expenditure (in real terms) has not declined. Sathyamala does not dispute these two points. She agrees with the first, and gracefully acknowledges that she is “not competent” to dispute the second. But if these two facts are correct, then it automatically and logically follows that there has been a shift towards more expensive calories – there is no need to “substantiate” that independently. Further, we actually have a good idea of how it happened, from disaggregated consumption data: during the last few decades, there has been a sharp decline in the consumption of so-called “coarse cereals”, the cheapest source of calories, and some increase in the consumption of more expensive non-cereal calories. We share Sathyamala’s concern about the possible nutritional implications of the decline of “coarse cereals” consumption, and discussed them in our paper. We are at a loss to understand why any further “substantiation” of the shift towards more expensive calories was needed. The rest of Sathyamala’s paper consists mainly of attributing to us points that we have not made or views that we do not hold. A few examples should suffice: 1. The author refers to our alleged “assertion that the poor are voluntarily curtailing their food intake” and even “voluntarily starving themselves”. We have made no such assertion at all. Our paper did not make use of the word “voluntary” or any equivalent. Nothing we wrote would justify such an interpretation. In fact, we discussed and rejected an argument developed by Pronab Sen that could be interpreted as an attempt to substantiate the “voluntariness” of the recent decline in calorie intake. 2. Sathyamala writes “the authors assume that if people are eating adequately, then it should manifest as rates of ‘hunger’ in surveys”. We did not make any such assumption at all. On the contrary, we discussed the limitations of survey data on self-reported hunger, including their possible unreliability and uncertain interpretation. None of our conclusions depend on these data. 3. According to Sathyamala, we have argued that the decline of per-capita cereal consumption in India reflects “self monitoring of the population… who regulate their calorie intake according to their accurately perceived physiological requirement”. This, again, is quite misleading. We did not invoke any notions such as “self-monitoring”, “regulation”

43 of calorie intake or “accurate perception” of physiological requirements. 4. This chain of misunderstandings and misinterpretations leads, in the concluding paragraph, to a startling and wholly gratuitous hint that our work’s hidden agenda might be to “relieve the state of its obligations towards meeting its citizens’ right to food and place the onus on the victims themselves”. The credibility of this suggestion is evident, we hope, from the fact that one of us wrote dozens of articles in the last ten years defending various aspects of the right to food and highlighting the state’s failure to meet its constitutional obligations in this regard. In our earlier work (including not only the paper discussed by Sathyamala but also the subsequent exchange with Utsa Patnaik in Economic and Political Weekly1), we went out of our way to clarify that the “declining requirements hypothesis” does not detract from the fact that there are massive calorie deficiencies in India. It may seem odd that people who don’t meet their calorie requirements in the first place might reduce their calorie intake further (at a given level of real per-capita expenditure) when the requirements decline. But this is, in fact, perfectly possible. Poor people are hemmed in on all sides – they have too little money to eat well and too little to spare for clothing, shelter, health care, their children’s education and other essential needs. In such circumstances, they may not be able to afford to keep calorie intake unchanged even when requirements decline (again, keeping per-capita expenditure constant). More likely, the little additional flexibility created by declining requirements would be used partly to reduce the calorie deficit (that is, the gap between requirement and intake) but partly also to diversify food intake and meet other urgent needs. Finally, it is important to remember that the bulk of the calorie decline has taken place among better-off households. For instance, in rural areas, more than 90 per cent of the decline in aggregate calorie intake between 1983 and 2004-5 occurred among households from the top and second quartiles (that is, the top half) of the population in terms of per-capita expenditure, and there was no decline of calorie intake in the bottom quartile – see Table 7 in our earlier paper. Seen in this light, the “declining requirements” hypothesis is less counter-intuitive than it may seem. 1

See Patnaik, Utsa (2010): “A Critical Look at Some Propositions on Consumption and Poverty,“ Economic & Political Weekly, 6 February. And the authors’ response “Nutrition, Poverty and Calorie Fundamentalism: Response to Utsa Patnaik,” Economic & Political Weekly, April 3, 2010.

44

mfc bulletin/Aug 2011-Jan 2012

To be the Rainmakers or not to be? That is the question. - Dhruv Mankad

Hi, I am back again on the armchair – on the bulletin’s armchair. Last night on one of the TV channel, I saw The Rainmakers (1997), the film version of John Grisham’s1 best seller novel. Normally, at home I nap when I am in the armchair but – this one kept me wide awake. No, no, not because it was a thriller or a court drama or because Matt Demon was there. It was because, what was on screen in 1997 was an American reality, then. What I saw in 2011 on small screen was a glimpse of what could be an Indian reality in 2026, worse, even earlier. This is just to recollect the story for those who had read it and a plot summary for those who may like to read or watch it! The “rainmaker” comes from American Indian culture. The job of the rainmaker was to carry out traditional rituals to make the rain fall. In the same way, the young lawyer follows the traditional ways of the American courtroom to make money fall into the hands of his client. Plot Summary Judy Baylor (Matt Demon) has nearly finished law school but hasn’t yet passed the bar examination. He secures a position with a Memphis law firm, which he loses when the firm is bought out by another larger firm. A poor couple, Dot and Buddy Black, ask for his help. Their son, Donny Ray, has leukaemia and needs a bone marrow transplant, but their health insurance company won’t pay for the operation. Rudy thinks they have a very strong case against the company, but he has no job and no money and isn’t licensed as a lawyer. After struggling for jobs with lawyers who takes away his cases but not him, Rudy starts his career with ambulance chasing. Here, he files the Black suit. Having passed the bar examination, Rudy prepares to fight the team of lawyers employed by Great Benefit Insurance. It is a daunting task. He has never argued a case before a judge and jury—but he now finds himself up against a group of experienced and ruthless lawyers from a large firm, headed by Leo F. Drummond (Jon Voigt). During the case, Rudy then set up their own firm of lawyers. The newly-appointed judge, an ex-civil rights attorney gives Rudy a ray of hope as he shows more understanding toward victims. Before the trial commences, the Blacks’ son dies but not before giving a video deposition in the front yard of his home. The case goes to trial, where Drummond preys on Rudy’s inexperience. A former employee of Great Benefit Jackie Lemanczyk testifies that the scheme generated an extra $40 million in revenue for the company. But Drummond gets the vital testimony of Rudy’s key witness, stricken from the record. Nevertheless, thanks to Rudy’s single-minded determination and skillful cross-examination of Great Benefit’s unctuous president, Wilfred Keeley, Rudy uncovers a scheme that Great Benefit ran throughout 1991 which was to deny every insurance claim submitted, regardless of the validity of the claim. Rudy collects evidence and builds up a picture of how Great Benefit makes huge profits by cheating the people who have their insurance policies with them. Great Benefit was playing on the odds that the insured would not consult an attorney (which Dot Black didn’t do until it was too late for Donny Ray). Finally, the jury finds for the plaintiff.Rudy wins the case.

It is a great triumph for Rudy and Deck, at least until the insurance company quickly declares itself bankrupt, thus allowing it to avoid paying fifty million dollars in punitive damages. There is no payout for the grieving parents and no fee for Rudy.Although Dot Black was never concerned with the money from the trial. In fact, she testified that if awarded any money from Great Benefit, she would donate all of it to the American Leukemia Society. Can it happen here? I think the question should have been – is it happening here? The three – one historical case, two recent reports points to a loud and clear YES. The Union Carbide v/s Bhopal Gas Victims case where the company flouted its own rules of maintaining the cyanide gas cylinders, government laws of placing industry in the vicinity of residential area apart from other, government after government bent its rules, made laws to ensure that the company is safe. The HPV vaccine case is not a legal battle but the ICMR report has almost exonerated all the ‘smart’ officials, agencies involved in twisting, bypassing or hiding information, rules and laws which could not allow to do what they did. The recent reports about drug trials in India also is raising our eyebrows about scruple less ground available to play ruthless, moneymaking, victim duping games here. Do we have adequate grievance redressal system? Is that enough or stringent legal – criminal and civil – remedies are required? If yes, is judicial system equipped for justice for a universal health care? Is law enforcement system adequate and prepared? What are the financial laws? These are issues which need to be addressed if insurance – a mechanism with purely financial purpose is adapted for universal health care. As public jury, we shall have to respond to what Rudy appealed2: “I hope you are astonished as I am that the wealthy insurance companies like the defendant will go to take money from low income family and then keep it, while denying a legitimate claim. It is no wonder they spend so much money on their lawyers. Their public relations machinery convinces us we need tort3 reforms and we need to end punitive damage. I am asking you the jury, just do what is right in your hearts. If you don’t punish Great Benefit, you will be the next victim!” (Words – 1146)

1

Grisham was born in 1955 in Arkansas, in the southern United States. His family was poor and they moved several times when he was a child, staying wherever his father could find work. They eventually settled in Mississippi, where Grisham did well at school and went on to study law at university. When Grisham qualified in law in 1981, he set up his own office. He often represented workers against big companies, or helped accident victims to get compensation for injuries. He also had a short career in politics, as a Democrat in local government.

2

3

http://www.script-o-rama.com/movie_scripts/r/the-rainmaker-script-transcript.html

A body of rights, obligations, and remedies that is applied by courts in civil proceedings to provide relief for persons who have suffered harm from the wrongful acts of others. The person who sustains injury or suffers pecuniary damage as the result of tortious conduct is known as the plaintiff, and the person who is responsible for inflicting the injury and incurs liability for the damage is known as the defendant or tortfeasor. (http://legaldictionary.thefreedictionary.com/Tort+Law)

mfc bulletin/Aug 2011-Jan 2012

45

Book Review

Back to the Future - Dhruv Mankad What are the historical, political, cultural and ethical dimensions of these (health) problems and of concrete solutions sought out in a day to day medical practice? Towards the Dilemma of Medical Culture Today explores the answers, looking through the glasses of kaleidoscopic knowledge, attitudes and experiences of medical academics and practitioners.

(Towards the Dilemma of Medical Culture Today, Edited by Anand Zachariah, R.Srivatsan and Susie Tharu on behalf of Christian Medical College and Anveshi Collective; Orient Blackswan, Hyderabad, 2010, 392 pages, 495.00. ISBN 97881-250-4091-0) We are at the threshold of transition in our health status and health care system – for the past 64 years! Infant Mortality Rate in 1950-51 was 165, today it is about 56 per 1000 live births. The prevalence of severely and moderate malnourished children is declining, so is the life expectancy. But, declining sex ratio is showing female feticides or no change in wasting and stunting is pointing towards looming hunger epidemic. The health infrastructure has increased from 725 in the ‘50s to 170000 now, but its utilization by the poor has declined. Availability of state of art technologies and human power trained for treating patients has gone up. But is it relevant, affordable and based on the needs of the sufferers and our people? Are the number and quality of health care services - both public and private as they exist today appropriate? If the answer is no, then the central question which needs to be answered is - what are the historical, political, cultural and ethical dimensions of these problems and of concrete solutions sought out in a day to day medical practice? ‘Towards the Dilemma of Medical Culture Today’ explores the answer, looking through the glasses of kaleidoscopic knowledge, attitudes and experiences of medical academics and practitioners. Some doctors had a sense of discomfort about the structure of medical knowledge. They were involved in a consultative process for improvement in medical education initiated by Christian Medical College. Anveshi also participated in the workshop as an observer. The participants of this collective discussed critically history, culture, institutions, assumption behind the theory and practice ‘dimensions of a crisis’ of medical knowledge. The book is a result of these different, new ways of looking at the crisis. (286) A Rich Dividend The book is a collection of essays which look into these ‘dimensions of a crisis’ which are: • Western medical practices as a norm for treating diseases • Monetary, institutional and experiential cost of treating patients with tertiary care • Increasing use of irrational and high cost drugs with a logic which has little to do with patient’s needs • Top down preventive program which are not sensitive for what the people want • Problems of doctors and academics in providing medical care, teaching and research

These essays are presented in five Sections after a comprehensive introduction. They are: Genealogies of Medicine in India; Health in the time of Development; Tertiary Care Medicine, Evidence based Medicine, Pharmaceuticals and cost; Thinking with the patient and finally, Resources of Practice: Calibrating Medicine to the needs of patients. Section I: Genealogies of Medicine in India elaborates the history of medicine in context of the health policies and plan of Indian Governments (Susie Tharu), the evolution of public health around a disease e.g. Kala Azar, (A. Zachariah and R. Srivatsan) role of the government and the biases of the scientific research. It also focuses on how the medicine evolved around the transient results of debacles of government e.g. PTSD with linkages of the medical professionals with the interests of the pharmaceutical companies.(K S Jacob) Section II: Health in the time of Development: Primary Health Care, Nutrition and Population Control narrates experiences of the past 30 years of actually working on relevant current issues. Working the Contradictions- Three Decades (Sara Bhattacharya) presents the canvas of her working with the community, in an academy and in a health department. Andhra Pradesh: Ground Level Observations (A P Ranga Rao), describes his experience in the government set up. The Career of Hunger: Critical Reflections on the History of Nutrition Science and Policy (Veena Shatrughna) outlines critically the development of policies regarding nutrition and food security, in a single stroke showing the bright and dark sides of the ‘hunger’ epidemic India is on the verge of. Different Readings: Demography and Population Control, (Sheela Prasad) talks about why Demography developed in the world and how it deviated the governmental models of health care against the peoples’or more specifically, women’s needs. Development and the Administration of Public Health – An Overview of Contemporary History (R Srivatsan) questions whether India was a Welfare state at all, dissects the logic of the development state and its consequences. He adds the origins of the politics of health in the twenty first century. Section III: Tertiary Care Medicine, Evidence based Medicine, Pharmaceutical and Cost delineates critically the existing approach diagnosing and treating patients suffering from a range of illnesses. Rethinking Organophosphate poisoning/ suicide in India (Anand Zachariah) based on history of Organophosphate poisoning in the West (Chemical Warfare) and in India (suicidal attempts), argues that the different treatment protocols need to be developed for different contexts, keeping in mind the contextual evidences. Development of Cardiovascular Epidemic in India and Inappropriate Tertiary Care Treatment Guideline (Anand Zachariah) traces the causes behind cardiovascular diseases epidemic in India and

46 recommends that an appropriate treatment guidelines should be developed which is affordable and effective for the large majority of the people who suffer – the poor. Drug Pricing and Access to Health Care: Some Issues and Options (S. Srinivasan) covers several problems the drug market in India faces: asymmetric information, market malpractices, overpricing of brand against generic medicines etc. and discusses options to overcome them. Section IV: Thinking with the patient narrates the experiences of patients from a varied background and expresses their feelings about inadequate attention from health personnel and institutions. It underlines what was expressed by the patient activism that there is a gap between healing and cure. ’The ‘Intractable’ Patient, Managing context, illness, health care (Lakshmi Kutty) articulates the feelings of poor patients who were ‘falling short of ideal behavior’ of health care system. Patient Questions (Duggriala Vasanta and Seemanthini Niranjana) attempt to question the official views about doctorpatient relationships. After Ervadi Healing and Human Rights in the Context of Mental Health (Jayshree Kalathil) presents thoughts about loss of community support systems to ‘mentally ill’ patients in case alternative therapy centres like Ervadi or dargas are closed. Section V: Resources of Practices: Calibrating Medicine to the needs of the patients focuses on the medical practice which is sidelined by ever growing medical theories. Rethinking Practices (Susie Tharu) points out toward the theory before practice is the conceptual approach in modern philosophy which always considers theory as superior than practice. Practice in the ICU: The case of Organophosphate poisoning (Sujoy Khan and Anand Zachariah) analyses the varied clinical presentations of pesticide poisoning and the clinical practices evolved out of these experiences, absence of such variation in treatment protocols and importance of dialogue with the patient in practice. People’s Struggle Producing a Curative Public Health for AIDS (Anand Zachariah) documents how the current understanding about AIDS treatment evolved through negotiations with patient groups, scientists, regulatory agencies and the pharmaceutical companies. What constitutes Evidence based Practice in Rehabilitation (Duggriala Vasanta) highlights case by case approach in rehabilitation of patients with Alzheimer’s Disease. It discusses the absence of evidences based non-biomedical practices for each patient. Finally, Reclaiming Primary Care: Marketing Depression and Anxiety in a different framework (K. S. Jacob) describes experiences of general practitioners of patients presenting psychosomatic symptoms of common psychiatric problems and emphasizes that at the primary care level, treatment of such illnesses should be based on these experiences.

Ethical Dilemmas in Medical Praxis Introduction – The Dilemmas of Medical Culture, by Anand Zachariah and R. Srivatsan. presents two cases which frame the fundamentals of the ethical dilemma they, the teachersactivists- researcher-practitioners face in dealing with each patients. According to the authors, these cases raise several questions viz., what was the best line of treatment: at primary level (by a GP) or at the tertiary level? What was the desire of the patient for survival, for treatment? Was clinical diagnosis

mfc bulletin/Aug 2011-Jan 2012 appropriate on a case by case approach or the protocol must be applied for each patient? Who was responsible for the avoiding the associated causes of illness – the patient or the hazardous products? Who should be responsible to treat when social determinants of ill-health like poverty are the main causes of illnesses like malnutrition, depression etc.? While going through the absorbing essays/chapters of this book I realized that the collective efforts are pivoted around four central theses. In summary, they are:

Thesis 1 (The principle thesis): Dichotomy in the Modern Medical Knowledge and Medical Practice •

Modern Medicine is based on the agendas of the governing, and not on the patient’s sufferings • Medical knowledge, its perspectives and content are determined by the governing – the state. • Therefore, it ensures that teachers and the students participate in this mode and act as its agent • During the practice, doctors apply the logic of the medical knowledge they have adopted, use the protocols directed by the government or professional association etc. • Also, we have to start a process subtly which does not have the same logic while treating a sick person. It is often contrary to what they have been trained to do and is illogical/ irrational from the perspective the medical knowledge is built upon. • Thus often the praxis (“a hard theory to put into practice”) instead of moving forward to a synthesis, disjuncts what was to be done for the sick patient from a patient-centric logic and what the doctor was directed to do as per the governmental protocol/ medical knowledge. • This leads to frustration, anxiety and burn out and even conflicts between doctors and patients/or medical practitioners and governmental medicine. Theses 2: Dichotomy in medical technologies transplanted from the Western context and its efficacy in India • Growth of Pharmaceuticals and Medical Technologies and Scientific Institutions – the Medical Industry was propelled by increased health expenditure during the post-World War era • Increased health expenditure inevitably ensured involvement of state by allocating large national budgets • Specialties developed along with the new technologies • Tertiary care developed in the western context, when applied in India, turned out to be non-feasible and inappropriate in local context, thus making difficult for doctors to provide cost effective and appropriate medical care • Doctors trained in sophisticated tertiary care hospital are unable to apply their knowledge in a less sophisticated or almost rudimentary settings Theses 3: Dichotomy in curative care services and public health services in India • Government shirked the responsibility of providing curative health care and focused on preventive programmes • These programmes were designed in ‘national interest’ by public health experts and administrators • Curative health care, particularly tertiary care is mostly in the hands of private hospital

mfc bulletin/Aug 2011-Jan 2012 •

Most doctors find private hospitals in India or in other countries lucrative and in congruence with their background, the investment they have made to get educated • Only the remaining doctors are available to provide services to rural or urban poor. In addition, they find difficult to relate to community coming from a different cultural, socio-economic background Theses 4: Dichotomy in the diseases and their manifestation in India and its presentations in classical (western) medical education • The epidemiological profile of illnesses in historical, cultural and clinical context are different in India as compared to any other countries • This leads to innovative diagnostic and treatment methods keeping in mind its cost • Medical education presents illnesses in context different from what exists in India Theses 5: Dichotomy in medical knowledge and its day to day practice • The tertiary care diagnostic and treatment protocols are developed on evidence generated in their settings • Practitioners often compute a different strategies based on their medical knowledge on the local context • Their contribution to people’s health needs is immense

Ethical Issues about the Healthcare Crisis The essay also lays down (p5) the dimensions of ‘the crisis that besieges health care..’: the disjunction between scientificity and efficacy (sic), cost that the patients bare due to illness, the treatment and its failure or success, relevance and appropriateness with Indian cultural, socio-economical context, healing - the patient’s need versus cure – the outcome of medical treatment and absence of ‘an academic orientation the crisis about the relationship between the medicine and the government to understand its form in the postcolonial, ‘developing’ country like ours. However, it is true that such dichotomies and dilemmas exist in reality; my contention is that the contradiction remains even if one takes patient centric views. It adds on further ethical issues:

1. Consent to choose an appropriate treatment source: Do the patients have the choice of accessing the advance tertiary care as against the residual, primary care that exists even if its governmental approach was dropped. Let us take the example of Hina. It is true that the best psychiatrist could not have done any better to the root causes, but was her access to an unqualified GP out of her choice or out of compulsion? This in a situation when we as doctors have by and large a choice of treating her or not, how and when, of deciding what is appropriate and what is not. She did not have any choice. 2. Whose interest is served if treated at a primary care – by a GP and not by an expert?Again, the answer is: neither was serving her interests. Treatment by a GP was giving her some psychological relief and was costing low. However, in case of Suicidal OP poisoning patient, adding the role of psychotherapy at the primary care level may ameliorate the miseries the patient is suffering from. However, does the clinician have autonomy of contextual trial and error in place of a well-tested clinical guideline even

47 if it is developed elsewhere? Particularly, this is so when the situation is an emergency.

3. ‘Cost’ reduction in a private health care setting is a double edged sword: When we consider the cost of treatment in any setting particularly in context of private health care, the issue is who gains from cost reduction approaches: Is it the provider who gains – need to spend less time per patient and hence delegated to the primary care provider or an assistant. (In infamous case of Dr Praful Desai, he retained the monetary cost but reduced time and hence the ‘responsibility’ nonmonetary costs.) Did the private hospital gained ‘profit’ by cost reduction - using comparatively cheaper treatment protocols (less investigations, less staff per patients, less services? 4. Conflict of interests: What is the interest of the various stakeholders involved in deciding the types of public health measures to be introduced? Is it because they have created a model which one side wants replicate on their terms? Particularly, this is more so when the model is competing with another one which has the same intention. E.g the Government of India wants to upscale Kala Azar as a National Programme (hypothetically, because the medicines are produced by a private pharmaceutical company) against say snakebite. 5. Policy and Guidelines as coercive documents in practice: If guideline generated by Government of India loses its guiding nature and becomes a mandatory clinical ‘fatwa’ then it is certainly a violation of the principle of formal justice of applying it as per the patient’s need. Using a single recipe ‘Khichdi’ in Supplementary feeding under the ICDS is one such example. The baby may need different feeding not just because of choice but as a patient still suffering or recuperating. Compulsory rural postings of doctors may be in patients’ interests and necessary in an ‘emergency’ but it is a coercion of the doctors. 6. ‘Misuse’ of Guiding Principles of decision making regarding ethical dilemmas: The central professional commitment reflected in the book, while finding solution of dilemmas, is being patient-centric. Often, when decisions are regarding ethical dilemmas particularly in public health, several of these principles are violated or often not used or worse, misused. E.g. Policy and guidelines are documented but not truths are revealed about the participants and the processes. (Principle of Veracity) or compromising the policy in favour of providers e.g. softness toward absenteeism in public health providers or missing social support component from Mental Health Programme for people with mental illnesses. Back to the Future In essence, the book is a rich source of theoretical perspectives and practical examples where not just the practitioner and the patients are having a dilemma, it is a presentation of the pitfalls in the future direction medical technology and public health is taking by looking back at the history of medical science, in the political and cultural context of problems of Public Health in India. My hats off to the authors particularly, Veena Shatrughna, K S Jacob, Anand Zacharia and R. Srivatsan for painting almost surrealistic canvas about the healthcare crisis in India and its

48

mfc bulletin/Aug 2011-Jan 2012

historical, political and ethical origins. They have exercised their vast knowledge and professional experiences of grappling the crisis ethically in day to day practices. One may have expected more guidelines, solutions and

preventive steps for health practitioners, to tackle the ethical dilemmas they face in medical practice. However, the authors have done an important job in emphasising the complexities and dilemmas of decision making in such situations, and this point is made in the title itself.

Contents Exploring a Roadmap for Health Care for All/UAHC

- Organizing Committee

1

Financing the Cost of Universal Access to Healthcare

- Ravi Duggal

8

Strengthening the Drug Regulatory Mechanism in India

- Anant Phadke and S. Srinivasan

13

Budgetary Outlay at TNMC Prices for ‘Medicines for All’ Scheme for Public Health Facilities

- S. Srinivasan and Anant Phadke

19

Towards a Critical Medical Practice

- R. Srivatsan

20

Some Bark, Little Bite

- S. Srinivasan

24

HHR for UHC:ARoadmap

- Shyam Ashtekar and Tarun Seem

26

Food and Nutrition in India: A Public Health Perspective

- C. Sathyamala

38

Response to Sathyamala

- Jean Dreze and A. Deaton

42

To be the Rainmakers or not to be? That is the question

- Dhruv Mankad

44

Back to the Future

- Dhruv Mankad

45

Subscription Rates

Annaul

Rs. Indv. 200

Inst.

U.S$ Asia

Rest of world

400

20

15

Life 1000 2000 100 200 The Medico Friend Circle bulletin is the official publication of the MFC, Both the organisation and the Bulletin are funded solely through membership/subscription fees and individual donations. Cheques/money orders/DDs payable at Pune, to be sent in favour of Medico Friend Circle,

addressed to Manisha Gupte, 11 Archana Apartments, 163 Solapur Road, Hadapsar, Pune - 411028. (Please add Rs.15/ - for outstation cheques). email: masum@vsnl.com

MFC Convener Convening Group: Premdas, Sukanya & Rakhal Contact Person: Rakhal - +91 9940246089 c/o Community Health Cell, No.31 Prakasam Street, T.Nagar, Chennai 600 017 Email: <rakhal.gaitonde@gmail.co> MFC Website: <http://www.mfcindia.org>

Editorial Committee: Anant Bhan, Dhruv Mankad, Mira Sadagopal, C. Sathyamala, Sukanya, Sathyashree, 'Chinu' Srinivasan. Editorial Office: c/o. LOCOST, 1st Floor, Premananda Sahitya Bhavan, Dandia Bazar, Vadodara 390 001. Email: sahajbrc@gmail.com. Ph: 0265 234 0223/233 3438. Edited & Published by: S. Srinivasan for Medico Friend Circle, 11 Archana Apartments, 163 Solapur Road, Hadapsar, Pune 411 028. Views and opinions expressed in the bulletin are those of the authors and not necessarily of the MFC, Manuscripts may be sent by email or by post to the Editor at the Editorial Office address.

MEDICO FRIEND CIRCLE BULLETIN PRINTED MATTER - PERIODICAL Registration Number : R.N. 27565/76 If Undelivered, Return to Editor, c/o. LOCOST, 1st Floor, Premananda Sahitya Bhavan Dandia Bazar, Vadodara 390 001


