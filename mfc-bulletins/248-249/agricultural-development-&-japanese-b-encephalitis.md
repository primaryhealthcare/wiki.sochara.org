---
title: "Agricultural Development & Japanese B Encephalitis"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Agricultural Development & Japanese B Encephalitis from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC248-249.pdf](http://www.mfcindia.org/mfcpdfs/MFC248-249.pdf)*

Title: Agricultural Development & Japanese B Encephalitis

Authors: Elamon Joy

medico friend circle bulletin November-December, 1997

248 249 Resurgence of Infectious Diseases Public Health Responses T Jacob John*

Introduction During the late 1980's and early 1990's western biomedical scientists have articulated their concerns about the frequency and magnitude of new or newly recognised infectious diseases in different parts of the world. During 1960's and 1970's they had believed that humans had virtually complete mastery over microbial pathogens. Starting from the last quarter of the last century, the discipline of microbiology has advanced so remarkably that we have been able to determine the causative agents of virtually all infectious diseases and to target the development of antimicrobials and vaccines against many of them. Using disinfectants, insecticides, food and water microbiology and public health engineering, industrialised countries have controlled most water-food-vector-borne infectious diseases. Although many such diseases are widely prevalent in developing countries, the constraint is the lack of resources or of "political will", not of technology to intervene. Smallpox was eradicated and poliomyelitis and dracunculiasis are on the verge of eradication. Over 80% of Infants born in developing countries are now receiving immunisation against the targeted diseases. Against this background some of the new or newly recognised diseases have baffled even the best of the western scientists. The human immunodeficiency virus (HIV) and HIV disease are sufficient to

illustrate this point. To focus the attention of policymakers, public health professionals, scientists and the public on this issue, the term 'emerging' infectious diseases was coined and later expanded to emerging and re-emerging diseases to highlight the resurgence of previously controlled diseases in several parts of the world.

Is there a resurgence of infectious diseases in India? Some thoughtful experts have asked whether the focus on emerging diseases is a western agenda to force poor countries to spend much money to contain infectious diseases so that they would not spread to their populations. One could argue that the West funded the smallpox eradication programme solely for the safety of their people; incidentally we also benefited in the process. While there might be an element of truth in such an argument could we say that we did not want smallpox eradicated? The West wants poliomyelitis eradicated and does not want any risk of re-introduction of poliovirus from India. Thus polio eradication is also a western agenda today. However, we could have mid should have made it our own agenda at least a decade ahead of the West, when we knew how to eliminate it in developing countries and the West did not. We should have taken * Department of Clinical Virology, Christian Medical College Hospital, Vellore, Tamil Nadu.

timely and independent initiative to ensure that not a single Indian child was crippled due to polio. Today we unashamedly admit that international pressure is the major impetus for us to eradicate polio in India and not our love and concern for our own children. Foreign visitors traveling to our country are often immunised against typhoid fever, hepatitis A, Japanese encephalitis, rabies and meningococcal meningitis; and they take malaria prophylaxis also. Such is our reputation about infectious diseases. In our hospital one of the leading causes for admission is tuberculosis. In our microbiology laboratory, about 20 isolates of Salmonella typhi are obtained, on the average, every month. Shigellosis, amoebiasis, giardiasis, cysticercosis, hydatid disease, lymphatic filariasis, malaria, kala-azar, Japanese encephalitis, dengue, leptospirosis, brucellosis and many more diseases are commonplace in many parts of the country. Cholera is endemic in all urban communities. Now HIV disease is being increasingly frequently diagnosed. Under these circumstances, should we worry about emerging infectious diseases? If we ignore all other infectious diseases without taking control measures then there is no particular advantage in naming some as new or resurgent diseases and ignore them also. On the other hand, if we want to take infectious disease control seriously, then there is some advantage in recognizing the phenomenon of the dynamic flux in which pathogenic microbes behave. We have old, new or emerging as well as resurging or re-emerging infectious diseases. The West knows how to deal with the old diseases which are well known, and against which there are vaccines, antimicrobials or other prophylactic methods. New or resurging diseases may bring surprises and there is no way of predicting their behaviour. The plague outbreak in Surat will illustrate this problem. Bubonic plague occurs every year in different parts of the world and no one is over-anxious about it since it is a vector-borne zoonosis. Pneumonic plague, on the other hand, spreads rapidly from person to person by the respiratory route. So, any outbreak of pneumonic plague will not be taken lightly by most people, either in the West or in India. The diagnosis of pneumonic plague, its open declaration, the wide publicity in the media, the house-tohouse search for cases, the consumption of tons of tetracyclines, the migration of people from Surat and the panic throughout the country were of our own making. The western media had an 'excellent story even

to simply report these happenings, thus fuelling international fear of spread through air travelers. Vienna had kept an establishment in readiness, capable of admitting several hundreds of plague patients, should the epidemic reach them. Millions of dollars worth of pre-emptive measures were taken by many other countries as well; our own loss in trade and tourism was estimated to be some 100,000 crores of rupees. As it turned out, there was plague in Surat, but it was a very small cluster of cases, without even one secondary case from any primary case. No one gives India the credit of having quickly diagnosed the forgotten plague, but we are pictured as a country that could not handle a small outbreak of a resurgent disease in a major city professionally and efficiently. Who is to blame if the West wonders 'if India would be able to handle another epidemic of, let us say, a new disease, such as Ebola virus disease of Zaire or Machupo virus disease of Bolivia? The lesson all of us have learned from HIV is that no one can predict the nature of new and emerging infections. We in India must put our act together so that the public health system will be able to respond to any new or resurgent disease. If not, the West can say that they have a right to intervene in India, since what happens here is likely to be a threat to them. The WHO has a new division on Emerging Infectious Diseases and the rules made by WHO will be binding on us whether we like it or not. As usual, the rules are likely to be formulated by those with expertise in the field. After the plague in Surat and the dengue haemorrhagic fever epidemic in Delhi in 1996, how could we make our voices heard?

The new cholera-causing V. cholerae 0139 emerged in southern India in 1991/92. The incidence of malaria is on the rise; even its geographic distribution is widening. The incidence of tuberculosis seems to be on the rise, based on the evidence that the proportion of TB patients with HIV infection far exceeds what could be expected on the basis of the overlap between HIV infection and TB. Obviously, TB manifesting as a result of HIV infection is showing up in increasing numbers. Leptospirosis outbreaks are increasingly being recognised in Andaman’s, Kerala, Tamil Nadu and Gujarat. It is also endemic in these states and in Maharashtra. Perhaps it is under diagnosed elsewhere. Melioidosis is more common than we realise. Anthrax continues to occur in some places. Thus, we have many examples of continuing or resurging infectious diseases in India.

Antimicrobial resistance of pathogens is another cause for concern. In recent years we have had several outbreaks of typhoid fever due to multi-drug-resistant S. typhi. Multidrug resistance is increasingly being feared in TB. Chloroquin-resistant malaria is already a public health problem. In summary, our scene is cluttered with infectious diseases against which control measures are already available and those against which vaccines or antimicrobials are not available. All of them can be controlled if we mount the appropriate responses. We must not lose any more time to initiate action. Defining public health responses Public health responses are essential if we are to control any newly recognised problems exemplified by the emerging and re-emerging infectious diseases. However, if we believe that interventions can be applied when such problems develop, then two questions arise. First, against a background of no interventions against the currently prevalent infectious diseases, can interventions specific for a resurgent disease be successfully applied? Second, in the absence of a routine disease surveillance system in place for the currently prevalent infectious diseases, will we able to detect any new disease before its outbreak becomes obvious enough for the media to draw people's attention to it? To put it more bluntly, unless there is a public system that is currently responding to already prevalent problems, we cannot expect meaningful public health responses to any new problems. While this paper is not the occasion to design or describe a public health system which will be efficient and responsive to challenges, the functional components of such a system can be enumerated and examined. First and foremost, the system must be able to generate information that is relevant to its role and relevant to the evaluation of its functioning. For ease of understanding I refer to such information as the components of public health surveillance. They are listed below: 1. 2.

3. 4. 5.

6. Monitoring of infections of vertebrates (domestic, commercial arid wild) including rodents, which may cause zoonotic diseases. 7. Monitoring of antimicrobial resistance of locally prevalent pathogenic microbes. Physicians are diagnosing and treating a variety of infectious diseases all the time. Some of such diseases are indicative of a breakdown of public health. For example, the occurrence of a vaccine-preventable disease, and especially if it is in a cluster, is indicative of inadequate immunization coverage or poor vaccine quality, both of which deserve immediate remedial action. An outbreak of cholera in a town is the result of faecal contamination and inadequate chlorination of the water supply; other waterborne infections may not cause such a severe or lifethreatening illness to alert the system. Malaria, Japanese encephalitis, typhoid fever, dengue haemorrhagic fever etc. are similar indicator diseases. We need a public health system to which all such cases must be reported and which is capable of quickly responding with preventive intervention. Today as we are in the poliomyelitis eradication mode, every case of acute flaccid paralysis must be reported and investigated. Only when the reporting habit is established can we expect to detect the very early cases of any unusual syndrome or fatal disease, in case of its emergence or resurgence. Thus, a disease surveillance system is essential if we want to confront endemic and resurgent infectious diseases.

Death is the end point of a variety of factors. There are patterns of age and causes related to the frequency of 'death in every community. Once these parameters are determined, monitoring of all deaths by age and by the perceived cause will give clear indications of any new fatal illness in the community, particularly in specific age groups. The diagnoses on diseases and data on death are already generated in an on-going manner everywhere; the Surveillance of infectious diseases of public health public health system must obtain them and use them for importance. monitoring. One might question the reliability of diagnoses Monitoring of all death reports in the community, by age that are reported or attributed as causes of death. The same question is also relevant in the case of all diagnoses and by perceived causes. reported in the surveillance system. In the beginning we must accept whatever diagnoses are currently being made. Monitoring of quality of drinking water. In due course, they can be verified on a sample basis; such Monitoring of the safety of food. a process will improve the quality of health care also. This exercise will also identify the need for laboratory service at Monitoring of biological vectors of infectious agents various levels of health care and thus further improve the prevalent in the region. quality of health care.

Wherever water is supplied, its quality assurance is the responsibility of the supplier, be it the Panchayat or a city. Today most middle-class people do not drink any town/city water supply for fear of infections. Instead, they buy bottled water. Volume for volume, bottled water is more expensive than pasteurized milk, proving thereby the huge demand for it. Viewed with public health in mind, the importance of food safety and vector bionomics are obvious enough and no further dilation is necessary here. The prevalence and range of infections of vertebrates must be monitored to recognise locally important zoonosis and to respond with remedial interventions. If infectious diseases are to be treated correctly, the antimicrobial sensitivity pattern of the relevant microbes should be known. In every region, there may be laboratories already testing microbes for drug sensitivity; the public health system must collect and disseminate such information. The public surveillance described above is only the information generating system for the purpose of determining the nature of interventions necessary. The public health responses must include the investigation of any outbreak or unusual illness or excess of mortality than anticipated. The investigation should be aimed at establishing the microbial cause and its transmission pathways. Therefore, microbiology laboratory expertise and epidemiological skills must be available to the system. The public health response must also include adequate intervention measures for prevention and control of the disease. Feedback of relevant information to the local members of the health care profession who report diseases to the system is necessary to keep up their motivation to continue reporting. Unless a system of responsive and responsible public health service is operative in every district of our country, and they are inter-linked and supported by a state level public health laboratory and epidemiology cell, and all state systems are backed by a national centre with laboratory and epidemiology components and facilities for training, we will not be able to prevent or control resurgent or endemic infectious diseases in our country.

(Contd. from page 12)

* The articles and the debate on bone marrow tests (mfc bulletins 217, 219, 230-31). Will also from part of the background papers for the theme meet of Jan' 98. References 1. Sanyal & Banerjee. A Longitudinal Review of Kala-Azar in India. Journal of communicable Diseases; 11(4): 149-169 (1979) 2. Rahman & Islam. Resurgence of Universal Leishmaniasis in Bangladesh. Bulletin of WHO; 61(1): 113-116(1983) 3. C.P.Thakar. Epidemiological, clinical and therapeutic features of Bihar Kala-Azar. Transactious Royal Society of Tropical Medicine & Hygiene;

78: 391-398 (1984) 4. Nandy etal. General Situation of Universal Leishmaniasis in India with special Reference to Proceedings of the International Workshop on West Bengal in Research on Control strategies for the Leishmaniasis, Ottawa 1987 IDRC/CRDIICIID (1988) Pg. 8-15. 5. (a) VHAJ. Kala Azar Re emergence of a Dreaded Disease. VHAI, New Delhi (? 1988) (b) P.N. Sehgal. Kala Azar Leishmaniasis (unpublished) VHAI, (? 1992) 6. Sen & Bhatia. Kala Azar, Current status and Evaluation of Control Activities in India. NICD (1988). 7. Dye et al. Earthquakes, influenza and cycles of Indian Kala Azar. Transactions RSTMH; 82: 843-950 (1988) 8. Elias et al. Universal Leishnnamous and its control in Bangladesh. Bulletin of WHO; 67: 43-49 (1989) 9. WHO Technical Report 793. Control of the Leishmaniasis (1990) 10. R.C. Dhiman. Epidemiology of Kala Azar in Rural Bihar using a village as a component unit of study. Indian Journal of Medical Research; 93: 155-60 (991). 11. Addy & Nardy. Ten years of Kala Azar in West Bengal. Part 1. Bulletin of WHO; 70(3): 341-346 (1992) 12. Kala Azar Janne yogya Bathein. Government of Bihar Ministry of Health, Medical Education & family Welfare (? 1993) 13. An historical review of malaria, kala-azar and filariasis in Bangladesh in relation to the flood Action Plan. Annals of Tropical Medicine and Parasitology; 87(4): 319-334 (August 1993) 14. An estimate of Kala Azar in 1991 in district Vaishali in Bihar. Bora etal Journal of Communicable Diseases 26(2): 120-122 (June 1994) 15. Dinesh & Dhiman. Plant sources of frutose to sandflies. Journal of Communicable Diseases; 23: 160-161 (1991) 16. Annual Report of DGHS 1992-93 17. Action Aid, Kala Azar in India. New Delhi 1995 18. EI Masum et al. Visceral Leishmaniasis in Bangladesh: the value of DAT as a diagnostic tool. Transactions RSTMH; 89: 185-186 (995) 19. Saxena et al. Visceral Leishmaniasis Control in India through primary health care system. Journal of Communicable Diseases; 28 : 2 (June 1996) 20. Wysocki. Overview of Health Situation in South East. Asia Regional Health Forum; 1(1): 7 (1996) 21. WHO. Dominant Communicable Diseases: South East Asia Region 1996. Fact file WHO, SEARO, New Delhi.

•

MFC Annual Theme meet: Resurgence of Infectious Diseases January 1 to 3, 1998, Sevagram, Wardha January 4, 1998: Annual General Body Meeting of MFC

Contact: Dr. Anand Zachariah, Medicine Unit I, CMCH, Vellore, Tamil Nadu 632004

•

Agricultural Development & Japanese B Encephalitis A Case Study from Kerala Joy Elamon Achutha Menon Centre for Health Sciences Studies, Trivandrum

Introduction There is a growing fear, across the world, over the emergence of new infectious diseases especially in the context of AIDS pandemic and drug resistant organisms. In countries like India, where there is a dichotomy of existing problems and the new diseases, the issue needs to be approached in a different way; The situation in Kerala is even more different in that there are some diseases like malaria which were thought to be no more a problem has started reappearing. In addition to these are the new diseases like leptospirosis and Japanese B encephalitis which were not seen o-r diagnosed here earlier. Not many studies have been conducted to find out the reasons for the emergence of these diseases. One has to view this in the context of the re-emergence of other communicable diseases and should look into whether the reasons are the same, especially as regards the environmental arid developmental factors. An epidemic of Japanese B encephalitis had broken out in the Kuttanad region, which stretches between Alappuzha, Kottayam and Pathanamthitta districts of the state of Kerala in South India, during the months of January and February in 1996. This was repeated during the same period in 1997 also. The region is known as the' rice-bowl of Kerala, as it is one of the major rice cultivating areas of the state. This was also one of the areas of the state where there was an organised effort to improve the agricultural production through planned agricultural development projects. Even though Japanese B encephalitis is known for its endemicity near paddy fields, there are no proved cases of it reported earlier from the area. According to reports of the Directorate of Health Services, Kerala, 96 cases of the disease and 16 deaths were reported in 1996 while so far this year, the corresponding figures are 100 cases and 3 deaths (Table 1). In spite of this, no major studies have been undertaken till now to find out the causes which led to the emergence of Japanese B encephalitis in the area. Hence this study.

The Disease Profile Japanese B encephalitis is an acute central nervous system infection caused by a group of arbo virus (Flavi virus) and transmitted mainly by culicine mosquitoes. It is a potentially serious condition, endemic through most of Far-East and South-East Asia. Fatality rates in epidemics have ranged between 10 and 50%, with a morbidity rate of 50% in survivors'. In recent years, it

has spread widely in South- East Asia, and outbreaks of considerable magnitude have occurred in Thailand, Indonesia, Vietnam, India, Burma and Sri Lanka". Institute of Virology, Pune has indicated that about half the population in South India had neutralising antibodies to the virus". The disease is characterized by prodromal symptoms which include fever, malaise, headache, photophobia, vomiting and neck stiffness. The neurological sequelae are more pronounced. It is mainly a zoonotic infection and the human infection is incidental.

The Agent The disease is caused by group B arbo viruses which are RNA flavi virus. They have several strains. The reservoir' of the virus has not been determined though antibodies have been detected in the blood of a variety of animals like horse, pig, cattle, goat, sheep and dog.

The Hosts The infection is mainly zoonotic and animal hosts constitute the majority. Their natural cycle in the animal host is known as wild cycle. As far as animal hosts are concerned, pigs are the most important among the vertebrates. Infected pigs do not show any signs and symptoms of illness but the virus in the circulation infects the mosquitoes, which act as the vector. This cycle is maintained through pigs and mosquitoes. Cattle and buffaloes, though not the natural hosts, act as mosquito attractants. Horses and goats are the only domestic animals So far known which show the symptoms of Japanese B encephalitis infection. Some species of birds such as pond herone, cattle egrets, poultry and ducks are considered as amplifying hosts. Man is an incidental dead end host. The disease is transmitted to man by the bite of infected mosquitoes. It does not show any specificity towards a particular age or sex, but in endemic areas, children between 3-15 years are usually affected. This has been attributed to the immunity acquired naturally over time by the adults. The incidence has been found to be elevated in the elderly due to decreasing immunity. The incubation period in man is not definitely determined but it varies from 5-15 days after the mosquito bite. All those who have been bitten by infected mosquitoes do not develop disease. The ratio of overt disease to inapparent infection varies from 1:300 to 1:10002. Even though the factors determining the outcome of the disease are not yet determined, the host factors and the dose of virus infection are thought to be important factors.

Though by soil topography and water conditions, the area can be divided into various zones (Table 2, Map-2), this paper considers it as one as the study does not intend to find out the differences between different areas. Major features of Kuttanad are the paddy cultivation, seasonal floods, salinity of water, fishing, lime shell collection and retting and defibering of coconut husks for coir manufacture.

The Vector and the Environment The vector which transmits the Japanese encephalitis virus to man is the mosquito. The species complex of Culex vishnuii, which consists of Culex tritaenorhyncus, Culex pseudovishnuii and Culex vishnuii is the main vector 16 group . Some of the subspecies of mansoniae and anophelines have also been found to be vectors.

Culex tritaenorhyncus breeds in irrigated rice fields, shallow ditches and pools. They prefer clean water collections and require vegetations like algae and other water weeds. The presence of water hyacinth and pistia are favourable environment for the larvae. The mosquito can survive in saline water. In rice fields, many kinds of natural predators exist to which the mosquito is susceptible. They include fishes, larvae of Dysticidae and Hydrophylidae, nymphs of Libellulidae, spiders, planaria and others. C. tritaenorhyncus has recently developed resistance to many pesticides. The adult mosquitoes are out of doors during the day time and attack the host animals at night. They usually rest for one to two hours on the wall before or after feeding or on both occasions. The species has a long flight range of 2-3 kms. Outbreak in Kuttanad Outbreak of Japanese B encephalitis in Kuttanad occurred in January and February months of 1996 and 1997. In other parts of the world, its usual occurrence is in summer or after the rains. Both the males and females were equally affected. The preference of infection in children was not pronounced probably because the infection was new to the region and immunity is yet to develop in adults. The virus has been isolated from Culex tritaenorhyncus species of mosquitoes collected from the area. Mansoniae uniformis also has shown the presence of virus. No reports on the animal hosts studies are available. The symptoms of the disease mimic other encephalitic conditions and malaria. The lack of diagnostic facilities for the infection also had influenced the non-confirmation of the diagnosis.

It has been already mentioned that Kuttanad is known as the "rice bowl" of Kerala as its major cultivation is paddy. Efforts to develop Kuttanad as a rice growing area began more than a century ago. Since the floodwaters carry a large volume of fertile silt, it was recognised quite early that if the flood waters were effectively regulated, much of the low lying land could Map 1 be used to grow a rich rice MAP OF crop. It was peculiar to KERALA Kuttanad that cultivation SHOWING LOCATION was only once in a year and OF the rest of the year, these KUTTANAD paddy fields were covered REGION with water. Due to the incursion of saline water from the sea, weeds did not grow in the fields during this period. But this salinity prevented the achievement of high crops. Kuttanad has a water body with an abundance of nutrients and receives strong sunlight which reaches a few meters below the water surface and has a temperature conducive to the production of water borne fauna. According to a study conducted in 1948, it had around 32 fish species.

Kuttanad

Agricultural Development Programme

Geographical Features

It is from 1930s that the history of agricultural development programme evolves. Towards the end of 1930s, due to World War II , deployment of paddy and rice from Burma was stopped, and faced with this severe shortage of rice, the then Government of Travancore explored the possibilities of raising two crops of paddy in Kuttanad". Studies by different agencies, both local and foreign, culminated in the Agricultural Development Programme of 4 17, Kuttanad two decades later .

Kuttanad is a low lying area extending over 874 sq. kms, distributed over 79 villages in Kottayam, Pathanamthitta and Alappuzha districts of Kerala in South India (Map 1)4. The peculiarity of Kuttanad is that about 65% of the area is below sea level (0.6-2.2 mts.), which is annually subjected to severe flooding during both the monsoon periods. About 80 sq. kms comprises Vembanad Lake and the various water courses including rivers and man made canals. It is a 4 densely populated area with 112.8 persons per sq. kms , However, the garden land, the area available for human settlement, is only 35% of the total area and so the density of population is much higher.

3.

•

In order to drain off floodwaters, a spillway with a length of 368 mts. was constructed at Thottappally and was commissioned in 1955.

•

During the summer, as the level of water lowers, saline water from the sea enters the area and this

4.

salinity causes damage to the crops. A regulator the salt water barrier of 1402 mts long - was constructed at Thanneermukkom to check the intrusion of saline water. It was completed in 1974 only. (Map 2) The combined effect of spillway and the regulator was expected to increase the area under double crop paddy by enabling (a) the date of sowing of the first crop to be advanced and (b) the raising of the second crop by preventing the incursion of saline water in the summer months. In addition to this were the changes in agricultural practices like the better seeds and increased 6 use of fertilizers and pesticides . Along with these, various non- agricultural development activities were also taken up, the most important among them being the 42 kms long road across Kuttanad, linking Alappuzha to Changanacherry.

Impact of These Development Programmes The impact on the agricultural field is not within the purview of this paper. Only those factors which may contribute to the study on the environmental aspects contributing to the health scenario is considered. When the spillway was commissioned in 1955, it was found that it could not discharge more than one third of

Map-2 MAP OF VEMBANAD BACK· Water SHOWING SALINITY AND KUTTANANO AREA ... BOUNDARY OF KUTTANAD

(See Note on Table 2)

-,

"\ \ \ \ \

I

\

_. - ---

--

/)

its expected function. Thus it did not contribute to the control of flood situation. Moreover, a sand bar (sand barrier) started appearing preventing floodwaters draining 7 into the sea . In the case regulator, towards the end of each year, in December, the saline water incursion into the area is prevented by lowering the shutters of the regulator. Studies have established that the salinity has come down, 7, 8 though it varies from region to region. (Table 2 Map 2). The decrease in salinity has also attributed to the decrease in the fish population, which were mainly salt water fishes. The stagnation of water has also led to the changes in the production of planktons and organic matter which form the nutrients of fish". The increased use of fertilizers has produced increased quantity of algae and weeds like water hyacinth, pistia and an unique water plant, known 5 in local language as "African payal' . These could not grow in saline water and whenever it grew, it used to be drained off by floodwaters. The faunal flora characteristics of the region have undergone many changes. The details regarding the impact of these development programmes have been brought out by the Indo- Dutch Mission, which conducted the Kuttanad water balance study in 1987-88. (See Appendix I) The 42 kms long Alappuzha-Changanacherry road cutting across the heart of Kuttanad paddy fields have prevented water from flowing from one side to the other and has thus aided in water stagnation.

Discussion and Findings 1. In various other parts of the world, like Thailand, emergence of Japanese B encephalitis had occurred in places where paddy cultivation was extended to newer areas which were dry lands earlier. But in Kuttanad the area was under paddy cultivation for more than a century. The conditions that prevailed then in the region might have prevented the disease from occurring here. The most important factor is the salinity of water. The vector of Japanese encephalitis, Culex tritaenorhyncus and the related species of mosquitoes do not breed in saline water. Since the salinity has been controlled by way of the regulator, the change in environment has cleared the way for the vector. It is to be noted that in all other areas where the disease is endemic, the outbreak follows rains or in summer. In Kuttanad, the regulator is closed in December thus preventing saline water entering the region, and the outbreak, of the disease has occurred in January- February months in 1996 and 1997. 2. Stagnant water without salinity and increased use of fertilizers have helped the water hyacinth, pistia, African payal and algae to grow. This has provided an ideal breeding place for mosquitoes. As there is no proper washing out of these weeds through floodwaters; which

was the case prior to the regulator and the spillway, their growth has increased. 3. Due to the changes in salinity and the absence of nutrients like the planktons which have been reduced due 7 to the changes in water eco- system (Table 2) there is a decrease in fish population. Fish in the region were salt water fish. The fish and other predators mentioned earlier in the paper were controlling the mosquito population. WHO Technical Report Series on Integrated Vector Control has established that the application of agricultural pesticides had actually led to reduced densities of natural predators and increased densities of vector populations 10. 4. The uneconomical nature of paddy cultivation has forced the farmers to rear cattle and pigs in increasing numbers as another source of income. The Economic Review brought out by the State Planning Board states that in the sector of milk production, meat production and 11 poultry, the state had a leap forward in the last decade . The population of ducks has also increased. As we had already seen, these animals and birds are potential hosts for Japanese B Encephalitis virus. Moreover, since the density of human population is very high and the garden land area available is very less, backyard cattle and poultry farming are being practiced. This gives ample chance for the agent in the natural cycle to enter into human beings. 5. While considering all these aspects, one wonders how the virus entered the area. Different hypotheses can be evolved, which need further study. The possibilities of pigs from endemic areas entering the region cannot be ruled out. Another possibility relates to the migratory birds coming to this area every year in November and leaving 12 by April. One among them is the Siberian duck (teals) . They travel all the way from Siberia every year to Kuttanad. Siberia is endemic for Japanese B Encephalitis and it occurs on the steppes, barren plains and in the 16 vicinity of lakes, swamps and marshy pools in Siberia , This needs further study. The decrease in salinity, increased use of fertilizers and pesticides, the water plants and algae not being destroyed or washed away, decrease in fish and other predators have all led to the growth of Culex tritaenorhyncus mosquito and the related species, which act as the vector of Japanese B Encephalitis. The increase in cattle, pig and poultry population has aided in maintaining the domestic cycle of the agent.

Limitations of the study 1. The paper is not based on a field study. It only tries to arrive at conclusions by analysing the literature already available on the disease and the agricultural development programme in Kuttanad.

2. No previous studies have been conducted to link the epidemic to the development programme and vice versa. Thus, this paper tries to bring together the ideas conveyed in two separate groups of literature, one on the disease and the other on the development programmes. 3. The paper tries to arrive at an hypothesis only and the actual linkage of the agent, host, vector and the environmental changes have to be verified through further studies.

Suggestions and Policy Options No specific treatment is there for the disease and the thrust is on prevention. One has to note that the already implemented development programmes are there to stay. The due weightage has to be given to them for the benefits they provide in other sectors. The suggestions for prevention of the disease are given below.

Vector Control Control of Culex tritaenorhyncus and the related species of mosquitoes can be done on a short term and a long term basis. In the short term method, anti larval and anti adult measures using aerial or ground fogging with ultra low volume insecticides (eg., malathion, fenitrothine), Indoor residual spraying and spraying over the vegetation around the houses, breeding houses and animal shelters are 2 suggested , Uninfected areas falling within a radius of 2-3 kms (flight range of the vectors) have also to be covered. Another strategy suggests that the cattle shelters be excluded from spraying so that infected mosquitoes are not 13 compelled to bite human beings . Long term vector control measures are through biological control. WHO technical report series has suggested the use of Bacillus 14 thuringiensis H-14 as a control agent :

Organised cultivation of larvivorous freshwater fishes like Gumbusia affinis may be tried13, 14. Control of weeds which act as the breeding place for mosquitoes require much evaluation because different methods like the chemical, biological and even manual methods have already been tried out in Kuttanad.

Animal Hosts As the pigs are the only amplifying hosts, pigsties should be kept far away from the houses. Cattle farming and poultry also should be away from the houses. In a place· where there is high density of population and less availability of land, the feasibility of this suggestion needs verification according to each location. Another suggestion is to have cattle sheds in between pig shelters and human settlements so that the infected mosquito may 13 spare human beings (vector being zoophilic) .

Vaccination Inactivated weanling-mouse brain-derived vaccine from Japan which claims 91% efficacy is costly with $147 in US for three doses. In 1988, comparatively inexpensive

($0.06-0.09 for three doses) live attenuated vaccine 15 claiming 97.5% effectiveness has been tried . In an endemic area like Kuttanad, this in turn may turn out to be a potential preventive measure but cost effectiveness in Indian standards have to be studied. Considering the huge amount spent in 1996 for spraying malathion in every Panchayat in the state of Kerala, it may be feasible to concentrate on preventive measures in Kuttanad and nearby places.

(* According to the salinity of water and the pattern in fish population, Vembanad backwater is divided into 10 zones, the number 1 being the northernmost and the-no: 10 the southernmost". Note that the zones 7, 8, 9 and 10 are the zones protected by the regulator. Source of data: Jhingram V. G., Fish and fisheries of India, Hindustan Publishing Corporation (India) 1975 for data on 1974 and Indo- Dutch Mission, Kuttanad Water Balance Study- Draft Final Report, 1989 for data on 1988.) Appendix I Studies and Reports on Kuttanad Development

Agency / Person

Study

Proper surveillance of the infection and disease has to be instituted. A detailed study on the emergence of the disease and the health problems of the region is a necessity before further development activities are undertaken.

Government of Kerala

Conclusion

Kerala Sastra Sahitya Parish at Government of Kerala

Report of Kuttanad Development Scheme Report of Kuttanad Enquiry Commission The Ecology and fisheries of Kuttanad Problems of Kuttanad

The salinity of water and soil, seasonal washing away of the weeds by flood waters and the presence offish and other predators had prevented the vector Japanese B encephalitis in Kuttanad, the rice bowl of Kerala. As the Kuttanad agricultural development programme was implemented, these conditions were reversed and became ideal for the vector to breed and grow, and finally to transmit the virus. Other development activities like pig and cattle rearing and poultry farming have aided in maintaining the life cycle of the virus. While considering the cost- effective and eco friendly preventive measures, a detailed study on the emergence of the disease is also necessary. To sum up, any agricultural development programme, with all its short term and long term, direct and indirect benefits to the population at large, may also have some impact not beneficial to the people, especially in terms of health. This has to be analysed prior to implementation of such programmes. Table 1 Focal outbreak of Japanese encephalitis Epidemiological Situation Report upto 2-3-1996 District Inside Outside Total Death District District 1 42 4 Alleppey 4 4 1 2 Kottayam 5 1 0 3 Pathanamthitta 2 3 5 *Note: This report is only 2 upto 2-3-96; Consolidated Report-96 cases and 16 deaths in 1996, 100 cases and 3 deaths in 1997 upto 7-4-97. Source: Report of the Director of Health Services, Kerala. April, 1997 Table 2 Salinity of Vembanad Backwater in 1974 and 1988 Zones Salinity ppt 1974 Salinity ppt 1988 One 27.3 16-30 Two 33.5 16-30 Three 25.5 15 Four 21.5 15-8 Five 18.8 10-8 Six 18.0 <11 Seven 18.0 <4 Eight 14.6 <3 Nine 10.5 <2 Ten 10.5 <2

Government of Kerala Samuel C T

John Abraham Kannan K P

Parameswaran M P Indo Dutch 'Mission

Report on Comprehensive development of Kuttanad Comprehensive Development of Kuttanad Ecological and socioeconomic consequences (Working paper, Centre for Development Studies, Trivandrum. ) Eco- degradation at Kuttanad Kuttanad Water Balance Study (final report)

Year 1957 1971 1977 1978 1978 1978

1979 1987 1989

References 1. Walter W. Holland, Roger Detels and George Knox, Oxford text book of Public Health, Vol. 3, Ed. 2, page 319. 2. Park J. E and K Park, Japanese encephalitis in Text book of Preventive and Social Medicine, edn. 12, pp. 176-177. 3. Indian Council of Medical Research, ICMR Bulletin, March, 1975. 4. Registrar General of India, Census of India, 1971. 5.Kannan KP., Ecological and socioeconomic consequences of water control projects in the Kuttanad region of Kerala, working paper of Centre for Development Studies, Thiruvananthapuram, No: 87, 1979. 6. Ampatt, Babu, Kuttanad, facts and fallacy, Integrated rural technology centre of Kerala Sastra Sahitya Parishad, 1992. 7. Indo-Dutch Mission1989, Kuttanad Water Balance Study. Final Reports, 1989. 8. Parameswaran M.P., Eco-degradation at Kuttanad- a review, Environmental Services Group, 1987. 9. WHO, Paraasitic diseases in water resources development, The need for intersectorsectoral negotiation, 1993. 10. WHO, Technical report series 'No: 369, Arboviruses and human diseases, 1967 and No: 683, Integrated vector control, 1983, 3.3.1, pp 2728. 11. State Planning Board, Kerala, Economic Review 1996. 13. Zachariah P.T., Article on Japanese Encephalitis in Kerala Medical Journal, Vol.37, No.1-3, 1996, pp 13-14. 14. WHO, Technical report series No: 679, Biological Control of Vectors of Disease, 1982. Pp 14-15. 15. Sean Hennessy et al. Article on Effectiveness of live attenuated Japanese encephalitis vaccine, a case control study in Lancet, Vol. 347, June 8, 1996, pp 1583-86. 16. Roy D. N., A. W. A. Brown, Entomology Bangalore Publishing Company, 1970, pp 176-179; 17. Rao K L. Et al., Report of Kuttanad Development Scheme, Government of India, 1957.

•

Kala-Azar Since 1977 Prabir Chatterjee Department of Community Medicine, CMCH, Vellore

Prelude: Early History of Kala-azar Kala-azar was first noticed in Jessore in 1824 and along with malaria, with which it was often confused, was responsible for millions of deaths in Bengal, Bihar and Assam in the second half of the nineteenth century. It continued to be a menace even after Leishman and Donovan first observed the causative organism in splenic material in 1903. It finally appeared to abate following the widespread spraying of DDT in India between 1953 and 1957 as part of the National Malaria Control Programme. By 1960, the number of cases had begun to fall and in 1961, there were only 196 cases reported from anywhere in India. Kala-azar was thought to have been conquered. (See Table-L)

The First Movement: Kala-azar Returns In 1975, however, visceral leishmaniasis, as kala-azar is otherwise known, began to reappear. (See Tables 1 and Map-l

BIHAR (INDIA> Visceral Leishmaniasis Affect •• d Areas

N

3). The School of Tropical Medicine in Calcutta reported a number of cases from Bihar. Back in Patna, there had always been a few paediatric cases, and the Leprosy Mission Hospital had been seeing Post Kala-azar Dermal Leishmaniasis (PKDL) cases in Muzaffarpur throughout the 15 year period. In 1977, The National Institute of Communicable Diseases carried out a sample survey in Bihar and estimated nearly 100,000 cases'. The bulk of these were from the area north of the Ganges— i.e. from Muzaffarpur and Vaishali Districts. From here it spread across Samastipur and Saharsa to reach Purnea. Aggressive control measures were undertaken and in 1984, the Professor of Medicine at Patna claimed that the 13 epidemic was under control . Cases in Purnea peaked at 4 this point and there was a spillover to Katihar, West Bengal 2, 8 and probably on to Bangladesh , where there were reports of a rise in cases at this point of time. It took till 1986 for Purnea to cease being the kala-azar capital of the world. By then Sahebganj, which is South of the Ganges, on the opposite bank of the river to Katihar and Malda, had taken over. This could have been possibly due to the large scale movements of Santal tribals to (and from) Sahebganj from Purnea and the neighbouring West Dinajpur in West Bengal.

Interlude: Research on Kala-azar Research into the source of the 1977 epidemic showed the possibility of old kala-azar cases from the earlier epidemic converting to PKDL and infecting their neighbours. This in fact seemed to be the case in 24 Parganas in Southern 11 Bengal which had a focal outbreak . PKDL cases in Bihar (near Patna) were also suggested to be the source of 10 outbreaks there . Less provable hypotheses were that earthquakes and influenza epidemic were related to the kala-azar epidemics. 7 They were however shown to influence death rates. One study hypothesised the role of big dams and embankments in preventing flooding and consequently protecting the sandfly from being washed away. But evidence presented 13 for such a phenomena was not convincing. 10

Some studies pointed to mud plastering of walls and the presence of plants like Amaranthus spiroza and Musa 15 sapientum as risk factors for a house to have kala-azar cases. The male sandfly apparently fed on the juices of these plants. 5.

S

5

Voluntary organizations clamoured for control measures 6,12 while the government institutions and the WHO 9 reviewed the situation . In 1983, a big workshop was organized in Patna by the Indian Council of Medical Research to discuss the problem (Indo-UK workshop on leishmaniasis, 1983).

The Second Movement: Kala-azar Peaks Again Meanwhile, in the complacency in North Bihar, kalaazar struck again in Vaishali in 1990. Cases in Bihar as a whole had fallen from 41,593 in 1978 to just 12,983 in 1984 when complacency was at its highest. In 1990, it had reached 54,650 cases (in Bihar) and continued to rise till it was 77,101 in 1992 (all India) (see Tables 3 and 4). Articles in the newspapers, statements in the assembly and 17 publicity by voluntary organizations about the disease have characterized the last few years. For instance, the headline "Health Minister Grilled on Kala-azar issue" (Times of India, Patna, 10.7.96), or "Failure to spray DDT led to Kala-azar spurt" (Times of India, Patna, 17.7.96).

Discordant Notes: A sudden Drop in Cases It is a fact that DDT spraying did not take place in 1994, 95, and 96. From personal experience, I can also vouch for the fact that Sodium stibogluconate (the mainstay of treatment for Indian kala-azar) was not available from government dispensaries in Sahebganj or Pakur districts from July 1996 onwards. These districts had been the epicentre of the 6 world's kala-azar only 10 years earlier , This acute lack Sikkim of medicines resulted in a fall in attendance of kala-azar patients in government OPDs. This was compounded by an insistence on bone marrow Map-2 testing from 1994 West Bengal (India) Bangla VISCERAL onwards. This desh LEISHMANIASIS test has been said AFFECTED AREAS not to be feasible in the field". This question of bone marrow testing has been discussed in the MFC bulletins 217,219 and 230 31. Now claims are being made that

7. 6.

kala-azar has again been controlled in Vaishali in 199419• There have been suggestions that testing of contacts and focal spraying around the houses of proved cases may be sufficient to control kala-azar. This is based on the fact that sandflies cannot travel far, unlike other insect vectors. There are also claims that Kala-azar is declining in India, as a whole'", Table: 1

Year 1977 1978 1979 1980 1981 1982 1983 1984 1985 1986 1987 1988 1989 1990 1991 1992

Kala-Azar Morbidity (mortality) Bihar West Bengal

India

18589(229) 41953 (62) 25 172 (28) 13620 (23) 14165(35) 11 120 (35) 11 832 (28) 12983 (67) 13 029 (37) 14079 (47) 19 179 (77) 19639 (123) 30903 (477) 54650 (589) 59614 (834) 66959 1266)

18742 (229) 42015 (62) 25 551 (29) 12830 (22) 14512 (41) 12,360 (38) 14406 (135) 17 224 (67) 17 291 (46) 17806 (72) 23685 (94) 22739 (131) 34489 (492) 57742 (606) 61348 (869) 68175 (1267)

(Source: DGHS Statistics)

63

NA 71(l) 333 (6) 917 (5) 1234 (3) 2717 (7) 4233 4257 (5) 3718 (25) 4447 (10) 3068 (2) 3573 (20) 3037 (16) 2030 (3) 1212 (l)

Enigma: Quo Vadis?

Table-4

Given the past history of kala-azar, how it came back with a bang in 1977 with 18,742 cases and was supposed to have disappeared around 1984, only to re-emerge and start increasing after reaching 23,685 cases in 1987, it is too early to predict that the apparently low figures in 1994 of 22,831 are the beginning of the end or the herald of a third peak.

Kala-Azar in Vaishali with comparisons with selected districts of Bihar & West Bengal Vaishali

Purnea

Sahebganj

6333

34

3*

9853

764

0*

4249

1042

-

1976

291

1977 1978 1979

West

History of Kala-azar (Early epidemics of malaria and Kala-azar tend to be confused with each other)

1917-29 1925 1935-37 1940 1943 1953-57 1960 1961 1975-77

bad 295

14

-

-

304

564

-

172

515

4.90

1980

1495

905

-

1981

716

5621

1982

346

4705

Jwar Vikar epidemic in Jessore (now Bangladesh) Burdwan Fever epidemic in Bengal

1983

282

5336

-

1375

367

907

Recorded epidemic in Garo Hills (now in Meghalaya Epidemic recorded in Bihar

1984

242

5937

529

3039

479

667

1985

208

4263

607

2960

560

633

1986

175

3137

1367

757

191

213

1987

,290

2781

6918

1988

394

1775

7078

1989

3047

1574

4715

1990

9916

1462

3357

1991

9658

1992

8758

1993

3037

1994

1788

Kala Dukh epidemic in Purnea (now Bihar) Leishman and Donovan first report characteristic bodies in splenic material from Dum Dum fever victims. Widespread epidemic in Eastern India Suton's maps of Kala-azar and Phlebotomus argentipes distribution Severe epidemic in Bihar

Phelebotomus argentipes demonstrated to be the vector of Indian Kala-azar. Second Bengal kala-azar epidemic NMCP which involved widespread DDT spraying in India Total of 3916 cases reported in India Only 196 cases in the country Sample survey by NICD estimates 1 lakh cases and 4,500 deaths in one year. Table: 3 Four Yearly Trends In Bihar Kala-Azar

Year North Bihar

1977-80

1981-84

1985-88

1989-90

Muzaffarpur

22160

2149

4326

9144

Vaishali

21392

1786

1067

12963

Samastipur

13415

2706

5076

14027

East Bihar Purnea

2745

21599

13956

3036

Saharsa

8714

2231

3936

4109

Katihar

2850

4272

3842

1622

12

Source: Government of Bihar publication

-

529

15970

8072

(Source: Government of Bihar publications12) This district wise data shows how kala-azar moved from the heart of North Bihar towards the eastern part, then across the Ganges to south Bihar. In 1984 claims were made that kala-azar was under control. In fact it spent the next four years devastating Sahebganj district before it made its comeback in North Bihar.

Saxena et all

10

This table shows the trends in Vaishali district, which due to its central position, proximity to Patna and political importance has the most complete data. The entry into eastern Bihar (represented by Purnea) and west Bengal is followed by Kala-azar in Sahebganj before a new peak in Vaishali *Statistics for Santal Parganas, from which Sahebganj was formed in 1984. Table 5 Kala-Azar In South East Asia Bangladesh

India

Nepal

1200

23685

169

2548

22739

442

2526

34489

291

3334

57742

446

3039

61670

870

1992

6818

77101

1395

1993

6030

44844

1500

5800

22831

1200

1987 1988 1989 1990 1991

South Bihar Saheb Ganj

Malda Murshida-

Dinajpur

Table·2

1824-25 1862-72 1863 1882 1898 1903

West Bengal

1994

Source: Dominant Communicable Diseases, SEARO 1996

(Conted on Page 4)

Resurgence of Infectious Diseases Crisis in Public Health Madhukar Pai Dept. of Community Medicine. Sundaram Medical Foundation Community Hospital. Chennai

"If you see a baby drowning you jump in to save it; and if you see a second and a third you do the same. Soon you are busy saving drowning babies you never look up to see there is someone there throwing these babies in the river." Wayne Eltwood India, as one author very bluntly put it "is slowly becoming an enormous lavatory which breeds not just disease but despair." 1 In addition to being labeled the global capital of AIDS, we have, over the last few years, witnessed outbreaks of plague, cerebral malaria, dengue, leptospirosis and drug resistant tuberculosis and typhoid." Infectious diseases that had been controlled in the past have re-emerged in a big way." To add to our woes, we are being warned about emerging infections (like Ebola, Hanta, etc.) even as the World Health Organization observed the 1997 World Health theme: Emerging 3 Infectious Diseases-Global alert, Global response. There has been a widespread perception that the frequent occurrences of public health emergencies like outbreaks are merely reflective of the declining standards of public health in several parts of the country. This phenomenon has been called the 'urban decay'. Pollution, overcrowding, poor quality of drinking water inadequate sanitation, poor garbage disposal and nonexistent vector control are vignettes of this decay. Alarmed by these concerns the Indian Government appointed an Expert Committee on Public Health System in 1995.4 The recommendations of this Committee, needless to say, have gone unheeded. Nothing worth the name has been done about the public health crisis. What ails public health in our country today? What priority does public health receive in our country? This article is an attempt to discuss these issues. Though sarcasm has been liberally used, I have not entirely given up on our ability to respond to common concerns!

Public health-A governmental obligation Have you heard of the A,B,C,D & E strategy used commonly by government functionaries in our country? It is probably the only strategy everyone in the governmental hierarchy uses uniformly, from the Central bureaucrats to the Municipal Sanitary Inspector. This strategy, perfected by generations of government staff, is used to accomplish the goal of not to accomplish anything! A stands for Abandon, B for By pass, C for Confuse, D

for Deny -and E for Escape. These are methods by which the government succeeds in not doing their basic duty to preserve health and safeguard the interests of the citizens. When reports of malarial deaths came pouring in from Rajasthan (during the 1994 outbreak), the government was 5 busy denying it (strategy D in action); when plague was suspected in Surat, the government did precious little to find out whether it was indeed plague the government officials were busy appearing on the TV to announce that 6 there was enough tetracycline in stock (strategy C)! Cholera outbreaks have become a part of our lives but our government machinery, you guessed it, always calls them as outbreaks of 'gastroenteritis'. It may also be worth mentioning that, cholera or no cholera, even chlorination of water is not being done regularly in most towns and cities in our country. We have even had rodent borne 7 infections (leptospirosis outbreaks) in Chennai. Equally worrying is the fact that Chennai City is fast emerging as a major center for urban malaria in India. Chennai city alone accounts for more than 50% to 70% of 8 all malaria cases reported in the whole state. According to the National Malaria Eradication Programme, Chennai 9 is considered a high risk area for malaria. A high risk area is a place which is epidemic prone, or where malaria deaths and falciparum malaria occur. What, one may well ask, has our Chennai Corporation done other than 'oversee' this phenomenal increase in malaria, and, of course, mosquitoes? These days, it has almost become a cliche to say that local bodies do not have the resources to perform their basic civic duty. Incidentally, this is the city where crores are spent on wedding(s)! What is the role of the government in ensuring basic public health for its citizens? By basic public health, I refer to things like garbage disposal, water treatment and purification, adequate 'sanitation, mosquito and rodent control, etc. According to the Indian Constitution, public health and sanitation are State subjects; a Constitutional obligation. On paper, it is the duty of every civic authority to provide basic amenities to its citizens and create a healthy environment to live in. In reality, one look at the functioning of our municipalities is enough to convince us that the raison d'etre of their existence is to perpetuate diseases like cholera, malaria, dengue, diarrhea and a host of other equally horrible

pestilences. By not ensuring even basic public health measures, the government, by the error of omission, is contributing to the resurgence of infectious diseases. Thing have come to such a pass that people have had to file suits against civic bodies for failing to perform their basic duties. Justice Krishna Iyer, in fact called them "mosquito-friendly 10 municipalities" . In this widely acclaimed recent judgement, Justice Narayana Kurup pulled up the Kochi Corporation for its 'lethargy and inaction' in curbing the mosquito menace in the city. "A responsible local body constituted for the purpose of preserving public health cannot run away from its duty by pleading financial inability," the judgement said.'? Years ago, Justice Krishna Iyer had tried to clean up Ratlam municipality by ruling that it was the duty of the civic authority to provide basic amenities to its citizens.' More recently, Justice Kuldip Singh issued detailed instructions on garbage collection to deal with garbage 1 problem in Delhi. Apparently, the only force which makes our civic bodies do the work they are supposed to be legal action. There also, judgments and rulings do not seem to lead to any sustained action. The judiciary in our country, after all, can only advise, it does not have the teeth to implement. We Indians are a tolerant lot. We actually seem to enjoy living amidst filth, squalor and plagues. People dying of eminently preventable diseases like diarrhea and malaria does not seem to raise any concern. The fact that even rodent-borne diseases like plague and leptospirosis have come back does not seem to wake us up from our indolence and indifference! As we approach 2000 AD, the magical year by which countries are supposed to attain 'Health for All', it is well worth noting the fact that only 14% of the 4 Indian population has access to adequate sanitation.

Public health-an individual's responsibility Is our affliction plain apathy or have we completely given up on our civic authorities? Let me illustrate with a story from Vellore. Every year Vellore experiences outbreaks of cholera during summer. One of the worst affected areas in Vellore town as far as cholera (and malaria and heaven knows what else!) is concerned is an underdeveloped area called Saidapet. In this area, water shortage is a chronic problem. People fix hand pumps wherever they can and suck out what little water they can get from the woefully inadequate water system. Needless to say, when they suck out water, they also succeed in sucking out sewage from the sewage lines which run crisscrossing the water lines (it is anybody’s guess as to when the pipes under-

grounds were last replaced!). "There are days when we open the tap and get raw sewage coming from it", said one resident. What has he done about it? ''What can I do, it is municipality that has to look into it," he said with a resigned look on his face. It also goes without saying that not much has been done about the appalling quality of drinking water in this area, despite the annual drama of cholera outbreaks. Our municipal friends find it much easier to go around doing cholera inoculation (knowing fully) well that no vaccine works during cholera epidemics!) To people rather than give them clean drinking water! They in fact believe that they are making a great contribution to public health by inoculating people with a useless vaccine! People, in general, do not like to think about unpleasant things like garbage and sanitation. Envisage this common scenario: garbage in your street has not been cleared for more than a week. What do you do? It is much easier to hold your breath, avert your eyes, and quickly walk past the rotting heap of garbage at the end of your street than raise a noise about it (individuals, apparently, are equally capable of using the A,B,C,D, & E strategy!). As days go by, you keep hoping that the Corporation people will come and clean up the mess. When it becomes absolutely intolerable, and if stress of living has not completely wiped out the traces of civic responsibility in you, you may dash off a letter to the editor of the local paper and hope that someone will do something about your problem. What about the other residents in your street? Do they share your concern too? Chances are many would not even admit that there is a problem! Some will even advise you not to get involved in such issues” what ought to be done by the corporation, should be done by the corporation," they will say. The arm chair cynic next door will say, "let us all stop paying our taxes that will teach them (Corporation)!" Quite obviously, the government alone is not to be blamed for the sad state of affairs we find ourselves in. There are two very fundamental questions here that need answering: Why is public health not a priority for our government? Why is it that people have become incapable of getting involved in common concerns?

A world out of balance To me, the apathy-at the governmental and the individual level-to public health is merely a reflection of the general malaise within the system. Its roots go very deep. We are beginning to see this, if we care to look for it, in many places, in many different ways. We see this when we notice industries poisoning our land, water and air for

immediate financial gains, without any concern for our own future; when we see sky-scrapers surrounded by squalid slums; when the rich exploit the poor without realising that the poor too have a right to survive and there as enough for everyone; we see this when our rulers act with absolute self-interest with no concern for the country or the people; we see this when we stockpile more and more arms when our children are malnourished and starving; when our elected representatives would rather invest money in fancy 'cutouts' than provide people with drinking water; when government officials deny the occurrence of outbreaks than help in investigating and controlling them; when doctors would rather wait for cholera victims to come to them (and make money) than protest against the poor water and sanitation conditions in the community where they work; when municipalities plead lack of funds while money meant for public health is siphoned off by corrupt officials; when yet another scam comes to surface and people say "so tell us something new!"

All this leads me to think that humans are slowly losing the ability to think beyond themselves. Us and Ours is giving way to Me and Mine. The obsession with material progress is threatening to wipe out feelings of sympathy and compassion in all of us. Social problems and common concerns rarely figure in our personal agendas. Those of us who try to discuss these issues (say someone who talks about protecting the environment) are called 'crackpots'. Those of us who feel uncomfortable with aggrandizing wealth are considered 'misfits' in today's savagely competitive world.

What is to be done?

15

expensive modalities of therapy used for the rich and elite, never on simple low-cost health measures for the masses. Almost nothing has been done to address the underlying issues. Needless to say, healths professionals (doctor’s m particular) have rarely take disease prevention seriously. When making money is all important, preventing diseases is akin to killing the golden goose! We also need to realise that we all are a part of ecosystem, sharing one small blue planet, as it were. None of us are Immune to any problem that concerns humans. A dengue outbreak in a tribal area in Orissa can easily reach New Delhi. A cholera outbreak in a slum can easily affect those living in five star hotels. A gas leak in an industrial area can easily spread to the whole of the city. Ultimately, we breathe the same air, drink the same water, and live on the same soil. Pollution and diseases know no barriers (can one think of polluted air in staying put in one place?) -what affects a poor, slum dweller today can easily affect you and me tomorrow! Those who live in duplex condos are as susceptible to pollution and lung cancer as the auto driver on the streets! Issues of public concern are everybody's business. It cannot be left to doctors or the government alone. No one can claim to be unconcerned. And not doing anything is not the answer. To be able to work together for a healthier tomorrow, we need to start caring for those around us. The ability to think beyond oneself and respond to common concerns will be the most crucial next step in human evolution. It may just ensure our survival. References 1.

Dhavan, R, Dung, dengue and death. The Hindu, 8 November. 1996.

There is very little doubt in my mind that if we have to get out of the mess we are in, we need to go back to basics! More money, resources and attention should be focused on basic issues like water, sanitation, garbage disposal, pest control, and civic amenities. Unless these issues are bare necessities for living, they are not a luxury. Every Indian citizen has a right to these basic services. Is it too much to ask for these basic needs when several crores of rupees get siphoned off by corrupt politicians? When one politician can walk away with nearly 1000 crores (enough to provide water and sanitation to many people!), where is equity and justice in this country?

2.John, TJ. Emerging & re-emerging bacterial pathogens in India. Indian J Med Res 1996; 103:4-18.

What has been the response of the health sector to this crisis in public health? By and large, the response has been one of building more and more tertiary, high tech, centers of 'disease care' which few in our country can afford. The emphasis has always been on high-tech,

9. Government of India. Operation Manual for Malaria Action Programme 1995. National Malaria Eradication Programme. Ministry of Health & Family Welfare, New Delhi.

3. World Health Organization. Emerging Infectious Diseases. WHO 1997, Geneva. 4. Government of India. Report of the Expert Committee on Public Health System 1996. Ministry of Health & Family Welfare, New Delhi. 5. Mankodi, K, Political and Economic Roots of Disease. Malaria in Rajasthan. Economic & Political Weekly, January 27, 1996. 6. Anonymous. Surat disease toll put at 27. The Hindu, 24 September, 1994. 7. Ratnam, S. Leptospirosis: an Indian perspective. Indian J Med Microbial 1994; 12(4); 228-239. 8. Malaria Research Centre. Seven Point Action Plan for Malaria Control in Madras City. MRC, Delhi.

10. Krishna Iyer, V.R. Mosquitoes and men. The Hindu, 15 February, 1997.

•

BACKGROUND PAPERS FOR THE MFC ANNUAL THEME MEET, 1 TO 3 JANUARY, 1998 CONTENTS • Resurgence of Infectious Diseases : Public Health Responses • Agricultural Development & Japanese B Encephalitis • Kala-Azar Since 1977 • Resurgence of Infectious Diseases : Crisis in Public Health

Author T Jacob John J Elamon P Chatterjee M Pai

Page

1 5

10 13

The Medico Friend Circle (MFC) is an all India group of socially conscious individuals from diverse backgrounds, who come together because of a common concern about the health problems in the country. MFC is trying to critically analyse the existing health care system which is highly medicalized and to evolve an appropriate approach towards developing a system of health care which is humane and which can meet the needs of the vast majority of the population in our country. About half of the MFC members are doctors, mostly allopathic, and the rest from other fields. Loosely knit and informal as a national organization, the group has been meeting annually for more than twenty years. The Medico Friend Circle Bulletin, now in its twenty first year of publication, is the official publication of the MFC. Both the organization and the Bulletin are funded solely through membership/subscription fees and individual donations. For more details write to the convenor. Convenor's Office: Vijay Jani, 34 Kailash Park Society, Near Water Tank, Akota, Vadodara, Gujarat-390020. Editorial Office: Sathyamala, B-7, 88/1, Safdarjung Enclave, New Delhi-l10029.

Life Subscription Rates: Inland (Rs.) Annual 500 Individual 50 1000 Institutional 100 100 Asia (US $) 10 150 Other Countries (US $) 15 Please add Rs. 10/- for outstation cheques. Cheques/money orders, to be sent in favour of Medico Friend Circle, directed to Manisha Gupte, 11 Archana Apts., 163 Solapur Road, Hadapsar, Pune-411028. Edited by Sathyamala, B-7, 88/1, .Safdarjung Enclave, New Delhi-110029; published by Sathyamala for Medico Friend Circle, 11 Archana Apts., 163 Sola pur Road, Hadapsar, Pune-411028. Printed at Colourprint, Delhi-110032.

Views and opinions expressed in the bulletin are those of the authors and not necessarily of the organisation.


