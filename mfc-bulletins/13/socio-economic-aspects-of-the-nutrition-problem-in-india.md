---
title: "Socio-Economic Aspects Of The Nutrition Problem In India"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Socio-Economic Aspects Of The Nutrition Problem In India from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC013.pdf](http://www.mfcindia.org/mfcpdfs/MFC013.pdf)*

Title: Socio-Economic Aspects Of The Nutrition Problem In India

Authors: Jaya Rao Kamala S

medico friend circle bulletin

JANUARY 1977

Malaria Eradication Programme: Its genesis ANIL .B. PATEL

A

T the very outset I want to make clear that there is a strong component of conjecture in my account of how, when and why the strategy of malaria eradication was conceived on a global scale. The dull official accounts cannot and do not reflect the drama and excitement that invariably accompanies such momentous decisions. I hope readers will appreciate the plausibility of my account. National Malaria Eradication Programme (NMEP) was launched in 1958. Before 1958- we had nation wide National Malaria Control Programme (NMCP). The results of the control programme were spectacular by any standard. From 75 million cases per year in 1953 the incidence had dropped to 2 million in 1958 (Park 1970). Why was then eradication decided upon? There are many reasons for this switch (Pampana 1963). The WHO recommended malaria eradication rather then control as an international objective in 1955. There'

were other developments occuring in a very quick succession. The discovery and wide scale use of DDT proved to be almost an ideal insecticide. Incidence of malaria dropped dramatically as it did in India. As problem of malaria started receding, malariologists everywhere felt a genuine fear that governments pressed by other demands might start cutting on malaria control budget. This would certainly lead sooner or later into unmanageable epidemics breaking out in the most populous parts of malaria regions. Meanwhile a little short of miracle was reported from Greece. Greece had embarked on the ambitious project of the eradication of the vector of malaria with the help of domestic spraying of DDT to eradicate malaria. After spraying all the houses with DDT for about five years the operations had - to be interrupted for the

Go to the people

*

lack of adequate supply. The vector returned very soon but malaria did not appear again! There was also another ominous development in Greece. The vector of malaria had become resistant to DDT. The deepening knowledge of epidemiology was now in a position to explain this strange happening and also to provide a sound theoretical foundation on which the grand strategy of global eradication could be based. With this background the malariologists advanced most important and persuasive argument in favour of eradication programme. (The cost-benefit argument, Pampana 1963). They were convinced that though the eradication programme involved heavy capital expenditure initially, its overall cost would be much smaller in comparison to the costs in control programme in the long run,' which would involve recurring expenses without any time limit. This major component of the elaborate argument has been recently subjected to critical scrutiny (Cohn 1972). Cohn has raised very fundamental and pertinent issues. The first and foremost being the appropriateness and the correctness of the methodology of cost-benefit analysis adopted by malariologists itself. To quote him' "One of the major deficiencies in economic assessments that came _ to my attention.... and which I would like to stress here is the fallacious practice of assuming that costs (and benefit) today and at: different points in the future are of equal value." Cohn has actually shown by using the method he _ considers appropriate that the control programme for 30 years would have been cheaper than eradication programme of 10 years. He has also argued quite convincingly that the benefits always to be computed in terms of money in cost-benefit analysis! — are * 1 Stradella Road, Herne Hill, London S.E. 24.

extremely difficult to compute and far from obvious. Also benefits accrued must be balanced by the gains foregone had the resources been employed differently. The major argument thus out of way there remains two other important considerations for the change in the strategy. The first is about the vector resistance to insecticide. However at the time when global eradication programme was adopted there was very scanty' information regarding the nature and the scope of the problem. (Macdonald 1965). Could it spread further in space and time? There was no firm answer to this and such many questions. After about two decades of eradication programme WHO is now in a position to declare that though serious in localities where resistance has emerged, its global significance is Dot so much as to endanger the strategy. Incidentally it is interesting to know that great proportions of insecticide resistance seen today are product of agricultural use of insecticides, and not due to its use for public health purposes. The second consideration is that of feasibility of eradication. This brings me to my conjecture. This term 'feasibility' does not bring out very well the emotional, psychological and human factors that were very much the part of decision making process. To appreciate this rich cocktail of logic we have to cast our minds back into the forties and the fifties. Epidemiology of malaria was getting much more refined, quantifiable and therefore permitting very accurate predictions (Macdonald 1957). This provided a great theoretical tool with which to manipulate the disease situation on an unprecedented scale covering the whole continents. This was possible in practice if and only if an ideal material mean was also made available. And believe it or not there came along almost by miraculous coincidence the greatest of all insecticide DDT!! The generations of malariologists who had dedicated their lives to fight malaria and who had given the status of Public Health Enemy No. 1 to malaria this was a girt from heaven! These scientists-malariologists were after all the products of the great optimism of the 19th century, the cardinal principle of it being the inevitable triumph of science over nature. The intoxicating effect of the theory and tool was so great and the excitement to test the new theory was so overwhelming that discretion had to take a second place. There were to be sure a body of opinion which saw malaria not as an isolated disease' entity, but as an integral part of the whole disease complex which was in turn deeply embedded in the total socio-cultural matrix of the

affected societies. Malaria Eradication could not and even if it did, it would make very little impact on the overall health status of the community. For the supporters of the eradication programme this was unwarranted pessimism. For did not Sinton, whose knowledge of malaria in India was extensive and intimate, say "There is no aspect of life in that country which is not affected, either directly or indirectly by the disease. It constitutes one of the most important causes of economic misfortune, engendering poverty, diminishing the quantity and quality of food supply lowering the physical and mental standards of the nation and hampering prosperity and economic progress in every way" (quoted by Russel et al 1963). . Malaria as a single most important cause of socioeconomic ills was clinched very eagerly. In the frame work of such reversed cause-effect relationship the technological solution to problem of malaria fitted very neatly. This simple, purely technological solution: to immensely complex problem was not only the reflection of incredibly naive optimism, and irrepressible scientific curiosity but also the reflection of ostrich like tendency of not seeing the unpleasant reality and wishing that it would disappear some how. In the event the reality has not disappeared. The underlying socio-economic problems have asserted their primacy. They can no longer be wished away. The great setback in the eradication programme was inevitable and now we are facing the grim prospects of nationwide occurrence of malaria epidemic. What is to be done now?

References 1.

2. 3.

4. 5. 6.

Cohn E.J. (1972) Assessment of Malaria Eradication Costs and Benefits. American Journal of Tropical Medicine and Hygiene 21, 5, 663-667. Macdonald G. (1957). The Epidemiology and Control of Malaria, London. Macdonald G. (1965) Eradication of Malaria: Dynamics of Tropical Diseases (1973) Ed. by L.J. Bruce Chwatt and V.J. Glanville P. 240. Pampana. J. (1963) A Textbook of Malaria Eradication. London. Park J.E. (1970) Textbook of Preventive and Social Medicine. India. Russel P.F., West L F., Manwell R. D and Macdonald G. (1963) Practical Malariology, Oxford University Press, London. P. 149.

News •

MFC regional camp was organised at Kishore Bharati from December 21 to 24.

•

A survey camp was held on December 28-29 to help planning health programme in a village nearby Friends Rural Centre, Rasulia.

Live among them

Editorial When we decided to give new configuration to Medico Friend Circle Bulletin at second all India Meet of MFC in December 75, we had some apprehension about its feasibility. We decided to bring out a printed bulletin instead of usual cyclostyled one. It was envisaged as a forum for the exchange of experiences and views among those who are genuinely concerned with health of the masses. It was-to motivate and involve readers sitting on fence. It also aimed to fill the lacunae in our knowledge and information' about important issues and aspects in the field of health. Experience of the last year proved to be inciting as well as limiting. Number of friends, not only welcomed the effort, but encouraged communicating fulfillment of their long awaiting wish. These friends were mainly from teaching institutions or from the field of community health, from home as well as abroad. They still appreciate the content and direction of the bulletin. But there is a sizable group of friends who look at the bulletin with admiration, but feel it beyond their reach. They found it too academic and unrealistic. They felt" the articles were informative, but were dealing analysis exclusively at macro level. They emphasised that problems should be analysed also at micro level and .solutions should be sought at micro level as well. Their' problem "is genuine. One of the deficiencies last year was absence of participation from field workers. Unless they pose their problems, experience and analysis, it will be difficult for others to appreciate and understand their problems. This requires an active dialogue rather multilogue among friends from various disciplines. It was realised by" editorial committee that it is beyond its capacity and scope .to reach the common man through this bulletin. But we must make it as simple in its contents and presentation as' possible so that second year medical students, diligent health workers and interested friends from other disciplines like agriculture, veterinary, education, etc. can understand it. MFC realises its responsibility to mobilise public opinion in right direction by providing correct information and critical analysis. We decided, therefore, the members will write relevant articles in local newspapers and journals. We failed to get, with exception of one, articles in Hindi. We therefore, request authors and readers to write in Hindi for the bulletin, if they can. To solicit the interest of friends from allied fields and to make the bulletin comprehensive, a new column will be added.

Love them

Inspite of frequent efforts, readers' participation could not be elicited. Besides all these, we faced financial hardships to publish the bulletin. Therefore we, earnestly request the readers, not only to send their subscriptions, but also to enroll substantial number of new subscribers so as to help their own bulletin. It is very difficult to find any link among various articles appeared during the last year. This is applied to both, leading articles as well as others. Moreover, readers' letters were also varied. There was neither any debate nor dialogue which showed continuity and consistency to reach to a set of conclusions on a given problem, There were diverse views on variety of topics. This diversity existed because of lack of any attempt on part of the editors to impose a pattern of thinking or unified outlook. There is no specific standpoint or ideology taken by the bulletin. In fact the bulletin's editorship is anarchist one, everybody has a free run. Even the length of articles varied from that of epigram to monograph!! Out of - this anarchism and diversity, does any picture' emerge? I am afraid inspite of this, the bulletin is taking up some, shape. The thing we readers authors, editors- should do is to envisage and anticipate the form. We not only wanted the true knowledge and critical analysis of what is going on; but launched the effort to help ourselves to find an alternative approach to health services which should be linked to human attitudes and values, which differ in different communities, and should require a clear motivation and commitment on the part of the people who have the knowledge and the political and/or economic power to introduce change. How far the bulletin has helped us to grow in correct direction? How best can it help us to initiate and perpetuate the change? —Ashvin J. Patel

Proceedings of the 'Third All India Meet of MFC Report-A

THE MEET The third all India meet of Medico Friend Circle was held at Rasulia, near Hoshangabad on December 25, 26, 27, 1976. Friends Rural Centre, a rural comprehensive development project at Rasulia, hosted the meet. Over 70 persons participated in the meet. They included practising doctors and vaidya, medical students and teachers, research scholars and scientists, nutritionists, veterinary students and doctors, agriculturists, engineers, statisticians, physicists, artists, journalists, and social workers from various parts of India. Among them were also representatives from 10 different field projects. Sudarshan Kumar Kapoor; the Coordinator of Friends Rural Centre, welcomed the participants and expressed privilege for hosting the meet. Lalit Khanra from West. Bengal gave a small introductory talk about MFC and background of the meet. Then participants introduced themselves one after another. This helped participants to know each other's background, interests and aspirations. We divided ourselves into four groups to discuss the working papers. First working paper, prepared by Suhas Jaju in Hindi, was on analysis of present day health system which posed questions about three areas- (1) relevance of present day medical education, research and care; (2) priorities in our health problems and needs; and (3) possible solutions to meet the problems. (For detail see Report- B). Second paper on socio-economic aspects of nutrition problem in India prepared by Imrana Qadeer was discussed again in four groups (see Report-C). This was the main theme of the meet. To facilitate the discussions, two experts in the field of nutrition were invited to give talks. Prior to discussion on the second .paper one of them, Kamala S. Jaya Rao defined the problem of protein calorie malnutrition, undernutrition, vitamin A deficiency and iron deficiency. She also explained the magnitude and nature of these problems. She shattered the myth of protein gap and emphasised the problem of food gap. After and during the talk number of fundamental issues questioning the premises defining and determining the nature and magnitude of the problem were raised. Similar questions challenging the high magnitude of malnutrition were posed by Prof. P. V. Sukhatme, a scientist of international reputation in field of statistics and nutrition. He tried to prove through statistical tools that· the figures of the magnitude of

undernutritions were exaggerated. He inferred that some 20% of the people in urban areas of the country were undernourished and not 50% as usually quoted and for rural areas the incidence was less than 20% against the estimate of 40%. The high figures are due to the tendency to ignore (a) the knowledge we have gained in understanding the concept of dietetic requirement and (b) the evidence that the human possesses a physiological regulatory mechanism which serves to maintain fixed body weight and energy balance over extended period. The responses to his proposition were mixed. Inspite of his best efforts, a good number of delegates could not understand him. These delegates felt that it was too academic for them. They were baffled by the statistical methods he used to explain his point. They also felt that the lecture was not related in anyway to the subject of' socio-economic aspects of nutrition problem in India. For them talk went over their heads. A few delegates were incited to understand the process of the inferences and the statistical tools he used. For them this was a very vital issue. So additional 1½ hour was devoted for discussing the issue with Prof. Sukhatme. Questions were showered on him and the problem was discussed with equivocal outcome. Three hours were given for the presentation of field projects in which certain participants were involved. Ten such projects were presented and discussed in common session. The last day was devoted fully for the organisational decisions. Ashok Bhargava gave a brief report of central cell and. other activities. The salient points were: 1. Annual meet was held last year at Sevagram and four regional camps were organised in Kerala, Gujarat and Madhya Pradesh. 2. Vadodara and Calicut groups commenced community health projects. . 3. Few camps were .organised in cooperation with local panchayat in Trichur district of Kerala. 4. A printed bulletin was started: instead of cyclostyled one, and published regularly. Involvement of readers as well as some of the members of editorial committee was poor. Bulletin was found more academic and high sounding. It could have been simple in content and language. 5. A number of persons and .institutions have shown interest in bulletin and organisation and offered their cooperation. 6. Financial hardship was felt throughout the year. More important than financial aspect was the

Serve them

lack of personal commitment on the part of members to devote their time and energy to consolidate the group and activities,

ORGANISATION OF MFC

f. picture, show, drama etc.

Structure, bulletin and budget 1.

Though until now MFC had existed as a loose and unregistered organisation, it was felt by the group that the organisation should now be registered under the Societies Registration Act 1860. For this purpose an eight member committee was selected consisting of following members responsible for preparing and signing the memorandum and registration of the organisation Ashok Bhargava, Navnit Fozdar, Ashvin Patel, Satish Tibrewala, Rajeevraj Chaudhary, Sudhir Shah, Monghiben Mayatra and Kartik Nanavati.

2.

Ashok Bhargava will continue to act as the Convener of the organisation for one more year.

3.

Membership dues were revised as Rs. 40/- per year for members earning more than Rs. 500/per month and Rs. 20/- per year for others. Membership fee includes the subscription of the bulletin. It is understood that capable members should pay more than this minimum limit and that the Convener can waive or reduce this fee in deserving cases.

4.

About acceptance of monetary help from other sources such as pharmaceutical companies, voluntary agencies etc. many delegates felt no harm in accepting such aids in acute circumstances provided it does not lay down any moral obligation upon the organisation. However it was agreed that MFC should try to become self sufficient in all its financial aspects.

5.

In order to make MFC bulletin self-sufficient, a subscription drive will have to be launched by the members and the Editorial Committee.

6.

An anthology containing the selected articles published in the bulletin till now shall be published during the coming year.

7.

The Hindi translation of the MFC pamphlet shall also be published.

8.

The General body did not see any objection for the reproduction of the articles published in the bulletin in any other magazine without making any changes, distortions, deletion or addition in the content of the original article with due acknowledgement, The translation of the articles may also be published similarly.

After the discussion on this report the house was led to decide the future scope, functions and structure of MFC. Having known the aspirations, resources, commitment and past experience we made following decisions:

Scope Considering the objectives of MFC set at Sevagram Meet, scope of MFC was broadly divided into four categories: a.

to provide a critical evaluation of present health system.

b.

to explore and evolve an alternative approach in terms of health care, research and training,

c.

to keep in contact with and integrate non-medical aspects of development like education agriculture, appropriate technology, environment etc. so as to induce the health projects to take up such programmes, and

d.

to try to mobilise public opinion regarding above.

Functions a. b. c. d.

Taking in view the above scope we have broadly four functions: to promote field work, feed back to and from the field projects, mass education, and critically evaluate present health system through individual and group study and discussion. Feed back may be in following areas:

a.

in conceptualization and planning the programmes of project,

b.

providing information about resources which exist,

c.

providing critical evaluation through bulletin, workshop and conference,

d.

involvement and placement of volunteers with short term as well as long term commitments for field work, and

e.

to help preparing working material for health education, survey and evaluation. Following ways were thought appropriate for mass education: writing articles in local newspapers and magazines, publishing pamphlets, keeping personal contacts with people in project area, communication with patients in hospitals, publishing books in simple language,

a. b. c. d. e.

Learn from them

The Editorial Committee was also selected. It the consisted of Imrana Qadeer, Kamala Jaya Rao, Mira Sadgopal, Lalit Khanra, Ashok Bang, Anant Phadke, Ashvin Patel (Editor). 10. Last year's income and expenditure were reviewed, 9.

and the budget for the year 1977 was agreed to as follows: Expenditure 1976 1977 (expected) Annual meet 438.36 1000.00 Printing 352.25 2500.00 Stationery 218.23 500.00 Postage 92.55 300.00 Traveling 500.00 1500.00 Bulletin 5418.94 7000.00 Convener's honorarium 1800.00 Contingencies 500.00 Deficit of year 1976 601.33 Total

7020.33

Income 1976 Membership 1900.00 Subscription 750.00 Contribution from members 1215.00 Collection by groups 2554.00 Sell of Anthology Registration fees of meet Total

6419.00

15701.33 1977 (expected) 2000.00 2000.00 1500.00 7500.00 2000.00 500.00 15300.00

Evenings of the meet were devoted to music. Some local artists and participants of the meet presented classical, semi-classical and folk songs. Among all spectacular was Upendra Vedpathak's harmonium which attracted a group of participants till 2.30 a.m. on the last day of the meet. The group had a very, live time for the three days at Friends Rural centre. The discussions were all held in vicinity of beautiful red roses in the garden under a tree. While the group had a privilege of dining under a big tree, freshly picked grams were also served in the midst of discussions which helped as a booster dose to the tiring delegates. The arrangements for accommodation of the delegates and the courteousness of the hosts were most appreciated among all other things. —Nimitta Bhatt

Report B

*

Report of the discussion on paper

Health Services: An Analysis The central aim of the paper written by Suhas Jaju in Hindi was to raise questions about the relevance of the present health system in relation to the needs of the people. It contrasted the aims set b, the Bhore Committee with the performance of

the present health services. Thus for example, it was suggested by the Bhore Committee that every PRC should have 3 doctors, 20 nurses, 31 assistants, 175 beds and that it should cater to a population 'Of 20,000. But today we have one or two doctors, only 8-10 beds per PRC and about one lac population to be looked after. There were 143 PHCs which had not a single doctor. All participants agreed that this gross failure is of serious concern. Some other questions were raised in this connection. It was asked as to why these recommendations could not be implemented. It was argued that most of the health budget was spent on medical colleges and city hospitals because those who were at the top of these institutions were also those who controlled the health budget. These people are more bothered about enlarging their own institutions and hence deriving more benefits for themselves through such expansion than about the health of the rural poor. There are some exceptions to this general pattern, but the direction of expansion is on the whole misdirected. It was pointed out that the only recommendation fully implemented was that of expansion of medical colleges. Another question raised in the discussion about the Bhore Committee, was— "If those at the head of the decision making process are urban oriented, hospital oriented, then why did they appointed Bhore Committee at all?" It was felt that a possible answer could be that health services in rural areas were extremely inadequate in relation to the raised expectations of the masses, thereforesomething had to be done about rural health services. Secondly, a very high incidence of diseases like malaria was economically damaging and would have an adverse effect on the process of industrialisation. One of the participants argued that it was more out of concern for the wastage of human resources which was unfavourable for industrialisation, than out of concern for the sufferings of the rural poor that a health programme for rural areas was planned. Discussion then proceeded to the point that if a fresh graduate goes to work in the community, he feels quite helpless when he sees that the main determinant of the health of a people is economic backwardness which medical science can not remedy. Moreover today's medical graduate find himself totally unequipped to face the medical problems outside the hospital or urban setting. It was pointed out that ignorance and misconceptions about health are also important obstacles in improving the health of the people. One participant

Start with what they know

pointed out that patients themselves are more bothered about injections, tonics etc. rather than preventive aspects of health. It was argued by others that this was bound to be so, and that the drug industry and doctors themselves are responsible for this state of affairs. If doctors make a systematic effort at educating people in those matters then picture’ would definitely change. It was also pointed out that education of the people in preventive and community aspects of health can be done only if it is combined with curative aspects and if the doctor wins the confidence of the community in which he works. All participants agreed that .though poverty of the people does not leave much scope for improving health of the people, curative oriented, hospital oriented education makes the doctor more helpless in tackling the health problems of the community in which he works. There were a number of facts enumerated in the paper, about communicable diseases accounting for 54% of deaths in India, about lack of supply of safe drinking water to most of the villages, about high degree of protein-calorie malnutrition in preschool children, high incidence of infant and maternal mortality, about the ills of drug industry, about gross neglect of rural areas, about wrong priorities and allocation of funds in health sector itself, etc. etc. There was no disagreement about these facts arid priorities, so not much discussion took place on this part of the paper. As regards the population problem, it was agreed by all the participants that .the so called population explosion was not the real cause of poverty and. that increase in population will be controlled if general socioeconomic situation improved. Since some detailed discussion had already taken place in the bulletin, this point was not discussed in detail. It was mentioned in the paper that there were thousands of practioners in India, of disciplines other than allopathic, and it was asked whether their services can be integrated by giving them some training and.' orientation. It was asked as to what kind of training should be given to these practioners. It was suggested that firstly many of them would need to be trained in their own discipline because they are not adequately familiar with their own discipline. This posed problems about -the Ayurvedic and Unani disciplines because standardized norms and methods of education were poorly developed in these disciplines. Secondly it would be necessary to find out whether these disciplines have developed preventive and community aspects of health. If not, then these

Build upon what they have

practioners will have to be taught preventive and community aspects as developed by the allopathic discipline. All participants agreed that our education and health services were not much related to our economic, social and cultural atmosphere, that such education hardly fulfills the functions it is supposed to. It is pointed out that undue emphasis is given to curative aspects of health-care. Moreover disorders like mitral stenosis and organ transplant take precedence over leprosy and primary obstetric care. Though more than half of the patients in any dispensary are from paediatric age group, paediatrics occupies a very small proportion of the medical education. Preventive medicine department is looked down upon, and very few persons in that department are really interested in their own department. One important reason as to why the PSM department is looked down upon is- that doctors working there are not likely to earn large sums of money. Today money and prestige go together! The last part of the paper was causes and remedies of the problem. Due justice could not be given to this aspect during the discussion because it was a very difficult topic involving many aspect of social reality. Secondly very little time was left for this discussion. Some of the reasons for these maladies were covered during discussion on the earlier part of the paper. Participants agreed that it was partly because of a blind imitation of the west, partly because of the vested interests of those who controlled the economic resources. These factors were felt to be mainly responsible for the gross neglect of rural health services. As far as solutions to these problems were concerned, it was felt that not much could be done without a major social change. But measures like reducing the lengthy course to a smaller one in which essential aspects necessary for training of multi-purpose basic doctor and health worker would be helpful. Similarly giving more stress on preventive and community aspects of medicine; and less stress on education within the hospitals, integration of various disciplines in the medicine and increasing the health consciousness and participation of the people, abolition of brand-names and provision of cheap drugs etc. would be helpful. It was felt that more study and practical experience are needed to evolve an alternative approach towards healthcare of the population. — Anant Phadke

Report-C Report of the discussion on the paper Socio-economic aspects of the nutrition problem in India Social analysis of health problems can be meaningful only if it is based upon sound knowledge of the underlying scientific principles and t¥ nature of health problems in a given situation. The nutrition problem in India is one which demonstrates the need to understand the science of nutrition and also to understand the socio-economic realities which have an significant influence on the nature of the problem. In this context, the paper was prepared by Imrana Qadeer, to discuss the role of the following factors on the nutrition of a large section of the country: 1. Income distribution and buying capacity. 2. Capacity of food production and distribution of food. 3. 4.

Environmental conditions. Cultural factors.

5.

International dimensions of the nutritional problem.

6.

A proper solution to the problem.

The question of income distribution and its ramifications could not be discussed satisfactorily since many of the participants did not' know much of this aspect. It was therefore requested that relevant figures (as recent as possible) be published in the MFC Bulletin. Generally though groups with higher incomes tend to spend more on food, in a particular group the rise of income has no impact on food consumption pattern. People tend to spend the extra income not on food but on items like clothes, transistors, social events and on gambling, alcohol etc. This shows the greater influence of social pressures than of health and nutrition. In the context of production capacity, the stratification of agricultural families into the land holders and landless was discussed. Free wage earners are not just those who are not in bonded labour. The meaning of the term freedom should be studied in its true sense. Freedom is meaningful only to those who have the opportunity and means to exercise it. This involves freedom to grow food, to distribute and utilise it. The availability of inputs like seeds,

irrigation and fertilizers, the growing production of cash crops, land holdings and land redistribution, procurement prices affected production and distribution. The predominance of vested interests and the apathy and helplessness with which the poor farmer accepts his status in society also contribute to the maldistribution of food. It was agreed by everyone that environmental factors like poor sanitation and prevalence of infectious diseases contribute to malnutrition. The role of socio-cultural factors on nutritional status was discussed. Many traditional concepts regarding food and nutrition exist in the community. It must be realised that these have evolved over several generations. They should not be totally condemned without critically examining their beneficial effects or ill effects on health. The tradition of breast feeding is being replaced by bottle feeding in urban areas. The ill effects of the - latter due to feeding inadequate milk formulas and unhygienic way in which this is done is proving detrimental to the nutrition of an infant. The strong influence of large commercial establishments on doctors and on the community in promoting expensive milk foods and weaning foods needs to be realised. The practice of unnecessary use of commercial foods, when equally nutritious and less expensive natural foods are available, needs to be challenged and weakened. The role of doctors in this is very crucial. It is also seen that urbanisation leads to the use of expensive luxury foods like biscuits, bread etc. and of foods of negligible nutritional value like carbonated beverages etc. In trying to understand the food and nutritional problems of a country, international influence should also be understood. The exploitation by colonial powers and the pressures of the international market and the emergence of neo-colonial power respectively are important factors which determine why a country was not self-sufficient in food and why it is unable to achieve self-sufficiency. How can the nutrition, problem of the country be solved? Various solutions like green revolution, production of protein rich foods, nutrition supplementation programmes have been offered time and again. It must be realised that to obtain a proper solution, the social realities of the situation should be truly and properly understood. Transporting a model developed in one country (like green revolution) to another country can never be a satisfactory solution. International aid can only be a self-defeating element and can never solve the national nutritional problem. In short therefore, the socio-economic factors in the community are very important in the development and. perpetuation of malnutrition in the country and this also determines the planning of an appropriate solution. —Kamala S. Jaya Rao

Editorial Committee: Imrana Qadeer, Kamala Jaya Rao, Mira Sadgopal, Ashok Bang, Anant Phadke, Lalit Khanra, Ashvin Patel (Editor)

Edited and Published by — Ashvin J. Patel for Medico Friend Circle, 21 Nirman Society, Vadodara-390005. and For Printed by him at Yagna Mudrika, Vadodara-390001, INDIA, on 24-1-77. Annual Subscription Inland- Rs, 10/- For U.K. by Sea Mail £ 4/ by Air Man £ 51/-; for U.S.A. &: Canada by Sea Mail $ 6/- by Air Mail $ 9/-


