---
title: "The Sevagram Meeting"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled The Sevagram Meeting from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC105.pdf](http://www.mfcindia.org/mfcpdfs/MFC105.pdf)*

Title: The Sevagram Meeting

Authors: Medico Friend Circle

105 Medico friend circle bulletin September 1984

NATIONAL TUBERCULOSIS PROGRAMME : Some Problems and Issues:* Binayak Sen** “Why is it that even though the felt-need oriented National Tuberculosis Programme is in operation in the country for over tow decades, more than 80 % of infectious tuberculosis patients are still being turned back at various health institutions with nothing more than a bottle of cough mixture?” — D. Banerji, EPW, 22 Jan. 1983

interruption of bacterial transmission. To this end, it defines a ‘case’ of TB as person excreting tubercle bacilli in his sputum. This approach is unscientific because it is only at a much later stage along the exponential curve of falling prevalence that the interruption of transmission becomes ever a remote possibility. It also ignores the fact that never in the history of human TB has a reduction in transmission been brought about by a specifically medical intervention. As a result of my four years experience of working in voluntary institutions participating in district and in Durg – in Hoshangabad this approach works in practice. A person who presents himself a t a Public Health Institution with symptoms suggestive of TB is not regarded as a person suffering from disability and consequently in need of help, but simply as an entity to be categorized, i.e., TB or not TB. After a cursory physical examination he is sent for a sputum test. If he obliges by producing a positive sputum, that is the end of the matter. He can then be placed on a standard treatment regime (generally INH

1. Conceptual Problems In their seminal 1962 paper on symptom awareness in tuberculosis, Banerjee and Anderson, reemphasized the problem of tuberculosis as a problem of human suffering, and outlined a strategy for TB control based on this concept. This strategy, abjured a policy of active case finding. Instead, it concentrated its attention on greater diagnostic sensitivity towards and adequate treatment for those people suffering from symptoms suggestive of TB who presented themselves at the existing hospitals and clinics. Together with the Madras Chemotherapy Centre study on domiciliary treatment, it forms the theoretical basis of our present day TB programmes. The credibility of this system rests on the adequacy with which the entire range of presenting adoption of this approach would, therefore, be the development of an integrated and well-defined system for tackling the entire range of TB symptomatology. Instead, the National TB Programme has set its sights on a Mirage – the

1

* A note prepared for the mfc core group meeting (July ‘84) at Wardha ** CMSS, Dalli Rajhara, M.P. —41228

and Thiacetazone daily) and forgotten about. Once in a way his sputum may be checked but the treatment with INH and Thiacetazone, being continued on the same drug. When challenged, the government available”. In point of fact, in practice this is often true.

remember that the Madras Chemotherapy Centre study on domiciliary treatment had weekly home visits as part of their protocol. It is a great pity that this investigation has formed the basis for a programme that thinks it sufficient to throw some tablets once a month at a desperately sick man.

But, we will come to problems of chemotherapy later. The point I am trying to make is, that from the point of view of a desperately sick man, frightened by a dreaded diagnosis, it is could comport to be given 30 tablets and told to come back again after a month’s treatments, and assured that he will get well in 18 months time. This is particularly so since there are doctors at every street corner assuring patients (with considerable honesty) that they will get well with some private treatment in six months or less.

2. Primary TB and extra-pulmonary TB Treating the problem of TB as a problem of suffering people, rather than as a problem of successfully eliminated parasitic myco-bacteria. Brings us to two sets of illnesses often neglected in the current programmes, viz.

a. Primary Tuberculosis Between 10 &20 percent of Indian children are tuberculins sensitive by the time they are five years old, though some surveys (Raj Narayan) yield a lower estate. The popular (medical) conception of primary tuberculosis is of a mild undercurrent illness that is only incidentally detected in a chest X-ray and attains clinical significance only in the ‘progressive’ form. This is not true in malnourished children not only is infection itself accompanied by significant morbidity, but it is the ‘interaction’ between infection and nutrition — that is the factor that needs to be considered. When we consider that, according to ICMR, 65% of Indian children are malnourished, the dimension of the problem becomes a little plainer.

Let us now come to the case of those who were sputum negative. The coast of a free MMR X-ray from Durg to a person in Rajhara, is well over Rs. 50.00. The cost of a local private X-ray is Rs. 35.00. which should the patient choose? It should be noted that I have been talking all along of the ideal case. We have not taken any account of the government doctor nudging the patient towards his private clinic; the laboratory technician asking for his ‘fee’; the X-ray technician’s rudeness, or the irregularity in drug supply. The patient of TB is basically a suffering person. It is the least of his concern that he is excreting M TB in his sputum. What he is much, more worried about is the fact that he has cough, chest pain, fever, body ache and nausea. He can’t work. He feels weak. He loses his sexual potency. His children starve and often fall ill in their turn. A physically distant and emotionally remote health centre can offer him nothing. It is well to

It is a common misconception (even, as I have discovered, among TB ‘Specialists’), that clinically apparent primary TB can safely be treated by a short course of INH alone. This is a notion that goes against all bacteriological logic. One only creates a population of INH resistant bacteria, strategically situated to subsequently produce reactivation disease.

2

b.

Extra Pulmonary Tuberculosis

4.

The chapter on Epidemiology in the Text Book of TB (by the TB Association of India) has nothing to say about extra pulmonary disease. In my experience this forms a significant proportion of cases of TB. In particular, ‘scrofula’ (burnt out tuberculous cervical lymphadenitis) is still a common finding in backward areas of the country. 3.

Chemotherapy

a. Existing patterns In theory, the National TB programme provides a wide choice among several alternative regimes. These include, daily INH and thiacetazone with or without an initial period of intensive treatment with daily streptomycin and / or PAS. The bi-weekly supervised regimes consisting of INH/SM and INH/PAS, have been designed specially to ensure patient compliance.

Staff Problems

Cases of ignorance among people working in the field of tuberculosis are not rare. This is because almost the entire field level medical staff of the Staff Problems programme are ‘dead-beats’ – people who have been promoted to an administrative position because their seniority has become an administrative embarrassment.

Even according to the treatment manual supplied to the district Tuberculosis Officers, only sputum positive patients are eligible for all these regimes. X-ray positive, sputum negative patients often just as sick as their ‘positive’ brethren and about 5 times an numerous, are eligible only for the daily, self-administered INH/TH regime. Presumably compliance is not a consideration where they are concerned.

In a Government District Hospital, despite all the other problems, one can at least meet doctors who are interested in their work in the medical, surgical, gynaecological and other specialist departments. Not so in TB. The department which should, by all epidemiological logic, claim the most brilliant and dedicated of our technical manpower, is invariably academically dead. In hoshangabad, the District Tuberculosis Officer was simply absent for a long period of time.

In actual practice, the only regime available with any regularity is daily INH/TH. (Incidentally, pyridoxine tablets necessary to counteract INH induced pyridoxine deficiency are practically unheard of. Patients are told to eat lots of peanuts! ) PAS I have not seen in the past one year. Streptomycin is constantly in short supply, so that patients are often randomly shunted back and forth between regimes containing SM and those without. The effect of such regime changes in ‘midstream’, on treatment effectivity, bacteria sensitivity, and patient compliance remains, as they say, a subject for research.

The Para-Medical staff, on the other hand, are often exceptionally dedicated and able. They often run the programme practically independently. However, they have to pay the price for their competence. In Durg, the statistical assistant –- a key person and in this case extremely competent and dedicated – has been on full time deputation to the Civil Surgeon’s office, helping to administer the hospital.

Coming to the INH/TH regime, TH is by no means an uncontroversial drug. Its use is banned in some countriesbut let that pass. The incidence of ‘major’

3

toxicity in a study in Madras showed the following incidence of side effects:

not known to me. But the success rate of the standard first line treatment regime is of the order of 80 to 85 percent, under ideal conditions.

Cutaneous hypersensitivity reactions – 7%; Jaundice – 3% Intractable vomiting – 3%

(2) There is evidence to show that pre-treatment drug sensitivity tests do not affect the outcome of treatment, provided standard two phase regimes are used, with an initial intensive phase using three drugs. However in my experience, such regimes are available only to a very small proportion of patients even in the district centres, and to practically none in the peripheral centres. Most patients go on a standard tow drug regime (generally INHTH).

Apart from these, there are minor side effects such as anorexia, nausea, vomiting and head ache. Weight gain and rise in haemoglobin level are less in patients on TH as compared with those on PAS. The effect of such minor side effects on patient compliance, especially in the absence of adequate medical supervision and reassurance, can only be imagined. We will consider possible alternative regimes in the next section. For the moment let us stick to the first line/second line chemotherapy model. We have already noted, some of the problems with the bi-weekly INH/SM regime not available for sputum negative patients, and limited and irregular supply of SM. In addition, there is a rule that SM injections can only be given at the PHC level. In other words, this regime is effectively available only to those who live within about 5 kms of a PHC. b.

(3) When a patient fails to respond clinically to a particular regime, there are no facilities for drug sensitive testing ever in these selected cases. Theoretically, in the existing model, they can be referred to Tuberculosis Sanatoria for treatment with 2nd line drugs. In practice, however, (a) practically none of these patients do get referred to Sanatoria; and (b) even among those who are started on second line drugs at such centres, there are not facilities to continue drugs after the patient is discharged.

Drug resistance

Coming now to the problem of resistant TB, there are a number of problems in the existing framework.

(1) Drug resistance in TB is not a rare phenomenon. Existing studies show that the prevalence of primary drug resistance to both INH and SM in India are (individually) of the order of 5 to 10 percent. The prevalence of acquired drug resistance is

The lone patient I managed to get referred to a Sanatorium in Bhopal emerged after two months looking much

4

better and clutching a prescription for rifampicin and ethambutol.

The cost of s complete course of treatment with the newer drugs at current market prices is of the order of Rs. 500.00 to Rs. 1000.00. Regimes containing Streptomycin are liable to cost more, because of the administrative cost of giving the injection.

C. Possible Alternatives It is well known that there now exists a wide variety of alternative drug regimes for the treatments of TB, many of which result in cure of a higher proportion of patients in a much shorter period of time than existing standard regimes. The conventional wisdom is that these alternative regimes comprise a ‘second line’ of treatment for patients resistant to the standard regimes.

We are not talking of enormous sums of money. The cost of bi-weekly INH/SM with an initial intensive phase in not such less. Neither is the cost of INH/PHAS regimes. The logic of the exclusive dependence on INH/TH now becomes clear. Put another way, the cost of treating a case of TB with newer drugs and the cost of treating a case of intestinal obstruction or pyogenic meningitis is about the same. The cost of treating a case of ischaemic heart disease or lung cancer or brain tumor or diabetes mellitus or chronic renal failure is several times higher. The comparison becomes ridiculous when one carries the contrast to fields outside medicine --say, to defence or CHOGM.

The fact that the government itself does not take this argument seriously is shown by the free availability of the so called ‘second line’ drugs in the open market. Of course, the price is far beyond the reach of the ordinary TB patient. As a result, we have in India the ironic situation, where the District Tuberculosis Officer and the PHC Medical Officers are the only medical practitioners who (in their official capacity) have no access to the newer drugs for the treatment of TB.

b.

In effect there are today, in TB, as in every other field of medical and indeed of public life, two sets of policies in operation – one for the poor and one set for those who can (even if only with difficulty) pay.

The second aspect of the cost equation --- what is the ‘cost’ of a twenty percentage relapse rate which is the best result obtainable with the standard ‘first line’ regime? What is the ‘cost’ of a case of thiacetazone induced agranulocytosis or Stevens-Johnson Syndome? What is the ‘cost’ of traveling up and down form village to PHC, village to District centre, village to wherever, for 18 months as against the six months with newer regimes? What is the ‘cost’ in bus fare? What is the ‘cost’ in lost income? What is the ‘cost’ in the suffering of a poor man? This is a

The argument against the newer regimes can now be seen plainly for what it is – a question of cost. It is worth going into this question in some details. 5.

Cost to whom

The question of cost

a. How much?

5

question which the policy makers of TB must answer.

THE SEVAGRAM MEETING hopes of programmes for identification and release of bonded labour in Rajasthan; the irresponsibility with which safety and waste disposal issues are tackled in a Nuclear fuel complex in Andhra; the successes of a small town library and cultural centre attempting to popularize science and social issues through discussions and street theatre in Madhya Pradesh; the experience of improving the economic status of women with ambarcharkha in Maharashtra; the process of rationalizing health care, and increasing community orientation among voluntary agencies in Gujarat; the complex cultural and social factors involved in countering the problems of witch-hunting in Maharashtra; the preoccupations of a small town general practitioner in West Bengal; the experience of a low cost drug distribution and quality testing project in Gujarat; a programme of cancer detection in urban Hyderabad; and the dilemma’s on the issue of postgraduation of a young doctor from Tamilnadu.

The core group of our friends circle spent five days together at the end of July, at the Sevagram Ashram in Wardha. The agenda included discussions on the 1985 annual meet in TB, the role of mfc in the Drug Action Network and various other organizational issues. The meeting was an informal interaction in the ‘mfc style’ greatly facilitated by the simple and ‘mat-level’ life style of the Ashram. We present here a few of the important issues discussed and the decisions taken for the information of our members and bulletin subscribers. Session 1 --- Our Concerns A salient feature of such meetings is the opportunity to get to know what various participant members are doing in the field and to discover the wider issues with which health and medicine are so intricately enmeshed. Responding to the question of “what have been your chief concerns since the Calcutta meet?” participants shared their involvement and action in the last few months. We heard about the problems of tribals vis a vis dams and eviction from forest lands in Gujarat; the inadequate response of authorities in Gujarat and West Bengal to the hepatitis and dysentery epidemics; the difficulties of attempting scabies control in tribal belts; taking steps against stepwells perpetuating guinea worm infestations in Rajasthan; the effects of proposed mechanization on workers specially women in the tobacco processing industry in Karnataka; the travails and

Being mainly a ‘thought current’ mfc perspectives need to be shared to wider and wider audiences through articles for the lay press, lectures in public forums and the organization of meetings around relevant issues. We heard about the meetings on Drugs organized by Lok Vidnyan Sanghatana on drug issues in Maharashtra; the birth of the Socialist Health Review and the Drug-Action Network newsletter; and the various articles for the lay press and

6

papers of Calcutta, Anand and Dacca meetings.

existing bulletins which members had contributed. 3.

Session 2 ---- Organisational Issues 1.

a) Trial subscription process will be activised

Funds

The budget was finalized and it was decided that the estimated deficit would be covered by a concerted bulletin subscription and membership drive including life subscribers for the bulletin (Rs. 250). Funding agencies would be approached only if the third anthology estimates went above the total proceeds from 2nd anthology sales. 2.

b) Editorial finalized

c)

The third anthology was finalized with addition of a few more articles than those decided at Calcutta / Hoshangabad meetings. VHAI’s offer to print it was accepted. The anthology was named --Under the Lens-Health and Medicine.

A pre-publication offer and a post-publication package deal for the three anthologies is also being planned

e)

An anthology on Medical education issues is being planned based on bulletin articles and background

were

d) The Back issues distribution system of Centre for Education and Documentation, Bombay was accepted. Mfc office will cater to only small orders. Copies of Index of 100 issues is available with the office at Rs. 2.50 each.

It was decided to request VHAI to reprint the second (Health Carewhich way to go) and first (In search of a diagnosis) anthologies

d)

guidelines

c) A reminder process for lapsing subscriptions was streamlined. Rubber stamps “your subscription ends ----------“ and ‘please renew your subscription’ will appear on the last two months of the subscription as well as for two months after the lapse. No money order form will be sent in future. Only two months lapse will be allowed before discontinuing.

Anthology

b)

Bulletin

e) The themes to be featured in the bulletin in the next twelve months are i) Child Health (in view of World Health day theme for 1984) ii) Worker’s health, iii) Urban healthslums and urbanization, iv) The return of epidemics, v) Health in the people’s science movement vi) Alternative systems of medicine. Apart from these there will be special issues on

7

problem. The planning for the annual meet which ensued came to the following important decisions.

Tuberculosis as well as Drug issues.

Contributions in the form of articles, book reviews, dear friend letters and entries for keeping track column are welcome from all our’ members and subscribers. Send them in as soon as possible so that these thematic issues can be planned well in advance.

i.

ii.

Session 3 --- Discussion on Tuberculosis The theme for the 1985 annual meet is “Tuberculosis and its control in India’. The theme was selected because Tuberculosis as a problem is very closely linked with the unhealthy structure of society and the national control programme is one of the oldest ones of its kind in the country. Two members Binayak Sen and Mira Sadagopal had written background papers for discussion. The former is featured in this bulletin. The latter entitled “TB and Society” is available on request from the author C/o. Kishore Bharati, P.O. Bankheri, Dist. Hoshangabad, Madhya Pradesh – 461990. a special reference file of papers on various aspects of the TB programme had been prepared by Ramakrishna, one of the associates of the Bangalore office. All these were very much appreciated.

The discussion raised many important questions about the problem and existing situation of the disease and the control programme in the country. However it was obvious that there was need to collate available information on various aspects of TB from studies and field projects to enable the group to arrive at a realistic, rational and alternative approach to tackling the TB

Dates / venue 27-29th January 1985 Bangalore is first preference for venue because of the proximity of National Tuberculosis Institute. Madras is second preference (To be finalized soon). Background papers The following twelve background papers will be prepared by members in groups or singly. a) Evolution of TB control in India – historical reasons and conceptual premises. b) Problem of TB in special situations like socialist countries, states in India like Kerala and Gujarat and reasons for differences. c) Epidemiological reality at District level. d) Case detectiondiagnosis, misdiagnosis and over-diagnosis. e) Exrapulmonary and childhood TB. f) Rational TB therapy and drug related issues like economics, availability and rationale of alternative regimens. g) BCG immunization --- the present status. h) Treatment of TB in other systems of Medicine. i) TB on the 20-point programme --- implications. j) Case holding and patient compliance and motivation. k) Role of private practitioners and nongovernmental voluntary organisations in TB control l) Education and awareness building in TB control.

iii. Surveys A practical survey on the existing situation of TB and the control programme will be carried out though the bulletin to get a much larger participation from members /

8

subscribers. Short-term enquiries on aspects such as the status of TB in medical education; the costs to the patients with alternative regimens; how is TB managed in hospitals, private practice and PHCs in the light of the recommendations on alternative regimes; drug availability problems at different levels, etc will be undertaken with the help of interested members especially student groups associated with mfc.

Session 4 --- Mfc role in DAN There was a detailed discussion on three papers written by Anant on the future perspectives and organizational character of Drug-Action-Network (DAN) and also of mfc’s role in it. Anant, who will has reported on this matter as well as the two-day DAN meeting (30t-31st July 1984) elsewhere in this bulletin.

Dear Friend……

iv. Pre-planning for meet September issue of bulletin will introduce theme and give tentative details of the meets. Background papers will be ready for circulation by 1st November. Bangalore group will identily questions for discussion and produce short working papers and plan out a programme for the meet --- small group discussions and plenary sessions. Background / working papers will be ready for dispatch to all interested members / subscribers from 10th of December on which date a special issue of the bulletin on TB containing relevant material as well as an article by Anant on the role of mfc like organisations in tackling the TB problem will be posted.

Rational Drug Policy Received the July 84 issue of mfc bulletin and found the memorandum of Rational Drug Policy very comprehensive, complete and interesting. This is one field which needs regular and persistent pressure. I am sure with the example of a country like Bangladesh practicing strict drug policy we can look forward to success. Was surprised to learn about the state wise break up of mfc readers. I shall be sending soon a list of friends at various medical colleges who may be potential subscribers. Also will motivate friends at Varanasi to get involved. I came to know of mfc during my internship at rural health training centre, Chiraigaon. I will feel happy to extend any sort of help at any time.

(Note: If any member / subscriber is keen to participate in any of the above mentioned preliminary exercises for the annual meet kindly write to us immediately giving areas of interest and ways and means of suggested involvement.) Group work will help us in gaining a such wider perspective!

Naresh Kumar 23rd July 1984 BHU, VARANASI.

ALL INDIA DRUG ACTION NETWORK 9

Rational Drug policy in India.” These notes were later presented in the Drug Action Network meeting.

A meeting of those groups who have been in contact with each other for the last twothree years in connection with ‘DrugAction’ was convened at Wardha on 30th and 31st July i.e. immediately after M.F.C’s mid-annual core-group meet. Representatives of the following organizations participated:

30th July: Discussion on the perspective The first day of the meeting was devoted to the discussion on what in our view is the outline of a Rational Drug Policy in India. Sujit Das (Drug Action Forum, West Bengal), Anant Phadke (MFC) and Amitab Guha (FMRAI) had prepared notes on Rational Drug Policy. These notes were discussed.

 Arogya Dakshata Mandal, Pune  Consumer Guidance Society of India,  Delhi Science forum.  Federation of Medical Representatives’ Association of India (F.M.R.A.I.),  Foundation for Research in Community Health, Bombay  Kerala Shastra Sahitya Parishad (K.S.S.P.)  Lok Vidnyan Sanghatana, Maharashtra,  LOCOST Project, Baroda,  Medico-Friend Circle,  Voluntary Health Association of India (VHAI)  V.H.A.I. West Bengal.

A consolidated draft of a Rational Drug Policy (a sort of manifesto of the group) was prepared by a small group at the end of the meeting at a late night session. It will be cyclostyled and circulated for further comments, modification, concretization etc. and would be finalized for printing in the next meeting of the Coordination Committee to be held in a couple of months. 31st July ----- Action Programme and Co-ordination:

As a preparation to this meeting of the Drug Action Network, there was a discussion in the MFC-core group meeting about MFC’s involvement in this activity. It was decided that M.F.C. would of course continue to be a part of this activity. M.F.C. can help by contributing to the spread to this movement in different parts of the country. MFC can also contribute by providing socially conscious medical expertise; this would be MFC’s specific contribution. It was also decided to form a drug-cell in MFC of those members who want to participate in this movement. Anant Phadke would coordinate this drug-cell and would act as MFC’s representative. Anant had prepared two notes – “Essentials of a Rational Drug Policy” and “Towards a

Since there are so many irrational drug-combinations and so many irrational practices, we have to consciously choose where to begin and concentrate. We have to prioritize activities which must be taken up immediately, for example: the Government’s ban order in JULY ’83 on twenty two hazardous inessential drugs and the sequelae to this ban-order. Secondly we have to concentrate on those around which a mass movement can be launched. It was therefore decided to concentrate on the following: 1) The Government’s ban-order The Ministry of Health and Family Welfare had issued a Gazette notification

10

on 23rd July ’83 “to prohibit the manufacture and sale” of 22 categories of drugs and drug-combinations. There are a number of loopholes and problems in this ban-order. The matter has now gone to the Supreme Court. In the meantime attempts were made to prepare a list of brands containing these 22 categories of drugs, since the Government has not published such a list. There are some problems in this work because the Government Order is vague on some points and because some manufacturers have deleted ingredients the brand-name of their formulation. For example amidopyrine has been removed from “Spasmindon.” The lists made by Mira Shiva and Dr. Rane would be complied together and Mira Shiva will send a copy to those who want it. It would also be published in the MFC-Bulletin. This list would enable people to know which brands to boycott till their manufacture itself is stopped or till these ingredients are removed from these brands.

Dinesh Abrol, informed that DSF is prepared to move the Supreme Court about enforcing a ban on this combination and wanted to know the opinion of others. It was unanimously decided that this combination has no scientific justification whatsoever and in view of the serious nature of the potential risk involved with the use of this drug, a water-tight case can made. With this go ahead with its plans in this regard.

2)

5)

4) Ban on Analgin, Chloramphenicol-Streptomycin combination, fixed dose combination of Cortico-steroids: As in the case of EstrogenProgesterone forte, these drugs need to be immediately banned. Medical aspects of these drugs has already been discussed in earlier Drug-Workshops and it was unanimously decided to take the question of the ban on these hazardous combinations in the coming campaign.

Supreme Court Case:

Drugs alert

Alert on indiscriminate use of oxyphenbutazone group of drugs, Lomotil and anabolic steroids in children and of clioquino -- There was no unanimity about a complete ban on these drugs but it was agreed that these potentially hazardous drugs are being widely misused. Since may doctors are not well informed about these drugs, it is necessary to alert all doctors about these high-level drugs.

As was reported in the March ’84issue of MFC Bulletin, Vincent Panikulangara has filed a Writ Petition in the Supreme Court to amend and add to his original petition about banning of the 22 categories of drugs. The drugcompanies have filed counter-affidavits and the Drug Action Network should make available medical-technical expertise to Vincent to answer the medical-technical issues raised in these counter-affidavits. It was decided the DAN should assure Vincent of such expertise.

6) Irrational drug-combinations used to treat common disease: For exampleantidiarrhoeal combinations, analgesic combinations, haematinics. The technical material on these three groups of drugs is almost ready and hence it is possible to say something very concrete about these groups of drugs sold under different brand-names.

3) Ban on Oestrogen-Progesterone forte preparations: This ban is still not effective. The representative of Delhi Science Forum-

11

Different persons have taken specific responsibilities and the technical material would be sent to Mira Shiva by Augustend; and she will then duplicate it and send this collected material to those who want it. 7)

a prioritized list of drugs based on the disease-pattern in India is not available today and this concept is also not accepted by the Government authorities. We must, therefore, argue out the necessity of such a list and prepare such a list. Mira Shiva has done most of the necessary groundwork and has summarized it in her two notes circulated at the workshop. We need to study and finalize the list prepared in her note – “Graded Essential Drug List.”

Over-the-counter drups:

Non-medical groups like Science groups or consumer groups would not be able to involve themselves much in issues which are highly technical. Hence technically simpler issues like over-thecounter drugs would be more appropriate for such groups. Lok Vidnyan Sanghatana has prepared certain demands about overthe-counter drugs – for example, withdrawal of irrational combinations, pre-censorship of advertisements, ban on advertisements of tonics in the lay media. These demands would now be made a part of the ‘All-India Campaign.” 8)

There are atleast 15,000 brands of drugs sold in India. Majority of them are irrational. But this has to be concretely demonstrated, item by item. If we can not scrutinize all the 15,000 formulations in the immediate future, we need to scientifically scrutinize atleast the topselling brands to show the irrationalities in these formulations. (Can MFC members take up this task? One person or a group can not take up the task of scrutinizing all categories of drugs. One person / group can take up responsibility of one or tow categories of drugs.)

Popular Slogan:

It was felt that we need to form some popular slogans to be used by all groups. The following slogans were agreed upon:

9)

i)

Make available adequate quantity of life-saving and priority drugs to the needy.

ii)

Implement the ban-order: ban all hazardous drugs.

iii)

Nationalize the Multinational Drug Companies.

iv)

Prepare Policy.

Scientific

10)

Co-ordination Committee:

It is obvious that a lot of coordination would be required to lunch an All-India movement. It was however felt that we should not form a separate organization. A Co-ordination Committee would be sufficient. Those groups who have been active, in contact with each other for the last tow years and hence present for this meeting would constitute the Coordination Committee. Those groups / organizations who agree with the “MANIFESTO” prepared during this meet, and who would show some consistency in the sharing of responsibilities can be coopted into this Co-ordination Committee of DAN. The network was renamed a ‘All India Drug Action Network.’ Mira Shiva has been the de facto convener of DAN. She was

Drug

Further study:

The concept of Essential Drugs (a better term would be priority drugs) is a vital aspect of a rational drug policy. Such

12

requested to become the Convener of the Coordination Committee. She had to agree. It was decided that the Coordination Committee would be decided that the Coordination Committee would be concerned only with the Coordination of National level work. The work at local levels is the responsibility of local groups / organizations. Expenses incurred for Coordination work would be shared equally by all members of the Coordination Committee.

LIC Quarters, University Road, Pune-16. In your letter pleases indicate how you would like to help a) By writing articles, giving talks on the issues taken up in the campaign? If yes, what kind of back-ground-material would you need? Will you please pay for the material? Will you please send some amount on an ad hoc basis as your contribution? --- Say: Rs. 10 to Rs. 25 /-.

Since the Drug Action Forum of West Bengal drugs issue from last week of November, it was decided that all groups in different parts of India would start their campaign from 23rd November 1984. Some groups will start earlier but would have a special booster programme from 23rd November so that an All-India impact is made.

b) By offering to scrutinize one or two categories of drugs? (Say cough-mixtures or tonics.) Those who have access to medical libraries are earnestly requested to take up this work. c) By offering to collaborate with other organizations in your local area?

In spit of the hectic schedule, we managed some time for a slide-show by Dr. Ekbal of KSSP on their work in the “save the Forests” campaign and the Drugs-issue. The work done on both these issues tells us how KSSP is a real massmovement and hence very inspiring.

d) By trying new ideas to make the campaign stronger?

Editor’s Note The simple yet comfortable arrangements at the Sevagram Ashram was quite a welcome change for citydwellers; except for the smokers since smoking was strictly prohibited in the Ashram premises! Anant Phadke

In this issue the Sevagram meeting is highlighted. The organizational issues on which decisions were taken are featured as well as a report on the DrugAction-Network meeting and MFC’s role in it. A background paper prepared by Binayak Sen is introduced to initiate hopefully a growing dialogue on the TB question which will climax at the annual meet in January 1985. The planning process leading up to the meet is also presented. Members and subscribers are requested to respond to all these reports with constructive criticism, ideas, suggestions and action.

Attention Please! Those MFC members who want to help out in the Drug-campaign are requested to write to Anant Phadke, 50,

13

Editorial Committee: Kamal Jayaroa Anant Phadke Padma Prakash Ulhas jaju Dhruv mankad Editor: Ravi Narayan View and opinions expressed in the bulletin are those of the authors and not necessarily of the organization. Annual subscription – Inland Rs. 15.00 Foreign: Sea Mail – US$ 4 for all countries Air Mail: Asia – US $ 6; Africa & Europe – US $ 9; Canada & USA -- $ 11 Edited by Ravi Narayan, 326, 5th Main, 1st Block, Koramangala, Bangalore-650034 Printed by Thelma Narayan at Pauline Printing Press, 44, Ulsoor Road, Bangalore-560042 Published by Thelma Narayan for Medico Friend Circle, 326, 5th Main, 1st Block, Koramangala, Bangalore-650034

14


