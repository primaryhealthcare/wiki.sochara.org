---
title: "The Disabled Child"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled The Disabled Child from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC072.pdf](http://www.mfcindia.org/mfcpdfs/MFC072.pdf)*

Title: The Disabled Child

Authors: UNICEF

medico friend circle bulletin DECEMBER 1981

Food Requirements as a Basis for Minimum Wages Chetana - Vikas is organising rural labourers for their legal right to work (1) and for lawful wages. During this, we faced the question - what should be the minimum wages for a rural labourer? This article is the result of an enquiry into this very important issue. The study may appear specific to Maharashtra, but can very well be generalised. The prevailing minimum wages for agricultural labourers in Maharashtra are based on the recommendations of a Study Committee appointed by the Govt. of Maharashtra under the chairmanship of Mr. Page (2). One finds that the Committee has made many errors in its method to decide the wages, and these errors form the scientific' secret or the low wages offered to about six million landless labourers and an even larger number of small farmers in rural Maharashtra for the last many years. What standard of life should be considered as ‘minimum’, when minimum wages are decided? Any definition of ‘Minimum standard of life' would be highly controversial. I shall stick to the minimum necessities as understood by the above Committee and to the basic method evolved by the Committee to calculate the cost of minimum living for deciding minimum wages. In this article an attempt will be made to point out some serious errors in certain assumptions about the food requirements used in this method, to correct them and to calculate the corrected minimum wages, using the same formula which the above Committee has established. To make it more relevant for the readers of MFC Bulletin, I shall elaborate only on the nutritional aspect of my enquiry and make only brief reference to the economic part of

it.

The Method Established By Page Committee: The report says, ‘Minimum wages must be in some way related to the cost of living. The Workers must be able to meet minimum requirements of food, shelter, clothing, medicine and education’ (page No. 113).

We should agree to this fundamental: principle. The method used by the Committee to calculate the cost of such living is as follows:

“The wages should be fixed in quantity of one kind of staple grain, i.e. Jowar in Maharashtra." "The wages should be calculated in kind first and then converted to cash at a price at par with “the selling price of first quality Jowar at ration shop". "For fixing the wages in kind, we have considered the following factors" (page No. 106). “An average working man requires at least 2000 to 2200 Calories for which 625 Grams of staple food is a necessity. We are assuming a family of 3½ units i.e., husband, wife and three children. Their requirement will be 2187½ Gms. This would be the staple food requirement of the average family I'. "Normally, we are advised, that staple food requirements are 40 to 50 % of the total budget: Working on this basis of 40% which is in favour of the labourer, the total budget would come to 5468 Gms, of Jowar. Making some allowances for one weekly holiday, we can safely assume that a poor worker's 'family budget would be 6000 to 6400 Gms. Normally 6 Kgs, should be earned by 2 persons". "We were advised that 3 Kgs. can be assumed as the daily wages in kind for an adult". The minimum wages and E.G.S. rates decided by the Government of Maharashtra were based on these recommendations. Three Kg. Jowar was converted into cash as 4 Rs. which thus became the minimum wages for the rural labourer.

Errors in the committee's method and necessary corrections: 1. The calorie requirements of the labourersThe biggest mistake is to accept 2000 to 2200



Abhay Bang, Gopuri, Wardha-442001

Calories as the calorie requirements of 'average working man'. This is insufficient even for a sedentary man. For a male rural labourer it is 3900 Calories/day. The figure of 2000 to 2200 Calories for working man was based on 3 supports (personal discussion with Mr. Page). i.

The poverty line accepted by Dandekar and Rath (3).

iii.

Calorie needs of rural labourers:

The ICMR Nutrition Expert Group has classified adults in various occupations into 3 categories Sedentary, moderate and heavy work, on the basis of their Calorie needs. The Calorie requirements are as follows:-

Average of National Sample Survey for 10 years.

ii.

used in the medical field all over India as authentic figures. Page Committee should use these figures.

Advice by nutrition expert.

Let us examine these one by one: i.

The National Sample Survey (N.S.S.) figure is the amount people actually purchase and consume on an average. In India, about half of the population live below the poverty line, and don't get enough food. It is ridiculous to consider the average consumption of such semi starved people as their biological requirement.

ii.

The poverty line defined by Dandekar and Rath (3) is based on 2250 Calories, average calorie consumption per capita per day, this being considered “nutritionally adequate". Unfortunately the authors are silent on how this figure has been considered “nutritionally adequate".

Further, the food consumption figures given by N .S.S. and the poverty line accepted by Dandekar and Rath are the average figures of various age, sex and occupational groups. It is well known that the calorie requirements of different age, sex and occupational groups grossly vary. Hence to accept such an average as the Calorie requirement of a ‘working man’, as done by the committee, is not correct. iii. The nutrition expert, mentioned by Mr. Page, is actually a diabetes specialist of Bombay. There is obviously some gross error in the 'expert' advice. (Has he assumed that rural labourers need the same amount of calories as his obese upper class diabetic?) My conclusion is that the figure of 2200 Calories is not applicable to the labourer. For the purpose of minimum wages and Employment Guarantee Scheme (E.G.S) we are dealing specifically with manual labourers: hence the calorie requirements of this specific group should be taken into account and not the' average'. The Indian Council of Medical Research (I.C.M.R.) is the highest body in she field of medical research in India. Based on WHO recommendations and scientific research in India, ICMR gives us the nutritional requirements of various age,' sex and occupational groups in India (4). These figures ate accepted and

Male (55 Kg.) Female (45 Kg.) Sedentary Moderate Heavy

2400 2800 3900

1900 2200 3000

In which category should the agricultural labourers and unskilled E.G.S. labourers be put? This is a very crucial question and needs closer examination. Let me make one observation here about the present state of scientific knowledge. In a predominantly rural country like ours where 70 % of the labour force is engaged in agricultural work, scientists and experts are not very certain about the calorie needs of the agricultural labourer. Not that the answer is very difficult or beyond scientific research. But the plain fact is, not much pains have been taken to study this supremely important issue, There are very few studies and many aspects are still unanswered. This is obviously an area which needs urgent attention from the researchers in the field of nutrition. 1) FAO/WHO Committee puts many farm workers in the' Moderately active' category. Obviously, the Committee has mechanised farm workers in mind because in ‘very active' category are included, unskilled labourers, some agricultural workers, some farm workers especially peasant agriculture)' (5) So our agricultural labourer and unskilled E.G.S. labourer should come in ‘very active' category. 2) The method used to determine the calorie needs of any occupational group is to break into 3 equal parts, 8 hours each of occupational work, off duty work like house hold work etc. and sleep. The variation in calorie needs of different occupational groups is because of the difference in the calorie expenditure during the 8 hours of occupational work. The calorie expenditure for the other 16 hours is the same for' moderate' or ‘Heavy' occupational groups. Hence we will concentrate on the rate of energy expenditure during occupational work. The ‘heavy work' taken for ICMR calculations is one which needs 5 Cal. per Kg body weight per hour (6) while ‘moderate' means 2.5 Cal/Kg./hour. There is evidence to prove that most of the work done by the agricultural and E.G.S. Labourers involve expenditure of calories either more than or equal to 5 Cal/Kg/hour.

There are two authentic studies (7, 8) referred to by the ICMR Nutrition expert group. The calorie expenditure in various types of agricultural and other unskilled operations as found in these and some other studies varied from 4.5 – 10.0 Cal/ hour / kg body weight. A question can still be raised that no labourer continuously works for all the 8 hours of occupation. They rest intermittently, Hence actual energy expenditure in 8 hours of work might be less. The above mentioned two studies (7, 8) have actually recorded the minute to minute activity of the labourers during the working hours (for 3 months in one of the studies). Thus the relaxing intervals were also covered. The hours spent in various types of activities were available from this record. The energy expenditure of the labourers during the 8 hours of work was then calculated on the basis of time spent and the rate of energy expenditure in various types of activities. The energy expenditure for 24 hours was 3,020 Calories for agricultural labourers and 3,025 for unskilled labourers. The average body weight of the labourers (all male) in these studies was 44 kg, to 46 kg. I f we correct the figures for the reference body weight I 55 kg according to ICMR) and for the faecal losses, the calorie requirements of the male agricultural labourer will be 4150 Calories and of the unskilled labourer, 3977 Calories. It is important to quote one of the studies (8), "However it would be wrong to conclude that this is the optimal Calorie requirement of Indians engaged in heavy activities. Such a conclusion would be permissible only if it is established that the activities investigated represent the maximal efficiency and output, It is possible that the output of work in the subjects investigated in the present study might have been better, had their calorie intake been higher". In other words, even this figure is likely to be an underestimate because it is calorie expenditure at the present level of intake which because of severe economic constraints is very low and limits maximum working capacity. The energy requirements for males in ‘heavy work' as advised by ICMR are 3900 Calories and those in ‘moderate work' are 2800 Calories Obviously the agricultural and unskilled E.G.S. labourers' calorie needs are at least those of the heavy worker and not those of the moderate worker. 2. Calorie requirements of children:

Another mistake of the Page Committee is to assume that the calorie requirements of the family are equivalent to 3½ units; 2 units for husband and wife, and ½ each for 3 children. The per capita ‘average’ given by N.S.S. or

Dandkar and Rath includes all the age groups. If the Committee accepted that figure as the average requirement, it should have at least used the same average figure for all the family members. Further, the assumption of the Committee that the calorie requirement of child is half of the adult is purely a guess without any scientific support. Because of their growing age, children require quite large number of calories. 3. Per capita Calorie requirements of the rural labourer's family:

ICMR gives calorie requirements of various age and sex groups (4). By taking into account the proportion of various age and sex groups in the population, the average per capita calorie requirement at the national level was also calculated. It has been assumed that all adults are moderate workers but we should consider the calorie requirements of the adult working population \ age group 19 to 59) as those of heavy workers. If we do this, we get a per capita recommended intake per day of 2560 Cal" This figure is much more scientific and true to the real needs of the workers than the one calculated by Page Committee 2200 X 3.5= 1540 Cal.). This is also closer 5 to the figure of 2400 Cal. per capita per day calculated as minimum requirement for the rural areas by the Perspective Planning Division of the Planning Commission of India ,9). 4. The family size of the poor:

The family size taken for calculations by Page Committee is 5. Assuming that the rural labourers come from the lower 50 % of the rural strata, which they surely do, the average family size of this' section of rural population is 5.6 (10). Calorie Requirements of a labour-family:

The real calorie requirements for a family of 5.6 would be 2560 x 5.6 = 14336 Calories I day. This corrected figure is much higher than the one calculated by the Page Committee (2200 X 3'5 = 7700 Calories / day). 5. The types of foods in the labourer's diet:

The next error in the Committee's calculation is the assumption that all the calorie requirements are met by jowar only. The diet of any labourer should contain: 1. Cereals and pulses 2. Other types of foods - Fats, Jaggery, Milk, Vegetables etc. To calculate the staple food requirements of the family, we must deduct the Calories provided by the other types of foods from the total.

ICMR recommendations (4) of a balanced diet for a “heavy work” man include milk (250 ml.) fats and oil (65 ml) sugar (55 Gm) Vegetables (l70 Gm). Considering that our economy to-day cannot provide all these things in this ideal proportion, we shall for the time being take into account how much food of this type the poor actually eat to-day. In doing so the poor arc probably left with deficient supply of minerals and vitamins. The rural people living at the level of poverty line obtain 200 Calories per capita per day from these 'other types of food' (3). So a family of 5'6 persons would obtain 200 X 5.6 = 1120 calories from these foods. The remaining calorie requirements (14336 - 1120 = 13216) will be met through cereals and pulses. It is necessary that the diet also provides the amount of pulses recommended by ICMR which for a family of 5-6 works out to 280 Gms per day. It must be borne in mind here that ICMR recommendation for pulses is with the assumption that milk is provided in the ideal proportion. As we have not provided for these, the requirement of pulses should be increased to cover the proteins. Hence in our given situation, this figure of pulses is an underestimate. It must be noted that the ICMR has recommended-that at least 25% of the total calories should come from non-cereal sources.

7. Provision for other necessities:

The Committee has assumed that 40-50 % of the family expenses are on staple foods and remaining on the other necessities of life i.e. other types of foods, house, fuel, clothing, medicine education etc. This is well in agreement with National Sample Survey and with Dandekar and Rath. Taking into account the fact that a labourer has to earn in 6 days amount needed for 7 days, the Page Committee calculated the daily requirement to be 6.4 Kg of Jowar (This the Committee unnecessarily “rounded off" to 6 Kg ). Using the same method but using the daily food requirements of 3.85 Kg. jowar and '308 Kg. pulses the total requirement to cover the minimum cost of living will be 11.27 Kg. jowar and '90 Kg. pulses per day. 8. Conversion of wages into cash:

Without going into the details of the errors involved in using issue price of jowar at ration shops for converting wages in kind into cash it should suffice to say that market price of jowar should be used for conversion because the issue price of jowar in ration shops does not reflect the general price rise. The prevailing market rates are; Jowar – 11.27 Kg. X 1.75 Rs. = 19.70 Rs, Pulses – 0.9 Kg. X 5 Rs. = 4.50 Rs.

9. Total cost of living of the family and wages: This amount of pulses will provide about 980 calories per day to the family. To the cover the remaining (13216 - 980) = 12236 Calories, the jowar required is 3500 Gms (11). Thus the food requirements of the labourer's family are not 2.187 Kg. jowar as calculated by the Committee, but 3'5 Kg. jowar and 0'28 Kg. pulses. In calculating this, we have taken into account ICMR recommendations for calories and pulses only. For other types of foods in the diet, we have, for the present, accepted the existing reality and not the optimum.

6. Other Wastages:

These are requirements of a healthy population. Individuals who suffer from worm infestations, diarrhoeas, other infections may have increased food requirements. The majority of the rural poor suffer from one or more of these illnesses, making their real food requirements higher. But we have no method at present to calculate this wastage and so we shall leave it uncovered.

There are various losses between the retailer's shop and human consumption. FAO / WHO Committee (19) and ICMR (20) consider that 10% allowances should be given for such losses. Thus to provide 35 kg. Jowar and '28 KG of pulses for actual consumption, the purchases must be 3'85 Kg. of Jowar and 0'308 Kg. of pulses.

Thus the total cost of minimum living for the labourer's family is Rs. 19'70 + Rs. 450 = Rs. 24'20 per day. By the Committee's own guiding principles, a labourer's family should get this much amount as wages to cover the minimum necessities of life. This amount is to be earned by a couple. Assuming equal wages are given to male and female; it should be Rs. 12.10 per day. 10. Earning Members:

In the organised sector the salary of white collar workers (E.g. Lecturers, bank employees, Govt. Officials etc.) are so decided that one member 'should earn all the family requirements. But for the rural labourer, the Committee assumes two persons earning the family requirements thus reducing the wages to half - to be earned equally by male and female. We must also remember that at present there is no provision for the days lost due to sickness, pregnancy and delivery.

Cost of living is the cost of production of the labourer — Thus 24.20 Rs. is the cost of minimum living for a labourer's family. In other words it is the cost of production of labour power i.e. the cost that a

labourer-couple has to incur to produce one day's labourpower. It is a widely supported principle now that the farmer should get the cost of production for his produce. Similarly the labourer also should get the cost of production of his labourpower.

Page Committee, whose report decided the daily wages for more than 10 million people. One tends to wonder whether these mistakes were innocent or motivated. We 'learn from this exercise that no report from such ‘authentic' Committees be accepted blindly.

Wage rates and justice: This rate (12.10 Rs.) may appear too high to some people, only because we are accustomed to a very low and unjust figure of Rs, 4.

3) There is an urgent need to do more research on the calorie expenditure and nutritional requirements of the agricultural labourer. The insufficient material presently available also speaks about the research priorities in our country.

These new wages are calculated to provide the necessary calories and proteins (only partly) from the cheapest sources (jowar and pulse) and to provide other necessities of life at the present level of actual consumption by the poor \ and not the ideal level). Hence they are the minimum.

4) Knowledge of nutrition and health can become an effective tool for economic fight of the poor. This way the benefits to the poor and their health might be more than through health work alone.

Four things must be borne in mind while thinking about these rates.

i. 24.20 Rs. per day X 26 days of work per month = 627 Rs. per month will be the income of a family of 5.6 persons i.e. 112 Rs per capita per month. This is still within the limit of our average per capita national income \ Rs. 142.50 per capita per month) (12). Our economy can provide such wages provided the exuberant incomes of few other people are slashed down. This is, after all, a question of political will. ii. The wages for the rural labourers in Kerala, Punjab and Haryana are already more than Rs. 10 per day.

REFERENCES 1. Government of Maharashtra has passed an act by which it ensures employment to every rural unskilled labourer through the Employment Guarantee Scheme.

2. Report of the study Committee on Employment conditions of Agricultural Labour in Maharashtra State with Reference to Minimum Wages, Govt. of Maharashtra, (July 1971). 3. Dandekar, Y.M;, and Rath, N. (1971) Poverty in India, Indian School of Political Economy, Pune. 4. Recommended Dietary Intakes for Indians (I 981), Indian Council of Medical Research, New Delhi. 5. Energy and Protein requirements, Report of a Joint FAO / WHO Expert Committee (1973), Rome.

iii. Minimum wage in E.G.S. are not merciful relief or dole offered by the Government to the labourers. This is a legal right of the labourers and a pledge and responsibility of the Government of Maharashtra. iv. Many persons argue, “but labourers don't eat the amount that has been recommended by ICMR, and still they are not hungry". Here one must remember what ICMR experts said— " Unfortunately, experience has shown that human beings can adapt themselves, at a low level of vitality and with their powers impaired, to an insufficient ration without realising that they are underfed" ( 11 ). CONCLUSION:

1 ) In spite of having accepted many limitations posed by the Page Committee's frame work (E.g. requirements of other types of foods, number of earning members in a family, wastages of calories and time due to illnesses etc.) the minimum wages for rural labourer work out to 12.10 Rs. per day instead of 4 Rs. per day. 2) The level of scientific accuracy is extremely low in the calculations of a high power committee like the

6. Patwardhan, Y.N. (1960) Dietary allowances for Indians, Special Reports Series No. 35, Indian Council of Medical Research, New Delhi. 7. Ramana Murthy, P.S Y., and Belavady, (1966) Energy expenditure and requirements in agricultural labourers Indian Med. Res. 54: 977. 8. Ramana Murthy, P.S.Y., and Dakshayani, R (1962) Energy intake and expenditure in stone cutters, Indian Med. Res. 50: 804. 9. Majumdar, K.C., and Datta, K.L. "Comparative Calorie deficiency among different states" presented at the Seminar on 'Poverty, Population and Hope’, Pune 10-12 June 1981. 10. Computed from Table no. 1.7 in Ref. 3. 11. Gopalan, C et al (1971) Nutritive value of Indian foods, LC.M.R. New Delhi. 12.World Bank estimate, published in its report of 1981, circulated by A.P. from Washington on 10th August.

**

THE DISABLED CHILD [In this year of the Disabled, the UNICEF has brought out a special issue of their journal, Assignment Children, on the Disabled Child. In view of the importance of the problem, we have considered it relevant to take some extracts from this journal and present it to our readers. 'Elsewhere in this Bulletin, we are publishing a small write-up on the recent Magsaysay Award winner, and how he has used the concept of appropriate, technology in the field of Surgery. Editor] In the past, the disabled, the crippled, were pushed aside, neglected, sometimes hidden from view. Not only the disabled, but the entire family suffered from the Community's reproval. That prevention was possible had not yet been realized. Even in those societies where some of the causes of impairment have been isolated and certain preventive measures instituted, the disabled are still exposed to a certain stigma and discrimination. The consequences for women are particularly painful. Disabled women suffer special hardship and have less access to rehabilitation services, even in highly industrialized societies. The vast majority of impairments are preventable. They are the result of conditions of existence that can and should be modified. It is not a coincidence that the relative member of impairments increases more as one descends the ladder of poverty. The severity of an impairment is aggravated by society's confinement of the person to the category of the disabled. What was in the beginning a physical impairment becomes a handicap in the social sense as well, much more painful to bear. The vast majority of locomotors and sensory impairments would be less handicapping if, instead of concentrating on the child's impairment, efforts were directed to ensure to the extent possible the same development process as that of normal children. The translation of these three essential notions into policies and concrete programmes is not particularly costly. Inserted into a policy of primary health care, it is less expensive, more equitable, and more effective, than institution based approaches.

REHABILITATION Not only illiterate villagers, but also community leaders, professional people, government officials, planners and administrators are uninformed as to the causes, prevention and' treatment of childhood disability. Resources allocated for prevention and treatment are being spent in high-cost projects which serve a

few children and ignore the great majority who are at risk of, or affected by, impairment. Too often the programmes are based on models of limited relevance, and are not providing optimum help even to the children who are reached. Furthermore, the interruption, and often distortion, of the normal child development process can constitute a far more serious problem than the functional or cosmetic consequences of the impairment. Programmes to develop improved health services, better nutrition, basic education and family planning have great value in preventing childhood impairments. However, their content rarely includes elements needed specifically for this purpose; they do not reach most of the communities and in practice disabled children are usually excluded from the benefits of existing services.

MAGNITUDE Data from many countries verify that at least one tenth of all children are born with, or acquire, physical, mental or sensory impairments, that will interfere with their capacities for normal development unless some special assistance and attention are provided. About 80%, or 120 million of disabled children live in the developing world. DEFINITIONS a) Impairment any loss or abnormality of psychological, physiological or anatomical structure or function. b) Disability: Any restriction to perform a normal activity, due to impairment. c) Handicap: A disadvantage, due to (a) or (b), that limits or prevents the fulfillment of a role that is normal for that individual. THE CONSEQUENCES There are critical periods in the child's life, most particularly during the earliest months and years, when it is most capable of mastering particular developmental tasksforming an emotional bond with another human being, learning to stand and walk, or to talk. It may be more difficult, if not impossible, to master the tasks later on. Moreover, all subsequent developments become much more difficult. A great majority of the children are capable of adapting to the limitations and can progress relatively normally through the development process. Unfortunately, most families do not understand this and overprotect the child who is not permitted to engage in the activities and experiences that are needed.

THE BEST FOOT FORWARD Pramod Karan Sethi, 54, is the Head of the Department of Orthopedics and Director of the Rehabilitation Research Centre of Sawai Man Singh Hospital in Jaipur. The Philippines-based Ramon Magsaysay award for community leadership for this year has gone to Sethi for his development of artificial limb popularly known as 'Jaipur Foot '. The award carries a cash prize of US 20,000 dollar [Rs. 160,000) and is described as the Asian version of the Nobel Prize. The award praises him for his 'surgical genius'-surgical perfection in creating the' Jaipur foot'. The question is really of the futile and continuous race for higher technology, whereas the attempt should be to achieve a higher science content, preferably with a low level of technology. He stresses indigenisation at a time when his profession is turning increasingly sophisticated and the basic needs of rural health and medicine are being quietly ignored. Though indigenisation of the foot piece was the obvious answer, few of Sethi's colleagues agreed, the professional orthopedists among them, arguing forcefully against any change. In fact, it was a local artisan, Ram Chandra, who first understood Sethi's concept, found it feasible, and began working on the Jaipur foot. When the need for information into rubber technology arose, established tyre manufacturers did little to help them. However, Chuga Bhai, who owned a small tyre retreading shop in Jaipur, did their work, taught them vulcanization and refused to accept money. Bhagwan Mahaveer Viklang Sahayata Samiti, a charitable body, without whose assistance Sethi's work might have remained incomplete, pays the salaries of a third of those 40 artisans, who, though talented, would not be employed by the Hospital, since they are not formally qualified. The success of this joint effort has been phenomenal. In 1975, 59 patients received new limbs, this figure rising to 437 in 1976, 771 in 1978, and 1,415 in 1980. The number for this year may be in the region of 1,800. A continuous quest for perfection- and indianisation has brought down costs drastically. Modern Limbs, for example, are made of willow and plastic, which makes them expensive and are not easily available. Sethi's team has reported to aluminum limbs, which were in use earlier and he is now accused of turning back the clock, even though they have proved their utility. Raw material costs for below-the-kneecalipers have dropped to Rs. 50 instead of the usual Rs. 380. Artificial limbs can be had for Rs 250 in piece of the normal Rs. 850. 'Centralization has to be avoided. We have a wealth of talent in our villages, thousands of artisans who can be trained to do this work. What

we need is doctors who will believe in them'. But the pity of it all is that India's medical establishment, convinced of its correctness, believes in nothing but itself.

Unique Concept: The search for what ultimately came to be known as the "Jaipur foot" began in 1967. Till that point, amputees were fitted with the Siold Ankle Cushioned Heel (SACH) foot, a western creation. It is shaped liked a shoe, so that it can slip easily into one, and is used worldwide. If the SACH gained popularity, it was because it ideally suited western conditions, where shoes are worn constantly.

But if it was a success in the West, in rural India, it proved to be an unmitigated disaster. The Jaipur doctors discovered this when they found that a large percentage of their patients had .taken off the SACH foot after a few weeks, and had gone back to crutches. In rural India, conventional leather shoes have never enjoyed a great deal of popularity. Firstly, they are too expensive and secondly it is too warm in them. Social factors also worked strongly against the SACH foot. In home, particularly in kitchens and in temples, villagers move barefoot. Moreover, the rigidity of the ankle made crosslegged sitting and squatting on the haunches impossible. The Indian villager finds it extremely difficult to go through a normal day without adopting these postures, were it be for having dinner, working, or going to the toilet. Moreover, his routine requires him to walk on rugged terrain - unlike the wonderfully smooth pavements in the West - and often climb trees. He could not do this with the SACH foot due to its lack of flexibility. Working in water-logged fields was also not possible, since the SACH foot is made of sponge rubber and promptly absorbs water. In the Jaipur foot, as it now exists, the fixing rod runs through an ankle which is made of blocks of wood. The rod rests on a heel made of sponge rubber, which is enclosed in hard rubber, so as to prevent wear and tear. The front portion, as also the toes, is again made of sponge rubber. These components are bound together with a rayon cord to give added strength, and the entire foot is covered with vulcanized hard rubber, 2 mm thick, which is flesh coloured.

Courtesy: Science for Villages, Oct. 1981.

***

NURSE - THE WOMAN: IN THE MEDICAL SYSTEM - II [The November issue contained the first pert of this article. This is the concluding parted.]

feeling and ignorance about their own health and learn to have a positive attitude towards their own body and health.

FUTURE POTENTIALS OF THE NURSING PROFESSION:

iv) Nutrition and health education:

In view of the preceding analysis, the future potentials of the nurses can be seen in two main fields: (1) Role in health care (2) Role in social change,

Being a woman, ANM can best convey the message to the women including mothers who form the most important target group for nutrition and other health education.

1) Role in Health Care -

v) Curative Services:

It has now been realised that the doctor is a white elephant' which our poor society arid people cannot afford to train and sustain' in large numbers. Hence the WHO ideal of one doctor for 750 populations is irrelevant in our context. Apart from the cost, the doctor is not effective in reaching the masses in the rural' areas because of his/her attitudes, aspirations, elitist family back ground and education. Thus the intermediate tier becomes important in the health care of our country. The potential of the nurses should be understood in this context.

ANMs have limited curative powers today. A diploma holder doctor is allowed to use all the medicines. Why can't an ANM use more medicines to be able to treat most of the common illnesses with some more training of this role?

a) Maternal and child health ( MCH) : Women of child bearing age and children below 15 years of age together constitute 2/3rd of the total population in our country. They together also form the' biologically vulnerable section of the population. Most of their diseases are easily preventable and treatable. They are also the 'weaker section' in the family structure, arid hence neglected. Due to these reasons, MCH has been accepted as the main thrust of community health care. ANM has the key role in MCR services for the needy masses. Because of her sex, less elitist social status and education, and, low cost of her training and functioning she is more suited for this role than the doctors and the other male functionaries.

b) Family Planning: Again ANM is more relevant in this role than male functionaries for helping women to benefit from F. P. methods.

c) Altitude towards Women's Health: The present outlook of the medical system with regard to women's health in general and Family Planning in particular is oppressive. Being a part of this set up ANMs are also infected with this attitude. ANMs should be helped to discard this attitude of seeing the problems of women through the male's eyes - with indifference, contempt and coercion, and should be helped to learn to see women's health problems through women's eyes and understanding. ANMs can also help women in gene rill to get rid of the guilty

"An auxiliary can treat 90%” of children's sicknesses"Rural health Research Centre, Narangwal. "I am convinced that in any field of health technology, it has been shown that with only 2-3% of conventional medical technology, we could arrive at 80% of necessary quality care.” -Mahler Halfman, Director General, WHO. Paramedical workers with proper training have successfully done tubectomies in Bangladesh (5) and Caesareans in Tanzania. More curative powers to the ANMs will make such crucial services easily available to the masses, specially to the women and will also improve the status and acceptability of the ANM by the community as an important health functionary.

vi) Other Health Functionaries: The ‘village health worker' (VHW) who should essentially be a female and the 'rural obstetrician' Dai - are further steps of the same logic. They should be welcome in the health care system. ANM, VHW and Dai together can form a strong female infrastructure for community health care. They together can manage more than 90% of the health problems of the community. Such female network will greatly help the women of the rural areas who don't have access to proper health care today.

vii) Role in Hospitals: The nurses in the developed countries perform much more complex duties independently. There is no reason why our general nurses should be mere robots. They can be and should be given more responsibility, respect and freedom.

Corrective Measurers: To enable nurses to grow to these fuller potentials, certain steps are essential.

A) Increasing the woman - power: - In i971 ANM: population ratio was 1: 13170. The Govt. has recognised the importance of ANM and has set the target of one female multipurpose worker (formerly called ANM) per 5000 population to be 6 achieved by 1985. But even this ratio is also inadequate. The Ramalingaswami Committee (1980) has recommended one for 7 3000 population.

to identify themselves with the problems of women and the poor in general. (B)

The nurse is probably the only professional

group which is so exclusively made up of women. The problems of this profession are the problems of

women.

So

through

their

own

issues

it

is

possible to make them aware about the problems

To meet this target, nurses training schools and the training capacity will have to be increased many fold. Though both general nurses and ANMs are needed in much larger number to reach the optimum requirements, the priority should be given to ANMs as they will form the backbone of the rural community health care.

(B) Training facilities will have to be improved not quantitatively alone but qualitatively as well. The living and working conditions should improve. Training should not be geared to use the student nurses as pair of hands for the hospital routines. The training should be more community oriented and community based than hospital based. (C) The new upgraded and expanded role, function and status of the nurses in health care should be clearly defined and their training and working conditions suitably modified. (D) The ANM herself should not only be a respected member of the health team but she herself should also learn to respect and work in harmony with VHWs and Dais. (E) To bring all these changes is going to be a major political decision. Funds will have to be diverted from medical colleges, MBBS trainings and doctors to the nurses training schools and rural health care. Planners and doctors will have to learn and accept these new priorities. As most of the elite class today sets its eyes on admission in medical college, a shift in the focus will be vehemently opposed by this class and the doctors.

and exploitation of the women in our society; and that they can be organised and activated to fight against this. Nurses have two strong levers for this purpose.

i) ANM will have close contact with female VHWs, Dais and the women" of the rural areas. They will have an entry point like health work. These two advantages they can utilise to work among the rural women to help them understand the problems of women in the society and to fight against these. ii) General Nurses are the arteries of the hospitals. During the crucial moments in the women's fight for justice, the GNs can utilise their unique power to paralyse the most essential service and turn the balance in the favour of women. Thus nurses as a profession have immense potential to playa key role in the struggle of women for justice and emancipation. It is high time that activists and the organisations in the feminist movement realise this and concentrate on the nurses than on the urban middle class women alone.

Rani Bang, Gopuri, Wardha.

REFERENCES 1. Unless mentioned otherwise, all the figures are based on Cahp - Tnai (The Coordinating agency for health planning and trained nurses association of India) Nursing Survey in India, 1975. 2.Pocket Book of “health statistics" Govt. of India (1973 ) 3. Quoted from' Text Book of Preventive and Social Medicine' by Park (1977) Page No. 715. 4. MFC Bulletin No. 50, February 1980.

2) Role in the social change -

5. Abhay Bang, Learning from Savar Project' MFC Bulletin, No. 58, October 1980. 6. Same as Ref. No.3 Page No. 718.

(A) The present problems of nurses are essentially the problems of the women, of the manual worker, of the low socio-economic group. Nurses cannot get their new role and the just status unless the social system and its values change. So the nurses will have

7. 'Health for all' Report of the study group Chaired by Dr. Ramalingaswami 1980.

**

MFC ANNUAL MEET As announced earlier, the VIIIth Annual Meet of the MFC will take place at Tara, near Bombay from 23rd to 25th January. First two days will be devoted to a discussion on the theme - "misuse of commonly employed drugs by allopathic practitioners." The General Body Meeting will take place on the 25th January. Those who want to attend should please write to me for further details. Those participants coming from outside of Bombay should write to Shirish Datar if they want return reservations. Please write to him one month in advance and send the requisite amount by M.O. or draft. (No cheques please) His address Shirish Datar, Deshpande building, M.G. Road, Vishnunagar, Dombivali-421202. Tentative Schedule of the discussion on the above quoted theme is as follow(i) Misuse of various common drugs - 2 sessions of 3 hours each. — Analgesics, Steroids, Antibiotics, Multivitamins, Antidiarrheal agents and oral rehydration salts, Therapeutic foods (exclusive of breast-milk-substitutes}, Nonallopathic drugs commonly used by allopathic practioners. Breast-milk substitutes will not be considered because it is a very exhaustive topic requiring a-full session. Misuse means using when they are not indicated, using irrational combinations and dosages. We have to focus on common mistakes made by General Practitioners. Some MFC members have agreed to prepare a small (about 500 words) back-ground note on each category of the drugs mentioned above. Main points about misuse of each of these categories would be presented in 15 minutes, followed by discussion for about half an hour. Some invited guests would give their expert comments during this discussion and will also chair the session.

ii) (a) Irrational prescription practices and (b) Causes of misuse of drugs. (a) -lack of proper communication (including language of prescription \ wrong order of prescription; brand names ... such points are expected to be discussed. (b) -role of medical representatives, of research and other grants by drug-companies, role of "injection culture ", role of deficiencies in medical education and lack of continuing education after graduation, commercial competition amongst doctors, relation between foreign aid-programmes and misuse of drugs Such points are expected to be discussed under this rubric. This discussion will take place in groups and is expected to last for 3 hours. iii) Role of Foreign and Indian monopoly drug companies About an hour's talk by an expert guest. Following points are likely to be covered Dependence of our medical system on Foreign & Indian Monopoly Drug Companies; their profits, their; harmful practices including the phenomenon of dumping of certain' drugs (in the Third World) banned in the advanced countries; role of public sector in counter-acting these practices. There will be a discussion for about an hour on this talk.

IN THE NEXT ISSUE The next issue will focus on the drug industry. It will contain a substantial report of the seminar "The Drug Industry and The Indian People,” [Delhi, 7th and 8th November 1981] organized by five different organizations. Those who want extra copies should write to me in advance.

Anant Phadke.

Editorial Committee: Anant Phadke

Christa Manjrekar Mohan Gupte Ravi Narayan

Kamala Jaya Rao, EDITOR


