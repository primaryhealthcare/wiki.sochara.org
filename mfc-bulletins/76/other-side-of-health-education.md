---
title: "Other Side Of Health Education"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Other Side Of Health Education from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC076.pdf](http://www.mfcindia.org/mfcpdfs/MFC076.pdf)*

Title: Other Side Of Health Education

Authors: Bang Abhay & Bang Rani

medico friend circle bulletin APRIL 1982

Other Side of Health Education Some Experiences of Health Education in Rural Community Let us clarify in the beginning that we are not academicians in this field. Nor our work was an experiment in health education as such. We were fresh M. D.s from clinical side and we started the rural health work with a view to organise people for their own health as well as other needs. During this effort we came face to face with many realities. Here we shall recollect a few of these experiences and try to understand and analyse them.

Education: A two Way process Not knowing how sensitive is the tool of 'health education, in our new enthusiasm to teach ignorant people many new and scientific things, we tumbled in the field of health education also. It is a common rhetoric now-a-days in the field of health education to say that ' education is a two-way process.' There is no body as teacher or giver of the knowledge and no body as mere recipient of knowledge. Both learn together. But in reality usually, we the prophets of scientific knowledge, sit on a high pedestal and deliver sermons to the illiterate, ignorant masses as to how stupid they are, how they don't know anything and hence how it is now up to us to deliver these from this hell of darkness of ignorance. This is called health education. With such sort of attitude and relationship, obviously there can not be a two way process of education. We are the net losers because we have lost the opportunity to learn. When we look at our efforts of health education, we must admit that we began with this attitude of a priest. But today when we recollect the whole experience, we feel that though we cannot say whether people learnt anything from us or not, hut we definitely learnt a lot. Health education has definitely proved to be our education. What did we learn?

Why People Do Not Behave Rationally'? I

In our clinical and community health work in the villages this question always used to come to our mind - why people do not behave rationally? Why do they not practise certain scientific advice given to them by us? You ask them to use latrines- they won't. You ask them to keep roads clean-they would defecate on it. You ask them to dig soak - pits - they would nod their heads but won't do anything You ask women to come to antenatal check up or bring their children to under five care clinic, they won't. This always created a sense of frustration and anger in us. We used to get annoyed with their non- compliance. But still a question used to creep in our mind-why do they behave like this? Let us mention from our experience that the key to grow is not in believing but in questioning. An honest doubt is more worth than thousand dogmas we asked- why?

Raibai Raibai Dabole, a woman of 30 years age brought her 1½ year old baby to our clinic in Kanhapur. The child had severe mal-nourishment with bronchopneumonia. Obviously in very serious condition. We advised her immediate hospitalisation of the baby. She blankly stared at us. We again explained to her that the child will not survive unless she takes him to hospital and keeps him there for at least a week or more. She left the clinic with the sick child- obviously unconvinced about our advice. We cursed her - “these rural people don't understand the value of the child's life. They are callous about it, that is why children

Dr. Abhay Bang and Dr. (Mrs.) Rani Bang. Chetana- Vikas, Gopuri, Wardha 442114

die like flies. Without bothering for children's life, they produce new babies. Thus our birth-rate 'and infant mortality rate both go high." Next day this baby was lying near public well and was getting convulsions. By the lime we reached, the baby was dead. We didn't find the mother around. We were told that this 1½ year old baby was left at home to be taken care by the 4 year old elder child and mother had gone out. Our view about the stupidity of our people was further strengthened. Next time when Raibai came to our clinic we scolded her for such utterly negligent and callous behaviour. Then she told her side. She was a widow with 3 children. Exploited and harassed by everybody, she was leading an unsupported life. She was the only wage earner in the family and was waiting her elder son, [of 7 years then] to grow up and share her burden. When we advised her to hospitalise her youngest child, she had a difficult choice. If she had to stay in hospital with the baby for 1 week, the elder two children would starve. Elder children were valuable to her because they would reach the earning-age earlier. She decided to save elder children at the risk of the youngest one's life. At the time of the death of the baby she had gone to the fields to earn her daily wages leaving the baby at the custody of the cider sibling of four years age. Was it an irrational behaviour?

Munnakhan Munnakhan came to our clinic. He was found to have cardionyopathy with cirrhosis of liver Cause: chronic alcoholism. We admitted him to the hospital and gave him a sermon on ill-effects of alcohol. We tried to convince him that he had already wrecked his body by alcohol consumption and he couldn’t afford to continue to consume more. He seemed unconvinced. After improvement in his congestive cardiac failure we discharged him and he continued his alcohol. The question perturbed us - why doesn't he see a simple rational thing? Munnakhan told us that he was working as 'hamal' in railway godown on daily wages. There, the hamal had to load & unload wagons very fast as there was wagon shortage. In one day he lifted 500 bags of one quintal each. After such strenuous exertion, he said, the coolies had to drink alcohol to relieve tremendous body pain. “You can't work as a coolie in god own without drinking alcohol. We come out of godown in the evening with a crave for alcohol. The extreme tiredness and pain is unbearable. Body asks for alcohol?" Within few days he died

A few days later one of us, Abhay, had to spend 3 hours in a god own for purchasing and loading grain bags for a grain bank programme of Chetana-Vikas. It was a hot summer. The god own was tin roofed and was hot like an oven. Coolies were lifting bag after bag. Within 3 hours, when he was just standing; he had to remove his shirt thrice and dry it of the dripping sweat. He could not stand after 3 hours and went our home for some cold drink. Now, he realised what Munnakhan had said. Munnakhan was lifting bags of 100 Kg. each which Abhay couldn’t even move-and such 500 bags in a day. Such inhuman labour was not possible without alcohol. These human cranes were working on alcohol. Who was irrational? Munnakhan or we who didn't see the inhumanness of such labour?

Mahakal: Latrines Your entry into the village- Mahakal-where we ran a clinic-is greeted by a paradoxical sight. There is a row of 10 pucca public latriness built by the Government, years ago on one side of the road, and women and children defecating on road-side just in front of these latrines. Now this sight immediately creates an urge in us to deliver a health education sermon. We did it. No change. Then we realised that the people had to fetch water from about one Km. for their domestic use and drinking. Water was a precious commodity. Who would be willing to spend such precious thing in abundance for keeping these public latrines clean? So they found it much more sanitary and clean to respond to the nature's call in open environment where they could select a clean place than to sit in those public latrines full with faecal material. We also must mention two other reasons for the faecal behaviour of the people us found that women always defaccate on road side. This annoys us because we have to pass through that dirt while entering into the village. But women have their reasons. In the dark hours or in the rainy season, road and road sides are the only safe and dry places available. And one study has found out that women in the village have two occasions for socialisation - for talking with other women. One on the wells, when they go to fetch water and another while defaecating Public latrines being walled from all sides deprives women from this opportunity of chitchatting with other women. Hence they prefer open space.

Soak Pits Kanhapur has high prevalence of filaria. Mosquitoes breed all around houses and wells because there is no arrangement for drainage. So people were advised to dig soak-pits. Now the soil of Kanhapur is black

cotton soil of best type. It holds water like anything. All soak pits were chocked, filled and overflowing. It worsened the problem. People said that they were better without these.

Who needs Education? Now the question is who needs education? We talk from a scientific ideal pedestal. But the appropriate measures that we advice them are often inappropriate in their situations. It is our blindness that we don't sec these reasons and blame them for their noncompliance. It is our experience that we need to see the situation in which people live and understand the reasons for their behaviour before giving them any advice or health education. That is why we said in the beginning that health education is a very delicate and sensitive tool. In the absence of such precaution quite often our advice is as irrelevant as Sanjay Gandhi's. He had mugged up a 5 minute speech which he used to deliver everywhere. When he went in the thick forests of Baster he delivered the same sermon to the naked hungry tribals-plant more trees. Most of us come from a common type of background: educated urbanised, middle class life. We have our certain needs fulfilled and hence we see certain things as priorities. Cleanliness, education, health, morality and so on are what we sec as priorities - we throw these to people. People don't bother and continue their own pattern of behaviour because they have their own reasons and priorities. We blame women for not attending ante-natal clinic or under five clinics saying that the ignorant people don't realise the importance of prevention. But we hardly see that poor people are in such a neck to neck struggle for survival that immediate existence is more a priority to them than preventing future calamity. When the labourers eat in the evening what they earn in the day, how can a woman afford to remain absent from her work to attend an ANC clinic? Now, while saying all this we don't mean that whatever people do is all correct. They have been applying COIV dung on umbilical cord and killing the neonates/tetanus for centuries. This is obviously wrong. But the point is we need to see their reasons, compulsions before blaming them.

Who Needs to Change? So the question comes who needs to change? Brecht was a German dramatist and poet. When the Russian tanks rolled on the streets of Berlin to suppress the voice of the people to have more democracy, he sarcastically wrote - the party and the Government is always correct. People are stupid. This Government really deserves better people. So instead of people changing the Govt. the Govt. should get the people changed. We are not such a Government. So, we

need to change our methods and advices to quite a large extent to suit to the people's life and needs. Our action has to be comprehensive. Isolated health education and action becomes ineffective. We learnt it from Raibai. We started Balwadis and cretches so that mothers could go to fields leaving their children in safe care. We took up issues of minimum wages of the labourers. The increase in wages in cash didn't mean much in the face of rising prices of food grains. So we started Grain Banks. We undertook programmes like adult education, labour organisation, agricultural programmes to increase production - these are the premises of health action. Without these our health messages can not be brought into practice by the people. For doing this, we need self questioning instead of standard dogmas. People could be right. We could be wrong. Let us understand and examine. Another thing we need is - faith in the people. The old Chinese proverb tells us Go to the People, Live among them, Love them learn from them, Start with what they know, Build up on what they have.

LDC - WHO CARES? LDC

Less Developed Labeled Developing Logically Developed

Countries, Countries Countries

Luckily Developed Lynching Developing Legitimately Developed Lately Developed

Countries. Countries Countries. Countries

Later Developed Lunatically Developed Lasting Development

Countries

World Whose Wrong

Health Health Health

Western Western

Hospital Health

Western World WHO why

Health Helping Hatch Health Have

We

Well-being Heeding

Countries-

but they are just by us. They were and arc now decreasingly We may be busty who basically were Thus, both these and we- in many respects may become instead of

Countries WHO Organization But Organized is? Is it not the Organization? While not being any more Obsessed it promotes Output and therefore can be called Organization, is it a Organization. Does not Ostrich policy? Objectives Only? Maybe, Omitted in our plans that WHO should be a Organization? Jan Wind [Persp, Biol, Med, Winter 1981)

NUTRITIONAL BASIS OF MINIMUM WAGES [The December - 1981 issue of the Bulletin carried an article by Abhay Bang on nutritional requirements as basis of deciding minimum wages of agricultural labourers. Dr. C. Gopalan, the renowned nutritionist, has at our request sent his comments on this article Editor. ] I. The energy requirement of 3,900 Kcals for heavy work is for the reference man with a body weight of 55 kg. Studies from the national Institute of Nutrition (1, 2) have shown that the body weights of poor labourers engaged in heavy manual work is 44 to 46 kg and their daily mean energy expenditure is 3,025 Kcals. Thus, even with the current low body weights, the energy requirement of a male agricultural labourer will be 3.100 Kcals. We may assume that he is laid off work for 3 months in a year, when his energy requirement will be 2,200 Kcals (sedentary work, corrected for body weight). Thus, he needs 3,100 Kcals daily for 9 months and 2,200 daily for 3 months, or on an average 2,900 Kcals daily throughout the year. 2. Similar calculations for the adult female (corrected to 40 kg. body weight) will yield a figure of 2,200 Calories per day. 3. The minimum wage can only provide for a family size consistent with our national policy of a small family norm, namely only two children. We may assume both the children to be below 12 years. The energy requirement figures will, therefore, be 1200 for the child below 5 years and between 1500 to 2100 (mean - 1800) for a child between 5 to 12 years of age. The total energy requirement for the children will work out to 3,000 Kcals (1200+ 1800). 4. The total energy requirement per day for the family will be 8,100 Kca1. 5. I would suggest that the cost of this diet be based not merely on the price of the staple cereal but on a least cost balanced diet. In 1979; for an urban area, the average cost for such a diet providing 2,400 Kcals was Rs. 2-90 without including fuel expenses (3). On this basis the total family diet of 8,100 Kcals will cost Rs. 10-11 6. We may accept two-thirds of total income as a reasonable and realistic level for food expenditure. Thus the daily minimum wage for the family will be Rs. 16-00 7. We must make an allowance for lack of wages for 90 days since agricultural and rural labour are

usually laid off for such a period. We also have to provide for short periods of sickness. Therefore, the daily wage will be 16 x 4/3, or a bout Rs. 22-00 per day for family. It may be pointed out that many of the assumptions in the above calculations are based on an appreciation of the hard current realities and not on “idealistic" considerations. 8. From the above calculations, we may accept the minimum daily wage of Rs. 22-00 for family. If we assume that both the man and his wife will work, the minimum daily wage wil1 be Rs. 11-00 per person. However, certain realities must be accepted. The output of work of women in an occupation involving manual labour wil1 be less Therefore, the landlords may feel reluctant to engage women for the same wages. It may, therefore, be prudent to suggest a minimum daily wage of Rs. 12/- for the man and of Rs. 10/- for the women on the basis that different types of agricultural operations are involved. It must be clearly emphasised that this is valid only if both the man and the woman in the family are provided employment for at least 270 days in the year and preferably for 300 days. The minimum wage proposed should automatically fluctuate with the cost of living index, using 1981 as the base year. 9. The prescription of a minimum wage must also go hand ill hand with fixing a minimum norm for productivity and discipline. This is what has really made Japan prosperous). Those who vociferously demand higher minimum wages rarely talk of productivity. The norms for productivity must however take into account the small body size of the labourer, the result of chronic undernutrition. 10. There should be acceptance and enforcement of a small family norm. Two children and no more. I have therefore taken only 2 children (or family of 4) for consideration in computing daily wages. You may ask why talk of the 2 child family norm only for labourers and not the rich. In my opinion, this should be the norm for all, irrespective of religion, caste and socio-economic status. 11. Health and nutrition insurance cover for women and children of the families must form an integral part of the programme. With both parents at work children below 5 are bound to suffer. Therefore, the landlord must be required to deposit Rs 5/- every month for each labourer (male or female) employed by him, in the local bank for this purpose One Health

Dialogue Dear Friend I was deeply hurl and pained by the totally negative view about the multinationals in the Indian drug industry. (Anant Phadke, MFC Bulletin 73-4, Jan Feb, 1982). Anyone who paints a totally black picture has of necessity to neglect any evidence that may contradict his emotionally charged hypothesis. That is what Phadke has obviously done. I am working in a multinational research centre and I know the inside story. We have heavily concentrated all our research efforts on the tropical and parasitic diseases. A bunch of dedicated and brilliant scientists are working on hookworm disease, filariasis, malaria, amoebiasis, tuberculosis, etc. Did Phadke take the trouble to find out about such an effort before writing all his invectives and politically motivated remarks? We have already developed some remarkably effective

Worker per 100 families could be employed, to provide special health and nutrition cover to the family, reinforcing the existing health structure. A creche for underfives can also be set up. 12. On its part, the State must of course provide safe drinking water, reasonable environmental sanitation and basic health care. 13. There is also another aspect to this. If the cost of agricultural labour operations are increased to very high levels this is bound to be reflected in increased prices of food grains. This may disrupt the economy to the eventual disadvantage of the labourer himself. One way of overcoming this is to propose that half the wages may be paid in the form of food grains, at prevailing prices. It has been the sad experience that there is often a wide disparity between what the rural labourer is supposed to get and what he actually gets. The prescription of a “minimum wage will have only academic value if there is no machinery for strict enforcement. This is really the crux of the matter. J,

C. Gopalan Nutrition Foundation of India, New Delhi - 110049. [Original note does not contain emphasis-Ed.]

References 1. P.S.V. Ramanamurthy and R. Dakshayani (1962) I. J.M. R. 50: 804. 2. P.S.V. Ramanamurthy and B. Belavady (1966) I.J.M.R. 54: 977. 3. Ann, Rep. Nat'l. Inst. Nutrition, Hyderabad 1980.

and safe new compounds for tropical diseases. Would he care to find out? Then he would have given even ‘the devil his due.'

There are many Indian companies, who are family owned, very feudally run, profiteering and also influence and corrupt the bureaucracy and the politicians by lobbying and other means. Does Phadke know about them? I feel one must refrain from too many generalizations, glossing over details Notwithstanding Indian or multinationals, there are' good' and' bad' companies, , ethical' and 'unethical' companies, research - based' and ‘not research - based' companies. Let us learn to discriminate and not just join political slogan mongerers. Phadke said ‘It costs around 50 Million Rupees to develop a new drug, more so in case of tropical diseases since in this field a lot of ground work needs to be done '. And where does he expect the money for investment in research to come from? He has taken a stance of a political activist and does not care to go into the tremendous scientific effort required to develop new drugs, which have meant so much to human health and happiness. I hope MFC takes a more balanced and discriminating view of situations and not be radical for the sake of just being popular.

Ashok B. Vaidya Bombay-12.

We are glad to receive the above reaction from Dr. Ashok Vaidya, Firstly, because we have been worried lately that our readers have become so passive and indifferent that we are not seeing any response, any reaction to the material published. This 'angry' note is therefore really welcome. Secondly, this is the other side of the drug MNC coin, thought not much is revealed.

Obviously Ashok Vaidya is hurt by Anant's comments on research. In all fairness, Anant did not say then: was no research. He only said how little it was compared to the needs. More importantly, Anant never questionedy the dedication of the "brilliant" scientists working for the MNC. It would be foolish if one were to measure dedication, loyalty and patriotism by which sector a person is serving. There must be people in the public sector and in the government who are ready to trade off the country for a bottle of Scotch whisky. I do not think, with respect to this particular article, Anant can be labeled ‘radical' and ‘political activist’.

Maybe the subtitle, 'No positive role to play' is the only 'radical, political statement'- I Know this is mild, compared to Anant's radicalism. Readers should note, the data were not collected by Anant. These were presented at a seminar by various workers. I am not going into the question of whether MNCs should stay or go. It is an entirely different question. The reality is, MNCs are dictating terms to developing countries. To what extent is this inevitable and to what extent can a country, given the necessary political will, reverse, or minimize this? Yes, this is a ' political' question. But a certain element of such' political' thinking is necessary even among scientists and medicos, for the country's progress. Anant presented hard facts; he said the interpretations were his. Each one of us can interpret the data as we see it. MFC is not ‘radical' in the sense Ashok Vaidya means. How we wish it were popular!

Kamala Jaya Rao I would only add the following to what Kamala Jayarao has said above - Ashok Vaidya has not presented any argument based on objective facts to contradict I1lV arguments and data. Since he knows the "inside story", will he tell us how many multinationals contradict the arguments I have given by their good and ethical behaviour? As for Indian private companies, I have not used a single word in their praise. [See the last para of my article] Their role as well as that of the Govt. companies needs a separate discussion. It is rather sad that a long-time associate of M FC should fail to see things objectively and react only on emotional plane.

Anant Phadke

optimistic about? I felt that many have judged simply in the light of MFC's ability to exist for a long time. Is that all? I think we should review what role MFC has played so far and what more it could do. As far as the platform is concerned MFC has given the opportunity to socially conscious medicos to exchange views and to interact. As I understand the Bulletin and the annual meet are the means for this. If the purpose of the Bulletin is to serve as a medium for exchange of views I question how far it is successful in this and I feel that we, members have not yet properly identified the purpose of the Bulletin. There may be many reasons for this and we should make an effort to find them out The Bulletin itself can serve the purpose by inviting discussion. Apart from enabling us to get to know each other, the Meet hardly serves the purpose that it could. It is nothing unique that some old members with some new ones around are meeting yearly and discussing some topic independent of its social demand. In certain circumstance when exposed to social interactions it will show its hollowness unless we take care to shape MFC. I recall the situation at Jamkhed meet and how MFC had not the stamina to bear different views. Again at Tara meet no positive action programme resulted from the discussions. We may find mutual support in meeting together. But] think this has no value unless one finds the potential to reach a common ground leading to action, As a sure symptom of dissatisfaction, it is also a notable feature how some have lost interest in M Fe in the last five years. I very much hope that some positive discussion will come out of the comments I have made.

Anon Ganguli, Bihar.

Dear Friend This is a short note in which I would like to make some comments about MFC. I must say that this year's annual meet at Tara prompted me very much 10 express my views. To begin with I recall the meeting we had on the second day (24th) evening. The meeting started with the newcomers giving their impression about the meet but it ended with many members expressing their opinion about MFC and its future. As I gathered, most of the members were very optimistic about MFC's progress so far and some felt that MFC was' unique' in its existence and structure in respect to India's political scene. Personally, I don't think so. I do feel that the broad platform such as MFC has is an important feature and MFC has the potential to serve a purposeful role - but what is there that we can be

We are thankful to Marian Ganguli for his critical comments, Some factual clarification however needs to be made- A participant from Bengal remarked that in Bengal, persons belonging to different political tendencies can not sit together to discuss any common issue. He thought that it is remarkable and unique to see such a discussion in MFC in the light of his experience in Bengal. Nobody talked about uniqueness in respect of India as a whole. I had at some length, clarified the role of the Annual Meet and the Bulletin. 1 had clarified that apart from acting as a medium for exchange of views, the Bulletin acts as a propagator of alternative perspective on Health and as a medium to bind together likeminded people. This role of the Bulletin is atleast as important as the first one. With these corrections we invite discussion on the role of the Bulletin and the Annual Meet from readers.

Anant Phadke

ATTENTION PLEASE! Two important decisions taken at the VIIIth Annual Meet of MFC at Tara 1) Anybody can become a life - subscriber to the Bulletin by donating Rs. 250/- to MFC. Please send your cheque in the name of Medico - Friend Circle life - subscription. 2) MFC members will not accept gifts or samples from the drug - companies.

*

*

*

*

The Yusuf Meherally Centre, a rural development agency, needs a M.B.B.S. doctor for its health project. The Centre's activities are located in Panvel and Pen talukas of Raigad district, Maharashtra. A person with rural background will be preferred. Any doctor, with a desire to serve the rural messes, and having an appropriate philosophy for the health delivery system, and also capable of working in a team should write to The Secretary, Yusuf Meherally Centre, National House, 6, Tulloch Road, Apollo Bunder, Bombay – 400039

**

**

**

**

Bryan Osbon from U. K. was working in .a clinic in South India during 1969-72. He is still associated with this clinic and visits India once in three years. He writes"It has always been my intention to return to do something significant for the Clinic. On' my last visit we discussed the possibility of my doing a survey of the prevalence of hookworm, its geographical spread, demographic spread, facilities in the Estates etc. But basically its, a dead issue as far as the Government is concerned. A visit to the Chief Medical Officer for Madras State in 1978 left me horrified and flabbergasted. I with no medical qualifications other than small experience in the field had to sit and explain the problems associated with a disease that was fast approaching epidemic proportions in certain parts of his State: But I'm sure 'that will not come as a surprise to you. So if you have within your ranks anyone who might be interested writing to me with a view to joining me in a two or three month tour of the Estates in Tamilnadu and Kerala I would be interested in corresponding with them, to determine whether or not it is worth taking the idea any further arid trying to get some finance together for it. I'M firing this idea at you rather sharply without much of an introduction,

in

but if you think there is anything in it, I would be glad to correspond at much greater length with anyone concerned. " Those interested should write to him - 46, John Street, Brightlingsea, Essex, U.K.

**** Anil Agarwal writes from Delhi - "I was recently in Bangladesh and Zafarullah Chowdhury showed us some figures saying that a very large number of very serious cases coming to his hospital were women who had tried to poison themselves with insecticides. This was a sample of admissions that he analysed;

Insecticide Poisoning Diarrhoea Tetanus Abortion Eclampsia Pregnancy related problems

ADMISSIONS DEATHS 24 81 20 76 37 26

5 8 7 1 10 I

Does any one of us, especially those working in rural areas, too face a similar situation? Please publish any such information in the Bulletin ". [We will definitely publish any, such information sent to us from those working in the field. - editor]

**** Matter for publication in the Bulletin should be sent to Kamala Jayarao in her name. Do not write 'simply "Editor." Mail addressed not in her name' goes in the official channels of the institute Where she works and causes confusion, nuisance to her colleagues and embarrassment to her. [Her address is only for editorial correspondence. The rest should be sent to Anant Phadke.]

*

*

*

*

Health Educational Campaign against the use of Oestrogen - Progesterone for pregnancy-test was successful. Articles were published on (his issue in Hindustan Times, Economic Times, Deccan Herald, Sunday Observer and three Marathi dailies. Offprint of a Marathi article and the letter: Dear doctor/Chemist was distributed in Women's rallies in Delhi, Bombay and Pune. A short warning letter was published in many district newspapers in Maharashtra. These articles and letters got a tremendous response from readers. The campaign is still continuing join this campaign! Send your report.

FROM THE EDITOR'S DESK Some years ago I had written about the Myth of Protein Gap (MFC Bull. No.4; In Search of Diagnosis. MFC, 1977). Therein I had presented data to show that there is no primary protein deficiency in our country, not even, among children. I had said that the main bottleneck was lack of adequate food. If our people including children eat adequate amounts of the traditional cereal - pulse combination (rice and dal or roti and dal), they will automatically get all the protein they need. Therefore a major part of the problem of malnutrition will disappear. This does not mean that our diets do not need qualitative improvement. Our diets are deficient in vitamins and important minerals and this need to be rectified. But the point made in that paper was as far as protein goes, whatever deficiency there is can be overcome by just increasing food Intake.

This point has important economic and political implication" This would mean we do not have 10 strive to provide protein rich foods, like milk, fish, eggs etc. The country today is not in a position to meet the demand for these foods and any emphasis on this would mean importing protein concentrates. This would be (i) a waste of foreign exchange, and (ii) When an individual is not getting enough energy through his diet, the added proteins would be burnt off as fuel and not used for the purpose it is meant. In the paper I had also mentioned that vested interests would try to sell milk powder to our country stressing on a protein deficiency, which really docs not exist. They would also talk of the inferior quality of vegetable foods, the need for amino acid fortification of foods and thus try to sell amino acids to developing countries.

Editorial Committee: Anant Phadke Christen Manjrekar Ravi Narayan Ulhas Jajoo Kamala Jayarao, EDITOR

I have found it necessary to resurrect the paper and discuss it, because of a news item I read recently. It was published in "Asian Medical News, Feb. 1981. According to this report, a workshop was arranged last year to assess existing technology for the development of high protein low cost (HPLC) food supplements. It was attended by delegates from all ASEAN countries. Among the papers presented at the seminar were "The future of Soya bean milk in the developing countries" by Nestle, and "The role of powder milk in HPLC food" by Danish Turnkey Dairies. There were also papers by the American Soya bean Association. I hope you now understand what I meant by vested interests trying to propagate the high protein food idea and the myth of protein deficiency. In my paper I had said “There are countries in this world which produce more milk than they can consume. It is such parties who want to perpetuate the myth of protein malnutrition, trying t o export their produce to the developing countries." Soyabean production is also an interesting thing. I do not know the basic agricultural reasons, but Western Countries particularly the U. S. grow Soya bean but it is not consumed at least by the human beings there. The excess therefore needs to be exported. That is why I have said that this problem has economic as well as political implications. The report in the Asian Medical News also says, “The workshop's main recommendations were that a market study for HPLC foods should be conducted by marketing experts". One wonders whether the representatives of the industries were also on the committee that made the recommendations. Perhaps they were,

Kamala Jayarao


