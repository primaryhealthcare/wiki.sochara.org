---
title: "From The Editor's Desk, It Is Not A Fish... It Is Not A Fowl..."
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled From The Editor's Desk, It Is Not A Fish... It Is Not A Fowl... from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC260-261.pdf](http://www.mfcindia.org/mfcpdfs/MFC260-261.pdf)*

Title: From The Editor's Desk, It Is Not A Fish... It Is Not A Fowl...

Authors: Sathyamala C

260 medico friend 261 circle bulletin Nov-Dec, 1998

From the Editor's Desk

it is not a fish.... it is not a fowl..... MFC is an organization. No, it is a circle of friends. No, it is a thought current. No, it is not even a effective thought current. MFC should debate issues. No, mfc should act. No, mfc is only for Mitra Milan. No, mfc should take stands. MFC has missed the bus. MFC members are unfriendly. MFC is like a family. The Bulletin serves no purpose. Bulletin must continue. Close the Bulletin. The Bulletin is MFC's life line. Let us decide once and for all what is mfc. How can we decide once and for all what is mfc? MFC is not professional enough. MFC is too elitist. MFC which way to go, which way not to go. As one flips through the past 250 odd issues of the Bulletin, one may not get a clear idea of the who, the what and the whys of mfc but what one is left with is a sense of wonderment, that this group of highly individualistic individuals, assorted in every sense of the term, have been successful in keeping alive one of the few truly democratic, still autonomous forum in the postindependent India without compromising on basic principles. And the marvel of it all is that every time one thinks the end has come and one has prepared the obituary and friends and relatives have1"oregathered for the wake, a spring of energy and enthusiasm wells up from unexpected quarters and there is mfc revived enough until the next crisis. So what makes mfc tick? Let us start with some facts since even mfc members may find it difficult to argue with them! MFC began as a letter from a friend to a friend during

the ferment of the seventies inspired by the J.P. Movement. The first all India level meet was held in Ujjain in 1975. It was in the second annual meet at Sewagram that the decision to bring out a newsletter in a printed form was taken. Till then a cyclostyled paper was being circulated. Initially "a loose, extra constitutional, unregistered organization" of 'like-minded' people, it became a registered organization in 1977 with a formal executive committee. The Bulletin had been registered earlier in 1976 as a bilingual (Hindi and English) monthly newspaper. The red circle which has willy-nilly become the logo of mfc had, to begin with as Ashok Bhargav tells me, no greater significance than that it balanced well with the black on the cover page. Puma, from NID, Ahmedabad, who designed the front page intended that every month the colour of the circle should change. But Red it remained, month after month and year after year. Barring nine months, (Dec'85 to Aug '86) when the publication of the Bulletin had to be suspended because of ' some new government regulations, it has continued more or less regularly with, in addition to the printer's ink, the blood, sweat and tears of the successive editors publishers. Apart from the first year when the then convenor received a honorarium of Rs. 300/- per month, the organization has functioned solely on unpaid labour of its members, thus managing to survive as a truly 'voluntary' organization. There have been so far ten editors, and fourteen convenors, the only two 'posts' that carry certain responsibilities and accountability. The

annual expenditure in 1976 was Rs 7,020/- and in 1997 a little less than Rs 40,000/- inclusive of the cost of printing 96 pages of the Bulletin. In 25 years the organisation has an accumulated capital of Rs. 1, 53,500. Barring some occasional donations from friends and well wishers, the mfc has not actively sought any external funding to support it is core activities. While it is true that no splits have taken place, there has been slow attrition which cannot all be blamed on old age! Major policy decisions are taken in the General Body meeting except in extraordinary circumstances, the executive committee is empowered to act. All this might sound unexceptional and could very well fit some other special interest clubs as well. So, what makes mfc unique, a word that everyone seems to use spontaneously while trying to describe an organization that appears to defy definition. Well, this space is woefully inadequate to even begin to address this question. Hopefully, the deliberations during the annual meet will throw some light and may be we will be able to decide, once and for all, what is mfc. I had like to end with this delightful passage from Lewis Caroll's Alice in Wonderland. ".... She (Alice) was a Little startled by seeing the Cheshire-Cat sitting on a bough of a tree a few yards off. The Cat only grinned when it saw Alice. It looked good-natured, she thought: still it had very long claws and a great many teeth, so she felt that it ought to be treated with respect. 'Cheshire-Puss', she began rather timidly, as she did not at all know whether it would like the name: however, it only grinned a little wider. 'Come, its pleased so far,' thought Alice, and she went on, 'Would you tell me, please, which way I ought to go from here?' 'That depends on a good deal on where you want to

'In that direction', the Cat said, waving its right paw round, 'lives a Hatter: and in that direction,' waving the other paw, 'lives a March Hare. Visit either you like: they're both mad.' 'But I don't want to go among mad people,' Alice remarked. 'Oh, you can't help that', said the Cat: 'We're all mad here. I'm mad. You're mad.' 'How do you know I'm mad?' said Alice. 'You must be', said the Cat, or you wouldn't have come here.' " Happy Silver Jubilee! Sathyamala

*******

25 Years of MFC My relationship with the MFC has been rather strange. I have watched it closely all of the last eighteen years, and have at the same time, been at a certain distance. At the Varanasi MFC meet on medical education, which I attended along with Binayak, it became apparent that the old guard, Ashvin, Ashok, and others were trying to hand 0v:er the responsibility to newer people. Binayak was motivated by friends to accept the Convenorship of MFC, and he insisted he would only take it up if I shared it with him. He managed to railroad a faintly bemused MFC membership and a somewhat reluctant me, and over the next year or so I found myself handling the bulk of papers ' which came by road from Gujarat, the MFC correspondence, and membership files. It' was soon apparent that this period was a watershed of sorts in MFC. While the old guard was bound up in the idealism of

get to,' said the, Cat.

the Total Revolution, and were trying to find out how their

"I don't much care where — ' said Alice.

own particular skills as health professionals could be used in

'Then it doesn't matter which way you go,' said the Cat.

this larger scheme of change, whether or not health could be a

'— so long as I get somewhere,' Alice added as an explanation.

problems of relevance .of medical science and health care in a

'Oh, you're sure to do that', said the Cat; 'if you only walk long enough.' Alice felt that this could not be denied, so she tried another question. 'What sort of people live about here?'

valid 'entry point', newer entrants were grappling with the world that was rapidly losing" meaning. Making space for this interplay of ideas was a challenging experience. These debates were played out over the next few years, and a more pluralistic MFC gradually emerged. Out of this pluralism, other changes took place that led to a 'professionalisation' of commitment among certain sections of the MFC in even later

years, but this was connected with larger changes that affected the women's movement and the environment movement as well. Personally, the MFC experience taught me, while I still had time, the importance of disengaging from the shadow of a slightly larger than life husband, and taking my own route if I was to 'arrive'. All through my co-Convenorship, I was haunted with the said and unsaid vibes that I was a glorified secretary, and once when I tried to make a statement prefacing it with "if you take my own interest in health...”, there was a sniggering comment from a listener... "Yes, Binayak's health...". It is great to be able to say after all this time that these hurts did lead to some positive results in my own life, and that my friendship with many of the people in MFC has remained.

Ilina Sen. (former convenor)

no more, about vertical and horizontal programmes, but where is the medical and health field? It is once again the symbol and sign of the times that MFC, like many other activist groups, has become totally quiet. Is it because of helplessness or are we too dazed? Today what has to be discussed is not 'Whether Pokharan II is right, how important is AIDS, whether a secular government should sing Saraswati Vandana or not, whether a group should oppose Vande Mataram, or whether Jyoti Basu and Sonia Gandhi should become a team or not. We have to see behind all this, the cause- the malady. MFC has to rejuvenate itself. We asked, which way to go? And finding no direction perhaps, have buried ourselves. Today there is not much of the 'Medico' and the 'Friends' too have disappeared. What remains is the circle, designed by Ashok some twenty years ago, staring us in the face. But let us not think it is a sign for STOP; let us once again move.

So, MFC not only completes 25 yrs but has also survived!

It is indeed happy news that youngsters like the Vellore group (I have not met them but I assume they are young) have taken up the cross. But the core has to extend to them, not a guiding hand, but a hand of friendship.

Indeed a very happy occasion.

If I don't sound coherent, attribute it to my cerebrosclerosis. Love and Friendship,

*******

• We have always considered MFC a unique organisation. May be it is, may be it is not so unique after all. Indeed, that there were and are so many eccentric 'doctors' does make it unique. But MFC was a symbol of the times. Times when young people, born not too long after Independence were getting disillusioned with the policies of the government. And, some of us, young children when the Tricolour went up at midnight, saw our golden dreams vanishing. So there were activists not only in the health field, but in many others. Each group questioning and trying to find solutions. MFC was questioning the national health policies and medical care policies. But today there is neither a health policy nor a medical policy for the government. The government is very quietly but determinedly withdrawing from the scene. When MFC was born and was a fledgling, most of the best among doctors, despite what we considered their failings were still with government institutions. At that time I never dreamt even in the worst of my nightmares that the corporate sector would walk in the way it has today into the health field. The problem is no more of essential and non-essential drugs,

Kamala Jaya Rao, (Kamala Bahen of the darling forum) Former Editor

Announcement The theme for the next annual meet is

"25 years of MFC and the Health Movement" Date: 28-30 Jan, 1999.

Venue: Yatri Niwas, Sewagram, Wardha. Contact: Convenors' Office

Re-thinking Public Health, Food, Hunger and Mortality Decline in Indian History (concluding part) Sheila Zurbrigg

We can get glimpses of the continuing importance of access to food in twentieth century health experience. For the issue of hunger is by no means limited only to epidemic starvation ("famine"): and thus the hunger factor in health history is by no means limited to famine control. Endemic starvation (acute hunger) is also extremely important-at anyone time in society individual households slipping into destitution through sudden loss of work, illness, or death of individual 'breadwinners'. And then of course there is the enormous prevalence of chronic hunger (undernourishments) which takes such a toll on young children and is to a large extent linked with conditions of women's work, conditions which preclude adequate feeding and care of their children, including those inutero. We can get a glimpse of the toll of these additional (non-famine) hungers through a few examples from other regions. If I may quickly refer to several dramatic, and little known examples which approximate almost controlled studies of the relative impact of food security on life expectancy. The first is the experience of rising infant mortality, in Sri Lanka in 1974 as a result of the transitory disruptions in the national programme of food subsidies, consequent upon soaring international foodgrains prices in 1973-74. Wheat imports were affected most, and the impact was felt most acutely in the tea estate population which had adopted wheat as their primary staple. In Sri Lanka as a whole, crude death rate rose from 7.7 to 8.9. But much of this mortality impact came from soaring child mortality in the tea estates. Infant mortality rose 44 per cent in 1974 (from about 97 to 144 per 1000 live births), with post-neonatal death rate rising by 250 per cent (from 30 to 76 deaths per 1000. live births). As Isenman points oat, there was little if any change in health care or sanitation, making it difficult to explain the mortality increase except in nutritional terms26. The second glimpse relates to British civilian life expectancy trends across the decades of the two world wars (Table 1). These were periods marked by dramatic increases in many forms of government support including public employment, food price control and indeed food rationing. Winters has observed that the main drop in civilian death rates across World war-Ito account for this unprecedented rise in life expectancy occurred among the

working class. And that it occurred in spite of a tremendous increase in urban crowding during the war years and reduction in medical services (half of all doctors and nurses in Britain being recruited into military service). Security of access to basic staple foods (bread, potatoes and oatmeal) increased substantially for the civilian 27 population . Perhaps the most dramatic examples of decline in hunger (chronic and endemic acute) in relation to human life expectancy can be seen in relation to Revolutionary China, most importantly in the earliest years after the Revolution Fig. 9. As yet there has been almost no attempt to systematically study this relationship. The fact that these examples are simply "glimpses" reflects the unfortunate fact that modem (post-1950) health analysis has to such a large degree failed to pursue, measure and assess the impact shifts in hunger in relation to mortality trends. Table 1: Longevity expansion in England and Wales I Decade

Increase in life expectancy per decade (Years) Male

Female

1901-11

4.1

4.0

1911-21

6.6

6.5

1921-31

2.3

2.4

1931-40

1.2

1.5

1940-51

6.5

7.0

1951-60

2.4

3.2

Source: Based on data presented in Preston, Keyfitz, and Schoen (1972: 240-71). See also winter (1986) and Sen (1987e).

1993: Investing in Health Nor is health analysis in the 1990s immune to the reductionist germ focus. Indeed one might suggest we are currently in the throes of a reductionist onslaught, with pulse polio and pulse IUD campaigns reflecting only the tip of the iceberg. To what extent does the World Bank's 1993 expert report, Investing in Health, address the role of hunger, historically or present. Within the extraordinary maze of DALYs calculations, it is easy to fail to notice what the World Bank

6

document does with the issue of hunger as a subject relating to health. In fact, the subject of food merits only several sentences in the entire report. There is passing reference in its single paragraph section on "Lessons from the Past". Even here however food is quite secondary to modern medical techniques of disease control as a factor 28 in health history. Elsewhere again token reference is made to food security programmes (possibly at the insistence of third world consultants). But here again qualifiedrather tainted-with the suggestion that food subsidies are often not "cost-effective". Thus except for frank famine, government intervention in food security policies effectively is dismissed.

LIFE EXPECTANCY FROM 1700-1990 (Beijing, China) 80 70 60

misleading in terms of what else it leaves out. Now where in the ten pages of analysis is there any mention of Indian women as workers, and thus no mention whatever of the conditions of that work-in terms of wages, hours of work, flexibility of work allowing for or precluding adequate breastfeeding, security of work to permit claiming of such "benefits", women covered by maternity leave, creches, etc. factors anyone of which potentially affects child nourishment profoundly, including those in-utero. Indeed, one would never know from these pages that there was a single female domestic servant, construction worker, or agricultural labourer in India. Women's waged work is invisible, and thus also the conditions of that work conditions which so utterly preclude adequate feeding and care of their children. There is a single reference to women's "heavy workloads" but no societal context for that reality. Likewise, nowhere appears data on the extraordinary class differentials in birth weight, "nutritional” hunger stunting among the labouring classes. (Fig. 10) One might well ask what provision for child (breast) feeding existed for the women who washed the floors and served the tea at the authors' offices?

50

40 30 20 10

o 1700 1750 1800 1850 1900 1950 2000

Fig. 9

But what is also interesting guy to note is that the report is 44 careful not to reject the issue of hunger explicitly. It is sufficient simply to ignore it-a process with remarkable parallels in mid-nineteenth century Europe, where as Hamlin has recently documented, the "extraordinary narrowing of outlook" in the triumph of the sanitarian definition of public health ("Chadwickian orthodoxy") was achieved also by simply ignoring earlier understanding of 29 societal determinants of disease . The World Bank Health report is perhaps the most prominent example of current reductionist health analysis. But it is of course not alone in the field. A recent UNICEF document exemplifies the consequences of a narrow behaviouralist or cultural focus. In examining the causes of extremely high levels of child undernourishment, the 1996 Progress of Nations report focuses on the cultural devaluation of women's work, including of course child care. Such an observation is not in itself wrong. But it is frankly

Like hunger, it seems, a condition of labouring women’s work is something not talked about in polite company. It is only the end result-the clinical state, "malnutrition" with the societal dimensions removed so to speak-that qualifies for discussion. There is a parallel in current approaches to women's reproductive health, as a clinical state, the causal factors removed, as opposed to hunger as a social (and unnecessary) reality.

is central omission (of conditions of women's work and food security) also characterises the most recent WBIWHO scheme of women's reproductive health, which CSMCH 30 faculty here at JNU have so pointedly criticized . The point of course is that addressing, head-on, conditions of women's work is public health, but of a kind generally not found in standard textbooks of community medicine. Does this mean that the medical care is not a priority for the poor? That they can only be interested in economic concerns, filling their stomachs and those of their children? I am not sure how this question ever became posed in this way (though I have my suspicions), but once again it sets up an entirely false dichotomy. There can be little doubt that access to basic curative medical care saves lives and thus is of extraordinary importance to those individuals and households involved. The relative contribution of these saved lives to overall life expectancy trends is quite a separate question, but does not alter its importance to those concerned. All citizens (except it seems in the U.S.) have an absolute right to such care, a right which is by definition irreconcilable with market-driven health care services. Under existing levels of poverty and economic inequality, this right of access is of even greater importance for those living near or below poverty line (viz., bare subsistence). It is so precisely because of the far greater morbidity and risk of death from ordinary diseases that they and their children face, a risk which is a direct and immediate function \ of high rates of undernourishment. Children in severe (grade 3) undernourishment, have a ten-to-twelve-fold greater risk of dying than those of standard weight and growth like our own. This far greater sickness and mortality risk means of course a correspondingly greater need for curative care, along of course with food, than for the elite classes in society. But there is another logically unassailable argument for the importance of access to early and effective treatment of illness for the poor. It is of even greater importance because of the economically catastrophic impact of even non-lethal illness among those living at bare subsistence levels. Inability to work even momentarily throws households into economic crisis. I was vividly reminded of the cost of incapacity during my years working in a number of villages east of Madurai witnessing the not infrequent phenomenon of suicide' among men suffering from tuberculosis; such decisions were explained in terms of the insupportable economic burden of incapacity (a noncontributing mouth to feed) for the household.

This logic of course is equally true for preventive public health services in the mode of Chadwick-clean water, sewage disposal etc. The morbidity and mortality costs of inadequate services are so much higher for the poor, again, because of hunger-compromised resistance to disease in general. And where the great majority cannot hope to buy their way out of the failure of public health systems through bottled mineral water, like ourselves. The "debate" between preventive and curvative services is a false dichotomy as well. Again, I feel rather foolish making these observations which are eminently self-evident. Yet feel the need to do so when reporting these historical malaria research results which suggest the overwhelming role of hunger (acute) and shifts in food security in explaining so much decline in mortality. But just because acute hunger appears to have been of such import historically does not mean malaria treatment and prevention are not important public health issues today. They most certainly are, and most important of all for the poor. A clear and unassailable argument exists for reducing malaria transmission (though there can be considerable debate on methods) and assured and ready (rural) access to malaria treatment morbidity/ infection. For aside from saving individual lives, especially those of infants and young children, malaria infection represents a serious economic cost for the poor. Re-thinking public health? Let me add a final note, a view from the so-called affluent West. Among industrialized countries, it is increasingly clear that mortality levels and life expectancy are predicted not by per capita levels of wealth (GNP); even less by per capita health/medical expenditure. By far the strongest predictor of life expectancy among the affluent countries is distribution of income, that is, economic inequality. Those countries where wealth is distributed most equitably (or least inequitably) are those with the highest life expectancy levels (i.e. lowest premature mortality rates)". Furthermore, only a very small proportion of the large class differentials in mortality in a society such as Britain can be explained by "lifestyle" factors; that is, differences in smoking, exercise, diet. Rather, these class differentials lie overwhelmingly in the realm of psycho-social "stress" economic inequality and unemployment (or fear thereof) affecting profoundly self-esteem, sense of control over one's life, and in turn sense of insecurity, anxiety, personal failure and general well-being. It has recently been estimated, for example, that if economic inequality in Britain were to be reduced to levels prevailing in Sweden, life expectancy could be expected to increase by two full years representing a fall in premature

(under the age of65 years) deaths annually in the range of tens of thousands throughout society, the majority of course among the working classes. This would be health economics "cost effectiveness" to make the World Bank's head spin. It would also be "public health" in the original (pre-Chadwickian), and JNU, understanding. Notes & References: 23. The western European "model" of mortality decline "does not fit the experience of the third world in some major aspects .... Fall in mortality was not caused by a rise in private living standards nor even by an improvement in public health facilities and sanitation. It seems rather to have been caused by spectacular advances in the medical technology for controlling such bacterial diseases as malaria, smallpox, and cholera" (in A.K. Bagchi, 1982, The Political Economy of Underdevelopment, Cambridge University Press, p. 204). 24. T. Dyson, M. Das Gupta, "Mortality trends in Ludhiana District, Punjab, 1881-1981". Paper presented at the IUSSP Asian Demographic History conference, Taipei, Jan 1996. 25. Much research needs to be done in exploring the factors underlying the remarkable rise in physical survival, particularly in the immediate post-WW 2 years. It mirrors not only the celebrated pattern in Sri Lanka (also assumed incorrectly due to DDT), but of many other developing countries. And it seems quite possibly related to the transient boom and related increase in employment among primary producing (developing) countries triggered by favourable terms of trade. In other words, once again, economic factors. But this is the subject of an entirely separate talk:

26. P. Isenman (1980) "Basic Needs: The case of Sri Lanka", World Development, 240-42; S. Meegama (1980) "Infant and child mortality in the (Sri Lankan) Estates", Scientific Reports, No.8 (April), International Statistical Institute, World Fertility Survey. 27. J.M. Winters (1986), The Great War and the British People, Harvard University Press. 28. Not too surprising given that S.H. Preston who effectively took over where Coale-Hoover and Davis left off in the 1950s, appears to have been a key consultative voice-his name discretely appearing in very small print in the list of consultants. 29. The pre-Chadwickian understanding of the importance of "pre disposing" factors in explaining epidemic mortality in society, factors such as unemployment, destitution and hunger, "was not something that could be disproved". Hamlin observes, "but it could be ignored as the questions preoccupying medical inquirers shifted from a focus on the lives and wellbeing of individuals to general questions about the presence of particular diseases in particular populations and as questions arose of what were the most easily taken public actions that could make a significant difference in the incidence of disease" (in Christopher Hamlin, 1992), "Predisposing Causes and Public Health in Early Nineteenth-Century Medical Thought", Society for the Social History of Medicine, 5,1, April, 43-70). 30. I. Qadeer (996), "Women and Health", Political Environments, Summer-Fall, no. 4. 31. R. Wilkinson (1994), "Health, Redistribution and Growth", in A. Glyn, D. Miliband (Eds). Paying for Inequality: the Economic Cost of Social Injustice, 24-43.

•

Now the story can be told ... or, Opening the Pandora's Box Legend has it that when my photograph which adorned the place of honor in my maternal grand parent's home (I am their first grand child) was removed to make room for the newly born male grand son (first son of the only son), I went on a hunger strike (without having had the benefit of lessons on activism or empowerment!) till my photograph went back to its customary place and that of my cousin to a less central place as befitted his later entry into the line of grand children. I was all of five at that time. One can say, I was one of those 'instinctive' feminists (although in the post-photograph-episode period, it was varyingly termed stubbornness, mule headedness and other non-translatable terms in Tamil). Since my smallness (together with my femaleness) belies the steel within, it is not surprising that I am often under estimated and many have come to grief in their efforts, either deliberately or thoughtlessly, to put me down as a person, a female and as a human being. The month is January; the year 1980; the venue, Comprehensive Rural Health Project, Jamkhed, Maharashtra; the occasion, mfc annual meet convened to 'critically' examine the whys, and the wherefores of the newly emerging Village Health Worker's concept. My first live contact with the organization (I had met Mira Sadgopal, Binayak and Ilina Sen earlier). Being a bubbly sort of a person those days, and not knowing my place (I never seem to know 'my place'!) I participate with great zip and vigour in the august gathering (in the days of yore it was 'auguster' than now!). Also because I had just 'walked out' of Jamkhed after a eight months stint, I had 'inside' knowledge as it were, some of it not so very savoury, and wanted to share all of my insights with the group. At the end of a particularly exuberant interchange, as I came bouncing out of the room, full of joie de vivre, there was Luis Baretto, leaning against a pillar, a cigarette dangling from the corner of his mouth, smoke curling up towards the ceiling. Looking at me from 'under lazy eyelids' (a technique Bertie Wooster would have given his purple necktie to acquire) he uttered these immortal words: "Are you a nurse Sathya?" stopping me in my tracks. Without a moment's pause in the tone I reserve for put downers (those who put one down) I snapped, "Sorry to disappoint you, but I happen to be a fully qualified doctor". This exchange witnessed by a large majority of the 'old' male mfc-ites as they came trooping down the hall, set the tone and tenor of my interaction for the years to come. As mfc and I came eyeball to eyeball in that meet, so to speak, it was certainly not love at first sight from either side. Parenthetically I must add that only after many years had passed was I asked by one of the witnesses as to what had actually transpired and why I reacted the way I did!

We move forward a few years. It is 1985. For the first time in my association with mfc (I had become a 'regular' mfcites by then). I feel comfortable about taking on a major responsibility and I agree to be the editor of the Bulletin. An erstwhile member comes up to me and asks, "Are you sure Sathya, you are capable of assessing the matter for the Bulletin?" By now I have six years of experience dealing with this kind of attitude, and so, without batting an eyelid, in my sweetest voice, I tell him, "You know— think I am exactly the right person for this job because if I do not understand the matter, then surely, 99.9% of the mfc also will not be able to understand it, in which case don't you think it will be a waste of precious paper to print it?". You see, the Bulletin was now no longer for the "common man" and I happened to be the first editor without a 'post-graduate' qualification! The next annual meet. I am in my early thirties and age is beginning to show. The same erstwhile male mfc-ites remarks in a voice full of concern, "Sathya, tu tho bilkul ganji ho rahi hai!" (Sathya, you sure are getting absolutely bald). I take a deep breath, and reply "Have you seen .... (I mention the name of another male mfcites whose loss of hair is far more obvious) pate? Have you remarked on his baldness?" The friend moves away without another word. Ravi Duggal distributes sweets at the GB meeting in honor of his newly wedded state. Someone quips, "Hey Sathya, How about you?" ''Why?'', I want to know. "We can all relax!" Laughter all round. Since I do not know what to make of this and therefore unable to come up with a quick repartee, I hold my peace. (Touchy? Paranoid? Can't take a joke? Innocent fun? , Boys will be boys? Me thinks not). I could go on but these few examples selected for their blatancy and for being not too embarrassing for the parties concerned is to set the stage for the grand drama (played out in many Acts) of the pregnancy outcome survey. For those of you who came in late: In March 1985, as we were in the process of data collection for the first mfc survey in Bhopal, sitting on a scruffy little patch of grass in the middle of Anna Nagar Basti, Anil Patel, Ashvin Patel, Anant Phadke and I conclude that there is an urgent need for a study on the effect of toxic gases on pregnancy outcomes. Being the only trained public health person active at that time in mfc, Anil Patel agrees to design the study. I, who am deeply moved by the sufferings of the Bhopal people, and unencumbered enough to be the sole determiner of the use of my time, agree to coordinate it. Imrana Qadeer is roped in to help with the design. There is an enthusiastic response from several of the women's groups for participation in the survey. The situation in Bhopal is tense to say the least. In order to quash all forms of protests from the gas affected people, the MP government resorts to harsh repressive mea-

sures. Indiscriminate arrests are made and many are booked under serious charges like 'attempt to murder'. Even doctors dealing with purely medical relief are not exempt. The MP government declares all 'outsiders' to be terrorists. There is a spate of arrests just before the data collection for the pregnancy outcome is due. We are therefore forced to postpone the dates by two months. The first whiff of trouble comes when Anil Patel decides to stay back in Mangrol to be with an official from the World Bank who wants to discuss the Narmada Dam issue. I am forced to carry through with little or no support from mfc (Mira Sadgopal was the only other mfc member present during the survey period. Rani Bang who was in-charge of the gynae clinic for the duration of the survey had disassociated from mfc long ago). The data collection is completed successfully despite the CBI's attempts to intimidate the survey team. The grit and perseverance of the almost 100% women's team (there was only one male gynaecologist in the group), makes the survey possible. The data is tabulated (manually) and is now ready for interpretation. Anil Patel and I pore over the tables and in the later half of 1987 I begin to write the report in Mangrol. The second whiff of trouble comes when I realize that for some reason, Anil is no longer showing much enthusiasm. I shrug it aside thinking 'may be it is that time of the month!' Together we set dates by which the summary and the full report are to be published. I send the summary report to Padma Prakash for printing. Unfortunately, Anil does not come for the Annual meet held in Jaipur two months later. He gets hold of a copy of the printed summary report through his colleague Ashok Bhargava only after the meet. Anil dashes off a letter declaring that he was not party to the publication of the report and that the study was "full of bias" and that the mfc had made a grave mistake by publishing it. There is panic in the streets! I bide my time and wait for the mid annual meet in Wardha six months later to meet Anil face to face. He does not come. (In fact, after this episode he has not attended any of the meets). He must have had good reasons but till date the public is yet to be let in on the secret. At the Wardha meet I am asked to defend my position. I refuse to do so and state that I will only discuss it when Anil is present. I am adamant. I want Anil, in my presence to declare to the group that he was truly not party to the decisions that we both had made in Mangrol that I had carried out on schedule. In that meeting I make a fundamental mistake. I am so angry that tears start in my eyes. Oh Boy! Was I ever allowed to forget that. I was charged with everything from being emotional to being manipulative. The implied statement was that I was not "scientific" enough. It went on and on and on ad nauseum (I am beginning to feel a little sick as I think back on

those days). But I stick to my guns and refuse to bow down to superior wisdom. There is a flurry of letters (always a copy marked to me as well). New actors are brought into the picture. But my stand is clear. I refuse to get into this fruitless pseudo-intellectual exercise designed to nurture the male ego, for the "big bias" in the study was in reality not a bias at all and could easily be handled with a. biological explanation. Finally when it is clear that mfc [" dithering from making a decision, I leave the Alwaye meet (1989) without attending the GB with the note that if mfc is not able to decide either way, I will go ahead and publish the study with the help of the women's groups who had participated in the survey (I had already discussed it with them). I have still not been forgiven by some (Anant Phadke for one) for that act of insubordination (this time the accusations were that I held mfc to ransom and that I twisted mfc's arms'). Then there is the saga of how I was banned access to the data of the study I had coordinated when I needed it at a most crucial time in the history of Bhopal; and how mfc as an organization spontaneously tendered an apology in the Annual meet at Gandhigram for this inexplicable and indefensible act; how the report was published finally after resolving the 'terrible' bias that had apparently made the study worthless; how despite the presence of experienced journalists and seasoned publishers in the group, the production of the report published five years after the survey seemed a pathetic and amateurish (to use a charitable term) effort; how the study, the only well designed study on pregnancy outcomes in Bhopal, which had in fact forced ICMR to admit that there indeed had been an increase in spontaneous abortion rate • and gynaecological morbidity in Bhopal, never reached the indexed journals and therefore the larger medical community; how for years I carried the guilt and felt personally responsible for failing the gas affected women in Bhopal who had participated in the survey well knowing that they were not going to have any individual benefits (we had informed them that since we were surveying randomly selected households, it would not be fair to give the surveyed family their proforma); how I have had to leach out the bitterness in my heart so that it did not poison my vision of myself, of mfc and of people irretrievably; how during all that time the controversy raged, there was not a single voice among the 'friends' to ask openly why indeed did Anil not come; how I have had to wait for twelve long years to find the 'auspicious' time and the right mood (mfc's and mine) to put down on paper for posterity my version of that piece of history; how today, mfc claims ownership of data (technically within its rights) and the right to dispose of it as it deems fit, of a study it had almost come to the point of trashing. Sexism exists; so also casteism and communalism within the organization. Professionals dominate in this organisation also. Hindi chauvanism raises its head now

and then. How not, when we are all part of the larger society and have been culturally, socially, economically and politically influenced by the dominant forces? We should ask ourselves, why is it that we do not have a single nurse or a paramedical person in the "core" group. We should ask ourselves why is it that we have had only one woman convenor so far (I am not counting Ilina Sen). We need to take a hard look at ourselves without getting on the defensive. On the face of it the pregnancy outcome survey debate was all about "recall bias" but underneath it all was the ordinary every day kind of bias/prejudice we females meet at home, on the streets, at the work place and with our loved ones irrespective of one's class, caste, creed, race or language. The pregnancy outcome study controversy in reality was a clash between an 'Expert' against the 'non-expert' so called scientific outlook versus emotional outburst, a male versus female. And to cap it all, when I wrote a ten page note to a friend in the circle, someone who had spent enormous amount of time and energy in trying to resolve the impasse, describing my feelings and analysing the episode, his reply consisted of a pithy advice that I should get married! (May be I should write my memoirs after all!). He still continues to be my friend. It is the measure of mfc's strength that the issue was finally resolved along the lines of truth and justice and the organization that could have split or taken an authoritarian position did not do so. To my knowledge this is the only mixed platform where it is possible to raise such a terribly sensitive issue with the hope that it will make at least some to pause and think. It is indeed a measure of mfc's strength that I am still with mfc and mfc is still with me (albeit a little battered around the edges). I believe that mfc has an important role to play in the coming years and therefore it must continue to exist. That is the chief reason why I took on the responsibility for the bulletin at a time when it had for all practical purposes closed and the organization was in a moribund state. I have a strong gut level feeling (oops, here I go again I that in the next few years we in mfc are going to face a crisis around the issue of communalism; on the question of who is an Indian; on the rights of the minority to exist not on the largesse of a majority but as equal citizens less to none in the polity. It will be another litmus test. It does not require great powers of prophecy to predict this. It is the sign of the times we live in. Let us wait and see.

Sathyamala, Special acknowledgments to Ilina Sen for inspiring me to write this.

Improving Medical Waste Management in India These recommendations are meant simply as guidelines to stimulate better and more specific planning and action programs at the municipal government level and then at the level of individual health care facilities: They are based on observations made by Hollie Shaner, R.N. and Glenn McRae of CGH Environmental Strategies, Inc. of Burlington, Vermont, USA. Ms. Shaner participated in a national conference on medical waste management as an invited speaker of the All India Institute of Local Self Government in Mumbai. Mr. McRae made a 25 day tour (November 1997) of hospitals in Delhi, Mumbai and Calcutta, and met with municipal authorities and NGO's in those localities.

1. Clearly Define the Problem Before any clear improvement can be made in medical waste management, consistent and scientifically based definitions must be established as to what is meant by medical waste and its components, and what the goals are for how it is managed. If the primary goal of "managing" waste from medical facilities is to prevent the accidental spread of disease, then it must first be acknowledged that there is only a small percentage of the waste stream that is contaminated in a manner that renders it capable of transmitting disease, and that the only documented transmission of disease from medical waste has been from contaminated sharps (syringes, etc.). In the United States we differentiate the waste stream from medical facilities in three major categories: (a) Hospital Waste-all waste generated from a facility (including cafeteria, office, and construction wastes) (b) Medical Waste (A subset of hospital waste) — waste generated as a result of patient diagnosis, treatment, or immunization of human beings or animals—a subset of" hospital waste (c) Potentially Infectious Waste (A subset of medical waste) — that portion of medical waste that has the potential to transmit an infectious disease. It is category "C" that a medical waste management scheme must address first. The American Hospital Association (Robert Fenwick), indicates that this category of waste should not be any more than 15% of the total hospital waste stream, and a number of U.S. hospitals who have implemented good segregation programs have reduced this portion of their waste stream to less than 8%. Based on observations at a number of health care facilities in India we believe that the average hospital waste stream contains less than 10% of materials that could be considered "potentially infectious waste". * Establishing a clear definition of the type of waste that is seen to be a problem will allow for the development of a sound solution. If we utilize the definition proposed and documented above then the volume of waste that is

identified as a problem is only 10% of the wastes being generated at Indian hospitals and health care facilities. The solutions to look for must address the 10% first, and not treat all waste generated at hospitals as the same.

2. Focus on Segregation First The current waste management practice observed at many Indian hospitals is that all wastes, potentially infectious, office, general, food, construction debris, and hazardous chemical materials are all mixed together as they are generated, collected, transported and finally disposed of. As a result of this failure to establish and follow segregation protocols and infrastructure, the waste leaving hospitals in India, as a whole is both potentially infectious and potentially hazardous (chemical). At greatest risk are the workers who handle the wastes (hospital workers, municipal workers and rag pickers). The risk to the general public is secondary and occurs in three ways: (i) accidental exposure from contact with wastes at municipal disposal bins; (ii) exposure to chemical or biological contaminants in water; (iii) exposure to chemical pollutants (e.g., mercury, dioxin) from incineration of the wastes. No matter what final strategy for treatment and disposal of wastes is selected, it is critical that wastes are segregated (preferably at the point of generation) prior to treatment and disposal. This most important step must be taken to safeguard the occupational health of health care workers. Hospitals are currently burning wastes or dumping wastes in municipal bins which are transported We support the efforts of the Hospital Infection Society of India to establish clear definitions and standards in this area, and recommend the following resources as a base line in this effort: • World Health Organization publication "Managing Medical Wastes in Developing Countries" (WHOIPEPIRUD/94.1), edited by Dr. Adrian Coad. • Society for Hospital Epidemiology of America, Position Paper on "Medical Waste" by Drs. William A. Rutala (Division of Infectious Diseases, University of North Carolina Hospitals, Chapel Hill) and C. Glen Mayhall (Division of Infectious Diseases, University of Tennessee Medical Center, Memphis), published in "The Journal of Infection Control and Hospital Epidemiology, 1992 : 13:38-48. • Center for Disease Control, standards for management of infections wastes. Atlanta, GA.

to unsecured dumps. The waste contain mercury and other heavy metals, chemical solvents and preservatives (e.g., formaldehyde) which are known carcinogens, and plastics (e.g., PVC) which when combusted produce dioxins and other pollutants which pose serious human health risks not only to workers but to the general public through food supplies.' Imposing segregation practices within hospitals to separate biological and chemical hazardous wastes (less than 10%; of the waste stream) will result in a clean solid waste stream (90%) which can be easily, safely and cost effectively managed through recycling, composting and land filling the residues. This resulting waste stream has a high proportion of organic wastes (food) and recyclable wastes (paper, plastic, metal) and actually very little that is truly disposable, especially given the high percentage of reprocessing and reuse of materials which exists in the Indian health care system. Several model Indian hospitals have already set up segregation programs providing local examples of what is possible. If proper segregation is achieved through training, clear standards, and tough enforcement, then resources can be turned to the management of the small portion of the waste stream needing special treatment. This is not to minimize the need for resources to be allocated to assisting with segregation. Training, proper containers, signs, and protective gear for workers are all necessary components of this process to assure that segregation takes place and is maintained.

3. Institute a Sharps Management System Of the 10 percent or less portion of the waste stream that is potentially infectious or hazardous, the most immediate threat to human health (patients, workers, public) is the indiscriminate disposal of sharps (needles, syringes, lancets, and other invasive tools). Proper segregation of these materials in rigid, puncture proof containers which are then monitored for safe treatment and disposal is the highest priority for any health care institution. If proper sharps management were instituted in all health care facilities throughout India most of the risk of disease transmission from medical waste would be solved. This would include proper equipment and containers distributed everywhere that sharps are generated (needle cutters and needle boxes), a secure accounting and collection system for transporting the contaminated sharps for treatment and final disposal, and proper training of all hospital personnel on handling and management of sharps and personal protection.

4. Keep Focused on Reduction Indian hospitals generate significantly less waste than

U.S. hospitals. In part this is a result of a decision to maintain a system that relies on reprocessing and reuse of materials. Establishing clear guidelines for product purchasing that emphasized waste reduction will keep waste management problems in focus. New emphasis needs to be put on waste reduction of hazardous materials. For example, hospital waste management would benefit from a policy of a phase out of mercury based products and technologies. Digital and electronic technology is available to replace mercury-based diagnostic tools. This is a purchasing and investment decision. Since there is no capacity in India to safely manage mercury wastes, this reduction policy will make a serious contribution to cleaning up the hospital waste stream. This is one example of reduction strategies which could be identified and implemented in India. Practicing pollution prevention is the most cost effective way of securing public health.

5. Ensure Worker Safety Through Education, Training And Proper Personal Protective Equipment Workers who handle hospital wastes are at greatest risk from exposure to the potentially infectious wastes and chemical hazardous wastes. This process starts with the clinical workers who generate the wastes without proper knowledge of the exposure risks or access to necessary protective gear, and includes the workers who collect and transport the wastes through the hospital, the staff who operates a hospital incinerator or who take the waste to municipal bins, the municipal workers who collect wastes at the municipal bins and transport it to city dumping sites, and the rag pickers, who represent the informal waste management sector, but play an important role in reducing the amount of waste destined for ultimate disposal. Whether rag pickers are considered as part of the formal system or not, they are integrally involved in waste management and their unique role and personal needs must be considered. Proper education and training must be offered to all workers from doctors to ward boys, to labourer and rag pickers to ensure an understanding of the risks that wastes pose, how to protect themselves, and how to manage wastes (especially how to properly segregate). Education and training programs must be developed which speak to each population in a way that will best meet the needs and build understanding and change behaviour in that population. There is no "one" way to educate all workers.

6. Provide Secure Collection and Transportation If the benefits of segregation are to be realized then there must be secure internal and external collection and

~

The Hazardous Waste Stream Specific waste streams that any hospital or health care facility must examine in its assessment and planning process include.

Hazardous Material

Point of Generation

Point of Use and Disposal

Chemotherapy and antineoplastic chemicals

Prepared in central clinic or pharmacy

*Patient Care areas * Pharmacy *Special Clinics

*Incineration as RMW "Disposal as HW

Formaldehyde

"Pathology * Autopsy *Dialysis *Nursing Units

*Pathology "Autopsy "Dialysis "Nursing Units

Diluted and flushed down sanitary sewer

Photographic Chemicals

*Radiology "Satellite Clinics offering radiology services

*Radiology "Clinics offering radiology services

"Developer and Fixer is often flushed down sanitary sewer "X-ray film is disposed of as solid waste

Solvents

*Pathology "Histology *Engineering * Laboratories

*Pathology "Histology * Engineering "Laboratories

*Evaporation *Discharged to Sanitary sewer

Mercury

*Throughout all clinical areas in thermometers, blood pressure cuffs, cantor tubes, etc.

*Clinical areas

*Broken thermometers are often disposed in sharps containers ",'If no spill kits, are available, mercury is often disposed of as RMW or SW *Often incinerated

*Labs

*Labs

Common Disposal

Anesthetic Gases

*Operating Theater

"Operating Theater

Ethylene Oxide

*Central Sterile Reprocessing *Respiratory Therapy

*Central Sterile Reprocessing *Respiratory Therapy

Radio nuclides

*Radiation Oncology

*Radiation Oncology

Storage in secure area disposal by national atomic energy commission

Disinfecting Cleaning Solutions

Hospital-wide Environmental Services, Facilities Management, Operating Theater

*Diagnostic Areas

"Dilution, disposal in sewer

Maintenance: Waste Oil Cleaning solvents Leftover Paints Spent florescent lamps Degreasers Paint Thinner Gasoline

Maintenance

Waste gases are often direct vented by vaccum lines to the outside Vent exhaust gas to the outside

"Operating Theater *Facilities Management Maintenance

*Solid Waste *sewer

transportation systems for waste. If waste is segregated at the point of generation only to be mixed together by laborers as they collect it or if a hospital has segregated its waste and secured it in separate containers for ultimate disposal only to have municipal workers mix it together upon a single collection, then the ultimate value is lost. While worker safety may have been enhanced, the ultimate cost to the environment and the general public is still the same. In addition the very real concern of hospital administrators and municipal officials to prevent the reuse of medical devices, containers and equipment after disposal should be taken into account in any management scheme. One has only to walk by street vendors selling latex gloves, or using cidex (a disinfectant regulated as a pesticide in the US) containers to hold water for making tea, to understand the risk that unsecured waste disposal systems have. In addition, the practice of cleaning and reselling syringes, needles, medicine vials and bottles, is not well documented but appears to have enough informal evidence to indicate that it is a serious concern. Items that could potentially be reused illegitimately must be either rendered unusable after their use (cutting needles, puncturing IV bags, etc.) or secured for legitimate recycling by a vendor or system that can be monitored for compliance.

7. Require Plans and Policies To ensure continuity and clarity in these management practices, health care institutions should develop clear plans and policies for the proper management and disposal of wastes. They need to be integrated into routine employee training, continuing education, and hospital management evaluation processes for systems and personnel. In the U.S. the Joint Commission for the Accreditation of Health Care Organizations has been developing a set of standards on the "Environment of Care" which includes plans and policies for the proper management of hazardous materials and workers' safety, without which a hospital cannot be accredited. The USEPA's new MACT rule now requires that hospitals develop waste management plans, a requirement that many states have had on the books for several years. Municipal governments or state governments in India could require waste management plans from all hospitals as a condition for operating.

8. Invest in Training and Equipment for Reprocessing of Supplies The science of the reprocessing of equipment and materials for reuse in medical facilities is well estab-

lished in India and should be supported. The Hospital Infection Society of India firmly supports judicious reuse of materials, and should begin to set standards for reprocessing. Maintenance of this effort within hospitals will provide quality products and thwart efforts to increase reliance on disposables. Disposables are costly, increase waste generation, and do not necessarily provide for decreases in infection rates in hospitals. A reprocessing industry must however be supported with investment in proper equipment and training so that it is carried on in a safe and efficient manner.

9. Invest in Environmentally Sound & Cost Effective Medical Waste Treatment and Disposal Technologies The rush to incinerate medical waste in India as an ultimate solution to a problem without definition is doing a great injustice to the Indian people, public health, and the environment. Of the eleven recommendations that we are making, it is no accident in giving attention to treatment technologies as ninth. Without proper attention being paid to one through eight on this list, whatever decisions being made for treatment and disposal will be insufficient, if not counter productive. The mass incineration of hospital waste given current practices of waste disposal will not reduce risk to workers (this is where the greatest risk of disease transmission or chemical exposure exists) and will actually create a greater threat to the general public as mercury and other heavy metals are spewed out into the general air of India's cities, pr dioxins and furans are created from the combustion of plastics such as PVC which is growing in use in medical packaging in India. Additionally the ash generated from incineration of medical waste is also tainted with heavy metals and other toxic residues. Lesser risks are associated with the treatment of unsegreagated wastes through other treatment technologies such as autoclaving, hydroclaving, microwaving and chemical disinfection, which affect workers more than the general public, and contaminate water sources rather than air if improperly operated. Choices of treatment technologies should be made in line with a clear knowledge of the waste stream to be managed and the goal to be achieved through treatment. If the technology is to be environmentally sound, the waste stream should be able to be treated (disinfected) without creating other hazardous by-products. Incineration may be an "overkill" technology. Its goal is sterilization, not disinfection. One has to ask the question as to whether sterilization is necessary, or if the goal is simply disinfection. Is achieving sterilization worth the cost of transferring the risk from a potentially "infec-

tious" material to a clearly hazardous chemical one? If the overall goal of waste management is to prevent disease transmission from waste products, then the emphasis should be placed on the "management" aspect of the process and not on the "technological fix" which time and again has proven to be an expensive diversion rather than an effective solution. Technology should fit the situation and work in the management system to achieve the final goal as part of the overall system, not as a replacement for the system. Technology choices will be made to meet local needs and conditions and cannot be uniformly applied throughout a state or country. National standards for operating acceptable treatment technologies should be set, and there is no reason for India to have standards any less stringent than those being modeled in the U.S. or Europe.

10. Develop an Infrastructure for the Safe Disposal and Recycling for Hazardous Materials There was little or no observable capacity for the management, treatment, recycling or final disposal of hazardous wastes in India (e.g., chemicals, mercury, batteries). Hospitals seeking to segregate hazardous wastes are left with little or no option for safe disposal. The development of an industry in India which is capable of managing hazardous waste (chemicals) is essential. Onsite reprocessing technology is available for hospitals for materials such as xylene or formalin, and recovery technology for silver from developing solution. These technologies may be cost prohibitive at this' time. Pollution prevention and the choice of nonhazardous or less hazardous material is the only real option left to hospitals, which should be followed regardless of the existence of a hazardous waste industry. As a result of a lack of waste segregation practices in most hospitals, many of these hazardous materials are mixed into general solid waste for disposal in municipal bins or are mixed into wastes which are incinerated as potentially infectious waste. In either case they represent a serious health hazard to workers and the public. At this time even if they were segregated the lack of real alternatives to properly dispose of them would mean that they would be stockpiled, potentially creating yet another threat.

11. Develop an Infrastructure for Safe Disposal for Municipal Solid Waste Improper disposal of all wastes, municipal solid waste, hazardous wastes, industrial wastes, human wastes, etc. poses a major health hazard throughout India. The development of sanitary landfills, sewage treatment plants and other waste management facilities is necessary to securing public health in the country, and providing for the ultimate safe disposal of those wastes

this cannot be otherwise recycled, composted or reused. Studies of the municipal waste stream in India conclude that approximately 50% of the wastes generated are organic and could be composted, another large segment includes easily recyclable materials, leaving a relatively small portion requiring actual disposal. Just as in the discussion of medical waste management, proper segregation and pollution prevention, combined with a clear definition of the problem and the goal will provide the best, most environmentally safe and cost-effective solution to waste disposal. Also again, proposals for large mass burn incinerators for the general mixed waste stream, not only do not address the real problem but are burdened with numerous "side effects" which render their real value as a negative. Health care facilities need to be able to tie into a municipal system of proper waste management to ensure that they are meeting their mission of providing for the public health. Until such an infrastructure exists there are numerous decisions and actions that any hospital can make (listed above) to begin the process of improving their waste management practices and ensuring public health and worker safety today.

***** I work with a non-profit research organisation in the U.S. called the Multinationals Resource Center. We research the activities of multinational companies and related interests and disseminate the information to health and environment advocates in less-industrialized countries. We are currently involved in tracking the medical waste incinerator industry as it search’s for new markets in Third World countries where the public and government officials are less aware of the immense dangers associated with incineration. I enclose an incredibly useful document produced by Hollie Shaner and Glenn McRae, two members of the Health Care Without Harm campaign in the United States. Both Hollie and Glenn visited India last year to work with health and environmental activities to promote safer and less-expensive alternatives to incineration for dealing with medical waste problems, This paper explains the issues clearly and proposes a viable, economical and responsible system for dealing with medical waste. An 'e-mail list serve has been established for people interested in medical waste issues in less-industrialized countries. The list serve has regular postings of scientific and policy developments relating to medical and municipal waste incinerators and is a forum for discussion among health professionals and advocates in Southern countries. To join, send an e-mail introducing yourself to Ann Leonard at aleonard@essential.org. Ann

Leonard,

Washington DC, USA

•

Very high child mortality in the tribal population in Gadchiroli Abhay and Rani Bang

.something unusual happened recently in the Gadchiroli district. the district Collector (now transferred) Mr. Nitin Gadre, 1.A.S., tried to estimate the child mortal it rate in the tribal population of the district. This was earlier reported by SEARCH (Society for Education, Action, & Research in Community Health), Gadchiroli, to be high; and the issue was brought to the notice of the chief Minister, who, in turn, inquired about the validity of the SEARCH's report. Such inquiries are usually lost in the government files. However, Mr. Gadre sincerely tried to estimate the child mortality in one tribal block, Aheri. In the absence of an effective system of complete reporting, he used the existing government staff and assessed the births' and child deaths during two monthsApril and August '98. Each death was verified by revenue officers and a Panchanama was made. The findings, officially reported by the Collector to the Govt. of Maharashtra recently, were as follows: Aheri block No. of reported births in the previous year (96-97) = 2858 Mortality rates in '98

April '98 August '98

Still Birth Ratel1000 births

31

105

Infant Mort. Rate/1000 live births

103

132

Child Mort. Rate (0-5 Yrs) / 1000 LB

119

209

The survey covered only two months, and there were few minor technical errors in the method of estimation. Hence, the findings are not very precise estimates but are, sufficiently indicative. But just contrast these with the

regular reported deaths in the same Aheri block for last five years (1992-97) by the Health Department. Govt. Report Report

Collector's

Mean of

Mean of

Apr & Aug '92-97

Apr and Aug '98

Still birth rate Infant

4

68

mortality rate Child

13

118

mortality rate (0-5 years).

20

168

We enclose a table which gives the actual number of child deaths in the Aheri block reported during 1992 to '98 by the Health Department. Notice how the number jumped up in the year 97-98 because the Collector started a parallel watch through the revenue department, It may be that such hidden mortality is occurring in other tribal areas as well, and the reported official figures of child mortality creates false sense of all is well. Parallel recording of child deaths by another agency or an NGO will reveal the true picture. . We are working further on this issue. Two questions have been posted for the coming session of the Maharashtra Assembly. We are again meeting the Chief Minister to discuss the problem and the possible actions to reduce the child mortality in the tribal areas. Meanwhile, if you can circulate this information, more NGOs, and more people will become aware and start investigating the child mortality.

Year wise vital events of Aheri Block S No. Years

1. 1992-93 2.1993-94 3.1994-95 4.1995-96 5.1996-97 * 6. 1997 -98

No. of Births

Still Births

0-1 yrs. Death

April

Aug.

April

Aug.

April

Aug.

31 33 109 133 45 225

132 57 119 228 174 257

1 1 5

1 I 14

1 1 9

3 _ 1 29

1-5 Yrs. Death

Maternal.

April

Aug.

April

_ _ 1 2 _ 3

_ _ _ 3 1 23

_ _ _ _ _ _

_

Death Aug.

_ _ _ _ _

Zilla Parishad Data

(N. B. Data based on C.R.S. & H.M.I.S. sources)

SD/District Health Officer. Zilla Parishad, Gadchiroli.

* O.

1997 -98

Revenue Survey Data

225

257

7

27

21

30

5

IX

00

00


