---
title: "If There Are No Side Effects, This Must Be Argentina"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled If There Are No Side Effects, This Must Be Argentina from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC073-74.pdf](http://www.mfcindia.org/mfcpdfs/MFC073-74.pdf)*

Title: If There Are No Side Effects, This Must Be Argentina

Authors: Health & Society

medico friend circle bulletin

JANUARY-FEBRUARY 1982

Multinationals In Indian Drug Industry —No positive role to play —Findings of a seminar I was one of the participants of the Seminar- The Drug Industry and The Indian People-held at Delhi on 7th and 8th November 1981. It was organized by five different organizations of scientists, medicos. Substantial amount of concrete material was presented in this Seminar on how the drug industry, especially the Multinationals is deceiving, exploiting the people. In this article, I have tried to present in a somewhat coherent manner, some of the most important facts presented in different papers in this Seminar. I have also used a couple of other sources. I bear the responsibility of interpreting the facts and figures. The authors of these papers or the organizers of this seminar may not agree with my interpretation.

Anant Phadke

Monopolistic Structure The Pharmaceutical industry all over the capitalist world is controlled by a few giant corporations, “In 1974, the top 30 multinational firms accounted for 52 percent of the total sale of pharmaceutical products in the world. In 1973 the top 20 firms accounted for over 75 per cent of the total ethical drug sales in the U.S.A. and the U.K. Individual enterprises tend to specialise in sub- markets leading to a concentration within product classes. For instance in 1973 according to Roche's own estimates, their two main tranquilizer formulations - Librium and Valium help more than a third of the entire world 1 tranquilizer market." ( ) These giant corporations can apply the latest fruits of scientific, technical research because they have the resources to do so. They can also set aside large sums of money to do research for newer, better drugs. But since their primary motive is to maximize their profits, this potential is not realized properly. Instead, their strength is used mainly to manipulate things to serve their narrow profit-interests. This is clearly seen if we see their role in a developing country like India.

The Pharmaceutical industry in India has been dominated by the giant foreign companies mentioned above. Even after 30 years of Independence, their domination continues. In India, "While the value of drug production increased from Rs. 10 cr6res (almost solely formulations) in 1948 to Rs. 445 crores in 1973 and Rs, 1376 crores Rs, 226 crores bulk drugs and Rs 1150 crores formulations) in 1979-80, the share of MNC subsidiaries and minority ventures still remains substantial. In 1973-74, 60 firms with foreign shares accounted for 70 percent of the country's total drug sales. The remaining 30 percent was shared by 116 large and 2500 small manufacturing companies" 2 ( ) Since most of the research in Pharmaceuticals in the commercial sector of the capitalist world is done by giant multinationals and since pharmaceutical industry is protected by patent laws, 90 percent of patents in the pharmaceutical industry in India are also held by these foreign controlled companies. What are the ill effects of this commercial, profit oriented, giant monopolistic sector? Let us know about these one by one.

Emphasis on drug formulations Production of bulk drugs requires setting up complex manufacturing units here in India. But foreign companies, which started here as marketing subsidiaries of their giant parents, are not interested in this. They have been forced by circumstances into manufacturing activity. But this mainly consists of importing bulk drugs from parent companies and merely mixing them together in various proportions to make various formulations with particular brand names. In 1978-79 out of a total production of Rs. 220 crores of bulk drugs in India, the foreign sector accounted for 16.7 per cent, whereas out of Rs. 1050 crores of drug formulations, it accounted for 43.8 per cent worth of 3 formulations. ( ) This affinity for formulations exists because a formulation means more profit. The average profitability [pre-tax] of four foreign companies during 1974-77 was 7 per cent for bulk 4 drugs and 21.8 per cent for formulations. ( ) One of the main reasons given for allowing the foreign companies to operate in India is that these Companies will bring their complex technology with them and thereby help setting up a modern, manufacturing drug industry in India. Though this has happened to a certain extent, the main effect has been to thwart the development of modern drug industry in India. Since the foreign' companies are the prime movers in the drug industry in India, Indian private companies also indulge mainly in production of formulations Thus in 1978-79 these Indian companies accounted for 22.3 per cent of bulk drug production and 32.4 per cent of formulations. The Public Sector however, produces 14.6 per cent of bulk drugs and only 5.7 per cent of drug-formulations. 5 ( )

Social Waste As we know, many of the formulations in the market are useless on account of either unnecessary, wrong ingredients subtherapeutic dosages, wrong combinations ere. It is estimated that “out of Rs 1260 crores worth of drugs manufactured in our country in 197980, essential and life saving drugs accounted for kg. 350 crores only; the rest were pick-ups, Tonics and formulations of marginal value" ( G)The WHO, the Indian Medical Association and the Hathi Committee have recommended respectively 200, 156 and 116 essential active substances as essential drugs. The WHO has in addition recommended 30 complementary drugs for treating rare disorders. To this list could be added a few rational drugcombinations. As opposed to these about 250 (at the most) drugs, in India, about 15000 formulations are being marketed under' different brand names. Most' of these are

repetative. An analysis of 289 manufacturing units (accounting for over 85 percent of drug-production) showed that in 1972, these units were marketing 244 multivitamin-C preparations, 262 7 Vitamin B complex tonics and 126 cough syrups. ( ) Since none 01 them are better than any other, the main way of selling these brands is high-pressure advertising and marketing. This advertising pushes up the price to be paid by the consumer. “According to one estimate, as much as 18 per cent of turnover on an average is spent 'by Pharmaceutical firms on sales promotion in 8 India..” ( ) In case of foreign drug companies, this expense is even more... "in the case of 24 foreign drug companies studied, overhead costs (including sales promotion expenditure) amounted to 33.32 percent during 1974-77 as opposed to an average of 20 9 percent in other industries." ( ) These expenses are a huge social waste they are however necessary for the drug companies for monopolistic competition amongst themselves.

Irrational combinations To justify a different brand-name, drug companies many times add some ingredients to the essential drug. Most of the times these additions are irrational. A subcommittee under the Drugs Consultative Committee stated that of the 34 categories of fixed combinations examined, 23 categories were to be weeded out. We know many examples of irrational combinations. But it would not be out of place to quote a couple of scandalous combinations- “It is well known that Analgin causes serious blood dycerasias as well -as gastric ulcers. Phenylbutazone and oxyphenbutazone are equally hazardous drugs. But a combination of Analgin and Phenylbutazone achieves a record sale of over Rs. 2 crores within a year of its introduction ... Amidopyrine is a very toxic drug that is banned the world over; but most of our antispasmodic combinations contain amidopyrine. "In 1979-80 we imported 95 tonnes of 11 Amidopyrine. ( ) Because of their monopoly-control, leading manufacturers can dump these products into the consumer's body; doctors virtually acting as agents of these companies.

Not for the poor Because of the brand-names, advertising, unnecessary ingredients and high profit-margins, most of these combinations are too costly for the vast majority of our population. The drugs that the poor need drugs against tuberculosis, leprosy etc. and also vaccines are under-produced [see table No. 1 and 2] because those who need these do not have the money to buy them. Even amongst the Vitamins, a similar pattern is seen. Vit. B complex preparations of various sorts

consisting mostly of irrational combinations consumed by the rich account for 5.5 percent of total drug production. However Vit-A, the deficiency of which is extremely widespread (and turns 12000 children blind every year) amongst the poor; accounts for a mere 0'3 percent of total drug production. This is even less than the 14 production of Vit. K and other such elements! ( )

“The drug firms, when they find that the profitability is Jess, do not use the licenses and letters of intent granted to them. It was reported that of 32 bulk items covered by 13 licenses, 21 items were not produced by Glaxo Laboratories during the last five 16 years."( )

Very little research for the poor 12

TABLE No.1 ( ) 1978 Name of the drug

installed capacity tonnes

Production tonnes

1.Isonex

539

94

2.PAS and its salts

1290

558

3.Thiacetazone

153

13

4.Streptomycin

257

225

5.Chloroquine

176

45

38

17

6. DDS and its derivatives

WHO recommended oral electrolyte powders are hardly ever available in the market. The one that is sold maximum is Electral; but it does not conform to the WHO formula.

Out of a total world production of drugs of 50 billion, the developing countries in 1974 imported about 2.1 billion's worth i.e. about 42 percent. But out of an estimated annual research bill of 2 billion, only 30 million i.e. 15 percent was spent by the companies on the Tropical Diseases which constitute one of the most pressing health-problems in developing countries. This amount is equivalent to the “cost of building a few miles of motorway "says WHO, and is less than one fiftieth of the annual expenditure on 17 cancer research. ( ) Whatever research that is being done by Western agencies on the tropical diseases, takes place in developed countries and is focused mainly on the problems which they are concerned with. For example, The US Walter Reed Army Research Institute is the only Western agency doing systematic research in malaria. It got interested in malaria because of the U.S. involvement in Vietnam where Malaria caused more 18 American casualties than did the Vietcong army. ( )

13

TABLE No. 2. ( ) 1980-81 (estimates) Vaccine against

Target

Production

lac doses

lac doses

Diphth; Pertu; Tetanus Diphth; Tetanus

400 250

145 120

Tetanus

210

70

Poliomyelitis

60

20

As against this, the drugs consumed mainly by the well-to-do or pushed by the doctors for their own interests (for example-In]. Terramycin) have been produced beyond their licensed capacity. Table NO.3 gives figures for Pfizer Ltd. l5

TABLE No. 3. ( )

Product

Licensed capacity metric tonnes.

Production during 1979 metric tonnes

INH

80

52

PAS and its salts

110

94

Terramycin

14

54

Protinex

110

290

In India, out of 45 foreign companies identified by the Hathi committee as under the Foreign Exchange Regulation Act, FERA only 7 companies performed R and D in the manufacture of basic drugs. An analysis of 20 multinationals in India showed that during 1974-1975, the R&D expenditure of these firms ranged between I' 5 to 2' 5 percent of their sales turnover. Whereas their parent-companies in the West spend typically between 5 to 15 19 percent of their annual turnover on R&D. ( ) The Sandoz group as a whole spends nearly 9 percent of its worldwide turnover on R&D, while its Indian subsidiary spent only 14 percent of its 20 turnover on R&D in 1975. ( ) The reason for this behaviour is simple. The multinationals "can not afford" to spend on research on drugs to be used by the poor; the poor being unable to pay for the research through higher prices for new drugs. This state of affairs is not going to change unless the profit motive of the drug-industry is abolished, unless human needs take a priority. Drug research is now no more a virgin ground. Now it costs around 50 million to develop a new drug, more so in case of tropical diseases since in this field lot of groundwork needs to be done first. The multinationals are

not going to change their research strategy unless strong public pressure forces them to do so.

MNCs not needed in India Even the fruits of the research done in the Western countries do not percolate quickly through their subsidiaries here. This has been shown by a study made by B. V. Rangarao. [See table no.

4.]

21

TABLE No. 4. ( ) Name of drug

Year of produc-

Year of produc-

Sulfadiazine Sulfathiazole

tion abroad 1940 1939

tion in India 1955 1955

Tolbutamide

1956

1960

Penicillin-G

1941

1955

Streptomycin

1947

1963

Chloramphenicol

1948

1957

Prednisolone

1956

1963

Out of 138 drugs listed as major Pharmaceutical innovations from 1950 to 1967, only 20 were being manufactured in India in 1973. Now in the West in spite of growing expenditure on drugs research, less and less genuinely new drugs are being innovated since the scope for newer drugs is becoming less and less for the developed societies. Thus in 1974, out of the 1500 drug patents filed in 1972, only 45 (3 per cent) were “genuine new drugs," 150 (10 per cent) were “major modifications," and the remaining 1305 22 (87 per cent) were purely imitative. ( )This means that the postwar period of explosion of new, better drugs is over Therefore one of the important arguments in favour of allowing the multinationals to continue here- 'they bring new, better drugs' - is now less v lid than ever it was. As of today, the Indian drug industry is technically competent to produce most of the drugs that are being produced here. In 1977, there were 64 foreign controlled firms of which only 38 produced bulk drugs numbering 207. Out of these 207 bulk drugs, 93 were produced exclusively by the foreign companies. The Indian sector [private and public] was also producing the rest of these 207 drugs. Out of these 93 drugs, only 29 were' high technology drugs,’ the production of which probably can not be 23 taken up by the Indian sector because of lack of know-how. ( ) The Indian sector has the technology for producing the remaining fi4 drugs currently being produced exclusively by the foreignsector. Amongst the 29 'high technology drugs' some may be closely related chemical analogues. In that case, this number will go down. Further, ‘high

technology' has become a tricky word. The committee on High Technology constituted by the Government has in its report [October 1979] included even the following in 'high technology' use of potentially explosive materials; use of toxic materials; 24 careful on line controls! ( ) Even in case of the drugs which can not be produced by the Indian sector, Indian companies can enter into technological collaborations (as they have been doing currently) with foreign companies. It is not necessary to allow foreign companies to operate in India. Thus on

technological grounds it is not at all necessary to allow foreign companies to operate here. Pumping off money out of India MNCs are not only necessary, but they also have deleterious effects on our Industry and the people. Some of these have already been outlined above. In the economic field, we find that I\1NCs have been pumping off money out of India, firstly through repatriation of profits and secondly through "transfer pricing."

A case study of 42 foreign drug companies showed that during the period 1968-69 to 1977-78, these companies repatriated Rs. 45.11 crores out of India in the form of profits, 25 dividends, royalties, office expenses etc. ( ) This means that though these companies earn huge profits by exploiting cheap labour in India, they do not reinvest all of it here. Repatriation of profits is only one of the mechanisms. The other mechanism is “transfer pricing.” The subsidiaries of multinationals import raw materials from parent companies at rates higher than the prices in the international market. This raises the prices of final products and thereby pumps off money from the pockets of our people into the coffers of the parent companies. A systematic study was made by Chandrasekhar and Purkaynstha to calculate the amount of money being transferred through this mechanism. In case of the 29 foreign companies for which they could get sufficient data, they found that the outflow through transfer pricing was an estimated 20 to 40 per cent more than the outflow through repatriations during 1977. A couple of individual examples will give a concrete idea about transfer pricing. A forcing subsidiary charged Rs. 60.000 / Kg. for dexamethasone which was later reduced to Rs. 16000/ Kg at the intervention of the Controller of Imports Gentamycin was being imported into India by a multinational subsidiary at the rate of Rs. 45000 / Kg. When import of some drugs was (Continued on page-11)

"ALLO-AYURVEDOPATHY" A NON-SCIENTIFIC HYBRIDIZATION A look at any drug directory will show that there are dozens of companies producing herbal and herbomineral drugs. The drugs are perhaps manufactured according to Ayurvedic or other non-allopathic principles of therapeutics. The drugs are however not marketed as traditional pills ("goli"), lehyas or powders. They are marketed as tablets or syrups, a common mode of manufacture of allopathic drugs. Obviously, the manufacturers as well as the practitioners appreciate the ease of dispensing and consuming drugs in this form. With the wide marketing of such drugs certain disturbing trends are becoming evident. The drugs are generally compounded only from herbs. However, there is a small proportion where they are being mixed up with allopathic drugs. Examples are given in Table 1. It is not known whether any work has been done on either the potentiating effect or adverse effects of one group on the other. It is not known whether the Drugs Controller has approved of such drugs. The question arises, is this for use by traditionalists (the term traditional is used here to include all traditional systems using herbal medicines like Ayurveda, Unani, Siddha etc.) or by allopaths or is it the country’s idea of integrating Allopathy with traditional systems? Although this cannot be condoned, by itself we may try to ignore it (The so-called quacks are anyway using allopathic drugs like aspirin penicillin, quinine, etc.) More serious, in my opinion, is the fact that some of these are being used nonchalantly by allopaths. Mostly these are used for infective hepatitis, urinary calculi and piles. Is it because deep-rooted within us is the “Ayurvedic” culture; is it because of the wide advertising (e.g.; Vinkola-12, Liv 52, Raktadoshantak, Jammis Liver cure) or is it because these companies also employ persuasive medical representatives? I have no ready statistics regarding how widely these drugs are used by allopaths one company. The Himalaya Drug Company publishes two journals Capsule and Probe. From this, one can get an idea and I therefore, use it as an example. The drugs are used by private practitioners, doctors in general hospitals and in teaching hospitals. The drugs of this company have been used in teaching hospitals from Kashmir and Kerala, if one believes reports from Capsule and Probe, The medical institutions include BJ. Medical College, Pune; Osmania Medical College, Hyderabad; Tirupati Medical College; Bunkura M.C., Bengal; Institute of Child Health, Madras; SMS M.C., Jaipur to name only a few.

Hybrid Herbal drugs Table I Name Ingredients 1. Spasmolin Ephedrine, Caffein, Tinc. belladona, (Add co Ltd.) vasaka, long pepper, kantikari, Bamanbati, Nagaswar, 7-10%aicohol 2.Vinkola - 12 Ferrous salt, Copper Sulphate, Drakshrista, (Standard B-complex vitamins, Pharma, Ltd) alcohol 10 % 3. Malaria tablets Quinine, godanti bhasma [Baidyanath )

Table II

Name Liv 52

Bonnisan

Geriforte

Ingredients Achillea millefolium, Capparis spinosa, Cassia occidentalis, cichorium intybus, Terminalia Arjuna, Tamarix gallica, Solanum niger, mandur bhasma. Achillea millefolium, Cappo spinosa, Cass. occidentalis, cich. intybus, Phyllanthus embica, Tamarix gallica, Term. Chebula, Tinospora cardifolia, Tribulus ferristus, long pepper, cardamom. All ingredients of Liv 52 plusTerm. chebula, asparagus ascendes, Asp. racemosus, casesalpinia digyna, withinia somenifera, Glycyrrhizza glabra, centella asiatic, Mucuna pruriens, Myristica fragrans, Eugena caryophyllata, Carum coptilicum, berberis aristata, Eclipta alba, Argyreo speciosa, Celastrus paniculatus, Adhatoda vasica, long pepper, mace, Cardomum, Shilajit, Chyavanaprash, Abrak bhasma, Loha bhasma, Jasad bhasma, Kesar, amber, Makardhwaj, haldi.

What if these drugs are used? Yes, what if? I give the composition of three drugs; these are chosen because all 3 have been used on children, by allopaths (Table 11). Do the allopaths know the therapeutic and toxic properties of all these ingredients? The allopaths boast of a "scientific system" of medicine. Have proper drug evaluations been carried out on these preparations. If not, are the allopaths who used and use these drugs quacks or are they indifferent to what happens to their patients? Do we know anything about drug evaluation and clinical trials? Are we taught these in our pharmacology classes?

a) How is a new drug tested (b) When is it ready for a clinical trial (c) How is a clinical trial to be conducted, etc. One may say that herbal medicines are being used in allopathy - for instance serpasil, digitalis, belladonna etc. So what is the harm in using others. Firstly these are mostly single principles isolated from herbs and secondly, they have been tested according to principles of pharmacology. I do not question the efficacy of the marketed ayurvedic drugs in treatment of particular diseases. I wish to emphasize that their use by allopaths is quackery and at best, unethical. The results of these trials are even published in regular medical journals! Pediatric Clin. India 10:157, 1975 and Indian Pediatrics 14:197, 1977).

Kamala Jaya Rao.

*

Vietnam: herbs and war A former UNICEF representative in Vietnam said recently: "Vietnam is facing up to the risks of morbidity and mortality with a degree of success unknown anywhere else in the developing world." The Vietnamese have developed a uniquely decentralised health system, marked by a high degree of self-reliance and the utilisation of local resources and manpower. This decentralised health system, the Vietnamese believe, went a long way to meet the medical challenge of the war against the USA, in which their country was bombarded with the equivalent of 700 Hiroshima-sized bombs. Surgery and other medical services had to be ready everywhere. Although the Vietnamese did receive some foreign drugs, their own traditional herbs played a major role in meeting their pharmaceutical needs. Herb gardens helped several villages to become self-sufficient, at least in essential medicines, during the long war years. The Vietnamese first learnt to respect the effectiveness of traditional herbs and simple technologies when they were fighting the French (1946-54). Herbal recipes were collected from peasants and used to produce antimalarial drugs, cough syrups, nerve sedatives etc. Medical technology was extremely simple -terracotta instead of lead cauldrons; bamboo scales, hypodermic needles made out of hair pins; scalpels,

scissors and forceps made from steel from railway lines blown up to cut French communications. In 1957, North Vietnam established two pharmaceutical corporations to supply medicines throughout the country. One of them specialised in Western medicines, the other in traditional drugs. The use of vegetable based pharmaceuticals grew 27-fold between 1955 and 1964. Active principles were extracted from plants: palmatin from Fibraurea recisa, rutin from Sophora japonica, phytin from rice bran. The Vietnamese now believe they paid excessive attention to plants during this period. If equal attention had been paid to the production of simple chemical substances like sodium bicarbonate, they say, many drug shortages would not have occurred. In the villages, each family was encouraged to grow at least a dozen of the 58 “family medicine" plants. The state farms grew medicinal plants on a large scale, but purchases from peasant families still constituted an important source of medicinal materials for the government, and augmented peasant income. When the US bombing began in 1964, Vietnam promptly decentralised pharmaceutical production. The central factories were broken into a host of autonomous enterprises scattered through the countryside and the mountains. The few remaining factories at the central and provincial levels replaced the bottled medicines by pills and powders. These were easier to transport and store, in polythene bags to protect them against rain and damp. The percentage of total drug production contributed by the dispersed, regional factories grew from 18 to 40 %. Family herb patches helped many villages to become nearly self-sufficient in essential medicines. A thorough study of local plants led to the creation of 40 new drugs. In the future, Vietnam intends to develop the large-scale manufacture of medicines from local materials. Modern drugs are still restricted mainly to district and provincial hospitals. In the communes, where 85 % of the people live, 80% of medical problems are dealt with through the use of local products.

[Courtesy-Anil Agarwal-Drugs and The Third worldEarthscan Publication.]

SRI LANKA'S EXPERIENCE WITH BULK PURCHASING Bulk purchasing sounds a simple exercise. But opposition at home from the medical community, drug retailers and pharmacists, and abroad from the international drug industry, can make it a severe test of a country's political will. Sri Lanka was a pioneer of bulk purchasing. (It also adopted a limited list of drugs, and restricted prescribing to generic names.) Now it is abandoning bulk purchasing under strong pressure from domestic and foreign sources. Ironically, this is happening just as international organisations are beginning to quote Sri Lank a as a model for other developing countries to follow.

40% Savings In 1972, Sri Lanka set up the State Pharmaceuticals Corporation SPC) to purchase drugs for the entire country. That year the SPC took over the imports. These were mostly the older, well-established drugs (e g. sulphonamides, tetracycline, streptomycin) since they were available from many sources a degree of price competition already existed. So very large savings by bulk purchasing were not expected In fact, Sri Lanka made great savings through bulk purchasing those drugs, which suggests that competition in the drug industry is not working. The SPC found that by worldwide tenders it was able to import these 52 well-established drugs for 40 % less than the private sector had paid for them six months previously. The private sector had imported 23 brands of tetracycline at an average price of S 16.92 per 1000 capsules. After considering 45 different offers, the SPC bought tetracycline for only $ 6.33.

In 1973, the SPC took over the import of the raw materials required by the 14 private drug factories operating within Sri Lanka, including the foreign owned ones Many foreign manufacturers then reduced the prices at which they had earlier been supplying their own subsidiaries Beecham (Singapore) reduced its earlier price for cloxacillin by about four-fifths and for ampicillin by about five - sixths. Hoechst (West Germany) reduced its price of tolbutamide by over a half; supplies of the same drug from Poland cost only one-sixteenth of the original Hoechst price. In 1973, the SPC purchased dizepam tablest from an Indian company atone-seventieth the price offered by the Swiss multinational, Haffmann-La Roche. Table No.2 shows that in 1975, the multinationals were still offering drugs at prices many times more expensive than they could be obtained elsewhere.

The SPC was very careful to buy all its drugs through worldwide tenders. For those drugs where quality and bioavailability were vital, quotations were invited from only a few good manufacturers. Bioavailability tests were first made to see which drugs were interchangeable, and then only the cheapest amongst them were bought.

MNCs Blow up the Plan Despite these precautions, the SPC remained under severe criticism from the domestic medical establishment, and the foreign companies. The multinationals charged the SPC with distributing substandard ineffective and even toxic drugs. In 1973, for example, a representative of a foreign company wrote a letter to Sri Lankan newspapers alleging: “Most of the drugs imported (by the SPC) are not even tested for chemical equivalency". There were also complaints that the corporation was importing too many inferior quality pharmaceuticals from East European companies. Hard evidence was seldom produced, The SPC always checked the quality of drugs reported to be faulty and recalled them when defects were found. Sometimes drugs manufactured by multinational companies were found to be sub-standard: Roche tetracycline and Burroughs-Wellcome malt, syrup, for example. Dr. Senaka Bibile, then chairman of the SPC, complains that the Sri Lankan medical establishment publicised the recalls of small firms but kept quiet about those of larger companies. Dr. Bibile also reports that many doctors in Sri Lankabelieved that tetracycline supplied by the SPC was not as effective as when it was privately imported. Tetracycline was then imported by SPC from Hoechst, and locally encapsulated. It was tested both before and after encapsulation by the Sri Lankan health ministry. Yet some doctors were administering double the usual does of tetracycline in an attempt to control infections. A detailed examination by a bacteriologist at the Colombo General Hospital found that the problem lay not with the quality of the drug but with its serious overuse for minor ailments, which had led to bacterial resistance. Harassment at home via drug importers, their salesmen and the medical community was matched by veiled: threats from the multinationals abroad to boycott the SPC. In May 1973, for example, Joseph Stetler, president of the US pharmaceutical Manufact-

TABLE No.1 The table shows the prices paid by the SPC in 1973, compared to the prices paid in 1972 by multinational subsidiaries for the same supplies bought from their parent companies. This illustrates the widespread phenomenon of "transfer pricing".

Raw material Tolbutamide

Private sector supplier 1972

SPC supplier 1973

Hoechst W Germany

Hoechst W Germany Polfa Poland

Private sector transfer price (A)

SPC purchase price (B)

B as % of A

40.62

19.24

47

40.62

2.52

6

Paracetamol

Sterling UK

R. Poulenc France

3.24

2.76

85

Chlorpropamide

Pfizer USA

Pliva Yugoslavia

126.21

9.46

8

Tetracycline

Pfizer USA

Hoechst W Germany

98.87

19.72

20

Aspirin

Glaxo UK

Polfa Poland

1.16

0.99

85

Ampicillin

Beecham Singapore

Beecham Singapore

569.90

95.11

17

TABLE No.2 SPC imports of processed drugs in 1975 (in USS). Sri Lanka found in 1975 that it had purchased 53 drugs at 30% of the cost quoted by the traditional suppliers to the private sector.

Drug

Traditional supplier

Traditional supplier's price (A)

Successful tenderer

Successful tenderer's price (B)

B as % of A

Isoniazid

Roche Switz

761

China Nat, China

177

23

Imipramine

Ciba Switz

20.932

B. Pharm. Lab, India

2,846

14

Dizepam

Roche Switz

7,956

Ranbaxy, India

192

2

DT, vaccine Glaxo (diphtheria, triple antigen) UK

4,451

Medexport USSR

539

12

32,800

14

DPT vaccine (diphtheria, whooping cough, triple antigen)

Glaxo UK

231,503

Merieux France

Table No.1, 2 - Source UNCTAD, Case studies in transfer of technology: Pharmaceutical Policies in Sri Lanka, June 1977.

uers, Association, sent a letter to Sir Lanka's Prime Minister Mrs.

citizens also started to attack the work of the SPC, which the

Sirimavo Bandaranaike, which talked about a "number of

committee said was ideologically ill-conceived and wasteful. The

corollary consequences" with respect to all foreign investment in

PIC recommended that the private sector should once again be

Sri Lanka. Companies having high investments in R&D, he said,

allowed to import drugs, particularly for private patients. The

would be discouraged from bidding for SPC contracts.

SPC, suggested the committee, could retain a monopoly of

The multinationals did not withdraw from bidding, nor did the

supplies to hospitals and other state institutions.

US boycott Sri Lanka. But an "information gap" about drugs did

The Jayewardene government decided in 1978 that the private

result from the disappearance of their promotional activities: free

sector once again will be allowed to Import drugs in parallel with

samples, literature and the visit to doctors of medical salesmen.

the SPC, under brand names. This action, says the government,

Sri Lanka overcame this by producing two quarterly bulletins

was taken so as not to keep the consumer "tied down to types of

giving information on the latest pharmaceuticals.

drugs dictated by the state".

In July 1977 the Jayewardene government took over from the more leftist regime of Mrs. Bandaranaike. A self-styled Public Interest Committee of Sri Lankan

[Extracted and slightly edited from Anil Agarwal's "Drugs and the Third World" an Earthscan document, August 1978.]

• MNCS THWART INDIGENOUS TECHNOLOGY In considering the role of the Foreign Controlled Companies vis-a-vis the indigenous firms, it is not' enough to find out what the latter is actually capable of producing, but also what they could have produced in the absence of the FCC's operations in India. We rely on few case studies to discuss how the presence of the FCS (and the government policies allowing them) discourages indigenous development of technology.

The Bengal Chemical & Pharmaceutical Works Ltd.: The BCPW is a pioneering Indian firm (recently taken over by the Government) which by the 1950s succeeded in developing the processes of a number of vital drugs without any foreign help Examples are: Thiacetazone (started in 1952): Nikethamide (1960s); Nicotinamide (1952); Nicotinic acid (1946); Isoniazid (1952; Dapsone (1950); Chlorpropamide (1959) etc. For one of these drugs we relate below how the behaviour of the FCS specifically affected the BCPW's operations.

compared to the import price of around Rs. 18/-. With a price

advantage it started replacing imports and by 1956, annual production reached 4.8 tons. Thereafter, however, it started facing difficulties in sales, stocks started accumulating and accordingly production had to be curtailed by more than 50 % The reason was severe price cutting by foreign competitors, who found their position threatened. Foreign controlled firms such as Burroughs Wellcome and the ICI drastically reduced their price to Rs. 10.50, Rs. 8.90 and even as low as Rs, 8. 55 per thousand tablets. The objective was evidently to win over the tenders from leper homes, hospitals etc. and thus recapture the market.

.

In response to the BCPW's protests, the Government did decide to restrict imports to the gap between demand and indigenous production. But authentic data for demand were not available. It has been claimed by the BCPW that interested parties often succeed in causing overestimation of demand so that imports tended to exceed the actual gap and the BCPW continued

DAPSONE It is an essential drug widely used for the treatment of leprosy. In 1950 the BCPW succeeded in producing it for the first time in India and set up a full fledged plant. It charged around Rs. 10 /per thousand tablets

to suffer from import competition. Further, though its product was certified as satisfactory by the School of Tropical Medicine, Calcutta, after the necessary tests, a campaign was started which

demanded that imports should be allowed without any restriction, since the BCPW's manufacturing was not up to the required standard. Co-operation from distinguished leprologists ultimately saved the situation. Moreover, the government gave Burrough-Wellcorne, a company with 100 % foreign equity, a licence to produce Dapsone on the basis of foreign technology, which required a higher percentage of imported materials. Thus competition from foreign firms operating in foreign countries was replaced by that from foreign firms, producing in India It may not be irrelevant in this context to point out that the BCPW objective was not only to develop a process but to use as far as possible domestic materials. As a result a particular process starting from imported materials - acetanilide, chloro-sulphonic acid, etc. and which would have required imported enamel or glass lined stills and equipments for condensation at an enormous cost, were avoided.

During the 1950s, a fresh difficulty cropped up which seriously threatened the BCP\V's production. The UNICEF in its efforts to eradicate leprosy initiated an aid scheme. It purchased Dapsone from foreign countries and supplied it free to the Government of India for ultimate distribution to hospitals and leper homes. The BCPW represented to the government that UNICEF should be asked to buy first from indigenous sources, but without any effect. Such UNICEF imports continued till 1969, when it was replaced by a new gift of Lamprene, another anti leprotic drug produced by a Swiss firm. The Haffkine Institute and the Hindustan Antibiotics Ltd.

In 1948 K. Ganapathi and S. S. Sokhey at the Haffkine Institute developed a process for manufacturing Penicillin (in addition to a number of sulpha drugs already developed) and, submitted a scheme to the government. But it was not implemented. Later S. S. Sokhey joined the World Health Organisation, where he opened a section on antibiotics to help member states to set up their own plants. It was under this scheme that using the data and blueprints prepared by Ganapathi and Sokhey, an antibiotics plant to manufacture penicillin was offered to India. Consequently a penicil1in factory was set up in Poona and Hindustan Antibiotics Ltd. was formed. K. Ganapathi was appointed as the Works Manager to ensure successful production and undertake further improvements. The process for manufacturing another antibiotics-Streptomycin too was developed by Sokhey. But the Government entered into a technical collaboration agreement with the US TNC, Merck & 

FROM EDITOR'S DESK In this issue, we are focusing on the multinationals in the Indian drug industry. Articles by Anant Phadke, Sudip Choudhary, J. S. Majumdar will help to clarify the pernicious role of multinationals. Attempts made in other countries (Sri Lanka and Vietnam) towards formulating some aspects of a comprehensive rational drug policy have been introduced by Anil Agarwal. The Drug-policy is of course only a part of the complex problem of Health policy. But each aspect of this Health policy needs to be analysed in details. This issue, we hope will give atleast some idea to our readers about the issues involved in formulating a rational drug policy. It will also act as a back-drop to the discussion at the Annual Meet. The next issue will now appear in March 1982. It will contain a report on the discussions and background papers presented at the VIIIth Annual Meet at Tara from 23rd to 25th January 1982.

Co. to produce Streptomycin. In the 1950s, the TNCs hardly made any efforts to produce bulk drugs in India. But as schemes were being formulated to produce bulk drugs and drug intermediates in big way with Soviet help, the Western firms naturally felt their position threatened. The following excerpt from the American Magazine Chemical and Engineering News (dated 24 November, 1958) makes the implications of the agreement very clear"... Fortunately for the free world, Merck and other US. & Western Drug and Chemical firms have not been idle. Much negotiating took place between the Government and the company men before Merck's Indian Venture got a green signal. As final steps take place in India, Merck's International head Antonic J. Kooppers meets Indian Finance Minister, Morarji Desai at a luncheon given in honour of the Minister in New York. Merck's entry into Indian Pharmaceuticals makes friends, future profits and helps side swipe Soviets."

Merck insisted among others that the scientists at the plant be screened. The firs: casualty of the screening clause was K Ganapathi, who had been involved in the development and improvement of processes for bulk drugs. [Extracted from - The Role of Foreign Controlled Firms Vis-A-Vis The Indigenous Firms In The Pharmaceutical Industry India paper presented at the Delhi Seminar by Sudip Choudhary.]

(Continued from page-4) canalised through a Government agency, the price was lowered to Rs. 10,000/ Kg. Similarly the price of doxycycline was brought down from Rs. 3000 to Rs. 1500 / Kg. (1 7)

2) C. P. Chandrasekhar and Prabir Purkayastha-Multinational Investment, Profit Repatriation and the Production of Drugs in India. pp. 129, 130. 3) Dr. Naresh Banerjee-Common diseases of the Indian people and their requirements of basic and bulk drugs; p 12. 4) Average taken from Chandrasekhar, Purkayastha op. cit.

To summarise - Multinationals in Indian drug industry have hardly any positive role to play. Because of their profit motive and monopolistic form of competition of product differentiation through brand names, advertising etc; these companies are primarily interested in manufacture of fancy formulations for the well-to-do at the expense of essential drugs for the vast majority. For the same reasons, they are not interested in research in areas vital for our people's health. Their presence in our country can not be justified on even technological grounds. These companies have moreover pumped out large sums of money [out of the profits gained by exploiting cheap Indian labour] into the coffers of their parent company. In the Delhi Seminar therefore, following resolution was unanimously approved- "The multinational drug companies operating in India should be nationalized. At the same time, the functioning of the Public Sector should be improved in order to make it truly national and truly pro-people.”

table No.5. 5) Dr. Naresh Banerjee op. cit. p . 12. 6) Banerjee op. cit. p. 12. 7) Chandrasekhar-Purkayastha; op. cit. p. 14. 8) Nagesh Kumar and Kamal Mitra Chenoy-MNCs In The Drugs and Pharmaceutical Industry; p. 8. 9) Chandrasekhar-Purkayastha op. cit. p. 4. 10) J. S. Majumdar-Drug Industry-Instruments of Policy; p. 10. 11) Dr. W. V. Rane and Dr. A. R. Patwardhan Priorities In Drug Manufacture; pp. 6, 7. 12)

J. S. Majumdar op. cit. p. 6.

13)

Dr. Ghosh-Pattern of Diseases and The Drug Production in India. (Lecture)

14) Drs. Rane and Patwardhan, op. cit. Table No.2. 15) J. S. Majumdar op. cit; p.7.

16) J. S. Majumdar op. cit. p. 8 One can not go here into the functioning of the Indian private sector and the Public Sector. But even a glance at the functioning of Indian private sector will show that it is also dominated by monopoly profit motives. The public Sector has not reversed the basic trends in the drug industry in India. An analysis of the drug industry will not of course be complete without analysing the dynamics of the Indian sector. All socially conscious medicos must know the dynamics of the Indian Sector also in order t o know the important obstacles before a rational drug policy.

17)Anil Agarwal-Drugs and The Third World- an Earthscan document, London, August. 1978; pp. 6, 12, 13 18) Anil Agarwal op. cit. p. 16. 19) J. Manohar Rao- A Note on Research and Development in Private Pharmaceutical Firms in India pp. 11. 20) Nagesh Kumar- Kamal Mitra Chenoy op. cit. p 3, 8. 21) B. V. Rangarao-Foreign Technology In Indian Drug Industry-paper presented at the International Seminar on Technology Transfer, New Delhi, 1973. 22) Anil Agarwal, op. cit. p. 25. 23) J. Manohar Rao, op. cit. 1, 2.

REFERENCES Unless otherwise quoted, all papers quoted below were

24) Majumdar op. cit. pp. 2, 3. 25) Chandrasekhar-Purkayastha op. cit. p. 5.

presented at Delhi Seminar. Page numbers are from the yet

26) Chandrasekhar- Purkayastha op. cit. pp. 7, 8.

unpublished cyclostyled papers. The papers are to be published

27)Nagesh Kumar- Kamal Mitra Chenoy op. cit. pp. 16, 17.

shortly. Those interested, should write to-Delhi Science Forum,

*

J-55, Saket, New Delhi-I 10017.

1) MNCs in the Indian Drugs and Pharmaceutical Industry, Nagesh Kumar and Kamal Mitra Chenoy. pp.6, 7.

*

*

IF THERE ARE NO SIDE EFFECTS, THIS MUST BE ARGENTINA DRUG

TETRACYCLINE

! UNITED STATES I

MEXICO

CAUTION

I

BRAZIL

AGAINST USE By infants, children; By infants, during pregnancy or children, during if overly sensitive to pregnancy light.

[Antibiotic used

By infants, Children;

against various infections; Lederle Laboratories]

during Pregnancy; Liver or Kidney impairment (latter can be fatal) or if overly sensitive to light ADVERSE REACTIONS PUBLICIZED Vomiting, Diarrhoea Vomiting, Diarrhoea, Vomiting, nausea nausea, stomach, upset nausea, stomach stomach upset, rashes, kidney upset. rashes poisoning, can poison fetus. CAUTION AGAINST USE If patient has If patient has If patient has tendency to blood tendency to blood tendency to clot, Liver dysclot, Liver dysblood clot. function, abnormal function vaginal bleeding, epilepsy, migrain, asthma, heart problem.

OVULEN [Birth Control Pills: G. D. SEARLE Co.] In U. S. used for contraception only. In some Latin countries, Searle recommends it also for regulating menstrual cycles, Premenstrual tension, menopausal problems.

ADVERSE REACTIONS PUBLICIZED Nausea, Loss of hair, Nausea, weight None nervousness, jaundice, change. high blood pressure, weight change, headaches.

IMIPRAMINE [Anti-Depressant: Ciba Geigy) In U. S. used for depression only. Jin some Latin American countries, Ciba-Geigy recommends it also for senility, chronic pain & alcoholism.

CAUTION AGAINST USE If patient has heart During first disease, history of trimester of urinary retention, pregnancy history of seizures, manic disorder or is on typhoid medicat-

I ARGENTINA None

None

If patient has tendency to blood clot.

None

If patient has heart disease, Not recommended for children or during pregnancy

May exaggerate response to alcohol.

None

None

ion. Not recommended for children or during pregnancy.

ADVERSE REACTIONS PUBUCIZED Hypertension, stroke, Dry mouth, constipastumbling, delusions, tion, itching insomnia, numbness, sweating. dry mouth, blurred vision, constipation, itching, nausea, vomiting, loss of appetite diarrhea,

SOURCE: Culled from the Physician's Desk Reference-the standard handbook tot U. S. Doctors, [Taken from the Mother

Jones, Courtesy-Health and Society.]

THE COMMITTEE FOR RATIONAL DRUG POLICY The two articles in this issue on the role of MNCs in Indian Drug Industry are based on the Delhi-Seminar and will give the readers some idea about the seminar. The organizers and the participants of the seminar looked upon the seminar as a beginning of a process of systematic analysis and pinpointing of defects in the structure of the

drug industry. It was expected that at the end of the seminar, a coordinating, mutually supporting body should be formed amongst the participants to concretize to broaden and to deepen our analysis and to foster a continuous action-study programme. At the end of the seminar therefore, a Committee for Rational Drug Policy was formed. It comprises of the organizers of the seminar and

Industry Federation of M. R. A. of India 5) Gathering information from the available morbidity surveys in India-Delhi Science Forum. A rational drug policy can not be formulated correctly unless we know the morbidity pattern more accurately 6) Dumping of Drugs by MNCs-All-India Federation of Junior Doctor's Association. The respective organizations are to send their papers on the above topics to the convener of this committee - Imrana Qadir before March 1982. A mass educational campaign can be taken on these issues on the basis of these papers. Moreover some resolutions asking for specific changes in the existing drug policy can be formulated and can be pushed forward with the help of raising of medical and non-medical public opinion.

representatives from organizations like MFC, Arogya Dakshala

It seems that different progressive, socially conscious

Mandai etc. The aim of the Committee is to evolve and to

organizations of medicos arc now planning to act and to

propagate a rational drug policy from the point of view of the

coordinate with other like minded people on the question of

interests of the vast majority of our people. Anybody wanting to

irrationalities of the drug-industry. Voluntary Health Association

work towards this goal and broadly agreeing wit h the resolutions

of India [C- 14, Community Centre, S D.A. New Delhi-16] is also

and consensus of the seminar can join this Committee.

planning on similar lines. It has brought out a special issue [Rs

At the first meeting of the committee, it was decided that we should work on two fronts. Firstly, it is necessary to propagate as much as possible through various channels, some of the most important findings of this seminar amongst the common people and amongst medicos, Substantial amount of material was ready on the following topics -role of MNCs in Indian drug industry, how MNCs flout or circumvent the laws of this nation, unethical human drug trial in a Calcutta slum, scientific scrutiny of some over-thecounter drugs. Some more work needs to be done on the question of abolition of brand-names. Secondly, it is necessary to choose some issues for concrete and time-bound study and then launch a

6/-] of its ‘Health for the Millions' on the drug-issue. It is organizing a workshop on "Drug issues and seeking feasible alternatives" from 8th to 10th January 1982 at Pune. Other organizations like Consumer Education and Research Centre, Centre for Education and Development, Resource Centre for People's Education and Development, etc. are trying in their own way to work on this issue. I therefore feel that MFC should also work systematically on this issue and cooperate with others. Those of you who have specific suggestions or those who want to join this battle should write to Imrana Qadir [Delhi Science FOflll1l-J-55, Saket, New Delhi - 17] with your suggestions.

Anant Phadke

mass-educational campaign on these issues. Following issues were selected by respective organizations. 1) Scientific scrutiny of the promotional literature of the drug companies to pinpoint the defects--Federation of Medical Representative's Association of India. 2) Brand-names Vs Generic names- Society for young Scientists. 3) Irrational drug combinations- Arogya Dakshata Mandai and MFC. 4) Public; Sector in Indian Drug

Do you want a doctor in your project? H. Gammeter, a Swiss doctor with 1ft year's of professional experience in General Surgery wants to work in an Indian villageproject for 7 months as an apprentice on, a Scholarship from the Swiss Govt. Those who can help him, .should write to him--Risi, CH-9630, Wattwil, Switzerland.

Pricing Strategies of drug companies The pricing strategies of the multinationals show the profitability of their products. With the drug price control order (DPCO), 1970, the prices of tetracycline and chloromphenicol were put by half. The price of tetracycline was further reduced later. Yet, the multinational drug firms found that they could earn huge profits in tetracycline. Tetracycline, therefore, remained the first item in their promotional plan. The pricing of tetracycline multi-dose vials exposes the break-neck competition and the wide margin of profits. The ten ml vial of Terramycin l M (the multidose vial of oxytetracycline 500 mg marketed by Pfizer,) which used to cost more than Rs. to per vial before 1970; was sold around Rs. 5.00 per vial following the DPCO. Even then, Pfizer found substantial profit in this product and geared all its promotional activities to capture a huge market. Sarabhai and others were keenly watching the expansion of the market for Terramycin 1M vials and its profitability. Sarabhai introduced a similar product Oxysreclin. A fierce competition ensued. Pfizer introduced an incentive scheme of 30 vials free for the purchase of every too vials. Sarabhai followed with the same offer immediately on introducing the drug. Other too joined the fray. A ten ml Teramycin IM vial now costs Rs. 3.22.

brand name phenomenally high. It refuses' to bring down the prices till it is compelled to compete with the same product marketed by another company under its generic name. A glaring example is Garamycin marketed by C.E. Fulrod. Gentamycin sulphate 80mg, under the brand name Garamycin, was introduced by the company in 1969. Every vial at that time used to cost about Rs. 35. This was brought down to Rs, 28.77 per vial in 1976. However, when it began to face competition from the Indian sector and the product became available under the generic name, the price of Garamycin was brought down sharply to Rs. 11.41 per vial. This price is still the highest amongst all other brands of the product. Hindustan Antibiotics Ltd., a public sector undertaking, offers a special rate of Rs. 5.80 per vial of this product for hospitals, though the general market rate for a vial of HAL Gentamycin is Rs 9.80. It is these huge margins, and not the safety and effectivity of drugs which lie behind the sales drive of the multinationals. They decide what antibiotic is to used, and when they do, they can expand the market whenever they like. For instance Cyanamind, the US multinational, did not give much emphasis on the sale of Ledermycin during 1975 to 1978. However, in 1980-81 the company decided to promote the product as the first priority item and make up the loss in the sales turnover of the company due to the withdrawal of products like Achromycin, Ledermycin drops and syrup, Delphicol, Lederkin, etc. And the company succeeded in increasing Ledermycin sales by 30 per cent in 1980-81.

Pfizer also introduced a novel scheme of "Poor Patients Sampling." Under this scheme, huge quantities of ten ml Terramycin 1M vials are given as samples. Sarabhai and other companies soon followed suit. Sarabhai introduced a 20 ml vial of oxytetracycline under the generic name costing Rs. 4.05 per vial, declaring in addition, a ten per cent bonus offer. The same product, under the brand name Oxysreclin, continues to cost Rs. 3.23 for the tell ml vial. In a special campaign for Achromycin IM, Cyanamid declared still more lucrative incentives, like 48 vials (!) free for the purchase of every 52 vials! Alembic offered a stainless steel utensils scheme for purchasers of Alcyc1ine vials. Other manufacturers offered suit cases

Many antibiotics often promoted by the drug manufacturers particularly the multinationals have been found to be banned in countries of their origin. Profits, after all, must come before people's health, especially if they happen to be a Third World People. Our antibiotic, policy has so far left the matter in the hands of these very companies. The only alternative is to have an institutionalised approach based on a clear and firm antibiotics policy.

While introducing an antibiotic, a multinational company keeps the retail price of its product under a

[Extracted from-The selling Game - by J. S. Majumdar, Science Today Sept. 1981.]

Editorial Committee: Anant Phadke Christa Manjrekar Mohan Gupte Ravi Narayan Kamala Jaya Rao, EDITOR


