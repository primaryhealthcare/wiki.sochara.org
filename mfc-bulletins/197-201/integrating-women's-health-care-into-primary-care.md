---
title: "Integrating Women's Health Care Into Primary Care"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Integrating Women's Health Care Into Primary Care from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC197-201.pdf](http://www.mfcindia.org/mfcpdfs/MFC197-201.pdf)*

Title: Integrating Women's Health Care Into Primary Care

Authors: ARCH Team

197-198-199-200 & 201

medico friend circle bulletin August-December 1993

Women's Work and Reproductive Health Veena Shatrugna, P. Vidyasagar and T Sujatha THE seventies and the eighties saw the emergence of the programmes upgraded women's marketable skills by them "embroidery, stitching, tie and dye, basket Women's Health Movement and women questioning the teaching weaving, beedi making, and a host of other labour dominant assumptions of the vast network of the health intensive skills etc". Few attempts were however made to care system in India. Their contention has been that both respond to the questions raised by the critiques of the the modem curative and the preventive health care Health Care System or change the assumptions about the sickness industry which was being gradually privatized system in India has dealt with women's illnesses in terms and controlled by the corporate sector (Occupational of pregnancy related problems or organ related illnesses, health Issues - Report of Task Force on Health 1988, and a result uncritically prescribed tablets, injections, Shram Shakti, 1988). immunization, FP devices or surgery in the "best The most recent policies of the Government, the traditions" of curative care. The micro level structural' adjustment programmes of the IMF and the experie':1ces of women's illnesses however revealed accompanying package the exit policy, scrapping of labour and simultaneous liberalisation of the economy significant variations among the social strata. legislation, also promises to increase women's employment in the non Differences in etiology, prevalence, response to formal sector/or the self-employed sector, by reducing the medication, and even compliance between men and male labour force in the organised sector. Simultaneously women, rural urban, rich and poor etc has not been it is proposed that public spending on social welfare measures such as health care, Public Distribution System, acknowledged by the "modern doctors", (Prakash, P. education etc. would be drastically reduced to minimise 1984, Sathyamala, C. 1984, Shatrugna, V, et al, 1987b) wasteful expenditure and vitalize the economy, towards These critiques highlighted the need to understand the industrial growth. prevalence of women's illnesses and access to cure in the The present investigation was therefore undertaken to context of women's time and work, the demands made study the relationship between women's work for incomes, on her by the family, children and society, her status in access and utilisation of health care and women's health the family the patterns of work related illnesses, and status. It was hypothesized that women's low incomes and poor purchasing power in the 1. Women's work for wages would result in higher unorganised sector, etc. Other feminist researchers family incomes, better information and knowledge about challenged the assumptions of the medical profession health and diseases, this would help utilisation of Health services thus resulting in better mother and child health which believed that women "complained unnecessarily," nutrition. had "non specific problems" and were" unreliable patients" and that most aches and pains in women were Methodology: This study was carried out in a large urban slum, because 40% of India's population is usually "normal" (Miles, A. 1991). These arguments projected to be in the cities governed by the market were specifically levelled against those women who economy by the year 2000, and unprotected by the feudal complained of side effects with the various bonds of land, caste and family. Any earnings of the contraceptives devices. Feminist researchers argued that mother in such pauperized situation would help magnify the advantages and intricate relationship between work, the reason for the gender blindness could be the money and women's health status. The impact of women's limitations of positivist science which adversely affected incomes on the children and the family could thus be the diagnosis of women's illnesses and their chances of investigated. getting well, and therefore underscored the need for a A total of 1124 households living in katcha units were shift in paradigm. enumerated. With a few modifications the women from these households were ranked and placed in the following But development organisations and NGO's uncritical occupation categories based on the ILO classifications for of the health Care System suggested that women's the unorganised sector (Anker, R., 1981). development would help raise women's status, self esteem, and autonomy. Arguing that improving women's 1) Self employed (SE): These women owned the means marketable skills and thus their incomes would result in of production and exercised control over their incomes and better utilisation of health services by women, they working time. Vegetable sellers, petty shop owners, fruit made sure that the "Gender factor" be included in all the vendors of various commodities etc. were placed in this programmes of the Government and the NGO's. These category. In addition piece rate workers like beedi makers,

were also included, because of the control over their II. Health Consequences of Women's Work time. This part of the investigation attempted to study the 2) Wage labour (WL): These women worked for actual morbidities of women and children by a 2 monthly wages. They controlled their incomes but did not have visit to the families, with a 15 days recall of women's control over their time nor did they own the means of illnesses. The current work category of the mother was production. They were paid wages usually cash, but used for analysis. This was necessary to understand the sometimes servant maids received food, clothes etc. relationship of current stress and work to illness pattern, WL women included coolies, sweepers, servant maids, utilisation of health care and expenditure on health. workers in small factory etc. Methodology: 3) Unpaid Family worker (UPFW): This category of Six field investigators we trained to record the women neither owned the assets nor received wages for symptoms in detail. It was possible to broadly classify their work. They worked in enterprises owned by their the morbidities in relation to the system as shown below. husbands or other members of their family who were Doubtful cases were reviewed and cross checked at the dhobis, tailors etc. In addition they had very little end of the day. A sub sample of the sick women was control over their time. examined by a Gynaecologist, who was part of the 4) House wives (HW) : This group of women at home project. doing all the household tasks like washing, cleaning, cooking, taking care of the children, the sick and the old. A total of 1625 households could be contacted in the 6 rounds. One fourths of these (506) reported were The men in the study population were mostly women's morbidities. Thirty percent of the women who rickshaw pullers, auto drivers, coolies, petty shop were ill reported Gynaecological problems followed by owners, vegetable and fruit vendor’s workers in the general feeling of ill health in 13%, upper respiratory factories and workshops as skilled and semi skilled tract infections (URI12.8%) and fevers 10.5%. 18% of workers etc. women had pregnancy or operation related problems. It appears that mothers in the SE category had a Sampling: The sample consisted of 340 households significantly higher percentage of URI problems from the 1124 HH. The distribution of the HH was as because of the long hours of work in doors in dusty surroundings. Whereas mothers in WL and SE had a follows: HW - 103, WL-IOI, SE -107, UPFW - 29. higher incidence of Gynaecological and symptoms of At the end of the study year women's work categories general feeling of ill health. HW reported more were reclassified for the purposes of the pooled annual pregnancy related problems because more pregnant analysis using the above ranking. However round wise women had shifted to HW. (See below) There were no analysis was done using the current work status. work related differences in the other illnesses. (Table -I) A majority of the women (51 %) did not seek medical Data collection help, 27 %sought free or cheap care from The following rounds of information were collected from the selected households. No rounds in the year 1. Background information 1 2. Utililisation of preventive Health care 1 3. Morbidity (every alternate month) 6 4. Shift in Women's Occupations 12 To validate the above information qualitative information was collected using case studies, life histories etc. Data was coded, cleaned and entered in the Pc. The SPSS package was used for the analysis.

Results and Discussions

Table - 1 SYMPTOMS OF THE DISEASE WORK CATEGORY AT THE TIMR OF CURRENT VISIT WL UPFW

HW

ROW

44

54

7

41

t46

(30.8) (37.8)* (25.9) 21 24 2 ( 14.7) ( 16.8) (7.4) 30 13 4 (21.0)* (9.1) ( 14.8) 10 17 2 (7.0) (11.9) (7.4) 5 8 3 (3.5) (5.6) (11.1) 9 2 1 (6.3) (1.4) (3.7) 1 7 —

(21.2) 22 (11.4) 18 (9.3) 24 ( 12.4) 13 (6.7) 5 (2.6) 6

(28.9) 69 (t3.6) 65 (12.8) 53 (10.5) 29 (5.7) 17 (3.4)

(3.1)

(2.8)

TOTAL 1. Gynecological 2. General Feeling 3. URI 4. Fevers

Demographic Status of the Index mothers:

5. Chronic problems

The mean ages and parity of the mothers were comparable. The caste and religious profile of the slum represented the population profile, except that there were no upper castes among the residents of the slums.

6. Gastro Intestinal! Diarrhoeas 7. Skin

I. Preventive Care Antenatal care was very popular and 88% of women had 3-5 checkups. About 73% of women visited the Government hospitals and 48% delivered there. The procedures in the Government sector were methodical and scientific. But about 42% of women during their first pregnancies utilised the private sector and 17% even delivered there inspite of the fact that weights, haemoglobin and other investigations were not satisfactorily carried out in the private clinics.

SE

8. ENT Problem

(.7)

(4.9)

3

1

(2.1) 9. Accidents/Burns

10. Pre+Opea. Rela Column Total

*P < 0.05

1

(.7) 19 (13.3) 143 (28.3)

14

1

6

11

(.7)

(3.7)

(3.1)

(2.2)

3

—

4

8

(2.1) (2.1) (1.6) 14 7 53 93 (9.8) (25.9) (27.5) (18.4) 143 27 193 506 (28.3) (5.3) (38.1 ) ( 100.0)

Table- 2 SOURCE OF TREATMENT BY WORK CATEGORY OF THE MOTHER SE

WL UPFW

HW

ROW TOTAL

1. No Action

67 72 13 108 (46.9) (50.3) (48.1) (56.0) 25 32 7 32 (17.5) (22.4)1# (25.9) (16.6) 22 7 4 30 (15.4)1# (4.9) (14.8) (15.5)1# 10 8 1 11 (7.0) (5.6) (3.7) (5.7) 6 15 2 6 (4.2) ( 10.5) (7.4) (3.1)

260 (51.4) 96 (19.0) 63 (12.5 ) 30 (5.9) 29 (5.7)

Even though a large number of women with chronic problems did not seek health care many sought cure from home remedies, self medication, and help from the neighbours. Upper Respiratory tract Infections and Gastro intestinal disorder; were also treated with popularly advertised drugs and preparations (Table-3).

Even though work for incomes increased women's morbidities more than 60% of the Gynaecological problems, general feeling of ill health and pregnancy 3. Govt. related problems were not treated because women did not seek health care. The most probable reasons could be that 4. Home Remedy in the early stages these illnesses are not acute and do not incapacitate women so completely where they are required 5. Neig./Self to stop housework or paid work. But very soon successful 6. Medical Shop 4 4 11 treatment requires that the doctors pay more attention to the 3 (2.8) (2.8) ( 1.6) (2.2) women's need for better nutrition, rest, help for child care and house work and a supportive family, and sexual 9 7. Para Medical 5 4 relationship, etc. But the curative nature of the modern ( 1.8) (3.5) (2.8) medical systems does not recognise the relationship of 8. Hospitalised 4 1 3 8 Gynaecological, pregnancy related or other problems with (2.8» (.7) (1.6) (1.6) Column Total 143 143 27 193 506 women's work and other social, economic and cultural (28.3) (28.3) (5.3) (38.1 ) ( 100.0) factors. Since these complex relationships, are not acknowledged, both the public hospitals and the private ( ) Indicates percentages clinics readily prescribe expensive pills and tonics instead. There are no differences in the 4 working categories. (Rs. 63-95/-) These drugs may at best offer only Government hospitals, or they tried home remedy, symptomatic relief, and at worst may prove dangerous to treatment from the neighbours para medical person, or the foetus. It is therefore not surprising that women were utilised the medical shops, the private sector accounted for only 19% of care. (Table -2) There were no major not willing to pay a heavy cost for temporary symptomatic differences in the source of treatment in the 4 relief. (Table-3) In addition women in the unorganised occupation groups, except that the WL mother used the sector are paid very low wages due to the absence of any private sector to some extent because of the time factor kind of labour legislation and social security. Therefore and the SE and HW who could afford to spend some occupational problems are not acknowledged, and the low time used the Government sector. wages are inadequate for better nutrition, support at home When the source and cost of treatment, was related to or even the high cost of curative care. Women had to the symptoms of the disease, it was significant that the private sector was utilised for acute problems like fevers continue with breast feeding and housework during these (S 1 %) and to some extent URI and GI disorders (when illnesses while working in an irrational system. When they fever was an accompanying symptoms). The location of reached a breaking point they had to opt out of the labour the clinic, its timings and the quick symptomatic care helped women get back to work by reducing the number force. of visits, travel time and waiting time. It appears that women were willing to pay the price (Rs.33/-) to be able to get back to work. (Table-3) 2. Private

Table 3 SOURCE AND COST OF TREATMENT BY MORBIDITY PATTERN Symptoms No Action

Govt

Private

Others" Govt

Gynecological (146) General ill health (69) Upper Respiratory tract infection Fevers (53) Chronic problems (29) GI disorders (Diarrhoea etc) (17) Skin (14) ENT(l1) Preg. related (93)

99 (68))# 41 (60) 27 (41)+ 8 (IS) 12 (41.4) 3 (17.6) 7 (50)+ 4 (36.4)+ 58 (62.4)+

17 (12) 7 (10) 7 (11%) 9 (17%) 1 (3.5%) 1 (5.9) 2 (14.3) 1 (9.1) 18 (19.4)

()=% .. Others include-------> home remedy, neighbours, medical shops, self medication.

14 (10) 9 (13) 13 (20%) 27 (51%) 7 (24.1) 6 (35.3) 3 (21.4) 4 (36.4) 9 (9.7)

16 (10) 12(17%) 18 (27%) 7 (14%) 9 (31.0) 7 (41.2)+ 2 (14.3) 2 (18.2) 8 (8.6)

Mean Cost Private

RS.

Rs.

62.65 7.70 7.10 3.80 50.00 2.00

94.60 29.00 12.20 33.00 86.00 11.S0

-

13.00

30.00

14.00 107.40

Table. 4 OCCUPATIONAL CATEGORY OF THE INDEX MOTHER BEFORE CHANGE Reasons for shift

SE

Pregnancy

21

23

4

(39)

(43)

(7)

2

3

-

(4)

(7)

Resumed work I11ness and occupational morbidity Economic Timing Non-availability of work Death in the

family Total

WL

UPFW

HW

21

11

1

(50) 18 (16)

(26) 4 (4)

(3) 5 (4)

5

9

-

(31) 8 (46)

(56) 3 (H!)

D.KNOW

ROW TOTAL

6

54

(11)

(19)

35

5

45

(78)

(11)

(15)

-

-

9

42

76 (67)

(21) 10 (9)

(14) 113 (39)

-

2

16

3 (18)

-

(13) 3 (18)

(6) 17 (6)

-

-

4

35

( 1) 291 (100)

1

2

1

(25) 76

(50) 55

(25) 14

111

Table – 5 OCUPATIONAL CATEGORY OF THE INDEX MOTHER TO WHICH SHIFT HAS TAKEN

Pregnancy I11ness and occupa-

tional morbidity Resumed work Economic Timing

Non-availability of work

SE

WL

1

1 (2)

9 (22) 24 (53) 18 (52)

6 (14) 14 (31) 4 (44)

4

5

(25)

(31)

5 (26)

2 (12)

-

Death in the

1

51

-

3 (7) 76

10 (2)

113 (39)

7

—

16

—

(6)

(44) 1 (6)

-

-

9 (53)

—

4

—

711

Figures in parenthesis indicates percentage.

8

98

17 (6) 4 (1) 219

(100) 102

54 (19) 42 (14) 45 (15)

27 (64)1 4 (9) 5 (2)

— (94)

(2)

-

family Total

ROW

UPFW HW D.KN TOTAL OW

(2)

It was obvious from the results that work related illnesses during pregnancy and other stressful situations, increased women's morbidities, and curative care was barely an adequate response, they had to shift out of their occupations to enable them to recover. Similarly those who "resumed" work had earlier opted out of paid work due to morbidities and stressful situations. Thus the largest numbers of shifts (48%) were due to women's work related illnesses, straining them physically and emotionally. Whereas housewives shifted into paid work largely due to economic reasons. Conclusions

Figures in parenthesis indicates percentage.

Reasons for change

But those who "resumed work" (15%) or shifted for "economic reasons" (39%) entered the labour market from the HW category. These shifts were not related the sex or age of the children, or to the physiological status of the mother. There were no difference in the demographic characteristic of those who shifted and those who did not shift occupation. (Table-4 and 5)

5 (100)

III Changes of Occupation It came as a surprise when the investigators discovered that large number of women changed occupations due to various problems. All the shifts (291) in the occupations for the one year were included for the purpose of the analysis. Reasons for shifts were investigated. It was observed that stresses like Pregnancy (19%), illnesses (3%), difficult working conditions (II %) contributed to 33% of shifts. In these cases women shifted out of the labour market to the IIW category or SE working inside the house.

I. This is an example of the limitations of a positivist frame work which assumes that increased incomes would result in increased utilisation of health services. Instead work for income increased women's morbidities without a backup of fair wages or a sensitive health infrastructure. The sickness industry continued to respond to women's illnesses for curative care by offering symptomatic cure for fever. It was not equipped for the treatment of Gynaecological problems, chronic illnesses, pregnancy related problems, occupational health hazards or even general ill health. It may be stated that since the private sector was unable to diagnose or treat the above problems, liberal prescriptions for non specific medicines and investigations resulted in increasing the cost of health care. 2. Though women continued to use the Government health services when time was not a constraint, but definitely for antenatal care, instead of strengthening and rationalizing the Government sector to respond to the curative needs of women, the new government policy aims at reducing public spending on health. Thus there is a danger of a large number of women being forced to utilise the services of the expensive private sector. In the existing scenario the private sector is least equipped for treating women's problems, consequently women's health status is likely to further deteriorate in the coming years. 3. The other disturbing trend is the high incidence of work related morbidities which go undetected in the unorganised sector. With the expansion of this sector, and the total absence of social security, occupational health problems may take a heavy toll of women's health. In addition women would be forced to opt out of the labour force. Fair wages and labour legislation arc a pre requisite for women's development and increasing women's participation in the economy y. Newer policies of the introduction of dangerous drugs for fertility control, will further increase women's ill nesses, and women will be left to negotiate an irrational system without any kind of help.

Women. Feb. 1988. Prakash, Padma. "Editorial Perspectives" Socialist Health Review (1984) I (2): 53-57. 5. Sathyamala, C. "Is medicine inherently sexist?" Socialist Health Review (1984) 1 (2): 53-57. 6. Shatrugna, Veena, Soundarajan. Nirmala, Sundaraiah, P., and Raman, Leela. Back Pain in Women -It's possible Relationship to Women's Work Prolonged Calcium Deficiency and Osteoporosis: A report prepared for the Task Force on Health, National Commission on Self Employed Women, Ministry of Human Resources and Development, December, 1987 (a). 7. Shatrugna, Veena, Uma Maheshwari, A., and Sujata, T. Women and the Health Care System in Zaheerabad - Towards a New Paradigm of Health: A report, submitted to the Task Force OD Health, National Commission on Self Employed Women, Ministry of Human Resources and Development, References December, 1987 (b). 1. Anker. R., (1981), "Research on Women's roles and 8. Shatrugna, Veena, "Women and Health" Current demographic change: Survey questionnaires for Information Series Research Unit on Women's households, women, men and communities with studies, S.N.D.T. Women's University (undated) background explanations", (restricted) ILO, Geneva. 9. Shram Shakti, "Report of the National Commission 2. Miles, Agnes. In "Women, Health and Medicine" on Self Employment women and women in the Open University press. Buckingham, pg.1-35, 1991. informal sector", Del'8rtmelll of Women and Child 3. "Occupational Health Issues of Women in the unorganised Development, Govt. of India. N Delhi. (1988) sector" Report of the Task Force on Health prepared for The National Commission on Self Employed

Acknowledgement: The authors are extremely grateful to Dr Vinodini Reddy, Director, National Institute of Nutrition, for her support and encouragement throughout the study. The study would not have been possible without the field work and active involvement of the team of field investigators: Uma Kailash, G.V.N. Rao, G.Ch. Reddy, Padma Siromani, Prema Kumari, B. Rajamani, K.S. Padmavathy, B. Vijaya Lakshmi and Bhagyavathy. They were willing to work all time of the day, share their experiences and infectious excitement during the year of study. B. Usha Rani's patience with the numerous drafts and the final typing is gratefully acknowledged.

4.

On Being Normal (Whatever That Is) Manisha Gupte.

WHEN we discuss reproductive biology with women, usually a small crowd stays back after the meeting, mainly to discuss problems related to fertility, menstruation and sometimes to sexuality. Whether a woman's menstrual cycle is not 28 days (which most health education material will imply to be the normal one), whether the husband is impotent, or whether the woman suffers from a persistent white discharge, the question at the back of most women's minds seems to be. Am I normal?” This was by far the most difficult question that we could even attempt to answer. Because even if my menstrual cycle were of 28 days, and I had no white discharge, there was always some other lurking fear in my mind:

was I married, a mother of sons, cohabiting with my husband in marital bliss, feminine enough, asexual (yet heterosexual), docile and attractive; the list is endless. If I were failing in any one of these virtues, I may not be normal after all. If we consider for a moment all those women who are outside the realm of normalcy - women who are deserted, post-menopausal, infertile, depressed, single, lesbians, disabled, without sons, sex workers, selfconfident, dark skinned, polygamous and so on, we would be confronted with the question as to whether any living woman is completely normal at all.

This brings us face to face with some pertinent questions: Who decides as to whether we are normal? What is it to be normal? What personal cost do we pay so as to become normal? Is there something like normalcy after all? Why do we want to be normal? What are the repercussions of being perceived by society as a deviant? These questions are closely tied up with the reproductive and human rights of all oppressed people, whether they are sexual, religious /ethnic or political minorities. These questions are especially sinister in the light of rising Hindu chauvinism and fascism in India, the new economic policy and the onslaught on people's reproductive rights through devious population politics all over the world.

Incentives For centuries, there have been elaborate rewards for conforming to the social structure and punishments for not. To be a woman is in itself the 'original sin', and we know of more than one woman in our group of villages by the name of Nakoshi (unwanted); most often she is the second or third daughter born in a family. On the other hand is the saying that a mother of grown-up sons is the queen of her household. A childless woman or a widow is unwelcome and suspect in social and cultural gatherings, whereas a natal family will perform the funeral rites of their daughter, who was murdered by her husband, with great ritualistic solemnity because she died as an 'Aayo' or Savashin (meaning that it is lucky to die while one's husband is still alive). We have to view the incentives and disincentives in population programmes in the same light. In some countries couples will receive financial support and encouragement from the government to have more children and on the other hand, there is a move to deny maternity benefits or ration cards to couples (read women) who have more than two children in India. I know of a middle class acquaintance who was even embarrassed to appear in public because she was pregnant for the third time.

patrilineality emerge?

Social Construction of Normalcy The idea of normalcy is certainly not value-free. Every time a girl is asked to sit or talk in a particular fashion, or is reminded of the wonderful marriage that she will have some day, she is being browbeaten into being normal. The role play in childhood, so cute otherwise, is a preparation for the future that will conform to well defined norms. The daughter is to be protected from harsh realities (except economic and drudgerous ones), and no coping mechanisms are to be taught. Ignorance is a feminine charm with which we must flatter men. Girls are not to learn about their bodies, much less about the parts 'down there'. A fertile ground for sexual abuse is thus created inside the home and in other 'safe environments' such as schools, remand homes, asylums and prisons. In our village alone, there are more than four girls who have returned to their natal homes because of sexual abuse from their male in-law. Equally rampant is the abuse in one's father's home from trusted relatives and neighbours. When most of a woman's life is spent learning to conform, how then can a woman identify with someone who is childless or deserted or who is beaten by her husband? Worse still, how can one childless woman feel solidarity with another childless woman if she hates her own condition? The impact of social conditioning is so strong that individual women try to escape from their own condition using individual solutions, and in our desperate attempt to move from deviance back into normalcy, the very realisation of collective political consciousness is denied to us. The case of childlessness illustrates the point. Rituals as well as reproductive technology, at el there J10 of the spectrum, offer individual solutions to the problem of infertility, without allowing us to take into account the politics of infertility and it's social treatment.

The divisive politics of wanting to be accepted as normal has devastating effects on the bodies and minds of women: So what if I am plain looking, at least I have a son! So what if I have no children, at least my husband has no other lovers! So ~hat if my is unfaithful to me, atleast he has not deserted The incentives for cohabiting with men (and the husband Even if the individual woman were to gain a little disincentives for not) are of course well known. Ask me! all her efforts to conform, beyond doubt the man single women or lesbians for the details. No wonder from we make so many compromises to keep our marriages in question gains much more. apiece. A friend in the village said that she was living with her no-good husband only to ward off the wolves Coping Mechanisms from her door. "Find me a man who I can sleep with When the threat of one's husband remarrying (for legitimately, and I will leave my husband today", was various reasons which include suspected infidelity to what another woman said. infertility to being head-strong to being dark-skinned) In marriage, women are supposed to gain economic becomes a reality, women sometimes try to get their security and are expected to surrender their autonomy own sisters married to their husbands. Not only would in return. Atleast this is what the Sanskritised tradition the new wife's children be related to oneself by blood, would have us believe. Never mind that most women and the chances of being thrown out on to the streets of the working class are wage earners (or farmers) all be reduced, but also that through natal family their lives or that many women hardly ever gain connections one would have some hold on the economically through marriage. To preserve the myth younger wife's behaviour. We have to understand of sexual passivity in women, there have been various such acts of desperation in the light of the fact that the controls; from food taboos (especially for widows) so rate of bigamy among Hindu men in our area is 10.5 as to reduce sexual urges, to the artificial construct of %. We conducted a census of our gaothan (main patrilineality for our children. If women are asexual village) in 1992 to corroborate the hunch that we anyway, is there any need to control our sexuality always had. through artificial means? When motherhood is a Because of remarriage and the constant threat of certainty and fatherhood only a matter of trust or desertion women also tend to 'tolerate' more violent speculation, how did the foolish concept of treatment from their husbands. In November 1993, a

fifteen year old married girl was beaten so badly by her husband that some of her teeth fell out. She 'escaped with the help of some hidden money and came to her natal household, with bruises all over her body. Her parents raved and ranted at the husband who followed her immediately. Yet, they were ready to send her back with him the very next day, inspite of the girl's protests that he was planning to kill her. This was only because they had got the girl married with great financial difficulty and they were afraid that her husband would desert her after all! "A woman must enter her husband's house standing and leave it sleeping (dead) ", is what the women in our region say. Another saying instructs a married girl's parents that they should "be prepared to hear either that the daughter is cohabiting or that she is dead". It is of little surprise then, that women turn their helplessness and sorrow inwards, resulting in various mental and psychological disorders. Are coping mechanisms normal behaviour? Can one be normal in abnormal situations? (What are normal situations anyway?). The overriding feminist concern here is to give women the opportunity to replace what is normal with what is desirable. In the face of poverty and drudgery, we have seen that women tend to fast (ritually), more often as compared to their affluent counterparts. At a modest estimate, one could say that poor women fast in our region atleast one a week. While the fasts are meant to appease deities or to seek their benevolence, one cannot hide from the fact that this is a form of voluntary starvation, a coping mechanism in poverty. The results of life long starvation and deprivation are well known; maternal mortality being just one example. Women tend to suffer more from chronic and persistent illnesses which have no real cure, and so the crime against working class women is doubled, because their access to health care gets further reduced. Neither can one always show the direct, oneto-one relation between the illnesses and me occupations of the working class, and so asking for compensation when a woman undergoes a miscarriage on the worksite is unthinkable, more so because most women work in the unorganised sector, anyway.

practitioners. Through the riots after the demolition of the Babri Masjid in December 1992, we got a horrible glimpse of Hindu pogroms against the Muslims, who are also a religious minority with many unpleasant stereotypes attached to them. All over the world, women are being dumped with dangerous contraceptives because we, being women are considered to be too foolish, irresponsible and scatterbrained to be able to decide about and regulate our own fertility. For our own good, we have to use anti-woman fertility control techniques. Pornographic stereotyping of women says that it is we who ask for rape, even when we are three years of age and the rapist is a married man of fifty. Stereotyping and generating myths is a precondition for creating irrational mass contempt, and this in turn becomes a fertile ground for carrying out a genocide.

Challenges Norms and Stereotypes Before the feminist movement got active in India, 'rape' was an unmentionable word in genteel living rooms. Today, the crime has a name. The women's movement especially that in the West, created a major breakthrough in the way that we looked at our bodies. Backed with a self-confidence and pride about our reproductive functions, some of us, including I had the opportunity to experience childbirth as a wholly new and refreshing act. We learnt to be suspicious of maledefined constructions of our bodies. Searching for new words, new meanings and new wisdom, is thus an ongoing attempt to free our bodies and minds from external controls.

To challenge stereotypes and unfounded beliefs about women has been an important exercise for all of us. The challenge before us today, is to provide space to all marginalised and persecuted minorities, because a society that carries stigma or contempt towards anyone oppressed stratum cannot become free of biases towards another section. The rights of the disabled, the mentally ill, the lesbians, the homosexuals, the prisoners, the sex workers, the dalits, the tribals, the religious minorities and the politically persecuted have to be a part of feminist real politic. Just as we do not believe that women will be automatically liberated through other social and political revolutions, feminist ethics demands that we place all the invisible minorities’ centre-stage. None of the marginalised minorities are going to be Myths and Stereotypes automatically liberated when feminism prevails, unless In spite of the fact that most rapes occur within the all of them are a visible part of our day to day struggles. four walls of the house, that women are battered inside To create space for each one of us, we have to create the house and that most women die in suspicious space for all of us, and vice versa. circumstances within the house, patriarchy would have Eugenics has to be challenged every where, whether us believe that the home is the safest place for women. Inspite of the fact that most women all over the world it takes the form of population control, of ousting are wage earners we are expected to believe that the tribals in the name of development, of weeding out the head of the household is invariably the male. We are disabled , of finding genetic explanations (and surgicotold to acccJ5t the fact that women are hysterical, medical solutions) to social 'deviance' such as dangerous, fickle and in constant need of protection homosexuality, aggression and crime, or of giving and control. Inspite of common experience and scientific validity to mythical stereotypes. With the knowledge, everyone is expected to believe these progressive space closing in around us, we have to go forward and reclaim what we are losing. We have to mythical stereotypes of women. question the very premise of normalcy and deviance, to We could have laughed off the matter as yet another be able to retrieve our human and reproductive rights. foolishness of patriarchy; only we have to remember that there is a sinister side to this stereotyping. Nazism played on prevalent myths and murdered six million Jews through the years of the Holocaust. Thousands and thousands of women were burnt at the stake as witches in Europe, and it is fairly, certain now that many of these women were midwives and health

Some Thoughts On Clinical Trials THE Phase III trials forNorplant-6 are currently on in ten Human Reproduction Research Centres of Indian Council For Medical Research all over the country. Peculiarly, this contraceptive is being offered to women along with already tested ones like oral pills and Intra Uterine Devices in the Family Planning Programme in a cafeteria approach. These decisions as well alo the design of these trials have raised many medical, social and ethical issues. In this exercise, we in the FFWH would like to focus on the research methodologies involved in these trials and the very basis of the so-called scientificity, objectivity, as well as on who decides what for whom.

The Question of Ethics The issue that has been raised most often has been of ethics and with in that too, that of informed consent. Although it is agreed to on paper, in reality no information is given to the woman undergoing the trial. The question of telling her about the good and bad effects of the particular contraceptive being tried is redundant because even the fact that it is a trial in which something with unknown effects is being tested on 'normal' women is not revealed. Right from the experience of the NET-EN trials to the testimonies of women on whom NORPLANT and anti fertility vaccines are being tried, this has been a common observation recorded by various people. This total lapse in maintaining basic norms of ethics has been explained away as an objective, scientific way of carrying out scientific trials. At the same time claims of scientific trials are many a times moulded to sui1 the requirements of many agencies in whose interests the long-acting, provider-controlled contraceptive or any drug/contraceptive work. Animal Trials are considered to be the basis on which human trials of drugs are based. The results of these are, however, very much interpreted according to the needs of the researcher and the establishment. When Depo-Provera proves to be carcinogenic to beagle dogs exposed to it, we women are told that we need not worry about similar problems because we are so different from the dogs in any case. At the same time and in the same vein, human trials of anti-fertility vaccine are begun because of its positive results when tried on animals! In this case, the fact that the supposed antigen molecule is a foreign element for the animal but it is something naturally produced in women's bodies and is hence not foreign also does not stop the similarity from being drawn. The point is that once it is decided by the providers that long acting contraceptive of particular kind are needed and are safe, this is what is proven apparently

very 'scientifically'! The latest example has been again of Norplant. The problem began last year when government decided to carry out phase IV trials for Norplant-6 without having carried out the phase III trials for the same. The 'scientific' argument that was forwarded was that since phase III trials had been done for Norplant2 and Norplant-6 contains the same synthetic hormone, namely .progestin "the phase IV trials of Norplant-6 could be started straight away. The truth, however, was that though the basic hormone was same in both, the implants were made differently. Norplant-2 had two rods of a silastic material that were impregnated with hormone and Norplant-6 has six capsules made of a different silastic material in which the hormone is filled. The silastic material used for Norplant-2 was found to be carcinogenic to the workers working in its Production plant. Hence further animal studies were demanded and to evade 'costs' of carrying out such investigations the manufacture of No nt-2 had been halted. The plea for carrying out phase-IY trials directly was that both these implants released the same chemical in the same quantity into the blood stream. That their method of delivery was different, that the material of which they were made was different considered inconsequential. In tI1e face of 3 strong protest from a over the country, this move was altered and phase-III trials of Norplant-6 were announced. The process of fooling people under the garb of scientificity has however, not stopped even DOW. Now phase III trials of Norplant meant for testing the efficiency and effect of the drug or device are proposed to be carried out alongwith testing of acceptability, which is a criterion usually tested in Phase IV of the trials. For this, a so called cafeteria approach is being propagated. So the study tests women's perspective on spacing methods by offering them the pill, IUD and Norplant simultaneously and asking them to make the 'choice'. How can an untested contraceptive method be offered along with others which have been used so openly and rampantly for so many yeas and hence which have been tested in the Indian context? What is the ethics of such trial? And what is its 'so- called' objective scientificity?

The Objective Scientific Paradigm This brings us to very crucial aspect of clinical trials, which is, their objective scientificity. Although all the above examples are from Indian context, it is not as if this is an issue of misuse or misrepresentation of 'scientific method' in a particular social situation. The scientific method itself has space for the researcher's subjectivity. It is not the subjectivity that we would like to

question. The objections are to the fact that the process claims objectivity and hence makes the result appear to be objective and value-free. As a result it successfully hides the existence of any agenda in the process of scientific enquiry. It projects it to be something that is an open, natural quest for knowledge while in reality it is far removed from this. The research on contraceptives has revealed this reality. In fact in the clinical trials the objectivity is demanded of the person who participates in the human trials. But precisely for this reason, information on trial and trial product is not given to the subject. The 'scientific' reason given by the researchers is that if they tell women about the possible side effects, they would turn hypochondriac and start 'imagining' all those 'side-effects'. This would prevent any 'objective' evaluation of these side-effects. In the absence of the knowledge about how a particular contraceptive works and in the situation when contraception is a dire necessity, many a 'times a lot of side-effects are not connected to the contraceptive and hence are not reported. Not knowing anything about hormones it is difficult to imagine that something put under the skin of the forearm could lead to pain in the calves! At the same time, the openness required of the researchers also does not exist. He/she has a set of given symptoms to look for and naturally ignores many others that might be connected. There is an understanding about how contraceptive works and only those aspects that fit in to this understanding are looked into. All others are ignored. Unless there is an openness towards the fact that the basic understanding itself is not necessarily complete, the trials themselves could miss out on a number of crucial side effects and problems. The problem is hence that an 'objective' method of this type essentially just succeeds in negating a lot of subjective experiences. It is not possible to eliminate subjectivity. The issue is of recognising this subjectivity' and stating the vantage point from which observations and deductions are made.

Reductionist approach Not accepting the role of the subjectivity of the researcher has a deep connection with the arrogance of possessing an understanding. At any point in 'scientific' research, it is necessary to acknowledge the transience of the understanding. In modern science today, it is the reductionist approach that prevails. Looking at the whole as conglomeration of its constituent parts is the method applied to all matter – living and non-living. The body then is looked at as made up of its organs and in the process the active interdependence of various organs is negated. With this approach it is almost possible to look at the body as a whole and hence effects of the contraceptive being are also not seen on the whole body but are restricted to the oarticular-0J"2ans on which contraceptive is supposed to work.

The side effects recorded and noted or observations made in the case of contraceptives, all essentially deal mainly with the reproductive organs. For example, with Norplant it is assumed that it affects the hormonal cycle related to reproduction and so only those effects are studied. In fact, Norplant disrupts a natural body cycle continuously for five years and yet after removal the only thing that is checked is whether the woman can conceive again or not (note the reductionism here, Norplant is a contraceptive and affects conception) with the one-dimensional attitude that return to normalcy after removal of Nor plant is confined to observing whether there is return of fertility or not. Since the presumption is that other systems are not affected, it is not even felt important to see if there are any other long-term effects on the rest of the body. Even while studying the effect on reproductive organs, the observations arc confined to the woman's organ alone. It is quite possible that although there is conception, there could be some disturbances in the reproductive system which would affect the progeny and their reproductive capacities. Follow up with at least one generation of women exposed to systemic changes for such long periods is the minimum that could be expected of a research trial. These, however, do not ever become the concerns of the 'scientific' researchers. The belief in a reductionist paradigm has led to a lot of problems of short-sightedness in various areas of science and technology. In contraceptive research the urgency that drives researchers is that of controlling population by more effective ways, Cost of women's health is probably seen as a small cost in the process. Yet, there could be unpredictable, irreversible effects which would alter the picture of the population. The effects of drugs like DES given to pregnant women are still visible in the disabled, cancer-prone, infertile DES daughters today. These are lessons of this century which have to teach us to pay heed and get over our sense of urgency leading to destructive short- sightedness. The pace at which new technology is being introduced, however, does not at all take into account such questions and concerns. Newer contraceptive methods are evolved to control reproduction since that is the major goal identified today and hence is pursued at any cost. The cost-benefit analysis which serves as the basis for acceptance of any technology is done without questioning the 'basic definitions and understanding of costs to be paid and benefits to be achieved. In this process the justification is further obtained through the most 'effective' tool of modern science, that of statistics.

Game of Numbers Whether something is statistically significant or not seems to be just a number evolved through rigorous mathematics. Yet what is hidden behind this index or co-efficient is a base and background of subjective social values. Objectivity demands negation of personal experiences and generalisation based on the statistical data collected. If the person's body undergoes drastic changes many times it does not matter so long as the average is within the decided norms Ie or the changed status of the body is normal according to the averages u calculated. Again Norplant-2 phase III trial results are important to d study. Reportedly by the end of two years (which is when the report v was written) 17% women had got their Norplant removed due to

complaints of menstrual disturbances. These were the women for whom the pain and trouble was unbearable. For whom fulfillment of even the dire need for contraceptives could not be compensation enough for the pain and discomfort. Other than these extreme cases, the menstrual disturbances caused by Norplant are studied in terms of certain average numbers. A three monthly observation of each woman was done. If that woman menstruated two to four times in these three months, had cycles of 22-35 day duration, bled for 10-20 days and maybe had some spotting for a total of about 10 days in that period, it was assumed that she did not suffer from menstrual disturbance! The criteria seem very exhaustive and considerate until one looks carefully at the experience of an individual woman that is hidden in these neat, specific numbers. A woman who has a regular cycle of m 30 days and bleeds about 5 days during each menstrual period is considered to be 'normal' by these criteria. If with the Norplant her as period changes in such a way that she has her period after 35 days and if she bleeds about 10 days each time and maybe even spotting for few days (something she never had before), she would still be re normal and 'scientific' decision would be that Norplant does not affect her menstrual cycle or that she does not suffer from menstrual disturbances due to Norplant! Another game of numbers is played when deciding on the sample size for the phase III trials. For testing of contraceptives it is necessary in this phase that observations be made for twenty thousand menstrual cycles. This could be achieved by studying two thousand women for ten menstrual cycles or two hundred women for hundred menstrual cycle or whatever he the combination. This appears to be large number until one realises that the criteria has remained the same for all contraceptives including those that are supposed to work continuously for five years. In case of Norplant - a contraceptive which works for sixty months this would have meant a study with about four hundred women for five years. Here the number is much smaller but we presume that the study s done for full period for which it is supposed to be used. In actuality, however, one thousand four hundred and sixty six women were involved and a total of twenty thousand six hundred sixty nine menstrual cycles were studied. This meant that the results were based on trials in which women were observed for only a period of one year or two years. It is quite obvious that for testing of long-acting contraceptives, studying 20,000 menstrual cycles is not at all sufficient. It in fact presumes that there would be no special effects on the body in spite of the long-term, continuous exposure to that contraceptive. Flaunting scientificity while actually cooling us with the results that arise out of each lop-sided trial is hardly something that can and should go unchecked. And here comes the most crucial question.

Calculation of Costs and Benefits Once we accept that no research or process of generation of knowledge is value free; once we acknowledge that even the most objective looking processes are coloured and affected by the subjectivity of the person/agency generating it; once we recognise the role of social norms in determining what are facts -we can not talk of costs or benefits without answering the more basic questions of qualifying what one means by costs and benefits. Benefits for whom at whose costs has to be clearly spelt out before even making the facts known. As far as the long acting contraceptives are concerned, the fact that they act for a long period of time itself means that they do not leave any space for intervention by the person on whom they are used. They mean a total dependence on that particular drug or device and hence on the agency that is responsible for providing that particular contraceptive. In that sense they are mainly and specifically developed for purposes of eugenic population control. They are aimed at and used against the most deprived sections of the world's population and meant to control these populations. We believe that so long as the agenda remains the control over population there can be no benefits for women on whom all these methods are used it is pure and simple control over their bodies to gain other ends. All research and development in this area will be affected by this attitude and aim and hence it needs to be questioned at its very basic tenets. Women are made the targets of all reproductive technology apparently because their bodies obey certain cyclical pattern. Hence the scientists claim that it is simple to intervene in their bodies. They also produce only egg at a specific point in the cycle which again makes it easier for an external agent to exercise control. Then again because women's bodies finally carry the child and produce it, it is assumed that they are more responsible for the process of reproduction. In all this very scientific reasoning there is, however, a strong reminder of society's norms and biases which are shaping the direction of research. Reproduction is more and more becoming women's business and controlling is society's. So the vaccine against sperms is said to work best also in the woman's body! At the same time as more knowledge about human bodies is produced, we are getting lesser access to simple facts about our biology. So in spite of the claims of the scientists, we feel that this attitude is arising out of what society believes in, rather than out of the specific biology of women.

The Way Ahead Within this scenario the question before us surely is how do we move ahead? If we have to stop reacting in a piece-meal fashion to every new method that is being introduced, we have to define ourselves and our needs better. We have to define for ourselves what our expectations from any reproductive technology are. While critically examining the research processes and methods it is essential that we begin to take control. On doing some thinking along these lines we have felt that the process of reproduction and conception has to be relocated in the interaction between the man and the woman. Obviously then any control over it would be achieved by alteration in this process.

Making internal changes in the body so that the egg or the sperm are not produced would be methods that would begin at the wrong point itself. According to us chemical interventions in normal human beings is not something which denotes any kind of control. When we looked at this way what appear to others as 'minor' side effects are actually serious conditions as they are result of an unnecessary intervention. When one begins from this point, the evaluation of the reproductive technologies has many different dimensions. It requires that the research processes also be altered. There is no need to fool ourselves with lop sided analysis and results. The direction of research, the methods used, the norms of ethics followed will all have to be very different. They have to be more' person' oriented; more for the user than for the population controller.

Reproductive Technologies and the Third World Malini Karkal THE developments in the field of reproductive developments in these fields expose the poorer and technologies have brought into focus the future of disadvantaged population, especially those in the Third reproduction, sexuality, parenthood and family. World, being made the guinea-pigs. Generally science and technology are seen as 'neutral' and above class, race and gender. However a careful One observes that India has made progress in look at the developments will show beyond doubt that several fields. In the field of health the training and they are much more like the less-than-neutral society that skills are comparable to that found in the produced them. The Third World women are therefore industrialised countries. It is therefore observed that faced with problems that originate from the exploitative the Indian medical professionals have much easier nature of the relations with the Industrialised societies on access to the opportunities for employment in the one hand and underdevelopment in their situations on the Industrialised countries and there is considerable outother. In addition, they are faced with the problems that migration of trained personnel. Those trained women face in the patriarchal structure of the society. individuals who opt to live in India have opportunities The dangers of the developments in the science increase to work for the economically better-offs. Most of the because modern science is action-oriented and it does general population meets its health needs through the not simply contemplate nature, but seeks to dominate services that are supplied by the Government and exploit it. (Through 20,531 Primary Health Centres and 130,390 Sub-centres asof31st March 1990, Government of In the developed societies in-vitro fertilisation, India, 1991, pp. 282 and 284). Private medical surrogate motherhood, genetic engineering, frozen practitioners take positions in several of the governembryos or egg donation, are getting wide popularity. ment medical institutions. A large population is The issues at stake in these societies are the portrayal of therefore available for research in the medical field. science as a realm of boundless progress that manipuThe birth of the first baby by in-vitro fertilisation lates natural obstacles such as infertility and genetic inheritance of 'undesired' qualities to the satisfaction of in India was reported in 1978, the year when the birth human needs. In such an image of science, what is not of Louise Brown, was reported from London. UK. seen are the extensions of possibilities of medical and (The New York Times, October 9, 1978) Today there scientific practice in new reproductive technologies are several medical technologists who practice in the outreaching human understanding and public control? field of new reproductive technologies. Same is true They interfere with the 'naturalness' of the reproductive for newer developments in other medical fields. And process and have dangers of commercialising it. They yet in the hierarchical society the average individual also allow greater scope for the application of eugenic does not have an access to the basic services. policies that would be discriminatory. For developing newer dimensions of the technologies as well as newer In a patriarchal society such as that in India the technologies themselves, requires experimentation with discussion on women's health gets much larger human material. The laws in the Industrialised countries attention in the context of illness and death during deny easy access to such material. Interests in the pregnancy, childbirth and to some extent issues related

to contraceptive use. This also limits the services to the reproductive health of women who are sexually active. This approach ignores the problems of women who are not sexually active. It is known that many of the problems arising during the reproductive years or related to reproductive performance of women are rooted in their life before they become sexually active and women suffer beyond the active life. Tragic fact of the society is that the women have internalised pain and suffering emanating from sexual and reproductive roles and they are considered to be the essence of womanhood. Poverty, unhygienic living conditions and several socio-cultural taboos cause health problems and a culture of silence. Reproductive tract infections (RTIs) are common and they have serious consequences, not only for women, but for the men and the children who are related to these women. Illnesses and deaths due to complications of pregnancy, childbirth, unsafe abortions, diseases of reproductive tract, effects of harmful contraceptives, are the major causes of ill-health of women. Poor health of women also results in poor health for the children that they produce. (Karkal, 1985) Consequently India is one of the countries with high infant and child mortality. In India the official estimates indicate that about30 percent or the population is living below poverty line. (That is the population that receives less than 2400 calories per day in rural areas and 2100 calories per day in urban areas-Government of India, 1991, p.180). Under these conditions malnutrition is common. National Nutrition Monitoring Bureau reports that barely 40 percent of the children receive diets that can be said to be nutritionally adequate. On an average these children face a deficit of about 350 calories against an estimated requirement of 1250 calories for normal growth and development of a three year old. As a consequence of undernutrition it was found that about 5.7 percent of the girls in the age-group 1 to 5 years had normal growth whereas 60 percent had moderate to severe nutritional deficiency (NNMB, 1989). These girls face several health problems during their lifetimes and among these are the problems related to their reproductive health.

In India the average number pregnancies that a woman undergoes is around 6 resulting in 4.3 births (1985). The numbers of live births are 4.6 and 3.3 for rural and urban areas respectively (Government of India, 1991, p.14). However it needs to be noted that these figures being averages, indicate that there are Woolen who bear 4 and more children. Available data indicate that 725 women out of 1,000 belong to parity 4 to 5. Such women are 733 in rural areas and 696 in urban areas (Government of India, 1991, p.154). Contrary to the popular belief that childbi5th gets easier with each experience of it the risks involved in repeated childbearing are many. The second and third births are the most trouble-free, while the risks of serious complications, such as haemorrhage, rupture of uterus and infection rises steadily from the third birth onwards. Maternal mortality rate for India is reported to be 500 (World Bank, 1990, p.240). However there is no figure that is considered to be conclusive, more so because levels as 1,360 have been noted in certain rural areas. Because of higher fertility the actual risk of dying from maternal causes, is much higher for an Indian woman compared to her counterparts in many other countries with lower fertility. Inspite of the fact that the population, in general has high mortality, it is observed that during the reproductive years female mortality, compared to the males in same ages, is atleast 50 percent higher (Registrar General, 1989). A study reported that for every maternal death there were 16.5 cases or maternal morbidity or illness related to pregnancy, childbirth and puerperium (Dutta et. al. 1980). Data from the developed countries have shown that girls take atleast 5 to 6 years for full physical growth after they have attained menarche. One study found that even two years after the onset of menstruation a girl may have between 2 percent and 9 percent of pelvic growth and 1 percent of height still to achieve (Moerman, 1982). Obstructed delivery due to disproportion between the size of the infant's head and the mother's pelvis (cephalopelvic disproportion) is most common among very young mothers. This fact is to be seen in the context that inspite of large expenses on medical education as well as establishment of primary health centres and subcentres, only 14.7 percent of rural and 48.7 percent of the urban deliveries receive medical attention. Training of Dais is one of the programmes of the government. And yet only 17.8 percent of the rum.1 and 25.5 percent of the deliveries are conducted by trained Dais (Government of India, 1991, p.l55).1t must however be noted that such trained Dais have limited abilities to tackle many of the complications and cannot do caesarean deliveries needed in the obstructed labour.

While the newer technologies are developed on one hand, it is observed that an average woman has hardly any access to minimum help at the time of her needs. In India, for example, marriage is early and universal, and so is child bearing. Latest available official data indicate that the mean age at marriage for girls in India was 18.33 years (1981). This however was an average and many girls were married in younger ages as is indicated by the figure that 6.30 percent- of the girls in ages 10 to 14 were married, widowed, divorced seperated. Percentage for such girls in ages 15 to 19 was 44, for ages 20 to 24 it was 86 and for ages 25 to 29 it was 97. (Government of A pregnancy before full physical development is India, 1991, p.l04) Seen from another angle 14 to 15 harmful to both the mother and the fetus since they need percent of all women who marry do so before the age 19; and 2 to 3 percent before age 15. Consequences of early to share the resources. No data are available from the marriage are more serious for girls who are married early developing countries. However it would not be wrong to believe that the prevailing conditions of undernutrition and have had less opportunity for full growth. and poor attention to the girls, the period required for It also needs to be noted that the child bearing in India adequate growth should be longer for girls in countries also starts at younger ages. Available data show that age such as India. It is generally believed that the ages 20 to specific marital fertility rate (number of children born 30 are the safest period of woman's life for childbearing. per 1000 married women in the specific age group) is Women in younger as well as older ages run higher risks 249 (rural) and 299 (urban) for women in ages 15 to of dying. Royston and Armstrong (1989, p.47) report 19,318 (rural) and 325 (urban) for women in ages 20 to that women aged 15 had a maternal mortality rate 7 24 and 240 (rural) and (209) urban women in ages 25 to times that of women aged 20-24 years. Similarly the risk of death doubled for women of30-34 years and increased 29. (Government of India, 1991, p.132). live-fold for women over 40 years compared with

women aged 20-24 years.

Bang (1989) in the state of Maharashtra in India showed that 82 percent of the 650 women examined were The degree of risk to any woman, i.e. including suffering from one or more gynaecological and sexual women in ages 20 to 30, is enormously affected by her disease related to RTI. On an average the number of socioeconomic circumstances - her general state of infections suffered by a woman was 3.6. Less than 8 health as well as her access to good quality health care. percent of the women in the survey had undergone a Royston and Armstrong report (1989, p.47) that a woman gynaecological examination. Overall it was observed already in good health and moderately active, requires that 99 percent of the symptomatic women and 84 about 300 extra calories a day as well as an increased percent of the non-symptomatic women had supply of vitamins, folic acid, iron and other minerals. gynaecological infections. Bang says that generally the For the first six months of breast feeding the extra calorie diseases that do not kill are neglected. However their requirement is approximately 550 per day for a woman consequences included difficulty in occupational and who is well nourished during pregnancy and who has domestic work because of chronic backache caused by thus been able to lay down reserves, particularly during pelvic inflammatory disease (PID) and cervical erosion the last three months. (present in 30 percent of women) foetal wastage due to abortion or stillbirth caused by syphillis or chronic PID The food intake of women in India, particularly for (38 percent of the women had bad obstetric histories) those in low income groups is substantially deficient in neonatal infections from birth-canal infections, anaemia calories. In respect of pregnant and lactating women, this due to menorrhagia, marital disharmony due to sterility daily deficiency has been estimated to be as much as 1, (7 percent) or sexual problems (9 to 12 percent) and 000 calories or more. During pregnancy these women anxiety and stress. gain around half the weight gained by their counterparts In a patriarchal society, that accepts social value in the better segments. To meet the dietary recommended requirements, unlike well-nourished which places emphasis on women bearing children, women, those from poorer groups need an additional women have little choice but to start their childbearing sixth of the food they habitually eat. Studies show that early, conceive at short intervals and produce many dietary intake of rural and urban women in India ranges children. Preference for male children further between 1,200 and 1,600 calories per day, but there is complicates the position of women. hardly any increase in intake during pregnancy over the The law on abortion was liberalised in 1971. Indian pre-existing levels, despite the fact that the low-income groups continue, of necessity, to remain active abortion law is one of the most liberal ones. Inspite of throughout pregnancy. Evidence suggests that a wide network of health services, under prevailing social combination of inadequate dietary intake and physical norms, Indian women cannot avail of the services of activity may result in deterioration of maternal terminations of their unwanted pregnancies. It is nutritional status and poor growth of the foetus. It has therefore found that about 3 to 4 pregnancies are been reported that foetal wastages i.e. abortions and terminated illegally for each pregnancy that gets stillbirths, occur in 20 percent of conceptions in poor registered under the abortion Act. (Karkal, 1991a) communities. These very groups tend to have low birth Mortality in legal abortions is estimated to be 2 per weights and perinatal (stillbirths + first 7 days of life) mortality rate ranging from 50 to 70 per 1,000 live 100,000 procedures in the Industrialised countries and 6 births. Clearly, the factors which result in foetal loss per 100,000 in developing countries. But clandestine overlap with those that bring about neonatal (first 7 days abortions give rise to very high mortality -50 deaths per 1,000 procedures in Industrialised countries and about of life) deaths. 400 deaths per 100,000 procedures in developing Doubts are however expressed about the From 40 to 50 percent of the urban and 50 to 70 countries. incidence of deaths in developing countries and the percent of the rural pregnant women are known to be of 400 is believed to be an underestimate. anaemic. In areas where hook-worn infestation is figure (Hogberg, 1985). Khan (1985) reports 10 deaths in 412 endemic, this percentage is about 90.llookworm procedures giving a death rate of 2,400 per 100,000 infestation is reported to be present in 40 percent procedures. In Asia 20 to 2S percent of the maternal pregnant women. Anaemia usually antedates pregnancy deaths are attributed poorly performed abortions and is aggravated during pregnancy and labour. The (Khan. 1985 and Rochatto1981). consequences of under-nutrition are compounded by pregnancy-associated problems like anaemia and In patriarchal societies women's rights to enjoy hypertension (UNICEF, 1991, pp.1I-12). sexuality, separate from reproduction and free of fear A study in Uttar Pradesh and Bihar has shown that a negative consequences, on equal footing with men, third of the pregnant women suffer from clinically and unacceptable. In addition there is medicalisation of significant (below 8g/dl of haemoglobin) anaemia and are about one in five are seriously anaemic (below 6g/dl). women's normal life and bodily functions. This has in shifting the control from an individual to the Taking the criterion of below 11g/dl haemoglobin level, resulted profession. It is observed that male dominance over 90 percent of rural pregnant women in these two medical in sexual relations and non-access to contraception states are anaemic (Agarwal, 1987). Clinically makes women have no control over their pregnancies significant anaemia among pregnant women in South and childbirths and diseases. Over and above India is 2S to 30 percent and thus it compares with the this under the contracting pressures from governments of figures from Uttar Pradesh and Bihar. Another countries as well as the international disturbing finding comes from a study conducted by the Industrialised agencies, the developing countries are forced to accept Indian Council of Medical Research (ICMR). This population control policies, and in the prevailing study evaluated the 20 years efforts to prevent situation the targets of these policies are invariably the nutritional anaemia among pregnant women. The women. In India the total couphs, 41.9 percent are findings showed that there was hardly any change in the contraceptors. Of of these 29.8 percent are sterilised, 5.9 incidence of anaemia among the women. percent are users of IUDs, 1.7 percent users of oral pills 45 percent couples use other methods including Both bacterial and viral infections remain major and condoms (Government of India 1991, pp.244-245). health problems in developing countries. A study by

Population controllers are not happy with the rate of acceptance of the contraception, since the birth rate continues to be high. There are therefore pressures to increase the acceptance of contraception. It is observed that 85 percent of the sterilisations are tubectomies and even among other methods there is a prevalence of female methods. The newer methods that are proposed to be promoted are essentially longacting providing controller-controlled female methods such as NOR PLANT, anti-pregnancy vaccines etc. (KarkaI1992). Experience of the functioning of the family planning programme in India, clearly shows evidence of coercion (KarkaI1991b). Over 40 years of experience in family programme in India has shown that it cannot bring about the results in terms of population control, nor has it helped women to improve their health, and yet there is very little change in the approach of the population policy, except being more aggressive and more compulsive (KarkaI1989). Four major' international surveys that investigated into the incidence of primary sterility report that the incidence in India is 3 percent compared to 6 in the USA, 5 to 7 in Europe (11 in Sweden) and 2S to 30 in some of the African and Latin American countries. (IPPF 1990) Promotion of in-vitro fertilisation and other techniques inspite of low incidence of infertility, is an indication other attitude towards women that imposes motherhood on them. It needs to be added here that much of the infertility is caused by the presence of infections and due to contraceptives such as IUD. There is virtually no service to attend on these problems of women that are much more serious and have wider consequences.

References Agarwal D. K. Volume 24

Family Welfare, Department of Family Welfare, Year Book 1989-90 Hogberg U. (1985) Maternal Mortality – A Worldwide Problem, International Journal of Gynaecology and Obstetrics, Volume 23 IPPF (International Planned Parenthood Federation) (1990) Infertility, IPPF Medical Bulletin Volume 24, Number 6, December, p.5 Karkal Malini (1985) Health of Mother and child Survival in Dynamics of Population and Family Welfare edited by K. Srinivasan and S. Mukerji, Himalaya Publishing House, Bombay Karkal Malini (1989) Can Family Planning Solve Population Problem? available with the author at 4 Dhake Colony. Bombay 400 058, India Karkal Malini (199la) Abortion Laws and the Abortion Situation in India, Issues in Reproductive and Genetic Engineering, Volume 4, Number 1 Karkal Malini (1991b) Compulsion- Political Will and Family Planning, available with the author at 4, Dhake Colony, Bombay 400 058, India Karkal Malini (1992) NORPLANT - A Long-Acting Contraceptive, available with the author at 4, Dhake Colony, Bombay 400 058, India Khan A. R. et. al. (1985) Maternal Mortality in Rural Bangladesh, World-Health Forum, Volume 6:325 Moerman M. L. (1982) Growth of Birth-Canal of Adolescent Girls, American Journal of Obstretrics and Gynaecology, Volume 143:5

NNMB (National Nutrition Monitoring Bureau) (1989) et. al. (1987) Indian Paediatrics, Registrar General (1989) Sample Registration Scheme, Government of India

Rochat R. W. et. al. (1981) Maternal and Abortionrelated Deaths in Bangladesh, 1978-1979, Bang R. A. et. at. (1989) High Prevalence of Gynaecological Diseases in Rural Indian Women, The International Journal of Gynaecology and Obstetrics, Volume 19:155 Lancet, January 14 Dutta K. K. et. al. (1980) Morbidity Patterns Amongst Rural Pregnant Women in Alwar, Rajasthan - A Cohort Study, Health, Population Perspective Issues, Volume 3

Royston Erica and Sue Armstrong (1989) Preventing Maternal Deaths, World Health Organisation, Geneva World Bank (1990) World Development Report 1990, -Poverty World Bank, Washington

Government of India (1991) Ministry of Health and

Integrating Women's Health Care into Primary Care Programme An Experience ARCH Team, Mangrol

SINCE the search began to evolve viable primary health care model in 1980, countless trials and errors have slowly directed us to new objectives and goals. From the relative down playing of the acute medical care we moved to primacy of acute medical care. With that our goals, methods and contents of training of CHWs got shifted. Similarly from maternal health car as an adjunct to under five care we were slowly moving in the direction of women's health problems. Women bear a major brunt of nearly everything that has gone wrong in the rural society. Their intolerable situation about literacy, access to resources, a place in the family hierarchy and a say in the family decisions places them at the bottom of the social heap. It is a fountainhead of the most important of health problems of India; ranging from horrendously high maternal mortality rates (among the highest in the world), high infant and childhood mortality, haltingly declining fertility rates, to still uncomfortably high population growth rate. The whole of the women's world, its suffering, it un-communicated anxieties, and their raging conflicts lay completely hidden from us. Women in rural areas are naturally not aware or high mortality indices, nor are they conscious or worried about the high national fertility rates and growth rate. Apart from generally perceived problems of leucorrhea, menstrual pains etc. their world seethes with other deeply buried problems and anxieties. In a well planned random survey of the women across the social spectrum we found that a large proportion of the women individually want spacing of the children. They want to limit their family size. They want relief freedom from pregnancy fora few years after marriage. They want relief from pelvic inflammatory diseases (PID) which is their lot because of the traditional practices at labour time and because of the sexually transmitted diseases (STDs) they mostly get unknowingly. They also want freedom from unwanted pregnancies. They flock the clinic with these problems but mostly their needs remain hidden or, non-communicated. They are their personal and intimate needs and even an experienced observant eye will fail to perceive to perceive these felt needs. Most women because of their position in the society are not easily accessible to routine health services through routine schedules of health workers. The core part of their felt needs remains hidden. Once these invisible barriers are overcome, primary health care will have obtained a foothold in the uncharted territory in which are breeding India's major health problems.

in the dark, unlit corners of village houses where the deliveries occur at the hands of traditional birth attendants (dais). We believe what we saw in the huts of Mangrol and other villages is what takes place everywhere in Gujarat, in Maharashtra and perhaps throughout the rural India. We observed dozens of deliveries taking place in the homes actually being conducted by the dais. The uniformity and equality of practices was stunningly same across the social classes. Although we were called out because labour, had run into a snag, the Dai dominance of the situation was unmistakable. It took some time and unusual assertion before we could have our say. Their confidence bordered on being arrogant and they cared little for even elementary hygiene. We saw that they obviously did not know how to assess the progress of labour and the descent of foetus. Unwashed, bare hands were repeatedly introduced into vagina whether the waters had broken or not. They had little knowledge of female pelvis. A woman or a Dai would press a small bag of ash against the anus by using her heel so that baby may no find its way trough anus! Question of supporting perineum with a palm holding a clean piece of cloth during pains in later stage did not arise. The casual chain of puerperal sepsis and pelvic inflammatory disease (PI D) which plague the women was laid bare to us. Dais not knowing how to judge the progress of labour seemed to panic easily and resorting to that infamous appalling practice of 'kalla' which is to rhythmically press abdomen to effect delivery. To hasten the pace or labour very often village quack is called, requested to give 'hot injection'. He readily obliges by giving injection Pitocin in the muscle - a horrible practice which can suffocate the unborn baby and may even sometimes cause rupture of uterus, a fatal condition.

The real climax comes when the baby is born. Nobody, including the Dai, is bothered about the newborn to clean its mouth and pharynx, to wipe the eyes with clean cloth. All the energy is now concentrated on the undelivered placenta. The Dai would hold the umbilical cord in her hand as the excitement mixed with a tinge of hostility towards the undelivered placenta mounts. Another woman applies pressure on the abdomen so that placenta comes out as soon as possible. The room is filled with shouts abuses and challenges thrown at the placenta. Placenta is believed to be an evil which can deceive, give a slip and rise high in the abdomen through chest to strangle the women! In popular parlance it is know as 'meli (filthy). The quick delivery of placenta because of the nature attributed to it through tradition is so urgent that in the maneuvers adopted and atmosphere and attitude prevailing at the time it is easy to see how in Dais, Maternal Mortality and PIDS unknown but high proportion of women, placenta is Before we started to live and work in Mangrol we separated prematurely from the womb causing excess were always intrigued by the fact that India has one of bleeding, which, even if moderate, could be lethal to highest maternal mortality rate in the world. It was many women who are anaemic and on the edge of iron even more intriguing because while other sensitive balance, thanks to no iron supplements during health indicators like infant mortality are slowly but pregnancy. To us the mystery of unchanging high steadily declining, the maternal mortality remains maternal mortality and morbidity got resolved in the dozens of deliveries we observed very closely in virtually unchanged over the decades. Mangrol and surrounding villages. This is another Our stay in Mangrol gave us rare opportunity and a example of entirely different type of casual chain at window to see very closely the play of powerful forces work, producing tragic results.

The dais are so cocksure of their practices that most are unwilling to change their ways. Some have changed their methods following reorientation and training sessions we had organized for them. They are ready to exercise restraint, observe elementary hygienic practice and view placenta differently to allow peaceful, passive and natural expulsion of placenta. But the challenge to primary health care remains because majority of dais are not ready to adopt new ways. All deliveries in rural areas take place in homes (this is a good practice) and the traditional dais are always called to conduct deliveries in home.

also. If a women eats heartily (as she should) then more space in her abdomen is taken up by the food, leaving less space for the foetus to grow! Tradition bound causal chain is at work here also. Both the beliefs are premised on the assumption that abdomen is one continuous hollow box with a fixed and limited space for which there are con£1icting claims like volume of food and the growing foetus. The concept of closed and separate intestines is totally missing. There is another assumption also that growth of foetus is independent of amount of food the mother eats. There is a clash again between the traditional culture bound causal cll3ins and the causal chains of the modern health sciences. The prescriptions based on two consequences of the two prescription, one so Ante - Natal Clinic (ANC) desirable and another so undesirable that harmful In 1986 when we started the ANC programme, causal chains must be replaced by the beneficial ones. barely 15 percent of the registered pregnant women, The antenatal clinic is the mechanism and. a tool of took regular benefit of the ANC clinic. By 1992 the achieving this transformation. figure had risen to 76 percent. In the early days they laughed at the very idea of ANC check-ups. Ante-natal clinic apart from providing services also "Pregnancy is not a disease or illness that we should becomes a training ground and a testing ground of attend the clinic" they would say. The logic was strategies to be adopted and to be taken to the impregnable. There was little we could do to persuade community to achieve a desirable transformation of them that if iron folic acid supplements are not taken women’s perceptions in the community. regularly during pregnancy, anaemia is an inevitable consequence and the new-born will be born with little FHWs and the Structure of Diffusion or no iron reserves when they begin their struggle for survival in the hostile environment of the poor Female Health Workers (FHWs), barely literate and household, 'They were encouraged or even forced not drawn from the local communities steeped themselves to take iron supplements because decision makers in in the traditional culture, are the first ones to imbibe the house hold decreed that these were 'hot' tablets and cause abortion! Poor young women living through examples, demonstrations, discussions, precariously at the margin of Vitamin-A store in their dialogue and sharing the important elements of bodies develop overt signs of Vitamin-A deficiency woman’s body and functioning to shed, if slowly and (night blindness) in very high numbers during imperceptibly, the causal chains bound to the pregnancy (15% of the pregnant women in one of our traditions and adopt the ones derived from medical community surveys). This means practically the science. Based on such intricate insights, studies and whole population has extremely small Vitamin-A dialogue we made a small slide show of women's storage in their bodies. The consequences are well reproductive system and its functioning. It made an known. The young women as told to them by elder instant success. More and more women feeling secure women believed that night blindness was a normal and safe in the close relations with our team members state of affairs during pregnancy, and nothing needs to wanted to know more about their bodies, how they be done! These were obvious problems of could control their fertility and how they can gain pregnancies. The deeper problems are the culturally more control over their bodies. In the villages where conditioned food taboos and prohibitions during we had great difficulty persuading mothers to bring pregnancy. It is a universal practice that a pregnant their children for growth monitoring and woman is forbidden to take milk of milk products like immunization, women on their own demanded to yogurt ghee etc. The idea behind this restriction is know about their bodies and to see the slide show. The simplicity itself. The popular thinking makes a tiny rooms were over flowing with women. A simply straight form\ward causal connection between the written and illustrated booklet in Gujarati on their' consumption of milk and its products to the presence bodies and functioning became immediately popular. of a white thick creamy coat on the skin of the foetus- The transmission of information in this booklet was Vermix this actually protective Vermix is believed to almost instant, because the theme and contents be derived from milk and its products. And it is appealed to their most important felt need. The believed that it acts as an adhesive glue which fixes language used was almost their own because it was the foetus to the womb resulting in difficult and tested and retested with semiliterate FHWs, changed prolonged labour- a dreaded complication in the according to the feedback and the pictures were simple popular mind. This is an excellent example of line drawings which these FHWs-CHWs could different causal chains in operation producing results understand easily. Very soon barely literate FHWs which are far reaching. 111is causal chain is also a took over to read this and other booklets to small powerful obstacle to any health education message groups of women in the dispensary, in the ANC exhorting women to consume milk during pregnancy. clinics and even in the village hamlets. A process of diffusion of vital information vital from health point of view had imperceptibly passed from professionals Furthermore there are powerful taboos operating to local individuals who belonged to the same cultural through out the pregnancy to ensure that a pregnant world and who had gained acceptance of the women. women eats less and less. There is a reason for this We were laying across the cultural barriers, the

structures of diffusion with the help of FHWs who about the choices we will be confronted with about the were themselves astride the two cultures as it were. felt needs that we must accept and those that we must The structures of diffusion would initiate, facilitate reject or ignore and to know how to go about it. and strengthen the exchange of information, problems, knowledge and ideas. We have come long way since 1980 and from the original problem situation we had formulated. Long As described above our studies indicate very years of patient explorations, gropings and reflections strongly that women, especially young women, want over the dead ends we have reached, the hidden felt spacing of the pregnancies, they want limit to the size needs we had unexpectedly stumbled into and the of their families almost matching the national goal, structures of diffusion we have been able to create they was to avoid unwanted pregnancies and want through a process of trial and error have led us to a freedom from PIDs. And still adoption of reversible new problem situation in which acute health care and contraceptive methods is extremely low erratic. They women's health care occupy a position of primacy. The fact that our programme provides various types of have made very poor headway in India. health services (including T.B Care) to a few thousand When we started primary health care in 1980 it was rural populations is important but incidental. The fact a Community Health Programme to be manned by that it has succeeded in evolving and establishing local health workers within the framework of health important health care components which have roots sciences. Little did we anticipate the resistance and both in modern medical sciences as well as in sociochallenge that will come the way of rationally cultural reality of the community is, we feel, our main designed health package by the tradition? Little was achievement since ARCH came into being in early our understanding of the felt need - expressed of eighties. unexpressed - of the community. Little did we know

Conceptual Framework For Assessing Women's Health Needs .

Thelma Narayan IT is now more widely accepted that during the past few decade the health system in India, in its planning and health care services, has viewed the health needs of women primarily in terms of their child be.1ringorreproductive function. Most health programmes for women have focused on family planning and mother and child health services. The main interest seem as to have been to evolve methods by which the reproductive function of women could be controlled, so as to serve the needs of the nation, of society; of demography, of the child or perhaps the family norm was acceptable topeople.1nereis probably a consensus among people oriented health workers that this has been a narrow and limited view concerning women's health. It does not take into consideration sufficiently the personhood and the wholeness of women. There is also a growing and anxious realization that this approach has not even been able to serve the purpose for which it was intended, namely of population control. However in the process of evolving alternative approaches, the basic assumptions concerning the position of women in Indian society and their resultant health status, on which the earlier approach rested, have not been challenged or questioned by the health system. Therefore, the same philosophies, with the same underlying goals have been repackaged or extended to cover more than just the child bearing age group of women. They now also cover the girl child and the adolescent girl, with the hope that these efforts would bear fruit during the crucial childbearing or motherhood period. International public health experts

and agencies have also floated various package deals like GOBI-FFF and safe Motherhood, which again are narrower and more verticalised versions of the earlier Mother and child Health Services. The other cause for deep concern in India has been the declining sex ratio as is Street revealed by the decennia I census, ever since the turn of the century. Levels of other indicators regarding the health status or women like the Maternal Mortality Rate, levels of anemia and malnutrition etc., are also unacceptably high. This has occured inspite of at least four decades of planned health interventions through an expanding health infrastructure. It has occured even though there has been an overall slow improvement in other health indicators of the population in general. There is therefore, dearly a need for a "rethink" and for evolving new approaches. If new polices, strategies and approaches for the improvement or the health of women in India have to be developed, there is a need to understand and define a new what the health needs of women arc. A few ideas are being raised in this regard. A woman is a person situated in society and her health has to be viewed with an integrated wholistic approach. Several non-medical, societal. Socio-economic-political and cultural factors determine her health status. Using the WHO definition of health itself there is a need to include the physical. Social-emotional and intellectual (mental) and spiritual aspects in understanding the health status and needs of women, when evolving health strategies.

When considering physical health, while her conditions access to health services etc. which are thus reproductive system does influence the functioning of indirectly indicated. The sex specific crude death her body and may be a cause for illhealth, women also rates, sex differentials in Infant Mortality Rates, suffer from morbidity and mortality resulting from Maternal Mortality Rates etc. based on studies of disease in any or all the other systems as well. sample population, are well known indicators of Availability and access to good basic and deaths occuring in different groups of the population. comprehensive health services is therefore essential. Life expectancy at birth or at 1 year also reflects the overall health status and conditions under which There is also a close interplay of all the aspects people/women live. The sex ratio is the number of of health mentioned in the WHO definition. The most women per 1000 men and its trend over the decades crucial fact cutting across class lines is that being a speaks volumes to us or the situation of women in the ‘woman’ straightaway category all women to an country. If one could disaggregate and study these inferior unimportant social status in India. When rates by geographical location, and by class and caste considering the sizeable proportion of women (30-40 enormous differences would be revealed, it is % by official estimates) living below the poverty line, necessary to do this if the health status of those in their health and social status is far worse than others greatest has to be recognized/assessed and also for the and would derive from the following scenario. Being monitoring and evaluation of health and related poor they are also susceptible to a variety of physical strategies that are employed, inspite of certain sicknesses, most of which are preventable. They limitations and cautions that are necessary when undertake exhausting work at home in poor undertaking such an exercise comparison of rates environmental conditions. At work too they have the between districts states. South Asian countries dirtiest most tiring jobs with inadequate remuneration Developing countries and developed countries are and rest. They have lower level so literacy and less useful. access to existing health services. Life in this situation also makes fora poor self-image low self esteem lack Information regarding, sickness (morbidity rates) of self confidence and to unrecognized emotional among women are harder to come by at the national or problems during the several episodes of life crises that state level. This is even more true of community based are experienced. epidemiological data. Studies by the National Nutrition Monitoring Bureau showed no evidence of Viewing women, particularly poor women improved height and weight among girls from 1955 to surviving in an inhuman situation primarily in terms 1979. One third or the babies born are low birth of their reproductive function, therefore does them no weight (less than 2.5 Kg.) which results from pour justice, and not surprisingly , does not meet the targets maternal nutrition. Other studies reveal inadequate set by the health system, and even much less caters to calorie and micronutrient intake. their total health care needs. There is evidence in fact that this targeted approach with the indiscriminate and Community based studies by Rani Bang, et al. have unscientific use of numerous family planning found that the prevalence of Reproductive Tract procedures, are an added iatrogenic cause of ill-health Infections (RTI) are very high, Contraceptive use in for women living under these adverse circumstances. the presence of RTIs have been found to aggravate the problem. Occupational or workplace related health Therefore it is imperative that the health needs of problems of women in the tobacco industry, among tea women should be viewed from a broader and more picket and in a host of cottage industries in the humane perspective. unorganized sector also reveal high sickness rates. Hazardous effects of pollutants on women during childhood, adolescence, pregnancy and lactation also * Her value as a human person of dignity needs to need to be studied. A study by Sathyamala et al. found be emphasized. This is to be de-linked from that toxic gases at Bhopal adversely affected reproduction or production of any type. This crucial reproductive health and reproductive outcome in aspect is not measurable or quantifiable. Bhopal. We have an indication about the extent of * Her total health needs in the context of her violence against women in Indian Society from the circumstances should be considered. media, and from the experience or group working with * Positive indicators of physical, emotional, women. A few studies regarding mental health intellectual and social health need to be used. indicate a higher proportion of suicides and suicidal * Positive indicators of physical, emotional, attempts among women than among men. intellectual and social health need to be used. * Periods of life crisis in women's life are to be It is necessary to integrate and pool all available recognized. This method can build on the strengths data concerning the different aspects of health of and infrastructure of the MCH approach and extend women to get a composite understanding of the not only to the girl child and adolescent but also to the situation. This needs constant updating and continuing post child bearing period. studies. When putting different studies together, it is also important to keep in mind that there may be differences in concepts and definitions used and in the Indicators to access health/disease methods employed. The assessment or the measurement of the health While accepting that having some indicators, status of women is an important yardstick for us to know where we are in our efforts to promote the however imperfect, are better than none, health health of women. It helps us to make a situation workers/activists have been feeling the need for analysis, to measure the extent of the problem, and indicators that could gauge decision making opportunities and capacities of women, their levels of also the effectiveness of strategies used. participation in health and societal life, their levels of Commonly used health indicators most often give us autonomy, their role in provision of health care in the information about levels-of disease and death among family their levels of knowledge and practice of the population. Loose are a result of general living traditional methods and systems of healing among

others. There is much scope for further work in this. Indicators of health related issues: These include figures regarding levels of literacy (formal. nonformal), income/wages, percentage below the poverty line, employment/unemployment, participation in different sectors of the workforce, purchasing capacity housing, food intake, access to water and safe sanitation. These are also crucial factors that impinge on the health of women. Health care indicators: These would indicate access to primary and secondary health care, distance to nearest health facility etc. Utilization (of services) rates and coverage rates are for some services e.g. immunization coverage and immunization status. The proportion of births attended by trained personnel is also an useful index.

When using any health indicators, it is important to keeping mind the methods or data collection and quality of data before deriving conclusions from them. Questions should be raised regarding methods of sampling used viz. are they representative of the population. Are findings from one or two studies conducted in relatively defined geographical areas being extrapolated or generalised to the entire population. There is thus a need fora critical appraisal of any data and rates. It is also important to keeping mind that the health situation in the community is dynamic and changes continuosly as a result of several factors. It may also not he all that easy to draw cause and effect conclusions from a particular health intervention and possible health outcome. The role or other (actors that could cause a bias or be confounding will have to be considered. However, inspite or all the above there is scope to build further and not just abandon what we already have. As we look at new perspectives emerging in health, new indicators will need to be developed.

The need for measurement and assessment of effectiveness and utilization of services is illustrated by a few examples. A study of in and out-patient References records showed that for every 3 men who utilised medical services only one woman did so. Male 1. M. Hammond and J. Gear, 1986, Measuring staffing of facilities was a deterrent to utilization (Ref. Health status of Indian people, 1988, Foundation (or community health Workshop 1, Oxford University Press. Research in Community Health). 2. NIMFIT (National immunization Mission Another recent study by the Paediatric Department Feedback Information and Technical Update), 1992, of Maulana Azad Medical College in 150 slums, Ministry of Health & Family Welfare, GOI, New covering 22,181 households in the capital city or Delhi, Issue No. 24 June 1992. 3. Puri R.K. and Sachadev H.P.S., 1992, MCH Delhi found the following: 45% mothers did not avail of antenatal services; 16% had the optimal four services in Delhi slums, NFI Bulletin, April 1992, Vol. antenatal checks; 12% Smoked even during 13, No. 2. 4. Shatrugna V. (undated), Women and Health, pregnancy; Awareness regarding health, nutrition, and awareness or possible complications during pregnancy current Information Series 2, Research Uniton was poor; 51% received iron and folic acid tablets; Women's Studies, SNDT Women's University. ' 5. World Bank, 1991, Gender and Poverty in India-A 63% mothers were immunized with tetanus toxide; 82% delivered at home, untrained birth attendants World Bank country study, The World Bank, 1818 H street, N.W., Washington D.C., 20433,' U.S.A. conducted most deliveries. (Source: 3)

Population Control Policy Roopashree Sinha 'POPULATION policy implies the government's role in motivating individuals and couples to decide the number and spacing of their children, providing information, education and means to do so. India has had an official population policy for reducing fertility since 1951 and has become one of the largest and most complex in the world, spending enormous amount of money and utilising vast aggregate of human power.

targets and incentives were introduced. The programme was implemented not only by health department but also by other government departments. In the seventies the message was reinforced through massive mass communication campaigns and through camps for sterilisations. In the eighties there was a barrage of new advertisements telling women that it was medically risky to be an adolescent and multifarious mother. The approach had changed to "this is what is good for you". Lately the government In the first five year plan period it spent Rs 6.5 in a renewed frenzy is using secretive and deceptive million and in the 7th five year plan the allocation methods for introducing, contraceptives like Depo reached Rs 32,560 million. During the first few years, Provera and Norplan- T. the Indian government's approach was to provide fami1.y planning services and supplies through the The objective set in 19.73 was to reduce birth rate in existing and newly established government clinics and India to 25 births per 1000 population. centres. It maintained the voluntary nature of the programme by emphasising the cafeteria approach The Indian government generally evaluates the and involving various voluntary organisations. since family welfare programme on the basis of quantifiable mid-sixties, programme was made time bound and factors. They are the number of births averted,

declining crude birth rate (CBR), increased couple workers have to pursue a personal target. A majority of protection rate (CPR) and reduction in the total medical and paramedical staff perceive this as a major task and therefore neglect the curative and preventive fertility rate (TFR) of Indian population. programmes. The medical bureaucracy supplies Thus the Ministry of Health and Family Welfare contraceptives more readily while ignoring the demand estimates that more than 100 million births have been for basic medicine several times. averted between 1956 and \ 1990. About 7, 26, 00,000 The approach of target selling and achievement sterilisations have been performed from 1956 to 1989. sees people more as numbers than human beings. The The birth rate has declined from 41.7 per 1000 during 1951-61 to 31.5 per 1000 in 1988. the couple coercion of incentives and social pressures applied by protection rate has increased from 23.6% in 1976-77 government functionaries are onslaughts on the very to 34.9% in 1986-87. The total fertility rate has basic moral right of a family to decide the number and spacing of children. decreased from 5.9 in 1960 to 4.3 in 1988. Four decades of programme implementation has generated meaningless statistics. Foe example, for the period 1980-85 the ministry determined that it would protect one million women with oral pills, and later reported that it had succeeded in motivating 1.29 million! In 1983-84 Maharashtra government claimed 1, 80,000 IUD insertions of which 25% were found fictitious by Indian Express investigations. The sample checks done by the Evaluation and Intelligence division and State Regional Teams of the Ministry of Health and Family Welfare (1981-82) provided a breakup of The total number of births averted seems cases not contactable for various reasons, such as, (a) impressive but it has been at an increasing cost. No such person being in the area - 0.5%, (b) Couple who had left the place permanently - 3.3%, (c) Couple Dandekar1 states that the cost per birth averted has who had left the place temporarily -7.4%, (d) Acceptors increased from Rs 285.63 in 1980-81 to Rs 590.87 in who had died - 0.1% (e) Couples who could not be 1985-86. This is more than double and is more than contacted due to wrong addresses - 5.2%, (I) Other they were can be justified by increase in prices. He also found reasons - 1.3%. The report also stated that 3 that in the aggregate, the expenditure on family 0.25% ineligible cases, a very high figure. planning per eligible couple is Rs 37.07 (1985-86)2 The medical bureaucracy in India has given Moreover an analysis of acceptors of family planning undue and illogical stress on tubectomies. The programme shows the Achilles heel of the percentage of vasectomies to total sterilisation government's programme. Somnath Roy pointed out operations was only 8.2% in 1989. The medical staff that 75% of the acceptors of vasectomy and 80% of has a grossly callous attitude towards women acceptors of tubectomy had already had three or more living children. It was calculated that the mean age of acceptors. In October 1990, The Independent carried an wives of vasectomy acceptor was between 33 and 34 article which stated that there were 363 deaths due to faulty tubectomy operations. It also revealed that in the years. last three years, the Ministry had reported at least 1100 Parentage of couples protected with the help of persons as victims of sterilisations. The national sterilisations is as high as 29%, that of IUD is 5.2% average of 1.7 deaths per 10,000 sterilisation operations and that of CC/OP is 5.7%. Moreover 43% of the total is far above acceptable death rate as per WHO which is IUD acceptors already had 3 or more 'children and 4 only 21.2% had accepted IUD after one child. The one per lakh sterilisations. usage of CC/OP in rural areas is very erratic, generally only a few weeks in a year. India's efforts in The mass sterilisation camps are places where the reduction in TFR have not yielded as positive result as minimum of safety measures and scientific procedures DPR Korea, Indonesia and Sri Lanka. Between 1960 are sacrificed in the interest of speed and target and 1988, the TFR for Indonesia decreased from 5.5 to 3.2; those for Korea and Sri Lanka decreased from achievement. Against the mandatory procedure that only physically fit women should be operated upon, the 5.7 to 3.6 and 5.3 to 2.6 respectively. clinical check up is casual and quick; the comfort and The inhuman strategies adopted by the privacy of the woman is unimportant; absence of government to achieve whatever it claims to achieve proper equipment especially in laparoscopic speaks of the real failure of a democratic government. sterilisations when the lapse can cause severe injury or The government in Delhi sets the targets for the number of couples to be protected from conception. death and finally, the complete absence of after care These targets are dispensed to states irrespective of its review. Rarely is it realised that tubal ligation fail in 1 size and socio cultural background. The state % of the women, who become pregnant in spite of the administration then sends it down the line to various sterilisation, 15-20% of failed ligations result in ectopic district,' town and village health centres where health pregnancies, which carry 30% fatality rate. However, a deeper analysis of these figures shows the weaknesses even in the measurable achievements. Although the CPR has gradually increased, it has not achieved a commensurate lower birth rate. The CPR has increased from 23.6% (in 1976-77) to 34.9% (in 1986') the CBR has stagnated between 33.7 and 33.9 for a longtime (1976-85) beforedroppingdownt031.5 in 1988. However this is still a far cry from the expected 25 births per 1000.

years in Kerala. Demographer Mani Nag points out In India, contraceptives have been used only as part of that Kerala's public distribution of food which covers national population reduction strategy and not to 9-'of/ts population and which represents 15% to increase the reproduction options for women. More 16%ofKerala's total calories intake and about 19% to and more women are likely to be uninformed 20% of its protein intake is a very positive factor.6 participants for untested and dangerous contraceptives like Depo Provera and Norplan-T, due to government's All this goes to show that economic development in aggressive population control programme. All this the conventional sense is not sufficient to make-an proves that the family welfare programme has been a impact. The best assurance for a small family come failure in more ways than one. egalitarian distribution of social justice and transforming social and economic institutions. The Indian government in its enthusiasm for implementation or family planning programme has One therefore wonders at the government's more or less ignored the social and economic factors blinkered approach of stressing family planning like unequal land distribution, unemployment, lack of unmindful of other socioeconomic factors. Like most resources, poor health and high infant mortality rates, governments the Indian government also believes that importance of having male children, lack of education, there is a higher return from the FP than almost any especial I y for women, the low status of women, early other government programme. The U.S. study shows marriages, lack of decision making in women's hands, that for every public dollar spent on family planning that affect family decisions regarding the size of the service, the government saves almost two dollars in family. At the macro level, the policy makers the next year alone on unneeded maternal and child mistakenly assume that children are an economic health. In the same vein the UNFPA report pointed out burden to families. that by averting 106 million births in India, there has been a saving of $742 billion. The unaccounted costs The developed nations have had long histories of to the environment, to development prospect generally intermediate processes and demographic transitions to are much higher. reach the point of more or less stagnant birth rate today. These processes are stimulation to economic The assumption of the UNFPA is that the child will activity, improvement in educational, economic and be economically unproductive during his/her life. This social levels, increase in average age at marriage, and is hardly true. Using conservative estimates, each facilitation of contraceptive practices. There are also Indian male inhabitant makes a net economic lessons learnt from Cuba, Sri Lanka and Kerala. These contribution of about $ 700 during his lifetime.7 experiences show that income and land distribution, employment opportunities and social security, educa- The assumption that children are an economic tion, improvements in the position of women, burden to families’ leads to further postulation that including a later age of marriage and accessible health population growth causes hunger and poverty, care and family planning services are the important environmental degradation and economic stagnation. factors that can bring about demographic transition. Its veiled implication is that the poor and marginalised Let us take a look at a few states within India: 5 breed too much and they demand more than they produce, and are thus ushering in a doomsday for the Maharashtra still has a high incidence of early world. marriages, an average of 18.7 years, much lower than 20 years in Tamil Nadu, 21 years in Punjab and 21.8 The question asked of India is similar to that asked of other developing countries. "Can India's land India Punjab Maharashtra Kerala S. No Heads support such a large and growing population, whether CMIE Index for 1. it has the carrying capacity?" An F AO study (1983) Economic pointed out that if India ran keep to its irrigation Development 100 213 119 105 targets, manage its crop lands, forest and grazing (1987) lands, soil and water properly, it can feed a population Sex Ratio 2. (Females of upto 2621 million, two and half times the projected per 1000 Males, population by that year. 929 888 936 1040 3. 4. 5. 6.

7.

8. 9. 10. 11. 12.

1991) % of Literacy 52 (1991) % of female Literacy ('91) 39 Annual Birth Rate (1987 -90) 29.9 Infant Mortality Rate (1990) 80 Births attended by qualified 41,200 personnel institutions (1988) TT2 (1986-87) 45.6 DPT2 (1986-87) 56.5 OPV3 (1986-87) 48.6 BCG (1986-87) 47.9 Measles (198616.7 87)

57

63

91

50

51

87

27.5'

19

55

58

17

80,600

46,300

91,100

27.6

56.2 70.0 71.2 69.0

93.7 92.2 92.0 98.8

104.0 82.9 91.t 99.2

18.4

23.7

32.0

Anyway, any talk of carrying capacity must clearly be marked with great caution. The pilot study of population-environment relationship in the eastern islands undertaken by UNESCO and UNFPA showed how Fiji with declining Copra industry and increasing poverty and emigration could, by converting to Taro (a root crop) production support a population five times its present sizes.8 The inference leads us to more thinking on alternative development strategies. Generally anecdotal data is presented to show environmental degradation in countries that have high rates of population growth. But most of them do not show definitive necessary relation between environmental quality and rapid population growth.9

Moreover an equal number of counter anecdotes could show that ecological degradation has been a result of the criminal waste of resources by the developed world. It is a known fact that 25% of the world's population uses 80% of the world's resources. It is said that it would take 500 people in the third world countries to do the same damage to the environment as is done by one person in a developed country.10 Densely populated Europe is heavily supported by the production of a range of items outside European countries, especially in the developing countries. The UN has never dared to estimate how much land in the developing world and how the quality of this land has been changing overtime due to newer pressure from developed nations. All this is to point out the fact that the present exploitative war making, arms producing world system, the disproportionate consumption patterns of the affluent, flooding of international market with unessential, profitable items, protectionism in international market, that force structural adjustments in national economic policies for acquiring economic aid. Thus we have national strategies that continue to bring about rapid, unequal, lopsided and inappropriate development, displacement of marginal and small farmers and their indigenous eco-systems, that are unmindful of what the poor and the women need but which toe the line alright. The structural adjustments made in the union budget for 1992-3 showed reduction in programmes like poverty alleviation, public distribution system, labour security, education, self employment, health and water programmes but with an increase in family planning budget.11 All this is not to shift the blame from developing countries to developed countries but to place the national population programmes in the perspective of global overpopulation theories. It is also to bring us to some crucial questions: (1)

Given the globalisation of population policies and our experience for the past 42 years, can we believe to have a family welfare programme that can be pro-people and pro-women in future?

(2) Is it possible to have protection of basic rights of women without a family welfare programme? (3) Do we need a family welfare programme or do we need an alternative eco-development policy?

Notes and References 1. VHAI, "Status of India's Health" (1992) pg 205, Table 5. 2. Ibid, pg 208. 3. Ibid, pg 209. 4. S.G. Kabra and Harsh Sethi, "Whose sacrifice, Whose survival". 5. Foundation for Research in Community Health, "State of Health care in Maharashtra", pg 24-26. 6. India Environment, 1984-85, "The Kerala Factor", pg 161. 7. Fernandes Orrego, Population Research Institute Review, March-April, 1992, Vol 2, No 2, pg 1-2. . 8. India Environment, 1984-85, pg 157-59. 9. Barry Commoner, "Rapid population growth and environmental stress." 10 Kristin-Shrader Frechette, "Ethics and the Environment", World Health Forum, Vol 2, 1991. 11 Research Centre for Women's Studies Newsletter; Monsoon issue, 1992, No 2, pg 8-9. NOTICE INVITATION TO ALL MFC MEMIJERS TO ATTEND THE XX ANNUAL GENERAL BODY MEEI1NG ATSEWAGRAM, WARDHA ON THE 12th OF JANUARY, 1994 Time : Executive committee meeting: 9.00 a.m. to 12.00 noon. General body meeting: 1.00 p.m. to 5.30 p.m. Date : Wednesday, 121h of January, 1994 Venue : Yatri Niwas, Sewagram, Wardha Agenda for the General body meeting: 1. Financial matters: Approval of the audited accounts for the year 2. MFC bulletin: Printing and editorship of the bulletin 3. Reporting of the issue based cells of MFC 4. Responsibilities regarding the annual meet on reproductive health 5. Election of the new convenor 6. Election of the new executive committee 7: Convenor's reporting 8. Any other matter with the permission of the chair

TENTATIVE SCHEDULE FOR ANNUAL MEET: JANUARY 13-15,1994

First day Introductions 10 a.m. 10 10.30 a.m. Introducing theme 10.30 a.m. to 11.00 a.m. First session

Social construction of Reproduction Parallel group discussion on same topic

Second day First session

Plenary: .

Parallel group discussions on 1. Contraception 11.00 a.m. 2. Abortion 3. R.T.I.s & S.T.D.s to 4.1nfertility 5. Sexuality 1.00 p.m. 6. Menopause & I-I.R.T.

First session reporting 2.00 -3.00 p.m. Group discussions on 1. Population policy & Demographic methods 3.00 -5.00 p.m. 2. MCII 3. FP Programme 4. Health care system

Third day ---------------------

Plenary

Group reporting & concluding session

Medico Friends Circle Bulletin These are some of the background papers of the 1993 annual met on Reproductive Health Editors Coordination Committee for the Annual Meet Typeset and Printed by: Noble Enterprise Please not that the previous issue date has been printed as May – October 1993, whereas it should read May – July 1993. Subscription rates

Annual

Life

Individual

30

300

Institutional Asia (U$ dollars) Other Countries

50 6 11

500 75 125

Inland (Rs)

Please add Rs. 5 for outstation cheques. Cheques/M.O. are to be sent in favour of MFC directed to Dr. Anant Phadke, 50, L.I.C., Quarters, University Road, Pune 411016. He may also be conducted for subscription/circulation enquiries.


