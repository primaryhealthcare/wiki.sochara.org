---
title: "Dear Friend"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Dear Friend from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC138.doc](http://www.mfcindia.org/mfcpdfs/MFC138.doc)*

Title: Dear Friend

Authors: Morley David

﻿


138

medico friend circle bulletin


MARCH 1988

Report of the XIV Annual Meet of the MFC on Child Health

20-21 January 1988, Jaipur, Rajasthan

The theme for the annual meet had been chosen keeping in mind the current emphasis on child survival as the most revolutionary concept in child health. As several participants had not received the background papers in time, the ses­sions started with the summarizing of the back­ground papers. The discussions that followed in the small groups was facilitated by the listing out of questions that arose in the context of the information that was presented. The four major areas that were identified for discussions were:

1. Acute respiratory infections; 2. Immunization; 3. Survival of the girl child; 4. Nutrition and child health.

Following is a brief report of the discussions that took place in the 1 t days of the mfc meet. The afternoon session of the second day was spent on discussing the changing emphasis from child health to child survival. The meet ended with the evaluation by the participants. Acute	Respiratory	Infections	(ARI) Among childhood diseases, ARI forms the second most common cause of deaths in children. Esti­mates of mortality range from 15 % to 25 % of the total childhood deaths. Studies have shown that 20-40 % of out patient and 12-35 % of childhood hospital admissions are' due to ARI. The background paper prepared by Abhay

Bang highlighted the problem and the strategies that are being proposed to deal with them. Studies have shown that the frequency of attacks of ARI per child per year is similar in the deve­loped and developing countries. Within deve­loping countries, frequency is more in urban areas as compared to rural areas. This would suggest that socio-economic improvement may not be a major determinant of the frequency of ARls but overcrowding is certainly an important factor as the organisms cannot survive in the absence of a large population in constant interaction with each other. However, the severity of the infec­tion and the resultant mortality are significantly high in the under-nourished children. For ins­tance, in a study carried out in Manilla, the case fatality ratio in well-nourished children was 6/1 000 episodes whereas in the severely under-nourished children it was 77/1000. Smoke (fuel and tobacco), vitamin A deficiency are other factors influencing incidence and mortality rates.

The control of ARI through a single vaccine is not possible because of the large variety of causa­tive organisms involved although it is estimated that 25% of the total mortality due to ARI can be prevented through the vaccines available today (against	diphtheria,	whooping	cough,	tuber­culosis and measles). Based on the fact that majority of the acute lower respiratory infections are caused by bacteria and hence amenable to treatment with antibiotics, and based on the Narangwal experience that showed a dramatic reduction in mortality when these children were treated with injection penicillin by Family Health Workers, a secondary prevention approach­ has Management Approach has been formulated.




According to the guidelines, a respiratory rate of 50jminute is the criterion chosen for identifying children who need antibiotics. Currently pilot studies are being carried out in 4 centres in India to assess the feasibility of launching a nation wide programme of ARI control through the case management approach. Question was raised about the sensitivity of the indicator recommended i.e., respiratory rate of 50jminute. According to the background paper, this rate was likely to give the lowest number of false positive and false negatives. Some partici­pants however felt that the 72% (true positive) was too Iowa figure to justify its use as a diag­nostic criterion. It was also noted that if one increases the sensitivity of a test, its specificity may not remain and therefore a balance has to be achieved and the recommended criterion of the respiratory rate along with history given by the mother (difficulty in breathing) should be suffi­ciently sensitive. A suggestion that field workers be given a stethoscope did not find" favour as it would not improve diagnostic possibilities. The case management approach raised two major areas of concern. One was related to the development of high levels of antibiotic resistance in the community and the other, the possibility of hypersensitivity reactions to the antibiotic recommended (penicillin). It was agreed that antibiotic resistance would definitely emerge as an outcome of its widespread use, but it was contended that in any case resistance is going to occur in about 10-20 years because of the widespread misuse of antibiotics by private prac­titioners, so at least till then why not let all sections of the society get its benefit! To the question of sensitivity reactions it was stated that in children anaphylactic reaction to penicillin is very rare and since some felt that intra-dermal test was quite useless it was well within limits to accept the risk of 1 in 2.5 lakh without the test dose. This question was not resolved to everyone's satisfaction. The question of referral was then considered. It was suggested that in the case of recurrent symptoms and wheezing, referrals may be required and that there was a need for an x-ray unit at the referral level. There was some dissent on this. The etiological picture in ARI is different bet­ween the developed countries and the developing ones.  In developed countries, viruses and my­coplasma were the major organisms whereas in the developing countries 60% of the cases were bacterial. It was suggested that climatic condi­tions could be an additional factor but it was pointed out that the Native American Indians living in Arizona in a relatively lower temperature, also showed organisms similar to that in developing countries.











 The manner in which UIP is being promoted, as an approach in isolation, as a panacea for childhood morbidity is not acceptable. UIP cannot be a substitute for socio-economic improvement. Individual experiences shared in the group highlighted the fact that UIP is accep­table only as an integral part of primary health care programmes. It is not true that vested in­terests are not involved in the planning and pro­motion of UIP. There could be international and national political influence in the planning and selection of strategies such as the UIP. Planning for an immunization programme is more likely to be effective if it is location specific rather than uniform nation wide. Several expe­riences were shared regarding the different stra­tegies each had adopted. The strength of the measles immunization programme implemented in the city of Jaipur was the large number of private practitioners that were enrolled for the effort. A ~ingle day was declared immunization day and prior publicity was given through the media. About 30,000 children were vaccinated that day in the age group of 0-5 years. The immunization programme carried out in the 20 villages around Wardha as part of the primary health care programme being carried out by the medical college had 90-95% coverage with measles, 70-75% with DPT and polio. The dropout rate between the doses was minimal and most of it occurred because the services could not be ren­dered at a particular time. I n the subsequent years, the drop out rates diminished and for the past three years, there is not a single case of measles or polio. If planning and execution is done in an efficient manner, targets can be reached effectively. Although at the macro level it is well established that in India females at ages from 0 to well into reproductive period suffer from higher risks of mortality as compared to males, efforts to understand exactly how discrimination takes place seem to pinpoint that medical intervention at the time of illness could be the crucial factor in deter­mining mortality rates. From her own experience and on the basis of other studies carried out in India, it appears that there is no hard evidence to indicate any discrimination on a regular basis, discrimination exists only in terms of special foods such as butter, sweets, ghee etc. Kamala Jayarao had analysed the data of the National Nutrition Monitoring Bureau (NNMB) which showed that adult females have less calorie deficit than adult­ males i.e., the food intake of women is better than that in men or that the energy intakes are similar. Further, in the underfive age group there were no sex differences although the under­five girls had a greater body deficit. This diffe­rence was lost when girls grew up and body deficit in women was less than in men. The conclusion reached was that women were not as underweight as men. Her paper also presented evidence from an NIN study which followed up a large number of children whose weights and heights were recorded when they were 1-5 years of age and later. The results showed that sur­prisingly in girls with severe malnutrition there was an attempt to catch up (the increase in height of girls who suffered from severe grades of malnutri­tion, the increase was much greater than not only those who were in mild and moderate grades but even than in American girls.) However the in­crements were not sufficient to allow them to attain a normal height. The reasons for the better performance of girls in late childhood and adoles­cence were not clear. Consensus could not be reached on these questions: can 2 live vaccines be given at the same time and whether 3 or 5 doses be given in polio immunization. Since the UIP is already amongst us, what should be our reaction to it could not be discussed due to paucity of time.

Survival of the girl child: Leela Visaria's paper "Gender bias in survival and Nutritional status of Children: Review of available Evidence" brought out clearly the paucity of substantiative data to support that girls are discriminated against in terms of food allocation. According to her, the only study that had attempted to measure in detail the quantity of food intake of children was carried out in Matlab (Bangla Desh) which indi­cated food intake was biased in favour of male children.
 
All this naturally led to a very energetic dis­cussion. In neither of the papers there was ade­quate evidence that no discrimination exists in terms of food intake. Yet the impression that was created by both the papers was that no nutritional discrimination exists. Can one really quantify in statistical terms the differences in nutritional intake within a family? The very fact that an outsider (the researcher) is measuring food allocated to the different members within a family would surely introduce a bias. Does a lack of substantiative data mean that discrimination does not exist? The NNMB data came under attack and was questioned for its statistical validity. The data had been obtained for purposes other than estab­lishing discrimination and it was agreed that the data should be checked for its biases. It was also pointed out that earlier when Harvard standards were used, which is a combination for girls and boys, girls tended to be more under-nourished whereas when different standards were used for boys and girl’s boys appear more under-nourished. The 1960-70 studies from ICMR, NIN had shown nutritional deficits in girls based on anthropometric indices. Therefore, it seemed unlikely that the situation had changed so much since the ICMR's findings that there was no longer a major nutri­tional deficit experienced by girls. The only conclusion that the group could come to was that there were no adequate studies to substantiate the forms that discrimination takes although they are observed as widespread phenomenon.

The group also contended that caution needs to be exercised in accepting 'access to health services' as the determinant factor in mortality rates among the girls. Firstly, until recently hardly any rural population had access to health services and this would have affected children of both sexes more or less uniformly. Secondly, we need to define what we mean by access to health services. There may be a PHC, subcentre, private practitioner or local healer easily 'accessible' to the population, but this in no way means access to effective health services. The mere act of taking a child to any of these services in itself does not indicate that they are effective enough to prevent mortality. It was also pointed out that the higher mortality rates in girls may be due to an interaction of several factors rather than the result of a single factor and that we may not be aware of these factors at this point of time because of the inade­quate attention paid to the subject. The discussion then veered off into the question of how important is it to understand the factors responsible for the higher mortality and morbidity rates in the female population in order to intervene positively? Are we all not aware of the fact that women are dying and suffering as a result of the patriarchal attitudes and values in the society? I n that case the positive intervention would be to attempt to change these attitudes and values and for this purpose the understanding of the forms of discrimination is largely irrelevant. This brought us to the same (old!) debate that has continued (and one hopes will continue) since the inception of MFC: To reduce a 'disease' of socio-economic-political origin what is the 'level' at which a socially cons­cious medico intervene?

Nutrition and Child health: "At the risk of stating the obvious, it must be emphasised that growth monitoring is no more than a diagnostic and (possibly) educational tool	 ……..Growth monitoring by itself, however efficiently executed, can not bring about nutritional improvement; it must always be followed by action on the part of the health worker and the mother-the action consisting of appropriate and necessary improve­ments in child-feeding and child-rearing practices.


4

	
  
	

Can mere health education improve the nutri­tional status in a situation where access to resources is extremely restricted because of poverty. The group also criticised the recent trends in 'harnessing' the adolescent girls for child rearing since they themselves are in need of opportu­nities for better growth.

Low birthweight continues to be a major problem. There seems to be a direct relationship between stunting of the mothers and the occur­rence of low birth weights in the offspring. Studies have found that the pre-pregnancy nutri­tional status of mothers an important determinant of low birth weights. Medical and nutritional intervention during the time of pregnancy seems to have little impact in reducing the incidence of low birth weights.

The major issue that faced the group was the difference in the concept of child health and child survival. Only 2-4% of the under-five children suffer from severe grades of under-nutrition and some	of	them	succeed	in	surviving.	About 60% of children are mildly or moderately affected by under-nutrition, and most of them survive to reach adult hood. What is the 'quality of life' of these survivors? Kamala Jayarao’s paper "Malnutrition and child survival" attempted to answer the question. Studies have shown that even severe under-nutrition does not damage the capacity of the body to grow during adolescence, particularly the linear growth, but the children cannot make up the handicap suffered during early childhood. A small body size results in a decreased work capacity, and hence the earning capacity, Studies have also shown that physical labour during late childhood in under-nourished children may adversely affect subsequent growth.

Kamala Jayarao in another paper entitled "Health and Nutrition" had compared the IMR and nutritional status of children in Punjab and Kerala. Punjab has a higher IMR as compared to Kerala, However, in the under five age group the incidence of under nutrition among 1-5 year aids is much lower than that in Kerala. The reason could be that the availability of better health services, better road facilities and the high female' literacy in Kerala, enables mothers to seek early treatment for their children thereby reducing IMR, child mortality but because of poverty (per­ cent living below the poverty line in Kerala, is about 47% as compared to the 15% in Punjab food intakes continue to be low and hence the incidence of under-nutrition is higher in Kerala. Child survival cannot be equated with good child nutrition. Therefore programmes that aim at child survival by 'fire-fighting' strategies should not be carried out in isolation but need to be implemented in the context of the improved possi­bility of improved child nutrition irrespective of their degrees of under-nutrition.


The current fixation for mortality indicators like the IMR have arisen out of the fact that these parameters can be managed and manipulated without attempting to change the socio-economic-­political situation. Since the time of Bhore com­mittee we have shifted from a near-comprehensive commitment to improved quality of life to narro­wer and narrower concepts like child survival. Gopalan's paper "Heights of populations : An index of their nutrition and socio-economic deve­lopment" pointed out that infant mortality rates, increase in life expectancy do provide a measure of the overall improvement in health status but do not tell us much about the state of health, nutrition and well being of the survivors. He suggests that height measurements and quantification of height deficits through comparison with an international or national standard and the application of the procedure of Z scores will help us to identify differences in nutritional status as between different regions, populations, groups and social classes in the country and to monitor changes OV6r a period of time.

It must also be remembered that the concept of child survival was postulated on the basis that a policy of population control will not be successful as long as there is no substantial reduction in infant and child mortality. The national and the International planners are not interested in improving the quality of life for the children in the third world countries and attempt to do so is considered a luxury that a poor country can ill afford and is therefore discouraged. This dan­gerous trend must be reversed and the propaganda be exposed for what it is.

Sathyamala

(Background papers of this meet are available from the conveyor’s office for Rs. 10/- per set).

5






Bhopal Studies : A compilation: It was suggested that all the studies conducted by mfc in Bhopal should be compiled, adding a suitable preface and be published as a single volume. Sathyamala agreed to undertake the responsibility of putting if together and getting it printed.

Moreover it was further decided that both the socio epidemiological study and the Reproductive and Menstrual Health study should be rewritten in an article form to be published in a journal like Lancet. This would create a wider impact, beyond the usual mfc readership.

NET EN Booklet : The draft of the booklet has been prepared by Anant RS and is being cir­culated to those who were interested to give critical comments. Meanwhile, Kamala Jayarao had written to him questioning the relevance of the booklet in view of the shift in Govt. emphasis from the injectable to the implantable contracep­tives. However, it was generally felt that the booklet should be published as all the major arguments apply equally well to the implantables also, Secondly, there is a great deal of ignorance about either amongst doctors.

In view of the financial problems, it was decided to explore the possibility of getting publication grant for all the above publications from DST. Anthologies, I, II, III. The General Body expressed its concern over the slow sale of these books which has created a paucity of publication fund. It also discussed ways and means to speed it up.

It was decided to make efforts through personal contacts to get the three anthologies reviewed as a set, in various popular fora. Further, it was decided to push these books through large book­sellers like AH wheeler and Higginbotthoms. Bookshops selling alternate literature should also be contacted.

Organisational Matters :

Election of the EC : Marie D'souza, Ravi Duggal and Mira Sadgopal retired by rotations. Ravi Narayan, Sathyamala and Dhruv were elected in their pace. Narendra Gupta, Ulhas Jajoo, Ashvin Patel and Mira Shiva continue to remain on the EC for the second year.


	­

Discussing the avenues of publishing the booklet, it was suggested that if acceptable to EPW, an article could be published in the journal and the same compose could be used to print booklets. Padma Prakash agreed to explore this possibility. Election of Convenor and change of Organi­sational Office :

Dhruv Mankad, having completed his two year tenure, expressed his desire to be relieved of Convenership. His request was accept.

TB Pamphlet/FP Pamphlet: The final draft would be prepared by Anant before the Core Group Meeting. Similarly, Sathyamala agreed 10 prepare an 'mfc statement' on Family Planning keeping in view the discussions at Kaya, before the Core Group Meeting.

Narendra Gupta was unanimously elected as the new Convenor. With the change in convenor the organisational office of mfc will now shift to :

'PRAYAS', village Devgarh (Deolia) via Pratap­garh, Dt. Chittorgarh, Rajasthan.

Medical Education Anthology: Exceprt for a couple of articles, the anthology is ready for publication. CEO, Bombay has expressed urgency in bringing it out. It has only been held up on our side because all the money available for publication is locked up in the 3 anthologies. Other matters discussed included audited statement of accounts for 1987, budget for 1988 the transfer of bank accounts and auditor.

Convenor. 7








