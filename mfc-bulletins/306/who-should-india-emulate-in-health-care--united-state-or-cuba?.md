---
title: "Who Should India Emulate in Health Care- United State or Cuba?"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Who Should India Emulate in Health Care- United State or Cuba? from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC306.pdf](http://www.mfcindia.org/mfcpdfs/MFC306.pdf)*

Title: Who Should India Emulate in Health Care- United State or Cuba?

Authors: Dr. Padma Balasubramanian

medico 306 friend circle bulletin August-September 2004

Exploring ‘Rights-based Approach’ for Renewal of Public Health - Abhay Shukla1 Among most health activists, there is a broad consensus about the need for strengthening public health, and for greater accountability of the public health system. However, how to practically move towards this goal in the real situation, at local, state and national levels is a matter that needs to be worked out in practice. This is both a question of strategy, and also relates to our broader perspective about processes for social change. I would like to suggest that adopting a ‘Rights-based Approach’, in the form of building initiatives for the ‘Right to Health and Health Care’ should be seriously explored for its potential to increase popular awareness about entitlements in the health sector. Such an approach can put pressure on the public health system to perform better, and to make it accountable. We can use the Rights Framework to also address the issue of quality of care and social regulation of the private medical sector. Such an approach can also have wider social implications and linkages. But along with the possible strengths of such an approach, some of its limitations and pitfalls have also been discussed in this article, with the aim of trying to clarify ideas and to refine strategies for the health movement in the coming period. Our Approach – Right to Health; One Major Strategy – Right to Health Care It need not be reiterated that achieving better health

for any population is dependent not only on quality health care, but equally importantly, on assured food security and nutrition, safe water supply, sanitation, healthy housing, safe occupational and environmental conditions and other conditions necessary for healthy living. Keeping this in mind, it is necessary to define and distinguish between the ‘Right to Health’ and the ‘Right to Health Care’. Let us start by specifying that when we talk of the ‘Right to Health’ we actually mean not the ‘Right to be healthy’ but rather ‘the Right to a variety of facilities and conditions necessary for the realisation of the highest attainable standard of health (ICESCR, General comment 14). Another definition of ‘Right to Health’ would be: ‘The right to the highest attainable standard of health in international human rights law is a claim to a set of social arrangements - norms, institutions, laws, an enabling environment - that can best secure the enjoyment of this right. (WHO) It is clear that achieving a decent standard of health for all would require a range of far reaching social, economic, and environmental and health system changes. There is a need to bring about broad transformations both within and beyond the health care sector, which would ensure an adequate standard of health. So to promote the Right to Health would require action on two related fronts: l Promoting the Right to Underlying Determinants of Health l Promoting the Right to Health Care

1

The author is with CEHAT, Pune and convener of Jan Swasthya Abhiyan. Email: cehatpun@vsnl.com

The first, the Right to Health Determinants (to use a

2 short term) is in effect a Spectrum of Rights, a set of rather Diverse Rights to various services and conditions necessary for health. Many of these services and conditions (such as education or housing or environment) are not particularly amenable to direct actions by actors in the health sector (because of their lack of expertise and mandate), despite their undoubted importance for health. Agencies engaged in the health sector may not be able to deal with most of these issues on their own, but they could highlight the need for better services and conditions, from a health perspective. Thus the role of health sector organisations in addressing such determinants may be to strengthen and substantiate demands, to advocate and support other agencies working directly in these areas, to help bring about relevant improvements. Regarding the second, the Right to Health Care, we would accept that given the gross and unacceptable inequities in access to health care and inadequate state of health services today, we need to work to ensure access to appropriate, rational and good quality Health Care for all. This would involve reorganisation, reorientation and redistribution of health care resources on a societal scale. The responsibility of taking forward the issue of Right to Health Care lies primarily with agencies working in the health sector, though efforts in this direction would surely be supported by a broad spectrum of society. With this understanding, I would argue that the overall approach of health activists could be the ‘Right to Health Approach’. Within this framework, establishing the Right to Health Care appears as one major strategy and an imminent task, to be taken up by organisations in the health sector, within the broader context outlined above. Simultaneously, health activists should seek to link with movements for allied Rights such as Right to food, water, housing, employment – ultimately leading to a widely and strongly felt demand for an alternative system, which could satisfy all these rights more effectively. In the remaining part of this paper, I will focus on the Right to Health Care, to be considered within the framework of the broader understanding of a Right to Health Approach. Rights-based Approach – Attitude Towards Public Health System and Private Medical Sector In this paper, I have not dealt with the detailed content of the ‘Right to Health Care’ since this has been dealt with to some extent in another paper of mine (‘Right to

mfc bulletin/August-September 2004 Health Care – Moving from Idea to Reality’). However, it seems important to make one clarification here. When health care rights are taken up as a form of resistance to weakening of the public health system, naturally the demands would be focussed on the public health system, especially in the initial phase. The framework and demands we raise would relate to strengthening the Public health system, under peoples monitoring. It is from the Public health system that we can demand a set of comprehensive health services, since it runs on taxpayer’s money and is accountable to all of us. There are comparatively clear population based norms, mandated levels of facilities and at least a nominal goal of universal coverage. So as we press for the legal and social right to certain basic health services, the Government would be pressurised to strengthen and reorient the public health system, in order to deliver these services. However the repeated experience is that when we make some general complaints about lack of proper health services, the standard response from Government officials is that ‘our systems are fine, we are giving all the services, and if there is some minor issue we will solve it’. We are handed out some statistics about number of PHCs and immunisation coverage etc., etc. This hides the fact that the public health system is being weakened day by day, and people are often not getting even bare minimum services of adequate quality. So if we are to effectively challenge this situation from a Rights-based Approach and even get legal support for our argument if necessary, we have to document various actual cases where people have been denied required health services from the public health system, and present this as a widespread, serious human rights violation. This building of pressure would of course always be accompanied by the demand for strengthening and accountability of public health services. Notwithstanding the fact that we would sharply point out weaknesses in the public health system, this does not mean that we exonerate the private medical sector, or that we promote privatisation of health services. In fact we do need to assert that citizens have certain rights concerning private medical services; to start with we should demand regulation of the quality and standards of private care, and care according to standard treatment guidelines. Regulation of costs of care is a more complex issue, which may be taken up as a subsequent step. New laws and regulatory mechanisms are required for this (for example in Maharashtra due to prolonged efforts by health activists, along with a favourable attitude of the Government, a modified act to regulate clinical

mfc bulletin/August-September 2004 establishments has been drafted.) We need to think of appropriate strategies to raise the issue of people’s rights concerning the private medical sector in the coming period, and link this with specific demands for regulation. Another related issue concerns irrational or unnecessary health care, including public health interventions that are unjustified from a socioepidemiological perspective (such as the proposed universal Hepatitis-B vaccination). Right to Health Care obviously implies the right to rational health care, and not the promotion of medical consumerism or overuse of medical resources. In fact it would encompass the right to freedom from irrational or unnecessary medical procedures. As we specify the concrete content of the Right to Health Care, we may keep these issues in mind. In short, raising the issue of Right to Health Care would imply as the first step, demanding quality health services from a significantly strengthened and more accountable public health system, combined with effective regulation of the private medical sector. This should subsequently lead to the Right to Health Care becoming a fundamental right, and the operation alisation of a system for Universal access to Health Care over a period of time. The detailed content, mechanisms for operationalising the Right to Health Care and universal access systems are issues that require detailed discussion and working out, which could take place in MFC or broader platforms like Jan Swasthya Abhiyan. Stages in Development of Health Rights Approach The utility of the Rights-based Approach changes with the level and stage of any movement. I would suggest that the movement for health rights might develop through three successive (though overlapping) stages: l

Today, the fight for Health Care Rights is primarily a form of resistance against withdrawal and weakening of public health services. Demanding the Right to Health Care can form the basis for struggles at various levels, against denial of health care and user fees which form a barrier to accessing care. Asserting rights can help people to protest against poor quality of care and to oppose various forms of discrimination related to health services. Along with many others, the SATHI team of CEHAT has been regularly collaborating with people’s organisations to develop such processes, as a part of various initiatives across the country.

3 l

Gradually, the Rights framework should also form the basis for a comprehensive policy critique, exposing neo-liberal health policies. We can demonstrate how these policies are responsible for moving from a welfare State to a market driven health system, and hence are responsible for denial of an entire range of health rights. Asserting the Right to Health Careas an overall approach can become the logical basis for demanding a system of Universal access to health care.

l

Further, we may want to use the Rights approach as the basis for counter-hegemony, challenging the entire dominant conception of the ‘Marketoriented Approach’ to health care. Building counter hegemony is a process by which oppressed groups or classes not only challenge the dominant social framework, but also concretely develop and offer an alternative system to reorganise society. Let us examine this in some more detail below.

Counter Hegemonic Action: Rooted in the Present, Reaching Towards the Future In the era of globalisation-liberalisation, ideas like ‘leave it to the market’, ‘the government cannot be expected to do everything’, ‘the private sector is more efficient’ etc. are emerging as dominant ideas. These ideas, centred on the ‘Market oriented approach’ can be considered as part of the ruling class hegemonic framework, and are accepted even by a large section of the middle class and intelligentsia. To counter this, and to win over an increasingly large section of society to the idea of a different, equitable and just social system, we can take the basis of the ‘Rights-based Approach’, as a form of counter-hegemony. In other words, one way of countering the ideological dominance of the market may be to publicise the need for the establishment of various rights. Counter hegemonic action may consist of asking for changes that are viewed by the majority of people as justified, even mandated by the Constitution, etc., but might not often be implemented in practice. Such demands can gather strong popular support, and shift the onus of non-performance onto the system. Even if such demands are partially fulfilled, the continuous expansion of the sphere of Rights throws up evernewer demands and continues the movement. Such action builds upon a component of people’s present consciousness, namely the widely held belief that certain basic rights are justified, but can reach far into

4 the future, by strengthening the movement for an alternative society. In this context, we can start from entitlements in the existing system, which are universally recognised as being ‘justified’, such as the Right to Life which implies the Right to Health Care. We can ask, why these are not being fulfilled, and can suggest how these rights can be fulfilled if systems are organised in an alternative way. For example, asking for a system for Universal Access to Health Care (along with Health Care being made a fundamental right) is an idea, which would be generally regarded as justified by most ordinary people. We can build a campaign based on such a counterhegemonic demand, which may command broad support, even from a section of Health Careworkers and professionals. Such a campaign may achieve concrete gains for people, and may also expose the system in certain other respects. If certain political forces oppose such a proposition, the real character of such forces standing for the ‘Market-Centred System’ becomes exposed, and people can mobilise for change. However, if the demand is accepted in part, this opens the space for some concrete improvements in people’s lives and at the same time, through monitoring and accountability mechanisms, can give people greater power locally. Hence counter-hegemonic demands can form a bridge between a seemingly hopeless present and a projected, detached ‘ideal’ future that may be viewed by people as a good idea, but unrealistic. Being able to fight for and achieve real changes, here and now, enables people to shake off pessimism (‘nothing will change’) and can provide hope, while opening the way for more far-reaching transformation. Usually, the State would not deny the demand for such a Right directly, but may seek to dilute, water down, appease such demands or even try to satisfy them in a ‘pseudo-progressive’ form by retaining the words but taking out the substance. In such a situation, we should work for appreciable improvements in the form of achieving certain rights, expose illusory programmes, and keep emphasising the larger aim and move towards a progressive expansion of rights. With each step forward, if people become more capable of asking for their rights, of monitoring the system and understanding the need for further changes, then we may consider this to be moving ahead. Some Potential Shortcomings of Rights-Based Approach to Health The Rights-based Approach has certain obvious

mfc bulletin/August-September 2004 pitfalls, which should be recognised and as far as possible avoided, whenever such an approach is adopted. l

If the struggle for health care rights is limited to specific local rights (such as the availability of certain services, medicines, etc.), this may pit people against the local providers (e.g., the PHC doctor or ANM) but may leave the state and national level policy makers and the global actors unscathed. Keeping this in mind, moving from resistance to comprehensive policy critique is important. Continuously pointing out the larger links and generalising the demand for health care rights beyond just local demands would increasingly bring the main decision makers into focus. Also, we may attempt to build bridges with the lower level functionaries in the public health system, and document the obstacles they face in providing services, to avoid targeting them and to raise our ‘gun sights’ towards the real decision makers.

l

A narrow interpretation of the Rights-based Approach may be viewed as a ‘non-political’ approach if confined to a limited interpretation of a single Right (‘All we are saying is give health a chance’). People may interpret it as the demand for specific improvements in one sector, while leaving the overall socio-political system intact. Here again, moving towards a counter-hegemonic process where struggles for various rights converge and strengthen each other, and begin to question the system from various angles, is a process that should be attempted and should emerge over a period of time.

l

As mentioned above, the demand for rights may be partly met by introducing certain reforms, and an attempt may be made to co-opt this demand as ‘good governance’. Even an agency like the World Bank may support certain limited, local ‘rights’ in the form of increasing accountability, checking gross corruption and so on. The coalition which has been built up to demand a certain right may be divided about whether to accept certain partial, at times even tokenistic measures being implemented by the Government. Here again, judiciously accepting genuine implementation of rights, while exposing tokenism and continuously pushing for more extensive changes is a matter of strategy, which needs to be debated and worked out in each particular situation.

mfc bulletin/August-September 2004 Some Strengths of a Rights-Based Approach to Health

l

Despite these potential pitfalls, there are a number of strengths in the Rights-based Approach, which can make it an effective tool for the health movement, to ensure people’s access to health related entitlements. Some of these features are: l l

l

l

•

l

A simple slogan like ‘Right to Health Care’ can be comprehended, at a basic level, by anyone - from an ordinary ‘person in the street’ to a WHO official. The rights language has a strong universal appeal, and can help a much larger mass of people, beyond health experts and activists, to relate to the basic issue and get involved. The rights approach can help us link somewhat complex issues of health policy with a demand that can be taken up by people anywhere, and considerably broaden and strengthen the health movement. The health rights approach empowers individuals, communities and organisations, enabling them to demand in a specific way, particular health services and facilities. Once grasped in its essentials, this approach can be wielded by any person or collective, and becomes a source of strength and bargaining power. The health rights approach focuses on functional outcomes, and measures all policy changes or declarations in terms of what people actually receive in terms of real entitlements. The rights approach can effectively challenge the claims of the health officials that “we have so many health centres, we are spending so much money, we have good policies” etc. by pointing out the violations of health rights as long as they continue to occur. When the idiom of health rights becomes part of the overall discourse, automatically health services become understood as important public goods, to be universally accessible, distinct from commercial goods or services to be purchased in the market. This is an important paradigm shift, helping to push back the dominance of the market approach to health care. Rights lend themselves to expansion and universalisation. Once certain rights become established, they become a precedent for other groups or marginalised sections to demand similar rights. The rights approach naturally strengthens the claims of the most disadvantaged and vulnerable sections of society, and helps us both to challenge discrimination and to ask for attention to the most deprived.

5 Rights once granted cannot be easily reversed. While policies and programmes may be changed by new governments, sometimes leading to weakening of services, once the Right to certain services or facilities is established, it would be very difficult to take away this right. The rights approach talks in terms of obligations and violations, thus placing the responsibility to deliver on the system. The beneficiaries are transformed from ‘supplicants’ to ‘claim holders’. When a right becomes a legal entitlement, any individual or group that has been denied their right can institute legal action, and even a few such actions have a much wider effect, in ensuring that all facilities deliver the services.

Some Pointers in the Rights Approach Finally, given the potential pitfalls and limitations of the Rights approach, we could keep in mind some of these pointers: l

The Right to Health Care would be realised in phases, and perhaps only to certain extent in today’s social system. However, we need to work for a progressive expansion and deepening of this right over time, rather than being content with partial reforms. We should continuously establish linkages with the systemic, structural and policy issues that underlie the violation of the Right to health care. This could pave the way for demanding systemic changes, and not just ‘making the existing system work’.

l

We may adopt the Rights-based Approach to continuously push for improvements, but should try to steer clear of reformism. The difference between reformism and a radical approach, based on popular pressure and pro-people advocacy, is the question of where power, initiative and decision making lies. Since ‘Reforms are changes introduced by the powerful’ (after Chomsky) as a top-down process, what is given from above may never reach below, or it may reach in a much diluted or distorted form, or if given today may be taken back tomorrow. On the other hand, when changes take place due to popular mobilisation and the pressure of public opinion, and where people are aware of their rights and actively demand these, the changes are more likely to be actually implemented, sustained and thoroughgoing. Ultimately, one of the major strengths of the rights approach is that it takes the debate out of the circle of ‘experts’ (even if the debate is between official versus pro-people experts) who may

6

mfc bulletin/August-September 2004 continue to argue about policies, and places ordinary people, who can relate to certain rights and demand them, on the stage. The touchstone of change is no longer who ‘wins’ the debate, but whether the change leads to real improvements in people’s lives by ensuring them certain entitlements, which they can ultimately fight for, even without support from experts.

l

l

In this context, any changes that we seek to bring about should strengthen the power of communities and people’s organisations in being able to demand accountability of health services, and in being able to negotiate for services as a right. In the process of a campaign, even if some representatives of the movement might be involved in lobbying and developing a framework for alternative intervention, the larger mass of activists and people should retain their freedom to ‘criticise from outside’ and to act as a strong pressure group to push for more thorough changes and to ensure effective implementation. Finally, while demanding health rights, we should remain aware of the context of our dependent capitalist system, in the larger setting of globalisation – liberalisation processes. Over a decade of virtually untrammelled liberalisation has pushed the social sector in this country to the wall. Yet paradoxically, the very weakening of the social sector has raised the awareness of social sector rights, since ‘when policies weaken, the demand for rights gets strengthened’. And fortunately, the ordinary people of India are once again teaching their rulers a lesson – that the needs of common people cannot be indefinitely ignored. Through many events large and small, the apparently invincible tide of liberalisation may be turning, even if only a little. Against the mighty Goliath of globalisation, David may be standing up at last.

And so the way ahead is difficult, but the outlines can be seen - the struggle for the Right to Health and Health care has to be strengthened, and linked up with the struggles for various other basic rights. The struggle for health rights would develop as part of a spectrum of movements for various rights, which together point the way towards the goal of social transformation. Because, despite the proclamations of certain scholars, there is no ‘End of History’. Though we have many more lessons to learn, and many more struggles to wage, we can look forward to a time when history will be made once again.

Testimony

Excessive Attention as Denial of Right to Health Care Parul is an arts student and also works for an NGO in Baroda. She is not married but lives in a basti in Rampura with her parents and siblings, some of whom are married. Whenever anyone from the basti is not feeling well, they have to visit the dispensary located in Rampura. Once when Parul had cough and cold, she went for a checkup. Dr. Hitesh, the doctor in charge, made her lie down on the examining table and examined her throat, chest and stomach and pelvic region. Parul was puzzled as to why this thorough examination for a minor ailment like cough and cold, but she did not comment. Again, after some days, she approached the doctor for obtaining a health certificate for a nursing course. She was accompanied by her mother but the mother was made to sit outside the room. Again the doctor made her lie on the examination table and examined her thoroughly. He then told her that he would now have to check her internally. She was scared and she lied that she was having her menses and escaped the situation because she felt his touch was not a professional one- why was an internal examination required? The other women and people from Parul’s area and the nearby bastis, go to the same dispensary for treatment. The women have similar experience with the doctor. They go to the dispensary because the medicines that he gives are effective. They are also scared to voice their feelings so this matter is only discussed among women when they sit chatting in groups. In another incident of a similar kind, Bijal, a resident of nearby basti, had complaints of cold and cough and went to the Rampura Municipal Dispensary on June 9, 2004. The doctor there, Dr. Hitesh, made her lie down and felt her feet, legs, abdomen, chest, etc., on the pretense of examining her. Bijal felt this was unwarranted. At the time of this interview, she and many other women in the basti complained that this doctor behaves like this with all women and therefore they hesitate to go for treatment to this Municipal Dispensary. This kind of indecent behaviour of a male doctor with female patients, in our opinion constitutes sexual harassment and must be seen as a denial of right to health care.

mfc bulletin/August-September 2004

7

Management of Severe Scorpion Sting without use of Scorpion Anti-Venom - Dr. H.S.Bawaskar, MD1 Of the 1200 different scorpion species flourishing all over the world, 20 of them are poisonous. Mesobuthus tamulus (an Indian red scorpion) is most lethal among all poisonous scorpions. Mesobuthus scorpion flourishes all over western Maharashtra, Pondicherry, Andhra Pradesh, Saurashtra, Karnataka, and Tamil Nadu. Fatality rates of 30-40% due to scorpion sting have been reported from Mahad region (1,2), Pondicherry (3), Andhra Pradesh (4), Tamil Naidu (5) and Karnataka. Various treatment regimens that included lytic cocktail, insulin glucose regimen and intensive care management did not help reduce the fatality in above areas. In the year 1976, I joined at primary health center (PHC) Birwadi, in Raigad (then Kolaba) district in Maharashtra state, wherefrom deaths due to scorpion sting were reported. I studied the clinical signs and symptoms in detail in these victims. I was the first to report that vomiting, profuse sweating and priapism precedes the severe cardiovascular manifestations due to scorpion sting (6). These cases subsequently developed hypertension, abnormally low blood pressure (hypotension), rapid heartbeats (tachycardia), cold extremities and refractory pulmonary oedema. I tried various remedies to overcome the pulmonary oedema such as rotating tourniquets to alleviate the pre-load to heart (1), decongestive treatment with chlorpromazine and beta blocker, a conventional line of treatment but there was no improvement in mortality (7). I accompanied patients to civil hospital and tertiary care hospitals at Mumbai and Pune, where each patient was investigated in detail. Their electrocardiographs and blood chemistry showed myocardial hypoxic injury pattern like acute myocardial infarction and raised vinyl-mandelic acids in urine suggestive of increased catecholamines level in the blood. Majority of cases died at tertiary care hospital. No additional treatment was added than the way I was treating at PHC. Discussion at medical colleges with professors was discouraging and every body was waiting for availability of scorpion anti-venom. I was disheartened and distressed by one or two deaths daily due to scorpion sting at PHC particularly in hot season. I realised the importance and need of rational treatment of this life-threatening, time-limiting, acute medical 1 Bawaskar Hospital and Research Center, Mahad, Raigad, Maharashtra 402301. Phone : 02145 222398 E-mail: himmatbawaskar@rediffmail.com

emergency evoked by scorpion sting often faced by farmers and farm workers. I was restless and all the while disturbed and was thinking round the clock how to save these victims in absence of availability of scorpion anti-venom. I studied the available literature regarding scorpion venom (8,9). I read that scorpion venom is a sodium channel activator. Venom stimulates autonomic nervous system. As a result of this action of venom, enormous amount of adrenaline, acetylcholine is liberated into circulation causing autonomic storm. Victims sustained myocardial damage and subsequently developed pulmonary oedema. These victims did not respond to routine conventional line of treatment, i.e., diuretics, digoxin, oxygen, aminophylline, antihistamines and steroids. This is known as refractory pulmonary oedema. I observed that drugs like diuretic, steroids, antihistamines, atropine, steroids and beta-blockers, used from many years in these victims, are responsible for rapid deterioration and potentiate the venom actions. Thus before I concluded this, majority of victims were killed by these agents rather than the venom per se (10,11). In the year 1983 I tried sodium nitroprusside drip (SNP) to unload the myocardium, a treatment for refractory pulmonary oedema. In all 86 patients recovered with SNP. But this needs intensive care facilities. Scorpion sting is often a rural accident and majority of cases are reported to PHC. PHC is ill equipped with untrained staff to treat such acute life-threatening medical emergency evoked by scorpion envenoming. Alpha-receptor stimulation plays an important role in the pathogenesis of pulmonary oedema. Prazosin a postsynaptic alpha–1 blocker, reduces preload and left ventricular impedance without causing tachycardia. Phosphodiesterase inhibitor causes accumulation of cyclic GMP one of the mediators for synthesis of nitric oxide (NO) in the endothelium. It is an ideal vasodilator for the treatment of refractory pulmonary oedema. Its action is similar to sodium nitroprusside. Prazosin is easily available all over India and it is given orally. By blocking alpha-receptors over beta cell of pancreas, prazosin enhances insulin secretion that is inhibited by scorpion venom (12). Prazosin reverses both haemodynamic and metabolic derangement caused by scorpion venom. Hence it is called pharmacological and physiological antidote to scorpion venom action (13).

8 Comparative study of conventional treatment, nifedipine and prazosin confirmed that prazosin is superior (14). Since the advent of prazosin the mortality due to scorpion sting has dramatically reduced to < 1% (13). I prepared printouts, slides and gave training to all peripheral doctors in Maharashtra (15), Pondicherry Karnataka, Andhra Pradesh (Hindupur) at my own cost. The mortality in these regions is reduced to < 1% (16,17). My clinical observations confirmed that scorpion antivenom is no more essential for the treatment of severe scorpion sting (18). Similar reports are published from Israel (19). Irrespective of this scientific warning, Government of India spends heavy funds to prepare scorpion anti-venom at Haffkine Institute, Mumbai. Scorpion anti-venom is expensive and it is no more cardio- protective and it is not free from severe anaphylaxis. I observed clinically that scorpion antivenom did not help to reduce the fatality from refractory pulmonary oedema (20,21). Scorpion anti-venom is no better than a placebo, says a report from Tunisia (22). Now prazosin has replaced the anti-scorpion venom and it is no more administered. Prazosin is as good as a specific scorpion antivenom and a number of specific antivenoms are available but their efficacy is uncertain. Ancillary treatment with prazosin is crucial in severely envenomed patients [reported by Warrell DA, Professor of Tropical Medicine at Oxford University (23)]. In one year, 51 peripheral doctors from Raigad, and Ratanagiri districts treated 3522 patients of severe scorpion sting with prazosin. Of these 13(0.3%) died (15). Since 1995 no death due to scorpion sting has occurred in Mahad region. From 1994 to 2003, 525 victims of severe scorpion envenomation admitted at Vijayangar Institute of Medical Science, Bellary, Karnataka recovered with prazosin alone (17). Oral prazosin is quick acting, easily available, relatively cheap and does not risk anaphylaxis, can be administered by peripheral doctors (24). In the management of severe scorpion sting, prazosin replaced the scorpion antivenom. Preparation of scorpion antivenom required scientific laboratory for parking scorpions, milking them for venom, injecting venom in to animals (horse or goat) and animals are bled to get antibodies (antivenom) for use of human victims. Thus this lifetime research saves enormous funds of the nation.

mfc bulletin/August-September 2004 Prazosin: Dosage, Indications and Precautions Prazosin is a selective alpha -1 adrenergic receptor blocker. It dilates veins and arterioles, thereby reducing preload and left ventricular impedance without rise in heart rate and renin secretion. It also inhibits the sympathetic outflow in central nervous system. Thus its pharmacological properties can antagonize the haemodynamic, hormonal and metabolic toxic effects of scorpion venom. Its dose is 125-250 ugm (microgram) in children and 500 ugm in adult and it should be repeated 3-hourly until there are signs of clinical improvement in tissue perfusion, i.e., warming of extremities, increase in urine output, appearance of severe local pain at sting site which was absent or tolerable on arrival, disappearance of paraesthesias, reduction or improvement in heart rate, pulmonary oedema and reduction in raised blood pressure, and rise of blood pressure in hypotensives without hypovolaemia. The dose should be repeated six hourly till extremities become dry and warm. If the initial dose has been vomited, prazosin dose should be repeated. In confused, agitated, non-cooperative children, prazosin should be administered by nasal tube after giving IV diazepam. First dose phenomenon is rare with this dose of prazosin. However due care should be taken to avoid postural fail in blood pressure. Children should not be allowed to be lifted. Postural hypotension should be treated by giving head low position and intravenous fluids. Prazosin should be used as short acting formula (prazopress) and not the sustained acting drug (Minipress XL). Prazosin should be administered by a treating doctor and he should confirmed clinically that its action started, i.e., reduction or improvement in blood pressure and improvement in peripheral circulation (palms becoming dry and warm). In case of vomiting soon after oral drug one should see vomitus for drug or tablet so as to repeat the dose of prazosin. Simple given prazosin does not solve the problem one should monitored case for blood pressure, hear rate, urine out put and development of pulmonary oedema may needs other intervention like sodium nitroprusside or dobutamine. Indication of dobutamine is tachycardia >130 per minute, warm extremities, air hunger and with or without pulmonary oedema. Dobutamine is to be administered 8-12 microgram per kg weight, one may need to be continue the infusion for >36 hours. It is important to note that this is lifetime research (last 4 decades) done from my own family earnings without help of any funding agency.

mfc bulletin/August-September 2004 References 1.

Mundle PM. Scorpion sting. (Letter) British Medical Journal 1961; II: 1042.

2.

Bawaskar HS. Scorpion sting and cardiovascular complications. Indian Heart Journal 1977; 29:228.

9 management of cardiovascular manifestations of scorpion sting. Lancet 1986;II; 511-512. 14. Bawaskar HS and Bawaskar PH. Severe envenoming by Indian red scorpion mesobuthus tamulus: the use of prazosin therapy. Q J Med. 1996; 89:701-704.

3.

Singh DS, Bisht DB, Sukumar G and Murlidhar K. Scorpion sting in South Indians at Pondicherry.

15. Bawaskar HS. Non-allopathic doctors form the backbone of rural health. Issues in Medical Ethics

J.Ind. Med. Assn. 1979; 72:234.

1996; 4:112-114. 4.

Rao P, Premalata BK, Bhatt H and Haranath, PSRK. Cardiac behaviour effects of scorpion venom in experimental animals. Ind. Pediatr. 1969; 6:95.

5.

Santhanakrishnan

BR.

Cardiovascular

manifestations of scorpion sting in children. Ind. Bawaskar HS. Diagnostic cardiac premonitory signs and symptoms of red scorpion sting. Lancet Bawsakar HS. Scorpion sting. Transactions of Royal Society of Tropical Medicine and Hygiene. 1984; 78:414-415. 8.

Freire-Maia L, Pinto GI, and France I. Mechanism of the cardiovascular effects produced by purified scorpion toxin in Rat. Pharmacological and Experimental Therapy 1974; 188:207-213.

9.

17. Hayagreev VN, Nagbushana MV, Sunder BK and Prasad G. Autonomic storm in patients with Association of Physicians India 2003; 51: Abstract 340: 1298. 18. Bawaskar HS and Bawaskar PH. Treatment of

1982; II: 552-554. 7.

37:504-14.

scorpion sting. A Decade Experience. Journal of

Pediatr. 1984; 21:41-48. 6.

16. Mahadevan S. Scorpion sting. Ind. Pediatr. 2000;

cardiovascular manifestations of human scorpion envenoming: Is serotherapy essential? J.Tropical Medicine and Hygiene 1991; 94:156-158. 19. Sofer S, Shahak e, and Gueron M. Scorpion envenomation and antivenom. J. Pediatric, 1994; 124; 973-78. 20. Bawaskar HS and Bawaskar PH. Envenoming by

Cardiovascular

scorpions and snakes (elapidae), their neurotoxins

manifestations of severe scorpion sting. Chest

and therapeutics. Tropical Doctor 2000; 30:23-25.

Gueron

M,

Yaron

R.

1970; 57:156-162. 10. Bawaskar HS and Bawaskar PH. Role of atropine

21. Bawaskar HS and Bawaskar PH. Clinical profile of severe scorpion envenomation in children at rural

in management of cardiovascular manifestations

setting. Indian Pediatric. 2003; 40:1072-1075.

of scorpion envenoming in humans. J. Tropical.

22. Abroug F, Elatrous S, Nouria S, Gaguiga H etal.

Medicine and Hygiene 1992; 95:30-35. 11. Bawaskar HS and Bawasakar PH. Cardiovascular manifestations of severe scorpion sting in India (review of 34 children). Annals of Tropical Paediatrics 1991,11:381-387.

Serotherapy in scorpion envenomation: a randomosed controlled trial. Lancet 1999; 354:906909. 23. Warrell DA. Venomous bite and stings in Saudi Arabia. Saudi Medical Journal 1993; 14:196-202.

12. Bawaskar HS and Bawaskar PH. Vasodilators:

24. Bawaskar HS and Bawaskar PH. Management of

Scorpion envenoming and the heart (an Indian

cardiovascular manifestations of poisoning by the

experience). Toxicon 1994; 32:1031-1040.

Indian red scorpion (Mesobuthus tamulus).

13. Bawaskar HS and Bawaskar PH. Prazosin in

British Heart Journal 1992; 68:478-80.

10

mfc bulletin/August-September 2004

Who Should India Emulate in Health Care – United States or Cuba? -Dr. Padma Balasubramanian The population of the United States was nearly 281.5 million in the year 2000 according to the U.S Census Bureau. It is a country with unparalleled economic and military power. On the other hand, Cuba is a small country with a socialist economy with few natural resources and a population of just over 11 million. This article will first briefly examine the health care system in the U.S and then move on to Cuba. The U.S spends about 14% of its $ 10.9 trillion GDP, which is a significant amount, on health care. There is no nationalized health care system available in the country unlike in the other western capitalist democracies. Health insurance is of two forms – public and private. Public insurance is government supported and the two important programmes – Medicare and Medicaid were enacted in 1965. The State Children’s Insurance Programme is for poor children and was legislated in 1997. Medicare is the government-supported programme for people over 65 years of age and some categories of younger disabled people. The hospital portion of Medicare, Part A, is free but Part B that covers visits to doctors has a monthly charge that is deducted from the person’s social security check. Medicare does not cover all the charges. Medicare does not cover medications and other necessities and the elderly often have to buy private insurance policies to cover their medications [1]. Medicaid is a public health insurance programme for poor people. Medicaid is administered by the state governments with financial assistance form the federal (central) government. There is a great deal of variation in the proportion of poor people covered. The national poverty income threshold for an individual is $ 8,000 per year and the person is not allowed to have more than $ 5,000 in savings or property. Many workers who earn minimum wages are not entitled to Medicaid as their net income may cross $ 8,000 a year [1]. Private health insurances operate in the private sector and are dominated by health maintenance organizations (HMOs). Private insurance is generally obtained through employment. The employers are under no obligation to offer insurance and the employee may have to pay a significant portion of the cost with the employer paying the rest. Uninsurance Rates in US Having given an outline about the different insurance programmes available, it is time to look at some hard

data. 43 million Americans lack health insurance. During the economic boom of the 1990s 8 million more Americans joined the ranks of the uninsured. Three quarters of those without health insurance are children or working adults. Uninsurance rates were greater than 30% in 1998 for workers in agriculture, construction and household services. In retail services, entertainment, fishing and forestry more than 20% of the workers are uninsured. About 12.2% of health care workers who account for 1.36 million people are without insurance. Of note, only politicians are fortunate and indeed there are no uninsured legislators! Poorer families have the highest uninsurance rates and minority groups such as Hispanics (those of South American origin) and African Americans are less likely to be covered by insurance. Hispanics constitute 12.5% of the total U.S population and African Americans constitute 12.3% of the U.S population. 33.4% of all Hispanic origin Americans and 21.2% of African-Americans do not have health insurance. The situation is particularly dismal for the youth. Among the young adults age 1824, 28.9% are uninsured. During the years 1995-96, 23.1 million children of the total of 70.8 million went without health insurance for at least for a month [7]. Health Risks in the US: Insured and Uninsured An analysis in the Journal of the American Medical Association (JAMA) concluded that the death rates for the uninsured are about 25% higher than for comparable people with health insurance, thousands die each year from lack of coverage. In their study, David Himmelstein and Steffie Woolhandler [6] concluded that 100,000 people died in the U.S each year because of lack of needed care – three times the number of people who died of AIDS. The uninsured also experience more adverse outcomes from negligence during hospital admissions than those with private health insurance policies. In a study carried out in New York hospitals, 40.3% of the adverse events suffered by uninsured patients were due to negligence compared to 20.3% with private insurance. The poor were also at a higher risk – 36.5% of adverse events due to negligence vs. 25% among the non-poor. The Archives of Internal Medicine in an article in 2000 reported that the uninsured forego care for serious symptoms that included loss of consciousness, breast lump, chest pain, productive cough with fever, for a long time. The US has the lowest prenatal care of any developed country: 17.5% of pregnant women and 27.7% of pregnant black women do not receive care during the first trimester of their pregnancy; 7.3% of black women do not receive any care even in their

mfc bulletin/August-September 2004 third trimester. The infant mortality rate (IMR) in 2000 in the US was 6.9 per 1000 live births. In 1998, the US ranked 28th in the world in IMR despite being the richest country in the world. The ranking is due to wide disparities in the health care provided to the minority groups such as African Americans, Native Americans and Hispanic Americans. The IMR in African Americans in 1998 was 14.1% per 1000 live births in 1998. While the facts painted above for the uninsured are depressing, life is not that rosy for many Americans with health insurance. Between 1988 and 1996 the proportion of money paid by the employees for health insurance in small firms rose from 12% to 33%. The employers have shifted costs onto employees and limit employees’ choice of plans. More than 4 months ago, about 70,000 grocery workers went on a strike and continue to be on the picket lines in California – the most populated state in the US, against supermarket giants who want to drastically slash health care benefits on workers who work on extremely low wages [13]. The elderly too are under attack on many fronts. Many major corporations have cut back company paid health benefits for retirees. Medicare, the public health insurance that is available to all citizens above the age of 65 offers very little coverage for nursing homes or prescription drugs. By 2000, the average senior citizen was spending 25% of total income for medical care. Long term care of the elderly in more than 70% of the cases is by informal – unpaid mostly female caregivers [7]. Private insurance covers only 7% of the long-term costs [5]. 45% of all bankruptcies involve a medical reason or a large medical debt. Vincent Navarro, Professor of Public Policy, Sociology and Policy Studies, at the Johns Hopkins Bloomberg School of Public Health has written extensively about what he has referred to as the ‘inhuman state of health care’ in the US He defines class as the root of the health care problem. A blue collared worker with cardiovascular disease is 2.4 times more likely to die from the disease than is a corporate lawyer. Gender and race are undoubtedly important considerations in the U.S and the mortality statistics do reflect this, a black man with cardiovascular disease is 1.8 times more likely to die of it than a white man [11]. Finally, profit driven health care in the U.S is the disease and certainly not the panacea for health related problems. Primary care doctors are being pushed to treat patients with complex health issues and not to refer them to specialists to save money in HMOs (Health Maintenance Organizations – private health care firms). Mentally ill patients are under treated in HMOs. Institutions caring for the mentally ill have had to close down in several states due to lack of funding and the

11 streets are filled with the mentally ill in many large cosmopolitan cities. Doctors Himmelstein, Woolhandler and Hellander in their landmark book ‘Bleeding the Patient’ talk about the suffering of many who are ill served by the U.S health care system while paradoxically a surplus of resources exist in a country that has some of the world’s best medical technology and medical expertise. While millions of Americans are denied health care, 300,000 hospital beds lie empty, a situation similar to India where hundreds of tonnes of grains perish in godowns while large numbers of Indians starve – starvation in the midst of plenty! Milton Friedman the leading academic who is an advocate of free market economics succinctly said “Few trends could so thoroughly undermine the very foundations of our free society as the acceptance by corporate officials of a social responsibility other than to make money for their shareholders as possible”. These sentiments also apply to the corporate world of health care in the US Profits over people in a nutshell. Cuba’s Health Care System Now, moving on to the Cuban healthcare system. Cuba has a population of just over 11 million per the 2000 census. The dictator Fulgencia Batista ruled Cuba for many years before the revolution led by Fidel Castro replaced him on January 1, 1959. Prior to 1959, there was no comprehensive government funded health programme. The very rich, a proportionately small number of Cubans controlled the bulk of the country’s wealth and went to privately run hospitals and clinics, and the middle class, about 10% of the population, went to ‘Mutualistas’ which were family owned pre paid insurance managements. The remaining large class of peasants went to 46 overcrowded, understaffed government hospitals, some first aid stations in the main cities and eight maternal and child health stations built for 8.5 million people [12]. The distribution of doctors was very uneven with majority in the cities and hardly any in the rural areas. There were 500 business establishments engaged in the production and distribution of pharmaceuticals, 70% of those were from North America. With the formation of the socialist government in Cuba in 1959, health care was seen as a basic human right rather than a service for economic profit. Health care was considered the responsibility of the state, and its leaders considered health indicators as measures of government efficiency. Act 723 of the government in January of 1960, a year after the revolution, stipulated that doctors were to serve in rural communities full time for a year, which was then extended, to two years. It also stated that graduate nurses and public servants could be sent anywhere in Cuba as well. Construction of 118 dispensaries in the interior of the country and

12 156 rural hospitals was launched in 1961. By 1963 there was one doctor for each 1100 people. By 1976, 336 modern polyclinics were set up with preventive and curative functions [9]. See Table 1 for Cuba’s health care institutions in 1999. Table 1: Cuba’s Health Institutions in 1999 Polyclinics 400 Rural hospitals 65 Children’s hospitals 28 Dental clinics 160 Blood banks 24 Medical schools 28 Dental schools 8 Nursing schools 76 Health training schools 104 Source: [9] Organization of health care services in Cuba: There has been a regionalization of health services and division of the country into geographic service areas, which has facilitated planning, and organization of facilities [3]. The Health Directorate in Cuba is a threetiered structure. Each level (tier) is semi-autonomous and has access to its own finances. The Ministry of Public Health is at the top of the hierarchy, then there is the Provincial health directorate and below that is the Municipal Health Directorate. Provinces are divided into municipalities, which in turn are divided into health areas. Polyclinics or primary health care facilities are present in the health areas. The polyclinic’s health areas are further subdivided into mini-polyclinics. At the municipal level, secondary or hospital care and specialized services are available, at the provincial level tertiary or super specialty care is provided. Super specialty care is also provided at the national level [3]. Between 1959 and 1965 there were intense ideological debates between the physicians from pre-revolutionary Cuba and the new government and its supporters in the medical field about the role of private practice. The medical school graduates of 1965 gave up private practice and private practice has been prohibited since [12]. In 1984 Cuba introduced to its health care system the Family Doctor Programme. Prior to that basic primary care in rural areas was provided by resident physicians, dentists, nurses and technicians and by specialists from the municipal hospitals who rotated regularly through the rural health facilities. By 1998, there was one family doctor and ancillary nurse to every 100-110 families in Cuba. The Ministry of Public Health (MINSAP) has declared primary health care and community health promotion to be its priority targets.

mfc bulletin/August-September 2004 Child health, maternal health, internal medicine and dentistry were held to be the most important. As of 1998, 5% of the GNP was allotted to the Family Doctor Programme. The MINSAP’s total budget accounts for about 8% of the Cuban GNP. The family doctor and nurse in each of the areas work with personnel from various mass organizations such as block committees – Committees for the Defence of the Revolution (CDRs) – and the Federation of Cuban Women (FMC). The latter have organized ‘Health Brigades’ which advise people on medical care and preventive medicine, it is largely because of these brigades that the average Cuban is so well versed in health issues [9]. Family doctors work closely with polyclinics and other institutions. They carry out timely diagnosis and treatment of diseases and promote preventive measures. They make appropriate referrals to secondary and tertiary centres as needed. Moving from the family clinic to the polyclinic the basic medical staff has 20 doctors and nurses. Specialists, psychologists, social workers and support staff such as typists, technicians and cleaners augment them. These specialists in turn co-ordinate with specialists from tertiary hospitals in areas such as surgery, dermatology, orthopaedics, ophthalmology, etc. Their work is aided by the reports sent by family doctors in small communities [9]. Cuba has had no cases of poliomyelitis since 1963, malaria has been eradicated since 1968, diphtheria disappeared in 1971, and gastroenteritis killed 4157 children in 1962, this figure dropped to 761 in 1975. The infant mortality rate in Cuba as reported in 2000 was 7.1 per 1000 live births. Life expectancy at birth is 76.21 years comparable to the advanced capitalist nations. Cuban health care for special populations – For the elderly concerted efforts are made to ensure that they remain with their families as a survey revealed that 70% more died in institutions when compared with those in the same age group and with similar problems that stayed in the family. Support is given to the caregivers in the form of education and institution of day care centres known as ‘grandparent clubs’. These clubs work closely with the polyclinic and the elderly participate in community health promotion and actively assist in infant health care in addition to other activities in the community. This has contributed to their enjoyment of life and good health [4]. See Table 2 for provision of the elderly. Table 2: Services for the Elderly Geriatric doctors 210 Grandparents clubs 12,229 Residential places 225 Beds in residential places 10,874 Day places 96 Source: [4]

mfc bulletin/August-September 2004 Integral Care of women and children – the health of pregnant women and children is seen as a major determinant of quality of life and the productive capacity of future generations. Pregnant women can have as much as 15 prenatal visits, education is imparted on hygiene, health during pregnancy, childbirth and child care. Special prenatal attention is given to women at high obstetrical risk; all childbirth takes place in hospitals. Polyclinic norms require seven examinations of children in the first year of life – considered ideal in the United States but in Cuba the national average even in the 1980s was 8.7 visits. The first three of these visits occur in the homes enabling the medical team to assess the hygiene and sanitation [3]. The Cuban government has made a reality of its slogan, “There is nothing more important than a child.” The Cuban health system has made significant advances and provides high-technology diagnostic and imaging care. There are now 60,000 doctors and the doctor to person ratio in Cuba is 1:167 and this figure is 1:358 in the United States. There have also been outstanding contributions to research. Cuba is now the sole producer of a vaccine against meningitisB, has produced a microsurgical approach to cure retinitis pigmentosa that causes blindness, which has a success rate of 80%, and has developed a successful treatment for vitiligo. From the beginning of the revolution, 91 countries have received aid from Cuba with the participation of 51,059 health care workers. Cuba’s internationalism in the health care area has been widely admired and it has become a well-known ‘medical power’. Of note, 12,000 Ukrainian children, victims of the Chernobyl accident, spent nearly a year in Cuban care centres [9]. The ‘Special Period’ – Cuba entered what Castro has referred to as the special period in 1990 after the collapse of the Soviet Union. The Soviet Union had provided Cuba with economic support since the early days of the revolution and the two countries had a lot of preferential trade agreements. The United States imposed economic sanctions on Cuba in 1962 in strong opposition to Cuba’s socialist policies, which had affected its business interests. Prior to the revolution US interests owned more than 90% of Cuban electrical facilities and telephone services and more than 50% of it sugar industry. The US embargo was reinforced in 1992 by the Torricelli Law, and in 1996 by the HelmsBurton Law. U.S companies and their subsidiaries are prohibited to trade with Cuba and 90% of the trade affected has been in food and medicines. This has had devastating effects on the health and well being of Cubans. Painkillers and anti emetics have not been available to treat patients who have cancer and are receiving chemotherapy. In 1997, the American Association of World Health (AAWH) published a report on the impact of the American blockade on

13 Cuban health. Antibiotics such as vancomycin and cephalosporins, lifesaving agents such as dobutamine and dopamine, steroids, cancer fighting medications were among those in short supply. In addition the AAWH reported that equipment for advanced diagnostic imaging that Cuba had established were also in limited supply as components could not be ordered due to the trade embargo [9]. The Cuban government has responded to the challenges of the special period by cutting down investment in arts, culture, defence and administration but has continued to provide quality health care to the population. The Cuban health budget dropped from $ 227.3 to $ 66.9 million in 1993 but it has recovered to $ 139.4 million in 2000. In 1996 the MINSAP set up a network of 169 municipal pharmacies (MMP). A pharmacist and a pharmacoepidemiologist manage each. Practice guidelines have been implemented for management of common condition like hypertension, upper respiratory and urinary tract infections. In the face of the ongoing embargo and shortages in drugs the network of pharmacies is able to ensure better drug utilization patterns and coordinate research. At present five national research programmes are on for drugs used in asthma and hypertension, cardiovascular agents, secondary prevention of acute myocardial infarction and drug use among the elderly [2]. In 1993 the daily intake of the average Cuban had dropped to 1863 calories with protein and fat consumption below Food and Agriculture Organization (FAO) recommendations. By the end of 2000, as per Siman Koont, coordinator of Latin American Studies at Dickinson College in Carlisle, Pennsylvania, US, the food availability in Cuba reached daily per capita figures of 2,600 calories and more than 68 grams of protein. The FAO considers 2,400 calories per day and 72 grams of protein per day as adequate. This remarkable improvement in food security was achieved by the aggressive efforts of the Ministry of Agriculture, idle land was put to use, existing fertile land lying in vacant lots and parks was brought into food production. Patios and yards next to people’s houses were brought into cultivation. New technologies replaced imports and organic/natural compost has been used to improve soil. Cuba is now on its way to self-sufficiency in rice production [8]. New innovative methods are not only being used in agriculture but also in medicine. Cuba has the greatest plant diversity in the Caribbean with some 6200 species. A thorough study of herbal resources is going on. Interferons, monoclonal antibodies and antibiotics are all being investigated as possibly being derived from primary herbal resources using intermediate level technology [9].

14 Conclusions Should India emulate the U.S or Cuba in health care? The question appears almost redundant. Cuba in 1959 had rundown hospitals and a legacy of hunger, disease, poverty and all kinds of deprivation for the majority of its population. Within a short period of time the government transformed the health of the population and indeed that of the entire nation. Profit oriented health care was dismantled and quality health care that was free was made available to all. The revolutionary government along with the measures taken to set up exemplary health care also instituted agrarian reform, nationalised key resources and industries and provided housing, employment and education. Medicine alone will not improve the overall health of the population in India. There has to be a radical social and economic transformation in the Indian society to achieve real improvement in health care. A band-aid approach will not eliminate disease either real or metaphorical [10]. As Rudolph Virchow, the famous scientist, remarked, “Health is Politics, Politics is Health.” References 1.

Budrys, G. 2003. Unequal Health. New York: Rowman and Littlefield Publishers.

2.

Diogene, E. et al. ‘The Cuban experience in focusing pharmaceuticals policy to health population needs: initial results of the National Pharmacoepidemiology Network (1996-2001)’. Pharmacoepidemiology and Drug Safety 2003; 12: 405-407

3.

Feinsilver, J. M. 1993. Healing the Masses. Berkeley: University of California Press.

4.

Greene, R. ‘Effective community health participation strategies: a Cuban example’. Int J Health Plann and Mgmt 2003; 18: 105-116

5.

Health Affairs 2000.

6.

Himmelstein, D. and Steffie Woolhandler, New England Journal of Medicine, 336:11, 1997.

7.

Himmelstein, D., Steffie Woolhandler, Ida Hellander. 2001. Bleeding the Patient. Monroe, ME: Common Courage Press.

8.

Koont, S. ‘ Food Security in Cuba’. Monthly Review, January 2004.

9.

MacDonald, T. 1999. A Development Analysis of Cuba’s Health Care System since 1959. New York: Edwin Mallen

10. Ministerio the Salud Publica, Subdesarrollo economico, Pg. 4 11. Navarro, V. ‘The Inhuman State of US Health Care’. Monthly Review, September 2003. 12. Roemer, Milton I. 1976. Cuban Health Services and

mfc bulletin/August-September 2004 Resources. Washington, DC: Pan American Health Organization, Pg. 44. 13. Thompson, I. Feb 19, 2004. The Workers’ World Acknowledgements: My particular thanks to Himmelstein, Woolhandler, Hellander, MacDonald and Feinsilver whose excellent books provided me with a lot of information and without which this article could not have been written. Announcement

Women Centred Health Project Publications Women Centred Health Project (WCHP) is a collaboration of Public Health Department (PHD) of Municipal Corporation of Greater Mumbai (MCGM), a non-governmental organization – SAHAJ, Vadodara, Gujarat and the Royal Tropical Institute, Amsterdam, the Netherlands. The Project aims to identify strategies for mainstreaming gender and rights perspectives into the PHD and MCGM. We believe in quality of care from the women’s perspective and within the framework of client’s rights. The Project has tried to integrate this vision in all the interventions. WCHP’s work since 1996 has resulted in several training manuals and process documents. ‘Training Manual on Women’s Health for Clinicians’, is designed to upgrade the knowledge and skills of the doctors working within public health systems and also to enhance their social and gender perspective in relation to women’s health. (Pages 240, price Rs. 100 per copy). ‘Paving the Way for RCH: Tools for Quality and Gender Mainstreaming’, is a set of Fact Sheets and tools on 14 important aspects of our work. (Pages 190, price Rs. 100 per copy) ‘Mainstreaming Quality Assurance in the Public Health Department, Mumbai, India’, is a documentation of the work done by Women Centre Health Project for sensitizing health care providers and administrators to the concepts of gender sensitive and client-centred quality reproductive health services. (Pages 40, Rs. 35 per copy) ‘Stepping Stones Workshops in a Public Health Department’. Stepping Stones is a training methodology that has been successfully used in several countries, for promoting communication around sexuality among various community level groups. (Pages 40, Rs.35 per copy) ‘Counseling Service in the Gynaecology Clinic of a Municipal Hospital in Mumbai’ is a process documentation of setting up a counselling centre in a secondary public hospital. It describes the counseling training given to para medical staff as well the result of a systematic evaluation done by an external evaluator. (Pages120, price Rs. 70 per copy) Postal charges are separate for each publication. Copies can be ordered from SAHAJ, 13 Tejas Apartments, 53 Haribhakti Colony, Baroda 390 007. Email: sahajbrc@icenet.co.in

mfc bulletin/August-September 2004

15

Experiences of the People at Public and Private Medical Care, Chennai, Tamil Nadu1 Francis Adaikalam.V.2 Curative services in the secondary and tertiary sectors are increasingly being privatised. A systemic phasing out of services by offering in-door and out-door outsourcing are clear steps towards privatisation of services within the public sector. Various societies and corporations established by the state government for regulating and outsourcing is further evidence of this move. The idea is to allow the private players to flourish and also to create independent authorities to regulate and monitor health care services. The other move towards privatisation by the state is to allow NGOs to become providers. This further paves the way for the state to withdraw its services. This write up throws some light on the exploitative nature of the health system (private and to an extent public health care services). The abuses people encounter when they visit a health care provider- be it public or private – is mainly in terms of verbal and financial in nature. The denial of services in public hospitals is many-fold. One such example is dividing the people based on perception of their ability to pay. Favouritism and negation of people by the state has been documented from independence onwards on various issues like developmental and industrial projects, beautification of the cities, with the dimension of caste, class and region. By showing partiality to certain people, the state fails to adhere to the fundamental rights - equality before law for instance enshrined in our constitution. The state conveniently argues for the privatisation of services and regulation of health services by showing its resource crunch . It is a fact that central government allocates more for defence than any for welfare schemes. The notion that health care is a wasteful expenditure and need to be axed is translated into action by introducing user fee and pay wards, privatisation of services, contract labourers and altogether restructuring the health system. ‘Restructuring’ is in terms of showing favouritism by providing better services (both better quality and better facilities) for pay wards, based on the ability to pay, a market phenomenon, which is unheard in the 1 Interviews with affected people were arranged by the Pen Thozhilalar Sangam (a trade union for women in the unorganised sector), Chennai 2 Ph.D scholar, Centre of Social Medicine and Community Health, JNU, New Delhi. Email: francisjnu@yahoo.co.in 3 Names have been changed to protect privacy and confidentiality.

public sector. Those who consume more are considered suitable for better and quality health care services. Denying the quality of services to poor sections of society altogether commodifies health care. This leads to no betterment of the people but only increases inequality among people. Over the years a notion has been created that people are getting services free of cost and they need to be taught and made accountable. Direct and indirect taxes, which are used for providing welfare services, are not considered. This is one way of privatisation and withdrawing of services. This write up is based on the experiences of women, the ultimate sufferers at any point of time, who have been ill-treated during the course of using public and/ or private medical services. Ramayee (female),3 Red hills Ramayee was married off at a young age (16 years). Her husband is a painter, mainly engaged in painting of newly constructed buildings, earning Rs.120/- per day. Ramayee comes from a family where her parents are employed in stone quarrying work. When she conceived she went for frequent check ups to a nearby government hospital. When she was seven months into her pregnancy, she was asked to go for a scanning; upon which she was told that there would be problems in the delivery. Later, she was referred to the Institute of Obstetrics and Gynaecology and Government Hospital for Women and Children, Egmore, Chennai. Subsequent examination by an army of doctors assured her of safe childbirth. Ramayee who is below poverty line could not afford private medical care. She was aware of the services offered by the government, which is priced low compared to the exploitative private medical care. In the government hospital verbal and economic (that is money-draining) abuse by the staff starts right from the day they enter till they exit. Irrespective of their class, caste, and religion everybody is asked to give their portion of money as ‘tips’ based upon their networks inside the hospital and class nature- means the worker decides by assessing the relatives and the patients, how much he/she can extract. After much negotiation, hospital workers having the upper hand, workers settle down for an affordable amount. Total expenditure incurred in government hospital is given in the table below.

16

mfc bulletin/August-September 2004 Corruption faced by Ramayee at Egmore, Chennai. No

Reason

Amount demanded (Rs)

Amount paid (Rs)

01

Admission (to get a slip since it was at night)

60

30

02

Watchman

10

10

03

Payment to ANMs immediately after delivery

150

100

04

Ward boy (to change the ward)

100

100

05

To pull the trolley

60

40

06

To bathe the infant after the delivery

150

50

07

To get doctor’s signature

30

30

08

Ward boy

50

50

09

Mortuary

150

100

10

To dispose the body

500

250

Total

1260

860

Though Ramayee accepts the fact that bribery is a normal affair in state-run hospitals, she has no other option. In her words, ‘it is deciding between lesser of two evils’. Of course, she continued with regular consultation at a nearby private practitioner’s. But soon she decided to move to the public hospital due to fear of not being able to pay. If Ramayee had opted to go to a private clinic for her delivery she would have landed in many problems including the death of her infant. Experience at Government Hospital Ramayee’s parents admitted her and gave enough ‘tips’ -bribery- to the hospital workers to get her daughter admitted in the ward. She soon delivered a child. Two days later the child died (they were told that it was due to premature birth) and was shifted to the mortuary for post-mortem. The mortuary workers demanded heavy money to handover the body. So Ramayee’s family let the hospital workers dispose off the baby by paying a smaller amount. In all the three days at the hospital, they had undergone severe trauma, which made them scary. Right from the para-medical staff to the doctors the patient and their relatives are abused (verbally and economically). If patients deny or state their inability to pay ‘tips’ they are abused verbally. Words like “We are doing service to you by cleaning the toilets and places which were made dirty by you and can’t you pay some amount?” “For such an auspicious occasion, can’t you pay us some amount?” In this particular case (see table above), I have presented the amount the worker demanded and the

amount that the patient paid. The user fee that the hospital charges for various labs, diagnostic services, registration charges and the entry fee for visitors are not included in this amount. Saraswathi (female), Sivathangal Saraswathi, aged around 50, was admitted to the Institute of Obstetrics and Gynaecology and Government Hospital for Women and Children (IOGGWC), Egmore, Chennai in the month of June 2003 for a complaint of excessive vaginal bleeding. Before getting into the corridors of the public hospital she went from one private doctor to another in her area. Initially she went to the doctor in her area who everyone calls kairasikarar (a doctor with the good, healing hand). Though he was not an allopathic medical practitioner, he had this reputation among the people there. After consulting him for more than six months she had no hope of relief for her ailment. She could not take care of her family affairs and had to spend most of her money and time for taking care of her health herself. Then she visited other private practitioners in her area before being referred by a lady doctor to the IOGGWC, Egmore. By that time she had spent more than Rs.5000 for consultation and drugs. Meanwhile her alcoholic husband died and she was forced to live alone. In the end, she lost both her money and her family. Had she been aware of the profit nature of the private providers earlier, all these things probably would not have happened.

mfc bulletin/August-September 2004 Amudha (female), Sivathangal Amudha was admitted in the Government Sanatorium hospital, Tambaram, Chennai during March 2003. She was detected as having pulmonary TB. Before settling down at the Government Sanatorium hospital she took treatment from private practitioners for about four months. Heavy cough with fever made her to visit private practitioners and also she believed in a kairasikara doctor near her area. She made the rounds of various medical practitioners - from General Practitioners to General Physician. But her problem aggravated. She was told that death was not too far. At last the general physician offered her to cure her problem if she could afford to pay Rs 10,000 for about six months. For a woman of her means, the amount was huge and she decided to go to the government hospital. By which time she had spent almost Rs 4,000 for her treatment. The private medical practitioners neither practised a standardised regime to treat nor referred her to a public hospital. This probably made her drug resistant, further complicating her treatment. Moreover, she virtually stopped going to work due to illness, her income was affected and she had nobody to lean on. On entering the public hospital, bribery kicked off right from the gate, and then at the hospital corridor, to the wardroom, to the operation theatre till the person is discharged. Various people are involved (mainly class IV staff) irrespective of the nature of their job. Sanitary workers, ayas, dietary supply staff, ward boy, technicians and other paramedical staff all had to be satisfied (sometimes even after the verbal abuse) to receive their services. Amudha could only recollect spending more than Rs 300 as bribery to all these workers during her stay in the hospital. But she stated that relatively speaking she had got cured for a lesser amount compared to the private medical care, the latter had made her incur debts. Sarala (female), Chehalayapuram This is the case history of a lady in her late 40’s deserted by her husband. After her husband’s desertion, she worked as a maidservant to earn her living. Meanwhile, she had frequent bleeding due to uterine cancer and spent about Rs 300-400 per trip for visiting the nearby private practitioners, which shot up to around Rs 5000 in about 6 to 7 months. After many months of frequent trips to various private practitioners, she visited a female medical practitioner in suburban Chennai. She treated Sarala for quite some time, but when she found that Sarala could no longer be exploited financially she asked her to get treated at the IOGGWC, Egmore. She is now currently undergoing

17 chemotherapy at regular intervals and much of her complaints have been alleviated. But treatment in the government sector was not all that sweet. Corruption runs to the core starting from aaya, the ANMs, to the sweeper, ward boy, watchman, x-ray technician, to each and every staff in the public hospital. This despite the payment of user fee charges by the patients. Added to this patients are denied diagnostic services if they are unable to pay. Conclusion Churchill (1994) in his monograph has pointed out that the purpose of health services is not just to improve the physical and mental health of individuals in the population. Rather the primary goal of health policy is to provide individuals a kind of ‘security’, that is, ‘freedom to live without fear that their basic health care concerns will not go unattended and the freedom from financial impoverishment when seeking or receiving care’. The question here is whether we practise such a type of health system. For example, for Saraswathi choosing the public sector was due to the fact that the doctors working in the public hospitals genuinely state the problem by diagnosing correctly without cheating the patient/health seeker. On the other hand private practitioners search for a reason to treat the disease to mint money. They also reveal everything only at the end, be it the disease or the money that is to be charged. That is the reason why poor people seek services from public sector. In spite of certain shortcomings like ‘tips’ and obeying the instructions without any questions to get well soon and to have better treatment, the hospitals run by the state have the confidence of the people. The government privatised the medical services with the argument that the poor are able to pay. The rationale offered was the fact that there is anyway increase in out-of-pocket expenses. But NSSO data shows that untreated morbidity is high for poor people. Nevertheless, 45 per cent of the poorest continue to depend upon public sector (quoted in Qadeer, I. 2002). One of the major problems with household spending is that it is predominantly paid out-of-pocket on a feefor-services basis. On the other hand, nearly 40% of Indians who are hospitalised in 1995-96 incurred debts to pay for hospital expenditures, with nearly one quarter falling below the poverty line as a result (Peters et.al.2002). This ‘risk line’ differs between states when poor people are hospitalised - ranging from 17% in Kerala to double that in U.P and Bihar (Peters et al 2002). The reason is that mostly public hospitals cater to the need in Kerala compared to largely unregulated private care in U.P.

18 The situation appears to be worse than what one expects. Various state governments find it difficult to identify the exact number of BPL in spite of the Supreme Court’s ruling. So will it be possible to identify ‘who can pay’? Here, user fee invariably results in marginalising the poor further, which is also shown by the NSSO data.

mfc bulletin/August-September 2004 What we need is to create a well-worked referral system as stated in Bhore committee (1948), in the 1983 health policy, instead of paving way for commercialisation and expansion of private markets that will further marginalise the poor.

Accountability and commitment will wither away in medical services if one goes to employ ‘contract workers’ instead of regular para-professionals. The cases discussed above shows that even the contract labourers demand money by stating the low wages and temporary nature of job.

v

Moreover, in countries where large numbers of private providers are still unregulated, health cost is on the rise. For example in the US, due to medical ‘race’ between hospitals, cost did not/do not reduce. Studies show that many hospitals compete in the medical care market and prices actually are on the higher level. The hospitals are engaged in buying more and costlier equipment than their competing hospitals. This ‘medical care arms race’ generated hospital costs 20 per cent higher in the most competitive markets than in the least competitive markets (Robinson and Luft, 1985). And also private medical practitioners in Amudha’s case did not practice a standardised regime. This type of over medication by various practitioners leads to medicalisation of life as Illich (1974) stated.

References

v

v

v

v

Churchill LR (1994): Self-interest and Universal Health Care: Why well-insured Americans should support coverage for everyone. Harvard University Press. London. Glied, Sherry (2003): ‘Health care cost: On the rise again’. Journal of Economic Perspective. Vol. 7(2) pp. 125-148 Griffiths, Sivan and Hunter, D.J(ed) (1999): Perspectives in Public Health. Radcliffe Medical Press Ltd. London. Qadeer, I (2002): ‘Debt Payment and Devaluing Elements of Public Health’. EPW, Jan 5, pp. 1216 Robinson, James and Hal Luft (1985): ‘The Impact of Hospital Market Structure on Patient Volume, Average Length of Stay, and the Cost of Care’. Journal of Health Economics. Vol.4 (4), pp. 333-56.

Acknowledgements: C.Sathyamala, Sujatha Modi for their help at various stages of the work.

Book Review

A Perceptive First Hand Account of the Chinese Health Care System (Observations from China, Philippines and Thailand and Reflections on India. Dr. Shyam Ashtekar, Bharat Vaidyak Sanstha, Sriramwadi, M.G. Road, Nashik, Maharashtra 422 002, pp. 150, Rs. 50/-) Health care system in India has been discussed in Medico Friend Circle, time and again, in different contexts. But we have hardly discussed health care systems in other countries. A discussion on specific features, experiments, achievements and failures especially in other developing countries is needed. Dr. Shyam Ashtekar’s book under review should stimulate a discussion in MFC and in other fora on this issue. The author received a population fellowship from Mac Arthur Foundation in 1998-99 and undertook a four and three week study tour of People’s Republic of China and the Philippines respectively. This book is 1

Email: amol_p@vsnl.com

-Anant Phadke1 an account of what he learnt during this tour. First hand accounts are always very interesting as they give a personal flavour to the description. However, they can be biased, less balanced and less comprehensive. But, Shyam Ashtekar has tried be quite objective and secondly, has not limited it to his personal observations. His table on page 116, based on a World Bank report in 1997 briefly compares India’s health and health care status with that of East Asian countries and with Canada, UK. It provides a much broader canvass to his individual observations of the study tour. Health Care in Today’s China The author begins with an overview of the Chinese health care system. Many would be surprised to read that today, 65% of the people in China pay at the point of services! Only 35% are covered by funds from the government or through some form of health insurance. The health professionals are all in the public sector but

mfc bulletin/August-September 2004 after the ‘reforms’ in 1978, now 65% of people pay them fees at the point of service. It thus appears to be a privately financed ‘Public’ Health System! He notes that a survey in 1998, by the Ministry of Health, showed that 16% of the people could not afford this post-reform payment system. Now on an average for 41% of the people, this system is unaffordable. Ashtekar then briefly describes their system of hospitals and health professionals. The famous ‘Barefoot Doctors’ have by and large been replaced by Rural Doctors, who undergo a three-year training course, compared to the 3-6 month training of the Barefoot Doctors. It is not clear from his account, whether every village has a resident Rural Doctor or not. Ashtekar describes what he saw in the health care system in both developed Zaiding district and a backward Huonig county in the Yunnan province. Even in this backward county, the Village Health Stations (VHS) are ‘clean, well-kept and pleasant’, with ‘130 medicines’ and clinic timings of 8 to 10 a.m., 10 to 5 p.m., and 7 to 9 p.m. It has two resident doctors. An Indian reader is bound to feel a bit envious! The author summarizes the following positive features of the Chinese Health Care System: 4 4 4

4

4 4

‘Astounding’ framework of a three tier system, with a solid base in the form of VHS’s Very impressive health indices ‘Sound and Broad based’ medical education system, medium of instruction is Chinese and not English. Prominent role for the Traditional Chinese Medicine (TCM). Both TCM and allopathic facilities available in the same hospital. Substantial preventive programmes. The Cooperative Medical System has been replaced with fee-for-service system. In some areas, co-operative insurance system is being employed. This trend is likely to grow.

The author also notes the following negative features of this system - overstaffing of hospitals, excessive and irrational use of medicines including the excessive use of injection and saline and high charges by doctors. The Chinese system is a ‘not for profit system’. The excessive use of injections in such a system indicates that the ‘popularity’ of this excessive use is partly due to some cultural factors and cannot be attributed to profiteering alone. As public health specialist, the author also went into the details of the issues of nutrition, sanitation, including toilets reforms, health education, etc. Ashtekar notes that piped water supply is almost universal in China. But tap water is ‘probably

19 unsafe’. Treated water cans are sold and widely used. We learn that doorless toilets are quite common and there is wide spread almost universal use of toilets of different varieties. Nutrition looked good, with liberal use of non-vegetarian sources. After going through this description of the Chinese Health Scenario, one once again feels very sorry for the abysmal situation in India. China was as backward as India in the1950’s. But after the Revolution, despite some lacunae and mistakes, the People’s Republic of China made great strides resulting in a far improved situation for the common people, especially for the villagers. In India, despite tremendous technical progress, the overall system continues to be abysmal. Health Scenario in Philippines and Thailand Ashtekar visited Philippines for three weeks and had a brief stopover in Thailand. He describes in some detail his findings of these visits. The overall socio-economic situation in both these countries is similar to that in India. However, patriarchal culture seemed to have less influence on women’s self-image. The overall health care scenario is also similar to the Indian one. There are two main differences. In both these countries, there is a strong paramedic base. Though the paramedics do not have much therapeutic function, they are there, everywhere, one health worker per 20 families! The second difference is - though medicines are widely available in Philippines under generic names, in both these countries they are far more costly than in India. The author thanks ‘Indian Pharmaceuticals’ (sic) for cheaper medicines in India. What he fails to realize is that the credit goes to the Indian Patents Act 1970 and not to any benevolent attitude of ‘Indian Pharmaceuticals’. The author, who is a firm believer in the enhanced role of the Community Health Workers and other paramedics, argues, in the last section of the book, a case for such a system in India. MFC Bulletin readers are familiar with and would agree in this case. Overall, the book is a good, studied account by an Indian public health specialist on visit to China, Philippines, and Thailand. The book is not very wellstructured. It is more in the form of systematic notes. Nevertheless, it is quite instructive, and the photographs also lend it the flavour of a first hand account. The price Rs 50/- for a 150-page book is low by today’s market standards, and the author deserves to be congratulated for it.

20

mfc bulletin/August-September 2004

Health Rights, Women’s Lives: Challenges & Strategies for Movement Building 10th International Women & Health Meeting September 21-25, 2005, New Delhi The International Women and Health Meeting (IWHM) has its roots in the global women’s movement and includes a wide range of organizations, networks, and grassroots women’s groups. The 10th IWHM will mark nearly two and a half decades of the global feminist solidarity on issues that impinge on the health and well being of women. The current context — of global economic restructuring and liberalization of markets, increasing militarisation of countries, regions and zones, growing fundamentalisms of various hues, reemergence of population policies, adoption of developmental models that are playing havoc with the environment — calls for urgent action by civil society including feminist groups. The 10th IWHM seeks to highlight resistance to such politics and related policies as issues of significance to women’s health. The linkages and interconnections of these seemingly disparate phenomena and policies and their impact of women’s health will be explored even as it will attempt to center-stage the issue of women’s health as a fundamental right. Focal Themes include: Public Health, Health Sector Reforms and Gender; Reproductive and Sexual Health Rights; Politics and Resurgence of Population Control Policies; Women’s Rights and Medical Technologies; Violence (of State, Militarism, Family and Development) and Women’s Health Call for Participation: The 10 th IWHM Programme Committee invites write-ups for paper presentation, cultural events, organization of workshops, etc, relating to Conference theme and Objectives. The deadline for submission is November 15, 2004. Website:www.10thiwhm.org Contact: Manisha Gupte(masum@vsnl.com) and/or Sarojini N.B. (samasaro@vsnl.com)

Registration Number : R. N. 27565/76 Subscription Rates

Annual Life

Rs. Indv.

Inst.

U.S$ Asia

Rest of world

100 1000

200 2000

10 100

15 200

The Medico Friend Circle bulletin is the official publication of the MFC. Both the organisation and the Bulletin are funded solely through membership/ subscription fees and individual donations. Cheques/ money orders/DDs payable at Pune, to be sent in favour of Medico Friend Circle, addressed to Manisha Gupte, 11 Archana Apartments, 163 Solapur Road, Hadapsar, Pune - 411028. (Please add Rs. 15/ - for outstation cheques). email: masum@vsnl.com

MFC Convenor N.B.Sarojini, I Floor, J-59, Saket, NewDehi 110 0017 Email: saromfc@vsnl.net; Ph: 011 26968972, 26562401

Western Regional Public Hearing on “Denial of Right to Health Care” organised by NHRC and JSA at Regional Science Centre, Shyamala Hills, Bhopal, July 29, 2004. For details contact: Abhay Shukla at CEHAT, Pune. Email: abhayseema@vsnl.com or cehatpun@ vsnl.com

Contents Exploring ‘Rights-based Approach’ for Public Health

- Abhay Shukla

1

Management of Severe Scorpion Sting Who Should India Emulate in Health Care – US or Cuba? Experiences of Public and Private Medical Care A Perceptive Account of the Chinese Health Care System

- H.S.Bawaskar - Dr. Padma Balasubramanian - Francis Adaikalam.V. - Anant Phadke

7 10 15 18

Editorial Committee: Anant Bhan, Neha Madhiwalla, Dhruv Mankad, Amita Pitre, C. Sathyamala, Veena Shatrugna, Chinu Srinivasan. Editorial Office: c/o, LOCOST, 1st Floor, Premananda Sahitya Bhavan, Dandia Bazar, Vadodara 390 001 email: chinumfc@icenet.net. Ph: 0265 234 0223/233 3438. Edited & Published by: S.Srinivasan for Medico Friend Circle, 11 Archana Apartments, 163 Solapur Road, Hadapsar, Pune 411 028. Views and opinions expressed in the bulletin are those of the authors and not necessarily of the MFC. Manuscripts may be sent by email or by post to the Editor at the Editorial Office address. MEDICO FRIEND CIRCLE BULLETIN PRINTED MATTER - PERIODICAL Registration Number: R.N. 27565/76

If Undelivered, return to Editor, c/o, LOCOST, 1st Floor,Premananda Sahitya Bhavan, Dandia Bazar, Vadodara 390 001.


