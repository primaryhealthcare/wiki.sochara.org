---
title: "God That Is Failing"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled God That Is Failing from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC040.pdf](http://www.mfcindia.org/mfcpdfs/MFC040.pdf)*

Title: God That Is Failing

Authors: Knaus William A

medico friend circle bulletin April, 1979

GOD THAT IS FAILING William A. Knaus MFC believes that cost1y western model of medicare to irrelevant in Indian context. However, blind aping of the health technology of the west is the fashion of the day in India with a faith, amounting almost to superstition, that this technological miracle will solve all our health problems. But now signs of decay are appearing in the ‘infallible’ western ideas itself. Author here points towards some of the manifestations of this nemesis. — Editor.

Most of the tools a doctor used 25 years ago fit into a small black bag. Today the typical American physician owns or has access to Dlrs 250, 000 worth of diagnostic equipments. Today’s hospital is becoming a warehouse for new medical technology. Most hospital expansion is designed not to add more beds but to provide room for new machines. But with the artificial kidneys, respirators, multichanneled blood analyzers, chemical and micro-biological laboratories, echocardiograms, CT scanners, radiation therapy machines, pacemakers, cardiac catheterization units, endoscope suites, hyperbaric chambers, laminar flow rooms, fetal monitors and coronary, burn and intensive care units have come questions. The most frequent one is cost, American medicine today is big business, accounting for nearly one-tenth of the gross national product. The bill for American medicine has quadrupled since 1965. New technology, with its emphasis on the latest machines and the trained people to run them, accounts for 20 to 25 % of the current Dlrs 140 billion total. An average of one-third of each hospital bill is directly linked to machines. Also at issue are benefits. Whenever one tries to link the development of new technology with a coincident improvement in health, the answer is always the same. There is none. The real advances in medicine have been simple and relatively inexpensive discoveries.

Antibiotics, vaccines—not machines—have produced this greatest return and saved on most lives. This emphasis on end-stage technology also has overshadowed the less glamorous but more effective issue of disease prevention. An article in a recent issue of ‘Science Magazine’ estimated that id minimal changes in the smoking, drinking and eating habits of Americans were combined with reductions in environ. Americans were combined with reductions hazards, their average life span would increase by 10 per cent.

In the past, the question of benefits form emphasis on end-stage technology has been of academic interest alone. But how, the rapidly increasing cost, combined with the desire on the part of the carter plan, has brought political sparks to this previously philosophical discussion. President Jimmy Carter, after reading a Washington post article describing the efforts of private insurance companies to reducing the use of medical technology, sent a hand-written note to Health, Education and Welfare (HEW) secretary Joseph Califano saying, ‘we should do the same thing.” The result was the recent announcement by Califano that he would place a cutting on the amount medicare will pay for laboratory tests and medical equipment. Beyond political considerations and the in potent but difficult to measure balance of cost versus benefit lie deeper concerns. Is America’s heavy

ordnance on medical reducing the clinical skills of physicians? Is promotion of these4 devices leading the public, patients and physicians to expect too much from medicine and thereby to demand too little of themselves? Mrs. C, had been having pain in her chest for two months, sine her 50th birthday. The pain was clearly angina, produced when an area of heart muscle doesn’t receive enough oxygen. The combination of hospital and drug treatment prescribed by her internist failed to relieve the problem the pain came every time Mrs. C. tried to walk more than a few feet. She was frustrated. “There got to be something else your ca do.” She demanded of her doctor. “I don’t like taking all this medicine and still having pain.” Mrs. C.’s internist sent her to cardiologist who injected dye into the arteries supplying her heart and found tow of them plugged with atherosclerotic plaques with allowed only a trickle of blood to reach her heart muscle. The cardiologist sent her to a surgeon who operated on her heart, removing the two clogged arteries and replacing them with veins taken from her leg. "I've got all new plumbing,” she told me proudly, 10 days after her surgery when she was recovering form a troublesome lung infection, a complication of the operation, “and I feel wonderful.” Mrs. C.’s diagnosis, operation and hospital stay cost Dlrs 35, 401. She left the hospital three weeks later, weak but without chest pain. Four months after her surgery, she was still pain-free but had not yet returned to work. “Have to be careful with my new heart,” she said. Mrs. C.’s case will be recorded and retold as a triumph of technology. But it is also a classic example of our uncritical acceptance of technology as the definite answer to complex problems. Atherosclerotic heart disease is the No. 1 cause of death in the US. An estimated 4.5 million Americans like Mrs. C. are disabled by it. A few thousand of them are able to lead as active life because of surgery. If the arterial block occurs in just the right place, there is some early evidence that life may actually be prolonged by an early operation. But the problem is the contrast between this technologic solution to the atherosclerotic heart disease and the fundamental issue of prevention. A quarter of a century before coronary artery bypass surgery became part of everyday medical practice, the first largescale population studies linking diet to heart disease began receiving attention. This type of evidence, that a diet high in saturated fats and cholesterol is associated with high rates of atherosclerotic heart disease, has been building ever since. But definitive proof linking diet to heart disease has never been found. Therefore as recently as last summer organised medicine reflected as “unscientific” a recommendation, by the senate select commi8ttee on nutrition that Americans should reduce their daily intake of cholesterol to 300 milligrams a day.

Why this double standard for technology? Why is it that Americans accept, on preliminary evidence aloes, a procedure whose average cost is Dlrs 12, 500, while they demand definitive proof that low cost and safe preventive measures, such as diet, work before they adopt them? The same concern is beginning to appear in medical journals. Replacement of medical “gadgetry” was the first of a long list of bicentennial resolutions in the annuls of internal medicine. The Journal of the American Medical Association published an article admonishing physicians to change their ways before they become “servants of machines.” To date there is no evidence that this technologic imperative has reduced the clinical skills of new-physicians. But there is evidence that, as machines replace the physician’s hands, eyes and ears, the ancient art of physical diagnosis and history taking suffers. The ability of a doctor to diagnose diseases of the heart using this hands and ears is one of the oldest and most fundamental of this diagnostic skill. Today, however, while exposed to these necessary fundamentals, a medical student is also taught that his ears combined with his stethoscope are not as sensitive or as precise as the technology available from echocardiograms, which use sound waves to diagnose abnormalities, or a phonocardiogram, which records graphically the intensity and locations of heart murmurs. This technological intrusion meant that many doctors are no longer listening to the patient’s description of his complaints. Today’s physician is especially likely to ignore symptoms that cannot be easily verified by a machine. The results, according to Reiser, are increasing patient dissatisfaction and the gradual loss of history taking skill on the part of many younger physicians. Take the example of a 39 year- old woman recently reported in the Journal of American Medical Association. For a year she had been bothered by a persistent fever. The physicians in charge of her case scanned, biopsies, analyzed, Xrayed and finally even operated on her to find the cause. Only after all of these tests proved negative was she more carefully examined and the site of their fever found to be a blocked and infected sinus. In commenting on the case, Karl Morgenstein, M. D. of the University of Miami said, “recent medical graduates have been taught all the intricacies of computerized technology and radioactive investigations, with intelligent understanding of what they are seeing. But still instead of the million dollars working this lady had over the course of a year, the diagnosis could have been made for less than Dlrs 100.” If reliance on machines has made physicians poor listeners and casual examiners, it has also made the rest of us lazy. An excellent example of the belief that technology can solve all other problems to the case of Mr. Z.

LEPROSY CONTROL IN INDIA Review of the Situation and Suggestions for Future R. S. Sharma Leprosy in India is becoming complicated and difficult to tackle. Specially the management and organisation part of its control work has created more difficulties. The estimated number of 3.2 millions of leprosy sufferers in India is quite alarming. Nearly 372 million populations live in the high & hyper-endemic areas of leprosy which is more than half the populating of India and which is in dangerous zone of leprosy infection.

Total picture of leprosy patients in different categories at a glance — Infectious cases

6,40,000

Children patients under 14 yrs of age

5,00,000

Deformed cases

8,00,000

Beggar patients

2,00,000

Others

6,60,000

Total No. of patients

32,00,000 (All estimated)

But more alarming than this is those factors which have become the focus of serious anxiety. 1. We have lost the enthusiasm which the advent of the patent drug

2.

Sulphone brought with it. The workers, the patients and the organisers – both technical and administrative, have lost faith and self-confidence in whatever they are doing for Leprosy Control. The long and irregular treatment with D. D. S. has produced

drug resistance. 3.

4.

5.

Talk of preventive vaccine is clear indication that we are unable to control leprosy by chemotherapy alone,

alone, which we were stressing upon during these 25 years. But while waiting for a vaccine, which is not very sure in near future due to various reasons well known to leprosy world, it would be another mistake to postpone or relax our effort in combating the problem of leprosy control. Health Education which still is a powerful effective weapon in our armory has yet to be organised effectively. Our main difficulty of ignorance, wrong notions about the disease, and inadequate knowledge in the educated sector is the main hindrance in tackling this problem. Whatever is being done in the name of Health Education is just a fringe or a drop in the ocean. We should be equipped with more material and better methods to evolve a really good work in Health Education. The S.E.T or Control Units have wrongly been described as if they are the only responsible unit for leprosy Control. There is no two options about the importance of D.E.T. or every over their emphasis in this field. But all other work like hospitalisation, training centres, choices, rehabilitation, research and laboratory work etc. should also be considered as part and parcel of leprosy centre work. Is think identifying

control work with S. F. T. has done more harm than good.

The history of leprosy work shows that we are very much prone to specialisation, and isolation. We do not know how to make a team of different compartments and work together in an integrated manner. Even coordination is not achieved between different Voluntary and Government agencies doing leprosy work. 6. The problem of avoiding contacts of positive cases, both early and advanced, specially with the healthy children is very important to control leprosy. We have not thought over this matter and very little has been done on this. I do not think there are more than a lakh of such children living with their contagious parents at home through out the whole country. This being an International child year, we should thing of providing residential schools for these children with educational facilities and boarding and lodging along with other normal school going children. This can be easily done by educating department, which is providing 90% expenses for educational institutions. Health Department, Social Welfare, and Industrial Department should cooperate in this work. 7. It is not difficult now a day to get candidates for training program. Even doctors are available for training. But the content of our training program is very inadequate as far as man, material and equipment are concerned. Our training dies not include motivation apart of it which is more important than mere imparting information. These few training centres would not suffice. All medical colleges and training centres in health a& social welfare and educational field should be involved in leprosy.

NEED FOR A TOTAL CONCEPTION LEPROSY CONTROL 1. Early detection of all cases through mass survey and by other means. 2. Early and regular treatment on a mass scale with dapsone and other chemotherapeutic drugs.

3. Prevention of contract with infective cases. 4. Protection of children from contract with infective patients. Necessary means to achieve these objectives in order to get success in the above stated four measures:i)

Health Education of the population to make them adopt a rational attitude towards the patient and the disease.

ii)

Education of patients for regular treatments and avoiding complications

iii)

Training of persons doing leprosy work as all levels.

iv)

Adequate training in leprosy in medical colleges. (Cont. on page 6)

Child psychology and education

PEEP IN THE CHILD’S MIND PADMAJA RANI "Every new child comes to this world with the message that God has not yet fed up with man.” When the renowned poet Rabindranath Tagore uttered these words, he tried to express in the most beautiful way the feeling of love and hope connected with the arrival of a new child in this world. For everybody at home, specially for the parents, a new dawn of endearing hope and loving expectations starts. Sensible parents will certainly want their children to be happy, healthy and fully developed as well. When we say the child should be healthy and happy, we mean, he should he healthy, physically and mentally as well. Mental health implies the full development of the child's whole personality. Obviously the parents get very much worried when their children become ill and doctors are readily consulted that is if they can afford it. But how many parents would equally be worried and careful about the mental health and development or their children?

Child's maturation: role of Innate trait:The child grows up and in that process develops new capacities and interests so what at each stage or development he is a different being, a new entity. When psychologists define maturation as ‘the unfolding and ripening of the abilities, characteristics, traits and potentialities present at birth but only later coming to maturity in a developing organism' they mean the behaviour patterns are innate and follow one another in specific order. Thus in a child, many capacities such as walking, talking and numerous other activities depend on its innate aptitudes and not on training. Any amount of practice or training will not help to teach a child those tasks for which the innate disposition is not already present or the particular stage of development is not reached. Only it can rake advantage of training and practice when the optimum period in the maturation process is started.

The role of environment :- In spire of this sort of supreme importance of the maturations process irrespective of environmental conditions, child psychologists have always emphasised on the important role of environment also in encouraging or retarding the development of a child. The mental health and development of a wholesome personality in the adult life of a child depends upon the factor how he starts off in the beginning of the game of life and so the first four years of life, when predisposing causes of neurotic disorders can easily be originated, are very crucial for a child. Now a days at a very early and tender age many children are sent out to school, so that they become ‘smart and successful’ according to the social norms. Thus the parents at home, is cheers at school and the society at large with which the child personality development.

The psyche of the child:- What goes on inside a child during these crucial and formative years? What are the main characteristics of development that occur in him? The various phases through which the child passes can be enumerated like this: Age of the child 1st year

Characteristic —

helplessness,

dependence,

control over body movements and sensuous pleasure 2nd year

— self display and desire for attention,

curiosity

and

exploration, imitation, self-will 2-4 years

—

imitation,

suggestibility

identification, formation of egoideal The characteristics of the 1st year of life are all centred on the infant itself. But in the 2nd year it goes out to meet the world. It begins to call attention to itself in self display. The child, in its efforts to gain people's attentions discovers what people like and what they disapprove. In the phase or exploration and curiosity he investigate each and everything’s inquisitively. In contrast to the earlier phase he is more outward going extrovert, concentrating in the outside world and not on himself. The spirit of curiosity changes it's forms as the child develops. About the age of two or three it takes the more intellectual form of asking questions like "what is this' and 'what is that'. By the age of four he is more concerned with knowing the "how' and 'why' of things. The phase of imitation also follows close by. Of course the child is not taught to imitate and so the propensity of imitation is innate. But what the child imitates depends on environmental influences. Though primarily a subconscious process, it is a very effective way of learning things. Avoiding dangers, learning new ways of doing, even learning social graces are all achieved through imitation. The age of two is the age of self-will. The child insists on having his own way. Child is beginning to learn to control the world in which he is to live and shape his own life. But this aggressive self-will does not continue into the adult life in it’s primitive form. Aggressiveness develops in to self-confidence where self-will completely dominated by his impulses, is transformed into the will-power. In suggestibility phase, the child takes over not only the actions of others, but the moods, feelings and ideas also of around him. In identifications the child going further takes over the entire personality of the other person and his own personality is absorbed within it. In the egoideal phases, the child having taken over the personality of others with whom he identifies himself, now accept their attitudes as this own, thereafter he has got a guiding principle within

his own personality by which to control his life and conduct. The healthy ego-ideal would be capable of utilizing, directing and coordinating all the energies and capacities of the personality.

Right recognition of the phases of development:Once the educators whether parents or teachers, learn to recognize these phases and their importance in a child’s life they can give the fullest opportunity for his development in each phase. Self display, being one of the earliest sources of social behaviour in the child, should be allowed full expression and encouraged to direct it in desirable forms. The child who does not get enough recognition and attention from other would become arrogant and antisocial. In the phase curiosity and exploration, the child needs plenty of material and scope of his curiosity. He needs help to find out things for himself. His natural and spontaneous thirst for knowledge can effectively be made use of to teach him the things he needs to learn. The child’s propensity to imitate and his suggestibility should be allowed the fullest scope, being careful ourselves to be and do what we want the child to be and do. The self willed child should not be represent but right outlets for the aggressiveness should be discovered so as to develop a strong character and will in him. Thus, if only the parents try and develop the right attitude, most of the things a child is forbidden to do can be allowed without any great harm to anyone. The child develops moral standards of right and wrong by the process of identification and the mental health of the child depends more than anything else on the nature of the ego ideal he adopts. All these facts show the importance of environment in the development of a child. The potentialities in the child’s nature are expressed and developed in the through the medium of the environment, which determines which of the child’s basic potentialities are developed in which direction and which are repressed.

The children are afraid, bored and confused:A child has an infinite capacity of learning. It is said that hardly an adult in ten thousand could in any three years of his life learn as much and grow as much in his understanding of the world around him, as every infant learns and grows in his first three years. But this tremendous capacity of learning and intellectual growth is crushed and the personality and individuality of the child killed by the process of mis-education going on in homes and schools by the things we do to them or make them do. Severe remarks, threats, punishments and discouraging form necessary components of our education. Good children are those who fear the elders remain dought do not embarrass the parents and teachers by asking unnecessary questions (many times they might not be solving the answers), always are obedient and hence they are told (and do not use one’s own imagination creativity and initiative.) Thus almost all children in the house that they fail to develop more than the small

part of their capacity for learning and creating and this failure goes unnoticed. John Holt in his book “How Children Fail” analyses the courses of this mass failure in very touchingly honest words. “They fail because they are afraid, bored and confused. They are afraid of failing, disappointing or displeasing the many anxious adults around them, whose limitless hopes and expectations for them hangover their heads like a cloud. They are bored because the things they are given and told to do in schools are so trivial, so dull and make such limited and narrow demands on the wide spectrum of their intelligence, capabilities and talents. They are confused because most of the torrent of words that pours over them in school make little or no sense and is far away from the world of reality.” The child bas the right:- According to the UN Declaration of the Right of the Child, the child is entitled to grow up in an atmosphere of affection, understanding, friendship and moral security. He is also entitled to such educations as is in his best interests for which the parents are to be responsible. In India alone there are 230 million children under 14 years of age. Without proper parental education, their attitudinal changes and a radical analysis about the process of so called 'education' going on in schools and homes, how are we to ensure these rights for each one of these 230 million children in our country in the International Year of the Child and many more years to come?

*

*

*

Living Arrows Your children are not your children They are sons and daughters of life’s longing or itself They come through you but not form you And though they are with you yet they belong not to you You may give them your love but not your thoughts For they have their own thoughts You may house their bodies but not their souls For their souls dwell in the house of tomorrow Which you can not visit, not ever in your dreams You may strive to be like them, but seek not to make them like you For life goes not backward, no carries with yesterday You are the bows from which your children as living arrows are sent from Khalil Gibran

(Cont. from page 3) v) Providing adequate number of fixed and mobile outpatient clinics for the treatment. vi) Providing adequate in-patient accommodation for treatment of acute complications of leprosy or other disease in leprosy patients. vii) Providing adequate accommodation and necessary facilities for selective short-term segregation of highly infective leprosy patients whether early or advanced viii) Total health care of leprosy patients with regard to all minor ailments. ix) Welfare activities for the patients including rehabilitation training, shelter work shop for infirmed, mutilated, burnt out cases. x) Protection and care of children of leprosy patients. The talk is difficult. This cannot be done either by Government or by Voluntary Agencies alone. We have to put up combined efforts in this field. If we all agree to the basic approach as briefly mentioned above which is not exhaustive, but only indicative, then detai1s can be worked out. There is a need of a Central Statutory Board of Commission for Leprosy Control in our Country. This Central authority should be given powers not only advising and planning the schemes but should be made responsible for implementation also. Voluntary and Government representative should be included in it. It has been a privilege for the country that Voluntary Institution of a good reputation have done pioneering work in leprosy field, of which we arc all proud or. But after Independence not many more new voluntary workers or institutions have con forward in this field. This is mainly due to the wrong notion that new everything has to be done by the Government. There is no denial of the fact that the National Government has a major responsibility to tackle such problems on a national basis. As we all know by experience that without people's participation, motivation and zeal behind the work, which is lacking every where, all our effort, are ineffective. We have more provision of fund and it has been told that money would not be lacking factor in leprosy work. It also indicates that personnel of vision, conviction and passion are needed. Well informed action is lacking. Now, how to attract and prepare people of the type we need is the main problem before us. This cannot be achieved by today's pattern of advertisements, appointments and training. Let us all think over this matter seriously and fine out the way out of the present situation.

MFC NEWS The MFC group of Sevagram Medical College, which was

already working in the village Nagapur, has increased its activities to include one more village Mandogarh. The institution ahs appreciated the work done so far and has extended support by providing conveyance. These interested interns have also been involved in the work.

OPERATION MEDICINE On the Path of Agitation

Faulty decisions, by the Central Health Ministry and Chemicals and Petroleum Ministry have cost the consumers about Rs. 1000 crores. In the past few years and the nation has lost around Rs. 300 crores. Mr. Bindumadhav Joshi, President of All India Grahak Panchayat Samiti, said here today. What is more that the recent decisions take by the two ministries would enable the foreign drugs companies to fleece the people to the tune of crores of rupees by a wide loophole in the rule, he added. Addressing a press conference Mr. Joshi laid that the Hathi Committee had suggested some very important change in the Drugs and Cosmetics Act, 1910. Some of these recommendations were being implemented and the G. R. in that connection was to be effective from 1st, June 1979. The Centre had in consultation with the Drugs Technical Advisory Board amended the drugs and Cosmetic Rules 1945 and the Schedule V to it and laid down that the "Forte" formula should not be used for manufacturing of Vitamin B complex pills. It was also laid down that the manufacturers should follow the National Formulary of India. This would have resulted in a reduction of B complex pills from 50 paise to 10 paise per pill, as implementation of the rule would have eliminated the excess material in them which was not absorbed by the body but was being eliminated by it as waste in urine. But this wholesome restriction which would have helped lakes, of patients has been completely done away with by the Union Health Ministry by a foot-note which grants an exemption to drug companies especially the foreign one. The foot note say that the above standard shall not apply to single vitamins only Of preparations for parental use (injections) and empowers local authorities to grant exemptions to drug companies from the operation of the rule. This Mr. Joshi stated would open large doors for corruption and negates the en tire purpose of the restriction. Foreign drug companies would get Rs. 70 crores from Indian customers as a result of it. The Union Health and Chemicals Ministries seem to have the interest of foreign drug companies in their mind than that of the consumers. The exemption must be with drawn as well known authorities in the field of medicine like Walter Model of Drugs 78 have expressed clearly that excess vitamins act as a sort of poison in the body and they are eliminated by it..

The Grahak Panchayat has decided to launch an agitation against the unwarranted exemption. It would press for it in deputations on drugs controllers in 150 cities. He urged doctors and their associations also to support the movement in the interest of the patients. Nagpur Times, 10th April

Dialogue To a Soul 'In Search of Utilisation’ In answer to Dushyant Puniyani’s letter (MFC bulletin No. 38), a few cobwebs needs to be cleared away at the outset. The discussion in the Varanasi conference was intended primarily to bring out the fact that the structure of the health services makes inevitable the defective utilisation of trained technical manpower. Certainly, the MFC has no wish to project the setting up of private practices in the rural areas as a solution to the problem. Moreover, I am sure Rani

Bang would join me in discouraging people from giving up their jobs hastily in a misconceived search for social relevance. As Dushyant himself points out, there is plenty of scope for a lecturer in KEM hospital to structure his work to accord with his social commitment. What is necessary, however, is to undertake a rigorous sustained analysis of the social basis of health and disease, and to determine one’s own role in the light of this analysis, with an honest awareness of the potential scope and limits of such a role. It is here that Dushyant’s letter strikes what I feel is a disquieting note. If one starts off with assumptions such as “I do not feel like being a pioneer….” Or “How can it be expected that people like me…..” etc, then one has admitted defeat before even starting the battle. In my view an activity that is very relevant in this context is the consolidation and organization of the voices of protest within the health professions. The MFC offers plenty of scope for people interested in this work. Would Dushyant consider it worthwhile to strengthen actively the growing MFC group in Bombay?

done. This can give new vision and can also enable to crystallize one’s views on community health work. This can also encourage one to jump on another type of programme i.e. starting one’s own health project as many MFC members have done and about which Dushyant is so hesitant at present. The best solution to his problems, within his given situation, can be better visualized in his letter itself when he says “the problem of deprived child is not confined to the rural areas." This is very true and so he can continue to be in Medical College itself and select a slum area in Bombay as a representative area of oppressed people and try to work there with a group of students from his own college or Bombay MFC group. This is quite practical for him and is being done by Ulhas Jajoo and Luis Barreto in Sevagram Medical College and Kartik Nanavati in Ahmedabad Medical College, where they are lecturers. This can also give one more practical experience and grip on community health work and can also help one ill understanding the social aspects of health and health problems. But one thing I would like to ask when he says, that "I am looking for an ongoing programme in Maharashtra which would utilise my services to look after their paediatric problems.” Why does he want to restrict himself to Paediatrics? In the course of work and experience will he not try to solve other problems of community health, not necessarily paediatric or medical? To have proper tuning with the community work, his mind should be so prepared before hand only.

Rani Bang Gopuri, Wardha.

Binayak Sen

II

Rasulia (M.P.)

I am tempted to reply Dushyant Puniyani’s letter, (Dialogue, MFC Bulletin, March 79) as my reference was made in it. I think nobody is an authority in MFC as to ‘guide’ someone else; but from my own experience should like to apply my ideas to his situation and problems, which I suppose, likewise may be faced by many other MFC members. As regards to his problems, certain questions are cropping up in my mind. When he says, “let me make it quite clear at this point that I do not feel like being a pioneers and setting up my own community welfare center- nor do I consider going to an affluent rural set up on weekends to salvage my conscience a means of community medicine,” will he make clear what are his views on community health work, its’ objectives and methods to achieve them.? To find answer to his question, how to work and where to work he can visit various health projects run in India and himself see how the work is being

I.

INTERNSHIP AND YOU Suhas Jajoo (December 78, MFC Bulletin and now B.B. Gupta have voiced the problems they faced during internship. The internship is rife with such experience; every medico masses through this traumatic experience some time in his/her career, specially during rural internship posting,

MFC Bulletin invite« the readers to write their experiences and thoughts on the topic given bellow; The 'article should not exceed 750 words. The best 3 articles will be published in the Bulletin. The last date for receiving the article is 7th June 79. ‘The rural internship: does it succeed in orienting medicos for rural health work? Facts and the factor responsible for them.’

(Cont. from page 2)

Every morning, Mrs. Z. came and sat in the wailing room of the hospital's intensive care unit. Every hour, for five minutes, she was allowed to stand by her husband's side and hold his hand. Mr. Z. did not respond to his wife of 60 years. A 70, he had a long history of heart and lung problems and had been an invalid for the last five years. While eating dinner one evening he collapsed and was rushed to the hospital in the coma. The stroke left him paralyze" and unable to breathe. A machine corrected the breathing

Editorial committee: Imrana Qadeer, Ulhas Jajoo, Binayak Sen, Anant Phadke, Ashwin Patel, Abhay Bang (Editor)

problem. But Mr. Z. never regained consciousness. After two months. Mr. Z. stopped waiting for her husband, and went to live in New York. Three weeks later Mr. Z. died. Not every care is as dismal. For some patients, technology can provide a reprieve or a respite from pain. But for many, especially those at the limit of our knowledge like Mr. Z, the wonders of technology can only do harm by making death impersonal.

(Courtesy 'Washington Post')

Views & opinion expressed in the bulletin are those of the authors and not necessarily of the organisation.


