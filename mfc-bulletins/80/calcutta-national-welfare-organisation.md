---
title: "Calcutta National Welfare Organisation"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Calcutta National Welfare Organisation from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC080.pdf](http://www.mfcindia.org/mfcpdfs/MFC080.pdf)*

Title: Calcutta National Welfare Organisation

Authors: George Thomas

medico friend circle bulletin AUGUST 1982

High Cost Medicine A mother entered the out patient department of a hospital situated deep in a tribal area of Chandrapur district, carrying her 3 year old child. The child was burnt around knee joint, the previous night. To save themselves from severe cold, mother and child slept near the fire and the child accidentally put its leg over the fire. Mother had to walk 10 miles to reach this hospital. The doctor talked sympathetically and applied a bulky dressing around the joint so that it should not slip. An hour later, mother was back with another child. The first child walked in with her. The dressing was missing. The doctor could see that all his efforts were in vain and started scolding the mother for her neglect. Doctor attributed this behaviour of the mother to the free services offered to them. According to him the patient and the mother did not deserve the compassionate behaviour bestowed on them. He refused to examine the other child. He asked the mother what she did with the big bandage. Mother went out and was back within a minute with two LANGOTI ES (underwear’s for the child) made out of the same cloth. Doctor could not help than to distressfully look at the naked child standing before him.

I was an observer of the whole drama. There was a real challenge-the naked poverty and the real priority needs of the people. If one wishes to take medicine to 'THE PEOPLE' one must consider whether it is within their limits to bear. A socially conscious doctor thus will start thinking of ‘low cost medicine' - effective, cheap and appro-

priate in the existing socio-economic situation. A series of questions start as to why allopathic medicine is so costly today. We will try to go through them.

Profit motivation of the drug industry:

i) Do you know what is the difference between the production cost and the market cost of Tetracycline capsule? (6 to 8 times). ii) Can you guess the extent of overpricing a multinational drug company can force on the local company to import its raw materials? (See appendix- 1). iii) Why the same drugs under different brand names have marked difference in Consumer price? iv) What is the motivation behind the gifts offered to medical practitioners? v) What do you feel is the reason for lavish samples that the drug representatives offer? (Hathi Committee Report 1975 "The scale of sampling has been lavish and has a particular significance in India since majority of general practitioners dispense the drugs they prescribed and may therefore charge their patients for drugs they have been acquired free."). vii) What is the drug representative / doctor ratio in our country as compared to western countries? Why?

Ulhas N. Jajoo Bajaj Wadi, Wardha, 442114

Drug Representative Underdeveloped 1: Country (India) Developed countries 1:

Doctor Ratio 4 30

vii) Why drugs banned in Western world arc still being sold in our country? E.g. Phecnacetin, Amidopyrine, Analgin, Lomotil, Depo-Provera; Proluton Depot. etc. viii) Why substandard and outdated drugs reach underdeveloped countries for marketing? ix) Do you feel that all the drug combinations in the market have a rationale? Experts have listed 100 drugs as essential. There are 30,000 drug preparations in Indian market. Sub-committee of the drugs consultative committee has recommended a list of fixed dose combinations to be weeded out from the market.

a) Having to wake up a child more than once in the morning b) Child unwilling to go to school or leaves pencils behind. c) Child complaining about other children at school. The drug companies want consumers to believe that with one vitamin-forte every day, he will stay active and alert all the day; tiredness, irritability, lack of concentration are all symptoms of vitamin starvation, which if neglected can get worse.) - (Vitamin C lessens smoking hazards!) - (Gripe water helps infants suffering from convulsions!) xv) Have you noted double standards in instructions?

Migril x) Do you feel all drugs available in the market today have proven efficacy and safety? xi) How many genuine new drugs come in the market every

year? Axe these imitations modifications of the existing drugs?

with

slight

("Drugs & Third World" 1974 quoted out of 1500 drug patents introduced in the year, 3% were genuine new drugs, 10% were major modifications % and 87 were purely imitative drugs). xii) Have you studied the fluctuations in the market price of Gentamycin over last 10 years? Why so? (1969 - 35 Rs. per 80 mg vial (1980 - 5.80 Rs. per 80 my vial) xiii) Why does drug industry generously support medical seminars and conferences?

Without their financial help do you feel these conferences can be held with same vigour? xiv) Have you noted the ways in which drugs are advertised? A Vitamin forte advertisement lists symptoms that are associated with Vitamin starvation like -

Ancoloxin

Country

Maximum weekly dose

U.S.A. U.K.

10 mg 12 mg

Africa/Asia

24 mg

(Meclazirie HCI 50 mg)

U.S.A. - Not to prescribe during pregnancy in view of the teratogenic effects of the drug in rats. U.K. - Undesirable during first trimester of pregnancy, the administration of Ancoloxin may be warranted if vomiting is severe. Africa & Third world - Primarily indicated 111 the treatment of nausea, vomiting of pregnancy. xvi) Do you know that the brand names are usually written in bold letters? Caution and toxicity in small letters which are read five times less than the headline? xvii) How do you feel about the following sales promotion methods? -

A prize of 10,000/- Rs. to a practitioner who prescribes certain number of prescriptions in fixed number of months or prescribes only one company product.

-

For every 100 boxes of a drug ordered, 20 boxes arc given free of charge (Bonus with purchase).

-

Given a 'cut' for every 100 prescriptions of a certain brand. xviii) How about these cultural activities?

-

Chocolate Companies show films on Cocoa growing in schools & run essay competitions.

-

Radio quiz or sponsoring athletic events.

-

'Beautiful baby contest' to promote sale of infant formula. Entry to these competitions is typically restricted only by number of bottle tops or wrappers. Draws are often televised or broadcasted and reported in press.

xix) How much you can think the packaging cost can be? (Packged food - Ingredient - Packing - Labour

56 % 33 % 11 %)

xx) How do you find this statement from a Marketing Manager- "Advertising does more than merely sell products, it informs, educates, changes attitudes and builds images? What do we sell? "Never a product, always an idea." The function of advertising agencies is to seek to influence human behaviour in ways favourable to the interests of their clients or to "indoctrinate" them. xxi) Have you heard of "Drug black-mail?" With 85-90% of drug patents in developing countries, multinational firms can virtually hold third world countries with inadequately developed resources, to ransom. Thus alpha-methyl-dopa can vanish from market. Scarcity can develop or essential drugs like anti-tubercular and anti-leprosy drugs. Who has heard of scarcity of tonics, Vitamins and cough - cold remedies? Too many investigations: Open up any book of Modern Medicine, you will find a big list of investigations. Try to calculate the cost of these investigations. I am sure, in majority you will find costs exceeding the cost of treatment. Are all these investigations absolutely

necessary? In our con text don’t you feel simple blood sugar estimation, X-ray, an ECG, is also a costly commodity? What are the criteria to guide us, before a patient is subjected for costly investigations? Will you like to ask the following questions before sending the patient for such investigations? a) Will it alter the management? h) Will it help in proper and definitive diagnosis? c) Can patient/society afford it? d) What is the risk involved? Do benefits overweigh the risks? Will you justify a battery of investigations, (i) Without a thorough bed-side examination? (ii) Without considering sensitivity or specificity of the diagnostic tool? (iii) For research purpose which is totally irrelevant to our health priority? I am quoting a few examples, which a student of medicine can scan through the literature and reach his own conclusions-

DO YOU FEEL THAT a) Each young hypertensive must be investigated thoroughly with IVP; orthography; Urine plus serum rennin levels, scanning; V. M. A. estimation etc.? b) Each case of viral hepatitis should be subjected to all the liver function tests? c) Every case admitted in medicine with cough need to undergo routine X-ray chest? d) Each case of head - injury should undergo computerised tomography for the diagnosis of subdural haematoma? e) Blood-gas analysis is necessary in the diagnosis of chronic obstructive airway disease? f) Ultra-sonography is much more useful than oral cholecystography in diagnosing cholelithiasis? g) Each case of massive upper G. 1. T. bleeding must undergo emergency fibre-optic gastroscopy? Overshoot by the doctors:

-

Do you feel acute diarrhoea in adult always requires antibiotics?

-

Do you feel upper respiratory infections need to be covered with antibiotics?

-

(In India an estimated l2% of all prescriptions for antibiotics are for common cold).

-

Don't yon feel corticosteroids are generally overused and misused?

-

- An extensive peripheral health infrastructure cuts down total expenditure on health.

Do you feel that some-times surgery is underta-

ken intentionally when not indicated e. g. Tonsils, Uterus, Appendix...... (According to W. H. O. there arc 75 ways doctors misuse drugs. Commonest is over use of drugs. - Too large quantities - For too long duration. - Entirely unnecessary drug. - Too many drugs at the same time. Medical Corruption:

- Have you heard of surgeons asking for money after putting the patient on operation table? - Haw you heard of bribe being routinely given [or admission in a T. B. Hospital? - Have you heard of links between the" Drug Shop Owners" and the doctors? - Have you heard of "Rings" that the doctors have in big cities? - Do you feel the results of drug trials published in the medical journals or quoted by drug sales representatives are unbiased? (Clinical trials of new compounds conducted by physicians doing clinical trials in, 1973 whose work were spot checked by the food and drug administrations were guilty of a range of unethical practices, including giving wrong doses and falsifying records. In lj3rd of all reports submitted, the trial was never done at all, in another 1/3rd it did not follow the manufacturer's protocol and only in final third were there results of any scientific value.) Costly Hospitalisation:

This includes- Doctor's salary, salary of nursing staff; hospital management and maintenance. Few facts to note- Bed occupancy of private hospitals in India on average is 40-50%.

- Much of the doctor's responsibilities can he handed over to less skilled but trained staff. (To be conduced in the September issue.) Appendix - I Product Multinational Quotation Percentage import price to more of over to Colombia developed pricing (US dollars) countries Chlordiazepoxide 1250 18.90-20 6,155 Diazepam 2500 30-45.55 6,478

Metronidazole

390

11-15

3,398

Hydrochlorothiazide Tetracycline base Promothazine Indomethacin

90 250 140 640

5.2 23.5 19.7 72.5

1,530,7 948 654.3 611

ATTENTION PLEASE The campaign on diarrnoea Please send me cuttings of articles published in the lay-press through your efforts on importance of oral rehydration, proper diet in diarrhoea and misuse of drugs in diarrhoea. Please inform us about the response of the people to this mass-educational campaign that we have launched. This information is necessary to plan the future action on this issue at the coming midannual Executive Committee meet of MFC at the end of this month. This meet will be followed immediately by the drug-issue-meet a VHAI-MFC collaboration. SAHELI, a Women's Resource Centre in Delhi and Voluntary Health Association of India have jointly prepared a poster warning women not to take any tablets or injections to confirm the suspicion of being pregnant. The text points out that these drugs may cause congenital defects in your baby, that this drug-test is unreliable and that these drugs can not induce abortion. The text it in Hindi and figures of a pregnant woman, a deformed baby and tablets, injections renders the poster useful to semiliterate population. Printed copies are available at Saheli, 10, Nijamuddin (East), New Delhi -14, and at VHAI, G-14-, Community Centre, S. D. A. New Delhi-H) at Rs. 1/— per piece. Bulk orders will get a concession.

KEEPING TRACK-I Dear Friends, The year 1982 h as been a year of travel for us, giving us an opportunity to meet friends and share experiences and perspectives- Meeting many who arc responding creatively to the problems and issues of Health care in the field has been a rich experience. At a meeting with Anant in Pune, in April this year, we discussed the need for most of our members to keep in touch with the growing medical literature on the issues relevant to the MFC quest. Though the bulletin has tried to keep the members in touch with ideas in the field, many would like to get to original sources. I have agreed to start a column entitled 'KEEPING TRACK' to enable MFC members to do just that- keep track of thoughtprovoking and interesting books and reports, easily available for one to read. The column will present notes and extracts from these books not with a purpose of reviewing it; within the context of any well defined MFC perspective (which is still being gelled!) but as all indication to readers of the scope and message of the book. Readers are invited to read and react to the generalisations of the authors by virtue of their own field experience. It was proposed that a few copies of some of these should be available to? MFC members as a sort of circulating library. While this can possibly be considered in our future meetings, we start off this column with two books on the Indian Health Scene. An Alternative System Of Health Care Sen-ice In India:

J. P. NAIK: Alternatives in Development Series, Indian Council of Social Science Research, Allied Publishers, New Delhi, 1977: Rs. 10.-00 This booklet presents three articles by J. P. Naik, D. Banerji and Jacob Chandy--three pioneers who seriously question the philosophical framework and organisation of health care service in India and raise issues and alternatives. It also presents important extracts from the Report of the Group on Medical Education and Support Manpower (Srivastava Report-see Bulletin No. 21). These articles place in perspective the growing national debate

on alternatives TO the existing system which Dr. Ramalingaswamy (Director General, ICMR) has described as "over centralised, over-expensive, over professionalised over-urbanised and over mystified" J. P. Naik raises issues regarding target groups, emphasis on Preventive and Protective aspects choice of technology, agents of health care, infrastructure, drugs, involvement of the people and educational aspects in his oration on the alternative system Professor Banerjee categorically emphasises that formation of an alternative is essentially a political question and makes a plea for greater democratization of the political system. This will "subordinate medical technology to the interests of the community; it will be demystified, deprofessionalised, debureaucratised and decommercialisation." Professor Chandy makes a plea to consider Health Science as distinctly different from medical science and introduce it as a compulsory part of the school curriculum so that there is "a scientific awareness created through formal education for the attainment and maintenance of health." Alternative Approaches To Health Care: ICMR (1978) (available on request gratis from I CMR, Ansari Nagar, New Delhi 110029). A Report of an ICMR Seminar held in Hyderabad in November 1976 and includes fourteen long case studies and a few short case studies on health projects in India, mostly in the voluntary, nongovemmental sector. Also it includes a review of the issues raised by these projects and delineation of areas of further research. An important report since it is the first one in thirty years where the Indian Council of Medical Research accepts Health Care Delivery as an important area of research, Important for MFC readers to sec these case studies, not as alternative approaches in themselves as portrayed by the project-wallahs but as raising issues which could be part of an alternative strategy in an alternative sociopolitical milieu!

Ravi Narayan

Calcutta National Welfare Organisation The Calcutta National Welfare Organisation (CNWO) has been in existence for approximately two years now. On 13th March 1982, the organisation hosted a seminar on “Rural Health Problems and the Role of Doctors". In the course of the seminar, and in conversation with the organisers and some members of the organisation, the following impressions were gained of their aims, objectives and methods. The organisation arose out of a need felt by students in N. R. Sarkar Medical college and R. G. Kar medical college to identify the flaws in the medical system in the country. Some of them felt that most members of the medical profession knew that our health care delivery system fell far short of delivering the goods, but the exact reason for this failure was not apparent. They therefore resolved to set up a forum for study and action in this direction. The result was CNWO. With the opinion that action would be an educative experience and also would point towards solutions to problems that appeared, two medical centres were set up in peripheral areas of Calcutta. Drugs, manpower and other requirements for these centres were obtained by the students, own efforts. These centres differed from the Lions and Rotary club affairs, in this aspect- the students who went there went with the understanding that the medical centre served a double purpose; for the medical personnel, it highlighted the problems in delivery of medical care and demonstrated how closely health care was linked with social, cultural, economic and political factors; for the people it was a needed service. Because of the dedication of the members, their willingness to work in all weathers and in the most adverse circumstances, they were able to attract many members. A conscious effort was made by the members to

involve all levels of medical personnel in their activity, so that everyone from students to professors could participate in the learning experience, and contribute to the solution of problems

identified. Efforts are also being made to unite all streams of medicine existing in the country on a scientific basis so that a verified and effective 'health care delivery system can be evolved,' and the existing mistrust and hostility between different systems of medicine might be removed. CNWO hosted a seminar referred to above on 13th March 1982. The seminar served at least two purposes. Firstly it served to focus the health problems existing in the rural areas, and some of -the difficulties that arose when trying to solve them through governmental agencies and, secondly it enabled the organisers to meet" people with similar ideas from other parts of India. The organisation also launched a magazine called 'People's Health' to coordinate the activities of similar organisations already existing in other parts of India, to keep in touch with such organisations and also to serve as a forum to debate the problems in health care delivery. The address of the organisation is; Dr. D. Dutta, Sec CNWO 8, Raja Gurudas Street, Calcutta 700 006.

Thomas George (Madras Medical College)

It is sad that we were not aware of CNWO. This reflects the deficient communication amongst unorthodox groups. We hope that MFC and CNWO would conic together on activities of mutual interest.

- Editor People's Institute for Development and Training is in need of an interested lady-doctor to run a healthprogramme and health training in Shahdol district of Madhya Pradesh where one of their spear-head teams in working. The health programme has definite social, economic and organizational overtones. Those interested should write to Prof. Subhachari Das Gupta, P I D T, 2 Kaushalya Park, Hauz Khas, New Delhi-16.

*

DEAR FRIEND Marian in his letter (see MFC Bulletin 76) makes the following points while commenting on MFC ill general and the bulletin and the Meet in particulara) MFC is not unique in its existence and structure. b) Bulletin does not fulfill its role completely. c) i) Meet also does not fulfill its purpose because people discuss some topic "independent of its social demand." ii) Meet docs not end in a positive action · programme. I would like to respond to these one by one. a) MFC is unique in its existence and structure because it is the only organisation of its kind comprising of medicos and paramedicos who are socially conscious, antiestablishment and who are seeking alternatives to this dehumanised and irrational system of health care. (Compare this with several other medical associations.) It is unique as the only voice heard from within the medical profession in favour of an egalitarian, community based and rational health care system amidst the cacophony of anti CHWs, pro-multinational and other elitist pronouncements by other medical organisations I'd just like to point out here that it was Abhay Bang of M FC who pointed out the unscientific bias in Page Committee's report on Minimum wages and not any association of nutritionists; it was MFCVHAI who campaigned against misuse of EP forte; not the IMA. It is in this context one can and should be optimistic about MFC's development and progress (I think Manan contradicts himself here, by pointing out on the one hand "the potential of MFC to serve a purposeful role" and asking in the same breath what there is to be optimistic about?) If he means the real potential, one which exists in reality and not the ideal one-which should be-then, that exactly is what should be optimistic about, for what else is the source of this potential if not the progress so far? b) MFC being an organisation of socially committed medicos, the bulletin thereof as Anant rightly points out has to act as a, propagator of its commitments to alternate health strategies, rational therapeutics and socially relevant views on health,

besides acting as a medium of discussion amongst its members. Only a polemical organ of a political organisation can have the sole purpose of being only a medium of debate which the MFC is certainly not, its 'political' role being limited to providing on the basis of scientific evidence, issues in the field of medical care for political action. c) (i) This comment of Manan's on the themes of the MFC Meet is totally baseless. In every Meet, the subject to be discussed was chosen for the precise reason of it being socially relevant (e.g. misuse of drugs, role of CHW etc.) Manan should have, before coming to any conclusion on this point, examined the themes of all the meets and pointed out which ones were really" independent of social demand". (ii) Considering the fact that MFC's primary role is not that of an activist organisation and also that most of its members do not have MFC as their No. 1 priority, the action programme decided at Tara (Campaign against misuse of drugs in diarrhoea), was in my opinion, adequate. Whether the actions are carried out is to be seen. This does not mean that I do not concede any shortcomings in MFC's functioning or lethargy in the members. 'We do need to take another look at the way in which we function, the way in which we conduct our discussions and also the priority we attach to MFC. We still need to be more disciplined in the matters of discussing topics, deciding on a programme, and most important on implementing it. Except a few members, we still do not attach significance to contributing to the Bulletin. Actions decided upon at the Meet are not followed up properly. All these need to be rectified before MFC's potential is truly realised. We also need to form a common perspective on certain fundamental issues like the scope of Community Health projects in improving the quality of peoples, lives, or criteria by which a Community Health care system is to be evaluated, In this respect, agree with Manan that the meet will be of little value "unless one finds a common ground leading to action" The potential exists and the time has come to concretise it? Dhruv Mankad, Nipani

FROM THE EDITOR'S DESK Banning Hormonal Pregnancy drugs-only a partial victory

The Times of India of 1st July carried heartening news for us. - “The Government has decided to put a total ban on the manufacture and marketing of all the pregnancy testing medicines in the country.... The decision to ban the fixed dose combinations or oestrogen and progesterone, prescribed only for the indication of secondary amenorrhoea and similar gynaecological disorders has been taken in consultation with medical experts in view of reports of large-scale misuse of these preparations for the termination of pregnancy." Though the Govt. may not admit it, the campaign against the misuse of these drugs by Voluntary Health Association of India, MFC, Arogya Dakshata Mandal, Peoples Science Movement and some journalists can proudly claim that their efforts, initiative built up a pressure on the Govt. to take this decision. But as usual, even the first announcement was marred by a clause —“The stipulated cut-off date for the manufacture of these drugs has been fixed as December 1982 and cut-off date for sale in the market – June 30, 1983.” Why this delay, time lags of almost a year before these potentially dangerous drugs stop reaching the consumers? The official news release has accepted the dangerous character of these drugs by saying. ''The use of these drugs in pregnancy could lead to the birth of babies having congenital abnormalities, experts say." But even then the ban does not come into immediate effect. This is perhaps in deference to the concern of the drug manufacturers that they will sustain losses. Does the concern for their losses over-ride that for the newborn babies who may be born without limos or with congenital heart defect? Within a fortnight of this first announcement, the deputy minister of Health, Miss. Kumud Joshi has announced that there is no ban on the use of

Editorial Committee:

Anant Phadke Christa Manjrekar Ran Narayan Ulhas Jajoo Kamala Jayarao EDITOR

drugs for pregnancy tests................. at present hormonal products were indicated only for secondary amenorrhoea not due to pregnancy........ The firms marketing these hormonal products had also been directed in March 1982, to include the warning reading as "not to be used for pregnancy test and in suspected cases of pregnancy, on the cartons." [Times, 16-7 -82] This announcement does not directly say that the earlier decision is invalid and that the Govt. has abandoned the idea of banning these preparations. It only says that at present there is no ban. But this appears to be the thin edge of a wedge. The ban will not come at all unless all of us continue to build a pressure on all sides in favour of AN IMMEDIATE BAN. Please note that the news does not make clear whether the warning would be in bold letters or not. Recent editions of text books of Gynaecology do not recommend preparations containing high dose combination of oestrogen-progesterone (or any case of amenorrhoea. A patient with secondary amenorrhoea of more than equivalent of 3 cycles, (or six-months) is given high doses of progesterone after confirming that she is not pregnant, If this "progesterone challenge" as it is called, fails to elicit a response, (start menstruation) a course of oestrogen is given, followed by progesterone after this "oestrogenpriming." Thus there is no indication {or the use of these high dose combinations and the patient will not lose anything if these preparations are banned. We appeal to all those concerned about preventing avoidable congenital malformations due to these drugs to rouse public opinion in various ways and to writ« to the Drug Controller of India [Ministry of Health, Nirman Bhawan, New Delhi-16] requesting him not to go back from the earlier decision but to go ahead with an immediate ban on these preparations,

Anant Phadke


