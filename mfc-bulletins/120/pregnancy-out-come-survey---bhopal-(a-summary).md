---
title: "Pregnancy Out-come Survey : Bhopal (A Summary)"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Pregnancy Out-come Survey : Bhopal (A Summary) from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC120.pdf](http://www.mfcindia.org/mfcpdfs/MFC120.pdf)*

Title: Pregnancy Out-come Survey : Bhopal (A Summary)

Authors: Sathyamala C

Medico Friend Circle Bulletin

120 SEPTEMBER 1986

Pregnancy Out-come Survey: Bhopal (A Summary) During the second half of March 1985, three months after the Bhopal gas disaster, MFC conducted an epidemiological, sociomedical survey on the effects of the toxic gas in the bastis of Bhopal (1). The study showed that between 50-70% of the ambulatory population in the severely affected areas of Bhopal continued to have one or more serious symptoms implicating different body systems. Among women in the reproductive age group, a significant alteration in menstrual cycle had taken place. The alterations were mainly in the form of shortened cycles, abnormalities in the menstrual flow, dysmenorrhoea, and leucorrhoea. These findings were also supported by independent studies carried out by Drs Rani Bang and Mira Sadgopal on women attending gynae clinics in the affected bastis (2). This evidence of extensive damage to the different body systems especially to that of the reproductive system in the women added to the already growing concern over the effects on the developing foetus in pregnant women exposed to the gas. This concern had been voiced earlier in February 1985 by a fact-finding team of doctors (3). Yet in March 85 the controversy over the teratogenic effect of the gas was still raging, and many women who were in the first trimester of pregnancy at the time of gas leak had already crossed the stage beyond which MTP could be considered safe. Even then, the question of the teratogenicity of the gas was still pertinent, especially in the light of the enlarged cyanogen pool theory which did not rule out the possibility of adverse effects on conceptions occurring after the gas leak.

toxic gas was a potential teratogen, unless proved otherwise, instead of resolving the issue it opened up many more dilemmas. How ethical was it to recommend termination of pregnancies, on assumptions not based on facts, well-knowing that the medical services were neither neither sympathetic nor geared to meet the abortion needs of the affected population? Since MFC itself was not in a position to run counseling centres and gynae clinics, would it not be unethical to leave unattended pregnant women with a gnawing anxiety about their pregnancies? The situation was further complicated by the fact that many couples had lost their children in the gas leak and the effect of the gas on the future fertility was still unknown. The debate was also coloured by shades of moral dilemma which invariably surrounds all discussions on deliberate termination of pregnancies. The best that MFC could do at the time was to make a guarded statement about the unknown effects and therefore of a potential teratogenicity of the gas, leaving the decision of termination of pregnancy to the women themselves. However as months passed and no authentic information was available on pregnancy outcomes, MFC, despite its ever present limitation of resources, decided to design and coordinate an independent survey on pregnancy of the affected population in Bhopal.

Letters requesting help were circulated to various women's groups and the overwhelming response of women' activists indicated that the survey was a realistic and feasible proposition. However the study was beset by many problems right from the start. To begin with, it was almost next to impossible to get any information from the medical establishment in Bhopal. This conspiracy of secrecy was extended to such ridiculous lengths that even Unfortunately at that time MFC was not in a position innocuous information such as the ICMR numbering and to assess the risks to the foetus. Firstly, the socio-medical the map of the bastis was treated as classified documents. survey had covered only 60 families, a number too small The situation worsened in June when to measure abortion rates. Secondly, although it seemed safer to assume that the

in a sudden move the MP government arrested the doctors Observations: and the activists of the Jan Swasthya Kendra under A total population of 8165 in 1632 households was preventive detention. This un-called for action of the surveyd from the 22nd to 29th Sept 1985. There were 275 government had the effect of terrorising the basti people live births and I3 still births in this population after the as well as the few previously helpful doctors from the G.L. The birth rate after the G L thus works out to be Gandhi Medical College. That the study took place at all 33.68/1000 population comparable with the national birth was a reflection of the conviction shared by the women rate of 33. The still birth rate after the G L is 47.27/1000 activists participating in the survey and the affected live births women from the bastis on its absolute necessity. (Table I). Table 2 shows the month-wise conceptions and the The survey was conducted in three of the affected number of abortions from Jan 1984 to Aug 1985. The bastis which were selected on the basis of the post gas overall spontaneous abortion rate after the G L is 370.96 leak morbidity and/or mortality rate (4). These were J P which is significantly higher than the spontaneous Nagar (mortality rate 65.3/1000, morbidity rate 66%), abortion rate of 32.178 before the G L (Table 3). The Kazi Camp (mortality rate 46.7 and morbidity rate 54-60 overall foetal death ratio also shows a significant increase %), and Kenchi Chola (mortality rate 35.7 and morbidity in the year following the G L (Table 4). These findings rate 91.9 %). The selection of the area and the sample was are higher than the ICMR data on pregnancy outcomes in also facilitated by the fact that the ICMR had already Bhopal spontaneous abortion rate of 24.2 % and still birth rate accomplished one important task fairly efficiently, namely the division of the bastis into 13 areas and numbering of of 26.1 (6). all the houses providing the much needed sampling frame. However our objective in undertaking this study was Based on studies done elsewhere (5) which showed abortion recall of 82 % accuracy even after a lapse of not merely to substantiate the general observation of an 10 years, a historic control was decided upon. This increase in spontaneous abortions after the G L, but was was to be the status of the study population in the year to assess the more important effects of the toxic gas on preceding the event of the gas disaster. Detailed conceptions after the G L. Our data clearly indicate that information on the menstrual cycles and reproductive the rate of spontaneous abortion in women who history gathered from the study population for the conceived after the G L is significantly greater than the period from Dec. 1983 to Dec. 1984 (the year before abortion rates before the G L (Table 5). Thus the effect the gas leak) was to be the control for the period after of the toxic gas on pregnancies was not limited to the the gas leak. This method of using a historic control period of acute exposure alone as is popularly believed eliminated the problems of finding an identical control but was on pregnancies conceived even months after the population, even though it could have introduced an disaster. element of under-reporting due to memory lapse. The families were selected by random sampling taking into Implications: account a non-response rate of 25 %. A pre-designed In recent years spontaneous abortion rate as an index of and pretested proforma was used to enter the reproductive and menstrual history of the women from environmental hazards has gained wide acceptance. In at least two Public Health controversies in the US (one Dec 1983 to Dec relating to Love Canal and the other to the herbicide 1984 and from Dec 1984 to Sept 1985. 'Agent Orange'), spontaneous abortion rate has been accepted as crucial evidence in assessing hazards to humans. The MFC study on the pregnancy outcome of the population in Bhopal affected by the gas leak shows a Table I significant increase in spontaneous abortion rate after the Population surveyed 8165 G.L. as compared to the year preceding it. This increase was noticed as early as Jan 1985 by both the activists and Number of deaths after the G.L. 271 the Media in Bhopal. The attitude of the medical 275 Number of births after the G.L. establishment, however, was to maintain a tightlipped silence. When it could no longer be denied that there was 13 Number of still births after G.L. indeed an increase in abortion rates, the tendency was to 33.68/1000 population Birth rate dismiss it as a natural outcome of a disaster and though the 33.19/1000 population Crude death rate rates had increased in the Still birth rate

47.27/1000 live births.

Table II Pregnancy Outcome Surrey (Jan 84-Aug 85) Month of Conception

No. con-

No.

No.

Total

ceived

spont.

spont.

abortion

each

abortions

abortion

month

before

after

G.L.

G.L.

Jan 84

46

3

-

3

Feb

42

3

--

3

Mar

23

2

-

2

April

29

3

-

3

May

23

1

-

June

20

1

1 4

5

July

34

-

6

6

Aug

40

-

15

15

Sept

44

_

16

16

Oct

55

-

18

18

Nov

48

-

11

11

Dec

40

-

5

5

Jan 85

36

-

4

4

Feb

47

-

8

8

Mar *

47

-

8

8

April *

36

-

5

5

May *

33

June*

37

July *

18

-

7

7

-

4

4

-

4

4

-----------

Aug (LMP) ** 16 * Some of these conceptions could have terminated as abortions after the survey period because they were still less than 28 weeks at the time of survey. ** Since the survey was in the 2nd/3rd week of Sept. Some women still had 1 or 2 weeks before they could report a missed period.

immediate period following the G.L., the rates would come down once the people 'settled' down. The reasoning was that this disaster like any other disaster (floods, War) had caused a disruption in people's lives and the high abortion rate was due to the physical and emotional stress suffered by the pregnant women at the time of G.L. There was also a great reluctance to comment on the effects of the gas on future fertility. A certain amount of complacency was expressed by the scientific community because apparently the rate of congenital malformations had not increased significantly after the G.L. The tendency was to reassure everyone, in the inimitable 'doctor-patient' style, that everything was normal and that there was no need for concern.

In fact at times one had the uncanny feeling that the major effort of the medical establishment was to substantiate the Union Carbide's claim that the gas that leaked was as harmful or as harmless as the soothing breeze from the sea! In this context our survey findings assume greater significance. Generally studies on congenital anomalies focus on the observations made at birth or later. However as postulated by Stein and others, spontaneous abortions or 'the observations made at early stages of gestation are a valuable and largely untapped source of information about congenital anomalies', because the assessment of congenital anomalies after birth miss out on anomalies which are incompatible with survival. They give the example of trisomy 16 to illustrate this point. 'Studies of spontaneous abortion show that for this anomaly the value of p (probability of anomaly originating either at conception or during pregnancy) exceeds that for all other trisomies in man. Yet this anomaly has never been observed in births at or around term. Teratogens increasing p for such lethal abnormalities could not be detected where F (the observed rate of birth defects) is obtained by monitoring at birth'. (7). The increase in abortion rates after G.L. could indicate that mutagenic effects incompatible with survival have taken place in conceptions after the G.L. But, it could be argued, that these effects were largely confined to the immediate post G.L. period. However as our findings show the effect of the gas on pregnancies was not just a point time occurrence but was being experienced by pregnant women even ten months after the G.L. This surely does indicate a chronic 'exposure' effect which could have a long term effect on future fertility as well. The consistently high abortion rate found in this survey, when considered together with the significant disruption in the menstrual cycles and the enlarged cyanogen pool theory put forward by ICMR to explain the multi systemic effect on the affected population, it could be hypothesized that the gas indeed had a mutagenic effect. Since mutagenesis is so closely linked with carcinogenic Sin processes, one could also hypothesize on the possibility of the increased risk of cancer in children born after the G.L. in years to come.

Table III Rate of spontaneous abortion before and after G.L.

Before G .L.

After G.L.

Total conceptions

404

310

No. abortions

13

115

Abortion rate

32.178

370.96

Table IV Foetal Death Ratio Before and After G.L.

No. delivered (LB+SB)

Quarter.

1984 No. aborted

F/D ratio

No. delivered

1985 No. aborted

F/D ratio

Jan-Mar

30

2

6.66

76

27

35.52

Apr-Jun

87

12

13.79

77

24

31.16

Jul-Sep

56

3

5.35

94

20

21.27

Table V Rate of abortion in conceptions before G.L. and aborting before G.L. and abortion rate in conceptions after G.L. Concep. BGL 404

Abortion BGL 13

Rate 32.178

So where does that leave the people of Bhopal who have formed the objects of so many research papers? Although our survey was conducted in Sep 1985, it has taken almost one year to tabulate and analyze the findings. We have the excuse of being a very small group with extremely limited resources. What does ICMR have to say for itself? To our knowledge ICMR was forced to scrap their pregnancy outcome study thrice as both the design and data were faulty. Even now, an apology of a result has been published without stating their implications. In the fight between the ambulance-chasing lawyers and the courts, and in the fight between one medical establishment (Gandhi Medical College) and another (ICMR), the affected people have been left high and dry with only their misery and pain for company. Our findings indicate that the suffering of these people is not over as yet and more is to come. The absolute need for proper and continuous monitoring has been stated so many times and in so many forums that stating it once more seems to be an exercise in futility. Already in September 1985 when the survey was being carried out we observed that families that had suffered the maximum due to economic disruption and deaths had started moving back to their villages. How are these people going to be contacted for long term follow up? A Government, a Scientific Community, and a Medical Establishment that has exhibited its utter unwillingness and an inability to respond to a People's need at the time of crisis is certainly not going to respond now when time has effectively erased the tragedy from the Public memory. One cannot put back the clock; one can only hope that this study will be treated as essential evidence when the case comes to the Court

Concept. AGL 310

Abortion AGL 45

Rate 145.16

and help in getting compensation for the people who have become the unwitting objects of a man made disaster. Sathyamala. Organizations that participated in the survey: Sabla Sangh, Action India, Ankur, Saheli, Jagori, Prayas, Mahila Mukti Morcha Sewapuri, Sahiyar, Nari Athyachar Virodhi Manch, and Search. (This report is only a short summary of the survey findings and focuses on pregnancy outcomes only. The survey covered other aspects of the reproductive health of women as well. A detailed report will be available in October. If interested please write to the Convenors office) References: I. The Bhopal disaster Aftermath, an epidemiological and socio-medical study, MRC, March 1985. 2. Effects of Bhopal disaster on women's health-an epidemic of gynaecological diseases (Part I) Mimeograph, Rani Bagh and Mira Sadgopal, 1985. 3. Medical Relief and Research in Bhopal-the Realities and Recommendations, MFC report, Abhay Bang et al, Feb 1985. 4. An epidemiological and sociological study of the Bhopal tragedy, Centre for Social Medicine and Community Health, JNU, unpublished. 5. Accuracy of spontaneous abortion recall, Am. J. of Epid, Wilcox A J, 120 (5): 729-33. 6. ICMR statistics as quoted in EPW, Vol XXI, No. 27, July 1986, p. 1134. 7. Spontaneous abortion as a screening device, the effects of foetal survival on the incidence of birth defects, Am. J. of Epid, 102:275-90, 1975, Stein et at.

An Apology Due to certain unavoidable circumstances including new governmental regulations, the printing of the MFC bulletin was temporarily discontinued from Dec. 1985. We hope that with this issue we will be in a position to bring out the bulletin more regularly.

.~.

WHO - CAN YOU TRUST? (Reprinted from 'Right to choose', issue No. 26, Autumn 1983; article contributed by members of the Anti-Depo Provera campaign, NSW)

Who are W.H.O. ? Looking into the makeup of the "expert" panels that have contributed to these reports we find the names of employees of the drug's manufacturers, plus the same experts who have written pro-Depo articles for the many journals and magazines published by the International Planned Parenthood Federation or the United Nations Fund for Population Activities. They are part of an international network of doctors and scientists whose uniting interest is population control and its direct and indirect profits. In some countries, in some circles, "population control" is given positive value-seen as a good thing-and the experts will be quite blatant there about their motivation. In western countries which do not have a population "problem" and where feminism has had some impact, the tone changes to one of "allowing women a greater range of options from which to make an informed choice". But do not be mislead it’s still population control. Throughout the literature relating to the use of Depo in western countries certain phrases recur frequently such as "target populations", "institutionalised women", "women who are not responsible contraceptors". Looking at use patterns in these countries we see what these phrases mean the typical users are women in institutions, and the racial minorities Asians in the U.K., blacks in the U.S., Maoris and Polynesians in N.Z., Aborigines in Australia.

-

-

Added to this racism is an underlying misogyny which says that women should be sexually available to men at all times at whatever health cost to the woman while men are protected from the annoying possibilities of unwanted paternity.

Illogical Leaps of Faith The introduction to the first article, ("Injectable hormonal contraceptives, technical and safety aspects") - (I) - sets the scene. We are immediately told how effective and widely used Depo is and why

its so good. After this promotion we get the disclaimer "However, this method is not entirely free of risks . . . .” The style is reminiscent of cigarette ads with the health hazard warning in small print at the bottom. Further into the article the authors do admit evidence which indicates the dangers of this drug. However, they gloss over it, twisting a lack of properly designed research into a lack of negative evidence. When they say no research has show problem X, it's often because there has been No research on problem X at all. By the time you reach the conclusion the most they are prepared to say is "further research is needed" meanwhile women should be encouraged to use the drug.

-

Some examples Teratogenicity (the effect of the drug on infants exposed to it while in the womb): Firstly, the authors admit that "NO studies have systematically followed the health and development of infants exposed in utero to DMPA (Depo)" (1). They go on to mention three reported cases of clitoral enlargement among the daughters of women who had received Depo early in their pregnancies. From here they leap to the conclusion that "iF any increase in risk of congenital anomalies exists and there is no clear evidence that it does - it must be quite small." Again in the conclusion we get the rider "Research should continue in these areas."

-

Exposure through breast milk: Here again we see the glossing over of possible risks accompanied by an admission of the lack of appropriate long term studies and followed by the reassurance that exposure to the drug through breast milk is "UNLIKELY to give rise to adverse effects on the child's development." (2) In the body of the text there is reference to one animal study which has "suggested a possible effect on reproductive development." Searching out this study (3) we find that rats exposed to Depo through breast milk exhibited a "significant delay in the onset of vaginal opening and of the first oestrous cycle" compared to controls. Human children have only

been studied up to the age of thirteen. NO studies have followed children through puberty. This absence of appropriate research is twisted by an Australian author into the statement "There is NO EVIDENCE of long term effects on the breast fed infants of DMPA treated mothers." (4) The world wide promotion of Depo use amongst breastfeeding women has led to the anomalous situation in Jamaica where nearly 9 % of breastfeeding women use it compared to only 5 % of non-breastfeeding women. (5)

Cancer: In the literature on the possible carcinogenicity (potential to cause cancer) of Depo we find the most amazing perversions of logic. The finding of cancer in beagles treated with Depo has been a large stumbling block for Depo advocates as it was a major cause of the FDA's refusal to approve the drug. Consequently, over the past few years medical journals have been littered with articles attempting to prove that the beagle is not a aspersions have been cast on suitable test animal monkeys too (l) & (2) and others.

-

INTRODUCTION OF NET-EN CHALLENGED For the first time in India, the attempts of the Health Ministry and the ICMR to push through a hazardous contraceptive for women has been challenged by a writ petition filed in the Supreme Court on the 7th of April this year. On 1st May, the Court issued notice to the respondents as to why the petition should not be admitted and stay order granted on further trials of the contraceptive. In addition to the Health Ministry, the ICMR, and the State of Andhra Pradesh, the Drug Controller of India was also impleaded as a respondent. The notice was returnable on July 15 1986. The respondents are yet to file in their reply. The contraceptive in question is Norethisterone enanthate (NET-EN), a progestin derived from testosterone, prepared in an oily solution and administered as an injection. The petitioners (three women's groups and six individuals) contend that the drug is a definite hazard to women's health and a potential hazards to their progeny. Further, they state that under Indian conditions, given the present state of health services, the potential hazards of this drug do not justify its introduction into the mass Family Planning Programme. The petitioners also allege that the clinical trials conducted by ICMR have violated the ethics of human experimentation by recruiting women without their informed consent. The petitioners demand that all further experiments on Indian women with this drug be stopped immediately and the drug be banned for use in India.

One would expect that this would lead to a search for a more suitable test animal. However, the urgency is too great and the conclusion has been that the only really suitable test animals are women. So far "most of the human studies have been poorly designed and do not provide much useful information." (6) As many cancers take 20 to 30 years to develop, millions of women will be —Saheli Collective, N. Delhi. exposed to this drug before any definitive statement on  the cancer question can be made. nature of the drug and the need for repeated cancer tests for at In a desperate attempt to refute the cancer risk theory least 20 years following their first injection. However, the researchers in Thailand conducted a study on previous WHO studies in fact say: "IDEALL Y annual pelvic and breast Depo use among women admitted to hospital for examinations should be undertaken" (2) and: "WHEN LOCAL endometrial carcinoma (cancer of the lining of the CIRCUMSTANCES PERMIT breast and pelvic examinations womb). Although 16 of the 27 women came from areas should be included. The pap smear is an OPTIONAL exawhere Depo is widely used, none of the drug. This study mination to be performed when indicated and when resources is quoted in both the W.H.O. reports. The logic of it is permit." (1) women who had cancer didn't use Depo, astounding therefore Depo is safe!! So what are they really saying? It appears that in order to

-

Despite the "experts" dismissal of the beagle as a suitable test model, the manufacturers, Upjohn have repeated the beagle studies and the results will be presented to the FDA who still requires beagle tests at the coming hearing (7).

maintain their scientific reputations some controlled cancer studies will be done in future on selected samples of women, but that in the meantime women worldwide should use Depo and only be checked for cancer "if resources permit".

The lack of a conclusive statement on the car- Distribution Guidelines cenogenicity of Depo just opens the way for more The articles give advice on how Depo should be presented to widespread use of the drug in the interests of research. To women and how to train non-medical this purpose one would expect that women given Depo would be told of the Investigational

personnel to do this. The WHO is part of the western oral and injectable methods "have all incredible profit medical monoculture and, therefore, pays lip service to a margin". They are "amongst the most profitable of all woman's right to make an informed choice. However, their pharmaceuticals" (7). These WHO reports are really attempts at feminism are transparent revealing paternalism advertising blurbs presented in the manner that doctors and the same old" doctor knows best" attitudes. For expect. The racism, paternalism and greed of the example: "If her choice is an injectable hormone then the international community of experts is not well hidden and nature and type of common side effects should be the lust for this drug is obvious. While protecting explained with an EMPHASIS ON THEIR TRANSIENT themselves by admitting the risks of the drug to each other (not to women, of course), they salve their consciences NATURE." with the universal panacea of "more research is needed". Meanwhile…..Mrs. Smith, did you say you were having They also give an example of the wording that could trouble remembering your pill….Well......... be used in the package insert which "should be worded as simply as possible. The essential information should be presented objectively and SHOULD NOT AROUSE References: (1) "Injectable hormonal contraceptives, technical and APPREHENSION OR ANXIETY Oil the part of the safety Aspects," WHO Offset Publication No. 65 (1982). consumer." The information they regard as essential includes details of possible disturbances to the menstrual cycle, headaches, dizziness and weight gain. NO mention is made of any of the cancer or breastfeeding debates. Thus, the advice that women are to be given (from which to make an INFORMED choice) includes none of the basic areas of concern which in the WHO articles are described as follows: "Doubts which have been expressed regarding the safety and appropriateness of an injectable hormonal contraceptive for widespread use are related to their possible carcenogenicity, impairment of future reproductive function, adverse metabolic effects, potential teratogenicity and other possible adverse effects on the progeny (as a result of exposure to the steroid hormone either in utero or via breast milk)."

Potential for Abuse For most of a decade women's and consumer groups have repeatedly pointed out that the main danger with injectable contraceptives is their potential for abuse. Women can be given the drug without their consent or knowledge or it can be presented so attractively that it is hard to resist. It can be used to curb unwanted sections of the population such as ethnic minorities, refugees, handicapped women, etc. Only one of the articles even mentions the question of abuse. Referring to a meeting of the experts they say that the topic of abuse "was not discussed in detail." (2)

Hard Sell The value of Depo sales has already reached $ 25 million and would rise dramatically with FDA approval, according to market analyst Arnold Snider. He adds that

(2) "Facts about injectable contraceptives", Bulletin of the World Health Organisation 60 (2): 199-210 (1982). (3) "The effect of medroxyprogesterone acetate, administered to the lactating rat, on the subsequent growth, maturation and reproductive function of the litter," N. Satayasthit, M. Tankeyoon & R.R. Chaudhury, Journal of Reproductive Fertility, (1976) 46, 411-412. (4) "Post-partum sexuality and contraception" by Edith Weisberg in Health right. Vol. 1, No.3, May, 1982. (5) "Women in the developing world who breastfeed their infants rarely use hormonal contraceptives", International Family Planning Perspectives, Vol 8, No.2, June 1982. (6) "Injectable Contraception", by Peter Hall & Susan Holck, World Health, May 1982. (7) "Depo-Provera debate revs up at FDA", by Marjorie Sun, Science, Vol 217, July 1982.

Form IV (See rule 8) 1. Place of Publication New Delhi 110029 2. Periodicity of its publication Monthly 3. Printer's Name (Whether citizen of India?) Address 4. Publisher's Name (Whether citizen of India?) Address

5. Editor's Name (Whether citizen of India?\ Address 6. Name and address of individuals who own the newspaper and partners or share holders holding more than one percent of the total capital.

Sathyamala Yes B-7/88/1, Safdarjung Enclave New Delhi-110029 Sathyamala Yes B-7/88/1, Safdarjung Enclave New Delhi 110029. Sathyamala Yes B-7/88/1, Safdarjung Enclave, New Delhi 110029. Medico Friend Circle Bulletin Trust 50 LIC Quarters University Road, Pune 411016

EDITORIAL Patients are socialized to believe that the Medical Profession acts in their interests. Medical Profession is socialized to believe that ICMR and WHO represent all that is best in their profession. It is then left to others to expose the decay that exists in these institutions. Immediately following the Bhopal gas disaster, expectations on ICMR ran high. This expectation seemed well placed especially when ICMR put forward the brilliant theory of enlarged cyanogen pool to explain the multi systemic effects of the gas. But later developments have shown that their interests have not been focused on the affected people. It has been left to an alternate group with limited means to design and coordinate an important study.

million Dalkon shield IUDs in eighty countries, with false claims of efficacy and safety. In the US alone, more than two million women were fitted with the untested contraceptive device by doctors who believed the misleading claims. To date, thousands of women have suffered serious damage caused by the shield from pelvic infection to sterility, miscarriage and even death. More than $ 340 million has been paid by Robins and its insurer to litigants and thousands of law suits are still pending. Although the shield was pulled off the market ten years ago, the full extent of the injury done to women, and the scope of deception is only now coming to light.

At Any Cost reveals for the first time the complete inside story of the Robins company's dangerous decisions, and the complicity of members of the legal and medical communities. Beginning with Robin's purchase of the IUD from the small Dalkon firm and their shocking failure to test the device for safety, it documents the complete suppression of unfavourable data, cynical marketing strategies, secretive arrangements So also with the contraceptive research currently with insurance companies, loss of evidence and courtroom intimidation being undertaken by the ICMR and the WHO. The of injured women.. (Unfortunately this book is very expensive and not tendency to push long acting invasive hormonal available in paperback). contraceptives is a classic example of the disregard these 2. To Do No Harm: DES and the dilemmas of Modern Medicine; organizations have towards the health of women. The Roberta J Apfel and Susan M Fisher, Yale University Press, 1984, arguments put forward by WHO and ICMR to counter (price, $ 8.95). the well substantiated case against such contraceptives only goes to prove their intellectual and ethical bankruptcy. It further goes to prove that medical research can never be neutral and the choice of pursuing a DES a synthetic hormone that was given to millions of pregnant particular aspect is determined by priorities outside the women to enhance the likelihood of healthy delivery, later turned out to context of the laboratory and as history has shown, be harmful to many of their children. This carefully balanced book priorities are shaped more by financial interests than a studies the medical effects of DES, its psychological repercussions and concern for human suffering. the factors that contributed to the DES disaster. It raises important — Sathyamala. KEEPING TRACK: 1. At Any Cost: Corporate Greed, Women and the Dalkon Shield; Morton Mintz, Pantheon Books; 1985. (Price, Hard Cover $ 17.95).

questions about the explosion of modern medical technology; delineates factors contributing to experimentation with new techniques & drugs and how the pressures experienced by physicians coupled with their fantasies of heroism, meld with pressures from their patients to tempt them to take risks prematurely.

Between 1971 & 1975, the A.H. Robins Company, a family controlled pharmaceutical manufacturer best known for Chapstick, distributed over four

Editorial Committee: Anil Patel Abhay Bang Dhruv Mankad Padma Prakash Vimal Balasubrahmanyam Sathyamala, Editor

Views and opinions expressed in the bulletin are those of the authors and not necessarily of the organization. Edited by Sathyamala, B-7/88/1, Safdarjung Enclave, N. Delhi 110029. Published by Sathyamala for Medico Friend Circle Bulletin Trust, 50 LlC quarters, University Road. Pune, 411016. Printed by Sathyamala at Kalpana Printing House, L-4, Green Park Extension, New Delhi 110016 Correspondence and subscriptions to be sent to-The editor, F-20 (GF), Jungpura Extn., N. Delhi-I 10014.


