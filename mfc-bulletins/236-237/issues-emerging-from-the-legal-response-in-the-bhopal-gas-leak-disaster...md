---
title: "Issues Emerging From The Legal Response In The Bhopal Gas Leak Disaster.."
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Issues Emerging From The Legal Response In The Bhopal Gas Leak Disaster.. from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC236-237.pdf](http://www.mfcindia.org/mfcpdfs/MFC236-237.pdf)*

Title: Issues Emerging From The Legal Response In The Bhopal Gas Leak Disaster..

Authors: Sathyamala C

medico friend circle bulletin 236 237

Nov-Dec, 1996

Health Effects of the Bhopal Gas Leak Disaster Research by ICMR in Bhopal Numerous studies have been carried out on the exposed population in Bhopal to understand the precise nature and mechanism of the adverse impact of the toxic gases that leaked from the Union Carbide pesticide plant on the night of December 2 and 3, 1984. Of the numerous research studies on the gas exposed population, the studies carried out by the Indian Council of Medical Research. (ICMR) are of particular importance because of their breadth of coverage and their attempt at long term follow up. Many of these studies which were launched within a month of the gas leak disaster, and on which about five to six crores of Rupees were spent until the end of March 1991, have been terminated. Based on the Annual Reports (1987-1992) of the Bhopal Gas Disaster Research Centre (BGDRC), an institution specially set up by ICMR for Bhopal studies, a brief review of some of the long term studies of the ICMR is being presented. These pertain to the effect of the toxic gases on the respiratory system, mental health of the exposed population and to the findings of the long term epidemiological study on a cohort of population registered after gas leak. This present review focuses mainly on the group of ICMR studies, most of which have not been published but have been summarised serially in the Annual Reports of the BGDRG. A more extensive, updated and, critical review of all the available literature on the health status of the gas exposed population is still required.

Part-I Damage to Respiratory System The damage to the respiratory system by the toxic gases released from the Union Carbide plant in Bhopal, comprises the most obvious and significant part of the overall damage to the health of the gas victims. For a long term follow up of this damage, ICMR has launched a number of studies. The following is a very brief account of the features of the important studies on the respiratory system.

effects of toxic gas on respiratory systems" started on 15th October 1985, with Dr. N.P. Misra, as the principal investigator.

Aims and Objects The aims and objectives were to study the effect of toxic gas on respiratory system of the exposed population and to identify a sample for indepth studies. A suitable

1. Population Based Follow up This study named as "Epidemiological study of long term

This document was prepared for the International Medical Commission by C Sathyamala, Anant Phadke, Mira Sadgopal, Satya Jaya Chander, Mira Shiva, Manisha Gupte, and Prabir Chatterjee of the Medico Friend Circle.

control population was included for the purpose of comparison. Methodology A list of about 5000 random persons from the gas affected population was taken from the registered cohort under long term epidemiological study during August October 1985. These were selected based on six categories as follows: Criteria Category in Cohort persons who were immediately I affected and are still suffering. II

III

IV

V

VI

Total No.

persons who were immediately affected but they are not suffering with respiratory symptoms at present.

46,910

immediately affected, became alright, developed respiratory symptoms later and became alright. 21,507

326

no immediate effects, only later respiratory

Table -1 CLINICAL DIAGNOSS Disease

119

A random sample of 2000 persons from control population 'was also taken, persons in both the groups were examined as per standardised protocol which included a standard respiratory questionnaire, physical examination, haematological examination, sputum, x-ray chest and spirometery using a field spirometer. Some of the important intermediate results of this study as reported in the BGDRC, annual report 1988, are as follows: Observations and Inferences Out of the 4964 cases in the affected area 2361 (47%) are males and 2603 (53%) are females. In the control group, out of 2000 cases 804 (40.25%) are males and 1196 (59. 75%) are females. The main symptoms and signs are significantly higher in all the exposed areas in comparison to control areas (Table 1). In the affected area 32 (0.64%) patients

Exposed population 4965 % in parenthesis

Control population 2000 % in parenthesis

Past History

Present Past Present Diagnosis History Diagnosis

Tuberculosis

46 (0.92)

28 (0.56)

18 (0.90)

7 (0.35)

Bronchial Asthma

17 (0.34)

32 (0.64)

43 (2.22)

52 (2.6)

6,99 0

persons who were immediately affected became alright, developed respiratory symptoms later and are still suffering. 3,907

immediately affected then later effects and have recurring attacks of respiratory illness.

developed features like bronchial asthma and 146 (2.94%) patients developed features of chronic airways obstruction. The incidence of pulmonary tuberculosis was identical in both affected and control population. More than 85% were diagnosed to have unspecified lung disease.

C.OAD

45

146

70

72

(0.90)

(2.94)

(3.5)

(3.6)

Unspecified

-

4225

-

3

lung disease

(0.0)

(85.09)

(0.0)

(0.15)

There is significantly higher incidence of interstitial lesion, pulmonary tuberculosis and peribronchial fibrosis in the affected as compared to control area (Table 2). Clinical data of 1550 exposed and 838 controls could be linked with that of radiological data. There is significantly higher incidence of restrictive and combined type of pulmonary function abnormality in the exposed patients as compared to the control (Table 3). In the affected area 1095 patients had normal x-ray, but 56% of these had PFT abnormality. This finding was significantly higher in the affected than control (Table 3). This emphasizes the fact that even the chest x-ray is normal there is gross physiological derangement. The mean values of FVC, FEV and FEV/ FVC were significantly lower in the exposed population as compared to control (P< 0.001). A total 4483 skiagram of the epidemiological studies were fed for computer analysis and after proper securitization 4362 cases are presented with detailed analysis (Table 4).

Follow up of a Randomized Sub-Sample Simultaneously 570 cases were randomly selected from the 5000 exposed population who were brought (from Jan 87 to Dec 87) to the pulmonary function lab of the dept.

Table-3

Table-2

RADIOLOGICAL FEATURES Vs. PFT

RADIOLOGICA FEATURES

(March 87-Januilry 88)

(Oct. 1985- Dec. 1987) Total linkage 2388 Exposed: 1550 Control 838 Radiological Features

Normal

Abnormal

PFT

10951704

218/58

7918

Exposed

480 (44%)

60 (27.5%)

22 (27.8%)

Control

576 (82%)

33 (57%)

6 (75%)

Exposed

64 (5.8%)

29 03%)

13 (16.4%)

Control

12 (1.7%)

2 (3.4%)

0 --

Exposed

34 (3.1%)

12 (5.5%)

44 (55.8%)

Control

25 (3.6%)

3 (5.1%)

2 (25%)

509 (46.5%)

117 (54%)

0 --

191

20

0

(27.1%)

(34.5%)

-

Severe Exposure n=577

Moderate Exposure n=792

Mild Exposure n=181

n=838

396

573

126

704

(68%)

(73%)

(69%)

(84%)

Combined

Control

181

219

55

134

(32%)

(27%)

(31%)

(16%)

48

11

17

(7.1%)

(6.1%)

(6.1%)

(2.1%)

Interstitial

82

106

30

58

vision

04.2%)

03.4%)

06.6%)

(6.9%)

45

6

8

Tuberculosis 41

peribronchial 28 fibrosis

(4.9%)

(5.9%)

(3.3%)

(0.1%)

emphysema

6

5

1

8

large opacity

(1.03%) 18 (3.1 %)

(0.6%) 44 (5.6%)

(0.56%) 7 (3.9%)

(0.35%) 12 (1.4%)

discrete

22

23

7

28

lesions

(3.8%)

(2.9%)

(3.9%)

(3.3%)

malignancy

Radiological Normal (E/C) Interstitial (E/C) Peribronchial (E/C)

Normal

Obstructive

Restrictive Exposed Control E. Exposed C. Control

1

2

1

2

(1.17%)

(0.25%)

(0.6%)

(0.23%)

Table-4 TYPE OF ABNORMALITIES (RADIOLOGICAL) OCT 85-87 Dec No. Radiological Findings

Severe Exposure

Moderate Exposure

Mild Exposure

Total Affected

Control

1.Normal

674 (70.1%)

946 (72.21%)

246 (68.3%)

1866 (70.92%)

1457 (84.17%)

2. Pulm. TB

68 (7.07%)

78 (5.95%)

20 (5.55%)

156 (5.92%)

51 (2.94%)

3.Emphysema

02(0.93%)

14 (1.06%)

02 (0.5%)

25 (0.95%)

04 (0.73%)

4.Interstitial Reiculo nodular

92 (9.57%)

132 00.07%)

63 07.5%)

287 00.90%)

87 (5.02%)

A, 0/1

38 (41.30%)

36 (27.2%)

09 (14.2%)

83 (28.9%)

22 (25.2%)

B, 1/0

36 (39.1%)

77 (58.3%)

3J (49.2%)

144 (50.17%)

43 (75.4%)

C, 1/1

18 (19.5%)

18 (13.6%)

23 (36.5%)

59 (20.5%)

19 (21.8%)

D, 1/2

0

01 (0.75%)

0

01 (0.34%)

03 (3.4%)

5.PBF (Peri bronchial fibrosis)

38 (2.08%)

60 (4.58%)

12 (3.33%)

110 (4.18%)

12 (0.69%)

Total Abnormal

287 (29.8%)

364 (27.7%)

114 (31.6%)

765 (29.08%).

274 (15.82%)

961

1310

360

2631'

1731

of medicine for flow volume study. In the second phase, cases were randomly selected from those already studied in first phase. For the purpose of getting representative cases of varying degree and type of respiratory systems and involvement, cases of first phase were divided into five categories and age, sex matched controls were also taken (Table 5). Table-5 CAT RESPIRATORY PFT CHEST COVERAGE FOLLOW UP SYMPTOMS X-RAY MAY88-APR 89 MAY89MAR 90

term follow up may reveal the final outcome of the damage. 2. Clinical Hospital Based Population Clinic / hospital based studies were launched on severely affected people who were hospitalised after the gas leak. Such studies give an insight into the patho-physiology of distress complained of by the gas victims, persisting even, months and years after the gas leak. By way of example two such studies are very briefly reported here:

I

NIL

NOR

NOR

55

50

II

PRESENT

NOR

NOR

66

63

I. Pulmonary Function Tests Including Blood Gas Analysis (BGDRC Annual Report 1992, Page 5780)

III

PRESENT

ABN

ABN

66

60

Methodology

IV

PRESENT

ABN

NOR

49

45

V

PESENT

NOR

ABN

75

70

311

288

Three hundred and twenty patients with history of severe exposure to toxic gas were registered in Feb. 1985 and followed up since then. Criteria for selection were either hospitalisation in -Jawaharlal Nehru Hospital or death in the family/neighborhood or prolonged' treatment in hospital.

TOTAL

This study was carried out between May 88 to Apr 89. Follow up of these cases was started from May 89 onwards and 288 cases were followed up till 31.3.90. All these cases were brought to-the respiratory lab of the dept. of Medicine. Besides clinical examination, PFT was done on Morgan's computerised lung function equipment giving values of flow rates, lung volumes and diffusion capacity, Chest skiagram were repeated, This follow up of the sub-sample confirmed the findings of the main sample reported above that there is significantly more proportion of cases of lung damage of various kinds in the exposed population as compared to the non-exposed control population. Briefly the results upto end of March 1990 were as follows (BGDRC, annual report-1990, page 70): A large number of cases continued to be symptomatic even at the end of five years. 24% of cases had developed features of bronchial asthma which were in fact cases of reactive Airways Dysfunction Syndrome. 11.4% Cases had developed Chronic Obstructive Airways Disease, 13% patients complained of recurrent chest infections and another 13% had developed bronchiolitis obliterans. Restrictive lung disease was seen in 1.4% of cases. The 'conclusion was' that MIC gas exposure has caused lung injury leading to damage to both large and small airways, thus resulting in obstructive airways disease of different varieties. Though restrictive lung disease is not seen significantly in large number of cases at present long

Their detailed history of exposure, smoking habit, occupation" previous lung disease was noted and clinical examination was done routinely on every visit. Complete investigation including chest skiagrams, pulmonary functions and blood gas analysis were done. Since 1987 uptil 1992 March, following number of patients could be followed 245, 269, 272, 270, 220, and 110 ill each year respectively.

Pulmonary Function Test In the fifth follow up i.e., 1990-91, 220 patients were followed. Out of220 patients, 158 (71.82%) had abnormal pulmonary function which on further analysis showed that 71 (32.27%) patients were having combined lung lesion, 56 (25.45%) obstructive lung lesion and 31 (14.09%) were having restrictive lung lesions. 42 patients showed significant annual decline (more-than 15%) in FVC& FEV-l over the period of 1987 to 1991. This year, 10 more patients have shown significant decline in their lung functions. Incidences were equal in smokers and non smokers and include all age groups. These findings suggest that these patients are having chronic airway obstruction.

X-ray Chest During the year 1991-92 x-rays done during the fifth and sixth follow up show the following findings:

TABLE-6 Year

1990-91 5th follow up

1991-92 6th follow up

No. of x-rays

220

110

Normal

52 (23.6%)

22 (20%)

Abnormal

140(63.7%)

72 (65.5%)

Repeat OV/UE

28 (12.3%)

16 (14.6%)

In order to establish relationship of lung function with chest skiagram, they were divided into four group’s i.e. abnormal x-ray and abnormal PFT, abnormal x-ray and normal PFT, normal x-ray and normal PFT. It was seen that over the last 5 years there is a gradual reduction in number of patients having abnormal pulmonary functions and normal chest skiagrams whereas there is gradual increase in abnormal 'lung function and abnormal x-rays. This shows that With the passage of time toxic gas exposed patient are

developing signs of permanent injury. On matching x-rays with socio-economic status it was seen that out of 124 patients of poor socio-economic group, 70 patients were having abnormal x-rays. On further analysis of these over the last years, it was seen that 45 patients who were earlier showing normal chest skiagram are now having abnormal chest skiagram. On matching 'pulmonary function with their x-rays it was found that out of this group of 124 patients, 91 had abnormal lung function. It is "Clear from the above finding that besides smoking certain socio-economic parameters e.g. traditional use of Burda wood, sharing of kitchen with living room etc, also had significant contribution in progression of lung disease in MIG exposed victims. In the beginning of the study the cohort comprised of 320 patients. By the end of the fifth follow up, 24 patients had died, 30 had migrated and 20 were defaulters. The social factors affecting the follow up of the patients has a direct bearing on the financial incentives given to these patients by the Department of Gas Relief and Rehabilitation of Govt of M.P. During the course of study 30 patients developed corpulmonale, of which 12 patients had expired. This constitutes 50% of total 24 deaths of this cohort. From the above study, it may be concluded that patients of cohort are gradually developing permanent lung lesion as well as corpulmonale which is mainly contributing for deaths. It is suggested that there is a need for continuous follow up of patients of this cohort for long period. There is also a need to register severely affected people, to closely monitor for pulmonary management and rehabilitation.

II. Pulmonary Function test including blood gas analysis: This study was launched in February Narayanan/Dhir/Sriramachari investigators.

1985, with as principal

Methodology ICMR laboratory was set up first in the Satguru Complex and in J.L.N. Hospital. The gas exposed patients were identified, located with the help of the social workers, and brought to J.L.N. Hospital for investigations. The patients were classified according to the ICMR criteria in the study as (i) the patients were living in the gas affected area (ii) there was death in the members of the family. (iii) symptoms were severe enough to need hospitalisation. (iv) patients took prolonged treatment in J.L.N. Hospital. It was envisaged to carry out examination of patients and identify if possible, patients originally investigated, otherwise select a cohort of patients from the affected area.

*

Complete blood analysis for the quality and quantity of cellular components in blood.

*

Blood gas analysis both arterial and venous to see if the patients have returned to normal from abnormal levels seen previously.

*

Pulmonary function tests on spirometer.

*

To assess 2-3 DPG levels by phospho glucomutase technique.

* To estimate methyl valine hydantoin and valine hydantoin in the blood of affected individuals: (BGDRC, annual report 1987, page 28.) This study shows that even in 1986-87, 24% of the affected population had more than 12 gms% of haemoglobin level; they were responsive to treatment with sodium thiosulphate and proportionate changes in urinary and high Thiocyanate levels. The exposed persons had reasonably good PaC02, PvC02. This study proved that Methyl isocyanate and other gases from the Carbide plant had passed the pulmonary barrier, had entered tissues and had resulted in carbamylation of Haemoglobin, (BGDRC annual report 1990, page 90-98,) These studies and others of similar nature prove not only the long term damage to the respiratory system of the gas victims, but also systemic poisoning by these toxic gases.

•

Issues emerging from the legal responses in the Bhopal Gas Leak Disaster case C Sathyamala

This paper was presented at the Second National Convention on "Bhopal Gas Leak Disaster and its Aftermath" (April 1991) in New Delhi. By then two years had passed since the Government of India and Union Carbide Corporation had arrived at an unjust settlement in the legal case. At the time of the convention, the hearings on the review petitions against the settlement had been completed and verdict was awaited. As intervenors in the Supreme Court case, Dr. Nishith Vohra and I saw the categorization of medical injuries and estimation of the numbers of injured as being central to the legal strategy. Although, on the face of it, "Unsettling the settlement" appeared to be the common demand of the victims' organizations and their supporters, analysis of the situation and hence the strategies adopted in the legal case differed. In the final analysis, these differing strategies were counter productive and laid the grounds for upholding the settlement. This paper is being reproduced here because of its historic importance, because very few are aware of what went on behind the scenes and because there are lessons to be learnt. When the settlement was announced in February 1989, there was shocked disbelief that the government would sell out the interests of lakhs of victims for a sum of 470 million US dollars. But looking back, the settlement should not have been such a surprise because the events earlier to that had laid the grounds for precisely such a settlement. The collusive settlement was the natural outcome of the Bhopal Gas Leak Disaster (Processing of Claims) Act. The Bhopal Act The Act passed in 1985, gave exclusive tights to the government to be the sole representative of the victims against UCC (Union Carbide Corporation). Under this Act, the government had the legal right to do whatever they wished to on behalf of the victims. They could settle the case, or drag it forever and ever or put up a good fight, or, as argued by the counsel for DCC, they could have opted not to file the suit and instead could have directly negotiated to settle the case. They opted for a settlement. While it was obvious to anyone with some modicum of political understanding that the interests of the government (which itself was a party to the disaster) would be more 'in keeping with the interests of the multinational rather than with the interests of the victims (who were largely from the poorer sections) there was-very little opposition/to the Act. Even if one did not have political understanding, the fact that every bit of relief to the victims was gained through countless demonstrations in which victims were beaten up, arrested or intimidated through other forms should have provided adequate

evidence to show where the government's interests lay. Right from the time of the disaster the non-provision of relief (both medical and economic); the haphazard manner in which assessment and monitoring of the impact of the toxic gases on the health of the exposed population and the environment was carried out; the suppression of information under the Official Secrets Act; harassment and arrests of activists who supported the victims' cause, should have led us to be more vigilant. In fact, even for something as elementary as access to an "antidote", Sodium thiosulphate, Dr. Nishith Vohra on behalf of the victims had to file a petition in the Supreme Court to direct the government to produce and distribute NaTS free of cost.

However, despite all this, we did not oppose the Act because we harboured a strange faith in the 'goodness' of the government. We believed that the government would perform its role of parens patria in all sincerity and protect the interests of the victims; or if we did not have full faith, we hoped that by maintaining a somewhat inconsistent pressure, the government could be forced to protect the interests of the victims; or it could be that we lacked confidence in the ability of the victims, their organizations and ourselves to mobilize and give legal coherence to their demands; or perhaps we believed that the State has the fundamental right to appropriate the rights of its citizens. Fortunately, for us, a few lawyers who understood the implications of the Act differently from us challenged the unconstitutionality of the Act in the Supreme Court.

Although the Supreme Court upheld the validity of the Act by maintaining the State's exclusive rights to defend the victims, important gains were made. The Supreme Court conceded that the victims should have been given notice prior to the settlement, that provision of interim relief was part of the parens patria role of the government, and that the quashing of criminal proceedings was outside the purview of the Act. Thus, at the time of the review, the many counsels representing the interests of the victims, as well as the Attorney General, could harp on Order-23, rule 3 B (notice to the victims) and the illegal quashing of criminal proceedings as grounds enough for setting aside the settlement.

The Civil Case - Quantum of Compensation The challenge to the settlement rested on three major grounds. First, the amount settled for was inadequate; second, there was no provision for compensating injuries that may develop in future; and third the criminal liability proceedings had been quashed. Since the verdict on the Bhopal Act made it clear that the quashing of criminal proceedings was illegal and outside the purview of the Act, I will concern myself to the quantum of compensation as related to the nature and extent of injuries i.e., ·the civil proceedings. The quantum of compensation for personal injury depended on two factors viz., the number of persons injured and the amount that needed to be paid to each individual. Although it was common knowledge that the process of assessment of injuries had been initiated by the Directorate of Claims, Bhopal, as early as January 1987, and although there was a 'gut-level' feeling among the victims that the assessment carried out by the government could not be very much to their (victims) advantage, little attempt was made to critique the methodology adopted by the Directorate of Claims. Moreover at the time of the settlement, less than 50,000 persons, out of the 6 lakh claimants, had been assessed for personal injury and the exercise had no relevance to the quantum of compensation that had been arrived at in the settlement. It was in April 1989, at the hearings of the petitions on interim relief that the Madhya Pradesh Government first disclosed the results of its Personal Injury Evaluation (PIE). According to them, out of the 48,000 persons evaluated, only 4 had total and permanent disability*. The Supreme Court then used these arbitrary figures in its 4th May 1989 order to justify the quantum of compensation post facto. Once again we had been caught napping.

Critique of Personal Injury Evaluation A meeting held on May 20, 1989, resulted in the planning and carrying out of a medical study' in Bhopal with a specific aim of, among others, evolving a theoretical critique of the methodology of PIE adopted by the Madhya Pradesh Government and for providing an alternative method of assessment of injuries due to toxic gases. The study carried out in October 1989, with the help of large number of volunteers (with medical and non-medical backgrounds) and later published in December 1989 was one of the most important medical contribution to the legal case and formed the turning point for later events. For the first time, a medical study was planned by an independent group not merely for epidemiological purposes but with a view to its direct relevance in the Court.

Briefly, the process of assessment of injury,** adopted by the Directorate of Claims, was found to be faulty because • It depended upon an arbitrary scoring system which converted signs and symptoms, treatment and investigation findings into numbers (marks). No attempt was made to diagnose the disease from which the claimant was suffering. This incidentally rendered the medical records useless for the purpose of prognosis and rehabilitation. According to the guidelines, only if the score today was more than the immediate post exposure period, would the person be categorized as permanently injured. This meant that unless one's health was now worse than the period after 3rd December 1984, she/he would be labeled as "temporarily" injured. In contrast, since the medical examination for PIE was being carried out 5 years after the gas disaster, any person with a gas related illness at the time of PIE must be considered permanently injured. •

According to the PIE, a claimant was considered to be gas exposed only if she/he produced medical documents for the post exposure period. This was despite the fact that almost all persons exposed to the gases developed symptoms in immediate post exposure period and the hospitals and emergency camps

* Table 1 gives the categorization position as on 31.10.90. ** For a detailed critique and findings of the survey see "Against all Odds: The Health Status of the Bhopal Survivors", study coordinated by Dr C Sathyamala, Dr Nishith Vohra & K Satish with technical help from the Centre for Social Medicine and Community Health, JNU, New Delhi, December 1989.

were flooded in the initial period when there was no time for the 'niceties' of giving medical documents to the victims. A large number of victim claimants (i.e., almost 45%) were thus being categorized as 'no injury' even though they were ill today and could produce proof of residence in the exposed area, all because they could not produce medical documents for the post exposure period. •

•

The PIE was based on very few investigations which partially measured only one system's functioning i.e., respiratory. Even these (x-ray, PFT, ETT) were done for very few people, since according to the government "It is just not practicable to subject every claimant to these time consuming investigations in mass operations like this". On the basis of little or no investigations, it was unjust to categorize victims into 'no-injury' or 'temporary' injury. The same was true for specialist’s opinion also. Three of the categories in the personal injury evaluation concerned injury and three concerned disability. Injury is the manifestation of physical and mental illness while disability is the effect of the illness on the day to day activities of a person such as personal caring, ability to work, fulfillment of social roles etc. (The medical definition of disability is different from that defined as part of Workman's Compensation Act etc. where disability is measured with reference to occupation. In the case of Bhopal however, a group of people were exposed to the toxins at their residence and not at their work place. Hence a different method for assessing disability needed to be used). Part B of the medical record which alone formed the basis for categorization, did not contain any information about the claimants' ability to carry out routine day to day activities including occupation. Thus the categorization of injury into disability, on the information in Part B of the medical record was arbitrary and subjective.

The implication of the findings in the survey against all odds" was that the process of PIE followed by the Directorate of Claims could not form the basis of estimating the quantum of compensation and that the method was designed to under estimate the nature and severity of injuries. Instead, the methodology should be to arrive at a clinical diagnosis. This allegation, that PIE was designed to underestimate injuries, was borne out when the figures provided by the M.P. Government in the

interim relief petitions, closely matched the arbitrary figures presented in May 4 order of the Supreme Court setting the case. At the time of the publication of the 'study in December 1989, two major events took place. Firstly, the National Front Government came to power and secondly, the Supreme Court delivered its judgement on the validity of the Bhopal Act. Change in 'Political Climate’ The change of government created an euphoria among the activists because it was believed that the necessary political climate for setting aside the settlement reached by the Congo (I) government was present and that it would merely be a matter of time before we achieved our heart's desire. To an extent the euphoria was justified at many of the leaders in the newly elected government had opposed the settlement in definite terms and had participated in signature campaigns against it when they were in the opposition. There was now a need to evolve a legal strategy. One of the important pre-condition for setting aside the settlement was to provide interim relief to the victims as the "compelling need for urgent relief' was, according to the' Supreme Court, the motivating factor behind the settlement. According to the critique of PIE, the method of PIE adopted by the Directorate of Claims could not be accepted for estimating the numbers of injured or for assessing the severity of injuries, and it followed that it' could not form the basis of interim relief also. Since the need for relief was urgent, as 5 years had passed, and the reassessment of all the 3.5 lakh claimants (claimed t" have been medically examined by the Madhya Pradesh Government) was not possible, the demand of the victims groups was that "all persons exposed to the toxic gases on the night of 2/3 December 1984 especially those from the 36 wards declared by the Madhya Pradesh Government to be directly gas affected must be paid interim relief. Besides that, there could be certain other categories of victims residing outside these 36 wards who would justifiably deserve to be paid interim relief. The scientific basis for this demand came from the study carried out by us. In the absence of a just and scientific method of assessing injuries at this point of time, the distribution of interim relief on the basis of proof of residence in one of the gas affected localities alone could be considered just and fair.

Concerted lobbying with this as one of the demands to the newly elected government resulted in their announcement in January, 1990, that interim relief would now be paid on a monthly basis @ Rs. 200/- per person to all the residents of the 36 wards irrespective of their categorization status for the next 3 years, and the National Front Government vowed to do its best to unsettle the settlement because "Indian life was not cheap". Euphoria knew no bounds and the feeling was amplified further, when Mr. Soli Sorabjee, who was to appear on behalf of the groups filing the writ petitions against the settlement, was appointed as the Attorney General and would now appear on behalf of the government in the review petitions. While it did somewhat appear to be contradictory (how can a Counsel who was to argue the use on behalf of the victims one day accept to argue on behalf of the government the next day accept again it was believed that only 'good' could came out of this move. It was believed that perhaps by appointing Mr. Sorabjee as the Attorney General the newly elected government was indicating its seriousness in opposing the settlement. The appointment of the Attorney General resulted in two gains fop' the victims. Firstly, for the first time, the Annual reports of ICMR on the impact of gases on the human population along with reports of other government agencies on the gas disaster, was taken out of the confines of the Official Secrets Act and made public, and secondly, the medical folders, prepared by the Directorate, of Claims, of the victims surveyed in the study "Against all Odds" were made available. While the availability of these information was essential for developing the legal arguments, they in themselves were inadequate. Need for a New Legal Strategy As stated earlier the quantum' of compensation was related to the numbers of injured and the severity of their injuries. The medical data available were, the assessment made by the Directorate of Claims, the ICMR studies, and the survey "Against all Odds". The assessment of injuries by the Directorate was faulty and if used, would lead DCC to contend that the compensation amount paid by them was far greater than justified by the assessment*. The ICMR Studies could not be used because (a) they were known to the government before the settlement took place (b) they contained qualitative information on the nature of injury which was important

for relief and rehabilitation purposes but not for estimating the numbers of injured and (c) the long term epidemiological study was so badly designed that the results were suspect and had been critiqued earlier, to no avail, by the minority group of the. Supreme Court Committee set up in November 1985. The study "Against all Odds" was based on a sample of victims, though randomly selected, represented only 12 of the bastis (11 seriously affected and 1 mildly affected areas) and since an independent group was involved it did not carry with it the official seal of approval. Thus, it was essential that the legal strategy of the government include (a) Withdrawal of the PIE carried out by the Madhya Pradesh Government. (b) ICMR be asked to file an affidavit suggesting the methodology of clinical diagnosis as the alternate and correct method of assessment of injuries. (c) ask for time from the Court to set up a commission which would synthesize the findings of ICMR's Study, other independent studies, would opine on the matter of categorization and work out an estimate of the numbers of injured. Further, because of the Bhopal Act which empowered the government to be the sole representative of the victims, it was essential that the government file its own review petition seeking the setting aside of the settlement. It was at this time conflict arose among those involved in the planning of legal strategy. Once again the recurrent motif in the discussion was "faith" in the government and the Supreme Court. It was believed that since legally, a review is just a matter of assessing the legality or the illegality of a judgement, it would be sufficient to show that the settlement was illegal by pointing out the illegal quashing of the criminal proceedings and by pointing out that victims had not received notice. It was believed that as long as even a part of the settlement was shown to be illegal, it would be sufficient grounds for reopening the entire case. It was believed that since the newly elected government had decided to support the review petitions of the victims this in itself

'" This is what the UCC counsel argued in the hearings on the review petitions, According to them they had paid 230 crores in excess. See Box 1 and Table 2 for calculations of compensation amount by UCC and GOI based on the same figures of injured.

would be sufficient to convince the Court that injustice had been done. As to the question of compensation, it was believed that there was no need to enter into that question because in any case the unsettling of the settlement was a foregone conclusion as we were now dealing with a 'friendly' government and opposing categorization would put it in the dock as it were, and getting into the nitty grittys of categorization would only lead to the confusion and mystification of the court. We were in fact told by the Attorney General, in categorical terms, not to raise the question of categorization in the Court. The scenario painted was that the victims groups would argue that notice had not been given to them, that the newly revealed ICMR studies and studies conducted by independent experts indicated grave damage to the system, that it was illegal to quash criminal proceedings and therefore the settlement should be set aside. The government on the other hand would argue that they supported the review petition of the victim groups because the verdict on the Bhopal Act had shown them their erring ways in not giving notice to the victims, that criminal proceedings should not have been quashed and in the light of the new ICMR studies it was likely that injuries may manifest after a latent period and therefore plead for setting aside of the settlement. In all this, the fact that UCC was a party to the settlement and would oppose the reopening of the case with all the resources it had in its possession seemed to have been forgotten. So too was the Supreme Court which had come under grave attack for putting its seal of approval on the settlement and would therefore be on the defensive.

reopener clause for injuries that may develop later on, develop in children yet to be born or who were born after the disaster. While the Attorney General argued that no court had the authority to quash criminal proceedings we argued that no court had the authority to dismiss civil proceedings of the future. We believed that even if the ruling party was different we were dealing with the State and that it would be dictated by its own concerns. True enough, the government of India refused to file a review affidavit and had no answer when both VCC and the Court repeatedly asked why they had not filed one. Worse, in their supporting affidavit they annexed the Madhya Pradesh Government's affidavit in the interim relief matters which waxed eloquent on the merits of categorization. By doing this, it was obvious that the government had no intention of doing away with this faulty process of assessment which would render more than 40% of the claimants ineligible for compensation. Hence, when the Court asked for the estimate of the numbers of injured all that the government could do was to juggle the figures around to "prove" that Carbide should pay 140 crores (70 million VS dollars) more! (See Table 2) Although BGPMVS strategy in the Court changed later to include the question of categorization we had lost an important edge in the case. And contrary to our belief, the Supreme Court was neither confused nor mystified when the counsel for the Madhya Pradesh Government took two days in the Court to convince the judges of the scientificity of their categorization, The Court was convinced.

So, where are we today? (This was in 1991) It was at this point of time, Dr. Nishith Vohra and I decided to part ways from the legal 'think tank' (Prof. Upendra Baxi) of BGPMVS (Bhopal Gas Peedit Mahila Udyog Sangathan) and BGPSSS (Bhopal Gas Peedit Sangarsh Sahyog Samiti) by intervening in the case as independent researchers. This was after we had failed to convince the other Counsels arguing on behalf of the victims that it was necessary to tackle the issue of compensation related to the numbers of injured. We did not accept the rationale (if it could be called that) of putting all our eggs, into the government's basket. In our intervenors petition* argued by Ms. Indira Jaisingh one of the key issue was the inadequacy of the settlement amount which was based on a gross underestimation of the numbers of injured. We argued for a

The verdict on the review petitions could lead to the (i) Reopening of the entire settlement, or (ii) Reopening of part of the settlement (i.e. uphold the

compensation amount but unquash the criminal proceedings), or (iii) Uphold the entire settlement. May be if we pray hard enough the court will reopen the entire case because if it does happen, it will not be because of our collective (!) strategy. What is more likely to happen, is that the criminal proceedings will be allowed while the compensation amount will be upheld. This is because the Court made it clear "See Box 2 for a summary of our submissions

that the unwillingness of the government to return the 470 million dollars, should the case be reopened in its entirety, would influence their decision. If this were to happen are we going to oppose it? Or, are we of the opinion that even with this limited gain we have won against the multinational DCC and this is justice enough. (If visions of Warren Anderson, Chairman of DCC, languishing in the Indian jails is what keeps us going then it is time to perish the thought because a corporation can only be fined, it cannot be arrested). Or, as some have maintained, are we of the opinion that it is better if compensation amount is not increased as it would only lead to over-consumption. Bhopal victims demand justice but they also need the money. Whichever way one looks at it, there is no getting way from the question of injury assessment and monetary compensation. The PIE evolved by the Madhya Pradesh Government is faulty and has been designed to under estimate the numbers of injured. Finally, it is this issue that will dominate till the-compensation amount is distributed.

POST SCRIPT As it turned out this paper was prophetic. The final verdict upheld the compensation amount but reopened criminal proceedings. The faulty categorization process was upheld as the most 'scientific' method of categorization of injuries. Thus most of the genuine victims of· Bhopal did not receive any compensation amount or if they did, it was grossly inadequate. For more than 50% of them, the interim relief for 3 years (approx-Rs. 7000/ -) formed the only compensation they received. Since' there was no pressure on the government to continue the monitoring of the health status of the victims, which would have been necessary had there been a reopener clause, the ICMR projects (bad as they were, they were the only information available) were discontinued a few years ago. Since most of the compensation money has not been disbursed because there are not enough 'injured', the government has gained a lot in foreign exchange, as befits its parens patria role.

I wish to acknowledge Ms. Indira Jaisingh for providing the guidelines for the use of scientific and statistical evidence for proof of injury in mass disasters; Dr. Nishith Vohra & Satish my colleagues and Subhash Bhatnagar & Imrana Qadeer for inspiring me to write this paper.

TABLE 1: AFFIDAVIT REGARDING PRESENT STATUS OF CATEGORIZATION* POSITION AS ON 31.01.89

31.07.89

30.08.90

31.10.90

(a)

No. of medical folders prepared

3,13,292

3,41,000 (Approx)

3,61,152 (Approx.)

3,61,966

(b)

No. of folders evaluated

1,38,558

2,04,000 (Approx.)

3,57,494

3,58,712 ~

(c)

No. of folders categorised

29,320

1,23, 560

3,57,485

3,58,712

(d)

Breakdown of cases categorised

_

_

_

_

(i) A- No injury

10,345

51,584

1,54,813

1,55,203

(ii) B-Temporary injury

17,321

64,0.64

1,72,77.6

1,73,382

(iii) C-Permanent injury

1,104

5,192

18,7.63

18,922

(iv) BD-Temporary disablement caused by temporary injury

451

1,629

7,153

7,172

(v) CD- Temporary disablement caused by permanent injury

_

310

1,277

1,313

Partial disablement

97

762

2,633

2,680

(vii) CF-Permanent Total Disablement

2

19

40

40

(vi) CE-Permanent

As per the present records the number of deaths has risen from 3787 to 3828 as on 31.10.1990. (* HANDED OVER BY THE STATE OF MADHYA PRADESH IN COURT ON 31.11.90)

BOX 1 Written submission by Union Carbide handed over in court on 6.8.90

• This Hon'ble Court emphasised that: "If the total number of Cases of death, or of permanent, total or partial disabilities or of what may be called 'catastrophic' injuries is shown to be so large that the basic assumptions underlying the settlement become wholly unrelated to the realities", the factual foundation of the settlement would be "seriously impaired".

• The M.P. Government figures also reflect 18,763 claims of "permanent injuries with no disability" which has no comparable Supreme Court estimate. Assuming that these claims areas serious as temporary disabilities, and compensating them on the same basis, an additional Rs. 117.26 crores would be needed. This would change the above total as follows:

The Court's May 4, 1989 estimates may be compared with 'the latest figures supplied by the Government of Madhya Pradesh as of June 30, 1990 as follows:

Supreme Court estimate Totals 55,000 claims (Rs. 500 crores)

Supreme Court May 4, 1989 Estimate

figures from M.P. Govt, as of June 30, 1990 on scrutiny and categorization

Deaths: "Approximately 3,000" (Rs. 70 crores)*

3,787 (Rs. 88.4 crores) **

Permanent Disabilities Total-s Partial 30,000 (Rs. 250 crores)

40 Total 2.663 Partial 2,703 (Rs. 34.9 crores)

Note: The settlement balance of Rs. 250 crores of the Rs. 750 crore settlement was allocated for (i) special medical treatment and (ii) an estimated 150,000 claims for minor injuries and loss of property in three payment categories of Rs, 20,000; 15,000 and 10,000 (with 50,000 claims in each category).

Sub-totals: 33,000 claims (catastrophic injuries) (Rs. 320 crores)

6,49Q claims

Thus, even by the comparison most favourable to the review petitioners, there was an overestimation of the "death" cum "Serious" personal injury claims by 21,317 claims, which leaves Rs. 230 crores available to compensate additional claims.

(Rs. 123.3 crores)

Temporary disabilities Disabilities (Total and Partial): 20,000

Latest figures of MP. Govt. 33,683 claims (Rs. 270 crores)

(Rs. 100 crores)

1,277 (permanent injury) Temporary disabilities 7,153 (Temporary injury) Temporary Disabilities 8,430 (Rs. 52.7 crores)

injuries of "utmost Severity" 2,000 (Rs. 80 crores)

0 0

TOTAL: 55,000 Claims (Rs. 500 crores)

14,920 claims (Rs. 176 crores)

The M.P: Government figures reflect 172,776 comparable claims for "temporary' injury" (minor injuries). The excess 22,776 claims are easily compensated on the same basis as the others by an additional Rs. 34 crores out of excess Rs. 230 crores leaving Rs. 196 crores to compensate additional claims. Note: (1) The above does not take into account the excess interest amount from October, 1986 - August, 1990about 40 millions US dollars nor does it take into account the future payments mentioned in para 28 of the

* Amount allocated by the Supreme Court to pay this category of claims. ** Amount required to pay actual number of claimants on the same

order of 4.5.1989.

TABLE 2: CHART HANDED OVER IN COURT BY UOI DURING THE HEARINGS ON THE REVIEW PETITION CHART SHOWING DIFFERENCE BETWEEN FIGURES ADOPTED BY COURT AND MEDICAL CATEGORISATION FIGURES AS ON 30.06.1990 S.NO.

Category

1. 2.

Deaths

3. 4. 5.

No. adopted by Court

3000 Permanent reduction Disablement Temporary reduction Disablement Injuries of 2000 utmost severity Minor injuries 50000

Rate of Compensation Crores 2.33 lacs 30000

Amount Actual figures awardbased on categorisation records· at same 70.00 3787 83,300/250.00

Amount awardable 88.24 18763

Amount to be increased decreased 18.24 increase 1156.30 93.70

20000

50,000/-

100.00

8430

42.15

4.00 lacs _ 20,000/-

80.00

2703

108.00

28.00 increase

100.00

172776

(-) 93.70 crores 57.85 crores (-) 151.55 crores

345.60 Total increase: (+) 18.24 crores 28.00 crores 245.60 crores (+) 291.60 crores

57.85

245.60 increase 140.29 crores

~

BOX 2 Brief summary of submissions on behalf of the applicant Dr Nishith Vohra &Dr Sathyamala in the Review Hearings 1. It is not possible to arrive at the number of persons injured without defining the criteria for determining who is injured and who is not and the criteria for assessing nature and extent of injury.

monitoring and follow up. Such a system can never reflect the correct number of persons injured. The result is that they have come up with an absurdly low and unrealistic figure of number of persons injured.

2. The criteria must be related to the nature of injury known to be caused by exposure to the toxic gases.

8. The only scientific method of evaluating is to take into consideration the nature of the disease/impairment and to periodically examine the same individuals over an extended period of time. The I.C.M.R. has conducted such studies. Based on these studies, they have arrived at percentages of persons who are injured indicating the nature of their injury.

3. The nature of injury known to be caused by exposure to the toxic gases is as follows:i. Toxic gases cause multisystemic injury i.e., injury to several organs and several systems. ii. Damage caused by toxic gases is irreversible and progressively deteriorating iii. Exposure to toxic gases causes damage to the immune system. IV. Previously asymptomatic persons become symptomatic at later dates. v. Possibility of unsuspected complications arising later cannot be ruled out. vi. Long term effects e.g., cancer, genetic damage cannot be ruled out. 4. Neither the settlement dated14/15 February, 1989 nor the orders of 4th May have spelt out the criteria for determining nature and extent of injury. 5. The order of 4th May, 1989 has not dealt with the nature of injury known to be caused by exposure to the toxic gases. The criteria for determining what is "Permanent total or partial disability", what is .a "temporary total or partial disability" what is a "minor injury" or an injury of "utmost severity" have not been spelt out in the said order. 6. The order of 4th May, 1989 has not dealt with the progressively deteriorating nature of injury, the fact that asymptomatic people are becoming symptomatic, the fact of damage to the immune system, the possibility of long term carcinogenic and mutagenic effect, the multisystemic nature of injury and the fact that a large number of persons suffer from permanent and irreversible injury. 7. As a consequence of this failure to spell out the criteria, the authorities under the Act have been entrusted with the task of spelling out the criteria and evaluating the injury caused to the victims. As on the date of the settlement, the M.P. Government had not categorized anybody. The criteria adopted by the State of M.P. and evaluation done after the settlements also do not take into consideration the nature of injury or the damage caused. Evaluation made by the State of M.P. is an one-time exercise without any provision for

Based on the said percentages, it is possible to estimate the number of the persons injured. The Court now has three sets of figures: • in the order of 4th May, 1989. • of the M.P. Government (which were not before the Court on 14/15 February, 1989). • estimates based on ICMR Reports & the Study made by the intervenors. (Which were not before the Court on 14/15 February, 1989). There is an irreconcilable conflict between these figures. This is because each has adopted a different criterion for defining and assessing injury.

The victims have never been given an opportunity to show that the criteria adopted by the order of 4th May and the M.P. Government are arbitrary and unrelated to the nature of diseases. In the light of-the judgement of 22nd December,' 1989, such an opportunity must be given at an evidentiary fairness hearing where the victims must be permitted to cross examine the experts who claim to have categorised the victims and also to lead expert evidence to show the correct criteria for evaluating injury. 9. A pronouncement on the fairness of the settlement. is impossible without an evidentiary hearing. 10. Several heads of injury have not been determined at all and have not been compensated (eg, impairment of fertility). 11. The settlement is bad because it ignores the need for long term monitoring and periodic review, and in fact blocks, any such monitoring or review and the need to compensate upon such review. Having regard to possibility of latent injury, the settlement is bad because it prevents ascertainment of damage when it occurs. There is thus a need for a reopener clause. .

From the Editor's Desk

A Requiem for Bhopal On September I.3 of this year, the Supreme Court came out with a verdict on the framing of Criminal charges against the main accused * in the Bhopal Gas Leak Disaster case. This judgement brings to an end, for all practical purposes, the possibility of pinpointing liability and punishing the guilty. The charges framed against the accused by the Sessions Court at Bhopal, later upheld by the MP High Court at Jabalpur, were: culpable homicide not amounting to murder in the death of 3828 or more human beings', causing grievous hurts to 21,694 or more human beings, causing of hurts to 8,485 or more human beings, and committing mischief by killing 2,544 or more animals. Quashing all these charges, the Supreme Court has stated that, on the basis of the evidence provided by the government, the "alleged" act of the accused, at best, could be considered the "direct result of rash and negligent act" carrying a maximum penalty of 2 years of imprisonment only. But it appears that the accused may not get even this penalty because it rests "ultimately on the evidence it is found that the act complained of was not the proximate and efficient cause of death and intervention of other's negligence has taken place the accused may get acquittal after facing the full-fledged trial". In effect all those who were responsible for the 'accident' that killed thousands of people and caused serious and permanent injury to more than 2 lakhs of people are going to be 'proven' innocent by the law of the land. And this in the absence of a trial which is necessary for establishing criminal liability. The judgement also appears to lay the ground for the 'sabotage' theory floated by the Union Carbide as the cause of the disaster by invoking "intervention of other's negligence".

In passing this order, the learned judges necessarily have had to contradict themselves. Elaborating on the charges of rash and negligent act, the judges accept that, "the material led by the prosecution ... prima facie shows that there were not only structural defects in the working of the Plant on that fateful night which resulted into this

grim tragedy". But the evidence was, according to the judges, considered insufficient to show prima facie, "that on that fateful night when the Plant was run at Bhopal it was run by the accused concerned with the knowledge that such running of the Plant was likely to cause deaths of human beings ... mere act of running a Plant as per the permission granted by the authorities would not be a criminal act. Even assuming that it was a defective. Plant and it was dealing with a very toxic and hazardousness substance like MIC the mere storing of such a material by the accused in Tank No. 610 could not even prima facie suggest that the accused concerned had knowledge that they were likely to cause death of human beings". Thus, according to the judges, even if the management had knowledge that they were running a defective Plant or that they were storing toxic materials in contravention to regulations, this knowledge could not be considered sufficient grounds to prove that the management had knowledge that such acts could cause death and. suffering.

With this judgement, a clear signal has been sent out to hazardous industries, particularly those run by Trans National Companies, that rules of justice need not apply to their operations in the country and that they can, literally, get away with murder. C Sathyamala * Accused No. 1- Warren Anderson, Chairman of UCC; 2 - Keshub Mahindra, Chairman of UCIL; 3-VP Gokhale, Managing Director of UCIL; 4- Kishore Kamdar, Vice President and Incharge of AP Division of UCIL. 5- J Mukund, Works Manager of the Bhopal plant; 6- RB Roy Chaudhary, Assistant Works Manager; 7- SP Chaudhary, Production Manager; 8- KV Shetty, Plant Superintendent; 9- SI Qureshi, Production Assistant; 10- VCC; 11Union Carbide (Eastern) Inc., Hong Kong; 12- VCIL. (VCCUnion Carbide Corporation; UCIL- Union Carbide India Ltd.)

•

Eveready battery was a well known consumer product of Union Carbide. Following the gas leak disaster in Bhopal, a campaign was launched, primarily by KSSP for the boycott of Eveready batteries. Campaign posters depicted the 'cat' (of nine lines!) in the battery with its jaws dripping with blood. As pant of the damage control activity, UCIL, sold its battery unit. The new Company is now selling the batteries under the same brand name 'Eveready'. As though to make a mockery of the sufferings of the people in Bhopal, the recent advertisement for the Red Eveready Battery plays upon the word "Red". "Give me Red", is their Slogan. It appears that they are not satisfied with the red blood they shed in Bhopal. Source: Illustration K Khanna in "Swasthya Aur Samaj: Ek Bhin Swat', C Sathyamala, N Sundharam, N Bhanot, 1996 KSSP poster, 1985

Views and opinions expressed in the bulletin are those of the authors and not necessarily of the organisation.


