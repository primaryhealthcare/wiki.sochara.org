---
title: "Maharashtra MFC - Aurangabad Meet"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Maharashtra MFC - Aurangabad Meet from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC175.pdf](http://www.mfcindia.org/mfcpdfs/MFC175.pdf)*

Title: Maharashtra MFC - Aurangabad Meet

Authors: Medico Friend Circle

175 medico friend circle bulletin September/October 1991 MAHARASHTRA NOTES SHAM ASHTEKAR (1) Recently there was a strike in Sasson Hospital in Pune, by resident doctors (MARD) for demands the kind of which were rather unheard of in trade unionism. They demanded that the meagre grants to this Government hospital be raised to fulfill standard requirements (drugs, equipment etc.) and to end the pathetic circumstances in which they have to treat the patients. Sasson Hospital is one of the major Govt. hospitals k1 the State of Maharashtra. The resident doctors continued the strike for 20 days but had to end it without much breakthrough due to the intervention of the State Chief Minister and Defence Minister (who belongs to Pune). Nevertheless they highlighted the apathy of the Government in this cause and brought it to the public notice. On the whole the Government did not seem much perturbed. The neglect of State hospital continues due to paucity of funds. MFC Pune h.1d supported the cause of MARD. (2) The Medical Colleges in the State of Maharashtra, except Pune and Bombay (where they still have honorary doctors); are experiencing an acute on Chronic Shortage of teaching faculty. The medical colleges in the backward areas like Nanded, Ambajogai, Yeotmal etc. are in jeopardy. A student in Aurangabad Medical College reported that the Aurangabad College now had only half the number of post graduate teachers and so the Post Graduate opportunities are halved. There is a general shortage of doctors in colleges. The reasons are: (1) Private Medical Colleges (now 13 in the state) have attracted experienced doctor/teachers in a big way though even here the faculty is not upto MCI standards. (2) The greener pastures of private medical practice. The consultation fees in private sector have gone upto Rs. 150, (the range being Rs. 30-150). In Pune/Aurangabad: the consultation fees are about Rs. 30-50. So four-five patients a day make Rs. 5000/- a month. A surgery here and there, attendance of inpatients etc. give an additional income.

So why subject to the tyrannies of Govt. Service transfers, bossing, reservations and public pressures? If medical colleges don't find enough doctors to teach and manage the show; the rural hospitals are all the more miserable. Nurses too don't like to go to rural hospitals. I was told by a nurses' union leader that out of the average of 8 nurses posted to rural hospitals, two never join: so of course the doctors. Rural hospitals are only empty buildings and likely to remain so for a long time to come. In Nasik more than 40% of the PHCs doctors' posts are vacant. Plus the fact that many nonallopathic doctors are manning PHCs; sorry Family Planning Centres. (3) Last week there was a death due to cardiac arrest (due to Anesthesia) in a Pune suburb, in the course of Arthroscopy performed on a young worker in a private nursing home. The angry friends and people demanded the' arrest of doctors including the anesthetist: and blocked the Bombay-Pune Road creating a high voltage traffic jam. The police ultimately arrested the three doctors and released them on bail. The doctors' association came out with the facts of the case of which the press took a sharp and oblique view. On Thursday the industrial holiday - the private doctors in the city observed a bandh due to the insult and injustice by way of arrests. The incident has sent shockwaves through Pimpri doctors. Yes, the situation is very complex. In the first place one should go into the merits of the event and whole of its management which courts might do afterwards. I talked to a few conscientious doctor friends in Pune about the incident. I myself have a private nursing home in a small place and can imagine it myself. Surgeries in small nursing homes (without monitors, fibrillators and many more doctors to help) have a real (though very small) risk of anesthetic deaths.

Anesthesia related death is a bolt out of the blue and most doctors are nervous about this possibility. Small nursing homes have no immunity against public outrage against such hazards. This implies buying of mere gadgets and indemnities but still there is no minimising this real risk; while it will add to costs of surgeries. Besides this is one risk which can almost kill a nursing home practice. The implications are heavily weighed against small hospitals and rural hospitals. Only government hospitals can suffer such risks. So here is one virus that can help the big in the health sector. Already newly post graduated doctors are finding it difficult to start hospitals on their own and are banking on the big hospitals for inpatient services; the arrangement is mutually useful. This two factors put together might alter the 'linear trends' in health sector and change the face of private health sector pyramid to that of a top heavy sector. (4) On rural internship: The rural internship still goes on. But the sentiment and reasons for its continuance are extinct. Some in PSM departments are now admitting that the present of rural internship is a facade: Neither interns nor PHC medical officers nor the department faculty take it seriously any more. The medical students find rural internship as a rest, paid holidays, a stupid encounter with the peasantry and 'India's villages' and encounter with the ugly government health machinery (An outgoing intern once said to the new incumbent "There are quite many rats and a medical 'Officer too in that PHC"), a PSM department tyranny and a rural 'window' at best, but no more a 'conscientious affair'.

At times the medical officers find interns good help to keep the show go on while continuing with their private practice unabetted, while others certify the completion without attendance at a consideration. The PSM departments are without enough means to supervise the programme. Internship was one exercise to orient our doctors to the countries realities. But there are other realities too- the enticing urban private sector, the lifeless villages, the ultra-modern high-tech medicine and so on. The rural internship can only 'earn you a guilt complex of being an 'Indian' in 'Bharat', if you insist in being sensitive. We have of course no answer to this. But I suggested to a PSM friend and teacher that for those interns who still want to have a real 'feel' of rural realities, there are still some honest friends working in remote areas on different issues, who are willing to share it with our medicos; not all voluntary agencies of course but a discreet list of honest friends known and yet to be known. This is the only thing we can offer as MFC. (5) There is now a Marathi play (Doctor, you too!) going into the intricacies and the complexities of medical practice today. The theme is a patient's fight against malpractice (Uterus removed without information, committed by a lady doctor) In litigation, the victim demands uterus for uterus to teach human values to the profession. The message to the profession is 'to be human' and not 'commercial executives'. There is a 'cliché' too, extolling preventive and rural development project against the high tech curative-invasive medicine. But in general this does the job-disturbing the doctors and the people. The picture of malpractice is very weak, when more blatant forms are the rule.

MFC; BACK TO MEDICOS ONCE AGAIN SHAM ASHTEKAR AND MANISHA GUPTE While talking to Dr. Asha Pratinidhi, (Head of the Dept. of PSM, BJ Medical College Pune) we conceived an idea of organising a regular interaction between undergraduates/postgraduates of PSM, and MFC members. Dr. Pratinidhi has squarely welcomed this idea. So we are putting forth an action plan for all MFC members to consider and help. It is like this. PSM dept. will give us time to talk to the students, postgraduates/faculty whenever we chance to come in Pune. This could be about once in a month or more frequently. We will accommodate this, whenever some of us are in Pune for some other job. Of course not all of us visit Pune that often. We shall try to travel at our own cost. For those who need, some help can be organised. Your turn may come once in a year.

We have chalked out a list of possible topics and resource persons within MFC. We have not taken prior consent of the resource persons. The list of topics and resource persons is open ended, Further, this experiment may be tried in other medical Colleges too if the dept. of PSM is sensitive to such things. The main theme is to share our views, experiences, information’s with students and postgraduates (in separate sessions same day). This might help both the "sides, let us hope so. Please contact Manisha Gupte for further information. Sham Ashtekar Manisha Gupte

Topic

- Padma Prakash, 1. Women's issues Manisha Gupte 2. Gynac disorders in rural - Rani Bang, Ratna Ashtekar community - Rani Bang', Vinay 3. Sexually transmitted Diseases 4. Rational Drug Policy & Politics of Drugs

Kulkarni -Anant Phadke, Anil Pilgaonkar, Meera Shiva, Nimitta, Bhat

-Amar Jesani, Ravi Duggal Mohan Deshpande - Dhruv, Mankad, Abhay Bang, Sham Ashtekar, Anant Phadke 8. Non Allopathic Systems in Health Cure - Darshan Shankar, Smita Bajpai, Vilas 9. Tuberculosis Nanal, Bhaskar Sathe - Ashok Bhargav; Uplekar 10. High Technology in - N. H. Antia Medicine 5. Private Sector in Health Core 6. School Health 7. Paramedical Programmes

11. Health Education

-

- Ashok Bhargav, Anant Phadke, Manisha Gupte

12. Family Planning

- Ramesh Awasthi, Padma Prakash - Ravi Duggal, Soniya

13. Heath Financing & Expenditure Gill' 14. Health Care Models -Sonya Gill, Shirish in other Countries Kavadi 15. Medical Education - Ravi Narain 16. Medical Ethics - Amar Jesani 17. Medical Students and -Shashikant Ahankari, Health/Community Ashok Belkhode 18. Law and Medicine -Mihir Desai 19.0ccupational Health - Vijay Kanhere 20. Insurance in Village -Ulhas Jajoo, Meeraai Health Care Chaterjee 21. Alcoholism -Abhay Bang, Manisha 22. Bhopal Gas Disaster - Satyamala, Nishith and us Vora 23. Reproductive Health - Vibhuti Patel 24. The Chhattisgarh - Binayak Sen Experience in Health Care

25. Blood Donations

- Dr. Goud

— Any other topics can be considered — Any body willing to involve himself is welcome

MAHARASHTRA MFC - AURANGABAD MEET In the process of revitalisation effects, it was decided to launch regional forum; and hence Mah. MFC.1hcfirst meeting of Maharashtra MFC was held near Aurangabad on 12th and 13th Oct. with the help of HALO, a medical student’s organisation in Aurangabad medical college. Forty six members attended through many could not come for the meet as was expected. But the beginning has been positively good. First of all, MFC could bring together medical students, Nurses, Health workers, voluntary workers and organisations, journalists etc. from various corners; and the discussions had a ground reality and it is good enough for the beginning. NGOs, students, HALO - all felt that such a meet was overdue and MFC has done well organising it. The theme was about rural health services and the focus was what we all can do to help people in this area. After a broad sketch of problems and possible lines of action as regards rural health services, there were deliberations on various aspects including the possibility of taking the issue to the people, through press, NGOs etc. The latter area is worth exploring through Mah. MFC. Now there is an ad hoc committee for running the affairs of the Mah. forum and the broad plan of action is as follows:

1. Taking the health services issues to the people through press, NGOs, meets etc and trying to generate public pressure for Change. Also link with other network& in Maharashtra and thus put the issues across to others working with people and try to identify 'consensus areas' to work on. , 2. To help NGOs, volunteers to update/upgrade their own programmes in village health services - through mutual sharing. 3. To bring together medical student bodies (like HALO) and needy NGOs to see if NGOs can get the right people for NGOs in field to man their projects and in the process help young doctors with

opportunities to work with rural organisations. 4. To reorient press, leaders, political parties, other activities in matters of rural health care and other related issues.

There are many more proposals that need attention, but these can be tackled once the organisation is set going. After going through the proceedings for two days I felt that there is a lot of in regional forums to make MFC go 'public' on health matters.

MINUTES OF THE GENERAL BODY MEETINGS HELD IN BOMBAY ON SEPTEMBER 7, 1991 The General Body Meeting of the MFC was held 'on Sept 7, 1991, following the XVII Annual Meet '(Sept. 5 and 6, 1991) at Vinayalay Andheri (East). 28 members of the M FC were present. The agenda for the GB meeting was as follows: I. Administration/Accounts. II. Steering Group Report. III. Revitalisation Process. IV. Bulletin. V. Mid Annual and Annual Meets. VI. Election of the convenor. VII. Election of the executive committee. VIII. Any other matter.

I. ADMINISTRATION/ACCOUNTS

The income expenditure statement for the year

The general consensus was that the MFC bulletin could carry an account of the financial status of MFC with an appeal to all members for funds. This would also help us to plan long term, rather that from year to year. All suggestions for fund raising and financial planning should be sent to Dhruv Mankad, so that a plan for financing and budgeting could be prepared before the mid annual meet in January 1992.

II. STEERING GROUP REPORT Of the many commitments taken up by the steering Group during the 'revitalisation plan of M FC some had been fulfilled. Many regional groups have been initiated. The report of their activities was presented by MFC members from these groups.

1990-91 was presented by the convenor and ratified.

1. In Gujarat, Rajesh Mehta reported that several

The audited accounts and budget were also presented and passed.

meetings of inteFe6ted persons had been held. The group had yet to decide as to whether they wanted to call themselves "M-FC". The participants wish to do constructive work by going to the people, but they wef1e not ready for confrontation. The group has discussed the sex determination bill in Gujarat. There was a local MFC group in Karamsad, but they had not felt keen enough to travel to Bombay for the Annual Meet.

The MFC expressed their gratitude to Vandana Shiva and to Meena and Bashir Mumdani for their financial contribution to MFC. Various members, as usual, took up the responsibility to raise money to cover the deficit. A collection of Rs. 16,000 was expected through this collective commitment by January 1992. The convenor mentioned that more than 70% of all financial commitments taken last year had been fulfilled and the entire financia.1 deficit covered. It was generally felt that regional issue based groups should raise their own finances. The MFC (audited) accounts need to be ratified by the general body within six months from the end of the financial year. Therefore, it was decided that henceforth the annual meets along with the general body meetings will be held in September each year. There was a discussion as to whether we should raise some money (Rs. 20,000 or more) through a concentrated fund raising drive, or whether MFC should have its' own corpus fund. A suggestion was made that non-present members should also be requested to contribute towards MFC funds or perhaps MFC membership could be raised to Rs. 250 per year.

2. Rajeev Lochan reported that in Indore and in

Bhopal, a group named "Adhikaar" was to be registered, shortly. This group would be independent of MFC, but constitutes people who are close to MFC thought. The formation of an issue based group is facing some problems in Bhopal. The members are thinking of a mobile "health van". 3. In Aurangabad the first meeting of the Maharashtra M FC was to be held in October 1991. Sham Ashtekar said that this was an effort to bring together rural and urban workers/activists who are working on, or are interested in health issues. The meeting which would discuss rural health services, was to be hosted by the medical students of the Health and Auto Leasing Association (HALO), Aurangabad.

4. The Bombay MFC group has been meeting regularly

every month during the past one year. About 12 of the 25 members of Bombay MFC are active, and there is now an effort towards restructuring of the group, to decentralise activities and to involve more people. Independent 'cells' had been created to study a) Human rights, b) Medical Malpractice, c) Regulation of private medical practice, d) Patient education and e) Population control. Each cell has ''a coordinator; elected after the mid annual meet held in Wardha in Jan. 1991. Legal action, press conferences/releases, workshops, talks by experts, slide; shows and documentation of events and experiences are the main strategy of action. The cell on regulation of medical practice conducted a newspaper survey on patients' responses to medical care. There was an overwhelming response to this survey. More than 400 filled in questionnaires have been sent by people, and the report is being prepared. This cell has also moved High court on the issue of Regulation of Nursing Homes and about the Six deaths in Bombay Hospital. They now want to support the case on the issue of private medical colleges being attached to government hospitals as co-petitioners along with the Maharashtra Association of Resident Doctors (MARD). The cell is also planning to take an active interest in the Maharashtra Medical Council (MMC) elections. The cell on Human Rights has participated in the inquiry on the brutal murder of two nuns, last September in Sneh Sadan, Bombay. Amar Jesani was part of the fact finding team; the report, which mainly deals with the role of the doctors and police in handling the case, has already been published. The cell on monitoring of medical malpractice helps victims with legal aid whereas the patients' education cell speaks to consumer group on related issues. A workshop on contraceptive technology was organised by the cell on population control, along with women's groups in Bombay. 5. In West Bengal, Smarajit Jana said that preliminary meetings towards forming a group had already taken place. These monthly meetings attended regularly by 8-10 people have discussed the issue of privatisation. There were 18 members in the West Bengal group. The Steering Group admitted to certain lapses on their part during the past six months. Eighteen pages of the Medical Education anthology were found missing after the copies were printed. The money loss has been recovered and the SG has prepared a note of apology which could be printed in the MFC Bulletin.

Increase in printing/binding costs and unfamiliarity with new printing technology were the main reasons for the problems related to the anthology. Dhruv Mankad agreed to prepare a set of guidelines for publication. The SG was an ad hoc body, created during the revitalisation phase, and thus the steering group resigned as on September 7, to make way for a new executive committee of the MFC.

III. DISCUSSION ON THE REVITALISATION PROCESS It was felt that MFC should take active interest in the training of CHGs and should work along with Open University for this purpose. Infact, there was a query from Open University, Nasik, as to whether NGOs were interested in being contact centres for training of para' medics and also to write on health issues. M FC should help to collect names of such NGOs. The need, to clarify whether our perspective had changed through the revitalisation process, was felt. Also MFC ought to work out the relationship of local / issue based groups with the national body.

IV. BULLETIN The MFC Bulletin is now a bimonthly, with six issues each year. The Xerox offset printing costs Rs. 1,400 per issue of eight pages, if the print order is of 500 copies. The circulation is around 380-400. The annual overheads for the bulletin are around Rs. 2,000 and printing /circulation of annual meet papers costs Rs. 5,000. The bulletin thus would require around Rs. 12,500 during the next year. . The editorial committee of the bulletin was dissolved. Sham Ashtekar would continue as editor of the bulletin, with concrete help from other members. Responsibility regarding provision of articles and funds would have to be shared. It was decided to update the mailing list and to centralise the subscription system. Various members took up the responsibility of preparing/providing articles for the forthcoming issues.

V. 1. MID ANNUAL MEET The general feeling of the group was that the mid annual meet should be regularly held at Wardha, unless there was enough justification to shift the venue. The mid annual meet should primarily deal with organisational matters, long term planning of activities and finances, and to review and continue the revitalisation process. One day could be devoted to discuss an issue of interest to a local group.

The dates of the next mid-annual meet are as follows:

Dates Venue Agenda

23rd, 24th'and 25th Jan. 1992. Yatri Niwas, Sewagram, Wardha 23rd Jan.: Organisational matters 24th Jan.: Sharing of experiences and- report of local/issue based groups. 25th Jan.: 1. Discussion on "Medical Education" 2. Follow up XVII Annual Meet.

It was felt that a follow up of the meet on Need for 1egulation of Private Medical Practice was necessary the Bombay group could decide before December 15, as to whether micro and macro costing could be discussed to build up a strong case for regulation. A write up could be circulated before hand to facilitate discussion.

V. 2, ANNUAL MEET , Three themes were proposed for the XVIII Annual meet to be held in September 1992. 1. Drug Policy 2. National Health Policy 3. Health Insurance Calcutta and Bhopal were the two proposed venues. Tentatively, it was decided that the next Annual meet would be held as follows: Venue : Calcutta Theme: National Health Care Policy Dates: Sept. 1992. Actual dates to be decided upon in

Wardha in Jan. 1992. The West Bengal team would confirm the above and make a presentation on the theme to the group in the mid annual meet at Wardha.

VI. ELECTION OF NEW CONVENOR Members from the Western Maharashtra area felt that Convenorship should now move out of this area to avoid "domination" of any group or region over MFC. Therefore the names of Ulhas Jajoo (Wardha), Binayak Sen (Raipur) and Smarajit Jana (Calcutta) were proposed. The majority of the groups felt that MFC posts have always meant work and responsibility without much power. Infact, the convenor should be chosen 'from an area that was most active in the process if 'revitalisation. The convenor ought to have a supportive group around him/herself and so it was decided that Manisha Gupte from Pune would take up the responsibility of Convenorship starting April 1992. It was also felt that a loose structure such as a steering/working/core group should be built around the convenor to facilitate day to day organisational matters and expedite correspondence and communication between members. This group would be constituted in the mid annual meet in Jan 1992.

VII. ELECTION OF THE NEW EXECUTIVE COMMITTEE Smarajit Jana, Rajiv Lochan, Prabir, Mira Shiva, Anil Pilgaokar (as ex convenor) and Sham Ashtekar (as editor of the bulletin) were nominated as the new executive committee.

A LETTER FROM NIROG 15, Tulsivan Society, Jivaraj Park, AHMEDABAD-390051. Phone : 418024. Dear friend, We have prepared posters for Health Education. These posters are available with lamination. Price of laminated poster is Rs. 6.00 per poster. If you purchase more than 10 copies it will cost Rs. 5.50 per poster. You must have received our earlier publications-booklets on weaning and' childhood diarrhoea and a poster on scabies and head lice. We would very much like to have your comments on these publications. Next booklet in this series will be on Acute Respiratory Infections 'of children.

From next month we will publish a series of booklets on women reproductive health. Earlier editions of this material were found very useful in educating rural women about their reproductive system. These booklets are designed to correct popular wrong notions about reproductive system. The series will be divided into four parts," Menstruation, Pregnancy and antenatal care, Child birth, and diseases of reproductive system. Project Nirog is started by a couple of voluntary agencies working in Maharashtra and Gujarat to cater to the felt-need of Village Health Workers (VHW) regarding reference material in their own language.

This project is being executed by ARCH, Mangrol, Ta Rajpipla (Gujarat). Under project Nirog the material is prepared by experts working a grassroots with VHWs and who are also in touch with the recent developments in medical science. The contents of material are tested extensively by a team of anthropologists, educationists and social workers at field level using various parameters. Every material undergoes many test editions

before it is finally printed.

.

We have also prepared life size models of digestive system and uterus. These models are found useful in teaching rural women about the body. Model o'-digestive system is priced Rs. 400/- and model of uterus (made of flexible rubber) is priced Rs. 20/-. Material in-'Marathi is also available CHETANAVIKAS, Gopuri, Wardha-442001.

from

During next three years we shall publish 10 posters and around 30 booklets on health problems which VHWs face in their day-to-day work. A list of titles is enclosed. Please place an advance order to plan us better. Payments should be sent in the name of ARCH at the address given above. With Season's Greetings, Sincerely Yours,

October 15, 1991

Ashok Bhargava Coordinator.

TITLES Booklets (Price Rs 2.5m Childhood problems 1. Weaning foods 2. Diarrhoea 3. Respiratory infections 4. Immunisation 5. Protein-calorie malnutrition 6. Vitamin-A deficiency 7. Worm infestation

Health of Women 8. Menstruation (Rs 4/-) 9. Pregnancy 10. Child birth 11. Iron deficiency anemia 12. Diseases of women Infections 13. Malaria 14. Tuberculosis 15.leprosy 16. Dysentery

17. Sexually transmitted dis- 29. Drinking water 30. Book on women reproeases 18. Inflections of eye 19. Inflections of ear

20. Boils and wounds Other 1itles 21. Family planning 22. Asthma 23. Ringworm 24. Burns 25. Snake bite 26. Backache 27. Sickle cell anemia 28. Sanitation

ductive health (Price

I

Rs. 15) POSTERS (Price Rs 6) 1. Scabies 2. Birth interval 3. Safe motherhood 4. Breast feeding 5. Child growth 6. Immunisation 7. Diarrhoea 8. Cough & cold 9. Home hygiene 10. Malaria

Subscription Rs. 50/- per year. Model of digestive system Rs. 400/ Model of uterus Rs. 20/ In 1978, the World Health organisation held a conference at Alma ata. The declaration of this conference asserted that health is a human right and that health care should be accessible, affordable and socially relevant. The basic strategy termed Primary Health Care involves community participation and the use of paramedical personal using simple but effective and appropriate technology. Components of Primary Health Care includes not only health promotion and personal curative and preventive services, but also education, water, sanitation, agriculture, and related economic development activities. Even after 13 years of this declaration a quarter million children die every week in developing countries. One of the causes of this tragedy is social synergy mainly the women's education and economic development. Another fundamental cause is that today's knowledge about protecting the health and growth of children has not yet been put at to disposal of the majority. An important link between scientific knowledge and people is Village Health Worker (VHW). There is a rapidly growing demand for trained health workers to serve the need of rural areas where doctors are rarely available. This has led to an urgent call for capable teachers and appropriate teaching learning material.

Please renew your subscription immediately if due. Please communicate the names/addresses of your friends who are not getting the Bulletin despite payment of subscription. This will help us streamline the process of mailing the Bulletin. MFC Bulletin invites articles on health and health related issues.

Editor MEDICO FRIENDS CIRCLE BULLETIN Editorial Office: Dr. Sham Ashtekar Dindori Dt. Nasik 422 202

Editors: Sham Ashtekar, Anita Borkar' Subscription/Circulation Enquiries: 1. Anil Pilgaonkar, 34-B Noshir Bharucha Marg, Bombay-400 007 2. Dr. Anant Phadke, 50, LlC Colony, University Road, Pune 411 016, India.

Published by Sham Ashtekar for MFC and Printed at Impressive Impressions, Nasik. Views and opinions expressed in the bulletin are those of the authors and not necessarily of the organisation. Subscription Rates: Inland (Rs.) a) Individual b) Institutional Asia (US dollars) Other Countries

Annual Life 30 50 6 11

300 500 75 125

Please add Rs. 5 to the outstation cheques. Cheques/M.O. to be sent in favour of MFC, directed to Dr. Anant Phadke, 50 LlC Quarters, University Road, Pune 411016.


