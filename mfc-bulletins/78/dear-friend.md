---
title: "Dear Friend"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Dear Friend from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC078.pdf](http://www.mfcindia.org/mfcpdfs/MFC078.pdf)*

Title: Dear Friend

Authors: Bang Abhay

medico friend circle bulletin JUNE 1982

Treatment of Acute Diarrhoea in Children Diarrhoea is the excessive loss of fluid and electrolytes in stool and its treatment can be conceived as two fold: 1)

Replacement of lost fluids and electrolytes - rehydration and

2)

Reduction of further losses of stool - - antidiarrhoeal therapy.

The first goal of rehydration with oral rehydration solutions (ORS) and parenteral fluids has been discussed previously (MFC Bull. 47 - 48.)

Antimicrobials-very limited role. This discussion is concerned with the many antidiarrhoeal preparations available in this country, usually advertised as infallible, suitable for all diarrhoeas and of low toxicity. Since most acute diarrhoeal diseases are both self - limited and short -lived, well designed double-blind and placebo controlled studies in which stool output or duration of diarrhoea are measured and compared are required to prove the efficacy of therapy. Tables 1 and II summarize selected therapeutic trials A plus sign in the efficacy column indicates that stool outputs were clinically and statistically significantly reduced. Table 1 present’s one classification of antidiarrhoeal preparations, lists examples, describes efficacy as demonstrated in therapeutic trials and lists side effects. t 2-4) The secretion-reducing drugs are potentially useful in the secretory diarrhoeas caused by V. cholera- or toxogenic E. coli., and are all currently experimental. Note that only ORS has unquestioned efficacy with low toxicity. ORS is the mainstay as it actually meets both goals of diarrhoea treatment. Antimicrobial therapy may also be considered antidiarrhoeal, but since each organism must be treated

specifically efficacy data is presented by organism Jl1 Table II. Viruses probably are the most important cause of diarrhoea in children; the organisms listed account for a minority of cases in most locales. Antimicrobial therapy may also reduce the excretion and spread of organisms and this is detailed as "effect on duration of positive culture." The antimicrobials selected here are those to which the listed organisms are usually sensitive in vitro. These antimicrobial sensitivities vary and local patterns on bacterial resistance should be monitored to select effective drugs. Note that only diarrhoea associated 11 14 with Shigella. ( ) V. cholerae, (12, 13) giardiasis ( ) and 15 amoebiasis ( ) are unequivocally benefited by antimicrobial therapy. It is important to note that six well designed trials of antimicrobials for uncomplicated Salmonella gastroenteritis all showed no effect of antimicrobial therapy (5-10). Some of the more recently discovered organisms have not yet been subjected to controlled therapeutic trials. In general a child with diarrhoea should receive ORS. Current antidiarrhoeal preparations, despite their long history of usage are ineffective or too toxic. Although most diarrhoea in children is infectious, the use of antimicrobials is currently justified only in those children who have severe cholera or shigellosis. Fortunately, bacillary dysentery and cholera are relatively easy to recognize clinically. Amoebiasis and giardiasis can be identified with simple microscopy and also respond to antimicrobials. Nonspecific or unidentified diarrhoeal disease is unlikely to improve with antibiotic therapy.

M. C. Steinhoff, Department of Child Health CMC Hospital Vellore-632 004

In summary, current understanding of the pathogenesis of diarrhoea and recent careful studies of therapy indicate the following: 1) ORS is safe for all children with diarrhoea. 2) Most antidiarrhoeal preparations are ineffective,

3) Only cholera, shigellosis, giardiasis and amoebiasis should be treated with antimicrobials. The clinician's problem is that he/she cannot know the etiology of every case of diarrhoea, making therapeutic choices difficult. I think this difficulty can be eased somewhat by realizing that only a minority (Continued on page 7)

some, such as Lomotil, are too toxic for children.

Table I Antidiarrheal Therapy Type

Example

Adsorbents (2)

Kaolin, pectin, attapulgite, bismuth salts.

0

Anticholinergics

Atropine, hyoscyamine

0

Opiates (3)

Codeine, tincture of opium, Lomotil, Immodium

±

Lactobacillus (4)

Curd

0

oral glucose-electrolyte fluids

+

Aspirin, chlorpromazine

±

Absorption-increasing

Secretiondecreasing (experimental)

Efficacy

Side Effects adsorption of antibiotics and other drugs. Salivary, ocular, and cardiac parasympatholytic effects respiratory depression, coma, prolongation of shigellosis none Hypernatremia possible salicylate toxicity, hypotension, dyskinesia.

Table II Antimicrobial Therapy Organism (reference)

Escherichia coli enteropathogenic enterotoxigenic Salmonella spp (5-10) Shigella spp (11) V. cholerae (12, 13) giardiasis (14) amoebiasis (15)

Selected Antimicrobials.

Decreased duration, volume of diarrhoea

Decreased duration of positive culture.

ampicillin, T /S tetracycline, T/S

± ?±

?+

chloromphenicol, ampicillin neomycin, amoxycillin

0

0

T /S, nalidixic acid tetracycline, T /S metronidazole metronidazole

++ ++ ++ ++

++ ++ ++ ++

? = controlled studies have not been done in children TIS = trimethoprim-sulfamethoxazole

±

RICE POWDER AS AN ALTERNATIVE OF SUCROSE IN ORAL REHYDRATION SOLUTION Oral rehydration solution (ORS) has been used successfully in the management of diarrhoea. It has been found that glucose and sodium are coupled in the small intestine and glucose accelerates the absorption of solute and water. Sucrose replacing comparatively expensive glucose in the ORS for all practical purposes has been found to equally satisfactory. Recently Dr. A Majid Molla of ICDDR, carried out a study to examine the efficacy of ORS using a cereal such as rice powder, in place of sugar. The WHO recommended formula for ORS was used, sodium 90 mMol, chloride 80 mMol, potassium 20 mMol, bicarbonate 30 mMol per litre. Sucrose (40g) was replaced by 30g of rice powder. Invitro hydrolysis converts 80-86% of the rice powder into glucose giving the WHO recommended amount of 20g of glucose to be liberated in the intestinal lumen. Rice powder was dissolved in water and cooked for a few minutes to make a smooth liquid Electrolytes and enough water were added to make one litre of solution. The stool output of the sucrose group was in general higher than the rice powder group. The success rate in the rice powder group was almost same as the sucrose group. Most of the failures were in cases where the intake could not match the output due to excessive purging and/or vomiting. They were transferred to intravenous therapy. The degree of dehydration and the purging rate were directly related to failure of the ORS to correct dehydration. This was also true for standard ORS (ORS with sugar). The advantages of using rice powder for the ORS are many. Starch is rapidly hydrolyzed in the intestinal lumen by (salivary and pancreatic) amylase to glucose, maltose, maltitriose and 1 branched dextrine . These carbohydrates are further hydrolyzed to 2 glucose by the maltases of the brush border of the enterocyres . Even one-month old infants can digest and absorb a large amount 3 of starch as most of the active disaccharidases are fully 4 developed at birth . Intraluminal digestion of rice powder used in the ORS liberates monosaccharide glucose slowly, it causes no osmotic diarrhoea, as seen when sucrose of glucose exceeds the recommended amount in ORS. The possibility of increasing fluid loss through osmotic diarrhoea also limits the amount of sugar used in the ORS with rice powder releases the glucose gradually and slowly in the intestine, it negates the possibility of causing an osmotic drag of fluid from

the vascular space to gut lumen. This finding opens up the possibility of using a higher concentration of carbohydrate in the ORS, which in addition to providing glucose as the vehicle for the transportation in the absorption of the electrolytes would also provide some energy. Rice is an unique starch containing the mixture of two different polyglucoses, amylose and amylopectin. It has 7-10% protein and very little electrolyte. As mentioned earlier, acid hydrolysis converts 80-86% of the rice powder into glucose. There are important amino acids in the protein content of rice: glycine 30-36 mg; lysine 30-34 mg; leucine isoleucine 30-40 mg 5 per 100 gm of rice . Glycine has been known to promote 6 transportation of sodium from the intestinal lumen . (Despite the protein content of rice, rice is not a rich source of protein in the diet and the amount of glycine may not be sufficient to promote the absorption of sodium). The efficiency of the specific intestinal enzymes to hydrolyze rice powder remains at a satisfactory level during diarrhoea due to V. cholerae and E. coli-post hydrolysis sugar content in stools passed in 24 hours remained similar. Studies on assimilation of nutrients have demonstrated that carbohydrate absorption from a rice meal is least affected during diarrhoea caused by cholera and enterotoxigenic E. coli; even in case of invasive organisms like rotavirus or shigella, this remains excellent. Rice is the staple food of 60 % of the world's population. In all the countries of Asia, where 50 % of the world's population live, rice is grown and eaten. In addition, rice is also the staple food to millions of people in Africa and Latin America. Most of the developing countries, where diarrhoea is a big problem are located in these areas. In Bangladesh one of the traditional treatments of diarrhoea is to feed the patient soaked flattened rice (Chira) to which salt and sugar has been added. Rice is readily available even in the poorest homes. It is also a traditionally familiar component of treatment of diarrhoea, hence would pose no cultural barrier in its acceptance. Since there is no adverse effect like osmotic drag ill the intestine, it is desirable to place more than 30 grams of rice powder per litre into the ORS. The patient should drink enough fluid to match the output. The higher starch content poses no hazard and actually has a potential benefit of added caloric density. [To be continued]

Dear Friend, I read with great interest Dr. C. Gopalan's comments on my article ‘Food requirements as a basis “minimum wages.' I am very encouraged to note at –

1) The issue has interested an eminent nutritionist like Dr. Gopalan. 2) The final figure of minimum wage arrived at by DR. Gopalan's calculations is almost same as that of mine. 3) The balanced way in which he has reacted is quite is lesson for us in MFC who often react in a very aggressive and emotional fashion which creates more eat than light. Even if the final figure is same, I dare to differ on some points with Dr. Gopalan. 1)

Energy Requirements: - Dr. Gopalan

assumes that the average body weight of male labourers is 44 to 46 kg. instead of 55 kg. as assumed by ICMR is reference body weight for Indian man. He has taken this figure from 2 studies. The sample size in these studies is 6 adult males in one and 30 in another. These studies were primarily designed to measure the calorie expenditure of male labourers by doing metabolic studies. For such tedious studies obviously sample cannot be too large. Hence these studies are useful for knowing the calorie expenditure of labourers of the given body weight, DR. Gopalan is not justified in using the average weight of the small sample from this study as the average weight of crores of Indian males.

destined for a low body weight as was previously thought and if provided with adequate food and other care, an Indian child matches the Western standard of growth & development. Hence it is chronic undernutrition which has resulted in our present underweight I pigmy' population. Unless this class is provided with more food, the underweight will persist So one should provide the food required for person of optimum body weight (55 or 65 kg) to break the present bottle neck and allow the labourer's children to grow to their fullest physical potentials. When treating a child with Marasmus, do we ever say that he should be given calories according to his present body weight? On the contrary he is given nutrition according to the expected body weight for his age so that he can grow to that optimum level. While calculating the calorie requirements for the unemployed period of the labourers, DR. Gopalan has assumed that their calorie requirements are those of sedentary persons. The off duty work and house hold work that labourers, specially females, have to do is much more strenuous than a sedentary class person has to do The ASTRA study of rural energy patterns has shown that rural women do on an average 8 hours of domestic labour (collection of fire wood, fetching water, cooking, carrying husbands food, livestock grazing) expending 1010 calories per day on this work alone. Obviously, even on unemployed days, the labourers need more Calories than a sedentary person. Batliwala has recently (EPW, Feb. 27) proposed an interesting approach to bridge the energy deficiency by cutting down domestic labour by providing amenities like electricity, water supply, easy fuel to the rural people so that this huge energy expenditure on domestic work is saved

I do not know why ICMR has taken 55 kg. as reference weight for Indian males. I am also not aware of any study which gives average body weight of Indian labourer, taking wide sample on National level. Hence I am not in a position to comment what is average weight of Indian labourers but obviously above two studies can not be used for this purpose.

DR. Gopalan has also not made any calorie allowance for the periods of pregnancy and lactation. As Kamala Jayarao has shown in her article. “Who is malnourished; Mother or the woman?” An average woman spends about half of her reproductive life (15 to 45 years) either in pregnancy or lactation.

The second question which crops up is are we going to provide food for the existing low body weight and thus seal the fate of Indian labourer at the present low body weight? As the studies at NIN have shown, Indian, by heredity or constitution are not

Thus it appears that Dr. Gopalan has underestimated the calorie needs of the labourers. This underestimation is further aggravated severely by the small family size of 4 as assumed by him.

2) Family Size:

3) Balanced diet

Dr. Gopalan supports his calculating food allowances only for a small family by saying that it is consistent with National policy. Who decided this National policy? How is this figure of two children decided? Has any thought been given to why do poor need and produce more children? When 25 to 30% the children die before the age of 5 yearsand this average figure will be still higher for the poor classhow can we enforce that poor should stop at 2 only? It is now fairly accepted that poverty is the main reason, for higher birth rate. Hence the slogans like 'Development is the best pill'.

Dr. Gopalan has aimed at providing a balanced diet to the' labourers. It is most welcome. As I was operating within the framework of Page committee, I couldn't venture to ask for a balanced nutrition and argued only for cereals and pulses. But let me point out that the cost of balanced diet taken into calculation by DR. Gopalan is one prevailing in 1979. By 82, the costs have scaled up by at least 40%. For calculations of minimum wages today prices of 1982 have to be used.

If food allowances are made only for 2 children because such is the National policy now, what will the poor do with their already existing extra children? Starve and kill them? Incidentally many of these ‘surplus' children were born when the National policy was of 3 children or when there was no National policy. By making allowances for a smaller family of 4when the reality is (hat the poor have a family size of 5.6, the allowance for 4 will be distributed in the family over 5.6 persons, obviously frustrating DR. Gopalan's efforts to provide minimum standard of life to the poor and perpetuation of poverty will frustrate all the efforts to achieve the' National goal' of family size of four. It is a self defeating proposition. While one should agree with DR. Gopalan that small family norms should be achieved, the methods have to be different. Even though I am not proposing an indefinitely large family, let me just point out that for the purpose of land ceiling or urban wealth taxes, there is no limit on the family size. The rich have the facility to have more children to save their wealth in the organised sector, the wages are calculated for a family of five. In such context, restricting the minimum wages of the poor so that they call maintain only a family of four does not seem justified. While Dr Gopalan states at one place that "many of the assumptions in the above calculations are based on a appreciation of the hard current realities and not on "idealistic" consideration," one fails to understand why he doesn’t accept the hard fact about the existing family size of poor.

4) Less wages for women.

The difference in wages for males and females proposed by DR. Gopalan is not justified on the basis of difference in work output because men and women do different types of work. Men do physically strenuous work while women do more skillful and tedious types of operations. The output of two different categories cannot be compared. But women usually put less hours of labour (they go late on fields due to their domestic duties) and on this ground unequal division may be considered. DR. Gopalan has touched the heart of the whole problem when he stated "The prescription of minimum wages will have only academic value if there is no machinery for strict enforcement." In our are we are facing difficulties. In trying to enforce even the existing minimum wage act of 4.5 Rs. per day. How the minimum wage of 12 Rs. can be actualised? But then this is the next inescapable logical step of all this exercise. May be other people can take over this responsibility than we the medicos.

Abhay Bang

1) The average body weights of rural adults are 46-50 kg (males and 40-44 kg, (females): National Nutr. Monitoring Bureau, 1980). The figure 55 kg. was fixed by ICMR arbitrarily when data on Indian adults were not available. Now ICMR has attempted to make suitable alterations. 2) The comparison of energy provision for adults and for a marasmic child is not correct. Since children possess the growth potential, their requirements are calculated on “ideal weight." For adults, since maximum growth has stopped, calculations are made for "actual weight"

-Editor

DIALOGUE During the last five years there has been a lot of discussion on the new concept of treating diarrhoea. The WHO produced a nice little booklet on this subject in 1976. One quarterly newsletter 'Diarrhoea Dialogue' is being produced by a WHO collaborating centre. In its turn MFC has published an article ‘Oral Rehydration' in Nov-Dec. issue, 1979. We are familiar with the name of' ORT " 'ORS' etc. - we, doctors, even have memorised how many mEq of Sodium or Potassium arc there in one litre of WHO recommended oral fluid. So what? Pharmaceutical companies are still producing 'rubbishy' anti-diarrhoeal preparations; doctors are prescribing 'Chlorostrep', 'Pectokab’, ‘Streptomagma', sometimes with 'Electral '. Saline drips are immediately put in a bit more severely dehydrated cases; 'quacks' are putting up a saline drip for any case of diarrhoea and charging poor villagers Rs. 10/or more. With this background, I want to fit in Somra somewhere - I find it difficult. Somra wants himself to be fitted in the more remote villages, on the distant village markets on his old noisy bi-cycle with a tin-box and pictures. He explains to the villagers gathered around him, sometimes in tribal language, sometimes in Hindi‘Take one litre of water, boil it, let it cool; then add the whole content of the packet, stir it and start drinking so long as diarrhoea continues. In case of vomiting…' Tora can fill this gap correctly, he knows what to do in case of vomiting. Once he brought his son 12 yrs. old with severe diarrhoea and frequent vomiting. At first Tara went to Somra and started giving oral fluid-but the vomiting was a nuisance. In the evening we again prepared another litre of rehydration fluid and started feeding Tora's son with a tea-spoon exactly at one minute interval. After five such feeds we stopped exactly for five minutes and started again. Soon Tara had adopted the time intervals and continued in the same manner throughout the night. In the morning the boy walked back home, one mile away, with his father. Let us go back to Somra. He explains' In case of vomiting, give the fluid slowly with a spoon just like in this picture, but never stop fluid in diarrhoea and vomiting.' Somra is a village health worker and it would not be an exaggeration to say that he has saved many lives with his rehydration packets. He knows this and so is concerned to make them as widely available as possible in remote village homes and in village shops and markets. His work seems far removed from those

places where intellectual discussion take place about ORT, where, for example, the merits of oral rehydration as a 'home remedy’ are expounded and it is explained that we must not make villagers dependent on packets, we must use local ingredients. 'But, Somra says, in many homes even sugar is not available, do we then use salt and water?' Does that mean that a serious case has to be transported to the centre for a drip to be put up and antibiotics given? Somra knows that he can treat even serious cases with his rehydration packets and without antibiotics... And more importantly, he is promoting a tool with which the villagers can fight against the doctors and quacks who exploit them. Even now, diarrhoea is a killer in many villages in many parts of India. In the villages it does not kill only the patient, but his/her family too. The exhorbitant charge (often between Rs. 200/- to Rs. 3001- ) made by the doctor or a quack ( no difference) for a saline drip and few injections of 'vitamin B camp,’ “Baralgan" results in losing land, property, animals. What causes diarrhoea? There is no doubt that we'll have to find out the causes ('aetiopathology’) before going to its treatment. The answer is - insecurity and exploitation in earning, lack of proper housing, ignorance and superadded by infection of enterovirus, shigella, E. coli and what not. Now treatment. We speak of anti-diarrhoeal preparations, we say ‘Ban Lomotil', at the best we try to adopt' scientific and appropriate' oral rehydration therapy. In fact, all our discussions are centred around the secondary-infection; we forget the primary cause. I know that I am going into some other discussions. Some may object: ' Now you are talking about something which is not the job of the great (!) medical profession. Some may say: 'This is a political discussion', some will simply agree - I would call it intellectual nodding. So again to Somra. I talked to him on this point. Somra knows the primary cause very well- the question is how to treat it. Let all Somras meet and discuss it.

Manan Ganguli, Jagadishpur, Bihar In his enthusiasm about ORT and CHW, perhaps Manan is exaggerating a bit. Antibiotics and doctors too have their place. Somra alone cannot decide whether ORT alone is enough or whether and which antibiotics are needed. Home made Jaggery - salt sodabicarb - water solution has some advantages and some drawbacks compared to the ready-made packets which Manan is campaigning for. A more objective view point is needed.

Editor.

(Contd. from page 2) of cases will be benefited by antibiotics. Therefore, the clinician should use antibiotics only in those severe cases which have a high probability of being caused by one of the four organisms mentioned above. Thus no mild case should receive antibiotics. Cases of very frequent watery stools with vomiting which may be cholera should be presumptively treated. Incidence of cholera varies across this country and local patterns will help a decision about cholera. (We at Vellore get it in sporadic outbreaks). Severe bloody dysentery with tenesmus and fever is probably shigellosis, and should also be treated. (Local patterns vary for dysentery also, amoeba are rare in Vellore). Chronic diarrhoea may be giardiasis or amoebiasis and should also be treated. These rules lead to the presumptive treatment of only a minority of diarrhoea cases. REFERENCES 1. WHO: A manual for the treatment of acute diarrhoea. World Health organization (WHO/ CDD/Ser/80.2) Geneva, 1980. 2. Portnoy BL, Du Pont HL, Pruitt D, et al: Antidiarrhoeal agents in the treatment of acute diarrhoea in children. J. Am. Med. Ass. 236: 844-846, 1976 3. Du Pont HL, Hornick R: Adverse effects of Lomotil therapy in Shigellosis. J Am. Med. Ass. 226: 1525-1528, 1973 4. Pearce JL, Hamilton JR: Controlled trial of orally administered lactobacilli in acute infantile diarrhoea. J. Pediat. 84: 261-262, 1974 5. Mac Donald WB, Friday, F, McEacharn M: The effect of chloramphenical in salmonella enteritis of infancy. Arch. Dis. Child 29: 238-241, 1954 6. Petterson T, Klemola E, Wager 0: Treatment of acute cases of Salmonella infection with ampicillin and neomycin. Acta. Med. Scand. 175: 185-190, 1964. 7. Effect of neomycin in non-invasive Salmonella infection of the GI tract. Lancet 2: 1159-1161, 1970. 8. Kazemi M, Gumpert TG, Mark MI: A controlled trial of Salfatrimethoprim, ampicillin and no therapy for salmonella gastroenteritis. J. Pediatr. 83: 646-650, 1973. 9.

Olarte DG, Trujillo SH, Agendelo ON, et al : Treatment of diarrhoea in malnourished infants and children. Am J. Dis child. 127: 379-388, 1974 10. Nelson JD, Kusmiesz H, Jackson LH, et al Treatment of Salmonella: gastroenteritis taiw ampicillin, amoxicillin, or placebo, Pediatrics 65: 1125-1130, 1980.

II. Nelson JD, Kusmiesz H, Jackson LH, et 31: Comparison of trimethoprim- sulfamethoxazole -and ampicillin therapy for shigellosis in ambulatory patients. J. Pediatr. 89: 491-493, 1976. 12. Lindenbaum J, Greenough WB. Islam MR: Antibiotic therapy of cholera in children. Bull WHO 37: 529-538, 1967. 13. De S, Choudhuri A, Dutta D, et al: Doxycyline in the treatment of cholera. Bull WHO 54: 177179. 1976 14. Levi GC, de Avila CA, Neto VA: Efficacy of various drugs for treatment of giardiasis. Am. J. Trop. Med. Hyg. 26: 564-565, 1977. 15. Rubidge CJ, Scragg IN, Powell SJ: Treatment of children with acute amoebic dysentery. Arch Dis Child 45: 196-197, 1970. Only 13 out of 51 Commercial Antidiarrhoeal found useful. Nitin Sane a MFC-subscriber in Pune has studied 51 commonly used commercial preparations sold as antidiarrhoeals to find that only 13 of these contained ingredients (in adequate doses) whose efficacy has been scientifically proved. The rest 38 preparations were found to be useless on account of various reasons. Their breakdown is as follows-

Preparations which are useless because they contain Drugs in insufficient amount-20 products. Irrelevant drugs for example chlorpheniramine 9 products maleate) c) Drugs of doubtful value. (for example-pectin, kaolin) ... 25 products d) Drugs not indicated in diarrhoea (for example-streptomycin …... 21 products. e) Drugs which should not be used on account of their toxicity (for example-diodoquin) ...............11 products. f) Drugs in wrong combination (for example combination of chloramphenical with streptomycin)..... 14 products. Many preparations contained more than one of the undesirable features mentioned above. a) b)

Preparations containing Metronidazole, furazolidine were considered as useful even if these were accompanied by ingredients like pectin, kaolin which are of doubtful value. The preparations given in the November 1981 issue of Monthly Index of Medical Specialties [MIMS] under the heading "antidiarrhoeals" were taken for this study. MIMS has not included single-ingredient brands of Ampicillin, trimethoprim-sulfamethoxazole etc. in this list. The above analysis is based on the latest editions of Textbooks of Pharmacology by Goodman-Gillmon, Martindale etc.

FROM THE EDITOR'S DESK Given below is a statement regarding imports of bulk drugs into the country. This was published in “Drugs and Pharmaceuticals" I expect the information is genuine.

You will notice that money spent on some is almost unchanged, some increased and some decreased. The last category bothers me. I, however, do not know whether the reduction in import indicates an increase in indigenous manufacture (a heartening news) or whether it is a true reduction (highly disconcerting). There is no reason to suspect that the prices of these drugs in the international market have come down. On the other hand, in view of the world wide inflation, the apparent reduction in import should be less than the real reduction. The drugs whose import shows a fall include anthelantileprotics, antituberculars, antiepileptics, menthics, (excluding barbiturates), vitamins and vaccines. The ones whose import has substantially increased include anticancer, antihypertensive, cardiac glycosides. It is necessary that we k now the true meaning of it. Can anyone give us details of indigenously manufactured bulk drugs, in the same time periods as given here? That may set our hearts to rest or give us points to ponder and issues to be set right depending on the available facts. Can I expect some interested member of M FC or reader of the Bulletin, to help us?

Drug imports during 1980-81 According to the Directorate General of the Health Services, in 1980-81 the value of imports of bulk drug intermediates was Rs, 56.93 crores, a fall of 13.0% on the previous year's total of 65.4 crores. The table below gives the breakdown category wise of the bulk drug imports for 197980 and 1980-81.

1979-80.

1980-81

Rs. lakhs)

(Rs, lakhs)

Anaesthetics/Analgesics

324.5

385.9

Anthelmenthics Anti-amoebics Antibiotics Sulpha drugs Antidiabetics Antihistaminics Antileprotics Anticancers Antituberculars Antimalarials Antidepressants Anti-epileptics Antihypertensives Anticholinergics 19.6 Antitussives and Expectorants Blood preparations Diuretics (mercurial) Diuretics (non-mercurial) Endocrines Sympathomimetics Vitamins Hormones (& intermediates) Minerals Haematinics Coronary vasodilators Digitalis/cardiac glycosides Tranquillisers X-ray diagnostics Immunologicals Vaccines Barbiturates Miscellaneous New drugs Drug intermediates

81.3 49.7 2288.7 240.0 58.2 140.0 30.8 39.0 366.7 149.0 16.1 18.0 51.7 05.1 18.8 28.2 14.9 170.1 243.4 1080 664.3 176.9 79.3 01.6 35.1 05.9 102.3 48.7 00.7 57.0 17.3 243.4 661.0

30.7 51.2 2473.5 356.6 NA 53.9 13.3 51.2 171.2 190.6 14.0 06.0 103.3

Drug Imported

Total

6545.5

05.1 10.3 05.3 106.7 248.5 99.2 360.3 198.5 138.6 NA 47.4 10.6 72.3 37.7 NA 41.2 15.8 297.6 91.3 5692.8

Kamala Jayarao

Editorial Committee: Anant Phadke Christa Manjrekar Ravi Narayan Ulhas Jajoo Kamala Jayarao, EDITOR


