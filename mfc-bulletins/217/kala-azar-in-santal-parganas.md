---
title: "Kala Azar In Santal Parganas"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Kala Azar In Santal Parganas from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC217.pdf](http://www.mfcindia.org/mfcpdfs/MFC217.pdf)*

Title: Kala Azar In Santal Parganas

Authors: Chaterjee Prabir

medico friend circle bulletin

217

April, 1995

Kala Azar in Santa' Parganas '/

Prabir Chatterjee

Sahebganj and Pakur districts are the northeastern part of Santal Parganas Division in Bihar. The Ganges separates Sahebganj from Katihar in the right and (after making a right-angled turn), from Malda in the east. Pakur is south of Sahebganj. They both border Godda district in the west, and at some points Sahebganj borders Bhagalpur district as well. They have a total population of one million.

There were some other centres-s-Mariampahar in Sahebganj district, (620 Kala Azar cases in 1994) and Soharghati in Pakur district which specialize in Kala Azar treatment. The statistics of Kala Azar patients in Soharghati dispensary is given in Table-I.

Most of the diagnoses made in these dispensaries are by aldehyde testing and on clinical findings. Usually, patients who have fever and markedly enlarged spleen Since 1987, Kala Azar has been recognised as a (anything from 2 to 8 cms from the costal margin), and major public health problem in this district. In that year, who do not respond to a full course of chloroquine, are the old Sahebganj district (currently two districts-Pakur tested. Occasionally co-trimoxoazole is also tried before and Sahebganj) had 5887 cases of Kala Azar, the highest restoring to testing. Quite a few tuberculous patients with number in any district in Bihar. Borio Block alone had spleens enlarged after repeated attacks of malaria may 3100 cases and the village of Mungra (population 100) also present with similar symptoms and this is had 56 cases (Sehgal, 1988). considered a differential diagnosis specially in patients "In 1992-23, a review of the cases seen in St Luke's· who present with cough as a major symptom.

Hospital, Hiranpur, (the largest hospital in Pakur district) showed that 105 cases had been treated that year (Karin Philips, 1993). A large number of these cases came from Binjha village (approximately 3 kms from Hiranpur), a second cluster from around the Block and a third from the market town of Litipara, 10 Kms away. Over the years, the number of patients treated at St Luke's has decreased due to the irregular supply of sodium antimony gluconate, the drug used in the treatment of Kala Azar. However, even in 1994, as many as 134 persons were diagnosed to have Kala Azar by the aldehyde test at St Luke's Hospital. Another 13, mostly those with symptoms and clinical signs of Kala Azar but with negative aldehyde results, had their bone marrow tested. However the LD Body positive slides are not traceable (MD Anis;, 1995). The clinical loads (percentage of patients with Kala-Azar) were thus 1 % in 1992-93 and 1.3 % in 1994.

In 1994, the Bihar government made bone marrow testing compulsory for the diagnosis of Kala Azar, and the number of cases in Bihar had risen from 19,179 in 1987 to 54,274 in 1990 (Health and Family Welfare Dept., Government of Bihar). Though people were unhappy with the fact that they would be subjected to bone marrow testing, the local NGOs agreed to try holding mass detection camps in the field. The government doctors were invited to undertake bone marrow testing while the staff from the NGO examined the patients and supplied certain basic medicines, as well as doing all the publicity work (i.e., announcing the dates of camp in the villages). At the first camp, 56 bone marrow tests were done. At the second camp, 13 bone marrow tests were done.

After that the government doctors found it difficult to keep up with the frequent requests for help. It became necessary to do some other confirmatory test for Kala Azar. The School of Tropical Medicine in Calcutta agreed to conduct camps in Soharghati, Satia and a few other central places where they did the Direct Agglutination Test (DAT). This involves taking only 5 to 10 drops of blood by finger-prick on a labeled filter paper. The actual test was done in Soharghati and Satia once for two series of about 40 patients. However, the usual practice was to send the samples to Calcutta. The results of the camps held in 1994 are given in Table-II. The results were quite alarming. The more on looked, the more cases were found. The cases that-had been seen at Hiranpur were the mere tip of an ice berg. The actual numbers of cases were much close to the numbers being treated at Soharghati and even those were probably an under-estimation. The team from the School of Tropical Medicine now suggested an active case-finding search in two or three highly affected villages. The staff of Satia did a census survey of three villages close to the school which were known to have more than 5 cases each. Two of the villages were inhabited only by Malto speakers (they call themselves Maler while others call them Pharias). This tribe is a Dravidian group, linguistically closely related to the language of Orans. There are only about one lakh of them in all and they are the original inhabitants of this area. They are also the poorest and least educated group in the region. Makbita village is 6 km from the main road while, Sonadhuni (Paharia), which is on the kuchha road leading to Makbita, is 2 km from the main road. The third village was the Santalportion of Sonadhuni which is much larger and is reached by crossing a few fields from behind Paharia village. Among the Santals, there were about 50 Marayas (a scheduled caste community of iron smiths) who speak both Kaithi (the local 'Hindi' dialect) and Santali quite fluently. This latter group was quite resistant to testing. Between 29th Nov and 5th Dec, we tried to take blood samples of as many of the total population of these villages as possible. The results are given in Table-III. Of those who tested DAT positive, around 40 had already been treated during the last six months. This is because DAT levels take some time to return to normal. However, the other 70 were newly detected cases. This gives us a DAT positivity rate (on the assumption that those who were absent or refused to

TABLE-I patients in Soharghati Month

Kala Azar 1991 1990

1992

1993

1994

Jan Feb Mar Apr May

2 2 7 2 3 6

110 89 125 115 121 108 54 71 55 65 82 72

73 72 95 130 94

Jun

74 95 93 160 170 158 111 84 119 100 101 80 1345

1067

Jul Aug Sep Oct Nov Dec

Nil 4 3 6 5 3

4 4 11 5 22 14 16 12 14 21 56 51

Total

43

230

60

54 52

.38_ 57 43 40 808

(Source: Sr Therese & Sr Valsamma)

give blood were all healthy and so able to go out for work or go visiting relatives) of 10%. The malaria statistics were quite mind-boggling. The villages were not selected on the basis of any expectations that they had a larger amount of malaria than others in the Block. Hiranpur statistics show that in December, there was a cerebral malaria epidemic but the cases came from all over Pakur district. To come back to the Kala Azar figures, they may reflect the situation in Dhorompur Panchayat (population 7680) where both the Sonadhuni villages are situated and in Baromasia Panchayat (population 2800) where Makbita lies. In that case, there would be around 1000 cases of Kala-Azar in-a radius of 5 to 10 kms. Would it be possible to do one thousand bone marrow tests at a special sub-centre? This would mean doing 5 bone marrow tests every working day of the year. A full-time doctor and a full-time laboratory technician would be needed (of course, there will be need for other types of staff as well, as each course of treatment involves 20 injections; 20,000 injections a year or 60 injections a day) even if patients were ambulant.

It all seems very impractical, in particular when we do not know how bad the situation is in the other Panchayats or Blocks. Imagine if Latipura Block alone required 4 to 5 special sub-centres? The Bihar government-will have to rethink its policies. If a

TABLE-II Results from the camps held in 1994

Place

Date

Litipara 4 Apr MariamPahar 27 Apr Soharghati 18 May Loharbani 30 May Mukri 10 Jun Sohorghati 16 Aug Sohor-

ghati Satia Kodma Dodio AmbaGonda ChotaChapri Loharbani Satia

3 Sep 4 Sep 5 Sep 5 Nov

Total Aide DAT LD/ MP Pati- hyde (BM) ent + + +

+

65

-

18 300 200 281

18* 67 29 15

- NA(/13) 77 13/(21)

26 Nov 26 Nov 29 Nov

67 56 100 1912

39

3.

Karin Philips, Aberdeen, 'Kala Azar in Rural Bihar' (unpublished), 1993.

4.

Seghal, 'Kala Azar: Current status and evaluation of control activities in India", NICD, New Delhi, 1988.

5.

Sr Therese & Sr Valsamma, Soharghati.

19

-

6 6

10

194

9

Announcement

-

1

Health and Family Welfare Dept., Bihar, "Kala Azar: Janne Yogya Bathe", (Hindi).

16 43 1

3/(10) 1(1)

-

2.

-

7/(10)

28 33 1

-

-

2

-

200 200 10 106 41

-

-

53

35/(51)

203 59 +/106

Applicants should send a brief bio-data along with description of their current activity to

LD/BM - number of LD Body positive slide per number of Bone Marrow tested.

Dr C Sathyamala B-7, 88/1, Safdarjung Enclave New Delhi-1 10029

MP - malarial parasite * pre-selected

Last date for applications: 30 June.

TABLE-III

The course fee of Rs 500/- will be waived in case of those earning less than Rs 2000/- per month.

Results of Blood Samples taken from 3 villages Village

Population

DAT+

Makbita

Malarial parasite + P falci- P. Vivax Both parum

200

21

51

6

2

125

13

29

10

2

(Santali)

350

77

48

6

2

Total about

700

Sonadhuni Sonadhuni

111

=

MFC, in collaboration with the Trust for Reaching the Unreached, is holding a 7 day workshop (19 to 25 Aug 1995) in epidemiology and statistics at Baroda. Since there is no external funding for the course, the participants will be charged Rs. 1200/- which includes boarding, lodging and the cost of Xeroxing,

88

DA T - Direct agglutination test

(Paharia)

References: 1. MD Anis, Torai, (1995).

280

11 Nov

Total

MP

meaningful attempt to control Kala Azar is to be made, proper clinical examination and aldehyde testing will probably have to remain the basis of diagnosis. May be DAT could be of use if active case-finding is undertaken.

Total blood samples obtained 350 Of the DAT + cases, 70 were newly detected.

156

Touch me, Touch-Me-Not: Women, Healing and Herbs Shodhini Team Introduction Health seeking behaviour in most societies is often marked by a multiplicity of medical or healing systems. Every healing system is a product of specific world view of a specific group of people. In India, though the allopathic system of medicine dominates, a number of traditional systems with scholarly traditions like Ayurveda, Sidha, Unani and Folk systems of medicines are utilised by people. Healers of Ayurveda and Sidha are generally-of upper castes/classes and in the case of Unani, access is limited only to men. On the contrary, folk medicine which has evolved over ages through experience in various communities is accessible to all classes without distinction of caste, sex, age etc. Unlike modern allopathic medicine, in the traditional system of medicine, treatment is not simply through medicines, it is rather a spiritual experience and process. There exists a relationship of mutual faith and respect between the patient and the healer. It is an integrated relationship not only between two humans but also between human and nature.

To meet the health care needs of women, a combination of both systems may be necessary, i.e., the technical achievements of the allopathic system integrated with the humanist psycho-social specialisation of traditional systems of medicine. Moreover, be it an allopath or a local healer, central to the therapy is the relationship between the therapist and the woman seeking care. The healer is sought not only to cure the disease,-but aid the woman in health education and self care. The practitioner would be one whom she can trust, who listens to her needs and anxieties, acknowledges her integrity and offers a choice in her own process of treatment.

Today there are numerous indications to show that a women's health movement is gaining momentum. Women's groups are involved in education about the human body, its functioning, reproduction, contraception, etc. Information that is essential to be able to discern between the good and harmful herbal remedies, better nutrition and fertility awareness is also provided. Some groups are organising and training health workers in simple diagnosis and Allopathy, a product of the industrial age, has captured treatment. the globe with its quick action therapies. In this changed paradigm, traditional health practices are generally dismissed as irrational, unscientific, and superstitious. Shodhini's Experience Health care has become commodified into something one The effort of Shodhini is one such attempt to search has to buy. The governments, both colonial and post for alternatives. Dissatisfied with modern medicine and independence, have never considered integrating traditional inadequate health care services, a keen desire to understand medicine within the fold of health care system. their bodies, and to explore the utility and efficacy of However, both allopathic and traditional systems of medicine have an inherent sexist bias in them. Allopathic practitioners often consider women to be ignorant, emotional, dependent and unhygienic. In the traditional system of medicine too, sexist bias lies in the conceptualisation of women's health and disease. There is definitely a sex distinction in their therapeutics. Patriarchal attitudes become apparent when considering 'white discharge', menstrual disorders and food restrictions. The modern system of medicine looks at women's health only in the context of maternity and child welfare, while reproductive tract infection, one of the areas of concern receives little or no official attention.

traditional home remedies, a group of women activists came together to form the Shodhini network, A feminist health activist, Rina Nissim from Geneva Women's Health Care, took the initiative of starting this project with six nongovernmental organisations from six different states of India. The project had two clearly articulated objectives:

* To identify traditional healers, document their use of herbs, preparations of these herbs and healing practices including the rituals and practices that accompany the process of healing.

* To take control of one's body through a process of self help so that the body and its rhythm can be understood.

The Process In nine different field areas from six states, namely UP, MP, Gujarat, Andhra, Karnataka and Tamilnadu, Shodhini members contacted local healers of their respective areas. Through a process of establishing rapport with them, we began the task of collecting afresh, the ageold information on herbal remedies and natural local treatments used to cure women's health problems. In committing what has largely been an oral tradition into writing, we were probably guilty on several counts, the major danger being one of appropriation by commercial interests, national and multi-national. But then, when we thought of the parallel possibility of this knowledge dying out, we preferred to put them in writing and enable women to adopt these cures for their ailments. This process of gathering relevant information was long drawn and required nearly two years. During this time, we also had discussions and meetings with different groups of women from the nine regions to find out more about the common, yet neglected, health problems. Our investigations and field interactions with women revealed that they suffered from a range of ailments like vaginal infections, problems linked to the menstrual cycle, etc. Our findings also corresponded to the study carried out by the 'Search' team from Maharashtra, that many women suffer from gynaecological problems which they feel shy and afraid to talk about and hence, these remain hidden and neglected. It was precisely for these problems of Women that we wanted to identify and enable the women to deal with. Thus, through a series of meetings, discussions and dialogue with various regional women's groups we arrived at the following areas for the action research.

visits to the forests and fields along with the women who shared this knowledge with us. They showed us the plants and trees which were of medicinal value and educated us in. the process. Along with this, we also collected information from other sources, including literature on medicinal plants, data from health professionals who were working with herbs for women's health. At the end of this period of data collection we sent all these information sheets and herbariums to be scrutinised by Dr. Indira Balachandran, a botanist from Kottakkal Arya Vaidya Sala, Kerala: She verified their medicinal properties with regard to symptoms, and uses for which the women had cited them. In all, 411 sheets were prepared, and 70 plants were recommended more than once, for the same complaint from different regions, and some of them were cited in the same complaint from different regions, and some of them were cited in the same regions twice or thrice by different healers thus already adding to their validity. We found herbs like Neem, Tinospora cordifolia, Asparagus recemosus being used in almost all the six states for treating vaginal infections and menstrual problems. One problem was that the women healers prescribed traditional medicines for health problems identified according to their own concepts or knowledge system. An example, which came up repeatedly in Gujarat, was Ratwa. The symptoms of Ratwa which we discovered on closer questioning were: repeated miscarriages in a woman, birth of a baby covered with red rashes and the baby turns blue as a neonate. Ratwa is believed to be caused by garmi or heat in the body and also due to chronic vaginal infections. As a result of this kind of grouping, we realised that we had to learn to understand the local modes of thinking and beliefs of health and disease.

- problems of the menstrual cycle (too long, too short, painful and heavy periods); Another problem that came' up in many field areas was that the healers would not articulate the names of plants or - infections (vaginal and urinary tract); the remedies that they used. They believed that this would - tumours (uterine, cervical, benign and malignant) diminish the healing powers of the plant or the information - other neglected aspects of women's health such as prolapsed would be misused. There might have been some validity in such beliefs. Once the healers were convinced, they were uterus, backpain, depression, weakness and fatigue. willing to show us the plants and share their knowledge. To facilitate the action research, a comprehensive Some of us learnt to get around the problem of the healers information sheet was established covering all essential not uttering the names by asking the healers to show the information with regard to herbal remedies for women's plants. health. Supplementing each information sheet, her-bariums Along with collecting data on plants, remedies and were also made to ensure that the species' names could be traditional concepts of health and disease, a identified by a botanist and verified for common knowledge. The data collected was translated into English, cross checked and verified through field

parallel process of understanding 'bodies' was also initiated. The healers and health workers showed interest in new skills of diagnosis and knowledge of the body and readily participated in self-help workshops. Self-help workshops created space and a supportive atmosphere which allowed women to speak about their problems related to sexuality and gynaecological ailments.

and Garlic. Another common and very effective herb> for the same symptom was Neem. Another common herb found very useful for a variety of symptoms was Mimosa pudica that cured not only vaginal, but also menstrual problems and allergies. Thus Shodhini set itself the task of demystifying knowledge and practices related to gynaecological disorders through a judicious combination of traditional and modern systems of medicine making the health care process more users friendly, cost effective and accessible to ordinary women, especially the poor.

When we started our association with Shodhini, some of us in the core group had a theoretical, intellectual understanding of the self-help methodology. None of us, except Rina, had been part of any self help group or had done self examination. Thus, whenever anyone The few significant findings that emerged from five suggested doing a practical self-examination session, years of our action research were: there was resistance: In contrast, the health workers were very keen to participate in these work-shops. However, * most of the rural women's health problems are linked to poor nutrition. once we got initiated, gradually by practice we learnt to distinguish the characteristics of a normal vagina, cervix, * The non intended ill effects of using IUD's among uterus, breast and other organs, from those which are women whose body weight and anaemia inflamed, infected or marked by any other ailment. This conditions naturally disqualify them from being process was not uniform in all regions. However, as Rina able to use them. When such underweight women Nissim worked sensitively with us, at our own pace, use IUD's they suffer from pelvic inflammation, without compulsion, sooner or later the whole Shodhini extremely painful periods, heavy bleeding, cramps team got an understanding of self-help and its link with and migraine. herbal medicine. After nearly eight months of this kind of practice we (core group and health workers) could make * The reported symptom of chronic 'white discharge' and tiredness were often associated with tubectomy a reasonably accurate diagnosis. We learnt how to and were commonly reported both by rural and conduct interviews, take a complete health history, how basti women. Through our work in Shodhini we did to do a pelvic examination and use a speculum. attempt to enable women to select a suitable method of contraception. Many women in Andhra Meeting every month in a workshop was important Pradesh have been successful in motivating their for exchanging our experiences. Central to these husbands to use condoms. processes was a very important principle-careful, attentive observation of whatever women came with both Some have opted for the use of rhythm method. physical and social problem. We tried to locate women's Others, who willingly decided to be sterilised, were health problems within the context of family and society. provided with emotional and social support, taking into account their physical/and nutritional status. Once we got the skills of examination and diagnosis we have selected 35 plants which are already validated. The outcome of the action research by Shodhini is We started using them for common complaints like being consolidated in the form of a book in six Indian vaginal infections (different types), urinary infections, languages and in English. The Shodhini work has also and menstrual problems. To assist the healers to record gained momentum in bastis and rural areas. Sabla Sangh their diagnosis and herbal treatment easily pictorial case in Delhi have started their own health centres in bastis sheets were designed. After a period of nine months we where women come for treatment and counseling. In found that the herbs tested worked 'well', be it for Andhra and Karnataka women have become barefoot common vaginal infections or for difficult uterine gynaecologists. In Gujarat health workers have started problems. To quote a few examples we found that across nurseries of different medicinal plants and preparing oils different regions, Asparagus racemosus was effective in and tinctures along with organising regular self help dealing with non specific vaginal infections. In Andhra, workshops. two herbs effectively used for vaginal infections were In a unique integration of self-help methodology Abutilon indicum with herbal remedies, the Shodhini experience opens a new dimension in the perspective of women's health and healing.

Dear Friend,

In fact, because the epidemic was declared too late in the day people had already spent a lot of money trying In 1994, when I joined the URMUL team at to get rid of the disease; the only doctor at the PHC Pokharan for the malaria epidemic, the epidemic had charged Rs. 30F for each prescription, even if the already been there for more than two months. The patient 'was represented by someone else. In fact, I doctor who was with the team before I joined, was suspect that PHC doctors deliberately fudged data and seeing about a hundred and twenty patients a day, diagnosis to avoid the epidemic from coming to notice. occasionally about three hundred. In Rajasthan, government' doctors are allowed to practice privately in non-duty hours. An epidemic By the third day, even I'd become as efficient entails more doctors being sent to the peripheral PHCs despite my earlier belief that anything less than five and hence the share per doctor could decrease. Worse minutes with each patient is poor practice. Where still, the government usually bans' private practice eighty percent of patients have palpable spleens and during epidemics, and even if it is not obeyed, it is an ninety per cent patients give a history of fever in the irritation one could do away with, if the figures are past forty-eight hours, presuming malaria is not prevented from being alarming—e.g., by examining the difficult. Spleens taking two hands to measure point backlog of slides first and releasing the results in their fingers at only one or two diseases which can batches so that the pre-epidemic 'results dilute the SPR. cause such an epidemic. But a 60 % slide positivity rate given by Anwar who's into his second major epidemic Kadar, a 'quack' who was nevertheless available of malaria in Rajasthan, thanks to his ability to catch to the patients of Kelwa during the entire epidemic, the earliest sniffle of Plasmodium falciparum, speaks has made a bounty by giving injections of in more convincing tones about the diagnosis. chloroquine, anti-pyretics, and anti-emetics. He is With drug regimens being violated and modified seen as a saviour. Ironically, he comes to us for by every doctor who has a license to kill, seeing and correct treatment of his own children and we tackle examining patients and prescribing at great speed, I their malaria by our implicit faith in "governmentlearn, is the least difficult job at hand. What's the more supply-chloroquine". difficult is curing malaria. Most doctors know that patients and diseases do not read textbooks before they come to you. This gets confirmed when P falciparum is seen in all the alternate day fevers, and even if the RBCs were teeming with malarial parasites, the patient often would have had no fever.

"The government has done a lot more this time than the previous epidemic hereof malaria about twenty years ago", says an old man who is impressed that teams-some NGOs too, but taken to be of governmenthave reached villages and dhanis all around. There lies the rub.... this contentment of the people also helps the government in continuing its slumber.

While discussions on televisions scream for Sunil Kaul, URMUL, Rajasthan mefloquine, malaria in the villages of Gandhiji's India displayed a true nationalist spirit by responding to * * * chloroquine, when administered under supervision, at cent per cent rates. We did a small study (under the We are a group of six doctors who plan to circumstances, even 100 patients given 4-4-2 regimen develop a health care programme in the tribal dry land each, under supervision could mean sacrificing seeing area of Bagli tehsil of Dewas district, Madhya more patients in more villages) to dispel people's Pradesh. Samaj Pragati Sahayog, a voluntary organization working in the field of Total Watershed Management is notions of chloroquine resistance. That malaria could have very high social costs is seen by the tall stalks of Bajra and Jowar still standing in their fields. Had it not been for the disabling fevers and large scale deaths, which require at least a ten-day mourning period each, these fields would have been harvested long ago. Our weavers fail to meet their delivery dead lines and face economic hardship for the months to come.

helping us in planning and implementation.

Broadly, we aim to provide low cost accessible health care in the 90 villages in Bagli tehsil and training of health workers to diagnose, treat and prevent common and important health problems. We also hope to design and carry out appropriate field and clinic based research on health problems specific to rural areas. Another area of study would be the indigenous system of medicine especially the local herbal

and folk medicine. This would involve the understanding of the concepts of disease, health and healing; evaluation of the efficacy of such practices and therapies; and documentation and protection of medically significant plants. We plan to test the hypothesis that Modern medicine, even if adapted adequately to make it relevant and low-cost, cannot solve the health care needs of a poor area. We therefore wish to explore the herbal remedies to arrive at a judicious mix of the two systems. We also feel that health care research should be undertaken where people live and on questions relevant to their needs. The technical support for research will be available from our colleagues at the All India Institute of Medical Sciences. Since many friends from the mfc have been and/or involved in (or have strong views on) primary health care' we would like to take the benefit of such experiences and ideas. We want your advice/comments on:

- funding; - relative importance of hospital and out-reach services; - cost-effective health care strategies; - hospital building .design; - and research issues of relevance. We have skills in some specialties-internal medicine, paediatrics, surgery and microbiology. We need to acquire some skills in obstetrics and gynaecology and anesthesia. Would you know of any place that can help us with a short term training in these specialties? We have prepared a small project proposal which gives a more detailed outline of our work plan. We would appreciate and value your comments, suggestions, advice and help. Yogesh, Raman, Anju, Biswaroop, Madhuri and Anurag, BB/49C, Janakpuri New Delhi-110 058.

Edited by Sathyamala, B-7 (Extn.), 12~A, Safdarjung Enclave, New Delhi-l10029; published by Sathyamala for Medico Friend Circle, 50 LIC Quarters, University Road, Pune-411 016; Printed 'by Sathyamala at Kalpana Printing House, L-4, Green Park Extension. New Delhi-110 016. Views and opinions expressed in the bulletin are those of the authors and not necessarily of the organisation.


