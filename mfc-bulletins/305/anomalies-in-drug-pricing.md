---
title: "Anomalies in Drug Pricing"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Anomalies in Drug Pricing from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC305.pdf](http://www.mfcindia.org/mfcpdfs/MFC305.pdf)*

Title: Anomalies in Drug Pricing

Authors: Anurag Bhargava

2 India it is nearly 50%; so the relative impact is higher in India. The mere fact there is no DPCO in US or UK or elsewhere does not mean that there is no price regulation in these countries. Some facts: United States: Nearly every one is insured against illness. Insurance companies who keep an eagle’s eye on both, prescriptions and prices, reimburse cost of drugs. If a doctor is found to be unnecessarily prescribing high cost drugs when cheaper alternatives are available, he can loose insurance backup. This in effect means he will have to leave the medical profession. Similarly, manufacturers cannot charge more than what they are charging in other countries. United Kingdom: The government through National Health Service meets the entire medical costs. Manufacturers have to negotiate prices with the government. In fact price control is more rigorous in England than India because there is only one buyer. The NHS pays at substantially discounted prices on all medicines. For example Buscopan is sold to NHS at a discount of 42% over the MRP. In any case, individual patients are not concerned. Belgium (as an illustration of European Community countries): Every resident, including foreigners, get total reimbursement of all medical costs from three government-controlled “mutuelle” who compete with each other but their annual subscriptions are decided by the state and are most reasonable.

mfc bulletin/June-July 2004 investors buy these shares at such high prices? z

A look at the balance sheets of pharma companies will show their profitability is one of the highest and beats nearly all other sectors except perhaps software. In fact most companies boast of 20 to 50% higher profits than previous years, year-afteryear!

z

India has the highest number of pharmaceutical manufacturers in the world: over 17,000. Surely no business minded person would enter a field where profits are not adequate.

z

Non-pharma large business houses are eager to enter drug business: some examples: JK Tyres, Camlin ink, Mesco, Raunaq group.

z

Pharma manufacturers pay one of the highest salary packets to their employees. The marketing heads of many pharma companies get more than Rs. 25 lacs+ annual pay packet with other perquisites. Such salaries are unheard of in other manufacturing sectors.

z

There must be substantial profits to allow the manufacturers to offer bonus schemes to chemists including some DPCO-controlled formulations. Examples: in February 2004, Nicholas Piramal was giving 15% discount on Paraxin (chloramphenicol) to chemists; Bombay Tablet 1 strip of Ofloxacin free with every 4 strips, Ranbaxy was giving 1 strip of Calmpose (diazepam) tablets free with every 11 strips and still better deal for Caverta (sildenafil): 1 strip free with every 2 strips. Alkem was giving 2 strips of Broadiclox free with every 10 strips. The manufacturers explained that they had to liquidate unsold stocks and hence resorted to bonus schemes. This is blatant bribe to chemists to substitute brands and that is illegal under Drugs & Cosmetic Rules!

z

India is THE ONLY country where people with very modest means have produced huge amount of wealth from pharmaceutical manufacturing business in the past two decades.

The effectiveness of the above measures can be gauged from the fact that US Government is currently prosecuting Glaxo USA for billion of dollars for overpaying Glaxo UK for import of ranitidine and thus “cheating” USA. On the other hand in India, the entire system is based on MRP (Maximum Retail Price). What about the transfer prices from a related manufacturer or on loan license? There are cases where there is huge difference between the transfer price and the MRP. Complaint: Indian Manufacturers do not make Adequate Profits Nothing could be farther from truth. Some facts: z

z

On the nation’s Stock Exchanges, pharma shares have always remained on a very high side compared to other sectors including manufacturing companies be it Siemens (Rs. 310) or Reliance (Rs. 270). Ranbaxy’s Rs. 10 share is being quoted at over Rs. 1050 (April 2004); Dr. Reddy’s at Rs. 1000. If the profits were not handsome, why would

Surely, the pharma business is highly profitable. Claim: Drug Prices in India are Among the Lowest in the World z

Nominally yes but then how does one compare the purchasing parity of various currencies. Is US$ 1 in USA worth Rs. 45 in India in its purchasing power? Certainly not. Exchange rates are

mfc bulletin/June-July 2004 determined based on demand and supply of foreign exchange, not their actual worth. The correct method of comparing two currencies is by determining the purchasing power parity. As per IMF/WB study, Indian currency is 5.3 times higher in its purchasing parity compared to its exchange rate of Rs. 45 with US dollar. In other words a dollar’s real worth is only about 9 rupees. It is because of this reason that UN employees when transferred from, say, USA to India, do not get their emoluments based on bank’s exchange rate. z

In the United States, the minimum wage is US $ 6.50 per hour; in UK about GBP 4. An unskilled person needs to work for less than 10 minutes to buy 10 tablets of paracetamol (even if his insurance company or government was not to reimburse). In India a person will need to work for at least one hour to buy equivalent amount of the same drug, which incidentally is one of the cheapest. So the cost of the cheapest drug is 600% more in India compared to the United States.

6.

3 Chemist’s margins are different in different countries and have an impact on retail prices.

7.

Local taxes are different that have an impact on prices.

8.

Local rules governing pharmacies are different. For example in some countries, fully qualified B.Pharm graduates have to staff drug stores; therefore the cost goes up. In India more than 60% shops do not have even a person with D.Pharm. Many states allow 5-year experienced shopkeepers to be classified as “pharmacist.” Hence the labour costs are very low.

9.

Even if the above reasons were not operative, how does it matter if the prices are higher, or lower, in other developing countries? After all prices in India have to be dependent on local manufacturing costs, local margins, local distribution costs and local purchasing power. The fact that some other government of some other country is less than vigilant or less than willing or powerless with respect to drug prices does not mean that India should do the same. In most cases, these figures are given merely to complete documentation in files to justify higher prices. India with its huge population and market has advantage of “economy of scale” and even if all other factors were not applicable, the prices have to be lower compared to other developing countries.

z

While seeking higher prices, manufacturers often resort to quoting figures from developing countries to compare prices. These arguments are selfserving and misleading. Reasons?

1.

Unlike India, most countries accept the Paris Convention on patents and hence pay royalties to original manufacturers if the drugs are produced locally. Naturally the prices are higher. Why should this be taken as reason to justify higher prices in India, which had not accepted the patent regime and manufacturers did not have to pay even one paisa as royalty?

10.

2.

As explained above currency parity is difficult to establish and prices based on exchange rates are deceptive.

Let us first look at some of the prices of the same molecules sold under different brand names. The variations are miles apart. Some examples:

3.

Standards and cost of living are different. How does one compare drug prices in Malaysia – with far higher income levels and standard of living with India?

1.

Fexofenadine 120mg: one tablet of Alernex (Dabur) costs Rs. 5 while Allegra (Hoechst) is priced at Rs. 8 - the difference being 60%.

2.

Two brands of cetirizine are priced at Rs. 1.60 and Rs. 2.60 per tablet: a difference of 62%.

3.

Gliclazide: Glidiet (Modi-Mundipharma) is priced at Rs. 31 (for 10 tablets) compared to Rs. 59 for Diamicron (Serdia) a difference of over 90%.

4.

Two brands of ofloxacin cost Rs. 100 for 10 tablets (Oflin by Cadila Healthcare) and Rs. 530 for Tarivid (Aventis). The difference: 530%.

4.

5.

Except for Nigeria, Egypt, Jordan and Brazil, there is hardly any pharma manufacturing industry in other developing countries of Asia and Africa, which are dependent on import of drugs from other countries. Naturally the prices are higher. Custom duties, where applicable, increase the prices. This is not applicable to India because the drugs are locally manufactured.

Industry’s Suggestion: Competition Should Decide the Drug Prices

4 5.

6.

The difference in the cost of Risperidone is beyond imagination: Less than Rs. 18 for all manufacturers except Johnson & Johnson that costs Rs. 135! A difference of 750%. Amlodipine prices are widely different: Amlodac 10mg is priced at Rs. 14 for 10 tablets while Amlovas of equal strength costs Rs. 35 for 10 tablets – gap of 250%.

There are scores of other examples. Since no manufacturer would be selling at loss, it is obvious that huge profits are being made. Normally the sale of cheaper brands should not only be substantially more than costly brands but expensive brands should die a natural death in due course. Let us look at the facts and figures: 1.

As per ORG figures, Cyclovir (Zydus) brand of acyclovir with the therapy cost of 812 rupees had a total sale of Rs. 57 lacs annually period compared to Rs. 3.17 crores for more expensive Herpex (Torrent) brand (cost of therapy: Rs. 922).

2.

Diamicron (Serdia) brand of gliclazide at Rs. 59 for 10 tablets was worth Rs. 7 crores against the cheaper brand that had a measly sale of Rs. 66 lacs (Glidiet of Modi-Mundipharma). The medicine is to be taken for life.

3.

z

z

Ranbaxy sponsored the visit of about 400 prescribers to Bangkok.

z

Glaxo has given thousands of refrigerators to chemists.

Under such circumstances, who except the state can protect the interests of patients? Selection of Medicines and Brands Unlike the developed countries, there are no standard guidelines on the selection of medicines. For example in England, the National Institute of Clinical Excellence (NICE) issues periodic consensus guidelines on the exact option, in correct sequence, for treatments of various diseases like Asthma, Hypertension, Diabetes etc. Such system has several advantages: firstly, it is scientifically valid; secondly, it is cost effective and thirdly it protects the doctors from legal litigations. In India every doctor decides on his own which medicine to give. Not infrequently the choice is scientifically inappropriate and financially costly. Let us look at some examples: 1.

The correct treatment of chlamydial genital infection is either tetracycline (cost Rs. 14). Yet most, if not all doctors are somehow “persuaded” to prescribe ofloxacin (minimum cost Rs. 70 to a high of Rs. 380 depending on the brand used) by manufacturers. Why does it happen? Because high profits lead to higher promotional efforts and larger prescriptions.

2.

Many clinical trials have established that there is insignificant difference in the efficacy of omeprazole (cost Rs. 40 for 10 tablets) and pantoprazole (cost Rs. 65 for 10 tablets). Yet large number of doctors prescribes pantoprazole apparently influenced by manufacturers.

3.

It is the same story of enalapril (Rs. 10.50 for 10 tablets of 10mg) and perindopril (Rs. 100 for 10 tablets). Incidentally against a price difference of 1:10 in India, the difference in UK is only 1:2 between the two molecules.

The most expensive brand of enalapril (Envas) sells hundreds of times more than cheaper but equally reputed brands including Cadila’s BQL! The cost difference is over 33%. It is a life long medicine

Obviously, doctors are oblivious of cost to patients. A more logical explanation is that doctors get easily influenced by manufacturers who have the capacity to spend large sums of money on aggressive promotion and offer huge incentives to “right” prescribers. Some examples: z

mfc bulletin/June-July 2004 Singapore with stay extended to 3 days! Needless to say spouses were also included. Result: its brand has the highest sale.

In the past eight years, a south Delhi based surgeon has been sent on vacation to Switzerland by a south Delhi based Pharma Company every year. Quid pro quo: he prescribed only the obliging Company’s products. In the case of antibiotics he went one step further. Instead of 5-7 days, the patients were made to swallow the bitter pill for 10 days. Johnson & Johnson that produces epoetin alfa (life saving for kidney transplant patients) was gracious enough to sponsor 300 kidney specialists to attend a 3-hour “scientific conference” in

Why does this happen? Because pharmaceutical manufacturers in India have tremendous influence over prescribers. The policy framework of DPCO and its implementation is not helpful at all; in fact it is making

mfc bulletin/June-July 2004 the matter worse. Due to various rules and regulations, mostly the axe of DPCO falls on what we call as the reference (initial) molecule that gets price controlled, leaving the related molecules to mint money for producers.

5 (Not to mention the fact that DCGI failed to ban PPA that stands outlawed in the west.) 5.

Cough syrups and tonics have a total sale of over 800 crores including the so-called Branded generics - a typically Indian nomenclature that does not appear on ORG retail audit. These are supplied to chemists to sell without prescriptions, particularly in semi-urban and rural areas. A bottle of cough syrup with a printed price of Rs. 24 is given to chemists for Rs. 10. He can make a profit of over 150%. Producer’s cost is below Rs. 7. Similarly a bottle of tonic with MRP of Rs. 50 is sold to chemists for Rs. 22. Its actual cost is less than Rs. 10.

6.

There are many diseases that require drugs belonging to several therapeutic areas. For instance H. pylori eradication needs a proton pump inhibitor (omeprazole or lansoprazole or pentaprazole), an antibiotic (amoxycillin or clarithromycin or oxytetracycline) and an imidazole (metronidazole or tinidazole or secnidazole). It would serve no purpose to control prices of one PPI, one imidazole and one antibiotic because prescriptions will invariably shift to uncontrolled more profitable drugs.

The industry is quick to provide “scientific evidence” based on isolated trials, often defective and deceptive, to prove that their molecule is “superior” to reference molecule. Often the alleged superiority, even when present, is 1-2% but costs 200-300% more. The solution lies in controlling the profits on all molecules belonging to the same class as well as other drugs used in the same therapeutic area. For instance: 1.

If the price of diazepam is fixed leaving other benzodiazepines uncontrolled, then prescriptions will shift to them.

2.

If the price of chloroquine (costs less than 60 paise per tablet) alone is fixed leaving other anti-malarials untouched for one or the other administrative or policy reason, then prescriptions will shift to uncontrolled agents such as mefloquin or artemether. The danger is not only that poor patients will pay more (artemether costs Rs. 14 per tablet) but also there will be long-term scientific repercussions. For example, mefloquin or artemether are scientifically reserve drugs and should only be used for chloroquin resistant strains of malaria or in cerebral malaria. If they are indiscriminately consumed, the day is not far off when all the three malarial parasites in India will become resistant to all agents.

3.

4.

Similarly if ranitidine price is controlled, prescriptions will shift to other H-2 receptor antagonists like famotidine or to sucralfate or omeprazole and its related molecules. A noteworthy example is that of phenylpropanolamine (PPA) v/s psuedoephedrine. Actifed, a cough and cold remedy of Glaxo is an international brand. All over the world it contains pseudoephedrine while in India it contains phenylpropanolamine. Two years ago PPA was found to be causing strokes. Coldarin issued frontpage ads to inform readers that its product was free from PPA. Yet recently they themselves have changed their own formula by using the discredited, dangerous PPA. Why? Because PPA is cheaper while psuedoephedrine is not only expensive but under price control. Since both are decongestants, they should be subjected to similar price controls or remain out of price control.

Misconception on “Essential Drugs” Unfortunately, the well-intentioned nomenclature of “Essential Drugs” coined by WHO several years ago in another context is sought to be used by the drug manufacturers to claim that medicines that are not included in the Essential Drugs list should be outside the price control mechanism. Nothing could be more unfair. All drugs are essential for specific patients. Is it fair to control the price of chloroquine that costs hardly 60 paise and is used for 3 days since it falls in the Essential Drug List and leave out leflunomide that costs Rs. 45 per tablet and is to be taken daily by patients unfortunate enough to suffer from crippling rheumatoid arthritis? If any thing, leflunomide needs price control even more than chloroquin. Is streptokinase (Price Rs. 2,500) required to treat heart attack less essential than drugs that fall in the “Essential Drugs List”? Are we suggesting that heart attack occurs only in rich patients? The cost of amifostine (used in ovarian cancer) is Rs. 4,400 per vial and the dose is 2 vials per day! The cost of triptorelin (for prostate cancer) is Rs. 6,500

6 per vial. Dose: one vial per 28 days till the unfortunate sufferer lives. The cost of paclitaxel (for breast cancer) therapy is Rs. 11,000 every week. Selective profit control based on total volume, market share etc is not the answer to patients’ problems. It may have an economic base but lacks scientific reasoning. A more humane and more scientific approach is required. An overall profitability ceiling is required in a country like India because manufacturers migrate from DPCOcontrolled to non-controlled drugs. For example Lyka Lab, known for antibiotics, went into cough mixtures. Due to lack of enforceable legislation on the lines of US Anti-Trust law, there is large-scale price fixation. Example: the first batch of one brand of sildenafil 50mg was priced at Rs. 12 per tablet. Within a week another brand was launched with a price tag of Rs. 18 per tablet; the first brand quickly hiked the price in the very second batch! All the five subsequent brands are today equally priced at Rs. 18. If the companies were in the United States, there is every probability that all concerned CEOs will be behind bars! Email inbox Immunisation Paradox ? Friends, I share my field experience from Jharkhand, where I took up an assignment of WHO-NPSP, monitoring the January 2004 round of pulse polio immunisation, also I tried to investigate informally, a long nagging question: Why routine immunisation is failing in India, whereas pulse polio immunisation is performing exceedingly well ? Just look at the paradox: The Public health system, for both pulse polio immunisation and routine immunisation are same. Health cadre that delivers immunisation to the community are same: ANMs, MPW-m, PHC MOs, BMOs or CDMOs etc., are same for pulse polio and routine immunisation. But ironically, the same polio drops when given through routine immunisation has dismal coverage where as it when given through the National Pulse Polio programme (NPSP), the coverage is exceedingly impressive. Very Very interesting!!

mfc bulletin/June-July 2004 I talked to few of my academic pediatrician friends from SJMC, Bangalore, and according to them it is because the chain of command in WHO-NPSP and their ‘army’. But NPSP-Surveillance MOs disagree. According to them, WHO-NPSP is providing only technical support to the Government. The regular Government health machinery does the actual work of carrying out the massive pulse polio. In fact it is pointed out that WHONPSP came into existence in 1997-98, whereas India started its pulse polio programme in 1995. To get to the root of the problem, I interacted with the PHC MOs in Meharama Block of Godda district, Jharkhand. They attributed two crucial logistical reasons for success of pulse polio programme: 1. Relative ease of administering polio drops by anybody (e.g. teachers) makes it easier 2. During pulse polio rounds booths are established in every village @ one booth per 250 under five children. On the first day, children are given drops at the booth, and on second and third days vaccinators go house to house to immunize missing children. Reasons for Failure of Routine Immunisation are given as: Other vaccines in routine immunisation are injectible (e.g. BCG, DPT, Measles) hence require skilled people i.e., ANMs, MPW-male, Health supervisors. Let us take the example of the Block (Meharma). It has a 75,000 population. In total 154 booths were established in this block for polio immunisation. Where as for routine immunisation, no booths are established and community is expected to come to the subcenters, which obviously cannot exceed more than 25 in this block. Hence the differenceof 25 and 154 that makes all the difference in failure of routine immunisation. Hence the coverage in routine immunisation is very poor, with measles vaccination coverage being less than 45% coverage, abysmally low. Even if similar booths are opened for routine immunisations. It is logistically impossible to train large army of volunteers in injections. Inspite of being a very strong pro-primary health care ideologist, I tend to wonder, as to whether vertical health programmes are the only way to control Vaccine Preventable Diseases in India? With concern, Rajan Patil E-mail: rajanpatil@yahoo.com

mfc bulletin/June-July 2004

7

Anomalies in Drug Pricing Drugs (pharmaceuticals) in India are overpriced. The profit margins extend to over 1000 percent in most cases. Tender prices in government are a shocking 2 (two) to 3 (three) percent of the retail market prices. Table 1 (please see Col 8) brings this out clearly. Table 1 compares the retail market prices of six common medicines with those paid by the Tamil Nadu Government through its open, transparent tendering process. The Tamil Nadu Medical Services Corporation (TNMSC) ensures that drugs brought through the tenders are of good quality. This percentage differential in pricing for the public sector and private retail sector is probably true of no other industry in India – or in the world. Would the booming computer industry sell in the market a laptop at Rs 100,000 and to the government tender for Rs 2000 to Rs 3000/-? Would a truck manufacturer sell The author is with Jan Swasthya Sahayog, Bilaspur, and is active in the current pricing case in the Supreme Court, of AIDAN, MFC, JSS and LOCOST vs. the Government of India.

- Anurag Bhargava trucks for Rs 5 lakhs in the market and to the government tender for Rs 10,000/- to Rs 20,000/- even if he had an order of 10,000 trucks at a time? This however is the situation of the drug industry in India. The prices of finalized tender awards of TNMSC, that is the “Approved L1 Rates for the Supply of Drugs & Medicines for the period from 01.11.2003 to 31.03.2005” for are available on the web at http// www.tnmsc.com/ 11rate0304doc. The Tamil Nadu Medical Services Corporation (TNMSC) ensures that drugs brought through the tenders are of good quality. The results are likewise for the Delhi State Government’s pooled procurement system for medicines. Price control itself is ineffective. There is blanket violation of the letter and spirit of government fixed ceiling prices. Prices of market leaders of captopril (Aceten), ranitidine tabs of Ranbaxy and cefotaxim injection are way above the government fixed ceiling prices.

Table 1: Tender Rates at a Fraction of Retail Market Rates Drug Name

Name of Firm

Tender Unit Rate (Rs)

Mfr.

Retail Market Price (Rs)

Overprice Index Col (6)/(3)

(1)

(2)

(3)

(4)

(5)

(6)

(7)

Tender Rate as percent of Retail Mkt. Price (8)

Albendazole Tab IP 400 mg Bisacodyl Tab IP 5 mg Alprazolam Tab IP 0.5 mg Diazepam Tab IP 5 mg Folic acid andFerrous Tab NFI Amlodipine Tab 2.5 mg

Cadila Pharmaceuticals P Ltd Lark Laboratories (I) Ltd

22.60

Torrent

1190

52.65

1.89 2.30

3.50

40.43

2.47

Pharmafabricon/LOCOST

3.05

German 717 Remedies Sun 141.50 Pharma Ranbaxy 92.50

43.45

Bal Pharma Ltd

30.33

3.29

Aurochem India P Ltd

5.89

10×10 tablets 10×10 tablets 10×10 tablets 10×10 tablets 10×10 tablets

Smith Kline

148.50

25.21

3.97

Lark Laboratories (I) Ltd

9.10

10×10 tablets

Lyka

148.50

16.32

6.13

16.50

Adapted from: Srinivasan, S. “How Many Aspirins to the Rupee? Runaway Drug Prices”, Economic and Political Weekly, February 27-March 5, 1999.

8

mfc bulletin/June-July 2004

Top 300 Drugs in India: A Brief Analysis -Chandra Gulhati1 We now present an analysis of the top-selling 300 drugs of India accounting for Rs 19,000 crores sales in India. If we examine the list of top 300 brands (as per ORG-Nielsen Oct 2003, see Table 2 for a partial list), we find that only 115 brands are of drugs that are mentioned in the National List of Essential Medicines (NLEM) 2003 i.e. only 38% of brands of the top selling ones are of drugs mentioned in the NLEM, the other 62% are of drugs which do not find mention in the NLEM. Of these 62% brands comprise drugs that are higher priced alternatives without a clear therapeutic advantage, and many drugs that are unnecessary, irrational and even hazardous. The number of drugs represented by these 115 brands is only 68. The National List of Essential Medicines includes 354 drugs. The majority of the top selling brands are of drugs which are outside the National List of Essential Medicines, which means that the majority of the drugs which are the most cost-effective for the treatment of priority health needs of the people are not the ones which are selling the most.

estimates for the total turnover of the pharmaceutical sector are Rs 40,000 crores (source: pharmabiz.com). The government quotes lower figures. The total moving annual turnover of from the retail sales of 300 brands alone (there are at least 20,000 formulations in the market) is a whopping Rs 18,000 crores. This figure of Rs.18, 000 crores would only be a part of the total retail sales. The final figure of total sales does not take into account institutional and governmental purchases, which would also be of very considerable magnitude. Some industry estimates put the figure to Rs 40,000 crores. The sales figure reflect the fact that in India, drugs which are not considered essential sell more than rational and essential drugs, that costlier drugs most often sell more than cheaper alternatives (even those made by well known manufacturers), downright irrational and hazardous drugs are among the top sellers. The pattern of production and the pattern of sales do not adequately reflect the real health needs of the people. (See Table 3)

A cough syrup, which has potential for addiction (Corex) and has been banned from distribution in many states of the northeast, is the top selling medicine brand in the country with an annual turnover of nearly Rs 80 crores. Elsewhere, a combination of multivitamin, minerals, and ginseng unknown to the pharmaceutical world outside India is ranked 27th in this list and has sales of Rs 47 crores. Nise (generic: Nimesulide), the 14th top-selling brand, is a pain and fever relieving drug withdrawn from distribution in its country of origin and not available because of its toxicity in UK, USA, and other developed countries, nor even in neighbouring Sri Lanka and Bangladesh. It is freely available in India as a single drug and with a bewildering number of combinations.

A significant number of the top selling formulations are combinations of drugs, rather than single ingredients. In fact there are 118 combinations in the list of 300. The majority of the combinations are irrational. Only around 20 of these combinations are rational, the rest are combinations, which lack any therapeutic rationale.

In this list of the top 300 brands, only 36 brands (a little over 10%) are of drugs covered by the Drug Price Control Order. The sales from 300 brands alone are huge and put the government estimates of the sales of the pharmaceutical sector into question. The

Therefore the majority of sales are coming from the sales of drugs not considered relevant by experts for inclusion into an essential medicines list, and not considered important by the government for regulation of their price.

The majority of formulations in the top 300 are of drugs, which are at the moment out of patent. The factor of patent protection is not a factor behind the high prices of drugs. The high prices is the result of the profiteering of the pharma companies, the pharmaceutical trade, with the added cost of aggressive drug promotion which is passed on to the hapless consumer.

mfc bulletin/June-July 2004 Table 2: Top-Selling 25 Brands in India as per ORG-Nielsen Retail Audit, Oct 2003 Brand Name

Uses and Remarks

Moving annual turnvover in rupees crores (retail sales)

1

Corex

Cough suppressant. Abused as drug of addiction because of presence of codeine.

88.18

2

Becosules

Multivitamin, unnecessary preparation.

79.74

3

Taxim

Bacterial infections

77.05

4

Voveran

Pain relief

76.14

5

Althrocin

Bacterial infections

68.46

6

Human Mixtard

Diabetes mellitus

63.39

7

Cifran

Bacterial infections including typhoid

62.70

8

Liv-52

Ayurvedic liver preparation

62.67

9

Asthalin

Asthma.

61.76

10

Sporidex

Bacterial infections

61.71

11

Betnesol

Allergy

61.11

12

Zinetac

Dyspepsia, ulcer disease

60.70

13

Neurobion

Irrational Multivitamin preparation

60.27

14

Nise

Hazardous drug for pain relief

58.31

15

Digene

Antacid

57.86

16

Dexorange

Irrational preparation for anemia.

57.65

17

Phexin

Antibiotic for bacterial infection.

57.03

18

Mox

Bacterial infections

56.36

19

Cardace

Hypertension, heart failure, 55.31 much cheaper alternatives exist

20

Rabipur

Vaccines against rabies

54.40

21

Omez

Peptic ulcer

53.52

22

Ciplox

Bacterial infections

51.69

23

Combiflam

Irrational analgesic combination.

49.02

24

Aten

Hypertension

48.87

25

Augmentin

Expensive antibiotic, not mentioned in the national list of essential medicines

48.63

9

10

mfc bulletin/June-July 2004 Brand Name

Uses and Remarks

Moving annual turnvover in rupees crores (retail sales)

26

Roxid

Antibiotic, bacterial infections

48.19

27

Revital

Completely irrational preparation with ginseng and multivitamins

47.64

28

Monocef

Bacterial infections

47.45

29

Phensedyl

Cough suppressant

47.30

30

Shelcal

Calcium and Vit D deficiency

47.29

31

Betadine

Antiseptic

46.13

32

Gelusil-Mps

Antacid

45.66

33

Vicks Vaporub

OTC product for colds

45.34

34

Deriphyllin

Asthma

44.96

35

Ceftum

Expensive antibiotic, cheaper alternatives exist

44.93

Irrational antibiotic combination

43.48

36

Ampoxin

37

Novamox

Bacterial infections

42.81

38

Glucon-D

Glucose preparation with no therapeutic value

42.79

Vitamin preparation with no clear value

42.29

39

Evion

40

Daonil

Diabetes mellitus

41.45

41

Envas

Hypertension, heart failure

41.07

42

Polybion

Irrational multivitamin

40.85

43

Huminsulin

Diabetes mellitus

39.96

44

Calpol

Drug for pain, fever

39.75

45

Crocin

Relief of pain, fever

38.97

46

Wysolone

Steroid preparation for allergies, inflammation.

37.44

47

Moov

Pain balm

37.31

48

Tegrital

Epilepsy

37.12

49

Mikacin

Serious bacterial infections

36.94

50

Aciloc

Dyspepsia, peptic ulcer

36.85

mfc bulletin/June-July 2004 11 Table 3: Top Selling 10 Categories of Drugs in the Top 300 Brands: Where is the People’s Money Going? Type of drug category

No. of Brands

Moving annual total (in crores of rupees)

Remarks:

1.Anti-infectives

65

1650.02

Most frequently used and abused drugs when antibiotics are given for fever due to viral infections

2.Analgesics

26

705.06

Hazardous analgesics like Nimesulide are one of the top sellers.

3.Endocrine disorders like diabetes mellitus, hormones

25

694.10

4. Multivitamins and minerals

27

651.29

Contains predominantly non-essential drugs in all kinds of irrational combinations.

5.Drugs for cardiovascular disease

26

601.64

The top selling cardiovascular drug is one that has little therapeutic advantage over less costly alternatives.

6.Drugs for respiratory system, including cough preparations

21

512.59

Cough syrups sell more than drugs for asthma.

7.Drugs for gastrointestinal system

20

427.21

Their large scale is also the result of over prescription.

8.Drugs for allergy

10

326.51

9. Anticonvulsants.

9

221.35

10.Hematinics

6

128.13

Contains such irrational wonders of the pharmaceutical world as Dexorange (57 crores) which till recently contained animal blood from slaughter houses, hepatoglobin, etc.

In no other situation in life does a consumer buy goods of which he/she has no knowledge, buys on the written recommendation of a second party from a third party; and the second party may charge heavily for doing so; and the second party may also get paid by third party and other parties manufacturing those goods; and bought usually at a time of severe distress with death as a possible threat of non-purchase. Is this not, combined with the above irrationalities, sufficient cause for thorough overhaul of the drug control and pricing system of India?

12

mfc bulletin/June-July 2004

Exclusive Marketing Rights in India: A Brief Note -Amit Sen Gupta1 Background

If the invention has been made in India, then:

After the signing of the WTO agreement India was to provide for a transitional arrangement till 1st January 2005 or provide for Product Patents in the area of pharmaceuticals. With the coming into force of the agreement in January 1995, the Government provided for Exclusive Marketing Rights (EMR) in January 1995 through an ordinance. The Ordinance, however, lapsed after six months, as the Government could not get it ratified in the Parliament. This led to the US filing a complaint against India in the WTO Dispute Settlement Body, arguing that the Indian Patents Act did not comply with the obligations set forth in the TRIPS Agreement.

z

In 1999 the Patents First Amendment Bill was passed by the Indian Parliament and provided for EMRs (text of the Act relevant to the provision of EMR in Annexure 12 ). Briefly, the Amendment providing for EMR required the following: The product must be patentable, that is, it must be an invention as per Section 2 of the Patents Act 1970, and must not fall under the non-patentable products listed in Sections 3 and 4, such as products derived from traditional knowledge or atomic energy-related products. The invention must have been made in India or in a WTO Convention country. If made in a convention country, the invention must have been: z Filed on or after January 1, 1995; and z The applicant must have obtained a patent and marketing approval in the Convention country. z Marketing approval of the article or substance should have been obtained from the appropriate authority in India, provided that the application has not been rejected by the controller on the basis of the report of the examiner that the invention is not an invention (Section 2) or the invention is an invention on which no patent can be granted (Sections 3 and 4). The author is a well-known drug activist and is associated with Delhi Science Forum and Jan Swasthya Abhiyan. 2 Annexures 1 and 2 are not reproduced here for lack of space but are available at the mfc briefcase on the web. To access, go to yahoo.com, sign in as user: mfcindia2004 and password: Bhopal. Click on “briefcase” afterwards. 1

z

The applicant has obtained Patent for method or process of manufacture for the same invention in India and filed a claim for Patent for product on or after 1.1.95 under section 5(2) of the Act. Marketing approval of the article or substance should have been obtained from the appropriate authority in India, provided that the application has not been rejected by the controller on the basis of the report of the examiner that the invention is not an invention (Section 2) or the invention is an invention on which no patent can be granted (Sections 3 and 4).

The EMRs, once granted, may last for five years or till the date of grant or rejection of patent for that substance, whichever is earlier. The EMRs are not sacrosanct in all circumstances. For instance, the government or a designated authority may, at its discretion, sell or distribute the protected good and fix the price of the EMR-protected product. Thus, in the case of a national emergency, where certain products are required in vast quantities or at low prices and such products are protected by the EMRs, the Government can take the aforementioned measures to meet public requirements. Further, Section 24C states that the government may grant compulsory licenses in the case that the owner of the EMR is unwilling or unable to meet the public’s demand for an ENM-protected product or is charging unreasonable prices for the product. The latter option, thus, also allows the Government to prevent the owner of an EMR from abusing his rights and accruing monopolistic profits on his product to the detriment of public welfare. Apart from these safeguards the Government also possesses the right to pursue any action, including the revocation of a patent or the EMR, in India’s security interests. Few EMR Applications — Possible Reasons It was expected, that after the enactment of the First Amendment Bill in 1999 providing for EMRs, one would see a large number of applications. It was also expected by many that, as the grant of an EMR would be virtually automatic, if the applicant had received a Patent approval in another “convention country”, a

mfc bulletin/June-July 2004 large number of EMR grants would ensue. What has puzzled many commentators is that neither of the above has happened. In 1995, India had also provided for a mailbox facility where Patent applications could be filed — which would be examined after the expiration of the transition period allowed to India till 31st December 2004. Till date over 5,000 applications have been filed through the mailbox. But, till date, only 15 applications for EMR have been filed and only 3 have been granted. All the three applications that have been granted EMRs, received the grant only in late 2003, i.e., 4 years after the enactment providing for EMR in the Indian Patent Act. Before looking at the specific cases of grant of EMR or their rejection, some discussion regarding the possible reasons for the very few applications of EMR and the even fewer grants of EMR, is necessary. One possible reason for the relatively few applications for EMR is the uncertainty among pharmaceutical companies regarding how the Indian market will adjust to a Product Patent regime. The EMR provision, while not the same as a product patent, places similar demand on the market in terms of conferring a monopoly right to the right holder. It should be remembered that for over 30 years the Indian Pharmaceutical market has functioned in a virtually “no Patent” environment. As a result of this, price elasticity in the market is very low, and this is compounded by the low purchasing power of the average Indian consumer. Thus the expectation that such a market would be able to absorb high prices driven by monopoly rights is unlikely to be fulfilled — at least in the immediate sense. The market might absorb high prices in the case of really “revolutionary” products with high therapeutic advantages. The very small number of EMR applications possibly also points to the fact that very few such products have been introduced into the global market since 1995. It can, of course, be argued that companies could apply and receive a grant of EMR and then choose to sell at comparatively low prices in the Indian market. This is, however, a strategy that pharmaceutical companies, especially “Big Pharma” is loath to follow the world over because such a strategy delegitimises their pricing mechanism for Patented products in the global market. Another possible reason for the dearth of EMR applications is to be found in the uncertainty regarding the procedure for examining claims for EMR applications. The TRIPS agreement does not specify the scope and effects of EMRs and there is very little

13 precedence in dealing with cases of EMR.. The TRIPS concept of EMRs, it has been argued, is modeled on US law — specifically the Hatch-Waxman Act of 1984, which requires inter alia that an innovator drug be granted at least five years of market exclusivity after it is approved by the drug administration before equivalent competing products are approved. This provision was meant to benefit drugs that have either no patent protection or had less than five years patent protection left at the time of approval. The Indian Act regarding EMRs was not as straightforward as many had presumed. It, for example, has a provision for examination of the Patent application (a Patent application being a pre-condition for an EMR application) : “This request shall be examined as to whether the invention is not an invention within the meaning of the Act in terms of Section 3 or the invention is an invention for which no patent can be granted in terms of section 4 of the Act...” (Section 24 A, (1) of Indian Patents Act). Section (3) of the Indian Patents Act says: “3. The following are not inventions within the meaning of this Act: (d) the mere discovery of any new property or new use for a known substance or of the mere use of a known process, machine or apparatus unless such known process results in a new product or employs at least one new reactant...” In other words, according to the Indian Patents Act, grant of an EMR is not “automatic” even if the conditions that a Patent and marketing right have been granted in another Convention country are fulfilled. Moreover, applications based on “new use” or “new dosage/usage” forms would logically be rejected, even if a Patent for such use had been granted in another country. Thus, companies were possibly reluctant to submit their Patent application to an examination, apprehending that this would prejudice the examination of their Patent application once the mailbox was opened in 2005. This would be a worry especially in the context that the Section on EMRs in the Indian Patent Act does not spell out a redressal mechanism in the case of the rejection of an application. In other words an EMR application could be rejected without the applicant being provided any reasons for the rejection and without the possibility for recourse to any redressal mechanism except through civil courts. Moreover, the Indian Patents Act also includes provisions for granting Compulsory licences on the

14 EMR granted (4A.3.0 Section 24C of the Indian Patents Act). Also, for the grant of an EMR the product has to also receive a marketing approval from the relevant authorities in India — i.e., the office of the Drug Controller General of India (DGCI). Technically the DGCI is empowered to refuse marketing approval even if all other conditions relevant to the Patents Act are fulfilled. The absence of co-ordination between the Patent office and the office of the DGCI has also been cited as a problem by the industry. Even if an EMR is granted by the Patent office, its implementation requires the DGCI to withdraw approval for marketing rights that it may have accorded to other companies producing the same drug before the EMR for that drug was granted. To recapitulate, some of the “inadequacies” in the EMR provisions that the pharmaceutical industry has pointed out (in other words reasons why they would be hesitant to file EMR applications), include: The EMR regulation does not elaborate a redressal mechanism for the EMR-holder if generic versions continue to sell in the market. Or, for that matter, for the generic manufacturer whose own brand of the drug had entered the market in the interim. Theoretically, the law does not prevent the Indian

mfc bulletin/June-July 2004 drugs regulator, the Drugs Controller General of India (DGCI), from approving copies of a drug whose EMR pleas are pending with the Patent Office. Also, the DCI can refuse marketing approval even if conditions for the grant of EMR are fulfilled as per the Indian Patents Act. In the absence of a nodal agency, the two offices do not communicate with each other — the DCI falls under the Ministry of Health and Family Welfare, the Patent Office works under the Ministry of Commerce. The industry claims that it is completely in the dark about the process of granting EMRs (this is perhaps true given the great difficulty we had in getting any information regarding this from the Patents office). EMR applications are referred to in terms of the application number assigned by the Patent Office, which is published in the Gazette on a ‘for information only’ basis. No further information is available even to a generic company that might be marketing the same drug. EMR Applications Filed Let us now turn to the specific cases of EMR applications and approvals till date. The accompanying table gives the status of EMR applications granted and rejected (the list is not complete for rejected applications because of inadequate response from the Patents Office — till date there have been 15 applications for EMR).

EMRs Rejected and Granted: List of Drugs Drug for which EMR Granted Carbendazim + Mancozeb

Company United Phosphorus

Description Fungicide (agricultural chemical)

Imantinib mesylate

Novartis

Anti Cancer (to treat chronic myeloid leukemia)

Nadifloxacin

Wockhardt

Topical antibiotic

Tadalafil

Eli Lilly

Drug for erectile dysfunction

Anti Malarial Combination

Nicholas Piramal and CSIR

Anti malarial

Gatefloxacin

Bayer

Antibiotic

Drug for which EMR Granted

Company

Description

Rosiglitazone

GlaxoSmithKline

Anti-diabetic

Saquinavir

Hoffmann-La Roche

HIV protease inhibitor

Moxifloxacin

Bayer

Antibiotic

2

Olenzapole

Elli Lilly

Anti-ulcer

Drug delivery system for Ciprofloxacin

Ranbaxy

Drug delivery system for Ciprofloxacin

EMR under consideration

1

mfc bulletin/June-July 2004 Glivec and Other Cases Novartis became the first pharmaceutical company to be granted an EMR on November 11, 2003 for Glivec — an anti-cancer drug to treat certain types of chronic myeloid leukaemia (CML) and stomach cancers. However, only days after the EMR application was accepted, the Indian Drug Manufacturers’ Association (IDMA) expressed its “shock” over the decision, saying that, “Greatest care is required to be taken for any such grant. Some Indian manufacturers have already developed their own processes for this drug and have been marketing it at about one-tenth the international price. The grant of EMR to Novartis can provide a leverage to the EMR holders to interfere with the generic production and distribution of this drug, and worse still, with the treatment of patients.” The IDMA’s concern (though not wholly altruistic!) has definite merit given that the international price of Glivec is about $27,000 for one-year course requirement of a patient, while the generic price is about $2,700. There are an estimated 15,000 patients in India who would require the drug, though even at $2,700 per year, most would not be able to afford it. IDMA also pointed out that Novartis had filed its application for Glivec’s Patent in convention countries in May 1993, and hence should not have been granted the EMR in the first place. Subsequently an Indian generic company Natco Pharma - which sells a generic version of the drug - has challenged the EMR in the Delhi High Court on the above grounds. The petition has also challenged the grant on another ground — the fact that Novartis is believed to have got its EMR on the basis of a patent application that was filed in 1998 for a new form of the drug, called the ‘betacrystalline form’, and not for an entirely new drug. Curiously, the second EMR granted to Wockhardt (an Indian company) in December 2003 for its topical antibiotic Nadifloxacin, has similar problems — Nadifloxacin is not an entirely new drug either. The controversy generated over the grant of these two EMRs has raised questions about the transparency of the process. The Indian Patent Controllers Office has not been forthcoming with information — either for the grounds for approving an EMR application or for rejecting applications. Rejected applicants have been informed that their applications “ do not fulfill the required criteria”. It is also curious that after not granting any EMR for four years since the EMR provision was introduced in the Patents Act in 1999, three applications have been accepted within a period of three months in late 2003.

15 The conduct of the Patent Office has raised eyebrows especially in the context of there being proma facie evidence that the grant of both the EMRs for pharmaceutical products, seem to be flawed. Some have interpreted this to be an attempt by the Patents Office and the Indian Ministry of Commerce to send the “right” signals regarding Patent Protection as India moves towards a product patent regime in the area of pharmaceuticals. Patents Amendment Bill (2003) The provisions for grant of EMRs are now poised to come to an end with the introduction of the Patents (Amemdment) Bill, 2003 in the Indian Parliament on 22nd December. The Bill has been referred to the Department related Parliamentary Standing Committee on Commerce. The Bill inter alia seeks: (a) To introduce product patent protection in all fields of technology as per Article 27 of the TRIPS Agreement (b) To delete the provisions relating to exclusive marketing rights and to introduce a transitional provision for safeguarding exclusive marketing rights already granted The Standing Committee has decided to invite views/ suggestions on the provisions of the Bill till the 15th February 2004. The portion of the Bill pertaining to EMRs is provided in Annexure 2.3 3

See Footnote 2. Sighted/Overheard 1) Readers may be interested in these comments of Munnabhai, MBBS in BMJ. At http://bmj.bmjjournals.com/cgi/content/full/ 328/7443/841 Also in BMJ: ‘India versus Pakistan and the power of a six: an analysis of cricket results’ Kamran Abbasi, deputy editor, Khalid S Khan, consultant in obstetrics and gynaecology At http://bmj.bmjjournals.com/cgi/content/full/ 328/7443/800 And see the Rapid Responses afterwards. Also ‘Is cricket the magic glue that unites South Asia?’ At http://bmj.bmjjournals.com/cgi/content/full/328/ 7443/843 Contd.

16

mfc bulletin/June-July 2004

Contd.

Registration Number : R. N. 27565/76

2) Taking Charge of Our Bodies, A health handbook for Women by Veena Shatrugna, Gita Ramaswamy and Srividya Natarajan. Penguin India. 3) Polio and immune deficiency disorders; Vaccinederived polio: lessons learned; Which post-polio immunization policy; Challenges to polio-free certification, etc. in Vol 82, No 1, Jan 2004, Bulletin of the WHO. Also at http://www.who.int/bulletin/ volumes/en/ Forthcoming z

Western Regional Public Hearing on “Denial of Right to Health Care” organised by NHRC and JSA at Regional Science Centre, Shyamala Hills, Bhopal, July 29, 2004. For details contact: Abhay Shukla at CEHAT, Pune. Email: abhayseema@vsnl.com or cehatpun@vsnl.com

Subscription Rates

Annual Life

Rs. Indv.

Inst.

U.S$ Asia

Rest of world

100 1000

200 2000

10 100

15 200

The Medico Friend Circle bulletin is the official publication of the MFC. Both the organisation and the Bulletin are funded solely through membership/ subscription fees and individual donations. Cheques/ money orders/DDs payable at Pune, to be sent in favour of Medico Friend Circle, addressed to Manisha Gupte, 11 Archana Apartments, 163 Solapur Road, Hadapsar, Pune - 411028. (Please add Rs. 15/ - for outstation cheques). email: masum@vsnl.com MFC Convenor N.B.Sarojini, I Floor, J-59, Saket, NewDehi 110 0017 Email: saromfc@vsnl.net; Ph: 011 26968972, 26562401

Papers Invited for Next Mid-Annual Meet of MFC At Sewagram, Wardha Dates: July 17-18, 2004 Agenda: Annual Meet on Right to Health Care Contents Drug Price Control: Principles, Problems and Prospects

Chandra Gulhati

1

Immunisation Paradox ?

Rajan Patil

6

Anomalies in Drug Pricing

Anurag Bhargava

7

Exclusive Marketing Rights in India: A Brief Note

Amit Sen Gupta

12

Editorial Committee: Anant Bhan, Neha Madhiwalla, Dhruv Mankad, Amita Pitre, C. Sathyamala, Veena Shatrugna, Chinu Srinivasan. Editorial Office: c/o, LOCOST, 1st Floor, Premananda Sahitya Bhavan, Dandia Bazar, Vadodara 390 001 email: sahajbrc@icenet.co.in. Ph: 0265 234 0223/233 3438. Edited & Published by: S.Srinivasan for Medico Friend Circle, 11 Archana Apartments, 163 Solapur Road, Hadapsar, Pune 411 028. Views and opinions expressed in the bulletin are those of the authors and not necessarily of the MFC. Manuscripts may be sent by email or by post to the Editor at the Editorial Office address. MEDICO FRIEND CIRCLE BULLETIN PRINTED MATTER - PERIODICAL Registration Number: R.N. 27565/76

If Undelivered, return to Editor, c/o, LOCOST, 1st Floor,Premananda Sahitya Bhavan, Dandia Bazar, Vadodara 390 001.


