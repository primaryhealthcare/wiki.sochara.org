---
title: "Medical Ehtics,Torture & Rehabilitation"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Medical Ehtics,Torture & Rehabilitation from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC238-239.pdf](http://www.mfcindia.org/mfcpdfs/MFC238-239.pdf)*

Title: Medical Ehtics,Torture & Rehabilitation

Authors: Sinha Roopashree

1.

medico friend circle bulletin 238 239

January-February, 1997

Health Effects of the Bhopal Gas Leak Disaster Part-II Mental Health and Neurological Status Research by ICMR in Bhopal

The following studies have been conducted by or in collaboration with ICMR on the mental health and neurological status and needs of people exposed to the Union Carbide gas disaster: Name of the Study

Date Begun

Date Ended

February 1985

Sept. 1992

March 1985

March'ti986

April 1985

May 1985

April 1986

March 1987

December 1989

Sept. 1990

1. Mental Health Studies in MIC exposed population at Bhopal 2.

Neurological manifestations of MIC poisoning

3.

Training in mental health care for medical officers.

4.

A pilot psychiatric study of children affected by MIC ...

5. Organic Brain Damage in toxic gas exposed population

All of these studies have been terminated.

I. Mental Health in Adults The investigation of mental health in adults following the gas leak (Study 1) began in February 1985 and concluded in September 1992. There was a significant change in the methodology in the fifth follow-up year (1990-91). There were basically three research aims: 2.To assess the prevalence of mental health morbidity through annual surveys in independent randomly selected samples of gas-affected families. 2. To investigate the factors associated wit? morbidity prevalence, including establishing the

fact and the extent of the gas disaster's specific effect, and 3. To follow the course and outcome in a cohort of gas victims with symptoms of mental health disorder. The studies were initiated under Dr. B.B. Sethi as principal investigator, who was replaced on retirement by Dr. Ashok Bhiman. A research article relating to the first year of work was published in the 1986 Supplement

This document was prepared for the International Medical Commission by C Sathyamala, Anant Phadke, Mira Sadgopal, and Satya Jaya Chander. Mira Shiva, Manisha Gupte, and Prabir Chatterjee of the Medico Friend Circle.

issue of the Indian Journal of Medical Research (Psychiatric 'morbidity in patients attending clinics in gas affected areas in Bhopal, pp 45-50). Methodology For the initial prevalence survey, at least seven hundred families (nearly 6000 population) in each of three localities of severe/moderate exposure, mild exposure and non-exposed control were selected by random sampling from the families in the long-term epidemiological study. The investigating team consisted of psychiatrists, psychologists and psychiatric social workers. The 'head of family' was administered a mental health 'item sheet (Verghese and Beig. 1973) including inquiry regarding the mental health of other family members. For probable psychiatric 'cases', psychiatric history was recorded using a semi-structured proforma. PSE (Present Status Examination) was also administered to these persons, who were diagnosed according to ICD-IX. Follow-up or 'rotational' prevalence surveys were carried out annually on independently drawn samples in the same manner as the initial prevalence survey. Follow-up of the cohort of 'positive cases' from the initial prevalence survey was done at one year intervals. Apparently, follow-up of the positive cases detected in the succeeding rotational surveys was also done (see 1987 report, page 38); but such additional cohort studies are not mentioned in the final report. The follow-up studies of individual persons with initial psychiatric symptomatology were done explicitly to study the extent of recovery, relapse, remission, response to treatment and pattern of course and outcome of illness.

Reported Findings The year wise summary table of prevalence of mental health disorders in the exposed and non-exposed areas, with the exception of partial 1991-92 data, is reproduced as Table 1. Prevalence of mental health disorder has consistently been twice to thrice as high in the gas-exposed population when compared with the control population. Some of the earlier annual reports show the breakdown of sex-wise and age-wise data. In the second rotational survey, for example, the incidence of mental disorder was three to four' times higher for women than for men in the exposed families while the same was only twice higher for women in the control families. In both exposed and control populations, the majority of persons with mental disorder was contained within the middle age range of26 to 45 years. A preliminary (not final) draft of the 1992 annual report states that when morbidity was studied with reference to the family's religion, the prevalence was "higher among the Muslim community in comparison (to the) same in Hindus" in the initial and "almost every rotational survey". In the exposed population, there was a fall in overall prevalence between the first and second year, from the high initial rate of 93.7 per thousand to 50.4. In the subsequent years the rate has hovered between 40 to 50 per thousand, until there was a drastic change in the final prevalence figures due to a change in the methodology, as footnoted in the table below. Administering the Self Questionnaire (SRQ) to individuals, instead of the previous practice of interviewing only the heads of families with the Verghese and Beig item sheet, resulted in a drastic jump in the prevalence figures, yielding much

TABLE 1: PREVALENCE OF PSYCHIATRIC MORBIDITY IN GAS EXPOSED AND NON·EXPOSED AREAS (From final summary report of 1992).

Year

Total pop'n

EXP6sED Cases detected

Preval. Per 1000

Total Pop'n

NON-EXPOSED Cases detected

Preval. Per 1000

1985-86

4559

427

93.7

1891

47

24.8

1986-87

4619

233

50.4

1845

30

16.3

1987-88

4541

185

40.7

1729

30

17.4

1988-89

4380

210

47.9

1752

26

14.8

1989-90

4451

214

48.1

1715

27

15.7

1990-91

1151*

308

267.6

480*

56

116.7

* The vast difference in the last row of figures compared to previous year is due to the change in instrument from the mental health item sheet of Verghese and Beig (administered to head of family) to the SRQ of Harding (administered to individuals).

TABLE 2: PREVALENCE OF DIAGNOSTIC CATEGORIES OF MENTAL DISORDER IN EXPOSED AND UNEXPOSED POPULATION

(Per thousand, from table in final report, 1992) Year

Anxiety State

1985-86

31.4

EXPOSED Neurotic Depression

Others

Anxiety State

NON-EXPOSED Neurotic Others Depression 11.6

5.8

54.8

7.5

7.4

1986-87

15.6

27.1

7.8

3.3

9.i

3.8

1987-88

13.7

25.5

1.5

5.2

8.7

3:5

1988-89

29.2

16.2

2.5

5.7

7.9

1.1

1989-90

35.9

9.4

2.7

11.7

1.7

2.3

(five and ten times) higher rates in both exposed and unexposed population. Even so, the rate in the exposed population for 1990-91 was more than twice as high as the rate in the unexposed population (267.6 compared with 116.7 per 1000). The implications of these vastly increased prevalence figures are not further discussed in the final annual report. One implication is that the initial (1985) prevalence rate for mental disorder might have been proportionately much higher than what has been recorded in the above table. With reference to clinical diagnostic groupings, the prevalence figures are available for the first five years only. The rates taken from the final report of 1992 are reproduced in Table 2. As the investigators themselves have pointed out, a striking apparent finding in the exposed population is the comparatively greater prevalence of 'neurotic depression’ in the first three years, contrasting with greater prevalence of 'anxiety state' for the following two years. The data for the control population show a less striking but similar trend. ~ In the fifth rotational survey (1990-91), with the change in methodology, the approach shifted "from the family to the individual". The SRQ (Self Rating Questionnaire, Harding, 1983) was administered to adult individuals selected by computerised 'pseudo randomization’. A total population sample about one quarter the size of the sample of the previous years was screened. Individual respondents rating positive from the SRQ were followed up for in-depth study and PSE. Regarding the follow-up of the cohort of 474 persons with ~ psychiatric disorders from the initial survey (279 severely/moderately exposed, 148 mildly exposed and 47 from the control area), attention seems to have been

focussed only on the outcome in the respondents in the severely/moderately exposed area. Table 3 is a summary table reproduced from the final 1992 report. TABLE 3: MENTAL HEALTH OUTCOME SUMMARY With diagnostic breakup, from the fifth follow-up survey

(Condition)

EXPOSURE Severe/Mod Mild N=164 n=97

Control n=39

Anxiety State

40

29

06

Neurotic Depression

50

42

10

Hysteria

05

02

—

Manic-Depressive Psychosis

01

03

—

Schizophrenia

02

03

02

Mental Retardation

—

—

02

Dementia

—

01

—

Other Neurosis

02

—

01

Recovered

64

17

18

No comment is made by the investigators on this data. However, it confirms that anxiety and neurotic depression continue to be important psychiatric problems among the gas victims. In contrast to the findings of the independent annual prevalence studies for 1988-89 and 1989-90, this cohort follow-up study of all three areas indicates that neurotic depression continues to be somewhat more prevalent than anxiety state, possibly in the original patients of depression. II. MENTAL HEALTH IN CHILDREN Regarding psychiatric problems in children (0-15 years), a study to .assess 'psychiatric and cognitive sequelae' (Study 4) was begun in April 1986. It was terminated after only one year, without continuing beyond the pilot phase. For the pilot study, 100 families having at least one 'child each were randomly selected from one severely

The Findings can be tabulated as follows:

affected area and from a control area.

Initial Number

Children

of Children

Followed Up

BHOPAL

317

204*

SEHORE

114

82

The pilot study of252 children from exposed and 241 from control area revealed the following: The frequency of psychiatric disorders in the children from the gas exposed area was 35 (13.9%) by PSSS, and 32 (12.7%) by Multiaxial classification, While in the control area it was 6 (2.4%) by both methods. In other words, during 1986-87, the rate of psychiatric disorder among gas-affected children was over five times that found in the control area. Nothing has been reported about the assessment of the children with psychiatric symptoms. There is no report available yet on qualitative findings.

III. NEUROLOGICAL CHILDREN

MANIFESTATIONS

IN

A study in school children (Study 2) was launched from March 1, 1985, three months after the gas leaked, under Drs. E.P. and N.E. Bharucha as principal investigators and Dr. N.P. Mishra and others as co-investigators. The aims were • To identify and assess neurological impairment associated with the gas exposure, and • To study the "long term deleterious effects on

intellectual development". 317 school children in heavily gas exposed localities next to the Union Carbide plant were compared with 114 school children from Sehore, 50 kms from Bhopal. Both groups were subjected to a WHO neurological questionnaire, and a detailed neurological examination. Secondly, performance of the children in the annual school examinations of March 1984 and March 1986 were compared. (School examinations in Bhopal were cancelled in 1985 after the gas leak.) Thirdly, a psychological assessment (by Raven's coloured progressive matrices test) was done initially and after one year, to monitor ongoing brain development during the year after the gas disaster. In addition, initially a Differential Aptitude Abstract Reasoning test (DAT-test) for intellectual performance was administered to 205 students at Bhopal and 230 at Sehore. This was abandoned, however, as 60% of the children could not complete it.

*Out of the 204 school children who could be followed up, 63 had experienced loss of consciousness at some time after gas exposure, and some children reported "faints" in classroom during the year.

With regard to the findings of "comparative intellectual performance" in the exams, 43.6% and 47.8% of Bhopal and Sehore students performed the same in 1986 as they had in 1984. A similar comparison with the 75 children who had lost consciousness revealed no significant difference in exam performance when compared without loss of consciousness. The Raven matrices test scores, measuring Intelligence Quotient, did not yield any significant differences between the initial and follow-up scores, nor between the Bhopal and Sehore groups. The investigators conclude that "exposure to toxic gas in children between the ages of 9 and 12 years produce no long term interference with scholastic performance or intellectual deficit on psychological testing."

IV. ORGANIC BRAIN DAMAGE A pilot study (Study 5) was initiated under Dr. B.B. Sethi in December 1989 and completed in September 1990. The aims were • to detect organic brain damage in the gas exposed population, and " •

To localise such damage, if possible, to specific areas in the brain.

The study sample of 100 persons was drawn from the severely exposed localities, adhering to the following criteria: 1.

Age 16 years or above

2.

Education 8th standard or above

3.

At least 1 death in family due to gas in the first year

4.

Severe physical pathology, gas related

5.

Persistent pathology, gas related

6.Excluding persons with pre-existing dementia The 100 subjects from the control area fulfilled only the first two criteria.

The four research instruments used were 1) Screening proforma (pertaining to socio-demographic variables and screening criteria), 2) Dementia questionnaire (for exclusion),

Health Care for Medical' Officers to cope' with post disaster situations was released in 1987. It is a good and nearly unique example of "service and research going hand-in-hand". The features of this composite piece of work are:

3)PSE (Present Status Examination to assess mental 'status), arid 4) Luria Nebraska Neuropsychological Battery (to assess and localise damage in the higher cortical areas- of the brain). The work was first carried out in rented field clinics in exposed and non-exposed areas of Bhopal. The last part (Luria Nebraska Test) was perhaps administered on the subjects at the Post-Graduate Institute, Chandigarh, also: The findings are reported in the Annual Report of 1992. Subjects with physical symptoms, without physical symptoms and control subjects were compared, as in this tabulated summary: Number of

Number with

Percentage

Physical Symptoms

75

30

42%

No Physical Symptoms

25

9

36%

Control Area

100

27

27%

No correlation, was found between organic brain damage and age or sex or psychiatric morbidity in either exposed or control subjects. Nor was any correlation found with severity of physical symptoms. With regard' to localisation, no pattern of left or right lateralisation could be discerned. Damage was often "equivocal". While in numerous cases damage could be localised to left or right parieto-occipital area, amongst three severely exposed subjects with physical symptoms, diffuse brain damage was "quite high"

v. MENTAL HEALTH CARE

Only one study (Study 3) has sought to address the problem of mental health care of the gas victims, and thus went beyond servicing academic curiosity and careers. The chief investigators were R. Srinivas. Murthy and Mohan K. Issac of NIMHANS at Bangalore. Their project was for one year only (1985-86). In addition' to their published paper in the 1986 Supplement of the Indian Journal of Medical Research (Metal health needs of Bhopal disaster victims and training of Medical officers in 'mental health aspects', PP 51-58); a Manual of Mental

1.

A frank, informative discussion of disaster as a phenomenon affecting mental health,

2.

A qualitative, human and humane assessment of the mental states of many individual gas victims, including elucidation of the types of emotional reactions people had, with special attention to children,

3.

An attempt at quantitative assessment of the mental health needs of the gas-affected people,

4.

Rational guidelines for psychotherapy and psychiatric medication for rendering relief, and 5.

Recommendations for rehabilitation at community level.

The investigators' pre-project assessment made in early February 1985 indicated that "approximately 50 percent of those in the community and about 20 per cent of those« attending medical facilities were essentially suffering from psychiatric problems". For further, systematic community psychiatric data, they relied on the early work of Dr. B. B. Sethi and his team from Lucknow (Study 1) which has been reviewed above on the basis of ICMR's annual reports. Quoting from the 'Manual', one presumably notes portions of the same data from quite a different slant: "Of 855 patients screened in 10 of the peripheral clinics... and further interviewed in detail (PSE), 193 were found to be psychiatric patients (22.6% of clinic patients). Most... were females (81.1 %) and... under 45 years of age (74%). The main diagnostic categories were: Anxiety neurosis (25%) Depressive neurosis (37%) Adjustment reaction with prolonged depression (20%) And adjustment reaction with predominant disturbance of emotions (16%) Psychosis was rare and was not related to the disaster. (Contd. on page 10)

Medical Ethics, Torture & Rehabilitation An Education Program for Health Professionals in Asian Region Roopashri Sinha, CEHAT

In many countries, the consciousness of health professionals with regard to human rights is based on every day reality. In other countries, the consciousness about their role has been stimulated by the increasing flow of refugees, the growing involvement of doctors in UN operations of humanitarian aid projects in situations such as genocide and political murder and the spurt in intense ethnic wars and communal clashes, low intensity but prolonged armed conflicts, and state sponsored violence. All these invariably involve health professionals. Medical experts and psychologists have also become involved both as technical experts who serve the torturer and as torturer themselves. There is a growing need for involving the health professionals in a more positive role of protecting human rights. Keeping this in mind the Psychological Trauma program (PST) of the Center for Integrative and Development Studies of the University Of The Philippines held a comprehensive training program in October 1996, in Thailand. This is a report of the Seminar. The objectives or Seminar were (i) To create among the health professionals an awareness of and sensitivity to the existence of torture in the Asian region. (ii) To disseminate information on international covenants

and international humanitarian laws regarding the prohibition of torture and ethical responsibilities of health professionals involved in torture, in the diagnosis of torture and in the care for survivors of torture (iii) To initiate the creation of a network of care givers in the area who will document the incidence of torture and treat survivors, exchange data and experiences with their colleagues in the region, and advocate for the eradication of torture in their respective countries. .

The participants from Bangladesh, Bhutan, Cambodia, China, India, Indonesia, Nepal, New Zealand, Philippines, Sri Lanka and Thailand expected to understand the scope, dimension and skills required for treatment, and caring for victims of human rights abuse. Most had experienced

torture themselves. A representative from a conflict ridden country shared, "our people do not talk to each other anymore". The intensity and implications of his words stayed with us all through. The four day training program with presentations, discussions and set of reference material provided useful insights into the understanding of torture, torture perpetrators and their victims, the role of health professionals in treating, caring and helping survivors to resume healthy and meaningful life. Torture is one of the obscenities that has persisted down the ages and has only become more sophisticated and refined in its technique in order to avoid being confronted with indisputable evidence. The sexual torture including rape technique is used to degrade and "break down the sexual identity and sexual capacity of the victim and to instill the fear that she/he will never again be capable of having normal sexual relationship. All kinds of humiliations are inflicted to confirm their views of the victims as inferior persons or' non-persons or objects. 'The experiences all over the world, force us to recognize that we must now redefine rape and torture to include children in its scope. The articles in the reference material and the presentations in the training session elaborated no torture today, the International Covenants and Humanitarian Laws Articles by Bent Sorenson and the Indian Medical Association on International Declarations, Principles and Guidelines applicable to medical professionals are comprehensive to make the reader sufficiently knowledgeable. However, it is evident that the plethora of International Laws and Covenants cannot initiate individual and professional level action. Both mainstream ·social scientists and health professionals have for very long time relegated the issue of violence, torture and. violation of human rights and the ensuing trauma of victims and survivors of violence, to the backstage. The painstaking research and documentation done by a small number of human rights activists, physicians, physiotherapists,

psychologists and scientists (on methods, psychological reactions and after effects) are represented in a couple of presentations by Dr. Bhogendra Sharma and Peter Vesti, Finn Sommier, Marrianne Kastrup. Some scientists' work in their home countries, while others work with tortured refugees, asylum seekers in host countries. Medical professionals have played an important role in political asylum cases, by providing expert testimony on historical, physical, and psychological evidence of torture and mistreatment. The concept of Alternative Medical Report can be an efficacious tool for documenting and prevention of torture: Dr. Veli Lok gave examples of his experience of working with torture victims in Turkey in which they have given AMR for 62 cases. He explains the examination procedures like a detailed anemnesis of torture, routine XRay graphics for lesional and related areas, CAT, MRI, EMG for suspension, 'biopsy for electro shocks, scintigraphy for hard beating and bastinado, dynamic scintigraphy for testis tortion and squeezing. The positive outcome of using evidential scintigraphy has been the prevention of bastinado in Izmir, since 1990. The application of the forensic sciences to human rights investigations is put forth forcefully in an article by Jorgen 1. Thomson. He states that the speciality of forensic medicine includes pathologist, odontologist, psychiatrist geneticist and toxicologist'. The author provides some features .of work by forensic scientists by giving examples from Argentina and Iraq. The presentation by Richard Claude highlighted how forensic medicine' serves four purposes: it helps families uncover the fate of their loved ones; sets the historical record straight; uncovers legally admissible evidence that will result in conviction of those responsible for the crime and finally deters future violations. Here it is enlightening to know about the work done by leading scientific human rights groups who have brought forensic medicine in Human Rights to the fore. The Minnesota Human Rights Advocates, Physicians for Human Rights (PHR), the American Association for the Advancement of Science (AAAS) and Amnesty' International have encouraged other local teams in Argentina, "Chile, Brazil, Columbia

organisations, committed scientists can succeed in the most adverse conditions. The UN Manual on the "Effective Prevention and Investigation of Extra legal, Arbitrary and Summary Executions", contain general principles and model protocols for the investigation and prosecution if such executions. Although not all countries will have sufficient resources to carry out their investigations exactly as prescribed in the UN Manual, it can prove useful as a guide as is pointed out in Amar Jesani's (India) paper. He stresses on the need for following such routine standardized and scientific investigation procedure and for making the process more accessible to the common man. A survivor has to face cruel memories, mistrust of the world, illness, guilt, anxiety, heavy responsibility, loss of esteem, and the re-adaptation and therefore there cannot be an automatic and harmonious adaptation. S/he has to be supported in this process by treatment, caring, counseling and rehabilitation centers. The underlying emotion has to be empathy and respect for her/his world and experiences.

A study report on the victims at RCT shows that they had the following symptoms: - 85% sleep disorders; 829(nightmares; 78% memory impairment; 76% impaired ability to concentrate; 72% identity changes; 700% low esteem; 13% signs of psychosis. However, in a majority of cases, they found a great discrepancy between the large number of physical complaints and inadequate medical findings. The reason for attending to such complaints as a standard procedure is primarily because the medical need of the survivor should be respected and because it is a prerequisite for building trust before undertaking any indepth psychotherapy. The survivors are psychologically injured to various degrees inspite of having undergone seemingly similar experiences. The factors influencing the psychology of the survivors could be any of these: age at the time of torture, personal ideology, pre-trauma personality, sex, socioeconomic status, ethnic background, religion, torture forms, and pre-trauma health condition. It is also influenced by the strategies used for adaptation. The characteristics of community resources and peer group support may be other sources of resilience. At present our knowledge about the exact importance of these factors is insufficient.

and Guatemala to come forward. The case studies of such teams in El Salvador, Argentina, Croatia, show

how

coordinated

efforts

by

physicians,

specialists, counselors, lawyers, judicial bodies and human rights

The RCT has gained a lot by sharing with centers from other parts of the world. Article by Soren Bejholm and Peter Vesti provide insights into the need for and impact

of multi-disciplinary methods of treatment. The combination of providing physical treatment, physiotherapy, psychotherapy, social advice and legal advice concurrently proves to be more successful. These experiences have encouraged the movement towards newer alternative approaches (like holistic, indigenous, feminist) in matters of treatment and rehabilitation. Their techniques have been nurtured while working in culturally distinct societies. Considering that medicine is a subculture with its own beliefs and practices, every framework, e.g. biomedical, the psychological, has its own concept of treatment. The indigenous system is a strand with several variants emerging from the need to develop and propose perspectives and techniques of therapy that .are enriched by the basics, within the framework of our people and culture. The mystical conception of disease and healing used in faith healing must be understood for 'indigenization from within'. Similarly, conceding that in eastern cultures the definition of self is not exclusive of the family and community and where a more dependent or external orientation is valued and accepted, is essential. Rebecca S. Gaddi's 'Practical Issues on torture And Sexuality', espouses that in the case of women, the absence of gender-sensitive service providers to handle cases of sexual violence and the existing inadequacies in the justice system leave a deeper impact on their self-image. The feminist therapy lays more emphasis on listening to women's experiences and sharing them with other victims. The therapists now understand that women victims were actually reacting largely to unhealthy and oppressive circumstances in their lives, to structures and systems that ignore and undermine their potentials, apart from simply looking at the direct cause or source of the trauma. Instead of the one-on-one approach, the new therapy has found mutual support group to be more effective. Certain norms and beliefs like sexuality, virginity have to be reconstructed. Two presentations by Nemuel Fajatagana and E. Marcelino and Pales a Mahlangu stressed on understanding the cultural, religious moorings of the concerned people, healing in a holistic manner with the mind, body and spirit as one, prayer to be seen as 'talking to someone', a/positive coping mechanism, to empower the victims to remember differently, to address the survivor's guilt and shame to normalize symptoms, and to emphasize coping strategies without developing them as psycho-

logical crutches. The examples from Africa indicated that we must extend the scope of work to ensure that we do not make a new set of perpetrators out of our survivors. The Medical Action Groups and the Philippine Action Concerning Torture have also been exploring specific client-relevant therapies. Elizabeth Marcelino, who has done pioneering work on children elaborated on how young people cope with stress and trauma arising out of situations such as sudden-and prolonged separation from parents, the stigma of being imprisoned, all kinds of physical and psychological and sexual torture inflicted on them' and near absence of emotional support from one's family members and from the community facing insecurity, fear and death. Since 1986, the internal armed conflict in Philippines has subjected millions of children to this kind of life. Their helplessness, frustration and anger expressed as, "I don't know, I don't want to know and I don't care." The therapy program involves both groups and individual modes of interaction in the form of free drawing, small talk, dramatization, self-discovery pictures and songs on child rights. The community based intervention programs mobilized the existing resources in the adults and other children and enhanced them with training, strengthened the community by providing financial, legal, educational assistance, skill development and caring of care givers. Even the most basic rehabilitation framework demands comprehensive understanding and skilled orchestration of interpersonal, family and community variables. The Medical Associations of different countries have a social responsibility of publicizing the ethical codes and guidelines, incorporating human rights 'in medical education, monitoring practitioners within their purview, and shaping national laws, Although their role has so far been dubious in most Asian countries, they 'have the potential for becoming important players in human rights promotion. The Indian example shows that a section of doctors are ignorant about this aspect' of medical work, another section is indifferent to the plight of sufferer due to their own social biases; a third section believes that being government employees, they are bound by the orders of their superiors and code of service rather than the Code of Ethics or The Hippocratic Oath. Their approach of seeing themselves only as technicians, with limited role as social beings compounds all these other reasons,

A major chunk of work in human rights is in the form of research and documentation. Without diminishing the importance of pure research, the present situation demands that the research exercises be made therapeutic, empowering and liberating. In a study with Filipino children, as well as with women raped in armed conflict situations it was reiterated that protecting their identity, treating and counseling them was an inherent part of the research program and had to be done simultaneously. Some of the participants had called themselves "walking wounded healers". Issues of healing of care givers with the help of team/peer group formation, crisis releasing exercises, keeping fit, meditation and creative expressions can go a long way in restoring the sense of well being and strength of the care giver. Counter transference were discussed in the presentations by June C. Pagaduan Lopez. Any therapy relationship inevitably leads to a dynamic and dialectical' link between the recipient of therapy and its provider. The set of psychic and behavioral reactions generated in the recipient is called transference and that generated in the provider is called counter transference. The acquisition of knowledge and skills has to be combined with retreat, healing, valuation and open, frequent process supervisions. Healing of self is the most important part of healing of others. In the article on "Care For Caregivers", Gillian Straker, shares about the crystallization of a process of training Aid Workers in Zimbabwe. 'Burn out', invariably brings in need for knowing others in the team better, to have meaning in their lives, to understand current tensions, to reconnect with one's past, one's sense of idealism, commitment, to get past the kind of cynicism, the more

pragmatic approach that actually emerges as one becomes immersed in this kind of work. Therefore, solutions of structural, interpersonal and intrapsychic nature are needed. Lastly, the ethics to be maintained in human rights promotion by activists in the field, the need 'to feel safe' through networking, sharing and supporting each other was also expressed. The pertinent questions on the international context of perpetuating, condoning human rights violations, the readiness with which international bodies grant 'impunity' to transitional governments, lack of funds for this work, hovered in the minds of participants and resource persons, but they needed a different forum for debate.

Contents of Reference Material (Copies are available from Cehat, 519, Prabhu Darshan, 31, SS Nagar, Amboli, Andheri (W), Mumbai-400058) . Torture Today Achievement After More than 20 Years of health Professionals' Work Against Government Sanctioned Torture Inge Genefke International Covenants and Humanitarian Law Regarding Torture

International

Abstract of Presentation Bent Sorenson Declarations, Principles and Guidelines Applicable to Health Professionals Indian Medical Association The Role of Health Professionals in Promoting Human Rights Health Professionals and Human Rights Adrian van Es and Tryggve Wahl in The Role of Physicians in Asylum Cases Physicians for Human Rights Guidelines for Prison Monitoring Guidelines for Visits to Prisons Bent Sorenson Detection And Management of Physical Sequelae of Torture Outline of Presentation Bhogendra Sharma Alternative Medical Report and Its Supports in Prevention of Torture Veli Lok, Orhan Suren, Emre Kaphin, Mehmet Tunca, Turhcan. Baykal, Suat Kaptaner Methods of Torture Peter Vesti, Finn Somnier, Marianne Kastrup Psychological Sequelae of Torture - Assessment and its Management Psychological After-Effects of Torture Peter Vesti, Finn Somnier, Marianne Kasrup Psychological Reactions of Victims During Torture Peter Vesti, Finn Somnier, Marianne Kastrup

Medico legal Investigations of Torture The Application of the Forensic Sciences to Human Rights Investigations Kari E. Hannibal and Robert H. Krishna Forensic Aspects of Human Rights Jorgen L. Thomsen Ethical Issues regarding Involvement of Health Professionals in Torture Medical Ethics in the Context of Increasing Violence Amar Jesani Ethical Codes and Declarations Relevant to Health Professionals (Book) Amnesty International Caring for Survivors: The Holistic Approach Multidisciplinary Approach in the Treatment of Torture Survivors Soren Bejholm And Peter Vesti Indigenous Practices as an Alternative Approach to Caring Nemuel Fajutagana and Elizabeth Protacio-Marcelino Rehabilitation of Survivors of Torture and Political Violence Under a Continuing Stress Situation: The Philippines Experience Aurora Parong, Elizabeth Protacio-Marcelino, Sylvia EstradaClaudio, June Pagaduan Lopez, Victoria Cabildo Special Concerns: Torture of Women and Children

Torture of Children in Situations of Armed Conflict Elizabeth Protacio-Marcelino, Maria Teresa C. de la Cruz Agnes Zenaida V. Camacho, Faye A.G. Balanon Community Participation in the Psychological Recovery & Reintegration of Children in Situation of Armed Conflict (The Philippines Experience) Elizabeth Protacio-Marcelino, Faye A. G. Balanon Agnes Zenaida V. Camacho, Maria Teresa dela Cruz Josefina Gabuya, Rosalinda Mercado Caring for Caregivers Counter transference Issues in the Care of Torture Survivors June Pagaduan Lopez Care for Caregivers Gillian Straker Research Issues in the Care of Torture Survivors: Ethics and Methodology Research Issues in Caring for Torture Survivors M. Brinton Lykes Empowering Research: A Politically And Culturally Sensitive Process Maureen C. Pagaduan The KAP Survey of Physicians Regarding TortureThe Indian Experience Abstract of Presentation Jagdish Sobti

••

Practical Issues on Torture and Sexuality Rebecca Samson Gaddi

(Contd. from page 5) As part of the work, Medical Officers in the Government medical service at Bhopal were trained. As far as we know, however, there has been no follow-up effort to sustain the quality of care and to assess the results.

Concluding Remarks While numerous findings revealed through the studies reviewed above are significant and may be important in various ways, there have been many research shortcomings, too. The initial choice and then radical change in the methodology in Study 1deserves some clarification and discussion from the investigators. The early focus given to mental health in certain population sub-groups, like women and religious communities, with significant findings, has been entirely dropped without discussion. The approach and methodology employed in Study 2 appears fundamentally flawed in assuming that neurological (used synonymously as intellectual) deficit is accurately measurable through an indicator like poor

school examination performance. We will not discuss this except to say that the study conclusion, which we have quoted, is unscientific and a shame. There appears to be a studied avoidance of the term 'post traumatic stress disorder' (PTSD), a recognised psychiatric entity expected to crop up in the aftermath of a major disaster. (The first time such a diagnosis was made was in the Oct. 1989 study carried opt by an independent team). This, we think was a deliberate omission as PTSD is event related and not related to the exposure to the chemicals. Hence, even those living as far away as 15 kms could have developed PTSD. As we noted, except for the NIMHANS study, the therapy element has been largely absent. Neither has there been follow up of the training given to doctors treating gas victim, nor have important research indicators been followed up. In general, low priority has been given to research and service in the area of mental health.

•

Clinical Re-appraisal: 5

Dengue Fever/ Dengue Hemorrhagic Fever Yogesh Jain & Sushi! Kabra Department of Pediatrics, All India Institute of Medical Sciences

Dengue fever is an infection caused by a virus which is transmitted by mosquitoes. In this infection, there is a short duration fever (<7 days) and severe muscle pains, pain behind the eyes, headache and severe backache. The disease may assume epidemic proportions, however, mortality is negligible. Occasionally Dengue fever may get complicated and may cause what is called Dengue hemorrhagic fever/Dengue shock syndrome. In this syndrome, after an episode of fever resembling Dengue fever, the defervescence of fever may be accompanied by severe volume depletion in the body causing shock and bleeding manifestations, either of which may result in death. This can also occur in epidemic proportions.

Dengue fever and its complications are essentially a disease of this century (Halstead, 1992). The story of spread of the dengue virus is closely linked to the development of human settlements in this century and specially to the urban development in developing countries. During the 18th and 19th centuries, epidemics occurred in newly settled lands, largely due to domestic water storage necessary for drinking/live stock. For its spread it requires (i) favourable temperature of 0 >20 C which leads to more infectious mosquitoes, and to mosquitoes which bite more frequently, a situation common in tropical countries (ii) stagnant clean water which occurs commonly in cities with ongoing construction/ demolition- typical of rapid urbanization and (iii) rapid and close transmission- a situation tailor made by overcrowding seen in city slums and multi-storied living quarters (Horton, 1996). Indeed, outbreaks of dengue fever in urban areas can be explosive with attack rates upto 70%. There are three important axioms about dengue. First, once dengue fever comes to an area, epidemics of dengue fever are likely to occur in subsequent years unless the vector is eradicated entirely. Secondly, if dengue fever has occurred in an area for a few seasons, dengue hemorrhagic fever is inevitable if a successive infection occurs with a different strain of the virus. Third, most dengue fever epidemics are noticed first in the cities of a particular country. In successive years, the disease moves out centrifugally to involve the rural areas and finally the entire country may be involved. The typical example is Thailand which has seen the entire march of this virus starting from Bangkok. Before we discuss the observations made in the last epidemic seen in Delhi and its neighborhood in Aug to Oct 1996, for a proper understanding, the following questions need to be answered:

Q. 1. How does a benign dengue fever give rise to a

potentially fatal disease like DHF? Dengue viruses are 4 strains-all of which can cause dengue fever. However, if there is infection with a second strain in a patient who has had dengue fever caused by another strain previously, this person is likely to develop DHF. The exact pathogenesis is not clear, but it is believed that antibodies produced after the first infection enhances the subsequent infection related complement activation. This complement activation causes generalized permeability of the vasculature; altered blood coagulability results from increased platelet destruction. In an area which has the mosquito (Aedes aegypti-the tiger mosquito) and the dengue virus (most tropical countries), dengue fever is likely to occur in the rainy seasons every year. If more than one strain causes these illnesses, DHF will occur too. Not all people who develop sequential infections get DHF. The reason is not clear. One hypothesis (Halstead, 1992) suggests that if antibodies are of neutralizing type, they do not enhance the growth of subsequent dengue viral infection. However, if there are no neutralizing antibodies, the other antibodies enhance the viral replication. The question still remains, why do some people develop neutralizing antibodies and others do not. In the Cuban epidemic of 1981, blacks were affected 5 times less often than whites. Perhaps, this example of enhancement of a second infection by the first one is unique among the entire spectrum of microbial infections in human beings. Q. 2. What has been the epidemiological trend of Dengue/DHF in other countries? Dengue fever is present on 3 major continental landmasses: the Americas, Africa and Asia. Dengue viruses of multiple types are endemic in most countries of the tropics—the implication is that DHF can not lag far behind.

The first cases of DHF were reported in 1950 from Thailand. Over the next 20 years, the entire South and Southeast Asia reported DHF with frequent epidemics. The first case in India was reported in 1963 from Calcutta. Thereafter, epidemics of DHF have been reported from Vishakhapatnam (1964), Kanpur (1968), Ajmer (1969), Jalore (1985), Delhi (1988) and Vellore (1990). Cuba reported the sharpest epidemic in 1981 with 116,000 hospitalizations within a 3 month period. If the present trend continues; it is likely that epidemics may occur frequently. In countries where dengue is endemic, DHF is among the ten leading causes ~f death in children aged 1 to 14. Experience from the Cuban epidemic of 1981 and Thai epidemics suggest that for every case of DHF stages III/ IV, there are 150-200 cases of dengue infections in the community. The usual mortality is about 5% if treatment is adequately given. One third hospitalised children required intravenous fluids in this epidemic. DHF remained a disease of children and young adults for almost two decades. However, the age group being affected is changing. In Singapore, the median age for DHF in 1973 was 14 years with most fatalities occurring in children below 10 years. However, in 1994 the median age increased to 20 years (Goh, 1995). In the 198f), epidemic in Delhi, more than half' of the hospitalized patients were adults. However, among the children with DHF, more than half were in the 6-12 years age group. DHF may be fatal in 40, 50% of untreated patients (Benenson, 1990); however, with appropriate treatment the mortality can be brought down to 1-5% (Halstead, 1993) Q. 3. How is DF/ DHF diagnosed? The clinical diagnosis of dengue fever is made only with a high index of suspicion or in an epidemic situation. Many other infections causing short duration febrile illness would be in its differential diagnosis. Dengue hemorrhagic fever (DHF) is diagnosed if a similar illness is accompanied by thrombocytopenia (<1, 00,000 platelets per cu mm) and presence of hemoconcentration i.e., rise in Packed Cell Volume (PCV) by more than 20% of baseline/recovery value (WHO criteria, 1996). If there is pleural effusion or ascities, DHF may be diagnosed even in the absence of hemoconcentration. The more advanced stages of DHF III and IV (also called DSS IIIIIV) occur when there is additional evidence of hypotension or narrow pulse pressure (see Table 1). Marked apprehension and irrita-

bility may be more important clues of poor perfusion than measurement of BP in a young restless child. In an epidemic situation, over diagnosis is common. However, health workers must be cautious in not missing other mosquito borne or water borne illnesses which may Occur in the same milleu which is suitable for dengue e.g., malaria and typhoid fever. Occasionally, dengue fever may be accompanied by minor or major bleeding manifestations without hemoconcentration/hypotension. This needs to be separately diagnosed because therapy is only symptomatic here and for bleeding there is no need for fluid replacement. Table 1

WHO Criteria for DHF Stages I II III IV

Uncomplicated (Hess' Tourniquet test positive, but no spontaneous bleeds) Spontaneous bleeding. Evidence of hypotension. Profound shock with imperceptible pulse or blood pressure

Q. 4. How is DHF/ DF managed? Patients with dengue fever are treated symptomatically for fever and body aches. In the scenario where DHF is a one should avoid all non-steroidal anti inflammatory agents like Ibuprofen, aspirin, nimesulide etc; for fever only paracetemol should be used. Otherwise the bleeding tendency may be enhanced because of possible platelet function abnormalities due to use of above mentioned drugs. If unusual bleeding manifestations occur with DF, they should be treated with blood transfusion if hemorrhage is significant enough to cause hypotension or severe anemia. If DHF occurs, there are two major principles of management. First, monitoring: perfusion should be monitored by recording pulse rates, blood pressure and pulse pressure by using appropriate cuff size and mercury sphygmomanometer, capillary refill time, temperature of the peripheries, central venous pressures in selected cases and packed cell volumes. Second, fluid rehydration is instituted. Children with DF are given IN fluids if they are vomiting. For DHF stages I and II, oral fluids (75 ml/kg over 6 hours) similar to that of moderate dehydration in diarrhoea are given. If the child is vomiting, the same volume is given parenterally over 6 hours. Thereafter, fluids are given adlibitum orally or 1500 ml/Sq. m. body surface area per day parenterally with frequent monitoring of perfusion and packed cell volumes. For all stages III and IV, 20 ml/kg lactated Ringer's solution is infused intravenously as fast as possible. If hypotension persists, 10 ml/kg colloids (plasma/

hemacel) are infused. If improvement is seen, lactated Ringer's or normal saline is infused @ 5-10 ml/kg/hour for the next 48 hours or till the PCV drops below 40. Frequent PCV monitoring is continued till the child is euvolemic for 24 hours. Intravenous fluids are discontinued when PCV drops below 40 or after 48 hours whichever is earlier. Q. 5. How can the disease be prevented? The disease can be prevented by adopting the following measures: i) The breeding places for the mosquito be eliminated.

Simply emptying all water collection objects every week will reduce growth of larvae. These water sources which cannot be emptied can be treated with larvicidal like Abate. ii) The adult mosquito may be additionally destroyed by adulticidal space sprays given frequently. iii) Personal protection using protective clothing, mosquito repellant cream, bed nets etc. All these are difficult but achievable tasks. Singapore is one country which has been able to drastically reduce Aedes transmission by enforcing stringent laws and levying heavy fines for harbouring Aedes breeding sites. In epidemic situations, fogging with adulticidal drug sprays is recommended in the area around a diagnosed case. Presently; no vaccine is available against dengue infection, and it is unlikely to be available soon. It has been suggested that in an area/country with poorly developed health care set up, chemical vector control [(ii) above] is the most cost effective strategy ; whereas better case management is more cost effective where health system . is better developed (Shephard and Halstead, 1993). Of course, environmental control with elimination of breeding places is the most lasting measure.

Lessons from the 1996 Delhi Epidemic Delhi witnessed an epidemic of Dengue/Dengue hemorrhagic fever from August to November 1996. The first case of DHF was admitted with us in AIIMS on the 18th of August. However, the diagnosis of DHF was made through postmortem and presented in our weekly mortality audit. Thereafter, due vigilance was exercised in the diagnosis and treatment of cases. Most of the clinicians working with us began to be convinced that the number of cases with clinical diagnosis of DHFIDF were more than usual. By

recording the details of the cases and guidelines for the management of the patients. However, no formal communication was sent to the Health authorities. This was because (a) there was no possibility of virologic confirmation at any of the central institutions; (b) there was the likelihood of harassment by the Health authorities; and (c) there were really no legal bindings. In the second week of September, an English newspaper published a story on the epidemic along with a statement from a Doctor from AIIMS confirming cases of DHF/DSS admitted at AIIMS. The government machinery thus got activated. The outcome was in the form of statements, mostly contradictory, as to whether there were cases or not and whether there was an epidemic or not. These statements published in English dailies, without ascertaining the background of the persons concerned, only led to more confusion especially among the medical personnel. Since no systematic attempt was made to investigate the epidemic, it was believed that the cases were from just one hospital (AIIMS) and that it was really not a major problem. However, in the next 2 to 3 weeks, cases began to be reported from other hospitals also and there was some attempt at taking control measures. But by then a crucial delay of 4 to 6 weeks had already taken place. This delay would not have occurred had there been a central surveillance and monitoring unit. We feel that a central body to monitor diseases of public health importance must be constituted. This body should make available, on a regular basis, the number of cases of such diseases and in case of an outbreak or an epidemic, it should take on the responsibility of providing reliable information and monitoring the outbreak. Clinicians must be asked to notify any abnormal increase in the number of cases they may notice during the course of their work to this central body for necessary action. During this epidemic, we interacted with many general practitioners, pediatricians and other specialists. It was quite evident that knowledge and experience regarding the nature and management of DHF was inadequate among general practitioners and pediatricians. For instance: * Patients with fever and platelet counts between 1-1.5 lac were sent for platelet transfusion; * A girl with cold extremities following 4-5 days of fever was kept in a nursing home with a diagnosis of typhoid; * A child with sore throat, cervical adenitis, and fever with a platelet count of 3 lacs was diagnosed as DHF and sent for platelet transfusion;

definition, this was sufficient to predict an * A professor of pediatrics gave aspirin to his son with a epidemic. Case Fatality rate was low. In the diagnosis of dengue and wondered why NSAIDs should pediatrics department of AIIMS, of the 240 cases not be given. admitted in various stages, there were 18 deaths. At the hospital level, a proforma was prepared for

Ignorance of doctors led to under as well as over diagnosis of the disease and inappropriate treatment. This underlines the need for continuing medical education for both doctors and paramedical workers. In our experiences the guidelines suggested by the WHO for the diagnosis and management of DHF/DSS was found to be largely satisfactory. According to the guidelines, a diagnosis of DHF/DSS requires a history of fever, platelet count of <1 lac/ml" and hemoconcentration of >20% in packed cell volume. During this epidemic, in 10% of our patients we could not demonstrate hemoconcentration of >20%. These patients had significant bleeding and thrombocytopenia and required blood and blood product infusion. According to the WHO manual, children with fever, thrombocytopenia, and bleeding without hemoconcentration are classified as dengue fever with abnormal bleeds. Our experience in this epidemic suggests that such patients form a sizeable proportion in an epidemic and their management protocol needs to be discussed in detail. These patients need less intravenous fluids and more blood and blood products infusion. In few children, intravenous fluid infusion improved central venous pressure, decreased packed cell volume but the blood pressure continued to remain low. This indicated that the hypotension was probably due to failure of pumping action of the heart rather than loss of fluid from the vascular compartment. We investigated these patients and demonstrated myocardial dysfunction by Echo cardiography. There is need to study whether myocardial pump failure has any therapeutic implications by studying a larger number of children. This would be in order to decide the role of vasopressors in selected patients. Few atypical clinical features observed by us need to be mentioned. These included hepatic encephalopathy and dengue encephalopathy. These patients were initially thought to be suffering from hepatitis or encephalopathy but when tested for dengues were found to be positive. Thus in an epidemic situation, certain atypical features may cause problem in diagnosis. Similarly, conditions such as malaria could also be confused with DHF. Children who presented with anaemia, fever, bleeding, splenomegaly and thrombocytopenia were found to be positive for malarial parasite (falciparum) and treated successfully. It is suggested that since splenomegaly is rare in DID', presence of splenomegaly should raise the possibility; of other diagnosis. However, in areas with endemic malaria, where spleen rates may be 1-10%, whether this holds true needs to be studied. Few cases of typhoid were also confused with DHF due to leucopenia,

thrombocytopenia, hypotension and bleeding. These patients did not show marked improvement after intravenous fluids suggesting an alternative diagnosis. During the early part of the epidemic we were doing Hess' capillary tests on all the children. Later on we realised that this test is difficult to carry out in small children. On analysis of our data, we found that it was positive in only 56% of patients. Low positivity could be a reflection of improper techniques. We feel that in children the tourniquet test is of limited value. During the course of this epidemic, several questions came up repeatedly which need to be resolved. These were: • What should be the screening tests for an OPD patient? Should a simple clinical examination with a tourniquet test be sufficient or a leucocyte count with platelets and PCV be required? Will the above screening tests be of any value in a child presenting on day 1 of fever? • How to reconcile the differing sensitivity of platelet counts done by smear, counter or other methods? Even in our hospital, the counts done by two different departments differed significantly in one child. Some children had low platelet count as done in a private lab which was found to be normal in our hospital. • Can the course of the illness be predicted using clinical and laboratory indicators? • Is there any role for bolus steroids and gamma globulins in the management? • The research question related to microbiology included the need for the development of a rapid diagnostic test and study of the pathogen of the disease. References: 1. Halstead SB. The twentieth century dengue pandemic: Need for surveillance and research. World Health Stat Quart 1992; 45:292-8. ~ 2. Benenson AS (ed). Control of communicable diseases in man. Washington DC: American Public Health Association, 1990. 3. Halstead SB. Global epidemiology of dengue: Health system in disarray. Trop' Med. 1993; 35:137-46. 4. Horton R. The infected metropolis. Lancet 1996; 347:134-5. 5.Goh KT. Changing epidemiology of dengue in Singapore. Lancet 1995; 346: 1098. 6. World Health Organization. Dengue Haemorrhagic fever: Diagnosis, treatment and control. Geneva: World Health Organization, 1986. 7. Shepard DS, Halstead SB. in Disease Control priorities in Developing countries Jameson DT (ed), Oxford University Press, New York, 1993.

•

Obituary

Nirmala Sundharam Nirmala was much younger than me and so when I got a message in Dehradun, from Sathya, that Nirmala was fatally ill, I could not believe it. I returned to Delhi on the same day. I could not believe that the patient on the ventilator lying in coma was Nirmala. If I could feel so helpless what about Nirmala's husband Subhash and other family members? Next day Nirmala was no more. It is strange that memories of moments lived, forgotten, resurface with such clarity and painfully at such moments. The first reaction of course is why did you not say all the good feelings you had about the person to her. I never told Nirmala that I deeply admired and respected her decisions about life, I never told her that she was amongst the very few who genuinely could rise above pettiness, above "I, me, mine" syndrome, that she was amongst the very few whose conviction and grittiness found expression in action, quietly, perseveringly, undertaken. Her concern was deep, arising out of a deeper understanding. To be able to know, analyze and understand is one thing; to genuinely empathise as an 'equal' and not 'patronizingly' is another thing. To openly "Take Sides" in action with the exploited and the oppressed when there are more remunerative and more comfortable options is a totally different thing. To do so, not as a 'matyr' but as a friend, colleague and 'comrade' gives so much strength, builds self-respect and genuinely empowers others. Nirmala could live up to her own convictions with her head high, caring in her heart and a smile on her face. It is strange how many remember her as an enthusiastic zestful soul with a 500 watt smile. Friends living tough lives, relating to the bitterness, difficulties and pain in other people's lives, usually develop serious and intense look and they rarely find cause to smile. It was Nirmala's genuine warmth, large heartedness, and her cheerful nature which made her so different, as she herself selected a difficult path. Nirmala and I had joined VHAI together in 1979 and were doing the one year Community Health and Development Residency. I was coming straight after having done my post graduation in Internal Medicine from C.M.C. Ludhiana; Nirmala from Economics background from Stella Maries. It was a revelation for me that 80% of the doctors chose to care for 20% of the population who had purchasing power, that large part of our people did not have access to basic services, that caste, class,

gender, urban or rural status had so much to do with health status. We spent 3 months in the 'Centre for Studies in Rural Development' in Ahmednagar, lived through a period of intense drought, sharing one bucket of water amongst three of us for the whole day. We learnt about digging of wells and sharing of water, about cattle, about crops, about land, about people, about power, use and misuse and abuse of power, about powerlessness, about the needs in different sections of society, about social justice, equitable distribution, about the role of socially conscious health and development workers. We had people like Dr. Ron and Edith Seaton, Coordinators of the programme, Father James Tong, Anne Cummins, Carol Huss, Ruth Harner, Simone Leigeois within VHAI and numerous preceptors to share an understanding and belief in community health. Such health care work was not remunerative and hence those who opted for it were those who chose to swim against the mainstream and were willing to pay a price. As I got more involved with School Health, issues of Rational Drug Use, Nirmala was more and more involved with Community Health training, evaluation, and initiating community health action. "Taking Sides" written by Sathya, Nalini and her, till today is considered one of the finest training materials in Community Health. When Nirmala opted to work with Unions and unorganised construction workers and leave VHAI, it was a' very brave decision and Nirmala lived upto it unflinchingly. She believed in herself, in the people she worked with and in her convictions. Nirmala had so much still to give, as she was needed much more now by her family, her parents who were her inspiration, her husband, Subhash Bhatnagar and her three year old little twins, Supriya and Sukanya, her friends, her colleagues and the workers. At Nirmala's funeral hundreds came to pay their last respects; people cutting across religion, across class. In her short life she had touched the lives of so many and each one had felt supported strengthened and cared for. We will miss you Eugenie, your cheerful warm hearted and caring approach to people while living a life of conviction and selflessness as a genuine human being. Mira Shiva VHAI, New Delhi


