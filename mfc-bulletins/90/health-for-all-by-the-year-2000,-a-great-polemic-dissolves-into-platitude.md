---
title: "Health For All By The Year 2000, A Great Polemic Dissolves Into Platitude"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Health For All By The Year 2000, A Great Polemic Dissolves Into Platitude from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC090.pdf](http://www.mfcindia.org/mfcpdfs/MFC090.pdf)*

Title: Health For All By The Year 2000, A Great Polemic Dissolves Into Platitude

Authors: Nabarro David

Medico friend circle bulletin

1)

JUNE, 1983

HEALTH FOR ALL BY THE YEAR 2000 A GREAT POLEMIC DISSOLVES INTO Platitudes? DAVID NABARRO* Department of Human Nutrition, London School of Hygiene and Tropical Medicine, London WCIE 7HT Introduction: Health for all and Primary Health Care

The PHC approach has three major components:-

The appealing slogan "Health for all by the year 2000" was coined by international agencies in the mid1970' s to usher in an era of intensive activity by Health Ministries in many Third World countries. The ministries have adopted policies which concentrate on providing Primary Health Care for their people — on bringing a new style of comprehensive health services within reach' of those who traditionally have been deprived of them.

1)

Increasing the availability of medical care facilities that tackle life-threatening problems commonly encountered by all sections of the population men women and children.

2)

Emphasizing the extent to which people can help to prevent illness for themselves by adopting healthier lifestyles and changing harmful practices.

In conventional health services medical care facilities are concentrated in large hospitals: the emphasis is on doctors and nurses administering medicines and other treatments to cure their patients’ illnesses. Little attention is paid to the underlying causes of illnesses or their prevention through changing people's behaviour patterns or living conditions. Public health measures however are taken and where effective government services exist may serve to reduce the health risks that people face in their everyday lives. In conventional health services the recipient of care tends to be passive. They have little or no say and certainly no involvement in the way that these services are delivered. Mainly observers have commented that these conventional services do not meet the needs of the people they are designed to serve, are only accessible to a privileged few and have little impact on health. The Primary Health Care (PHC) approach is

3)

Promoting the involvement of the people served in the delivery of their health services.

radically different from this conventional pattern. It has, since 1977, been promoted widely by the World Health Organisation (WHO).

In order to develop PHC Services with wide coverage. Health Ministries throughout the world have established new groups and medical manpower - different grades of medical and nursing auxiliaries, trained over a short period to undertake a small number of basic tasks. However many countries have found it difficult fully to implement PHC. They have also begun to question whether PHC services really can be expected to improve health - particularly whether they will lead to the achievement of "Health for all by the year 2000",

Health — a problem of definition It is difficult to find a definition of "health for all" that is likely to be widely accepted. Good health is not an absolute condition. What does eacHof'9S mean by a healthy individual or a healthy community? Does the healthy person not suffer from any diseases? Does he or she experience illness, just like anyone else but have a low risk of dying or becoming disabled when young?

* Presently on secondment to Save the Children Fund. P.O. Box 992, Kathmandu. Nepal.

Most of us would identify the healthy person as someone who is unlikely to experience physical and mental disability and who faces low risks of premature death. In a community of hear1thy people few children would die before the age of one (infant mortality rates .would be low); most people would live to a ripe old age (average life' expectancy would be long); and children would grow rapidly in childhood (average growth rates of children would be fast). Few children would suffer disabilities like 'blindness or lameness. Using numbers which describe levels of illness death or nutrition in the population (like infant morality rates. child growth velocity, child blindness prevalence or average (life expectancy), we can identify populations that are more or less healthy than others. But there is no absolute value for "any one of these variables which indicates good health. We can identify countries in Western Europe which have Infant Mortality Rates of 15 per thousand and others in South Asia where the rates are 100 per thousand or more. Countries with high Infant Mortality Rates will usually be described as less healthy than those with lower rates. Inside each country it will always be possible to identify groups of people whose infant mortality rates vary. By the same token, these groups are more or less healthy than each other. Members of these different groups may be identifiable in terms of their sex, occupation, religion, caste, ethnic origin or where they live. Usually, it is the wealthiest families whose members have the highest life expectancy and lowest infant mortality rates. The poorest families experience the reverse. They are the least healthy. Variations in health of different population groups inside countries are found both in the "developed" nations of Western Europe and in the "undeveloped" nations of the Third World. Although national average values for health indices emphasise the extent of deprivation faced by most people in Third World countries they disguise the existence of these intra-country variations. In practice, the goal of the "Health for Ail by the year 2000" movement is to reduce the disability and mortality rates experienced by groups of people throughout the world to the level currently experienced by the most healthy (who are usually the privileged) This means reducing, for 'example, infant mortality, child and adult disability rates for countries in the Third World towards levels experienced by countries in the West. Similarly, the rates for different population groups inside countries — characterised-by their' economic-or caste status, ethnic origin or gender — need to be reduced to the level of the healthiest. Effective ways for improving the health of unhealthy population. How best can “health for all” be achieved? First we have to consider the causes of ill health among the

world's least healthy populations. Many of the deaths and disabilities in population groups with high infant and child mortality rates are due to infectious diseases. As they go about their daily lives, people in, these groups are exposed to large numbers of the pathogens which cause disease - particularly those responsible for illness in the gastro-intestinal and respiratory track. Following this exposure, people will be particularly susceptible to developing illness as a result of which they may suffer severe consequences (disability or death). This exposure to large numbers of pathogens results from people living in environments that are highly contaminated. Problems are accentuated in hot climates which encourage the multiplication of pathogens in food and rotting vegetation; People are unable to obtain enough water to keep themselves clean. There is no sanitation: widespread defaecation increases the environmental contamination. There is insufficient fuel for householders to sterilise foods they prepare. People are susceptible to infection by pathogens because their body defences against them are weakened. When pathogens have come into contact with an individual, the body surface defences, together with the immune and inflammatory responses, are called into action: These defence systems help to stop the 'pathogens invading the body and causing disease, disability and death. People living in conditions of high exposure to pathogens are particularly likely to undernourished and to have weak body defence system as a result. The population groups who are exposed to large numbers of pathogens, who lack the facilities to protect themselves from their effects and whose body defences are weak are also likely to have only limited access to medical care services. When ill they are limited to using traditional remedies. They are unlikely to have access to the powerful medications that modern curative care can provide. The PHC approach recognises that many of the infections whish cause deaths and disability are preventable. Through intensive health education and promotion combined with mass immunisation, water chlorination and so on, PHC workers attempt to modify people's lifestyles. People are, encouraged to take steps to reduce the contamination of their environment, lessen the number of occasions on which they come into contact with these pathogens and to increase their, body defences against those pathogens which do succeed in invading. PHC also involves the regular surveillance of groups of people who are likely to become ill (particularly children) the detection of early illness and its prompt treatment using the minimum of safe medicines; Uncertain, benefits of health education For the last five years I have worked alongside Primary Health Care Workers in South Asia and the

Middle East. Frequently have been struck by the way in which even the unhealthiest groups of people in a population (usually the poorest) try to respond to educational messages about how they can modify their lifestyles and make themselves healthier. Families' often recognise the value of, and understand, the changes being required of them. They may change their practices, but only in special circumstances, such as when in hospital or a nutrition centre. Yet, back in their own homes, they are 1ess able and likely to alter the way they live and the practices they follow. The people who do change behaviour consistently in response to education are usually those who have spare cash, food time and consider the investment worthwhile. Other families do not have the spare resources to make the changes required - or even if they have the resources, do not see the point of changing. Health care workers, after delivering educational messages, may well receive· challenging replies Why should I make these changes in the hope of living a year or two more when I do not know where I will .find the next month's food?" Poor people are not likely to be confident that the future has good things in store for them: this hopelessness is an inevitable barrier adopting new "healthier" behaviour. One example of this is found in the rural areas' of South Asia, where severe disease is most common during the early monsoon months while family members are particularly busy in the fields: irrigating, pouching, sowing, planting, weeding and so on. Time is precious - families may well consider that an hour or two away from fields during these months will have an adverse effect on the subsequent harvest. Yet it is spare time more than anything, which- is needed by mothers and fathers when nursing a sick child through a bad attack of diarrhoea - time to encourage the child to eat or drink, and time to attend a clinic to receive treatment and medical advice. During these months, too, food is in short supply. The family may not have the special nourishing foods needed by a sick child. During the months when they and their children are more likely to be ill, poor farmers and agricultural labourers are so short of time, food and cash that they are unlikely to adopt measures which will prevent or contain illness.

It is no surprise that the people who are most at risk of being ill - or having children who are ill - are also least likely to come regularly to child health clinics for health check-ups. They may not even be able to afford to give up time at work and stay at home to wait at home when a health worker’s visit.

Regular personal health surveillance only

becomes possible when people consider it worth their while to attend health clinics or to wait at home when a health worker is due to visit. Perhaps they

face fewer competing demands on their time or have decided that the services provided by the clinic or health worker are really useful and beneficial. A number of well-reported non-governmental programmes provide very popular services which are regularly attended by a wide range of people from the populations they serve. In some situations, government run programmes achieve the same results. In these exceptional cases, much of the success results from the personal commitment and organisational skill of district medical and nursing officers. To what extent can medical professionals improve people's health? The observations I describe have primarily been made in South Asia. They suggest that attempts to change people's lifestyles are likely to be ineffective or at least, inefficient, ways of improvi.'1g the health of the least healthy people in that region. Personal health surveillance programmes will only work if they are wanted by the people they serve - this inevitably restricts their effectiveness to the more privileged groups in society. Among poorer communities, surveillance activities are only likely to reach people if they are run by committed health workers who communicate well and inspire the people they set out to serve. They may still only have a very small impact on the health for all people they set out to serve. These observations suggest to me that even most carefully managed new-sty-e Primary Health Care programmes will be unlikely to lead to substantial improvements in the heath of the least healthy people in a population. They will only become more healthy if their wealth, incomes, access to services like water or sanitation, and intakes of food improve first. My observations have been based on a few health programmes in a small number of countries from one part of the world. But they are not unique - the kinds of constraints described are felt by health workers in other parts of the world. Behind the "Health for all" slogan is WHO's major concern the health of people throughout the world. But WHO is also specifically concerned with the medical, nursing, public health and allied professions, and their work, The WHO's main point of contact with member nations is with their health ministries and with medical and allied professional’s worldwide. It is important all of us make clear our personal commitment to the goal of health for at but that we recognise that members of the medical profession can db little to help achieve this goal. Medical and paramedics/ professionals are well positioned to investigate the causes and consequences of ill health. However, they are rarely in a position effectively to promote improvements in the health of unhealthy populations.

By launching a campaign for "Health for all by the year 2000" WHO has issued a polemic. The concept revolutionary - it implies that there should be redistribution of resources to the poorest by the year 2000. One professional group, on its own, cannot possibly hope to age the revolutionary transformation required to eve health for all. By implying that poor people's health can be improved without enabling them to increase the productive resources at their disposal or to improve their access to water, fuel, housing and so on, WHO and other international organisations who adopt slogan might even be working against the achievement of "Health for All".

Indeed, "Health for all" is not a realistic goal for pie who trying to plan and run basic health care services. They end up designing policies or drawing up proposals for programmes that sound good and read well but are almost impossible to implement successfully. The plans are just wishful thinking or platitudes. The whole Primary Health Care movement is awash h platitudes at every level, and they distract us from making a critical assessment of the issues that underlie World Health Organization’s challenge. In the villages where they work, Primary Health care fieldwork

ers are often confused and disheartened - the people they serve disenchanted. Increasing numbers of health professionals and administrators criticise the Primary Health Care approach because they are unable to make it work. But we should not be surprised that improving the health of the least healthy people in the world is difficult. We should not expect that it will be easy or inexpensive - to provide widespread health care services, change people's lifestyles 1hrough health education or elicit people's participation in their health care. These are all political activities: they require action by groups and communities; they are concerned with the distribution of power between different groups in society, between professionals and the people they serve. The implementation of the activities embodied in Primary Health Care programmes inevitably involves conflict. Yet so much of the literature written about Primary Health Care ignores the financial, political and professional barriers to improving people's health and to developing new patterns of health services. In this respect, the guidance health professionals receive is often misleading unrealistic and unhelpful. One example is the -way in which health professionals are encouraged to seek the people's participation in the services they provide. (To be continued.)

RATIONAL THERAPEUTICS SELECTION OF APPROPRIATE DRUG U. N. JAJOO

With advancement in pharmacology, more and re new drugs are added to the list. A physician has choice to select a drug from a pool of drug having similar therapeutic properties. Under the influence of persuasive selling practices of pharmaceutical firms physician tends to neglect the rationality and believes in the claims of drug industry, thus prescribes costly drugs. The sufferer is the patient.

ANALGESICS & ANTI-INFLAMMATORY DRUGS: Few facts to note: i)

ii) iii)

There are some guiding criteria which needs to be lowed by scientific mind before selecting a drug. Each drug needs to be weighed on the following criteria: i)

Effectivity or potency of the drug

ii)

Toxicity of the drug

iii)

Cost

iv)

Availability in the market An attempt is made here to rationally

analyse analgesics and anti-inflammatory drugs on the above criteria.

iv)

There is a large variation in the response of individuals to different analgesic-antiinflammatory drugs, even when they are closely allied members of the same chemical family. If drug must be given to a pregnant woman, low doses of aspirin are probably the safest. Only those drugs which are extensively tested by time should be used in children. This commonly means aspirin, indomethacin and ibuprofen. Most of the diseases where anti-inflammatory drugs are used have a chronic course. This means palliative therapy with anti-inflammatory drugs needs to be continued for long time. Thusi'1 out of the above listed criteria toxicity and cost have priority consideration.

v)

vi)

vii)

viii)

Fixed dose combination of different analgesics anti-inflammatory drugs must be denounced on the ground that their dose needs to be titrated in every individual and usually sensitivity of one drug cross reacts with other. Timed-release preparations of salicylates are of limited value, since the half time for elimination is so long. Absorption from enteric coated tablets is some timesincomplete.5

iii) Among injectable analgesics, there is little to choose. Cost is prohibitory for pentazocine while toxicity for analgin.

Salicylamide- is no longer an official drug. Its effects in man are not reliable and its use is not recommended. The small doses included in "over the counter" analgesic and sedative mixtures are probably ineffective."

1. For chronic inflammatory joint disorders like osteoarthritis, rheumatoid arthritis, ankylosing spondylitis — Aspirin remains the first drug of choice followed by Ibuprofen and Indomethacin.

of Oxyphenylbutazona, a metabolite Phenylbutazone, has antirheumatic and sodium retaining activities similar to those of the parent drug. It is extensively bound to plasma-proteins and has a half life in plasma of several days. It accumulates significantly during chronic administration and contributes to the pharmacological and toxic effects of parent drug and hence ranks low as compared to phenylbutazone. - See Table No.1 & 2.

ix)

Acetaminophen has less over all toxicity and is thus usually preferred to phecnacetin.

iv) For antipyretic effect - Inj paracetaucal remains the drug of choice for hyperpyrexia though cost is prohibitory. ANTI-INFLAMMATORY DRUGS:

2. For conditions like Gout Indomethacin and phenylbutazone can be used, keeping in mind their toxicity over prolong use. 3.

Aspirin remains the drug of choice in pregnant women and children.

4.

Steroids score high in view of its potency and low cost, but dangerous side-effects on long term use necessitate it to be pushed down as a last race. Their combination with non-steroid antiinflammatory drugs must be denounced and are unethical.

REFERENCES: 1.

Which Anti-rheumatic drug by F. D. Hart. Drugs 11: 451, 0976.

2.

Annual Review of Pharmacology. Vol. 6, 1966 page 157. Vol. II. 1971, page 241. Vol. 19: 469, 1979.

CONCLUSION: Analgesic Antipyretics: i) ii)

Aspirin stands as the drug of choice, except in conditions where gastric erosion is endangered. For long term use, aspirin stands as the drug of choice.

3. New Eng. J. of Med. 302, 1179, 1980. —, — 302; 1237; 1980. 4.

Journal of Applied Medicine.

5.

The pharmacological basis of therapeutics, 6th Edition, Goodman Gilman.

6.

J. of Applied Medicine 6, 735, 1980.

FOR YOUR ATIENTION DEAR READERS, NOW AVAILABLE

Revised and fresh version of paper on RATIONAL MANAGEMENT OF DIARRHOEA - made upto date. Anybody who would like to have a copy may please send Rs. 5/- per copy. To, Anant Phadke, 50, LIC Quarters, Pune- 411016

Table -1 ANALGESICS ANTIPYRETICS …….SELECTION OF APPROPRIATE DRUG Drug

Analgesic efficacy & (Score-I)

i) Aspirin

Anti-Inflammatory efficacy

** (4)

Toxicity (Score-II)

0

(3)

Dose per day

1-2 gm

Cost of treatment per day & (Score-III)

Total Score (I x n x III)

300 mg tab= 2np

4x5x3=60

Comments

14 paise (5) ii) Paracetamol

* (2)

0

(4)

1-1.5 gm

500 mg tab =15nb

(Acetaminophen)

2x4x4=32

— Suitable substitute for aspirin in patients

45 paise (4)

when aspirin is contraindicated (peptic ulcer) or when the prolongation of bleeding time caused by aspirin would be a disadvantage. —

— — — —

iii) Codein

* (2)

0

(3)

30 mg QID

10 mg tab=10np

iv) Dehydrocodein

* * (4)

0

(3)

60 mg QID

not available

—

v) Dextropropoxyphen

* * (4)

Available in only

—

(4)

2x3x1 =6 Rs. 1.80 (1)

A dose of 25 gm or more is potentially fatal. Chronic abuse may cause methyaemoglobinaemia. Drugs should not be used over long time. Its combination in ‘over the counter’ drug is not justified. If given should be a S.O.s. dose. One of the common drug incriminated in poisoning with analgesics in western countries.

— Constipatory — Addiction potential though less

— Minimal dependence.

in combination

INJECTABLE ANALGESICS i) Inj Pentazocin

* * (4)

0

(4)

50-75 mg QID

30 mg/ml=2.5 Rs.

— Smell abuse potential (dependence).

ii) Inj Analgin

* * (4)

0

(3)

500 mg QID

500 mg/ml=0.25 Rs.

— Standard pharmacological books does not mention this drug at all

iii) Inj Acknil

* * (4)

0

(4)

150 mg QID

150 mgl2ml=1.25 Rs. (1)

2x4x1=8

Table - 2 ANTI-INFLAMMATORY DRUGS SELECTION OF APPROPRIATE DRUG Drug

Analgesic

Anti-inflam-

Toxicity

Dose

Cost of treatment

Score

efficacy .

matory efficacy (Score-I)

(Score-II)

per day

per day & (Score-III)

Total (I x II x III)

Comments

..

Aspirin

**

**** (4)

(4)

5 gm or more

300 mg tab==2np 0.54 Rs. (5)

4x4x5=80

- Extensively used over long time - Safest drug in pregnancy.

Phenylbutazone

**

**** (4)

(3)

200-400 mg

200 mg tab=25np 0.50 Rs. (5)

4x3x5=60

- Should not be given for more than 7 days due to cumulative toxicity. - Cannot be used for chronic disorder for long time, not recommended in pregnancy. - Urico-suric drug.

Oxyphenbutazone

**

**** (4)

Indomethacin

**

**** (4)

Ibuprofen

**

*** (3)

(2) 200-400 mg

(2.5)

25-150 mg/day 75-100 mg/HS

(5) 600-1200 mg

100 mg tab=25np Re. 1.00 (2) (4.5)

4x2x4.5=36

- More toxic than the parent drug (Phenylbutazone)

25 mg tab=25np Rs. 3.50 (3)

4x2.5x3=30

- High incidence of severe side effects over prolong use. - Not safe as Aspirin or Duprofen. - Nrico-suric drug - Not recommended in pregnancy

200 mg tab=60np Rs. 3.50 (3)

2x5x3=30

- Better tolerated than other drugs. - Better tolerated than other drugs - Not recommended in pregnant women or lactating mother.

Naproxen

**

*** (3)

(5) 375-750 mg

250 mg tab = 2Rs.

3x5x1 = 15

- Cost prohibitory.

Rs. 6 (1) Ketoprofen

**

** (2)

(5)

100-200 mg

Not available

—

Fenoprofen

**

*** (3)

(5)

300-600 mg

Not available

—

Enfenamic acid

**

** (2)

(5) 800 mg twice a day upto 2400

Flufenamic acid

**

** (2)

400-600 mg

Mefenamic acid

**

* (1)

750-1500 mg

Not available

Steroids (Prednisolone)

0

***** (5)

5-75 mg

5 mg tab=25nb

400 mg tab=75np 3 Rs. (3.5)

5x3x5=75

Not available

- STII under trial, An Indian Product - More toxic, less effective - Not to be used for more than 7 days - Safety during pregnancy is not established.

2X5X3=35

- Drug of last resort in view of long term side effect

From the Editor’s Desk In Bulletin No. 82 (Oct. 1982), I said that MFC should think of issues other than diarrhoeas, too. However, Abhay Bang has been persistently urging me to write on use of rice powder solution as therapy for diarrhoea. I am therefore forced to come back to this topic. We had reproduced (Bulletin No. 78 & No. 79) report from The Glimpse which outlined the experience of the ICDDR. Dacca, in using cooked rice powder solution as ORT. The advantages of starch over sucrose in the ORS were discussed. The study itself appears to have been based on the traditional practice in Bangladesh of feeding soaked rice flakes (Chudwa) with salt o children with diarrhoea. In some parts of South India, it is the practice to feed rice gruel called "ganji" or sago gruel with salt added. In metaphysics, life is referred to as a cycle Samsaara). Everything we perceive in life appears to move in cycles too. In thirty years, we seem to have traveled a full circle regarding feeding starchy solutions o child. Repeated episodes of diarrhoeas and other infections are known to precipitate the development of kwashiorkor. The common practice of withholding normal diet and of feeding starchy gruels during diarrhoeal episodes was considered to be one of the important precipitating factors. Mothers and grandmothers were to be educated about these dangers. The Fifties constituted the era when traditional practices and home remedies were considered irrational, unscientific and generally detrimental to health. We had forgotten that

modern day medicine grew out of ancient, empirical therapies. Then came to be Seventies, China pulled up its "bamboo curtain". Barefoot doctors, acupuncture and herbal remedies are the "in-thing". We have now a new found respect for traditional medicines and therapies. This is the cycle. One of the definitions of a cycle is, "a recurrent sequence of events which occur in such order that the last event of one sequence immediately precedes the recurrence of the first event in a new series". Therefore we need to pause and reflect. What is the truth in the fear that starch feeding could lead to kwashiorkor? Earlier it was believed that excess feeding of carbohydrates, without sufficient protein leads to kwashiorkor. It is true that present day theories are different. But the change is only with respect to the general dietary pattern of the community. The role of starch or carbohydrate cannot be totally denied. A kwashiorkor - like condition can be produced in experimental animals by feeding starch. Therefore, simultaneous health education will be needed when advocating rice powder or similar solutions as ORS. We must realize that WHO-formula type ORS will be treated by the community as medicine, but not so the traditional rice water or sago gruel. It is essential to tell the mothers that this has to be fed in addition to milk and other normal constituents of the diet. One also has to examine whether, in view of the greater density of rice water compared to glucose or sucrose solutions, the child will be able to ingest the diet plus rice water. In treating diarrhoeas, let us not bring down more children with kwashiorkor.

WEANING FOOD AND DIARRHOEA The International Centre for Diarrhoea Disease Research, Bangladesh, has conducted a study on the role of wearing food in the causation of diarrhoea and against of it is published in their Bulletin, Glimpse December 1982. The study included 70 children between 5 to 18 months, of whom only 30 were exclusively breast-fed. Atleast 10 food and 10 water samples of each of the 40 children receiving weaning foods were examined for bacterial contamination. 70% of the samples were cooked rice, 16% cow or goat milk and 14% -were other foods containing rice or atta and milk. Nearly half of the milk samples and of other foods had E. Coli. About 37% of the rice samples were also contaminated. The colony count increased with environmental temperature. In milk and milk containing foods count

increased with storage time. Generally the food was cooked early in the morning and stored. Half of the water samples were also contaminated with E. Coli, counts being higher on hot days. However the counts were 10 fold lower than in food specimens. Contamination of food was directly and significantly related to incidence of diarrhoea among these children. The report says that this clearly demonstrates the importance of food as the vehicle of transmission of E. Coli. The report goes on to say that this hazard must be kept in mind while recommending home-made wearing foods to children. As far as colony counts in water are concerned a study from NIN, Hyderabad (Ann. Rep. 1980) showed that the counts were less when water was stored in mud pots as compared to that in metal vessels.

Editorial Committee: Anant Phadke Padma Prakash Ravi Narayan Shirish Datar Ulhas Jajoo Kamala Jayarao - Editor

Views & opinions expressed in the bulletin are those of the authors and not necessarily of the organisation.


