---
title: "Dear Friend (Ban On Liquid Tetracycline)"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Dear Friend (Ban On Liquid Tetracycline) from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC077.pdf](http://www.mfcindia.org/mfcpdfs/MFC077.pdf)*

Title: Dear Friend (Ban On Liquid Tetracycline)

Authors: Anand Raj

medico friend circle bulletin MAY 1982

Misuse of Antibiotics, Antimicrobials A medical practitioner with time slowly realises that there are really few diseases where allopathy can offer a cure. An infective illness is one such area ‘Antibiotic’ is the greatest tool that modern medicine offers today against bacterial infections. However it is a double edged sword, if not utilised properly it not only harms the patient but also has wide ranging social implications, evidence of which is ample in the medical literature.

Will you call this misuse? Antibiotics account for 20 % of drug sale in India (1976 figures). Many times it is difficult to prove that drugs are being misused or irrationally used, because in majority of prescriptions, the doctor hardly ever writes the diagnosis. Even from the hospital records it is difficult to conclude correctly because written documents do not mention all that is in the mind of a treating doctor (It speaks of our recording quality.), say for example: i) A critically ill patient of men in go-encephalitis where diagnosis is uncertain; use of Inj Chloromycetin, inj. Chloroquin, Inj. S/M, INH together can be justified to cover up enteric encephalopathy, cerebral malaria, tubercular encephalitis and pyogenic meningitis. It is a shot gun therapy, but is justified if one takes into consideration the seriousness of the illness and non-availability of investigational support. ii) A child with upper respiratory infection may have conducted throat sounds in chest which are wrongly interpreted as crepitations and thus patient is

thought

to

have

bronchopneumonia.

Use

of

antibiotics now is perfectly justified. It may be a serious mistake on the part of the treating doctor that he did not examine after the child is made to cough, but is a part of the game which has to be conceded.

iii) A child with severe diarrhoea is treated with a combination of anti-protozoal (Metronidazole) and antibiotics to cover up wide range of diarrhoeal diseases in a setting where examination facility is not available. This may also be justified if one keeps in mind that the doctor will not like to delay the treatment and risk the child's life. iv) It patient with fever of more than 7 days duration who cannot afford to get his blood investigations (Widal, blood culture, peripheral smear for parasites) done for a perfect diagnosis, is put on Trimethoprim + sulpha combination to cover up resistant malaria, resistant' typhoid fever, and gram negative septicaemia, this may be justified. It all means, that the prescriptions may vary considerably in the same patient in different settings The budget of the patient, availability of investigative procedures, human error on the part of the doctor al' have their say. Therefore it is difficult to rationally analyse some-one else's prescription without knowing the situation in detail.

But there are indirect means to judge that doctors do overshoot. Indian literature in this regards is scarce. However there are studies available where the prescribe is informed in advance that his / her prescription will be screened for appropriateness of the drug prescribed or he / she is asked to fill up a form justifying the use of antibiotics These trials have shown decreasing trends in antibiotic use up to 25 % (1-5). In other studies where physicians have been made to write the diagnosis over the prescription, it was found that antibiotics were prescribed without any evidence of infection in as many as 62 % 90%

Ulhas N. Jajoo Bajaj Wadi, Wardha, 442114

6

prescriptions . Thus there is no doubt that antibiotics are improperly used. How lire antibiotics improperly used?

A) Used when not indicated e. g. i)

ii)

iii)

For common cold and all upper respiratory illnesses which are in majority self limiting viral infections. It is estimated that as many as 12% prescriptions of antibiotics are given for 8 common cold. For acute diarrhoea in children without any evidence of dysentery, severe malnutrition, septicaemia. For viral infections without any evidence of bacterial super infection.

B) Used when contraindicated, e.g. i) A patient of chronic renal failure gets sulpha-drugs, tetracycline, amino glycosides (Nephrotoxic). ii) A new born infant gets Chloromycetin (grey baby syndrome).

iii) A diabetic patient gets sulpha drug like Trimethoprim + sulpha

combination

(Papillitis Necroticans).

iv) Inj. streptomycin in a patient of ear-disease (ototoxicity).

C) Irrational combinations, e. g. i) Penicillin with tetracycline or Chloromycetin (See appendix). ii) Gentamycin + Kanamycin (two drugs of the same group).

D) Improper selection of drug, e. g.

vi) Use of Rifampin + Pyrazinamide + INH in a case of defaulter of tuberculous treatment who has turned up for the first time to the hospital. Majority of these patients 'still respond to primary drugs", and in our setting shift to costly drugs of secondary line is not justified. vii) Use of Chloromycetin ear drops which contain propylene glycol as preservative which irritates the ear. viii) Using chloromycetin + streptomycetin combination orally for cases of acute diarrhoea (streptomycin need not be given in short lasting bacterial diarrhoea. The common organisms are not sensitive to this drug.)

E) Defective route of administration: i) Use of Injection chloromycetin when patient can be given oral drug. (Injectable drug has erratic absorption).

F) Inadequate doses: The possibilities are i) Doctor prescribes dose for inadequate duration ii) The patient does not have enough money to buy the total course of antibiotics, thus either reduces the dose or the duration. The notorious drug misused by doctors is injection Terramycin which is available in the concentration of 50mg/ml. For adequate dose, 5 cc of this oily preparation has to be given in an adult which is so painful that probably patient will not come back: The most convenient way is to reduce the dose. (125 mg I ml. concentration is not generally available.) It serves two purposes; one it reduces cost to the doctor and seconds it continues to give satisfaction to a patient of getting a coloured injection. What are the harmful effects? i) Adverse reactions ii) High cost of the prescription (See appendix) iii) Resistant bacterial infection.

Mutation in genes destroys affinity of target site for the antibiotics or modifies permeability of the cell so that i) Use of Ampicillin because organisms are thought to antibiotics cannot enter the cell and find its target site. This is be resistant to penicillin (Ampicillin does not act against the mechanism for development of resistance in a given patient. penicillase producing organisms). However, problem of drug resistance does not remain limited to ii) Demeclocyclin is used when other the patient. Drug resistance in an infectious organism can be Tetracyclines which have less toxicity and transmitted to other sensitive organisms of the same or different equal effectivity are available. species through so-called “Resistant Factor. This drug iii) Erythromycin Esteolate (hepatotoxic) is used when resistance is due to ability of the bacteria to modify the one other salt of erythromycin (erythromycin antibiotics with the help of certain enzymes that they can ethylsuccinate) which is less toxic and equally effective is produce. The modified antibiotics cannot recognise their cellular target and therefore have no inhibitory effect 0 n the available. cell. iv) Use of penicillin G when more acid stable preparation (Penicillin V) is available. v) Routine use of Inj. streptopenicillin for bacterial infection. Tuberculosis is being so rampant and streptomycin being one of the cheap primary line of drugs, routine use of this combination is not justified if one keeps in mind the drug resistant tuberculous infection.

To make things worse, this transmissible resistance is against series of drugs (multiple drug resistance). The fact that R factors can be, transferred to every genus of enterobacteriasae through 'non-pathogenic bacteria like E coli (normal inhabitants of intestine) has, become a major public health problem. If a. person harbours E coli with R-factor in the intestinal tract, they can turn sensitive pathogenic organisms like shigella, salmonella, V. Cholera to resistant ones. If this continues further, we may reach a situation when the future' of chemotherapy can appear bleak. Evidences for this type of 'resistance in the Indian situation 7 is many. ( ) Studies done on healthy subjects who had not consumed any antibiotics for atleast one month showed 28% of them harboured multiple drug resistant strains of E. coli and much lower l6%) incidence of multiple drug resistant strains among individuals of nearby village The resistance was' predominantly for drugs like Sulfonamide, Streptomycin, chloromphenicol, ampicillin, kanamycin, and tetracycline which are most commonly used antibiotics. Resistance to, newer drugs like gentamycin and trimethoprim has also emerged. Why antibiotics are misused? The possible reasons could be:i) Poverty of knowledge of the prescribe ii) Shot gun therapy iii) Antibiotics are prescribed also by the doctors from other disciplines' of medicine such as Ayurveda, Homeopathy and Unani etc. i.e. those who are supposed not to be qualified allopathic practitioners. iv) Persuasive sales, promotion by drug pharmaceuticals which are often the' only source of knowledge for a busy-practitioner. v) Easy availability of these drugs over the counter to the public who quite often practice self-medication. vi) Absence of cross-checks on the prescribing habits of the doctors. vii) Consumer is unaware of the harm that mis-use of antibiotics can inflict.

Is there a solution to the problem? — Refresher course for the doctors on indication of' antibiotics in infective disorders? — Availability of antibiotics only by the prescription from the qualified allopathic doctors? — A mandatory justification by a doctor for the prescription of antibiotics? — Abolition of different brand names and insistence on generic name?

— Mass education of the "Consumers" about the indications of antibiotics uses in common infective illnesses? I personally feel that the last option will be most effective, if one keeps in mind "that antibiotic misuse involves vested interests of both doctors and drug industry.

Appendix- I Cost of Commonly used antibiotics Drug Dose per tab./cap/

Sr. No.

...

1. Sulphadiazine 2 tabs 3 times (M&B) x 5 days 2.Penicillin-V 130mg 6 hrly.x (M& B) 5 days 3. Tetracyc line 500 mg6 hrly. (Paran) X 5 days 4. Chloramphenicol 500 mg 6 hrly. X5 days 5.Septran 2 tabs. twice (Bruxwell) a day x5 days 6.Inj. Gentamycin 40 mg 8 hrly (Lyka) -80 mg. x 5 days 7.Kanamycin-lgm 1.5 gm total X 5 days 8. Amoxycillin 1 tab. 3 times a day x 5 days 9. Doxycyclin 2 stat; 10.0. (US Vit)-100mgx 4 days

Inj. Cost

Total Rs. cost

0.30

9.00

0.48

9.60

0.34

13.60

0.31

12.40

1.00

20.00

10.20

76.50

1575

133.00

1.70

25.50

1.80

10.80

References: 1) JAMA 2585: 242, 1979 2) JAMA 242-l981, 1979, 237: 2819; 1977; 227: 1023, 1974; 227: 1048, 1974 3) Annals of Int. Med. 76, 537, 1972; 79: 55, 1973 4) Med Ass()c. J.116 : 253, 1977 5) Lancet:2: 407,1981: 2,461: 1981; 2: 349, 1981 6) JAMA. 213:264, 1970 7) Science Today: Sept, 1981page 26 8) Insult or Injury 1979, published by social Audit Charles Medawar p. 123. 9) WHO Expert Committee on tuberculosis-9th Reper. No. 552, p. 21. 10) Ann. of Int. Med. 128 : 623, 1971 11) The pharmacological oasis of therapeutics, 5th edition, by Goodman and Gilman, [This article is based on the discussion paper that Ulhas Jajoo had prepared for the VIIIth Annual MFC Meet] .

HEALTH ACTION INTERNATIONAL This is an "international antibody" formed in May-198J "to resist the ill-treatment of consumers by multinational drug-companies." It’s a network of about 50 groups including consumer developmental and other groups interested in health and pharmaceutical issues.

IOCU-BUKO conference by the British research action group, Social Audit; confrontation at company’s annual general meetings; and the possibility of inter-national consumer boycotts and legal actions against “the truly intransigent."

Achievements: Member groups of Health Action International will address such issues as: An end to the commercial anarchy of prescription drug competition (for instance in India, there are some 15,000 branded drugs on sale-compared with just 225 “essential drugs" identified by the WHO) "An end to patent protection for essential drugs. The "essential drugs" identified by WHO "are too important to be left in a monopoly domain.” The progressive replacement of proprietary brands with generic drugs-which usually cost many times less The "decommercialisation of essential drugs" assuring that people who need drugs get them. Regional or national production and bulk-buying arrangements to reduce to an absolute minimum the cost of essential drugs.

HAI has forced Searle to revise labelling of Lomotil. It is active in the boycott against Ciba Geigy for the sale of clioquinol drugs in many parts of the world. A news bulletin [HAI, News] is being published from October- 81 every two months. It will serve as a channel for communicating news on the activities of HAI participating members, ideas, new information and resource material in health and Pharmaceuticals. The International Organization of Consumers, Unions (IOUC) links the activities of consumer organizations in some 50 countries. An independent, non-profit and nonpolitical foundation, IOCD promotes world-wide cooperation in consumer protection, information and education. The Headquarters of IOCD are at 9 Emmastraat, The Hague, Netherlands. The Regional Office for Asia and Pacific is at P. O. Box 1045, Penang Malaysia. For further information contact Editor, HAI-News on this address. [Based on IOCU newsletter]

Immediate action plans for the new coalition, Health Action International includes: Setting up an international clearing house for information on commerciogenic disease; pharmaceutical industry structure, ownership and marketing practices; and for the coordination of consumer action campaigns. The launching of an international Hazardous Product Warning Network- "the Consumer Interpol.” The withdrawal or restriction on the use of any drug in anyone of five reference countries will trigger immediate communication to each of over 110 different organizations in some 50 countries. The aim is to encourage local groups to pressure government and industry to effect simultaneous restrictions - and to avoid the commonly-found double standards between developed developing countries. Direct actions will be aimed at the worst offenders in the drug industry: publication of counter information, such as the leaflet on Lomotil released at the

Boycott against Nestle pays off Nestle, the giant multinational company has been selling its breast -milk -substitute powder all over the world. It had refused to accept the WHO code of marketing of breast- milk substitutes Infant Formula Action Coalition, [INFACT] consisting of over 50 organizations has organized an international consumer boycott against all Nestle products. Feeling the crunch of the boycott, Nestle has now agreed to comply with the WHO code. The boycott is to continue" until the Nestle actions match their rhetoric ... abiding by both the letter and spirit of the WHO code, country by country around the world. “For further information contact- INFACT 1701 University Ave. S. E. Mpls. MN 55414, U.S.A.

Attention Please! Campaign against irrational use of drugs The mass-educational campaign against hormonal pregnancy-test was an encouraging experience. Articles on this issue were published in most of the leading dailies and some periodicals all over India. Now Voluntary Health Association of India, MFC, Arogya Dakhata Mandal and other similar organizations have sent letters to the Drug Controller of India asking him to take specific steps that we have outlined to curb the misuse of hormonal preparations in pregnancy. Various women's organizations are also sending similar letters' to the Drug Controller. The manner in which different organizations co-operated with each other has raised expectations about the next campaign. MFC Voluntary Health Association of India, Arogya Dakhata Mandai and like - minded people are starting a mass-educational' campaign on diarrhea, oral rehydration and uselessness of most of the commercial antidiarrheal preparations. Let us make this campaign a better organized one and on a larger scale than the one against hormonal pregnancy-test. A scientific background paper on this topic would be ready by 15th May. It is expected that Bulletin readers would make use of this paper to write articles, give talks and make posters etc. for the people. The paper will be sent to those who ask for it. Please send Rs. 5/- (or more!) if you can, to cover the cost of cyclostyling.

The Voluntary Health Association of India (VHAI) requires a young, energetic and enthusiastic medical doctor (MBBS), to work with the Community Health Team. The candidate should be prepared to travel extensively and be able - to organize and conduct workshops / seminars in Community Health and Development mostly in rural areas. Experience preferable. Salary will be commensurate with qualifications and experience. Apply within two weeks with bio-data to: Voluntary Health Association of India C-14, Community Centre, S.D.A., New Delhi 110016. Manan Ganguli writes: Their centre can supply oral rehydration packets prepared according to WHO formulation. Price Rs.0.75 per packet plus postal charges write to Lok Kendra, Titamo. Jabhaguri Panchayat, Madhupur 815353, (Santhal Parganas) Bihar.

Dear friend At the VIIIth Annual Meet of MFC at Tara, there was a' discussion about banning of liquid tetracycline preparations. There was a consensus that some times there is a choice between a child receiving a cheap antibiotic like tetracycline \ which is less hazardous than chloromphenicol) or no antibiotic at all. In rural situations a patient may not afford to come to the clinic from his village daily for a five day course of Procaine Penicillin. It was therefore argued that unless we know the incidence of' tetracycline-side effects, at what dose they are more likely to occur etc, we should not ask for a ban on these liquid preparations. Dr. Anand is continuing the debate in his letter below “I support the ban on tetracycline liquid preparations for

the following reasons. 1. Side effects due to tetracyclines like staining of teeth, depression of bone growth and increased intracranial pressure in children are very well documented (Goodman. and Gilman's pharmacological Basis of Therapeutics, 6th Edition). Samples used after the date of expiry have been found to cause renal damage due to degraded products of this particular antibiotic. Since returning from the MFC meet held on 23rd and 24th Jan. 82, I have seen four cases of ugly looking stainning of teeth due to tetracyclines. It can be argued that it was a coincidence and that this observation need not reflect the true incidence of the disease. But is it also possible that sometimes we do not notice certain things unless we make a conscious effort to look for the same? For instance, how many of us carefully examine the teeth so as not to miss an apical tooth abscess as a cause of unexplained fever? 2. Many of us who have been treating children of poor parents from slums and from the rural areas have not used tetracyclines for bronchopneumonia and other infections for many years. We have never felt handicapped for this reason. 3. I have seen prescriptions of tetracycline liquid preparations for new-born babies. If these were not available, the doctors who have been prescribing the same are less likely to use the capsules of tetracyclines."

Raj Anand, Bombay.

PRIMARY HEALTH- CARE: THE REAL PICTURE There is much written, and published on alternative health strategies by health planners, highlighting the various options available to ensure that at least basic health services are available to the masses. The most recent to climb the ambitious ladder being the “Health for all by 2000 A. D." THE EXPECTATIONS

Starting with the Bhore committee that set out recommendations as early as 1946, a series of committees have made voluminous recommendations on the new approaches, new breed of health workers and changes in medical education, training unipurpose and. multipurpose workers and the like. This apart, there are atleast more than half a dozen schemes sponsored both by the centre and the state, operating through the state machinery of the Primary Health care services, like malaria control, leprosy control; tuberculosis control, Family Planning besides the programme of mother and child care which covers a gamut of basic services. The promises of primary health care hinge heavily on the Primary health infrastructure covering a population of 1,00,000. The primary health centre is generally staffed by two medical officers, one of them being a lady medical officer and other staff. The final unit of 1000 population is manned by one male and one female worker. The ‘premise' is that these front line workers will deliver services to meet the primary health needs of the masses. In Karnataka these front line workers are the ANM and the multipurpose worker. Some of the specific Primary health care functions of the front line workers aremaintenance of ante-natal care, eligible couple and child registers, referring mothers with gynaecological and medical problems follow up of these referral cases, conducting deliveries immunization of underfives, distribution of iron tablets and contraceptives and referring cases for medical termination of pregnancy and sterilization. This note attempts to high light a few obvious, loop holes at the final stage of health care delivery. To begin with, how far in reality have the front line workers and the medical officer internalized the concept of preventive service and health education? What is the expectation of people to whom the services are delivered?

THE REALITY AT PERIPHERY

A simple example based on the observations made in the field in a backward area in Karnataka is taken for elaboration. It was found that the focus of the PHC service was predominantly curative. The available curative services are again used only by the people living within a certain radius, further helped by communication facilities. Immediate needs were for penicillin injections, Paracetamol, treatment of minor wounds and accidents, ‘B' complex injections as placebos and other therapies needed for diarrhoea and viral infections. However it was not uncommon to observe a mother carrying a prescription for ‘protinules' recommended by the doctor for a two year old bordering on second degree malnutrition; a mother rushing in a child with diarrhoea and fever of unknown cause, being treated with two doses of analgesic and two doses of 'antibiotic', as revealed by the doctor himself. If this be the practice of qualified doctors then nothing much be said on the practice by the male workers. How is one going to stop such practices? Most of the front line workers are busy building the health data base. Much of the preventive services on paper never reach the huts. There is hardly any direct supervision of their field work the responsibility rests heavily on their honesty and trustworthiness. The medical officer with a meager vehicle allowance of Rs, 300/- month can hardly hope to move out of his head quarters. There is a constant shortage of essential drugs, b1 aching agents and DDT so much so they can hardly cover the target area leave alone the frequency with which they have to be used. The undue emphasis given to family planning services has come under much criticism especially after the Emergency. The pressure was for meeting a target. A critical look at the resource allocation for family planning shows a continued increase of Lie Family Planning budget as a proportion of the total health and Family welfare budget (Table 1). At the same, time, in successive plans investment in health has occupied a smaller and smaller share of the total planinvestments.

On the weak shoulders of the ANM who has taken on an unenviable role rests the success of the primary health care services. Sandwiched between the government and the rural people she becomes a weak line in the chain. Demedicalisation of the people's health beliefs and expectation of health services appear to be some of the crucial issues in the battle to be won. People no longer feel ‘Health as a personal responsibility'. The dependency on the allopathic system of medical care has penetrated to the practitioners of indigenous medicine even in interior villages. The indigenous system of medicine is dying a fast death. The practitioners have resorted to antibiotics and injections to raise the hopes of the people for a fast cure. The relationship is one of customers receiving service and officials delivering the service. Sometimes a poor image of the health worker leads to non-acceptance of the preventive services.

Table 1. Family Welfare and Health Budgets by Plans FW budget Rs. Million

Health Ministry Rs. Million

1st Plan (1951-'56) 1.45 2nd Plan (‘56-' 61) 21.56 3rd Plan ('61-66) 248.60 Annual Plans '66-'67 '68-69. 704.64 4th Plan

1939

Health Plan as % of Total investment 006 3.3

3697

0.58

3.0

7,128

3.49

2.6

7238

9.74

2.1

2,844.33

21,372

13.31

2.1

4,507.00

35,434

12.72

1.8

Year

'69-'74

FW as % of Health budget

5th Plan ‘74-'78

* includes medical, FP, sanitation and water supply ** provisional figures Source: Department of family welfare, Year Books, 77-' 78 and' 78- '79; Pocket Book of Health statistics, Govt. of India.

A PIPE – DREAM? It is heartening to read the policy recommendations made by the recent committee set up jointly by ICSSR and ICMR. One of the recommendations reads- "...to replace the existing model of health care services by an alternative new model which will be firmly rooted in the community and aiming at involving the people in the provision of the services they need and increasing their capacity to solve their own problem..." The report has also outlined the conditions essential for success. 'The alternative model which is decentralised and participatory will require a different set of attitudes. The bureaucrats and the professionals will have to cultivate respect for, the people and a faith in their ability to identify and find solutions for their problems." This is easily said than done. The well meaning expert committee has stressed on similar hopeful conditions. They add "…the success of the alternative model will depend largely on the quality of health services. This will of course include the usual problems as proper selection, training, creation of proper conditions of work and service, adequate supervision and guidance and good administration which will ensure justice and fair play and relate rewards visible to quality and punishment to failure". We cannot afford to forget that for the very same reasons not only the programme of primary health care but several other well planned schemes have met with incredible failures. In the present, complex sociopolitical environment how is one to assure these conditions" Who is to fulfill these responsibilities’? As is clearly seen, most of the plans and recommendations on paper beautifully fit into the objectives to be achieved, but at the point of delivery the link hardly withstands the socio-political pressures and the problems of personnel. If the PHC were to be used as the point of delivery, it is that phase of implementation that should receive maximum attention. On the other hand, the entire ideology gets watered down at that stage. Finally to conclude, the fast changing socio-political and cultural conditions do not let the accepted modes of basic services to percolate deep down to the really needy people. The blocks are far too many not only in the health services but also in many of the rural development services. Unless there is commitment to clear these blocks on a war footing, the promises of primary health care will continue to be a pipe dream. Vanaja Ramprasad.

••

FROM THE EDITOR'S DESK The Poor Man's Poison is Nobody's Concern The above statement is from one of the four articles by Prabhash Joshi on lathyrism, published by Indian Express consecutively starting April 9, 1982. Two MFC sympathisersBeena Naik of Navsari and R. Chandrasekhar of Madras, have written to say that MFC should do something about it.

I may remind our readers that in December 1977 I wrote all about this problem (Kissa Khesari Ka Bull. No. 24). I traced the historical development, the clinical picture, the first ICMR survey, the suggested solutions and the total indifference of the government, the' impractical' solutions released from the research laboratories, etc. I used it as a model to show the socioeconomic structure of our public health problems. Abhay Bang was much excited and enthusiastic over the issue (Bulletin No. 26). As a result, the M FC organised a 3- day survey of rural Rewa. As a strict, “scientific" epidemiological survey, it had drawbacks. But, for each one of us who participated in it, it was a rich, never-to-be forgotten experience. "Walking 15-20 km. every day in the hot sunny summer of central India, sleeping under trees, starving for the whole day, tolerating insults, threats from the police, all these formed the memories…" (Luis Barreto, Report of the Camp, Bull, No 30, 1978). Yes, the landlords resented our searching questions our meeting the landless and bonded labour, our interest in lathyrism, and sent the police after us. 'They knew what will be revealed, like the statement of a bold labourer, "we are being slow - poisoned by the landlords so that we should always remain weak and be dominated" (Barseto, loc. cit). Compare Joshi's reference to the landlord's “confession" that they do not give wheat and rice as wages, lest the labour become strong and disobedient. Twenty years after the ICMR survey, Dr. C. Gopalan, the eminent nutritionist revisited Rewa He was told that the labour was not anymore getting Khesari dal as wages (NFI Bull. April 1982). This is

not true. We saw with our own eyes, as did Joshi now, that they continued to receive birri (a mixture of wheat, barley, Bengal gram and khesari). Dr. Gopalan believes that the decline in the practice is due to the fact that khesari is being “exported” to other states for adulteration of arhar and Channa dal. This we too learnt in 1978, both from the landlords and the labourers. Perhaps, its all for the good. Arhar and bengal gram today are only in the reach of the upper classes. Let us realise we are being' slow poisoned by our own" class. "Maybe the powers that be will sit up and notice. Dr. Gopalan says, "Nutrition scientists in their idealism often plead for a nutritional orientation………..the reality seems to be that commercial considerations and the play of market forces will determine ... agricultural development" (NFI Bull. April 1982). Did I not say that solutions offered by scientists failed to take socioeconomic realities into consideration (MFC Bull. 24)? I am glad Dr. Gopalan too now sees this. But, why has he left the play of the power structure? That which keeps the poor not only poor, but weak and oppressed? True, khesari is being exported. But the distribution of birri continues, as before. In this land of plenty, none shall be denied. Let me not be accused of evading Beena's and Chandrasekhar's question. What will the MFC do about it? MFC is a small organisation. Its voice is weak - the accumulation of whispers from different comers of India. We need a broad and large base, we need money and people, particularly people if each Bulletin reader can enrole more subscribers, if each member can bring in one more member then in the future may be we can link hands with like - minded groups all over the country - so that the whisper becomes a roar. Till then, our strongest voice is only the Bulletin. Please see that its vocal cords do not get paralysed

Kamala Jaya Rao

Editorial Committee: Anant Phadke Christa Manjrekar Ravi Narayan Ulhas Jajoo Kamala Jayarao; EDITOR


