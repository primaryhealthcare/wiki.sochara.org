---
title: "Statistical Malpractice In Drug Promotion: A Case-Study From Brazil"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Statistical Malpractice In Drug Promotion: A Case-Study From Brazil from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC092.pdf](http://www.mfcindia.org/mfcpdfs/MFC092.pdf)*

Title: Statistical Malpractice In Drug Promotion: A Case-Study From Brazil

Authors: Victora Cesar G

medico friend circle bulletin

AUGUST, 1983 STATISTICAL MALPRACTICE IN DRUG PROMOTION:

A CASE-STUDY FROM BRAZIL Cesar G. Victora INTRODUCTION Drug corporations have been accused of providing Latin American physicians with misleading information about their products by exaggerating their indications and minimising their hazards [1]. Such practices have also been reported for several other developed and peripheral countries [2-6]. The purpose of the present paper is to show how faulty, misleading statistics have also been employed in order to influence the prescribing habits of Brazilian doctors. During a 6-month period in 1979, all advertisements delivered to 4 Internal Medicine specialists in a private group practice in the city of Pelotas, Brazil were examined. These totaled over 350 materials ranging from full reports of clinical trials (always accompanied by a promotional leaflet) to a unsubstantiated claim on the properties of particular products with any references. The very diverse nature of these promotional materials makes it possible to establish specific figures on the prevalence’s of the different types of fallacies, since the majority of advertisements avoided details of sampling, diagnostic criteria, measurements of outcome or statistical analysis of the evidence being employed. The examples below, however, include only those fallacies which occurred 3 or more times in the materials surveyed. COMMON TYPES OF FALLACIES

Biased sampling The antibiotic phosphomycin is advertised as being -1000 effective in chronic urinary tract infections. The small print of the paper supplied with the advertisement, however, informs that this assertion was based on

a sample of eight patients with chronic U.T.I. obtained after excluding "those whose urine contained phosphomycin-resistant bacteria". The 1000 figure, after all, is not so impressive.

Inexistence of control groups This was the commonest fallacy in the survey. For example, diphenidol (an antiemetic) is quoted to be 960 effective in the control of vomiting in children. As then is no information on the length of observation of each child, nor on the existence of a control group, the figure being quoted is meaningless.

Small experimental groups The advertisement for a cold remedy reproduces the results of a trial in which 1000 of the patients receiving the preparation showed improvement of symptoms. The small print reveals that the treatment group included only five patients. It is understandable that clinical trial involving rare diseases would not be able to include many patients, but it is impossible to accept such results when the disorder being studied is the common cold.

Non-significant differences A sulfamethoxazole-trimethoprim combination is advertised as being superior to the tetracyclines for the treatment of acute exacerbations of chronic bronchitis. The statement, however, is not supported by the evidence supplied by the manufacturer, since the differences (Table I) do not reach statistical significance.

Fallacious comparisons Comparisons of efficacy. Mefenamic acid is compared to acetaminophen and codeine

in the treatment of "osteoarthritis, rheumatoid arthritis, intervertebral disk herniation and nonspecific rheumatism". These comparisons, however, make little sense, as the problems involved are relieved by the anti-inflammatory, as well as by the analgesic properties of mefenamic acid, whereas the two other drugs possess only the latter effects (7). The advertised drug should have been compared to others with both properties. Such as aspirin, ibuprofen, oxyphenbutazone, etc. Comparisons of adverse reactions. An advertising campaign for acetaminophen included the distribution of two leaflets entitled "The stomach and aspirin" and "Why risk?" The first makes no reference to the product being promoted, but includes citations from 27 articles on the adverse gastric effects of aspirin, headed by the assertion that "its risks often outweigh its benefits". The second advertisement, which contains information on acetaminophen, shows an endoscopic photograph of the stomach of "a 24year-old woman who

erythromycin in a table listing the plasma concentrations of each antibiotic necessary for killing different species of bacteria. Such a comparison is not valid, however, since the two drugs are pharmacologically distinct, and the fact that a lesser quantity of erythromycin is not per se an indication of any superiority. A better indicator of a drug's usefulness is the therapeutic ratio, that is, the balance between its efficacy and undesirable effects [9].

Table 1 Comparison of the efficacies of sulfamethoxazole-trimethoprim (SMX- TMP) and" tetracyclines in the treatment of 'acute exacerbations

of

chronic

bronchitis',

as

it

appeared

in

a

promotional leaflet (significance levels not quoted in the original)

Trial

%

No. patients

p*

Cures in group SMX- TMP x tetracycline

76

98

68

96

SMX- TMP x dimethylchlor-

81

27

tetracycline

67

24

0.30 0.37

* By the X test with one degree of freedom in each trial.

had ingested nine grams of aspirin in a 12-hour period, after intake of alcoholic beverages". Since the dosage is considerably larger than the maximum recommended, and alcohol is also a gastric irritant, it should be expected that inflammation of the gastric mucosa would be observed. Both advertisements omit the fact that although it does not seem to cause significant gastric side-effects, acetaminophen is on the whole considered to have more serious and irreversible toxic effects than aspirin, and that it should be reserved to those patients who cannot tolerate the former [8.9]*. Comparisons of potency. The publicity for a brand of amoxycillin compares this drug to

* The lesser toxicity of aspirin relative to that ·of paracetamol has been recently questioned. The present paper, however, refers to promotional materials distributed in 1979, when the prevailing opinion among pharmacological authors was than aspirin was a safer drug [7-9].

Fig.1 Efficacy of miconazole and two other drugs in topical fun9al Infections.

Misleading graphs This was the second commonest fallacy identified in the materials surveyed. An advertisement for miconazole, for example, compares the efficacy of this agent with that of two competing drugs - c1otrimazole and haloprogin - by depicting these as three circles (Fig. 1). The first drug, c1otrimazole, quoted to be effective in 59-70% of topical fungal infections, is represented by a circle with an area of about 14 cm2. The second, haloprogin, which is 68-92% effective, is shown as a 41 cm2 circle, whereas the advertised drug miconazole, being 75-100% effective, is depicted as a 133 cm2 circle. By looking at the areas of the circles, it appears that the efficacy of the product being advertised is 10 times larger than that of the first, and over 3 times larger than that of the second, whereas the actual ratios should be approximately 1.4 and 1.1 respectively.

DISCUSSION The examples described above are not intended to represent, or to quantitative, the whole spectrum of faulty medical advertising in the country. They should rather be seen as instances in which faulty methodology has been applied with commercial purposes. It is hoped that the documentation of these errors will lead physicians to analyze with greater caution the information which is supplied to them by drug corporations. This

is particularly relevant for Third World countries, where the actions of these companies seem to suffer less governmental restrictions than in the developed world. It might be argued that such methodological fallacies are the result of incompetence, rather than ill intent. Actually, as Huff has noted, "as long as the errors remain one sided, it is not easy to attribute them to bungling or accident"· [10]. In the promotional materials surveyed, not a single example was found of a fallacy which would make the advertised product seem worse. It should also be stressed that many of the advertisements provided information which was apparently of good scientific quality. The proportion of 'sound' advertisements, again, is hard to quantify since so many of the pieces examined did not include enough detail for a judgement to be reached. The mere existence of the errors described, however, should be sufficient to arise concern about the subject of medical advertising. Unfortunately, many doctors seem to rely on the drug industry as their main source of information on new products. This is partly due to inadequate teaching of medical statistics and therapeutics in medical schools as well as to the lack of continuing education, but the structure of health care is also to bear part of the blame. Most physicians hold several jobs with limited time being left for studying, competition is often fierce and the promotion of medicines is intense and sophisticated. The possible solutions for these problems are therefore not simple and certainly involve many aspects of the organization of the medical sector in the country. Some useful measures, however, could be taken without delay. These include: (a) Better teaching of medical statistics at the schools of medicine. This discipline is usually taught in the pre-clinical years, and the study of clinical trials is often left out. Scheduling this subject in part to one of the later years of the medical course may make it more relevant to the main use most future physicians will make of it - which is, rather than getting involved in doing research themselves, evaluating the work of other researchers with a view to applying it to the care of their own patients.

a medical school in the country was incorporated into the teaching of the specialties during the seventies. In many places, the overall result, rather than being better integrated teaching was that the subject lost the importance it once had. This is strongly felt by medical students, who have given a high priority to the re-establishment of therapeutics as a separate discipline in several lists of demands of their unions. (c) Continuing education of physicians. Continuing education must overcome many difficulties, especially in a large country such as Brazil to be effective. There are some simple measures, however, which may have a beneficial effect. One of such is the distribution of independent, non-profit publications such as the Medical Letter on Drugs and Therapeutics, or the Drug and Therapeutics Bulletin. These publications besides reviewing specific products may help to create a healthy skepticism among doctors regarding the claims of manufacturers. The establishment of ethical codes to be followed by the industry in their advertisements to the medical profession is a controversial subject, of very difficult enforcement. This matter certainly deserves detailed examination by medical associations and unions and by the Ethics Committees of the Regional Medical Councils. With very few exceptions, these do not seem to have dedicated enough attention to the subject up to the present. Perhaps one of the first measures to be taken in this field would be a critical triage of the advertisements carried out in the medical associations1 own journals which not infrequently employ the faulty techniques described above. The overall priorities of the drug industry in the Third World also deserve critical examination by professionals and layman alike and the educational measures cited above constitute only a partial solution. They represent, however, steps which could be taken on the short run. And, considering the situation described above, there is an urgent need for action, As long as some manufacturers continue to employ biased advertising practices in order to increase their sales, the lives and well-being of patients must have been at some risk.

References 1. 2.

(b) Better teaching of therapeutics at the schools of medicine. This subject which was taught by itself some 10 years ago in most

Silverman M. The Drugging of the Americas Univ. of California Press, Berkeley, 1976. Muller C. The overmedicated society: forces in the marketplace for medical care. Science 176, 488, 1972.

3.

4.

5. 6.

Waldron I. Increased Prescription 01 Valium and Librium: an example of the influence 01 economic and social factors in the practice of Medicine Int. J. Hlth sert. 7, 37, 1977. Dunne M., Herxheimer A., Newman M. and Ridley H. Indications and warnings about chloramphenicol. Lancet ii. 781, 1913. Yudkin J. S. Provision of medicines in a developing country. Lancet I. 810, 1978. Ledogar J. R. Hungry for Profits. IDCC/North America.

New York, 1975. Goodman L. S. and Gilman A. (eds.) The Pharmacological Basis of Therapeutics. MacMillan. New York. 1980. 8. Acetaminophen. Med. lett. 18, 7J, 1976. 9. Modell W. (Ed.) Drugs of Choice 1978-1979. Mosby. St. Louis. 1978. 10. Huff D. How to lie with statistics. Penguin. Middle sex. 1973. Source: Social Science'" Medicine 16:707, 1982. 7.

ANTIBIOTIC THERAPY [Recently the Medical clinics of North America published a symposium on Antibiotics. Since many of the infectious conditions seen in our Country are rare in the U.S. and vice versa, we have taken some aspects which may be useful to us and presenting them here, as a single write-up] The cornerstone of rational management of the patient with an infection remains the isolation and identification of the infecting microbial pathogen and determination of its susceptibility to antimicrobial agents. However, because of the lag time between recognition or suspicion of infection and the availability of susceptibility data, initial therapy must often be based upon presumptive evidence derived from the initial evaluation of the patient. In choosing the most appropriate antibiotic for a particular infection, one of the first decisions that must be made is whether a bacteriostatic agent will suffice or whether bactericidal therapy is required for most uncomplicated infections, a bacteriostatic agent will be adequate. Indications for combination antimicrobial therapy continue to evolve as newer, broader spectrum antibiotics are developed for clinical use. Situations where the combination of two or more agents has been shown to the beneficial are (1) mixed bacterial infections where organisms are not susceptible to a common agent (2) to overcome bacterial tolerance, (3) to prevent drug resistance, and (4) to reduce drug toxicity. . Chloramphenicol This has been the subject of severe abuse, resulting in numerous unnecessary cases of a plastic anaemia as well as escalating rates of resistance. In view of the potential toxicity and restricted spectrum, it is generally recommended for serious infections in which location of infection or susceptibility of pathogen indicates limited antimicrobial option. The infections which satisfy these criteria the

best are (1) S. typhi and other saln onellosis (except S. getis which should not be treated with antimicrobials) (2) CNS infections because of excellent blood-brain barrier penetration and good activity against many common CNS pathogens. Aplastic anaemia has been observed even in patients who used chloramphenicol ophthalmic ointment or eye drops, so that other antibiotics are generally preferred for superficial eye infections. The combination of Trimethoprimsulphamethoxazole (T MP-S MX) is effective against a large spectrum of micro-organisms. It is marketed in a ratio of 1:5. Both are folate metabolism inhibitors. Each drug is bacteriostatic when used alone but the combination has bactericidal action. Even when organisms are resistant to S MX, they may still be sensitive to the combination. 1.

It is currently the preferred therapy for shigellosis. In many parts of the world, shigellas have become resistant to tetracycline and ampicillin, but are still sensitive to T MP-S MX.

2.

It is an excellent back-up drug in Salmonellosis. Currently chloramphenicol is the drug of choice for serious typhoid fevers whereas ampicillin can be used against less serious species. T MP-SMX is an excellent alternative to both these.

3.

It is ideal for chronic and recurrent upper respiratory tract infections. FDA (USA) does not recommend its use in initial acute upper respiratory tract infections, because less expensive single agents are equally effective.

4.

It is extremely effective in acute otitis media due to H. influenzae.

Urinary Infections After treating urinary infections with appropriate antibiotic, the frequency of recurrent symptomatic infections can be reduced only by long term prophylaxis (months to years). Infections recur soon after prophylaxis is stopped. Nitrofurantoin appears to be a versatile antiseptic because it is effective against upper urinary tract infections, recurrent bacteriuria and as a long term suppressive agent in children and pregnant patients. Methanamine is also effective in females with uncomplicated recurrent bacteria including those with multiple resistant pathogens as well as a prophylactic agent in males with recurrent infections. Methanamine per se is not bactericidal. Its mode of killing microorganisms is the result of its hydrolysis to ammonia and formaldehyde. Formaldehyde is only liberated at acidic pH and once generated, is bactericidal at any pH. Fortunately normal urine is sufficiently acidic, to generate free formaldehyde from methanamine. It was hoped that weak acids such as mandelic acid, hippuric acid or ascorbic acid would further lower pl-l. However, there is little evidence that methanamine combined with these acids (mandelamine) confer any therapeutic or pharmacologic advantage over methanamine base alone. These urinary antiseptics offer increasing financial advantages over TMP-SMX.

Tetracyclines: These have an usually broad spectrum of antibiotic activity. They are generally well tolerated, have few serious side-effects and are one of the most commonly prescribed antibiotics in the world. . Oxytetracycline and chlortetracycline are short-acting. Doxycycline and minocycline are long-acting. All tetracyclines are bacteriostatic. There has been much discussion whether bactericidals are superior to bacteriostatic antibiotics. This distinction is an in vitro rather than an in vivo difference. Bacteriostatic antibiotics act in a "cidal" fashion when present in high concentrations and conversely, bactericidal

antibiotics act in a "static" fashion when present in low concentrations. Furthermore, bactericidal agents have never been shown to be superior to bacteriostatic agents in the therapy of infectious disease processes. Except where granulocyte count is less than 500 cmm. and in bacterial endocarditis, whether an antibiotic is bacteriostatic or bactericidal should not be a factor in antibiotic selection. Bacterial resistance to tetracyclines is usually mediated by R-factor. Clinically, resistance has been a problem primarily with conventional tetracyclines and much less with doxy and minocyclines. All tetracyclines should be avoided in children less than 8 years of age. They may cause temporary inhibition of bone growth.

Erythromycin This has a broad spectrum of antimicrobial activity and is one of the safest antimicrobial agents in clinical use. Resistance among pathogens that were previously highly susceptible is being reported. It is a drug of choice for a number of infections and is an alternative to penicillin in streptococcal infections, syphilis and in rheumatic fever prophylaxis. A l0-day course of erythromycin is recommended by the American Academy of Pediatrics as the treatment of choice for pertussis. It clears the pathogens from" the nasopharynx effectively. Erythromycin is used extensively for treatment of diphtheria and diphtheria carriers. In acute infections, however, antitoxin remains the primary therapeutic modality. Erythromycin is highly effective in eradicating (C.diphtheria from the nasopharynx within 48 to 72 hours. A 10-day course is recommended because of a high relapse rate after a briefer course. Acute otitis media caused by streptococci may be treated effectively by oral erythromycin. Combinations of erythromycin with sulphonamides have been shown to be comparable to ampicillin, in treating infections, regardless of causative organism. Erythromycin should not be used alone in serious staphylococcal infections.

SINGLE DOSE THERAPY FOR ACUTE INFECTIONS Abhay Bang At the M.F.C. annual meet at Tara, a discussion had cropped up on the possibility of a single large dose of antibiotic for treating acute respiratory infections. The discussion

remained inconclusive in the absence of definite information. I recently came across a small review on

single dose therapy in the management of urinary tract infection' (I. N. Slotki, 'The medical Annual' 1980-81 eds. Sir R. B. Scott. Sir James Fraser) I am quoting it here to advance this discussion in M.F.C. further need not reemphasize the advantages of single dose therapy in the treatment of acute sections specially while working in the al areas. The review says – In the earliest report on single dose therapy of acute UTI Gruneberg and Brumfitt, 1967) found that a single 2 g. dose of the drug-acting sulphonamides; sulphomethoxine, is as effective as a 7-days course of ampicillin 500 mg 8 hourly in-50 non-pregnant women. Williams and Smith (1970) achieved a re rate of 77 per cent using a single dose combination I g of streptomycin and 2 g of sulhametapyrozone in pregnancy bacteriuria, sulhametapyrozone was shown to be as effective as a 7 day course of ampicillin 10 mg. t.d.s. (Slade and Crowther, 1972). Sterilization of urine by an antibiotic to which the organism is sensitive can be achieved within 24 hours (Fairley et al., 1978). The same study also showed that, where organisms persisted at 24 hours, they' were present in all daily urine collections over the next week. It would seem likely, therefore, that en a single dose of an antimicrobial agent would eradicate uncomplicated UTI. In the last 4 years the efficacy of single-dose therapy of UTI has been established an both children (Kallenius and Winberg, 1979) and adults' (Bailey and' Abbott, 1977, 1978). Socialization studies of UTI using the 'bladder washout' procedure (Ronald et al., 1976) and the 'antibody-coated bacteria' assay (Fang et. al., 1978, Leading Article, 1979) have shown that kanamycin sulphate (500 mg i.m.) amoxycillin (3g) and co-trimaxazole orally each in single does eradicated lower tract infections '" almost all cases. A high proportion of upper tract infections as indicated by these techniques either persistor rapidly relapse. Bailey and Abbott (1977, 1978) found that ingle doses of either co-trimaxazole or amoxycillin 3 g were curative in 85-90 per cent of patients' with normal intravenous programs but in 50 per cent or less of those with abnormal radiology. Fairley et al. (1978) confirmed the different responses' between, patients with and without radiological abnormalities, of the urinary tract and proposed that allure of singledose therapy to eradicate UTI is an indication for further investigation.

In conclusion, single-dose therapy of lower UTI in the absence of radiological abnormalities is highly effective. Moreover, it is less expensive, easy to administer, non-toxic and has not been shown to be associated with more frequent relapses or re-infections than conventional longer courses of antibiotics (Kallenius and Winberg, 1979).

REFERENCE 1.

Bailey R. R. and Abbott G. (1977). Treatment 'of urinary tract Infection with a single dose of amoxycillin. Nephron 18, 316.

2.

Bailey R. R. and Abbott G. (1978). Treatment of urinary tract Infection with a single dose of trimethoprim-sulfamethoxazole. Can. Med. Assoc. J. 118, 551

3.

Fairley K. r., Whitworth J. A., Kincaid-Smith P • et al. (1978). Single dose therapy of urinary tract Infection. Med. J. Aust. 2, 75.

4.

Fang L.S.T., Tolkoff-Rubin N. E. and Rubin R. H. (1978). Efficacy of single-dose and conventional amoxycillin therapy in urinary tract Infection localize by the antibody-coated bacteria technique N. Engl. J. Med. 198, 41J.

5.

Gruneberg R. N. and Brumfitt W. (1967). Single dose treatment of acute urinary tract Infection I a controlled trial Br. Med. J. 3, 649.

6.

Kallenium G. and Winberg J. (1979). Urinary tract infection treated with single-dose of short-acting sulphonamides. Br. Med. J. 1, 1175.

7.

Leading Article (1979) Single-dose treatment of urinary tract infection J. Am. Med. Assoc. 241, 1226.

8.

Ronald A. R., Boutros P. and Mourtada H. (1976). Bacteriuria localization and response to single-dose therapy in women J. Am. Med. Assoc. 235, 1854.

9.

Slade N. and Crowther S. T. (1972). Multicentric survey of urinary tract Infections In general practices Clinical trial of single-dose treatment with sulfametopyrazine. Br, U. Ural. 44, 105.

10. Williams J. D. and Smith E. K. (1970). Single-dose therapy with streptomycin and sulfametopyrazine for bacteriuria during pregnancy. Br. Med. J. 4, 651.

WORLD HEALTH AUTHORITIES CONDEMN INDUSTRY PRACTICES May 1983 marked the second anniversary of the adoption by the World Health Assembly (WHA) of the International Code of Marketing of Breast milk Substitutes. As the Assembly met again in Geneva last month, they reviewed progress made in implementing the Code at the national level and also commented: on current practices of the infant milk industry. Although no changes were made in the text or legal status of the International Code at this WHA, many delegates were out-spoken contd. on page 7……

in their condemnation of recent practices of the industry and called for more vigilance on the part of WHO and governments. A few examples follow:

LITTER TO EDITOR, Dear Friend, I have gone through the article "Rational therapeutics, selection of appropriate drug" f U.N. Jajoo in the June 1983 Issue of M.F .C. bulletin. It has been mentioned in the article lat Aspirin remains the drug of choice as an analgesic and anti-inflammatory agent in regnant women. This doesn't appear rational since Aspirin is contraindicated in pregnancy .because of its (?) teratogenic effect and toxic effects. Paracetamol (Acetaminophen) is safer in pregnancy. I wish to quote following sentences from well known authors in this respect. l)

"Aspirin and other salicylates, when given in pregnancy are associated with occurrence of achondroplasia, hydrocephalus, congenital heart disease, congenital dislocation of the hip and talipes" (FORF AR - Prescriber’s Journal (l973) 13, 130).

2.

"There is no evidence that therapeutic doses of salicylates cause fetal damage in human beings, although babies born to women who ingest salicylates chronically may have significantly reduced weights at birth. In addition there is a definite increase in perinatal mortality, anaemia, ante pattern and post partum haemorrhage, prolonged gestation and complicated deliveries". (GOODMAN GILMAN'S - The Pharmacological basis of therapeutics, 6th Edition (1980).

3. "Salicylates are used in a free and uncontrolled manner throughout pregnancy. Impairment of platelet functions and haemostatic functions can occur in the new born foetus born to these women. It has been experimentally proved that Aspirin can produce congenital defects. There is no report to indicate teratogenic effect of acetaminophen. Acetaminophen appears to be a safer drug in pregnancy". (ALFRED SCOTT (l98l): Archives of Internal Medicine Vol. 141, No.3, 358). You will agree with me if I say that congenital malformations due to drugs should be prevented at any cost. Please think twice before prescribing Aspirin in pregnancy. Or. P. S. Patki Pune

"A more dangerous development which we are witnessing today is the attraction of mothers, even in the rural areas, to artificial feeding, not only because it is seen to be convenient, but because it has become a sort of status symbol. These dangerous trends must be counteracted. The activities of promoters of artificial infant feeding must be curtailed and closely watched. It is with considerable regret that we have received information that some major industrial establishments are beginning to develop cold feet and may not after all be that keen now (to implement the Code.)" Professor U. Shehu, National WHO Programme Coordinator, Nigeria "It is distressing that both local and foreign manufacturers of infant milk still engage in marketing practices which take advantage of the loopholes of the International Code. We hope that WHO will continue to review progress regarding national efforts to implement the Code, and in particular, regarding industry compliance with the spirit and letter of the Code. Violations of the Code cannot be tolerated .when it is infant health that suffers as a result". Dr. S. S. Sidhu, Secretary, Ministry of Health and Family Welfare, India. "Some companies involved in the manufacturing of breastmilk substitutes have produced parallel codes which they claim are in support of the International Code. It is important that WHO should be vigilant, for such action in future might end up by undermining the Code." Dr. Z. M. Diamini, Director of Health Services, Swaziland

"It was hoped that manufacturers would... cooperate in implementation of the Code by, as much as possible, trying to modify their practices, sales promotion, labels and so on… There is really strong evidence that the practices have not changed - the same sales promotion, tins are labeled exactly the same - and just recently we have also seen the distribution of child growth charts by one of the manufacturing firms to be used in the health services." Dr. J. T. Kakitahi, Director of Nutrition Services, Makerere Medical School, Uganda [From IBFAN News, June 1983]

CENTENARY OF TUBERCULOSIS BACILLUS Tuberculosis once a killer disease is completely curable today. The credit goes to 'Robert Cock, who hundred' years back discovered mycobacterium. Last year all over the world the centenary of the TB vaccine was celebrated. Included in this celebration was pipariya, a small town of Hoshangabad District Madhya Pradesh, A T8 Camp was being organised by the local unit of Indian Medical Association in collaboration with Pipariya Government Hospital. The Seven day camp has raised many questions concerning clinical and social context of this disease that is rampant amongst people in this country. The organisers of the camp consisted of six practising medicos from the town and some social workers. Finances for the camp came through donations in terms of cash and kind both; the latter included X-ray plates,' medicines and injections. In all 428 persons from the low income group were identified and invited to the camp for systematic diagnosis, through pathological tests and X-ray, resulting in detection of T8 in 148 persons. Of these 148 persons, 52 turned out to be new cases, the rest 96 had some history of treatment. In all 3.5 cases were detected who didn't have TB at all but were being treated for T8. A shocking revelation indeed: Many of them have received permanent damage through intake of large dosage of streptomycin, resulting in 60 percent loss of' hearing power. The irony is that these people were being treated by no quacks but by qualified (MBBS) doctors. These 35 patients were actually found to be suffering from bronchial asthma or other lung and heart diseases. The doctors were strongly of the opinion that bronchial asthma or other lung infections can easily be differentiated from T8 through simple clinical tests. This raised a serious question on the quality of our trained medical personnel in general and their professional ethics in particular. A continuous treatment for nearly 18 months ensures complete cure of this disease. And it is for this reason alone that TB is popularly known as King of all diseases. TB is also fairly well known to have a high incidence rate among the weaker sections of the population. For the poor people TB

simply amounts to an additional cause of indebtedness; and for the medical practitioner it is merely a boon (in disguise if you wish), A long course of expensive treatment leads to total pauperisation and ultimately to death. Since TB is infectious and germs easily multiply in unhygienic, dark and suffocated dwelling places, it easily embraces the whole family. This eventually strengthens the old belief that TB is a killer disease and is hereditary as well. This is a major reason for the failure of National TB eradication programme. A poor patient has the financial limitation to discontinue treatment the moment he feels slightly better after a few days of treatment, only to come back to the doctor after several months with added complications. A discontinuity in treatment for a week means extension of the 18 month period by 3 weeks. Doctors have preferred to keep quiet since this laxity ultimately benefits them. A full course of treatment ensures 98 percent cure, whereas in our country this rate, on an average, is merely forty percent.' Not only had this, in this camp some persons were identified who have had treatment running to more than 15 years. TB is a major concerned so far as people’s health is concerned. Governments program is in a sordid state, limited only to identification drive with minimal concern for curability. Medical education in the country has failed to give cognizance to this national priority in their curriculum or specialisation. Even the Govt. Health structure had laid more weightage on Ear, nose and throat diseases than on TB that is believed to plague the larger section of the toiling masses. This TB camp has been a tiny oasis in a large desert. Unless government itself shows concern for this and acts rationally nothing significant can be expected of these voluntary charitable efforts in an ocean of misplanning and fixing up of wrong priorities on the part of the Government; incompetence and narrow self interests on the part of the trained medical personnel of the country; and lack of education, indebtedness and peculiar social psychology that is a characteristic feature of the toiling masses of this country.

Editorial Committee: Anant Phadke Padma Prakash Ravi Narayan Shirish Datar Ulhas Jajoo Kamala Jayarao - EDITOR

Views and opinions expressed in the bulletin are those or the authors and not necessarily or the organisation.


