---
title: "Kahani - Ek Nurse Ki, Aur Stree Ki Bhi!"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Kahani - Ek Nurse Ki, Aur Stree Ki Bhi! from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC050.pdf](http://www.mfcindia.org/mfcpdfs/MFC050.pdf)*

Title: Kahani - Ek Nurse Ki, Aur Stree Ki Bhi!

Authors: Bang Abhay

medico friend circle bulletin FEBRUARY 1980

WHO IS MALNOURISHED: MOTHER OR THE WOMAN? Kamala Jayarao* “If you do not raise the women, .......................... ·····, don't think there is any other way to rise." — Swami Vivekananda In the parlance of the nutrition scientists pregnant and lactating women, along with children under five, are referred to as the vulnerable groups. This means that they are most susceptible to the ill effects of malnutrition. This is because the nutrient requirements go up tremendously during pregnancy and lactation; in fact, more so in lactation, a fact not always well appreciated by clinicians. Between 15 and 45 years of age, that is, in the 30 year span of reproductive life, an average Indian woman becomes pregnant about 8 times. She may ultimately be left with only 3·5 children because the abortion and stillbirth rate is about 15% (1), infant mortality rate is 122 and death rate of children under five is 18 per 1000. Each live born infant is breast fed generally up to 2-3 years, and the milk output is quite substantial in the first 18·24 months. Thus, of the total 360 months of reproductive life, 200 months or 50.60% of the time is spent in pregnancy and lactation (Table 1). Of this, some 140 months are completed before the woman reaches 35 years of age (2). Of the total female population in India, about 50% amounting to nearly 140 million are in this age groups (3). Table 1 Time spent by an average Indian woman in Pregnancy and Lactation Months Total reproductive life span (I5 - 45 years of age) Total duration of Pregnancies 7 live-births 1 abortion Total average duration of lactation 1 death in infancy 6 survivals beyond 1 year Total time spent in pregnancy and lactation

* National Institute of Nutrition. Hyderabad.

360

63 5 6 120

194

Most of these women are underweight, and their food intake even in the non - pregnant state is extremely poor. Although the nutrient requirements go up in pregnancy and lactation; their food intake does not change from what it is in the non- pregnant state. They fail to gain adequate weight during pregnancy and give birth to low weight babies. The incidence of iron deficiency anaemia, vitamin A deficiency and signs of Vitamin B complex deficiency are very high among the pregnant women(4)'. The incidence increases with increasing parity (5). The high incidence of malnutrition, coupled with poor antenatal and obstetric care, results in an extremely high maternal mortality rate, which is around 370 (6). The figure reported from a rural referral centre is staggeringly high1800 (7). The maternal mortality rates in some countries are compared in Table 2.

Table 2 Maternal Mortality Rates in Some Countries

(Maternal deaths per 100,000 births) Sweden

U. K. U. S. A. Poland Japan India

7.7 10 7 12.0 14.3 27.6 370.0

Sources: References 3 and 6

The nutritional problems of Indian women have therefore been generally considered-to be the nutritional problems of pregnant and lactating mothers. Great emphasis is therefore being placed On feeding programmes for pregnant women, anaemia prophylaxis, family planning etc. Though in themselves these are no doubts, essential services, the attempt to implement these as nationwide public health programmes is, in my opinion, a shortsighted and narrow approach. The reason is, firstly that in such an approach the female is viewed only in the context of her motherhood and, therefore secondly, the problem is seen in isolation. The problem should be understood as fundamentally an offshoot of a deeper and more complex malady, namely the inferior status and expendable nature of the female in India society.

3. To understand this let us look at certain facets of female life in India.

That the low sex ratio is not due to maternal mortality alone is also evident from age-specific death rates. Although normally male births are-higher male deaths are also in excess, right from infancy. In India however we see that female death rates are higher. (Table 5 on page 3). At the beginning of this century despite a deplorably low level, life expectancy of the females was higher than of the males. In the' past 40 years, the males have overtaken the females (Table6). No specific cause could be found for the higher female death 8 rate ( ) and this may be attributed to the negligence and apathy towards the female child. Though protein calorie malnutrition was found to be higher in girls, more boys 9, 10 were brought to the hospital for treatment ( ). Following

The Sex Ration The sex ratio (ration of females to males) in human is biologically determined to be more than 1000. This is due to the higher life expectancy of the female. A look at Table 3 shows that the sex ratio in India is much lower then 1000 and in fact, the Indian subcontinent gets a very low ranking among various nations. The top ranking of the communist countries should not be interpreted to mean that female life is the best there. This is due to the peculiarity of their recent history. During World War II (1935-1945), many young men died resulting today in extremely, small numbers of men above 45 years; this is reflected in an abnormally high sex ratio in these age group, as seen in Table 4. Table 3 Sex Ratio in Different Countries U.S. S. R. East Germany Poland West Germany Hungary

U.S. A U. K. Czechoslovakia Switzerland Spain Brazil France Yugoslavia Zambia Rumania Japan Indonesia

1160 1150 1122 1096 1061 1054 1053 1053 1051 1050 1047 1044 1033 1033 1030 I 030 1030

Chile Sweden Denmark Thailand Canada Kenya Argentina Guatemala Australia Mexico Peru Uganda Bangladesh Iran .India Pakistan

the adoption of the small family norm, the number of female children has declined markedly; whereas there is a 53%

decrease in' the number of female children, the number of male children has decreased by only 30 % (11). Table 6

1026 1019 1015 1008 1002 997 997 992 991 988 986 983 962 937 925 886

Different Countries § Female Male U. S. A. 76.5 68.7 Japan 76.0 71.0 Sweden 76.0 70.3 U.S.S.R. 74.0 64.0 U.K. 73.8 67.8 India 48.8 50.1 Pakistan 48.8 53.7 (Including Bangladesh)

-

U. S.S. R. U. S.A. Japan U. K. India

Trend in India §§

Female 1911 23.3 1921 20.9 1941 31.4 1961 40.6 197.1 44.2 1976 48.8 (Projection)

Male 22.6 19.4 32.1 41.9 46.4 50.1

The Declining Trend More disconcerting than the low sex ratio is the declining trend in the ratio over the years (Table 7). Right from 1901, the ratio has been steadily declining. Whereas Punjab alone (known to practice deliberate female infanticide) had a ratio less than 900 and four states had ratios above 1000 in 1901, today Kerala alone has a ratio above 1000and three states have ratios less than 900. Since there is no reason to believe that female mortality has actually increased over the years (and thank God or men, for it), it can only be concluded that whatever development has taken place over the pas t five decades has favoured the males more. Even Kerala, the most progressive of all states with its high literacy rate and low infant mortality

Source: Reference No.3

Age (years) country

I

§Ref.3 §§ Ref. 6

To compensate for the low life expectancy of the males, normally more male infants are born, In the developed nations there are 40 more boys for every 1000 girls, in the first two decades of life. In India there are ~05 more boys. Thus, right from infancy the sex ratio is quite low in India (Table 4). The low sex ratio in India cannot therefore be attributed solely to maternal mortality. A sex ratio or 1000 is reached only after the seventh decade when the population is hardly 2% of the total. Table 4

Life Expectancy at Birth

Age-wise Sex Ratios in Different Countries

<4

5-9

10-14

15-19

20-34

35-44

45-54

55-59

965 957 956 943 ·905

962 972 952 948 903

964 961 956 948 903

960 974 958 955 931

1012 1015 990 973 934

1106 1053 995 973 916

1610 1069 1091 1020 882

1963 1156 1230 1113 892

Figures calculated from data in Reference No. 3

>70 2445 1560 1370 1 06 1000

Table 5

Age - Specific Death Rates (Sex-wise)

--

Age (years)

1

1-4

5·9

7.6 6.3 11.3 8.7 18.6 13.9 18.3 14.4 120.0 125.0 20.5 196.0 17.7

0.9 0.6 1.0 0.7 0.7 0.7 0.8 0.6 16,0

0.7 0.4 0.4 0.3 0.4 0.2

Country

---

M F M

USSR Japan

F U. K.

M

U.S.A.

M

F F M F M F

India Pakistan (including

167.0

10-14 1.4 0.6 0.3 0.2 0.3 0.3

15-19

20-24

25-29

30-34

2.1 0.9 0.9 0.3 0.9 0.4

3.2 1.1

4.0 1.4 1.0 0.6 0.9 0.5

5.2 1.9 1.3 0.8 1.1 0.6

1.1

0.6 1.0 0.4

0.4 0.3

0.8 0.6

5.8 7.7 3.8 1.9

17.0

3.0 2.7 1.7 2.7

2.1 4.2 2.2 2.6

5059

2.0 0.9 3.9

3.7 5.5 0.9 6.6

5.5 2.8 4.5

4.1 6.4 0.4 5.6

Bangladesh)

-Table 5 Continued Age (years) Country

35-39

40-44

45-49

60

USSR

M

6.6

8~4

12.4

22.8

39.4

F Japan

M

2.6 1.9 1.1 1.5 1.1

3.6 3.2 1.7 2.8 1.9

5.4 4.6 2.5 5.2 3.4

10.4 8.0 5.0 15.4 8.2

20.4 23.5 12.8 32.8 16.2

U. K. U.S. A. India Pakistan (including Bangladesh)

F M F M F M F M F

3.5 1.9 6.5 6.0 3.0 3.1

8.6 4.6 8.5 7.6 3.9 4.2

13.2 9.4 3.1 9.9

20.3 10.1 22.4 17.8 ll.2 10.3

30 12

Sources: Reference No.3 and 2

Trends in Sex Ratio hi India

Table 7 States

1901

1911

Bihar Tamil Nadu

1054 1044 1037 1004 990 985 983 978 954 945 937 905 848

1044 1042 10.56 1008 986 992 981 966 946 925 915 908 807

972

964

Orissa

Kerala Madhya Pradesh Andhra Pradesh Karnataka Maharashtra Gujarat Bengal Uttar Pradesh Rajasthan Punjab All India Reference No, 6 and 18

1921

1941

1016 1029 1086

1961

1971

974 993 969 950 944 905 909 896 A21

996 1012 1053 1027 970 980 960 950 941 852 907 906 850

994 992 1001 1022 953 981 959 936 940 878 909 908 854

955 979 989 1019 941 977 960 933 936 892 8S3 914 873

955

945

941

930

1011

70

80 81.0 60.7

82.5 56.5 98 58 44 22.5 71.0 66.5

133 102 149 101 95 60

39.5 49.5

rate, has shown a steady, though small decline in sex ratio. Marumakkathayam or the matrilineal pattern of inheritance was practised-by a sizeable section of the population of this state previously. This has been now giving way to the more widely prevalent patrilineal system of inheritance. Whether this could be the cause of the decline in sex ratio in a state which unlike others, registered an actual increase in the first half of the, century needs to be seriously examined. The Woman as a Worker The neglect of the female is to be seen in all aspects of life. The work participation ratio of females is only 13% 12 as against 52.5% for males ( ). Almost half of them are engaged in unskilled work as agricultural labourers (Table 8). They are employed for shorter periods (Table 9) and art; paid less (Table 10). In fact, they are treated on par with male children. The high agricultural wages in Punjab and Haryana are attributed to the increased agricultural activity while in Table 8 Pattern of Employment of Labour Force' Cultivators Sex

Male Female

Agricultural Others labourers per cent 32.6 26.0

21.0 46.0

46.4 28.0

Source: Reference No. 12

Table 9 Total days of wage employment in a year (agricultural labourers)

Agricultural work

Non-agricultural work

Total

217 149 207

25 11 17

242 160 224

Men Women Children

Source: Indian Labour Year Book, 1970

Table 10 Average daily earnings ofagriculturallaboure1"s (Rupees)

Punjab Haryana Kerala Uttar Pradesh Tamil Nadu Gujarat Bihar Maharashtra Andhra Pradesh Karnataka Madhya Pradesh Source : Ref. No. 13

Men

Women

Boys (below 15 years)

4.9 4.5

3.5 2.7 2.2 1.6 1.5 1.7 1.9 1.3 1.5 1.5 1.3

2.9 3.0 1.5 1.3 0.9

4.1

2.4 2.4 2.3 2.2 2.2 2.1 1.9 1.6

1.8

1.2 1.3 1.2

1.2

Kerala this is believed to be due to the organization of the 13 labour force ( ). It is, however, apparent that there is no such organisational support for the females. Traditionally, apart from agriculture, women found gainful employment through household industries. The decline of handicrafts and small-scale industries and the rapid development of the organised sector are pushing the' women 14 more towards casual, unskilled labour ( ). The rapid mechanization of agriculture and wider use of fertilizers [and HYV seeds are also working against female participation. An important, though not the sole, operative factor is the low literacy rate of women which is only 18.4% 12 compared to 89.5% among the males ( ). In rural areas the rate is only 13% and-in-at least 7 states it is less than 10% 15 ( ). These women are unable to participate in the new technologies being introduced and no concerted efforts' are made to impart to them any special training. Even those who stress on the importance of rural industries have failed to 16 highlight this aspect ( ). The plight of the urban female is no better. The increasing numbers of women in professional and white-collared jobs have hidden that in the masses women 17 are being displaced from employment ( ). Whatever benefits have accrued to women in the past three decades, have gone to those in the high socioeconomic groups with a good level of literacy. There are deliberate measures against employment of women in the organized industrial sector. They are employed either in unorganized sectors which brand themselves as small-scale industries or as 18 domestic labour ( ), and in either case the labour is bought very cheap.' Thus, the average Indian woman lacks a proper socio-economic status, arid she and her life are not of much social consequence. The nutritional status of any population group is a good reflection of its socio-economic status. Viewed with this perspective, the nutritional problems of Indian women assume an entirely different connotation. The problems do not arise merely out of poverty and ignorance; it has to be viewed in its totality. Neither nutritionists who formulate and recommend the programmes nor the administrators who are responsible for their implementation, appear to view the female first as a woman and an individual, but seem to view her only in her role as a mother. Implied there in is the view that the woman is important only because she is the bearer and nurturer of children. There is no gainsaying the fact that the nutritional status and health of Indian women needs improvement. To think, however, that this can be done through welfare programmes such as food distribution and anaemia prophylaxis, is a serious misconception. Equally misconceived is the notion that the programmes will 'succeed' through nutrition education, a wasteful exercise at this juncture. The wisdom of this policy where in the woman is viewed mainly as a mother, needs to be questioned and reviewed.

5 The problem must be seen in its entirety. It must be appreciated that motherhood, howsoever significant, is only one aspect of female life. All along we have devised welfare programmes for women and directed all developmental activity towards men. This needs to be changed if the 'nation' has' to progress. Until and unless deliberate efforts are made to bring women into the mainstream of developmental activity, and to enhance their economic and social utility, all welfare programmes formulated for women, in India are bound to be disastrous failures and wasteful expenditures.

REFERENCES 1. C. Gopalan. Bull. Wld. Hlth. Org., 26: 203, 1962. 2. Measures of Fertility and Mortality in '. India. Vital Statistics Division; Registrar General, New Delhi, 1972. SRS Analytical Series No. 2. 4. U. N. Demographic Yearbook, 1976, New York. 5. C. Gopalan, J. Amer, Diet. Assn., 39:129, 1961. 6. K. V; Rao in Proc.vof tht: Seminar. On Population Problems. World Population Year" 1974. Dept. of Health" Govt. Kerala, , 7. Pocket Book of Health Statistics. Central Bureau of Health Intelligence, DGHS, GOI, New Delhi. 1975. 7. S. R. Santpur -and S. V: Savaikar in Proc of the Second International Seminar on Maternal and Perinatal Mortality. (G. L. Jhaveri and R. D. Pandit, Eds.) Fed. Obstet, Gyn. Soc. India, 1975. 8. J.B. Wyon and J.E. Gordon. The Khanna Study. Harvard Univ. Press Cambridge, USA. 1971. 9. C. Gopalan and A.N. Naidu, Lancet 2:1077, 1972. 10. P.L. Graves, Amer. J. cue. Nutr, 29:305. 1976. 11. A. J. Singh' and S. Siddhu in Rural Labour in India (S.M. Pandey ed.), Shriram Centre for Industrial Relations and Human Resources, New Delhi, 1976. p. 41. 12. Census of India 1971. Series I. Provisional Population Totals. Registrar General & Census Commissioner, New Delhi. 13. S. M. Pandey in Rural Labour in India, Same as Ref. 11. 14. P. Kalhan in India since Independence (S. C. Dube, ed.) Vikas Pub. House. Ltd New Delhi, 1977. p. 215. 15. R.J. Joshi in Rural Labour in India (Same as Ref; 11), 16. Charan Singh. India's Economic Policy: The Gandhian Blueprint. Vikas Pub. House Ltd., New Delhi, 1971. 17. Critical Issues on the Status of Women. I.C.S.S.R" New Delhi, Publication No. 107, 1977. 18. P.B Desai. Size and Sex Composition' of Population in India. Asia Pub. House, Bombay, 1969.

•••

DEAR FRIEND I appreciate the earnest audit of the failures and successes of the MFC (“When the search began”- Nov-Dec. 1979. Those who have worked in villages would-agree with most of the observations. Some of the conclusions are striking to me: (i) "Our medical education in the hospital is inadequate to equip us with skills useful in the rural setting". Every one acquainted with the health problems of the country knows very well how inadequate our Hospitals and Medical Colleges ale to meet the need of the country-either urban or rural. This is a desperate question and no one knows what answer will be found and when. However, even under the' best circumstances-there can be no standard answer to the highly divergent and vastly numerous problems of our country. The best solution is what the MFC has done give up the armchair discussions, plunge into action and work out the solution based on hard facts obtained by experience. (ii) "Medical problems are not of priority to the people". Absolutely true. There are far more pressing and demanding problems to a common villager than the scabies of one of the boys or the .place for his defecation. I do' appreciate the efforts of MFC, but can a group of young doctors stand this challenge? Do they have the required knowledge and ability for this work? Will they-not be wasting either own skills in trying to do what is beyond their competence? The answer is to have a more composite group of voluntary workers. But a heterogenous group has its own problems of organization and working.' (iii) Even in health work there are areas of priority like communicable diseases as against chronic ailments. Attention to drinking water facilities, immunization are of utmost urgency. (iv) In my opinion, one of the biggest social evils coming to us from generations (particularly in India). Is the habit of preaching and the importance given to such preaching? Religious and political leaders have made-it an established technique. Health work is assessed on the number of pious talks on health and 'sanitation .irrespective of their feasibility and practicability without ever bothering as to how much is being followed'. I repeat that this is a "social evil" among the educated and enlightened people. Youngsters should break this practice. People who mean business should talk less but find practical solutions. I am happy to note, for example, that the MFC has tried to understand the reasons for open air defecation rather than give lantern lectures regarding the best village latrine which is only an object of curiosity to the town people who visit .exhibitions, (v) The idea of a self-supporting system of health care is extremely good. However, this is too' much of an ideal state. It may be impossible to reach, but our aim should be in that direction. K. V. Desikan Director, Central Jalma Institute For Leprosy, Agra

The report of 6th Annual meet of MFC, held at Jamkhed from 24th to 27th January will be published in the next issue of the Bulletin.

ABORTION : The Woman's Plight and Right

The heart of th6 matter seems, to be that whether abortions are legal or illegal they are a worldwide and growing problem. As the recent IPPF conference on abortion in Bellagio made clear, abortions are increasing particularly fast in developing countries with rapidly growing' urban populations, and practical strategies to deal with the situation are urgently needed. Everywhere there is confusion about the dangers involved in abortion, because the relative safety of the procedure in the first 12 weeks is frequently not recognised. And almost everywhere those with money and influence have access to safe terminators while the poor, the underprivileged and rural dwellers' do not. Even where the law is liberalised, it is frequently not fully or fairly put into practice. It is in dealing with these realities, through better access to contraception, through better sex education, and through the improvement of whatever services cart be legally given, that progress will be made. 'The Universal Practice: Abortion of an unwanted pregnancy is the oldest medical cure known to man. Although universally practiced, abortion was not universally accepted. Today, abortion continues to be practiced, whether accepted or forbidden, in all parts of the world and by women in all walks of life. Because some Women are deprived of first - rate medical care by law or by lack of services or, money, they continue to undergo abortion procedures which are not substantially different from those endured by their ancestors. About 30 countries have changed their formerly restrictive laws or policies to permit abortion on request or on a broad range of social .indications. Today, about two thirds of the people of the world, mostly in Asia, Europe and North America live under such laws or policies. In most of these countries,' the impetus leading to the relaxation of abortion law was rooted in the humanitarian principles that distinguish modern man from his forbears. 1) Illegal abortion is recognized as a threat to public health. 2) Social justice demands equal access to abortion for rich and poor alike. 3) Women's rights imply that control of her own body is the responsibility of the individual woman. The liberalization of abortion laws in recent years has been followed by a sharp increase in the number of legal abortions, while the number of deaths from illegal abortions has dropped markedly. The decline in abortion related mortality is due partly to the replacement of illegal procedures by legal ones and partly' to improved techniques for treating complications. When legal abortion is used as a backup measure if contraception fails the risk to life or health is very low.

1) Among young women under 30 years of age the total risk to life associated-with each of the, four major methods of fertility control (pill, IUD, diaphragm- or condom, and, first trimester abortion) used alone is about equal and is very low (1.2 per l00,00 women per year), significantly lower than the birth- related risk to life; without fertility control. 2) Beyond age 30 the .risk to life increases rapidly for pill users who smoke, until after age 401t is much higher than the risk experienced by women using neither contraception nor abortion, For all other methods, the risk remains constant or increases moderately, but remains far below the level of mortality associated with complications of pregnancy and child ~birth without fertility control. 3) At all ages, the lowest level of mortality, by far, is achieved by a combined regime, that ill, use of barrier methods "with recourse to early abortion in case of failure. In the United States,......... the risks were lowest when the abortion was performed within the first eight weeks of pregnancy and increased steadily as the pregnancy advanced. After 16 weeks of gestation, the risk associated with legal abortion exceeded that of childbirth. Repeat abortions continue to be a matter of concern, and it is a fact that the .number and percentage of repeat abortions are increasing each year. By itself, abortion is an inefficient method of birth control. Women who use no contraception will need .two or three abortions to replace one birth because of the shorter interval during which they are protected from another pregnancy. However, as pointed out by ‘Potts Dig gory and Peel in their recent book entitled ‘Abortion’ "Contraception and abortion are complementary not competitive methods of fertility regulation." "Given reasonable access to contraception, the resort to abortion declines as a society becomes more experienced in controlling its fertility, but it is never eliminated. A combination of abortion and contraception remains the prime method of fertility regulation throughout the contemporary world." "Unfortunately, the means of contraception available in the closing decades of the twentieth century, with few exceptions, remain, incapable of meeting the goals of family size, set and attained in developing countries. The exceptions are the statistically lucky minority, who consistently use sophisticated methods, such as oral contraceptives, over long periods and those who choose, and are able, to be sterilized when they have attained their desired family size." The Price Women Pay: The price which women pay, in mental anguish, physical harm and cash, to terminate an unwanted pregnancy, cannot easily be stated in statistics' or explained in general terms.

Each of the millions of women involved, and their families, suffer in a different way, and to a different degree. No two cases are the same, as is shown by the following selection of personal stories told by-women who have undergone abortions.

Evidence to Sri Lanka Abortion Survey: The Labourers wife: I got married at 18 to a labourer in government service. Now I an128 years and have five living children one died at nine months. After the last child the clinic doctor said I would be given milk powder for my baby only if I was sterilized or had a loop inserted. My husband did not like family planning before but as we wanted the milk powder badly (I did not have enough breast milk) I got a loop. I did not like it and after about six months my husband was persuaded by a friend to allow me to have an operation. The loop was removed in the clinic, but when I went to hospital I was told to come back in a month as they had no supplies or something. During this time I got pregnant again. This was a severe upset to my family. My husband went to the clinic and some other doctors, to get the pregnancy terminated. They all refused. In desperation we went to a person who was supposed to do abortions 30 miles from our home. My husband sold our cow and I pawned the jewellery. I had to raise Rs, 450 for the fee and car hire. I was bleeding for about three days and had severe pains in my stomach, I went to hospital. I was told that an operation was necessary. I had to stay about a week in hospital. I was told that I will not have any more babies. We have not yet recovered the jewellery. The Rich Man: My fiancée, a professional person, got pregnant. She was living with her parents (extremely conservative people) as we were not ready to get married. My fiancée was getting morning sickness and something had to be done. The difficulty was in deciding-whether or not to get the pregnancy terminated. After a long and painful'discus8ion we decided on abortion. A friend introduced me to an obstetrician. This doctor agreed to operate on the very next morning. My fiancée left home as if she was going to work, had the operation and was back home for lunch as usual. That was all. Of course I had to pay substantial fee. We are still surprised that in this country getting an abortion done is so simple. India: The Untouchable: I am a Harijan, 35 years old and have five children aged; 11,9,7,5 and 2 years. We live in a basti near a colony of big houses. My husband and I work as part time sweepers in several houses. After my fourth child, one of the ‘memsahibs’ work for had been after me to get myself or my husband sterilized. I refused and she was angry. But what could I do? My husband 'Will not think of getting himself operated upon and I am also very nervous because, despite all that they say about it being very simple. I have known many women who have had this done and become chronic sufferers with stomach pains -and other problems. In my work I have to pick up heavy loads, squat, bend and be very active all the time. I can't afford not to be fit. Besides, I would have to be away from work for atleast two weeks and how can I do that? My jobs would go in the meantime. They say it is only two days in the hospital but I have seen so many women have the cut go septic that they are in trouble for weeks and weeks. But when I conceived again I was desperate. I tried to do something about it. An old 'woman who lives in our basti has helped many women before. She gave me a herbal medicine and I did have bleeding with it. Although I had solved the problem. When I didn't bleed the next month. I didn't worry too much because I knew the cycle gets disturbed. But then somehow I began to feel that things weren't quite right. I spoke to the old woman again and this time she tried to help me with a stick. It hurt terribly and I got sore inside but nothing happened. Finally I took courage and told my memsahib. She was even more angry and said I deserved it. But she' gave 'me a: letter and sent me to the hospital. There they examined me…… they did it so roughly that I was bruised and hurting for days....... and told me' that it was too late to do anything this time since I was too weak anc1'tvith not enough healthy blood. They told me to have the baby and that they would sterilize me alongside the delivery. But I didn't go to the hospital for the delivery because I didn't want an operation. (Extracts from: People, Vol. 5, No.2)

Editorial Committee: Anil Patel, Binayak Sen, Kamala Jayarao, Luis Barreto, Vidyut Katgade, Abhay Bang (EDITOR)

Annual subscription - Inland Rs 10 /- For Foreign countries by Sea Mail- 3 US $ /- By Air Mail for Asia - 4 US $, Europe, Africa - 7 US $, USA Canada -9 US$. Edited by - Abhay Bang, Gopuri, W ARDH: 442001 Printed by him at Paramdham Mudranalaya, Paunar, WARDHA, Published by - Abhay Bang, for Medico Friend Circle, Gopuri, WARDHA, 442001 INDIA on 2-2-1980

Views, & opinions expressed in the bulletin are those of the authors and not necessarily of the organisation


