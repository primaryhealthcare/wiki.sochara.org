---
title: "Naturopaths In The USA"
description: 
published: true
date: 2023-06-18T06:26:01.566Z
tags: mfc
editor: markdown
dateCreated: 2023-06-18T06:24:33.842Z
---

*This is a web-friendly version of the article titled Naturopaths In The USA from medico friend circle bulletin. You can find the original in the PDF at [http://www.mfcindia.org/mfcpdfs/MFC093.pdf](http://www.mfcindia.org/mfcpdfs/MFC093.pdf)*

Title: Naturopaths In The USA

Authors: Pediatrics

medico friend circle bulletin SEPTEMBER, 1983

HEALTH "CARE" VS. THE STRUGGLE FOR LIFE Mira Sadgopal* (January 1983)

Part - I India's people, and the "world's people, are faced with a gigantic health "care"" establishment. It is far from being a vacuum, a situation of "neglect" as most politicians and planners would have us believe or sometime themselves believe. Like a huge and ungainly bureaucracy, it is both organised and unorganized. Its various parts are linked with each other in both gross and subtle ways; equally, the parts function in contradiction with each other. Some of the parts of the establishment succeed in holding away in certain spheres by virtue of historical advantage and the forces that back them at the 'moment. Any group claiming to explore "alternatives" must understand human health, and likewise any other sphere of human welfare (like education, economic development, legal justice, etc.) in this perspective, The individual man, woman or child is powerless and thus always prone to being sucked, duped or dragged into the establishment system. India provides a magnificent panorama of such a health care establishment. Most obviously, we have in this country a 'giant multi-tiered Government-operated public health infrastructure, the bottom levels of which are organised into something called the "primary health care" system. It is topped by a spread of state hospitals and national medical institutes as well as various large central public health agencies. Ultimately, this government system is empowered through finance by international organisations and agencies like the WHO, UNICEF, DANIDA, etc.

Second in consequence is the vast body of "qualified" Private Practitioners which, although it is less organised and partially thrives on its own disorganization, also exhibits a hierarchy of influence and power largely corresponding to the proximity of its parts to the cities and the drug industries. It includes graduates of "allopathic" medicine as well as graduates of the ayurvedic colleges, although most of the latter depend on the use of modern allopathic medicines. The minimum requirement for organisation to promote and protect the interests of their members as a class is fulfilled by the Indian Medical Association. Taking third place in visibility, although it exerts the most pervasive and devastating influence, is the huge drug industry complex. There a polarisation within this group between competing indigenous and multinational companies which is unequal, so that indigenous industry either succumbs or adopts policies in tune with the multinationals? The multinational drug industry profoundly controls policy and practice within the Government health system as well .as the behaviour of Private Practitioners by plying central government committees and deploying a large army of medical representatives. Fourth is a large group on the fringe of the health establishment power structure, loudly names "Quacks" by the Private Practitioners. It is a very interesting group without any real political power or legal sanction which thrives on the contradictions of the

* Kishore Bharati Group, P.O. Bankhedi, Dist. Hoshangabad, M.P. - 461 990

establishment, the extreme powerlessness of the masses and the total culture of mystification "which maintains this. This group finds its niche in the rural areas and the lacunae" of the towns. A fifth group exists in the twilight beyond the fringe, often indistinguishable from the masses but merging into the category known as -"quacks". They cannot really be called part of the establishment, but they are quite often the first, last, and sometimes the only recourse of the poor. These are the village dais, the bonesetters, the guineas, ojhas and bhapts (faith healers and magicians). They are traditional, indivisible from the belief system of the masses. The larger health care establishment has an ambivalent attitude towards this section - it is largely ignored or ridiculed. Recognizing their hold over the people, some members, such as the dais, are sought to be co-opted by government training into the primary health system. Also according to establishment values, organised health services are operated to a greater or lesser extent by large public 'and private industries and by the central government for its employees. These are all subject to the same pressures of the health care culture which bear on society in general and are only partially modified by local or specific political conditions or practical purposes, we may add to this category the attempts of a number of voluntary agencies to provide proper and uniform health services in project areas. Seeing the larger interconnecting structure of the health establishment in this way gives us an intellectual idea of its magnitude, but what does it mean for the common man and woman in India?" For a start, we can listen to the stories of hundreds upon thousands of men and women suffering from tuberculosis in our cities, towns and villages. Over and over again we can see a plot thus exposed in stark nakedness, as each tells of the struggle to get treated and cured by any possible means. For instance, a villager who gins cotton as noticed a gradual loss of weight and energy and may be a cough for several months. But so many of the poor are already exhausted and emaciated by life - they find the line between relative health and disease is imperatively crossed - and they think it is only “weakness". When work becomes impossible

they seek quick help from private practitioners, knowing it will cost, but anxious to get well and back to work. They hope to get by with a strength-giving injection, a few pills may be, and a bottle of life-giving tonic which the doctor will prescribe. So a couple of chickens and some grain is sold to raise money. The doctor well recognises the story and the appearance. He suspects it is tuberculosis. He knows the capacity of the poor- they will pay for the belief that they will get well, and as long as that belief can be sustained, they will keep on paying the same doctor. He also knows that this disease, if properly managed, has a good chance of continuing without cure for several years before the patient, dies. Furthermore, the widespread attitude that TB is incurable, supported by the vast majority of cases which eventually end in death, and the doctor's own- observation that patients cannot sustain regular treatment does not lead him to nurture any professional interest in obtaining a cure. Therefore, neither is he interested in proving the diagnosis. A private practitioner will avoid telling that he is treating a man for TB as long as possible. Otherwise he is sure to lose his patient to another doctor. Likewise, sending him for sputum test or X-ray, which may be available through the nearest government hospital, would be giving him away, or privately done would use up available funds. He is not interested in prognosis either _ it will be sufficient to see that the man gets temporary relief and is kept fluctuating within a safe margin between cure and death, with an occasional dramatic rescue from death's clutches, for as long as possible. What does the doctor's treatment consist of, aside from its psychological content? First on the list is Streptomycin injections, one daily if possible, which is more likely impossible if the patient lives far away. (He may be given tablets of Isoniazid in various proprietary preparations in place of streptomycin, in which case he is certain to be sent off with a couple of impressive on-thespot injections, such as liver extract and red-coloured vitamin B12.) Next, he will be prescribed ethambutol tablets (under one of the marketed brand. names), a second line drug for TB which is comparatively expensive but which is being promoted by multinational companies through their medical representatives as a first-line drug. Third, a corticosteroid hormone like betamethazone (again, under numerous brand names) will

be routinely given or prescribed by most private practitioners at the start of anti-TB treatment, as it is expected to bring about rapid relief from symptoms and a specific false sense of physical well-being which may be the major factor in hooking the patient. Fourth will be a large bottle of mineral and vitamin tonic which also ironically contains something to stimulate the appetite of the person who is basically dying of hunger anyway. Fifth, a syrup will be added to suppress the cough. The expense of the first week of such treatment works out as follows (approximately): 1.

Inj. SM @ Rs. 3.00/day x 7

21.00

2.

Tab. Ethambutol 1 twice/day @ Rs. 2.50/day x 7

17.50

3.

Tab. Betamethazone 1 thrice/ day x 7 = 21 tablets

8.00

Vita-mineral tonic - single large bottle

20.00

Cough syrup - single bottle

8.00

4. 5.

74.50

The doctor's initial fee will vary, but he will also take a daily fee for injecting streptomycin. If he is a good dramatist and psychologist, and the family is obviously prepared to pay, he may set up an intravenous drip and charge heavily. Quite often, the person does not have enough cash to buy some of the medicines. Typically, the tonics and nonTB medicines will be bought and the anti- TB medicines will be partially or totally dropped from the list. (A survey done by Veena Shatrughna has shown that many doctors write the tonics and less necessary medicines first, perhaps to oblige the drug companies and the specific curative medicine last.). How long is this to go on? We have found that a doctor tells the patient initially that his treatment may take a varying period between two weeks to three months. He may decide to further prepare a mental frame by stating that the man is lucky that the doctor has caught the "disease" at this stage because, although he doesn't have TB yet, "There is a chance of it turning into TB!" Even if a man has collected enough funds for the initial treatment, he may not be able

to follow up. After a varying number 0: visits to the doctor, and especially after c marked improvement, he stops going - he may go back to work. He also meanwhile consults a gunia of his community about wording of: the risks of getting TB, and after certain divination the gunia advises him to carry out certain rituals and sacrifice, which are usually done. After some time, he again loses weight, and his cough worsens. He thinks about returning to the doctor. The doctor's mention of TB has scared him, and he is ambivalent. He may do one of three things: he may go to another private doctor or a quack, he may go to the government doctor, or he may return to the same doctor after all. If he goes to another doctor, he goes with a blank slate - he doesn't mention that he has seen another doctor, or flatly denies previous treatment. Hence, a second version of his first experience is likely to unfold. A streak of realism may hit him. He may realise that the chance he has TB is high now, and decide to see the government doctor. At least he may get a clear answer even if he doesn't have faith in the government treatment. The government doctor is a strange kind of super human. He is invested with the power to treat when he pleases at the Government's expense. (He also carries out a respectable private practice in his home at the Government's expense.) A patient approaches him in fear and trembling. Diagnosis for purposes of initiating government treatment is obtained through sputum exam or X-ray, whichever is feasible. Anti TB treatment is started on the doctor's orders. He tells the patient he has TB, or he says, "There is a chance of it turning into TB:" depending on the role he wishes to play in the drama with the Patient - Government Doctor or Private Practitioner. Sometimes he adopts a dual role, issuing government drugs from the Primary Health Centre for seeing him privately at home, too. Government rules for the treatment of new cases of TB are clear and rational, the full treatment of eighteen months provided for under the National Tuberculosis Control Programme. After positive sputum examination, treatment is started. Streptomycin injections are to be given daily for one month, then on alternate days for two months more. (An abbreviated schedule which is medically

d) e) f)

acceptable is 'daily x 15 days, then alternate days x 2 weeks, then twice weekly x 2 months, again totaling 3 months.) Daily Isoniazid (INH) tablets are also given. After three months, sputum examination is to be repeated (if the patient is still coughing up sputum). There should be no more tuberculosis bacilli detectable in the sputum. Then, if not before, an Xray screening is called for if feasible from the nearest TB X-ray facility. The reduction in the extent of lung damage is' thus monitored every six months until six months have passed since disappearance from the Xray of the signs of damage, when treatment may be officially discontinued. If progress is satisfactory, Streptomycin injections are to be replaced after three months by another drug, usually Thiacetazone (THZ) but it might be ParaAmino Salicylic Acid (PAS). The PHCs dispense Isoniazid and Thiacetazone in combined INH/THZ tablets to be consumed daily for the total remaining period of treatment. To ensure that a patient keeps up regular treatment, he is supposed to be called every month on a particular date three days before the drugs with him are due to finish. In case he does not turn up within a few days, a printed postcard reminder is to be sent to him. If he does not respond to three such reminders (and he has not died), he is known as a "defaulter".

3.

Problems of Drug supply and Regular Issue. a) b) c)

genuine short supply to PHC from District HQ siphoning off of TB drugs into the market siphoning off of TB drugs into the private practice d) incomplete issue of drugs e) doctor's failure to indent (maladministration)

4.

Problems of Medicine Cost from the market when unavailable through government supply a) b) c)

5.

b) c)

There are innumerable obstacles in the way that ensure failure of treatment or "default". We can list these, as follows: Problems of Diagnosis a) b) 2.

6.

Sputum exam: technician not available, or refuses X x ray/screening facility distant, expensive, out of order, or x-ray plates not available.

Failure of Communication to Patient by Doctor a)

intention, or lack of intention of doctor to inform

b)

patient's fear

c)

contradictions in the belief system in society about disease

high/rising prices of essential first-line drugs, especially Streptomycin injections shortage of all first-line drugs in the market due to gross under-production. increase in market supply of expensive second-line anti- TB drugs like ethambutol, rifampicin

Unnecessary Medicine Cost on Vitamin and Mineral Injections and Tonics. and costly Cough Mixtures a)

But what really happens to the ordinary patient, or to our villager friend who gins cotton?

1.

doctor's impatience mystification of doctor's role poor relations/faulty communication between PHC staff

brainwashing of doctors by medical representatives overproduction beyond licenced capacity of tonics, etc., by large and multinational drug companies mystification among the masses about tonics and the desperation for quick life-giving cures

Problems of Local Arrangement to Inject Streptomycin a)

unavailability of doctor/health worker to inject b) fee for injection daily c) PHC may refuse to issue injections to patient to take home

7.

Problems of Transport a) b) c)

8.

distance cost in time, energy, fare irregular public transport services

The Social Milieu at Home a) poverty - poor shelter, starvation

b) c)

9.

demoralisation sex-bias in case of women, especially when childless or without living male offspring d) belief in magic and lack of scientific concept of disease Conditions of workplace and Occupation a)

economic exploitation

b)

noxious physical conditions, like inhalation of

c) d)

cotton fibre and poor ventilation, etc. lack of safety standards lack of alternatives

b) misinformation or non-information of patient c) failure to record (incomplete) issue of drugs d) neglect of monitoring schedule e) failure to maintain treatment card f) failure to contact defaulters by postcard. Now, it is sufficient to say that the average poor man of India who gets TB today is likely to face every single one of these obstacles, except 8 (c) as he is not a woman. Inevitably, he becomes a defaulter, or he dies, or more likely both. Are there really any alternatives? (To be continued)

10. Specific Malpractices by PHC Staff and Doctor a) Private practice

ANTIBIOTICS IN DEVELOPING COUNTRIES In 1977, the WHO provided a list of 210 essential drugs, to help developing countries choose a limited number of- drugs that are inexpensive but of high quality. Though such lists have been in use in Scandinavian countries, the drug industry was highly critical of the WHO list. A· survey studied marketing of antibiotics in Central America and was published in Lancet (Jan 3, 1981). In Mexico, 430 brands of antibiotics were marketed, of which 180 were combinations. In comparison, Sweden has 90 brands with only 2 combinations. The stated reasons for use of combinations are that they have a broader spectrum of action and that antibiotics reinforce each other or that they will be effective even if resistance against one occurs.

A second type of combination is antibiotics with enzymes, claimed to improve uptake by inflamed tissues. Some preparations for gastrointestinal infections contain Kaolin and/ or pectin. A third combination is antibiotic with a mucolytic and/or cough suppressant. ‘Bisolvon Eritromicina' with bromhexine is said to increase immunoglobulin A. Antibiotics ire claimed to be effective against influenza and viruses. A preparation meant for infants contained streptomycin, tetracycline with enzymes. The Survey shows that in each country of Central America, not less than 200 brands of antibiotics are marketed. The investigators say "how can doctors in these circumstances become familiar with the essential properties of important drugs. A reduction in the number of drugs might improve antibiotic use in clinical practice”.

[Do Bulletin readers have any such information for India? - ED]

NATUROPATHS IN THE USA (Extract from Pediatrics 68:407, 1981) Despite the availability of a highly developed, medical care system, many Americans place substantial reliance on folk medicines and unorthodox practitioners. We often encountered families who indicated that a naturopath was a major source of their health care. Schools of naturopathy reached a peak around 1950 and declined by 1960. The fortunes of naturopathy took a dramatic upturn in the 1970s, along with the increasing popularity of natural foods, organic gardening, and

"holistic medicine". Today's naturopathic colleges require 3 or 4 years of undergraduate study for admission with a basic pre medicine background. The graduate is expected to be skilled at performing minor surgery, and assisting in all phases of obstetrical care for natural child birth and home deliveries. Fasting-from days to weeks-is recommended for many ailments, including arthritis and sinusitis. The symptomatic treatment of fever is thought to interfere with natural

curative processes. The efforts of naturopaths are therefore directed toward strengthening the Individual's resistance to disease. Through optimal nutrition and hygienic practices, be need for vaccinations could be totally obviated. Several practitioners also

expressed the belief that injecting antigens was an abnormal form of exposure, an invasion of the patient's defenses, and therefore potentially harmful. True exposure to some of the infectious diseases was often considered the preferred method of obtaining long term community. "The inoculations are not known to give life-time protection, whereas actually contracting the disease does. In the old days, they used to have a 'measles party' in order) deliberately expose children. I would like to see the Public Health Department make his kind of exposure available". Such approaches were also defended in egalitarian grounds. "The vaccine route as chosen because of the medical orientation, essentially a pinnacle type hierarchical system with a very clear authority figure and people

subservient to the authority. Injections are only available on a prescription basis. When we

teach people to Jive well, to eat properly

that doesn't structure.

require

a

pinnacle-type

Homeopathic remedies are an important component of many of the naturopaths' interventions. While defending homeopathy as efficacious, many naturopaths acknowledged the placebo effect of these remedies. The emphasis by naturopaths on patient teaching, individualized care, and 'natural' remedies, and their a version to scientific medicine have become increasingly valued by medical care consumers. Because many ailments are minor and self-limited, and many naturopathic remedies are without obvious harm, 'encounters with naturopathic practitioners are often benign, if not clearly beneficial. In the case of childhood infectious diseases, however, immunizations can be life saving. Specifically, immunization programs are preventative, and their efficacy involves stimulating the body's natural defense mechanisms.

LETTER TO EDITOR Dear Friend, Medical Ethics and Practice Medical Ethica has become the talk of the day both inside and outside the medical community. A large section of people are lustrated with the treatment they get from hospitals, medically and otherwise. Private treatment is expensive and even the middle ass is neither able to afford the specialist or his prescription. The common man is becoming more and more sceptical about re professional integrity of medical men, in the other hand" medical men of eminence id professional integrity are also very much sizzled as to why such a curse has fallen upon such a noble profession. People in power also lose no opportunity to accuse us of erosion of values, perhaps to shirk their own responsibilities. While confronting different adverse conditions in our profession, doctors are also grown into an ethical dilemma. Circumstances force doctors to compromise with medical ethics every now and then. Why at all such an ethical crisis today? That has, gone wrong with our system? What are the real factors behind all these maladies?

How are we going to establish the divine image we had once upon a time? We have come to a stage where thorough reevaluation and redefining of ethical values, to suit the present day problems of our system, has become absolutely essential. Medical ethics involves seeing that patients get proper and adequate treatment. Looking back, we find that the emphasis of traditional ethical codes was on the responsibility of doctor towards his patient. But with the progress of, science, particularly medical science and society, the effectiveness of health services is primarily decided by how best the medical system is organised, though responsibilities of the doctor towards the patient continue to remain fundamental. In modern days, when medical science is capable of eliminating certain diseases altogether and can prevent the occurrence of many others' it is not only the individual doctor's competence, but mainly the effectiveness of the medical policy and its implementation over a social plane that ensures the health of the society. Hence, maintenance of medical ethics has become more of Governmental responsibility. Moreover, institutionalisation of medicine,

specialisation, team (or) group practice etc., are the outcome of progress of medical science and practice. Hence under conditions of institutionalised medical care; an ethical responsibility also rests on the institution and is shared by other medical personnel, like nurses, assistants, technicians, etc. These developments have opened up new ethical questions which cannot be solved by traditional codes of medical ethics. Is the erosion of medical ethics, accidental and isolated or is it a reflection of the political, economic, social and cultural crisis that has engulfed our country? Is our ethical dilemma due to our own individual vacillations or is it due to the contradiction between personal and social interests, between backward

conditions and scientific advance? What is going to be our attitude towards these grave problems facing our community? Is it going to be one of coming to oversimplified conclusions, superficial judgement and illmotivated, escapist accusations of people in power? Or are we going to analyse the problems in the overall context of the medical system and strive to evolve proper medical policies and their effective implementation, thereby evolving a new code of medical ethics that will suit present day conditions? We invite you to give your valuable suggestions and opinions. Medical Action Forum Madras

WHY SOYA BEAN? K. T. Acharya 'During the current year, nearly 8 to 9 lakh hectares appear to be under soya bean cultivation mainly in Madhya Pradesh, with a yield expectation of perhaps 6 lakh tonnes of soya beans. These figures are expected to double· in the next three years (M.P. Mansingka, Chairman, Soya bean Processors Association of India; quoted in The Hindu, September 29, 1982).

from earning foreign exchange to the tune of some Rs. 80 to 100 crores, there are such attractions to individual producers as export entitlements. But there is another side to this success story. For long it was convenient to argue that soya beans were being additionally grown on land that would otherwise lie fallow during the Kharif season, thus ensuring sufficient soil moisture for the following rabi wheat crop. Growing soya, a leguminous crop, on such land was said to fertilise the land, while shedding of its leaves helped to conserve needed moisture. Today, however it would appear that two-thirds of the land under soya in Madhya Pradesh was what once used to raise jowar, millets, several lentils, and groundnut (India Today September 30, 1982, p.127). All these are foods that can be directly cooked and consumed by common people, which is not true of the soya bean.

What has led to these rapid and remarkable developments in what is after all an unfamiliar crop? One is the support price offered by the government to the soya bean, which ensures a profitable return to the farmer. There is no such attraction for the groundnut, our major oilseed crop, which continues to anguish. It is stated that the profit, per hectare of soya considerably exceeds that derived from the groundnut (India Today, September 30, 1982 p. 127). Industrialists are well content too. Processing soya yields about 16 to 18 percent oil and any edible oil today etches an excellent return because of desperate shortages and high oil prices. The oilcake which results is an excellent protein-rich cattlefeed with a well established international Undoubtedly soya oilcake ' is edible. It is now eJ'1land: The high lysine level of 6.2 percent is exported, but even were it to be used for humans in India, exceptional among oilcakes, though it is well to remember this would necessarily be 'in processed foods that will not that many common dhals bengal, gram, masoor, tuvar and reach everyone as will jowar, millets or pulses. The value mung) have even higher levels. There is no worrisome of processed foods in India is just two percent that of problem of aflatoxin contamination. All of it exported, total foodstuffs. The oil yield of the Soyabean is small earlier largely to Southeast Asia rd to the Gulf countries, just 16 to 18 percent, against 40 to 45 percent for all and more recently to European counties as well (Dattu groundnut. So unless the yields of the Soyabean are 2.5 Hegde, Economic Times, August 19, 1981). Apart to 3 times that of the groundnut, there is little advantage to the oil economy (A.C. Chhatrapati; Economic and Political Weekly, 1980, 15 No. 37, Sept. 13, 155.7). In practice,

FROM THE EDITOR'S DESK Mira Sadgopal had recently raised the question of popularising natural methods of family planning (Bull.No.89). Whatever the disadvantages and even the health risks of chemical contraceptives, they cannot be replaced by the natural methods, in this country. The women need to be educated about the physiology of fertility; the methods' for testing for ovulation need to be really simple, inexpensive and accurate. Most important, as Mira too has pointed out, it need total cooperation and commitment from the man, which in present day Indian Society is well-nigh improbable. Even if every other criterion is fulfilled, the protection rate may not be as high as with other methods. This can cause much tension among the couple. This is not to deny that simpler methods for detecting ovulation need to be discovered. But, this is to emphasise that continuing research is needed on other forms of female contraception and more commitment towards work on male contraception. As we have discussed and pointed out more than once, there is an urgent need to focus attention on barrier methods. It is a matter of real concern that no research groups, either in the public or private sector, is concentrating on improving the technology in this area. Some 

(continued from page 7) the average output of soybeans per hectare is 800 kg. no different from that of groundnuts under ordinary rainfed conditions, and far below that when its is given even two irrigations (A.C. Chhatrapati, Economic and Political Weekly, 1980, 15.No. 37, Sept. 13, 1557). Can the escalation in the raising of the soya be justified from the point of view of food needs? As it is, over 50 percent of our land area is cultivated. This is an excessively high figure, and the areas devoted to forests (22 percent) and pasture (4.4 percent) are both alarmingly low. The possibilities of more cultivable land are therefore all but exhausted, and further increase in food production must come from higher productivity per hectare. For the vast majority of our people, what matters for reasons of cost are foodstuffs that can be consumed without processing. Are there such overwhelming reasons for the: extensive lobbying and deliberate market support in respect of this commodity when there are desperate shortages of such

argue that this is perhaps due to the fact that turn-over of barrier contraceptives is less compared to pills and may not be profitable to the drug industry. Not so, really. Effective barriers will be more popular and ensure a wider market. Even if the argument of a low consumption is true, why are the research groups in the public sector too, equally inattentive? Perhaps, they are not prestigious areas of R&D? We hear so much these days about relevant science and relevant technology? Is this area not relevant, considering the 2 percent or more growth rates? What does relevancy in research denote? Relevant for the research workers or relevant for the country as a whole? Working on steroidal contraceptives brings international recognition. Even the parent country may ignore research in barrier contraceptives. And of course, funds for research - if they are received from a foreign or international agency, that includes an annual holiday abroad for the, scientists, or, is it simply this-that all concerned have forgotten that there existed at one time barrier contraceptives for women, too and that the available technology needs only to be improved upon. Women's groups and doctors alike should ask for free availability of effective barrier contraceptives.

everyday foods as pulses, oilseeds, and prices of these are skyrocketing? The soya bean is an acknowledged source of protein for animals, and marginally of processed foods for man. Neither of these can be considered priority matters in India. Nor 'must we disregard the inherent risk of 'competing in international soya meal export markets with giants like the USA and Brazil who will largely dictate prices and policies; At one, 'vegetable', cooking type soya bean varieties like Verde, Disoy, Bansai and Kim were reportedly being developed. As foods with a natural high protein and medium oil content, these would deserve a fair trial as potential items in the everyday food basket, but little is heard about them now… To encourage the use of scarce agricultural land in India in ways that contribute only marginally to supplies of everyday foods that are in increasingly short supply is a policy that is fraught, with danger. Serious reconsideration is called for. [Reprinted from NPI Bulletin, Jan.1983]

Editorial Committee: Anant Phadke Padma Prakash Ravi Narayan Shirish Datar

Ulhas Jajoo Editor Kamala Jayarao

Views and opinions expressed in the bulletin are those of the authors and not necessarily of the organisation.


