---
title: Health, Nutrition and Development Initiative of Azim Premji University
description: This initiative aims to offer a broader and integrated understanding of health by locating it within the larger development narrative and by necessarily engaging with multiple perspectives, approaches and methods.
published: true
date: 2023-06-14T09:25:47.798Z
tags: 
editor: markdown
dateCreated: 2023-06-14T09:20:59.806Z
---

i