---
title: Maya Mascarenhas
description: 
published: true
date: 2023-07-31T16:06:00.145Z
tags: 
editor: markdown
dateCreated: 2023-07-31T16:06:00.145Z
---

# Dr. Maya Mascarenhas

Treasurer of [SOCHARA society](/sochara/members)

## External Links
* https://twitter.com/mayaninamas?lang=en
* https://scholar.google.com/citations?user=XWpafbYAAAAJ&hl=en