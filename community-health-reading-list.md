---
title: Community Health Reading List
description: 
published: true
date: 2023-06-15T03:39:46.120Z
tags: 
editor: markdown
dateCreated: 2023-05-25T09:27:48.916Z
---

## Regional Disparities and inequalities – Trends 

1.  Kurian, NJ (2007). ‘Widening Economic and Social disparities: Implications for India’, Indian Journal of Medical Research’, 126, October 2007, pp 374-380
2.  Government of Karnataka (2001). ‘Study on health and health disparities in Health and Health care services Karnataka’ in ‘Karnataka towards Equity, Quality and Integrity in Health
3. Final report of the Task Force on Health and Family Welfare’, Bangalore, April 2001

**Recommended**

1. Braveman, P. (2006). Health Disparities and Health Equity: Concepts and Measurement&#39;. Annual review of Public Health, 2006; 27; pp167-94

## Issues of Access- Availability, Accessibility, Affordability, Acceptability and Quality in Health Care

1.  Dilip T. Extent of inequity in access to health care services in India. Rev Health Care India. 2005;247–68. (for data presentation)

1.  Jacobs B, Ir P, Bigdeli M, Annear PL, Van Damme W. Addressing access barriers to health services: an analytical framework for selecting appropriate interventions in low-income Asian countries. Health Policy and Planning. 2012;27(4):288–300.

## Right to Health

1.  Health is a Fundamental Right in The Evoluation of Alma Ata Declaration. The People's Health Source Book.pp74

Movies:

1.  Public Hearing on Right to Health Care. Vol. 1 - 4
2.  Left Out
3.  Report on Binayak Sen’s Arrest under POTA Act
4.  Constitutional and legal aspects of health

## Ethics of health and health care

1.  Medical Ethics, Medical Education and Health Care. The People's Health Source Book.pp332-357

Movies:

1.  Ship of Thesus

## Equity in Health

### Promoting community mental health and intervention

1.  Mental Health in India – An Overview. Defending the Health of the Marginalised. pp40-50

Movies:

1.  Lalaeppa - A Journey from Mental Illness to Recovery

## Understanding Social Exclusion/ Marginalisation including stigma and discrimination

### Social Exclusion as a determinant of Health

1.  The Marginalised and the Vulnerable.The People's Health Resource Book.pp198-203
2.  Nayar, K. R., & others. (2007). Social exclusion, caste & health: a review based on the social determinants framework. Indian Journal of Medical Research, 126(4), 355.
3.  Health For Dalit Communities. Defending the Health of the Marginalised. pp7-20

### People with Disability

1.  We are the Differently Abled! The People&#39;s Health Resource Book.pp253-257

Movies:

1.  Oonam un kannil (Disability in your eyes)
2.  Disability is not inability

## Social Determinants of Health and Social Action

### Social Determinants of Health, action on determinants and social vaccine

1.  Rakku’s Story. Rakku’s Story Structures of Ill-health and the Source of Change.pp19-41
2.  Making Life Worth Living! The People&#39;s Health Resource Book.pp132-196
3.  The Economic Base of Ill=Health. Rakku&#39;s Story Structures of Ill-health and the Source of Change.pp47-59
4.  Kerala: A Beginning. Rakku’s Story Structures of Ill-health and the Source of Change.pp174-181

1.  Towards The Concept Of A Social Vaccine. Background paper for Double Plenary Session on ‘Social Vaccine’ at Forum 10 Cairo, 30th October, 2006&quot;
3.  Baum F et al. Social vaccines to resist and change unhealthy social and economic structures: a useful metaphor for health promotion. Health Promotion International, Vol. 24 No. 4
4.  Towards a Broader Understanding of Social Vaccine: A Discussion Paper. Social Justice in Health Multiple pathways towards Health for ALL. pp130-134

### Environmental Sanitation and Community led total sanitation

1.  Prahlad IM. Environmental Sanitation: &quot;Reflections from Practice&quot;- A Module for Community Health Practitioners.

Movies:

1.  Faecal Attraction - Political Economy of Defecation

### Understanding the scope of environment, ecology and health

AV Material

1.  Rajagopalan R. “Drink Coffee in the US and make the Song Bird Vanish in South America”, In: From Crisis To Cure. Oxford University Press.

1.  Rajagopalan R. “Connections – get rid of Malaria and invite Plague”. In: From Crisis to Cure. Oxford University Press.

Movies

1.  Water Gives life - A film on Participatory Watershed Development
2.  Water of the People
3.  Rajagopalan R. Is there a global crisis? In: From Crisis To Cure. Oxford University Press.

## Understanding environment as a determinant of health

AV Material

1.  Sprays of Misery, film by SOCHARA
2.  The killing fields - programme on Pesticides
3.  Case Studies file-book/print-outs


Reading list:

1.  Transect walk notes compiled by Adithya Pradyumna.
2.  Conant J and Fadem P. Promoting Community Environmental Health (Chpater 1). In: A Community Guide to Environmental Health. Hesparian.

1.  Bhopal Gas Tragedy – The Call for Justice, film by The Other Media and ICJB

## Understanding developmental models and its impacts on equity, environment and Health; Understanding about resource exploitation, industry, energy, products, consumerism Pollution; and Understanding about urban and rural issues in the context of environmental health

AV Material:

1.  Iron-ore mining in Bellary (Kannada), film by Sakhi
2.  Documentary based on Plachimada and water as a resource
3.  Nagara Nyrmalya (Kannada), film by ESG

Case Studies

1.  Rajagopalan R. “Too many people, too few resources”. In: From Crisis To Cure. Oxford University Press.

2.  Excerpts from “Everybody Loves a Good Drought” by Sainath P.
3. Pradyumna A. "The Endosulfan Tragedy of Kasargod"(Unpublished). CHLP Resource Material, SOCHARA.

“Development induced displacement”. Compiled by Adithya Pradyumna.

1.   Sainath P. Extracts from The Khariar Bull case, In: Everybody Loves a Good Drought.

Reading list:

1.  ESG. Bangalore’s Toxic Legacy: Investigating Mavallipura’s illegal landfills. Online.
2.  Cotton For My Shroud, a film by Nandan Saxena and Kavita Bahl

### Understanding the science, politics and implications of climate change on health, and the potential solutions to the problem at various levels, keeping an India focus

AV Material:

1.  Short movies by WOTR on climate change
2.  Changing climate: The politics

Reading list:

1.  WOTR. Health Vulnerability Assessment: A Manual.
2.  An Inconvenient Truth, a film by Al Gore

### Understanding about the environmental movement in India and abroad, and the impact it has had on policy, practice and general conscience

AV Material:

1.  Case Study: Hesparian. The Green Belt Movement. In: A Community Guide To Environmental Health.
2.  Our School on a Watershed, film by WOTR

Reading list:

1.  Doob – a film on Narmada Bachao Andholan, by Initiatives: Women in Development
2.  Pradyumna A. Environment Movement – Countervailing Power. In: Social Justice in

Health. SOCHARA.

**To learn about community health processes in monitoring health and environment; To learn about the Community Health and Environmental Survey Skill-share (CHESS) initiative**

AV Material:

1.  Powerpoint presentation

Reading list:

1.  SACEM. Chemical Odour Incidents in SIPCOT Industrial Area, Cuddalore. The

Other Media. 2004.

1.  Pradyumna A and Narayan R. Story of CHESS. In: Examining Environment and

Health Interactions. SOCHARA. 2012.

**To understand about the concepts and process in conducting a Health Impact Assessment (HIA), and how it is important in decision making processes in environmental health**

Reading list:

1.  Pradyumna A. Health aspects of the environmental impact assessment process in India. EPW. 19th Feb 2015.

**To understand about environmental health policy; To familiarise with key issues in environmental ethics**

Material:

1.  Poison on my platter, film on GM Foods by Mahesh Bhat
2.  Story of Stuff, film by Annie Leonard

Case Study:

1.  Rajagopalan R. “Inspiration: From Crisis to Success Story”. In: From Crisis to Cure. Oxford University Press.

Reading list:

1.  Conant J and Fadem P. Environmental Rights and Justice (Chapter 4). In: A Community Guide to Environmental Health. Hesparian.

**f. Understanding Community Health/ Public Health - Principles and Axioms and Primary Health Care**

**Community Health, Public Health, Community Medicine, Preventive and Social Medicine**

Reading List:

1.  Community Health in India. Community Health In Search of Alternate Processes.pp18- 31
2.  Community health as the quest for an alternative. Community Health In Search of

Alternate Processes.pp71-82

1.  The Medical Model of Health in Sources of Legitimation. Rakku&#39;s Story Structures of Ill-

health and the Source of Change.pp130-134

1.  Community Health: Search for a New Paradigm- Dr. Ravi Narayan, Health Action Vol.

12, No.11, November 1999 p 5-31

1.  Emerging Concepts and Paradigms. Social Justice in Health Multiple pathways towards

Health for ALL. pp41-42

Movies:

1.  Community Health - Search for a people oriented paradigm

7\. Community health: A Search for a New Paradigm

**Social, economic, political, cultural, ecological determinants of health and their inter relationships and dynamics**

Reading List:

1.  Community Health: The Political Dimension! in A Continuing Dialogue:Key Responses to the Red Book. Community Health In Search of Alternate Processes.pp96
2.  The Economic and Political Order. Rakku’s Story Structures of Ill-health and the Source of Change.pp116-124

**Axioms and principles of Community Health**

Reading List:

1.  Axioms of Community Health. Community Health In Search of Alternate Processes.pp44-48

## **History and relevance of comprehensive primary health care and strategy / approach towards health for all** 

Reading List:

1.  Health System in India: Crisis &amp; Alternatives

First Edition : October 2006

1.  http://www.nhp.gov.in/
2.  Health For All, NOW – The People’s Health Sourcebook – JSA
3.  Alternative Approaches to Health Care; ICMR and ICSSR
4.  Evaluation of Primary Health Care Programmes; ICMR 1980
5.  Health and Family Planning Services in India, D. R. Banerji 1985
6.  Central Bureau of Health Intelligence – National Health Profile

**Learning from community health initiatives and action for health**

Reading List:

1.  Mallur health cooperative (Community Medicine Department of St. Johns Medical College),

Mallur, Karnataka. Alternative Approaches to Health Care.

1.  Towards a people oriented health care system. Community Health In Search of Alternate

Processes.pp83-94

1.  Anubhav Series

## **Occupational Health and Urban Health**

### **Occupational health of workers – organized and unorganized**

**Understanding the perceptions of the fellows with respect to occupation; Understanding the linkages of occupation to caste, class and gender**

1.  Chatterjee P, Dasgupta J, Rangamani S, Patel J. Work, health and rights. Mfc Bull. 2013, Feb;352-354:1-5.

2.  Pradyumna A. Perceptions of conservancy workers and manual scavengers on their

3. Occupation and health – a qualitative preliminary study \[Internet\]. 2012 \[cited 2013 Oct 23\].

Available from:

http://www.academia.edu/4405844/Perceptions\_of\_conservancy\_workers\_and\_manual\_scavengers\_on\_their\_occupation\_and\_health\_\_a\_qualitative\_preliminary\_study

**Understanding the two major categories of work**

AV Material:

1.  Movie on Manual Scavenging, by Jeeva

Reading list:

1.  Dasgupta J. A short note on the unorganised sector in India. Mfc Bull. 2013 Feb;352-354:13–4.
2.  Srivatsan R. Informal labour - elementary aspects. Mfc Bull. 2013 Feb;352-354:10–2.

### **Understanding the impact of workplace on health; Understanding the meaning of the terms hazards, exposure and outcomes**

Movies

1.  Short film on Silicosis (Hindi), by Shilpi Kendra
2.  Short film on agriculture and climate, by WOTR

Reading list:

1.  Gupta A, Nidhi A, Wadkar N. Silicosis - action research. Mfc Bull. 2013 Feb;352-354:23–5
2.  Cotton for my shroud, movie by Nandan Saxena and Kavita Bahl

### **Understanding about the special situation of women and work**

Case studies

1.  “Doctors do not always have the answers”, taken from Conant and Fadem, A Community Guide to Environmental Health. Hesparian.

Powerpoint presentation (SOCHARA policy)

Reading list:

1.  Dasgupta J. Maternity and Women Wage Workers in the Informal sector. Mfc Bull. 2013 Feb;352-354:26–30.

2.  Swaminathan P. Revisiting the theme of Women as Workers and Women’s Work. Mfc Bull. 2013 Feb;352-354:31–2.

3.  Khanna R. Women

### **Understanding the laws to protect the vulnerable in occupational health settings - Child labour as case study, social protection schemes such as ESI**

**Understand about the efforts being made by NGOs, peoples movements and trade unions in improving occupational health -**  CHESS

Reading list:

1.  Pradyumna A, Narayan R. Examining Environment and Health Interactions: Responding with communities to the challenges of our times \[Internet\]. Bangalore, India: SOCHARA;2012

2.  Patel J. Legal Provisions for Protection of Health and Safety. Mfc Bull. 2013 Feb;352- 354:47–50.

3.  Jana S, Ray P. Strategising occupational health and safety in sex work. Mfc Bull. 2013 Feb;352-354:18–22.

## **Social security and social protection and occupational safety**

Reading List:

1. Securing People&#39;s Livelohoods. The People&#39;s Health Resource Book.p177-191

## **Urbanization and urban health challenges**

**Reading List:**

1. Health of The Urban Poor. Defending the Health of the Marginalised. pp21-29

**Movies:**

1. From the margins: Do nadolan; concerned city; citi&#39;s edge; fish tales; kahani pani ki

2. Bangalored in Bengaluru

3. Bombay our city

4. Urban Health

## **Health Policy**

### **Understanding health policy process**

**Reading List:**

1.  Walt G, Gilson L. Reforming the health sector in developing countries: the central role of policy analysis. Health Policy Plan. 1994;9(4):353–70.
2.  Brugha R, Varvasovszky Z. Stakeholder analysis: a review. Health Policy Plan. 2000;15(3):239–46.
3.  Russell J, Greenhalgh T, Byrne E, McDonnell J. Recognizing rhetoric in health care policy analysis. J Health Serv Res Policy. 2008;13(1):40–6.
4.  Parkhurst JO, Vulimiri M. Cervical cancer and the global health agenda: insights from multiple policy-analysis frameworks. Glob Public Health. 2013;8(10):1093–108.
5.  Morgan-Trimmer S. Policy is political; our ideas about knowledge translation must be too. Epidemiol Community Health. 2014;jech-2014.

### **Understanding health policy history and current situation**

**Reading List:**

1.  A Promise of Better Healthcare Service for the Poor- NRHM, A summary of Community Entitlements and Mechanisms for Community Participation and Ownership for Community Leaders by Community Monitoring of NRHM, First Phase, 2007
2.  Dentralized Health Planning in What is to be done?The People&#39;s Health Source Book.pp97-100
3.  Mission Document- I, National Rural Health Mission (2005 – 2012) - Ministry of Health and Family Welfare, Government of India, 2006

## **Primary Health Care and Health For All**

**Reading List** 
1.  Health System in India: Crisis &amp; Alternatives: First Edition : October 2006
2.  http://www.nhp.gov.in/
3.  Health For All, NOW – The People’s Health Sourcebook – JSA
4.  Alternative Approaches to Health Care; ICMR and ICSSR
5.  Evaluation of Primary Health Care Programmes; ICMR 1980
6.  Health and Family Planning Services in India, D. R. Banerji 1985
7.  Central Bureau of Health Intelligence – National Health Profile
8.  Public Health and Primary Health Care. Chapter 5, Karnataka: Towards Equity, Quality and Integrity in Health, Final Report of the Task Force on Health and Family Welfare,GoK, April 2001. Page 52-57
9.  Local Planning Needs an Effective Health Information System. The People's Health Resource Book.pp84-92

## **Universal Health Coverage**

1. Medico friend circle bulletin. Medico Friend Circle. 2011 Jul;345–347.

## **Special Competencies – I**

**Leadership**

1.  Topic 13. How to lead and build a Health Team?. A hand out from the project on “Integrated Management of public health programmes at district level.

### **Governance and Decentralization**
**Reading List:** 

### **Partnership and Advocacy**

1.  Topic 14. How to promote, communicate and advocate for health?. A hand out from the project on “Integrated management of public health programmes at district level.
2.  Topic 16. How to build and sustain partnerships?. A hand out from the project on “Integrated management of public health programmes at district level.

## **Special Competencies – II**

### **Communication including informatics**
**Reading List:**

### **Monitoring and Evaluation**

1.  Topic 12. How to Monitor and Evaluate? A hand out from the project on “Integrated management of public health programmes at district level.

## **Public Health Management**

### **Understanding Systems and Management Principles**

1. Topic 1: Public Health management at District level: Concepts and Values. A hand out from the project on “Integrated management of public health programmes at district level.

### **Public Health Management at community and district levels**

1.  Topic 2. A New Frame Work For Public Health Management: Roles, Skills and Challenges. A hand  out from the project on “Integrated management of public health programmes at district level.

### **Managing partnerships with community and other sectors**

1.  Topic 16. How to build and sustain partnerships? A hand out from the project on “Integrated management of public health programmes at district level.

## **Research- I – Measuring Health and Disease**

1.  Varkevisser CM, Pathmanathan I, Brownlee AT. Designing and conducting health systems research projects. Vol. 1. Amsterdam: KIT Publushers/IDRC/AFRO; 2003

1.  Varkevisser CM, Pathmanathan I, Brownlee AT. Designing and conducting health systems research projects. Vol. 2. Amsterdam: KIT Publushers/IDRC/AFRO; 2003

## **Research-II**

### **Basic Epidemiology - What / Who/ When/ Where/ Why/ How**
**Reading List:**

#### **Historical context of epidemiology – the John Snow case study**
**Reading List:**
1.  John Snow and Cholera – A case study. Adapted for Bangalore-CHLP by Adithya Pradyumna

#### **Descriptive and analytical epidemiology; Populations, Exposures, Outcomes, Controls, Interventions**

1.  Varkevisser CM, Pathmanathan I, Brownlee AT. Designing and conducting health systems research projects. Vol. 1. Amsterdam: KIT Publushers/IDRC/AFRO; 2003
2.  Varkevisser CM, Pathmanathan I, Brownlee AT. Designing and conducting health systems research projects. Vol. 2. Amsterdam: KIT Publushers/IDRC/AFRO; 2003

### **Epidemiological perspective and understanding data**
**Reading List:**

### **Epidemiology in community health practice**
**Reading List:**'

### **Steps in Research**

1.  Varkevisser CM, Pathmanathan I, Brownlee AT. Designing and conducting health systems research projects. Vol. 1. Amsterdam: KIT Publushers/IDRC/AFRO; 2003
2.  Varkevisser CM, Pathmanathan I, Brownlee AT. Designing and conducting health systems research projects. Vol. 2. Amsterdam: KIT Publushers/IDRC/AFRO; 2003

#### **Understanding steps of research**

**Approach to Research in Community Health – a note for fellows taking up a research study as part of the CHLP Fellowship**

1.  Allende, J. (2012). Rigor – The essence of scientific work. Electronic Journal of Biotechnology, 7(1).

2.  People’s Health Movement. (2005). Research for People’s Health: A Researcher’s Encounter.

### **Research Ethics**

1.  Research for People’s Health – A Researcher’s Encounter at the Second People’s HealthAssembly of the People’s Health Movement, Cuenca, Equador, 2005

2. Ethical guidelines for Social Science Research in Health by National Committee For Ethics In Social Science Research In Health (NCESSRH), November 2000

3.  Ethical Guidelines for Biomedical Research on Human Subjects, ICMR, 2000

4.  The Tuskegee Syphilis Trial 1932-1972

5.  Nuremberg Code 1946

6.  Helsinki Declaration of 1964

7.  The Belmont Report 1979

## **Research- III**

### **Qualitative Methods in research**

1.  Varkevisser CM, Pathmanathan I, Brownlee AT. Designing and conducting health systems research projects. Vol. 1. Amsterdam: KIT Publushers/IDRC/AFRO; 2003
2.  Varkevisser CM, Pathmanathan I, Brownlee AT. Designing and conducting health systems research projects. Vol. 2. Amsterdam: KIT Publushers/IDRC/AFRO; 2003 3. Mack N, Woodsong C, MacQueen KM, Guest G, Namey E. Qualitative research methods: a data collectors field guide. North Carolina: Family Health International; 2005

### **Quantitative Methods in research**

1.  Varkevisser CM, Pathmanathan I, Brownlee AT. Designing and conducting health systems research projects. Vol. 1. Amsterdam: KIT Publushers/IDRC/AFRO; 2003
2.  Varkevisser CM, Pathmanathan I, Brownlee AT. Designing and conducting health systems research projects. Vol. 2. Amsterdam: KIT Publushers/IDRC/AFRO; 2003.

## **Research IV**

### **Participatory Action Research**

1.  Loewenson R, Laurell AC, Hogstedt C, D’Ambruoso L, Shroff Z. Participatory action research in health systems: a methods reader. TARSC, AHPSR, WHO, IDRC Canada, EQUINET, Harare; 2014

### **Knowledge Translation and Advocacy**

1.  1\. Varkevisser CM, Pathmanathan I, Brownlee AT. Designing and conducting health systems research projects. Vol. 2. Amsterdam: KIT Publishers/IDRC/AFRO; 2003

## **Health Technology and Innovation**

### **Understanding rational drug policy and prescription, pharmaceutical policy**

1.  Rational Medical Care in Confronting Commercialization of Health Care. The People’s Health Sourcebook. The National Coordination Committee, Jan Swasthya Abhiyan. New Delhi: 2004

2.  Medical Ethics, Medical Education and Health Care in Confronting Commercialization of Health Care. The People’s Health Sourcebook. The National Coordination Committee, Jan Swasthya bhiyan. New Delhi: 2004

### **Immunization challenges, policy and action**
**Reading List:**

### **Appropriate Technology and innovation**

1.  Appropriate Technology for Primary Health Care. Proceedings of the National Workshop on Appropriate Technology for Primary Health Care held at the Indian Council of Medical Research, New Delhi: 1981

2. New Technologies in Public Health – Who pays and who benefits? The National Coordination Committee, Jan Swasthya Abhiyan. 2007

## **Situation Analysis Of Health And Health Care In India**

### **Situation Analysis of Health and Health Determinants in India**

1.  DLHS, NFHS, AHS fact sheets

### **Regional Disparities and inequalities, trends**

1.  Kurian, NJ (2007). ‘Widening Economic and Social disparities: Implications for India’, Indian Journal of Medical Research’, 126, October 2007, pp 374-380
2.  Government of Karnataka (2001). ‘Study on health and health disparities in Health and Health care services Karnataka’ in ‘Karnataka towards Equity, Quality and Integrity in Health - Final report of the Task Force on Health and Family Welfare’, Bangalore, April 2001
3.  Braveman, P. (2006). &#39;Health Disparities and Health Equity: Concepts and Measurement. Annual review of Public Health, 2006; 27; pp167-94

### **Understanding Health care sectors - public, private, voluntary tradition, people sector and local health tradition – strengthens / weaknesses**

1.  Private Health Sector in India: A Critical Review.The People&#39;s Health Resource Book.pp309-330

## **Building Blocks for Fellowship – Learning Together**

### **Understanding oneself - intra personal and Inter personal skills Inside learning, outside learning,** 
**Reading List:** 

### **learning skills, social skills and self-learning**

1. John Staley. Looking Inwards in People in Development.

**What is Health? Physical, Mental, Social, Economic, Political, Cultural, Ecological. Differentiating Health and Medical Paradigms**

1.  Emerging Concepts and Paradigms. Social Justice in Health Multiple pathways towards Health for ALL. pp41-2

**s. Health Economics**

**Health Equity and Universal Health Coverage**

1. Kurian, NJ (2007). ‘Widening Economic and Social disparities: Implications for India’, Indian Journal of Medical Research’, 126, October 2007, pp 374-380

2. Government of Karnataka (2001). ‘Study on health and health disparities in Health and Health care services Karnataka’ in ‘Karnataka towards Equity, Quality and Integrity in Health - Final report of the Task Force on Health and Family Welfare’, Bangalore, April 2001

**Basics of Health Economics including Health Financing, Budget analysis**

1. What Every Indian Should Know About the Financing of Health Care in India. The People's Health Source Book.pp37-45

2. Anexure 1 and 2.The People's Health Source Book.pp358-361

**Movies:**

1. Sick around the world

2. Sicko

**Community Financing and Insurance**

1. Devadasan N, Ranson K, Van Damme W, Acharya A, Criel B. The landscape of community health insurance in India: An overview based on 10 case studies. Health Policy 2006; 78\_224-34

## **Communicable Diseases – Community Health Responses**

### **TB/HIV/AIDS**

1.  The Rediscovery of Tuberculosis in Case Study: Strategies for Tuberculosis Control. The People' s Health Resource Book.pp123-126

2.  Educational approaches in Tuberculosis control: Building on the Social Paradigm, Tuberculosis: An interdisciplinary perspective, Thelma Narayan and Ravi Narayan,Editors: John D. H. Porter and John M. Grange, Imperial College Press, London, 1999. P 489-509

3.  Alternative Approaches for Tubercuosis Control in Case Study: Strategies for Tuberculosis Control The People&#39;s Health Resource Book.pp126-128

**Movies:**

1. Ananta - a film on AIDS

2. TB / HIV - the dual epidemic

3. Sankalp ESI project

4. Saheli - a friend in need

## **Water Borne Diseases**

1.  The Community Health Paradigm in Diarrhoeal Diseases Control by Dr. Ravi Narayan. Chapter selected from the book ‘Diarrhoeal Diseases: Current Status, Research Trends and Field Studies. Edited by D. Raghunath and R Nayak. The Third Sir Dorabji Tata Symposium, Bangalore. P299-304

**Vector borne diseases – Malaria, Dengue, Filaria and other diseases**

1. Case Study: The War Against Malaria. The People&#39;s Health Resource Book.pp110-118

Movies:

1.  Fighting Malaria
2.  Manmade malaria
3.  Community participation in malaria control
4.  Antibiotic Resistance for Idiots

## **Health Systems and Health Policy**

**Health systems and health policy in India - history and evolution**

1. Introduction. The People&#39;s Health Source Book.pp264-272

2. The Health Care System. Rakku’s Story Structures of Ill-health and the Source of Change.pp80-98

### **Health system at different levels – local, district, state, central**

1. Indian Public Health Standards

### **Issues of access acceptability, affordability, availability, quality**

1.  Dilip T. Extent of inequity in access to health care services in India. Rev Health Care India. 2005;247–68. (for data presentation)

2.  Jacobs B, Ir P, Bigdeli M, Annear PL, Van Damme W. Addressing access barriers to health services: an analytical framework for selecting appropriate interventions in low-income Asian countries. Health Policy and Planning. 2012;27(4):288–300.

3.  The Economic Base of Ill=Health. Rakku&#39;s Story Structures of Ill-health and the Source of Change.pp47-59

**Movies**

1.  Ramakka’s Story

## **Understanding Community/ Society / Development And Health**

### **What is community, society, family, collective, cooperative**

1.  Chapter 3 of Community Tool Box: Section 2. Understanding and Describing the Community
(http://ctb.ku.edu/en/table-of-contents/assessment/assessing-community-needs-and-resources/describe-the-community/main)

2.  Community Organisation in Community Health in India. Community Health In Search of Alternate Processes.pp25-26

3.  Community Participation in Community Health in India. Community Health In Search of Alternate Processes.pp25

### **Class, caste, gender, social exclusion, marginalization**
**Reading List:** 

### **Caste and Social exclusion**
**Reading List:**  
1.  Nayar, K. R., &amp; others. (2007). Social exclusion, caste &amp; health: a review based on the social determinants framework. Indian Journal of Medical Research, 126(4), 355.
2.  A World Where We Matter! The People&#39;s Health Source Book.pp198-262.

**Movies:**

1.  Lesser Human

**Gender and Health**

1.  Engendering health by Dr. Sundari Ravindran –seminar web collection

2.  gendered vulnerabilities: women’s health and access to healthcare in India- Ms. Manasee Mishra, Ph.d, The Centre for Enquiry into Health and Allied Themes (CEHAT),

3. Mumbai quarterly news letter by Asian Pacific Resource and Research Centre, Malaysia 

4.  Women and Health, Today evidence-Tomorrows agenda , executive summary of WHO report

5. Womens Health. The People's Health Resource Book.pp203-224

### **Structures, stratification, power dynamics, conflicts, transitions**
**Reading List:** 

### **Understanding dalit issue; adivasi issue; agrarian distress**
**Reading List:**

1.  The Marginalised and the Vulnerable. The People&#39;s Health Resource Book.pp198-199
2.  Health for Dalit Communities. Defending the Health of the Marginalised. pp7-20

## **Nutrition and Women and Children’s Health**
**Reading List:**

### **Understanding Nutrition &amp; Food Security**
**Reading List:**
1.  Globalisation and Food Security. The People&#39;s Health Source Book.pp53-60

2.  Supplementary Feeding and Early Childhood Care in Her Name is Today. The People’s Health Source Book.pp241-43

3. Recommendations on ICDS in Her Name is Today. The People&#39;s Health Source Book.pp247 Dev SM, Sharma AN. Food security in India: Performance, challenges and policies. 2010;

4.  Gross R, Schoeneberger H, Pfeifer H, Preuss H. The four dimensions of food and nutrition security: definitions and concepts. SCN News. 2000;20:20–5

**Movies:**

1.  Assassination - Starvation deaths and rights to food campaign in India

## **Understanding Women’s Health (beyond RCH)**

1.  Women&#39;s Health. The People&#39;s Health Resource Book.pp203-224

**Movies:**

1.  Abortion &quot;MTP Act&quot;
2.  Antenatal clinic (ANC) at Health Post (SHC)
3.  Burnt not defeated
4.  Count down maternal deaths: A documentary film on maternal health
5.  Fine imbalance - Sex determination
6.  How to organize maternity wing at peripheral hospital (PHC/CHC
7.  India&#39;s daughter
8.  The Holy Wives
9.  Traditional Birth Attendant - Training

## **Understanding child health**

1.  Her Name is Today. The People&#39;s Health Resource Book.
2.  And they call us children!. The People&#39;s Health Resource Book

## **Non-Communicable Diseases – Community Health Responses**
**Reading List:**

### **Heart Disease/ Stroke/ Diabetes**

1.  Schmidt LA, Mäkelä P, Rehm J, Room R. Alcohol: equity and social determinants. Equity SocDeterminants Public Health Programme. 2010;11.

2.  Mendis S, Banerjee A. Cardiovascular disease: equity and social determinants. Equity Soc Determinants Public Health Programme. 2010;31.

3.  Whiting D, Unwin N, Roglic G. Diabetes: equity and social determinants. Equity Soc Determinants Public Health Programme. 2010;77.

## **Mental Health and Substance abuse**
**Reading List:**

### **An introduction to Mental Health, Mental Illness, and to Community Mental Health and Development**
**Reading List:**
1.  Patel V, Lund C, Hatherill S, Plagerson S, Corrigall J, Funk M, et al. Mental disorders: equity and social determinants. Equity Soc Determinants Public Health Programme. 2010;115.

**Movies:**

1.  Samatha
2.  Exploring madness series
3.  Reshma’s ‘A Drop of Sunshine’
4.  Perspectives on Mental Health

### **Substance Abuse**

1.  David A, Esson K, Perucic A-M, Fitzpatrick C. Tobacco use: equity and social determinants. Equity Soc Determinants Public Health Programme. 2010;199.

Movies

1.  India Inhales
2.  Safer Healthier Alternatives to Tobacco
3.  Tobacco the Devil

### **Cancer/ Accidents etc**

1.  Roberts H, Meddings D. Violence and unintentional injury: equity and social determinants. Equity Soc Determinants Public Health Programme. 2010;243.

### **Risk reduction, life style change, prevention and promotion**

Movies:

1.  The 6th Global Conference on Health promotion 7-11 Aug. 2005