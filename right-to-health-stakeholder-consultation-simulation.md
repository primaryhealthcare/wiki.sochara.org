---
title: Right to Health Stakeholder Consultation Simulation
description: 
published: true
date: 2023-07-02T03:38:10.195Z
tags: 
editor: markdown
dateCreated: 2023-06-29T15:19:50.570Z
---

# Right To Health Stakeholder Consultation Simulation

This is a mock Parliamentary consultation held by a multi-party committee of MPs in charge of looking at the legislation of a "Right to Health" based on the Rajasthan state's Right to Healthcare act.

Number of participants: 10-50

Duration:
* Preparatory phase (1-2 days)
* Roundtable for stakeholder consultation (90 - 180 minutes)
* Debrief (90-180 minutes)

Medium: Online or offline

### Format of the activity

For organizational convenience the group of participants can be divided into two.
* One group will consist of stakeholders who are mostly in favor of the right to health.
* The other group will consist of stakeholders who are mostly against the right to health.

The division of people can be voluntary or based on random allotment.

## Arguments list

You can find various arguments in:

* https://www.livemint.com/Opinion/BzI9QijMRAv5MkiKqj0Y3M/Should-India-make-health-a-fundamental-right.html
* https://www.thehindu.com/podcast/what-does-the-right-to-health-bill-really-mean-for-rajasthan-in-focus-podcast/article66706736.ece
* https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1126951/
* https://indianexpress.com/article/explained/explained-health/doctors-vs-rajasthans-right-to-health-bill-8524518/
* https://www.mfcindia.org/mfcpdfs/MFC262-263.pdf (article titled advocacy for right to health care)
* https://www.mfcindia.org/mfcpdfs/MFC308.pdf