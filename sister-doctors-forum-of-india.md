---
title: Sister Doctors Forum of India
description: 
published: true
date: 2023-01-19T06:48:31.007Z
tags: 
editor: markdown
dateCreated: 2023-01-19T06:48:31.007Z
---

# Sister Doctors Forum of India

Website - https://www.sisterdoctorsindia.org/

As per the website


> 
> The Sister Doctors Forum was founded on 5th June, 1993 during the National Convention of CHAI in Ernakulam.  Sr. Dr. Lilian JMJ, was the first President of the forum .  In a span of 25 years, the forum has grown under the able leadership of Sr. Dr. Lillian JMJ, Sr. Dr. Hermina SSA, Sr. Dr. Lucian SCC. On 4th February, 2009, the SDFI was formally registered as a Trust in New Delhi.  In the initial years, SDFI had its office in the premises of St. John’s Medical College, Bangalore.  Thereafter, the office was shifted to the headquarters of CHAI in Secunderabad.  However, presently, the functioning office is at Malur, Bangalore. For better co-ordination, SDFI has divided itself to eight regions – Northern, Central, Eastern, Western, Andhra Pradesh and Telangana, Karnataka, Tamil Nadu and Kerala.
> 
> There are about 1000 Sister Doctors in India today. A majority of the Sister Doctors have dedicated their lives in the service of the unreached and less reached of our country.  They provide preventive, curative, promotive and palliative care service and thus contribute a lot to restore health and wholeness in rural, tribal and remote areas of our country.
> 
> Presently, SDFI organizes various activities at the Regional and National level like Save the Girl Child campaign, Cancer Prevention in Women through Screening and Awareness Creation program, Anemia control and treatment program, Mother and Child Welfare program, and activities for Prevention of life style associated diseases etc. Few Sister Doctors are also actively participating in the National Health program like RNTCP and NACO. Besides this, CME programs are organized at Regional and National levels.
> 
> We have been in the forefront during natural calamities like earthquake, floods, the Tsunami etc. We also work in collaboration with other organizations like CBCI health Commission, CHAI, Catholic Medical Mission Board, Catholic relief Services, Caritas India and other likeminded organizations.
> 