---
title: ಪ್ರೀತಿಯ ಗಾಳಿ ಬೀಸುತಿದೆ
description: 
published: true
date: 2024-06-16T07:24:35.896Z
tags: 
editor: markdown
dateCreated: 2023-06-16T05:14:58.936Z
---

# ಪ್ರೀತಿಯ ಗಾಳಿ ಬೀಸುತಿದೆ
ಪ್ರೀತಿಯ ಗಾಳಿ ಬೀಸುತಿದೆ
ಹಟ್ಟಿ ಮೊಹಲ್ಲಗಳ ನಡುವೆ
ಗಡಿಗಳ ಮೀರಿ ಗೆಳೆತನವ ಮಾಡೋಣವಾ
ನನ್ನ ಸಂಗಾತಿಯೇ ಓ ನನ್ನ ಸಂಗಾತಿಯೇ

ಸ್ಪೃಶ್ಯ ಅಸ್ಪೃಶ್ಯತೆ ಬೇಲಿಗಳು
ಜಾತಿ ಧರ್ಮದ ಗೋಡೆಗಳು
ಎಲ್ಲವ ದಾಟಿ ಎತ್ತರಕೆ ಹಾರೋಣ ಬಾ
ನನ್ನ ಸಂಗಾತಿಯೇ ಓ ನನ್ನ ಸಂಗಾತಿಯೇ

ಅವ್ವನ ಪ್ರೀತಿಗೆ ಕೈಮುಗಿದು
ಅಪ್ಪನ ಮಮತೆಗೆ ಶರಣೆಂದು
ಹೊಸಿಲ ದಾಟಿ ಬಾ ಹೊಸ ಸಂಬಂಧ ಕಟ್ಟೋಣ ಬಾ
ನನ್ನ ಸಂಗಾತಿಯೇ ಓ ನನ್ನ ಸಂಗಾತಿಯೇ

ನನ್ನ ನಿನ್ನ ಈ ಸ್ನೇಹ
ಒಪ್ಪದಿರಬಹುದು ಸಮಾಜ
ತಪ್ಪೆಂದಾದರೆ ಆಗಲಿ ತಪ್ಪ ಮಾಡೋಣ ಬಾ
ನನ್ನ ಸಂಗಾತಿಯೇ ಓ ನನ್ನ ಸಂಗಾತಿಯೇ

ನೆನ್ನೆಯ ತಪ್ಪು ಇಂದು ಸರಿ
ಇಂದಿನ ತಪ್ಪು ನಾಳೆ ಸರಿ
ಕಾಲದೇಶಕೆ ಬದಲಾಗುವುದು ತಪ್ಪು ಸರಿ
ನನ್ನ ಸಂಗಾತಿಯೇ ಓ ನನ್ನ ಸಂಗಾತಿಯೇ

ಬದಲಾವಣೆಯಲಿ ನೋವು ಇದೆ
ಮೌಲ್ಯಗಳ ಸಂಘರ್ಷವಿದೆ
ಇಂದಲ್ಲ ನಾಳೆ ಬದಲಾಗುವುದು ಜನ ಮನ
ನನ್ನ ಸಂಗಾತಿಯೇ ಓ ನನ್ನ ಸಂಗಾತಿಯೇ

ಬದಲಾಗದಿದ್ದರೆ ನಾವಿಂದು
ಜಗವ ಬದಲಾಯಿಸುವೆವು ಎಂದು
ಬದಲಾವಣೆಯು ಶುರುವಾಗುವುದು ನಮ್ಮಿಂದಲೇ
ನನ್ನ ಸಂಗಾತಿಯೇ ಓ ನನ್ನ ಸಂಗಾತಿಯೇ

## English 

prithiya gaaLi bisuthide 
prithiya gaaLi bisuthide 
haTTi mohallagaLa naDuve 
gaDigaLa miri geLethanava 
maaDoNavaa 
nanna samgaathiye 
oa nanna samgaathiye 

sprrishya asprrishyathe beligaLu 
jaathi dharmada goDegaLu 
ellava daaTi eththarake 
haaroNa baa 
nanna samgaathiye 
oa nanna samgaathiye 

avvana prithige kaimugidu 
appana mamathege sharaNemdu hosila daaTi baa
hosa sambamdha kaTToNa baa 
nanna samgaathiye 
oa nanna samgaathiye 

nanna ninna ia sneha 
oppadirabahudu samaaja 
thappemdaadare aagali thappa 
maaDoNa baa
nanna samgaathiye 
oa nanna samgaathiye 

nenneya thappu iamdu sari 
iamdina thappu naaLe sari 
kaaladeshake badalaaguvudu thappu sari 
nanna samgaathiye 
oa nanna samgaathiye 

badalaavaNeyali novu ide 
maulyagaLa samgharshhavide 
iamdalla naaLe badalaaguvudu jana mana 
nanna samgaathiye 
oa nanna samgaathiye 

badalaagadiddare naavimdu jagava 
badalaayisuvevu eamdu 
badalaavaNeyu shuruvaaguvudu nammimdale 
nanna samgaathiye 
oa nanna samgaathiye 
