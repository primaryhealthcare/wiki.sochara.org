---
title: Azam Khan
description: 
published: true
date: 2023-10-30T05:51:59.098Z
tags: 
editor: markdown
dateCreated: 2023-10-30T05:51:59.098Z
---

# Azam Khan

Azam Khan is an lawyer working in community health with SOCHARA.

## Education

Azam Khan's CHLP report can be seen [here](https://archives.sochara.org/s/communityhealth/item/458).