---
title: Uncategorized
description: 
published: true
date: 2023-01-06T11:05:46.160Z
tags: 
editor: markdown
dateCreated: 2023-01-06T06:24:30.228Z
---

# Some notes


Dr Ramalingaswamy asked ICMR and ICSSR to try and internalize Alma Ata into internal health systems thinking.

JP Naik was the only non-doctor in the committee (because he was heading the ICSSR).

The report by them is called Health for All - An Alternative Strategy.

Last paragraph before VI appendices.

SEPCE analysis and communitization comes from these.

ICMR saying these makes it more relevant.