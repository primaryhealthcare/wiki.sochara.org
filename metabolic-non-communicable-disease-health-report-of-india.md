---
title: Metabolic non-communicable disease health report of India: the ICMR-INDIAB national cross-sectional study (ICMR-INDIAB-17)
description: 
published: true
date: 2023-06-23T09:26:20.846Z
tags: 
editor: markdown
dateCreated: 2023-06-23T09:26:20.846Z
---

# Metabolic non-communicable disease health report of India: the ICMR-INDIAB national cross-sectional study (ICMR-INDIAB-17)

Original: https://www.thelancet.com/journals/landia/article/PIIS2213-8587(23)00119-5/fulltext

## Summary

### Background
Non-communicable disease (NCD) rates are rapidly increasing in India with wide regional variations. We aimed to quantify the prevalence of metabolic NCDs in India and analyse interstate and inter-regional variations.
### Methods
The Indian Council of Medical Research–India Diabetes (ICMR-INDIAB) study, a cross-sectional population-based survey, assessed a representative sample of individuals aged 20 years and older drawn from urban and rural areas of 31 states, union territories, and the National Capital Territory of India. We conducted the survey in multiple phases with a stratified multistage sampling design, using three-level stratification based on geography, population size, and socioeconomic status of each state. Diabetes and prediabetes were diagnosed using the WHO criteria, hypertension using the Eighth Joint National Committee guidelines, obesity (generalised and abdominal) using the WHO Asia Pacific guidelines, and dyslipidaemia using the National Cholesterol Education Program—Adult Treatment Panel III guidelines.
### Findings
A total of 113 043 individuals (79 506 from rural areas and 33 537 from urban areas) participated in the ICMR-INDIAB study between Oct 18, 2008 and Dec 17, 2020. The overall weighted prevalence of diabetes was 11·4% (95% CI 10·2–12·5; 10 151 of 107 119 individuals), prediabetes 15·3% (13·9–16·6; 15 496 of 107 119 individuals), hypertension 35·5% (33·8–37·3; 35 172 of 111 439 individuals), generalised obesity 28·6% (26·9–30·3; 29 861 of 110 368 individuals), abdominal obesity 39·5% (37·7–41·4; 40 121 of 108 665 individuals), and dyslipidaemia 81·2% (77·9–84·5; 14 895 of 18 492 of 25 647). All metabolic NCDs except prediabetes were more frequent in urban than rural areas. In many states with a lower human development index, the ratio of diabetes to prediabetes was less than 1.
### Interpretation
The prevalence of diabetes and other metabolic NCDs in India is considerably higher than previously estimated. While the diabetes epidemic is stabilising in the more developed states of the country, it is still increasing in most other states. Thus, there are serious implications for the nation, warranting urgent state-specific policies and interventions to arrest the rapidly rising epidemic of metabolic NCDs in India.
### Funding
Indian Council of Medical Research and Department of Health Research, Ministry of Health and Family Welfare, Government of India.

## Simplified by ChatGPT

### Introduction

South Asia has a lot of people and is going through changes in terms of health issues. More and more people in this region are getting diseases that are not easily passed from one person to another. In South Asia, India is the biggest country and has the highest number of these diseases. Many studies have been done in the past 20 years that show India has a large number of people with diabetes, high blood pressure, and high cholesterol. However, most of these studies relied on people reporting their own conditions, didn't use reliable methods to diagnose the diseases, or looked at data from surveys that weren't specifically about these diseases. They also didn't represent the whole population of the country. Additionally, these studies didn't provide information about the number of people who have prediabetes, which makes it hard to understand the current situation and predict what will happen in the future. Different regions and states in India have different ethnic backgrounds, eating habits, and levels of development, so overall estimates for the whole country might hide big differences between regions and even within regions. In India, each state is responsible for providing healthcare, so it's really important to have detailed information about each state to help them plan and carry out programs to prevent and manage these diseases. This is especially important because recent evidence shows that more people in low-income and middle-income countries like India are dying or getting sick from these diseases compared to wealthier countries.

The ICMR-INDIAB study is the biggest survey on diabetes and other health problems in India. It looked at all 28 states, two union territories, and the National Capital Territory of Delhi. In this study, researchers analyzed the data to find out how many adults in urban and rural areas have metabolic health issues like diabetes, high blood pressure, obesity, and high cholesterol. They also wanted to see if there were any differences between regions and states in the country.

#### Existing research:

Before this study, we looked at various sources like PubMed, Google Scholar, and the Cochrane Database of Systematic Reviews to find studies published before April 10, 2023, that talked about the number of non-communicable diseases (NCDs) among Asian Indians. We used specific keywords related to diabetes, high blood pressure, obesity, high cholesterol, and the Indian population. We focused on studies written in English. We found that there is a high and increasing prevalence of these diseases in India. However, many of the studies we found relied on people self-reporting their conditions, didn't use reliable methods to diagnose the diseases, or were secondary analyses of data from surveys that weren't specifically about NCDs. Also, many of these studies didn't represent the whole population of India, so it's hard to understand the differences between regions and how applicable the results are to the entire country.

#### Value of this study:

The current report is from the Indian Council of Medical Research–India Diabetes (ICMR-INDIAB) study, which is the largest study on diabetes and metabolic NCDs in India. It includes data from all 28 states, two union territories, and the National Capital Territory of Delhi, making it nationally representative. This study shows that the prevalence of metabolic NCDs in India is even higher than what was previously estimated. It provides information on the number of people with diabetes and prediabetes in different states of India, using reliable sampling and diagnostic methods. This helps us understand the current situation of the diabetes epidemic and predict its future. The study also shows that some states have already reached the peak of the diabetes epidemic, while others are still in the early stages. It reveals that the rates of diabetes, high blood pressure, obesity, and high cholesterol are higher in urban areas, but the rates in rural areas are much higher than what was previously reported.

#### Implications of the evidence:

There is a significant number of people in India who are at risk of developing heart disease and other long-term health problems due to metabolic NCDs. This is expected to become a major public health challenge in the near future. The study also shows that the NCD epidemic is spreading to rural areas, which lack the necessary healthcare resources to effectively diagnose and manage these conditions. This calls for urgent action from the government to prevent and manage NCDs by strengthening the public healthcare system and prioritizing healthcare services. The detailed state-level data on these NCDs will be particularly useful for state governments in India, as they are responsible for healthcare in their regions. It will help them design interventions based on evidence to control the progress of NCDs and manage their complications effectively.


### Methods

#### Sampling

The ICMR-INDIAB study is a survey that looked at adults aged 20 years and older. They used a specific method to select people from urban and rural areas in 31 states and union territories of India. They wanted to make sure the sample represented the whole population, so they divided it based on geography, population size, and socioeconomic status. They selected households and then chose one person from each household using a fair method. The study took place between November 2008 and December 2020, in different phases, covering different regions of the country. They excluded people who were temporarily away or very sick. The study was approved by ethics committees and they got consent from all participants.

#### Procedures

During the study, the researchers collected information from the participants using a standardized questionnaire. They also measured weight, height, waist circumference, and blood pressure to calculate BMI. For measuring blood glucose levels, they used a glucose meter after ensuring that the participants had fasted for at least 8 hours. In participants without a previous diabetes diagnosis, an oral glucose tolerance test was conducted, while those with self-reported diabetes only had their fasting blood glucose measured.

Additionally, a venous blood sample was taken from individuals with diabetes and every fifth individual for assessing HbA1c (a measure of long-term blood sugar control) and lipids (such as cholesterol). The samples were processed and stored in freezers until they were transferred to the central laboratory. The laboratory technicians used standardized methods for conducting all the biochemical tests throughout the study period.

HbA1c was estimated using a machine called Variant II Turbo, which is certified for accuracy. Serum cholesterol, triglycerides, and high-density lipoprotein cholesterol were measured using automated biochemistry analyzers.

(For simplicity, I have omitted specific technical details of the methods used.)

#### Outcomes

The study focused on several health outcomes of interest, including diabetes, prediabetes, dysglycaemia (abnormal blood sugar levels), hypertension (high blood pressure), generalised obesity, abdominal obesity, and dyslipidaemia (abnormal lipid levels). The researchers also made projections for the prevalence of these conditions in the year 2021.

To determine the presence of diabetes, individuals were considered to have self-reported or known diabetes if they had received a physician's diagnosis or were currently taking medications for diabetes. Medical reports and prescriptions were used to verify the diagnosis and determine the year of diagnosis. For those without a self-reported diagnosis, diabetes and prediabetes were diagnosed using oral glucose tolerance tests (OGTT), HbA1c measurements, or a combination of both.

Prediabetes was defined as having impaired fasting glucose (IFG), impaired glucose tolerance (IGT), or both. Dysglycaemia referred to the presence of prediabetes or diabetes.

Different criteria were used to define these conditions. For example, diabetes was determined based on fasting blood glucose levels of 126 mg/dL (7·0 mmol/L) or higher, or 2-hour post-oral glucose load blood glucose levels of 220 mg/dL (12·2 mmol/L) or higher, as recommended by the WHO consultation group. Isolated IFG and isolated IGT were diagnosed based on specific blood glucose ranges.

The researchers also assessed IFG using the criteria set by the American Diabetes Association (ADA), and diabetes and prediabetes were evaluated using HbA1c cutoffs. Generalised obesity was defined as having a body mass index (BMI) of 25 kg/m² or higher, while abdominal obesity was determined based on waist circumference measurements using guidelines from the WHO Asia Pacific region. Hypertension was defined as having systolic blood pressure of 140 mm Hg or higher, diastolic blood pressure of 90 mm Hg or higher, or receiving antihypertensive medication.

Dyslipidaemia was evaluated using criteria from the National Cholesterol Education Programme—Adult Treatment Panel III, which considered factors such as serum cholesterol, triglyceride, HDL cholesterol, and LDL cholesterol levels.

The study conducted various biochemical assays at a central laboratory, and the coefficients of variation for these assays ranged from 3.1% to 7.6%, indicating the level of precision in the measurements.

#### Statistical analysis
The researchers determined the sample size separately for urban and rural areas based on previous studies that showed differences in the prevalence of diabetes between these areas. Assuming a prevalence of 10% in urban areas and 4% in rural areas, with a margin of error of 20% and a non-response rate of 20%, the estimated sample size was 1200 in urban areas and 2800 in rural areas for each state studied. This resulted in a total of 4000 individuals per state.

To account for the complex survey design, sampling weights were calculated to adjust for different sampling levels within each state. The statistical analysis used the "proc survey" procedure, which considers factors like stratification, clustering, and weighting in multistage survey designs.

For subgroup analysis, the Indian states were divided into six geographical zones: north, south, east, west, central, and northeast. This division helped in comparing variables between different groups.

To standardize estimates according to age, sex, and region and obtain current prevalence, the researchers used information from the National Family Health Survey-5 (NFHS-5). This standardization accounted for population aging and demographic trends. Similar assumptions were made by the International Diabetes Federation for future projections.

NFHS-5 study weights were computed for subgroups defined by region, sex, and age, and these weights were multiplied by the weighted prevalence in each subgroup to calculate the prevalence of cardiometabolic risk factors in each state.

The estimated adult population of India for the year 2021 was obtained from population projections published by the Ministry of Health and Family Welfare. Projection estimates for diabetes, prediabetes, dysglycaemia, hypertension, obesity, and dyslipidaemia were calculated based on the standardized prevalence and the projected population.

#### Role of Funding Source

The study was funded by the Indian Council of Medical Research and the Department of Health Research, Ministry of Health and Family Welfare. The funding source provided scientific inputs, quality control, and assistance in revising the manuscript.

### Results

A total of 119,022 individuals from 31 states and union territories were assessed for eligibility in the study. Out of those, 113,043 individuals participated in the study, with 33,537 from urban areas and 79,506 from rural areas, resulting in a response rate of 95%. Around 5,979 individuals were not included in the study because they either refused to participate, were not available, or their houses were locked during the survey.

For the analysis of various cardiometabolic risk factors, data from the following numbers of individuals were included: diabetes or prediabetes - 107,119; hypertension - 111,439; generalised obesity - 110,368; abdominal obesity - 108,665; and dyslipidaemia - 18,492 (available only for every fifth participant).

The average age of the 113,043 individuals studied was 43 years. Among them, 52,602 (46.5%) were male, and 30,119 (26.9%) had no formal education. When comparing urban and rural residents, urban residents were found to be younger and had higher BMI, waist circumference, diastolic blood pressure, and educational attainment. Urban residents also had higher levels of fasting blood glucose, total cholesterol, and LDL cholesterol compared to rural residents. Males, on the other hand, were older, had higher education levels, lower BMI, but higher blood pressure, triglycerides, and HbA1c compared to females.

The study found that the overall prevalence of diabetes was 11.4% among the individuals assessed. The prevalence was significantly higher in urban areas compared to rural areas, as well as among males compared to females. Different methods of diagnosis, such as HbA1c and OGTT, yielded varying prevalence rates for diabetes.

For impaired fasting glucose (IFG) and impaired glucose tolerance (IGT), the prevalence rates were 10.1% and 3.3% respectively among the individuals assessed. IFG was more prevalent among females, while IGT was more prevalent among males. The overall prevalence of prediabetes, which includes IFG and IGT, was 15.3%.

When considering different criteria for diagnosis, the prevalence of IFG increased to 27.6% when using the ADA criteria. Similarly, the overall prevalence of prediabetes increased to 32.8% when using the ADA criteria.

The prevalence of hypertension was 35.5% among the individuals assessed, and it was higher in urban areas and among males. Using different criteria for diagnosis, the prevalence of hypertension nearly doubled to 66.3% when using the ACC/AHA criteria.

Generalized obesity was present in 28.6% of the population, while abdominal obesity was present in 39.5%. Dyslipidemia, characterized by abnormal lipid levels, was found in 81.2% of the individuals assessed, mainly driven by low levels of HDL cholesterol.

The prevalence rates varied across different states in India. Diabetes and prediabetes had higher rates in the southern and northern regions, with higher prevalence in urban areas. The central and northeastern regions had lower rates. The ratio of diabetes to prediabetes varied across states, with higher ratios in states with a high human development index.

The study projected the number of people with cardiometabolic risk factors in 2021. It estimated that 101 million people had diabetes, 136 million had prediabetes, 315 million had hypertension, 254 million had generalized obesity, 351 million had abdominal obesity, 213 million had hypercholesterolemia, and 185 million had high LDL cholesterol.

[See all tables here](https://www.thelancet.com/journals/landia/article/PIIS2213-8587(23)00119-5/fulltext#tables)

[See all figures here](https://www.thelancet.com/journals/landia/article/PIIS2213-8587(23)00119-5/fulltext#figures)

### Discussion

This report is about a study conducted in India that shows the high number of non-communicable diseases (NCDs) in the country. The study found that the prevalence of diabetes and prediabetes is much higher than previously reported figures. The previous report only included data from some states and did not include certain socioeconomically developed states, which may have led to lower reported rates. The current report includes data from all states and is considered to be more accurate.

The study also found that using a combination of tests for diagnosis resulted in a higher number of people being diagnosed with dysglycemia (abnormal blood sugar levels). This is because there is a high prevalence of iron-deficiency anemia in India, which can affect the test results. However, national guidelines do not recommend using one test alone for diagnosis.

The report also highlights variations in the prevalence of diabetes across different states and regions in India, as shown in previous studies. It also mentions the high rates of other metabolic risk factors such as dyslipidemia (abnormal lipid levels), hypertension (high blood pressure), and obesity in the population. Low levels of HDL cholesterol were found to be common among Asian Indians compared to other ethnic groups.

The findings of the study are consistent with other large studies conducted in India, which also reported high rates of diabetes, hypertension, and other risk factors. The high prevalence of these conditions poses a significant burden on healthcare in the country, especially considering the long-term complications associated with diabetes.

Prevention, early diagnosis, and treatment of these metabolic risk factors are crucial for preventing cardiovascular disease and other complications. However, the study found that a low percentage of people with diabetes in India meet treatment targets for blood glucose, lipids, and blood pressure. Therefore, there is a need to prioritize healthcare resources and strengthen the public health system to improve diabetes and NCD care, particularly in states with a high disease burden.

The report emphasizes the importance of state-specific strategies due to variations in the prevalence of diabetes and prediabetes across different states. States with high prevalence need to focus on risk factor control to prevent complications, while states with a lower prevalence should take measures to prevent the future increase in diabetes cases.

The study faced various challenges in implementation, such as manpower, cultural barriers, and logistics. However, these challenges were addressed through collaboration with local communities, translation of study documents, and multiple visits to collect data.

The study has some limitations, including its cross-sectional nature, limitations in differentiating between type 1 and type 2 diabetes, and variations in the timing of data collection. However, it provides valuable insights into the prevalence of metabolic NCDs in India and highlights the need for future research and interventions.

In conclusion, this report emphasizes the significant burden of NCDs, particularly diabetes, in India. It calls for interventions to prevent the progression of prediabetes to diabetes and to provide optimal care for individuals with diabetes. Strengthening the healthcare system and implementing evidence-based approaches are essential steps towards addressing the NCD epidemic in the country.