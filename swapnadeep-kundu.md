---
title: Swapnadeep Kundu
description: 
published: true
date: 2023-08-12T15:48:38.450Z
tags: 
editor: markdown
dateCreated: 2023-08-12T15:42:16.801Z
---

# Swapnadeep Kundu

Swapnadeep Kundu was from Bagula of Nadia district of West Bengal. He was studying Bengali at Jadavpur University.

Swapnadeep was the son of Swapna, an ASHA worker.


## Links

* https://www.telegraphindia.com/my-kolkata/news/tragic-end-to-swapnadeep-kundus-dream-to-study-at-jadavpur-university/cid/1958167
* https://www.telegraphindia.com/my-kolkata/news/before-death-swapnadeep-kundu-first-year-undergraduate-student-to-mom-i-am-very-scared-take-me-with-you/cid/1958164
* https://countercurrents.org/2023/08/toxic-masculinity-and-the-student-suicide-in-jadavpur-university/
* https://thewire.in/rights/jadavpur-university-ragging-death-swapnadeep-kundu