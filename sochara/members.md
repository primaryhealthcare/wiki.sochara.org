---
title: Members
description: 
published: true
date: 2023-07-31T15:58:39.035Z
tags: 
editor: markdown
dateCreated: 2023-07-31T15:52:37.330Z
---

# SOCHARA Society Members

## Executive Committee Members
1. [S. Pruthvish](/s-pruthvish)
2. [Denis Xavier](/denis-xavier)
3. [Maya Mascarenhas](/maya-mascarenhas)
4. [Gurumoorthy M](/gurumoorthy-m)
5. [Adithya Pradyumna](/adithya-pradyumna)
6. [J. Ganthimathi](/j-ganthimathi)
7. [Susanta Ghosh](/susanta-ghosh)

## Ordinary Members

8. Mohan K. Isaac
9. P. Chandra
10. Thelma Narayan
11. Ravi Narayan
12. Sam Joseph
13. Abhijit Sengupta
14. Sunil Kaul
15. M. K. Vasundhra
16. Valli Seshan
17. Ravi D’Souza
18. K. Gopinathan
19. Kishore Murthy
20. H. Sudarshan
21. N. Devadasan
22. Edward Premdas
23. Mani Kalliath
24. Anand Zachariah
25. Neela Patel
26. K. Ravi Kumar
27. Shanmuga Velayudham
28. Arvind Kasturi
29. Aquinas Edassery
30. Anjali Noronha
31. Muraleedharan. VR
32. Priyadarsh
33. Madhukar Pai
34. Prasanna Saligrama
35. Ramani D’Souza
36. Lalit Narayan
37. As Mohammed
38. Ameer Khan
39. Prahlad I M
40. Rahul ASGR


## Honorary Members
1. John Vattamattom
2. D.K. Srinivasa
3. A. Arumugham