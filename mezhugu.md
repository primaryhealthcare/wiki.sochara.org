---
title: Mezhugu (Short Film)
description: 
published: true
date: 2023-01-19T06:56:24.374Z
tags: 
editor: markdown
dateCreated: 2023-01-19T06:53:21.194Z
---

# Mezhugu (Short Film)

Mezhugu is a Tamil short film presented by [SDFI Chennai](/sister-doctors-forum-of-india) directed by Stephen msfs.

[Youtube link](https://youtu.be/515s8fj4FJ4)
