---
title: PHM related discs
description: 
published: true
date: 2023-12-04T14:16:52.177Z
tags: 
editor: markdown
dateCreated: 2023-12-04T09:55:45.449Z
---

# PHM Related Discs

This [youtube playlist](https://www.youtube.com/playlist?list=PLQYNo7poXEuRWZiUPFgWJhh_ymErrJ3gW) contains all the videos uploaded.

## 1

### Identification

| Location | Appearance |
|--|--|
|Sticker on cover | PHM-2005 <br/>II Peoples Health Assembly in Cuenca Ecuador<br/><br/>- Buildup<br/>- Inauguration<br/>- Assembly sessions|
| Cover | \|\| Asamblea Mundial de la Salud de los Pueblos<br/>JULIO - 2005 |
| Disc inner | PB214077563-14011 |

### Contents

10 minutes 14 seconds video. Curtainraiser for PHA-II

[Uploaded to Youtube](https://youtu.be/QCoRV3o5EgI)

## 2

### Identification

| Location | Appearance |
|--|--|
| Sticker on cover | Inauguración: festival de la esperanza y la alegremia |
| Cover | \|\| Asamblea Mundial de la Salud de los Pueblos<br/>JULIO - 2005 |
| Disc inner | PB284080123-07305 |

### Contents

Disc failed to read

## 3

### Identification

| Location | Appearance |
|--|--|
| Sticker on cover | CEREMONIA Y DECLARATORIA DE LOS PUEBLOS |
| Cover | \|\| Asamblea Mundial de la Salud de los Pueblos<br/>JULIO - 2005 |
| Disc inner |  |

### Contents

Partly recovered

## 4 

### Identification

| Location | Appearance |
|--|--|
| Sticker on cover | Plenaria 1: <br/>Unidos por la defensa de la Salud como derecho humano fundamental |
| Cover | \|\| Asamblea Mundial de la Salud de los Pueblos<br/>JULIO - 2005 |
| Disc inner | PB284080123-07260 |

### Contents

11 videos could be recovered

[Uploaded to Youtube](https://www.youtube.com/playlist?list=PLQYNo7poXEuRZqkfGcJoKOtf2dmI1jKn0)


## 5

### Identification

| Location | Appearance |
|--|--|
| Sticker on cover | Plenaria 2: <br/>DENUNCIAMOS Y UNIMOS NEUSTRAS ACCIONES CONTRA LA MILITARIZACION LA OCUPACION MILITAR Y INACEPTABLE SU IMPACTO SOBRE LA SALUD|
| Cover | \|\| Asamblea Mundial de la Salud de los Pueblos<br/>JULIO - 2005 |
| Disc inner | B283412863-09938 |

### Contents
1 hour 22 minutes 13 seconds video recovered

Encoding failed after that

The recovered part is [uploaded to youtube](https://youtu.be/gsYBAmpRHLE)

## 6

### Identification

| Location | Appearance |
|--|--|
| Sticker on cover | Plenaria 3: <br/> RECHAZAMOS LA DEGRADACION AMBIENTAL |
| Cover | \|\| Asamblea Mundial de la Salud de los Pueblos<br/>JULIO - 2005 |
| Disc inner | PB214077563-06883 |

### Contents

Failed to read

## 7

### Identification

| Location | Appearance |
|--|--|
| Sticker on cover | VIOLENCIA SOCIAL POLITICAS Y GENERO|
| Cover | \|\| Asamblea Mundial de la Salud de los Pueblos<br/>JULIO - 2005 |
| Disc inner | B063412814-06877 |

### Contents

7 titles recovered.

[Uploaded to Youtube](https://www.youtube.com/playlist?list=PLQYNo7poXEuQaZfR69rtAQB3XqftVaRCL)

## 8

### Identification

| Location | Appearance |
|--|--|
| Sticker on cover | INFORME ALTERNATIVO MUNDIAL Y LATINOAMERICANO|
| Cover | \|\| Asamblea Mundial de la Salud de los Pueblos<br/>JULIO - 2005 |
| Disc inner | B283412863-05793 |

### Contents

Partly recovered.

[Uploaded to YouTube](https://youtu.be/7W-2rVnfbLM)

## 9 

### Identification
| Location | Appearance |
|--|--|
| Sticker on cover | MILITARIZACION, GUERRA Y OCUPACION |
| Cover | \|\| Asamblea Mundial de la Salud de los Pueblos<br/>JULIO - 2005 |
| Disc inner | PB214077563-04273 |

### Contents

Partly recovered

## 10

### Identification
| Location | Appearance |
|--|--|
| Sticker on cover | LAS VOCES DE LA TIERRA NOS CONVOCAN |
| Cover | \|\| Asamblea Mundial de la Salud de los Pueblos<br/>JULIO - 2005 |
| Disc inner | PB234079013-17183 |

### Contents

Cleanly recovered 7 titles

[Uploaded to YouTube](https://www.youtube.com/playlist?list=PLQYNo7poXEuS-FJUiqtBpRxC_GkVcvOIW)

## 11

### Identification
| Location | Appearance |
|--|--|
| Cover | \|\| Las Voces de La Tierra nos Convocan! <br/>Asamblea Mundial de la Salud de los Pueblos - People's Health Assembly|
| Disc inner | B063412814-13757 |

### Contents

Cleanly copied

[Uploaded to YouTube](https://youtu.be/s_a8EFF4pgA)

## 12

### Identification
| Location | Appearance |
|--|--|
| Cover | \|\| Las Voces de La Tierra nos Convocan! <br/>Asamblea Mundial de la Salud de los Pueblos - People's Health Assembly|
| Disc inner | B063412814-13680 |

### Contents

Cleanly copied

Duplicate of 11 above


## 13

### Identification
| Location | Appearance |
|--|--|
| Sticker on Cover | CPHE/PHM4 10-A PPT |
| Cover | \|\| Asamblea Mundial de la Salud de los Pueblos<br/>People's Health Assembly 2<br/> equipocomunicandonos<br/> powerpoint show|
| Disc inner | EC1021C21C18PA80 |
| Disc pen | People's Health Assembly - II <br/> Arturo - PRESENATION |

### Contents

A couple of PPTs


## 14

### Identification
| Location | Appearance |
|--|--|
| Cover | \|\| multimedia<br/>Un Mundo Saludable es Posible<br/>Memorias<br/>I, II y III For Internacional<br/>"Dos Gigantes de la Historia: Eugenio Espejo y Ernesto Che Guevara"<br/>Encuentro Latinoamericano por la Salud de los Pueblos|
| Disc inner | Same as cover |

2 CD slots, only 1 CD present

### Contents

Cleanly copied
Many small audio files, photos, flash anmiations, etc.

## 15

### Identification
| Location | Appearance |
|--|--|
| Cover | An Altenrative World Health Report <br/>Global Health Watch<br/>2005-2006|
| Disc inner | P3180519050904Y |

### Contents

Global Health Watch report from 2005-06 PDF form