---
title: Tamuku
description: 
published: true
date: 2023-02-16T06:44:28.801Z
tags: 
editor: markdown
dateCreated: 2023-02-16T06:44:28.801Z
---

# Tamuku

> Tamuku 'identifies, curates and provides' high quality & relevant information, opportunities, tools and resources for NGOs and Development Sector Professionals in India on a subscription based model. Currently, we have accumulated 2000+ subscribers who have created and amplified their impact with the help of our timely alerts and services. ~ https://www.tamuku.org/