---
title: Evolution of Primary Healthcare
description: 
published: true
date: 2023-02-17T12:27:24.755Z
tags: 
editor: markdown
dateCreated: 2023-02-17T11:14:29.978Z
---

# Evolution of Primary Healthcare

Mcaulay & Bentick reforms 

Henry Siegreist - background paper in Bhore committee. Volume 3 of Bhore committee report contains this paper.

Major General S L Bhatia history of medicine.

Alternative approaches to healthcare.

D Banerji - Health and Family Planning Services in India

British rule healthcare was limited to cantonment areas and hinterland was left to traditional healing, etc.

1947-1972 whole of the country will be covered by Primary Healthcare. 25 years to cover the whole country.

Don't close the LMP colleges - medical school (Felscher's scheme in Russia) vs medical college debate.

1972 - Srivastava report, Kartar Singh - India in PHC model siloes of various illnesses. MPW, supervisors, and linkers.

Ramalingaswamy DG of ICMR wanted to find other. Kamala Rao brings together 14 models. Calls for conference. First report is Alternative Approach in Healthcare.

Travancore Maharaja and Mysore Maharaja's role in investing in primary healthcare.

Alma Ata - Jamkhed, Banaras Hindu University (Safrulla Chaudhary, Guatemala).


Primary healthcare vs community health (as done by India).

Haefdan Maeler and D Banerji - Sociology of Tuberculosis. (National Tuberculosis Institute).

Bhore committee
Mudaliar report 
Kartar Singh committee
Srivastava report in 1974 - as a criticism 

74-86 - 5 reports came out - each one building on the other - therefore so what..

76 - First one ICMR process - 14 projects
78?? - Kenneth Newel - Health by the people - Alma Ata (Jamkhed, KN Udupa)
75 - WHO UNICEF study -  Alternative approaches to meeting basic healthcare needs in developing countries.
78 - Alma Ata - In Europe Russia and China are seen as sides, India on one random side - "Health is a right"?? - Health for All - Both sides (capitalistic and socialistic) felt that it was a compromise.
Socialist coutries interpret Alma Ata differently - capitalistic countries differently.

Term "Community health" disappears after Alma Ata and Primary healthcare comes in.

It became restricted to "primary care" - primary surgery

D Banerji
- Poverty, class and culture

Prabha Ramalingaswamy & D Banerji set up CSMCH JNU

D Banerji identifies 14 villages - 

Studies the effect of Indian health systems in the villages.

Second book on Health & Family Planning Services in India.

Banerji misses the point in the last chapter - some alternative programs - will have the same trouble as the Indian health system.

---

ICMR wrote to AIIMS - whether project 3 & 4 (Alternative approaches) could be taken over.

We need to have a policy document - ICMR & ICSSR was asked by the government of India. Health for All - the Alternative Strategy - 82

They theorized by putting together all 
- extent to which possibile to reduce poverty, inequality, and spreading education
- organize the poor and the underprivileged to fight for their human rights
- while doing it, the doctors who are stuck in the medical model - to move away from the consumerist western model and bring a model based in community

1980 to NRHM - 25 people in NRHM were consultants JH