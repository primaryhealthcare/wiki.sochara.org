---
title: Mukerji Committee
description: 
published: true
date: 2023-06-23T06:11:24.866Z
tags: 
editor: markdown
dateCreated: 2023-06-23T06:11:24.866Z
---

# Mukerji Committee

## Report

### Volume I

* http://www.nihfw.org/Doc/Reports/Mukerjee%20Committee%20Report.pdf

### Volume II

http://www.nihfw.org/Doc/Reports/Mukerji%20Committee%20Report%20%20-%20Part%20II.pdf