---
title: Public Health India Score
description: 
published: true
date: 2023-06-15T06:26:01.566Z
tags: public health india score, phin
editor: markdown
dateCreated: 2023-06-15T06:24:33.842Z
---

The **Public Health India Score** was developed by Dr. [Ravi Narayan](https://wiki.primaryhealthcare.in/index.php?title=Ravi_Narayan) of the [Centre for Public Health and Equity](https://wiki.primaryhealthcare.in/index.php?title=Centre_for_Public_Health_and_Equity) as a guide for people working in [public health](https://wiki.primaryhealthcare.in/index.php?title=Public_health) and [community health](https://wiki.primaryhealthcare.in/index.php?title=Community_health) in India. It is a compilation of key documents, reports, publications, experiments, events and initiatives that cover the most important aspects of Public Health Policy and System Development in India since Independence.

## Policy Documents

1\. [National Health Policy (1983)](https://wiki.primaryhealthcare.in/index.php?title=National_Health_Policy_(1983))

2\. [National Education Policy (1986)](https://wiki.primaryhealthcare.in/index.php?title=National_Education_Policy_(1986))

3\. [National Education Policy for Health Sciences (1989)](https://wiki.primaryhealthcare.in/index.php?title=National_Education_Policy_for_Health_Sciences_(1989))

4\. [National Population Policy (2000)](https://wiki.primaryhealthcare.in/index.php?title=National_Population_Policy_(2000))

5\. [National Health Policy (2002)](https://wiki.primaryhealthcare.in/index.php?title=National_Health_Policy_(2002))

6\. [National Policy and Programmes on Ayurveda, Yoga and Naturopathy, Unani, Siddha and Homeopathy (2002)](https://wiki.primaryhealthcare.in/index.php?title=National_Policy_and_Programmes_on_Ayurveda,_Yoga_and_Naturopathy,_Unani,_Siddha_and_Homeopathy_(2002))

7\. [National Pharmaceutical Policy (2002)](https://wiki.primaryhealthcare.in/index.php?title=National_Pharmaceutical_Policy_(2002))

8\. [Karnataka State Integrated Health Policy (2003)](https://wiki.primaryhealthcare.in/index.php?title=Karnataka_State_Integrated_Health_Policy_(2003)) [PDF](http://stg2.kar.nic.in/healthnew/PDF/STATE%20HEALTH%20POLICY.pdf)

9\. [National Rural Health Mission](https://wiki.primaryhealthcare.in/index.php?title=National_Rural_Health_Mission) - 2005-2012 [PDF](http://stg2.kar.nic.in/healthnew/PDF/NRHM%20MISSION%20DOCUMENT.pdf)

10\. [National Knowledge Commission](https://wiki.primaryhealthcare.in/index.php?title=National_Knowledge_Commission&action=edit&redlink=1)\- Report of the working group on medical education and education for community health. 2007

## Expert Committee Reports

1\. [Health Survey and Development Committee](https://wiki.primaryhealthcare.in/index.php?title=Health_Survey_and_Development_Committee) (Bhore Committee), 1946

2\. [Health Survey and Planning Committee](https://wiki.primaryhealthcare.in/index.php?title=Health_Survey_and_Planning_Committee) (Mudaliar Committee), 1961

3\. [Committee on Multi Purpose Health Workers under Health and Family Welfare Programmes](https://wiki.primaryhealthcare.in/index.php?title=Committee_on_Multi_Purpose_Health_Workers_under_Health_and_Family_Welfare_Programmes) (Kartar Singh Committee), 1973

4\. [Group on Medical Education and Support Manpower](https://wiki.primaryhealthcare.in/index.php?title=Group_on_Medical_Education_and_Support_Manpower) (Srivastava Committee), 1975

5\. [Committee on Drugs and Pharmaceutical Industry](https://wiki.primaryhealthcare.in/index.php?title=Committee_on_Drugs_and_Pharmaceutical_Industry) (Hathi Committee), 1975

6\. [Health For All: An Alternative Strategy](https://wiki.primaryhealthcare.in/index.php?title=Health_For_All:_An_Alternative_Strategy) (ICSSR- ICMR), 1981)

7\. [Expert Committee on the Public Health System](https://wiki.primaryhealthcare.in/index.php?title=Expert_Committee_on_the_Public_Health_System) (Bajaj Committee 2), 1996

8\. [National Commission on Macroeconomics and Health](https://wiki.primaryhealthcare.in/index.php?title=National_Commission_on_Macroeconomics_and_Health), 2005

9\. [National Knowledge Commission](https://wiki.primaryhealthcare.in/index.php?title=National_Knowledge_Commission&action=edit&redlink=1), 2007 [PDF](http://knowledgecommission.gov.in/downloads/documents/wg_med.pdf)

10\. [Annual Report to the People on Health](https://wiki.primaryhealthcare.in/index.php?title=Annual_Report_to_the_People_on_Health&action=edit&redlink=1) - [MOHFW](https://wiki.primaryhealthcare.in/index.php?title=MOHFW), 2010 [PDF](http://www.mohfw.nic.in/Annual%20Report%20to%20the%20People%20on%20Health%20_latest_08%20Nov%202010.pdf)

## Key monographs and reference books

1\. [Alternative Approaches To Health Care](https://wiki.primaryhealthcare.in/index.php?title=Alternative_Approaches_To_Health_Care) – [ICMR](https://wiki.primaryhealthcare.in/index.php?title=ICMR), 1976

2\. [Evaluation Of Primary Health Care Programmes](https://wiki.primaryhealthcare.in/index.php?title=Evaluation_Of_Primary_Health_Care_Programmes) – [ICMR](https://wiki.primaryhealthcare.in/index.php?title=ICMR), 1980

3\. [Appropriate Technology For Primary Health Care](https://wiki.primaryhealthcare.in/index.php?title=Appropriate_Technology_For_Primary_Health_Care) - [ICMR](https://wiki.primaryhealthcare.in/index.php?title=ICMR), 1981

4\. [Health and Family Planning Services in India](https://wiki.primaryhealthcare.in/index.php?title=Health_and_Family_Planning_Services_in_India) – [Debabar Banerji](https://wiki.primaryhealthcare.in/index.php?title=Debabar_Banerji&action=edit&redlink=1), 1985

5\. [Jamkhed](https://wiki.primaryhealthcare.in/index.php?title=Jamkhed) - [Rajnikant Arole](https://wiki.primaryhealthcare.in/index.php?title=Rajnikant_Arole&action=edit&redlink=1) and [Mabelle Arole](https://wiki.primaryhealthcare.in/index.php?title=Mabelle_Arole&action=edit&redlink=1), 1994

6\. [State of India's Health](https://wiki.primaryhealthcare.in/index.php?title=State_of_India%27s_Health) - [VHAI](https://wiki.primaryhealthcare.in/index.php?title=VHAI), 1992

7\. [Reaching India's Poor: Non-governmental Approaches to Community Health](https://wiki.primaryhealthcare.in/index.php?title=Reaching_India%27s_Poor:_Non-governmental_Approaches_to_Community_Health) - Saroj Pachauri, 1994

8\. [Report of the Independent Commission on Health in India](https://wiki.primaryhealthcare.in/index.php?title=Report_of_the_Independent_Commission_on_Health_in_India) - [VHAI](https://wiki.primaryhealthcare.in/index.php?title=VHAI), 1997

9\. Towards Equity, Quality and Integrity in Health: Report of the Task Force on Health and Family Welfare, Govt. of Karnataka, 2001 [PDF](http://www.whoindia.org/LinkFiles/Health_Sector_Reform_karna-BW.pdf)

10\. Review of Health Care in India. Leena V Gangolli et al (Ed.). CEHAT. 2005 [PDF](http://www.cehat.org/publications/PDf%20files/r51.pdf)

## Key publications of the alternative sector

1\. [Poverty, Class And Health Culture](https://wiki.primaryhealthcare.in/index.php?title=Poverty,_Class_And_Health_Culture) – Debabar Banerji, 1982

2\. [Rakku's Story: Structures of Ill-Health and the Source of Change](https://wiki.primaryhealthcare.in/index.php?title=Rakku%27s_Story:_Structures_of_Ill-Health_and_the_Source_of_Change) - Sheila Zurbrigg, 1984

3\. Development With People: Experiments with participation and non-formal education - Walter Fernandes, 1985, Indian Social Institute, Delhi

4\. [Taking Sides: The Choices Before The Health Worker](https://wiki.primaryhealthcare.in/index.php?title=Taking_Sides:_The_Choices_Before_The_Health_Worker) – [C. Sathyamala](https://wiki.primaryhealthcare.in/index.php?title=C._Sathyamala)

5\. [Health Care: Which Way To Go?](https://wiki.primaryhealthcare.in/index.php?title=Health_Care:_Which_Way_To_Go%3F) - [mfc](https://wiki.primaryhealthcare.in/index.php?title=Mfc)

6\. [Under The Lens: Health and Medicine](https://wiki.primaryhealthcare.in/index.php?title=Under_The_Lens:_Health_and_Medicine) - [mfc](https://wiki.primaryhealthcare.in/index.php?title=Mfc)

7\. [Medical Education Re-Examined](https://wiki.primaryhealthcare.in/index.php?title=Medical_Education_Re-Examined) – 1991 - [mfc](https://wiki.primaryhealthcare.in/index.php?title=Mfc)

8\. [People's Health in People's Hands](https://wiki.primaryhealthcare.in/index.php?title=People%27s_Health_in_People%27s_Hands) – N.H. Antia and Kavita Bhatia, 1993

9\. [Health For All Now! A People's Health Resource Book](https://wiki.primaryhealthcare.in/index.php?title=Health_For_All_Now!_A_People%27s_Health_Resource_Book) – [JSA](https://wiki.primaryhealthcare.in/index.php?title=JSA), 2000

10\. The Rights Approach to Health and Health Care – Policy brief, JSA [PDF](http://www.cehat.org/rthc/policybrief2.pdf)

## Projects/experiments of national public health significance

1\. Narangwal Project [PDF](http://heapol.oxfordjournals.org/content/2/2/150.full.pdf)

2\. Najargarh Project [Project write-up on Ministry website](http://www.mohfw.nic.in/kk/95/ib/95ib1701.htm)

3\. Singur Project

4\. [Gandhigram Institute of Rural Health and Family Welfare](https://wiki.primaryhealthcare.in/index.php?title=Gandhigram_Institute_of_Rural_Health_and_Family_Welfare&action=edit&redlink=1) [Official website](http://www.girhfwt.org/)

5\. [Chittaranjan Mobile Hospitals](https://wiki.primaryhealthcare.in/index.php?title=Chittaranjan_Mobile_Hospitals&action=edit&redlink=1) [No. 139 on this list](http://www.education.nic.in/cd50years/12/8i/67/8I670801.htm)

6\. [ROME Scheme](https://wiki.primaryhealthcare.in/index.php?title=ROME_Scheme&action=edit&redlink=1) ( Reorientation of Medical Education ) 1976 [Brief on ROME on cafemedico forums](http://cafemedico.net/forums/9-lecture-notes-study-topics/6527-rome-programme.html)

7\. [Expanded Programme of Immunisation](https://wiki.primaryhealthcare.in/index.php?title=Expanded_Programme_of_Immunisation&action=edit&redlink=1)

8\. [Integrated Disease Surveillance Programme](https://wiki.primaryhealthcare.in/index.php?title=Integrated_Disease_Surveillance_Programme&action=edit&redlink=1) [PDF](http://www.whoindia.org/LinkFiles/Public_Health_Laboratory_Networking_IDSP_brief.pdf)

9\. Community Health Workers – Janata Scheme 1977 [JSTOR link](http://www.jstor.org/pss/4368284)

10\. [National Rural Health Mission](https://wiki.primaryhealthcare.in/index.php?title=National_Rural_Health_Mission) [NRHM mission document on NIHFW website](http://www.nihfw.org/NDC/DocumentationServices/NationalRuralHealthMission.html)

## National institutes of public health significance

1\. [All India Institute of Hygiene and Public Health](https://wiki.primaryhealthcare.in/index.php?title=All_India_Institute_of_Hygiene_and_Public_Health), Kolkata

2\. [National Institute of Health and Family Welfare](https://wiki.primaryhealthcare.in/index.php?title=National_Institute_of_Health_and_Family_Welfare), New Delhi

3\. [Centre for Social Medicine and Community Health](https://wiki.primaryhealthcare.in/index.php?title=Centre_for_Social_Medicine_and_Community_Health), Jawaharlal Nehru University, New Delhi

4\. [National Institute of Epidemiology](https://wiki.primaryhealthcare.in/index.php?title=National_Institute_of_Epidemiology), Chennai

5\. [National Institute of Communicable Diseases](https://wiki.primaryhealthcare.in/index.php?title=National_Institute_of_Communicable_Diseases), New Delhi

6\. [National Tuberculosis Institute](https://wiki.primaryhealthcare.in/index.php?title=National_Tuberculosis_Institute), Bangalore

7\. [National Institute of Nutrition](https://wiki.primaryhealthcare.in/index.php?title=National_Institute_of_Nutrition), Hyderabad

8\. [National Institute of Malaria Research](https://wiki.primaryhealthcare.in/index.php?title=National_Institute_of_Malaria_Research), New Delhi

9\. [National Institute of Occupational Health](https://wiki.primaryhealthcare.in/index.php?title=National_Institute_of_Occupational_Health), Ahmedabad

10\. [National Health Systems Resource Centre](https://wiki.primaryhealthcare.in/index.php?title=National_Health_Systems_Resource_Centre), New Delhi

## Public health and community health resource centres and networks

1\. [Medico Friend Circle](https://wiki.primaryhealthcare.in/index.php?title=Medico_Friend_Circle)

2\. [Voluntary Health Association of India](https://wiki.primaryhealthcare.in/index.php?title=Voluntary_Health_Association_of_India), New Delhi

3\. [Catholic Health Association of India](https://wiki.primaryhealthcare.in/index.php?title=Catholic_Health_Association_of_India), Secunderabad

4\. [Christian Medical Association of India](https://wiki.primaryhealthcare.in/index.php?title=Christian_Medical_Association_of_India), New Delhi / Bangalore

5\. [Society for Community Health Awareness, Research and Action](https://wiki.primaryhealthcare.in/index.php?title=Society_for_Community_Health_Awareness,_Research_and_Action), Bangalore

6\. [Foundation for Research in Community Health](https://wiki.primaryhealthcare.in/index.php?title=Foundation_for_Research_in_Community_Health), Mumbai

7\. [Anusandhan Trust](https://wiki.primaryhealthcare.in/index.php?title=Anusandhan_Trust) ([CEHAT](https://wiki.primaryhealthcare.in/index.php?title=CEHAT), Mumbai; [SATHI](https://wiki.primaryhealthcare.in/index.php?title=SATHI), Pune and [CSER](https://wiki.primaryhealthcare.in/index.php?title=CSER&action=edit&redlink=1), Mumbai)

8\. [Jan Swasthya Rakshak](https://wiki.primaryhealthcare.in/index.php?title=Jan_Swasthya_Rakshak&action=edit&redlink=1) and MP Health Missions [Details from SOCHARA website](http://www.sochara.org/new/index.php?option=com_content&task=view&id=184&Itemid=80)

9\. [State Health Resource Centre, Chhattisgarh](https://wiki.primaryhealthcare.in/index.php?title=State_Health_Resource_Centre,_Chhattisgarh)

10\. [Jan Swasthya Abhiyan](https://wiki.primaryhealthcare.in/index.php?title=Jan_Swasthya_Abhiyan) (People’s Health Movement - India)

## Significant public health events and developments (Mainstream)

1\. Calcutta Declaration – 1999 ([http://www.ncbi.nlm.nih.gov/pmc/articles/PMC1731556/pdf/v054p00749.pdf](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC1731556/pdf/v054p00749.pdf))

2\. [Indian Public Health Standards](https://wiki.primaryhealthcare.in/index.php?title=Indian_Public_Health_Standards&action=edit&redlink=1), NRHM - 2010 ([http://www.mohfw.nic.in/NRHM/iphs.htm](http://www.mohfw.nic.in/NRHM/iphs.htm))

3\. [Public Health Foundation of India](https://wiki.primaryhealthcare.in/index.php?title=Public_Health_Foundation_of_India) - 2006

4\. [South East Asia Public Health Education Information Network](https://wiki.primaryhealthcare.in/index.php?title=South_East_Asia_Public_Health_Education_Information_Network&action=edit&redlink=1), SEARO - 2003 (www.seaphein.com/document/news/news)

5\. [National Rural Health Mission](https://wiki.primaryhealthcare.in/index.php?title=National_Rural_Health_Mission) Task Forces ([http://www.mohfw.nic.in/NRHM/Task\_grp/Tg\_index.htm](http://www.mohfw.nic.in/NRHM/Task_grp/Tg_index.htm))

6\. NRHM – Communitization Policy Initiatives ([http://www.mohfw.nic.in/NRHM/adv\_grp.htm](http://www.mohfw.nic.in/NRHM/adv_grp.htm) and [http://www.mohfw.nic.in/NRHM/asha.htm](http://www.mohfw.nic.in/NRHM/asha.htm))

7\. NRHM - ASHA Mentoring Group (AMG) and Advisory Group for Community Action (AGCA) ([http://www.nrhmcommunityaction.org/](http://www.nrhmcommunityaction.org/))

8\. National Consultation on Public Health Workforce 2009 ([http://www.whoindia.org/LinkFiles/Human\_Resources\_National\_consultation\_on\_PHWF\_Report.pdf](http://www.whoindia.org/LinkFiles/Human_Resources_National_consultation_on_PHWF_Report.pdf))

9\. Strengthening Epidemiological Principles for Public Health Action – SEARO Initiatives (towards a socio-epidemiology) 2009 ([http://www.searo.who.int/LinkFiles/CDS\_Epid\_meet\_C&R26-27feb09.pdf](http://www.searo.who.int/LinkFiles/CDS_Epid_meet_C&R26-27feb09.pdf))

10\. [Planning Commission Expert Group on Universal Access to Health Care](https://wiki.primaryhealthcare.in/index.php?title=Planning_Commission_Expert_Group_on_Universal_Access_to_Health_Care&action=edit&redlink=1) ([http://www.thehindu.com/health/policy-and-issues/article838394.ece](http://www.thehindu.com/health/policy-and-issues/article838394.ece))

## Significant public health events and developments (Civil Society)

1\. [Indian People's Health Charter (2000)](https://wiki.primaryhealthcare.in/index.php?title=Indian_People%27s_Health_Charter_(2000))

2\. [National Human Rights Commission](https://wiki.primaryhealthcare.in/index.php?title=National_Human_Rights_Commission) –Right to Health Initiative including Peoples Health Tribunals [PDF](http://www.ghwatch.org/sites/www.ghwatch.org/files/health_tribunals.pdf)

3\. [People’s Rural Health Watch](https://wiki.primaryhealthcare.in/index.php?title=People%E2%80%99s_Rural_Health_Watch&action=edit&redlink=1) [Details on PHD-India website](http://phm-india.org/index.php?option=com_content&view=article&id=4:peoples-rural-health-watch&catid=10:jsa-campaigns&Itemid=3)

4\. Peoples’ Health Manifestos for Indian national election 2004-2009 [Link to PHM website](http://www.phmovement.org/en/node/2023)

5\. [Community Health Environment Skill Shares](https://wiki.primaryhealthcare.in/index.php?title=Community_Health_Environment_Skill_Shares&action=edit&redlink=1) (CHESS) [p.5 of this document from Greenpeace India](http://www.greenpeace.org/india/Global/india/report/2003/8/status-of-human-health-at-the.doc)

6\. Community Health Fellowship Schemes in India since 2003 [Details from SOCHARA website](http://www.sochara.org/new/index.php?option=com_content&task=view&id=48&Itemid=42)

7\. [Public Health Resource Network](https://wiki.primaryhealthcare.in/index.php?title=Public_Health_Resource_Network)

8\. [Right to Food Campaign](https://wiki.primaryhealthcare.in/index.php?title=Right_to_Food_Campaign)

9\. [Right to Water Campaign](https://wiki.primaryhealthcare.in/index.php?title=Right_to_Water_Campaign&action=edit&redlink=1) [/ http://www.indiaresource.org/campaigns/coke/2008/cokeimplicatedteri.html Details from ncasindia.org](http://www.ncasindia.org/right-water-campaign)