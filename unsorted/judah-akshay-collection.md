---
title: Judah - Akshay Collection
description: Notes shared between Judah and Akshay
published: true
date: 2024-01-21T05:17:29.583Z
tags: 
editor: markdown
dateCreated: 2024-01-21T04:45:09.060Z
---

# Judah - Akshay Collection

## Books

* OPEN: How we'll work, live and learn in the future - David Price
* [Routledge Library Editions: Social and Cultural Anthropology](https://www.routledge.com/Routledge-Library-Editions-Social-and-Cultural-Anthropology/book-series/RLESCA)
* David Graeber and David Wengrow - The Dawn of Everything
* Ethics is a Daily Deal Choosing to Build Moral Strength as a Practice by Leslie E Sekerka. JP: I really love this ethics book I feel it's extremely practical and not moralizing
* The Cunning of Freedom Ryszard Legutko
* Why We Write: The Politics and Practice of Writing for Social Change - Book by Jim Downs
*  Synthesis Lectures on Information Concepts, Retrieval, and Services (13 books) 
*  The Neural Simulation Language: A System for Brain Modeling Book by Alfredo Weitzenfeld, Amanda Alexander, and Michael A. Arbib



### Medicine

* What If Medicine Disappeared? - Book by Frances B. McCrea and Gerald E. Markle
* David Wootton - bad medicine doctors doing harm since hippocrates
* An Invitation to Health Book by Dianne R. Hales JP: Found a really nice school type textbook about health in general. I felt it was a major lacunae in my premedical education


* Trick Or Treatment?: Alternative Medicine on Trial Book by Edzard Ernst and Simon Singh

### Social change

* Power and Love: A Theory and Practice of Social Change Adam Kahane
* Anarchy as Order: The History and Future of Civic Humanity - Mohammed A Bamyeh
* Self-Regulation and Human Progress How Society Gains When We Govern Less Evan Osborne
* State, anarchy and collective decisions Book by Alex Talbot Coram

* Anarchism, Organization and Management: Critical Perspectives for Students 

* World-Systems Evolution and Global Futures Power Concentration in World Politics William R Thompson
* Holacracy: The Revolutionary Management System that Abolishes Hierarchy Book by Brian J. Robertson
* Holacracy: The New Management System for a Rapidly Changing World Book by Brian J. Robertson
*  Hierarchy in the Forest Book by Christopher Boehm
* Egalitarian Politics in the Age of Globalization  Craig N Murphy
* The Mystery of the Kibbutz Egalitarian Principles in a Capitalist World
*  Redesigning Distribution:Basic Income and Stakeholder Grants as Cornerstones for an Egalitarian Capitalism
*  Democratic Hypocrisy Domination, Egalitarian Criticism and Apologetic Narratives
* Democratic Multiplicity Perceiving, Enacting, and Integrating Democratic Diversity


* An economist's exploration of what economic prerequisites have to be there to achieve social peace Pareto's Republic and the New Science of Peace 
* https://sci-hub.se/10.1023/A:1022091609047 JP: Here's a cool idea. What I take back from this and from sparsely reading your thoughts on leadership and negative feedback, and some general thoughts about utopia and psychology is that...: Humans have these preconceived notions of right and wrong, and a lot of problems are created when a person who comes into power creates a reality out of some kind of expectations. And a good proportion of those expectations may come from what this article describes - belief in a just world. I feel like people who believe in a just world may want to enact a kind of justice that is harmful and punishment-oriented. And I think that punishment doesn't really achieve anything.  
* This article as well: Why do we punish? Deterrence and just deserts as motives for punishment https://pubmed.ncbi.nlm.nih.gov/12150228/
* Contemporary Anarchist Criminology Against Authoritarianism and Punishment
* The case against punishment Book by Deirdre Golas



## Articles
* [Transformative Experiences](https://lapaul.org/papers/PPR-TE-symposium.pdf)
* https://en.m.wikipedia.org/wiki/Readers_theater
* http://www.socialcritic.org/review.htm
* https://www.edge.org/response-detail/25442
* https://www.onthecommons.org/work/introduction-commons
* https://en.wikipedia.org/wiki/Green_anarchism
* https://feminist-software.gitlab.io/commons-handbook/nourishing/
* https://80000hours.org/2012/08/how-many-lives-does-a-doctor-save/
* https://80000hours.org/2012/09/how-many-lives-does-a-doctor-save-part-2-diminishing-marginal-returns/
* https://80000hours.org/2012/09/how-many-lives-does-a-doctor-save-part-3-replacement-84/
* https://80000hours.org/career-reviews/medical-careers/
* https://www.researchgate.net/publication/340332505_Changing_Design_Education_for_the_21st_Century/fulltext/5e840aada6fdcca789e59457/Changing-Design-Education-for-the-21st-Century.pdf?origin=publication_detail
* https://unesdoc.unesco.org/ark:/48223/pf0000379707.locale=en
* https://ferosevr.medium.com/digital-librarian-for-of-the-world-9ec7cf1a239e
*  On Communication. An Interdisciplinary and Mathematical Approach 
*  Social Understanding On Hermeneutics, Geometrical Models and Artificial Intelligence
* https://pubmed.ncbi.nlm.nih.gov/34622015/
* https://theanarchistlibrary.org/library/tom-knoche-organizing-communities Found something relating to Agency
* https://theanarchistlibrary.org/library/daniel-oberhaus-meet-the-anarchists-making-their-own-medicine
* https://theanarchistlibrary.org/library/peter-gelderloos-anarchy-works
* https://theanarchistlibrary.org/library/juan-carlo-perez-cortez-relationship-anarchy
* https://theanarchistlibrary.org/library/seattle-solidarity-network-build-your-own-solidarity-network


## Podcasts

* [David Graeber - Bullshit Jobs](https://www.youtube.com/watch?v=R07TlNqz9X4)


## Links

* https://www.filmsforaction.org/
* Hey I found a cool programming learning site: https://imagilabs.com/
* https://www.goodreads.com/author/quotes/8107.Rebecca_Goldstein
* https://www.interaction-design.org/courses/design-for-the-21st-century
* https://youtu.be/ELKNL9oNv5g
* https://store.steampowered.com/app/1444480/Turing_Complete/

