---
title: Digital Humanities @ SOCHARA
description: 
published: true
date: 2025-02-24T07:12:55.919Z
tags: 
editor: markdown
dateCreated: 2023-06-15T05:58:53.518Z
---

# Digital Humanities @ SOCHARA

> "Digital humanities (DH) is an area of scholarly activity at the intersection of computing or digital technologies and the disciplines of the humanities. It includes the systematic use of digital resources in the humanities, as well as the analysis of their application. DH can be defined as new ways of doing scholarship that involve collaborative, transdisciplinary, and computationally engaged research, teaching, and publishing. It brings digital tools and methods to the study of the humanities with the recognition that the printed word is no longer the main medium for knowledge production and distribution." ~[wikipedia](https://en.wikipedia.org/wiki/Digital_humanities)

SOCHARA works towards a [paradigm shift](https://www.sochara.org/perspectives/Paradigm_Shift) that involves looking at health through its psychosocial, cultural, economic, political, and ecological dimensions, using technology for education and social processes, enabling, empowering, and building autonomy in communities, with research on socio-epidemiology, social determinants, health systems, and social policy.

This body of work that transcends traditional boundaries of disciplines when put under the lens of DH is what comes under Digital Humanities @ SOCHARA. For example:

* Maintaining a digital library
* Maintaining a digital archive
* Working on creating content in multiple Indian languages (machine translation?)
* Research on increasing accessbility to educational content for scholars who have difficulty accessing content due to language, medium, or geography.
* Modernizing content from older formats (handwritten, typewritten, printed, PDFs) into web-friendly formats.
* Maintaing a wiki for community health (the one you're reading this on).

A more detailed task list can be read below.


## Relevant Documents

* [SIMS & SJMAP concept documents](https://drive.google.com/drive/folders/183v8hoDNp-AoVJ4UShEDOexDmUwYM4sS?usp=sharing)

* [Health for All in the Internet Age](https://docs.google.com/document/d/15CeFSkc17vmPjiJol9Z4PmeIXQM-mjcCCLNyt0MQleU/edit?usp=drive_link) - a theory of change.

* [Digital Archives Project - Objectives and outcomes](https://docs.google.com/document/d/15gYfSNInqXxw9HgusBiQty57VldmeTWKhXU-scokgvE/edit?usp=sharing)

* [Community for Online Action in Community Health](https://docs.google.com/document/d/1ETCC_pCgYkXsWeOEKV7dcR8CW3mJihHuGeaOUBZgvwU/edit?usp=sharing)

* [Annual Reports](https://docs.google.com/document/d/1icVVPfu4IZK8RgQwgVL9wZAuuoj1HEaX1DaMsGjnc2g/edit)

* [Half yearly progress reports](https://docs.google.com/document/d/1aVFVkJebRzJ6wAUtItCYW8VTYCu-v_7gEnirJqCVKsE/edit)

* [Presentation on SUDHA](https://docs.google.com/presentation/d/1zY4llU9prnU_Osdy796H_fjw9GcHAYl0ZuzoF070tYs/edit#slide=id.g2c6d9643cda_0_52) and [video](https://www.youtube.com/watch?v=g90FWGpTQ-8&feature=youtu.be)

* [[Draft] SOCHARA Extensive Learning Strategy 2024-25](https://docs.google.com/document/d/1ZJZzHQvQrSLO5LkUefPPIqDFmMkewnwxpIOBjIe_k2U/edit?usp=sharing)

## Volunteer

You are welcome to volunteer towards this effort where we're chipping away every day. We can structure your relationship with SOCHARA in various forms (including internships and fellowships). Please contact [akshay@sochara.org](mailto:akshay@sochara.org). You can also use call/message/whatsapp/telegram/signal ASD on [8123148720](tel:8123148720). You could also directly join the [GLAMWW workgroup on whatsapp](https://chat.whatsapp.com/KxfY0pZhOig8ufUcdzHvAy) - please introduce yourself.

## Tasks List

### Popularization of digitized content

There are various groups which are potential users/consumers of the digitized content:
* whatsapp groups
* email groups
* instagram
* twitter
* newsletter

We need to:
* Create targeted, catchy communication for various audiences.
* Distribute these through these groups.

For example,

* Take a front page photo of a book
* Write a small paragraph about it in the caption that makes the audience want to read the book
* Add a web link to the resource

### Archives curation

The SOCHARA archives is getting digitized and uploaded online at:
* [archives.sochara.org](https://archives.sochara.org), and
* [archive.org](https://archive.org/details/ServantsOfKnowledge?query=sochara.*&sort=-addeddate)

The task is to go through these and add relevant metadata and curate these resources into accessible wiki pages on [wiki.sochara.org](https://wiki.sochara.org)

### Community health chatbot

Use the various content we're making available in digital form to train/fine-tune a language model that can give intelligent answers in the topic of community health.

### mfc bulletins modernization
* mfc has published bulletins from many years. These are in PDF forms that are hard to share and read: https://mfcindia.org/bulletins/
* We have already extracted text and created separate page for each article under this wiki itself (check mfc-bulletins folder in the navigation).
* Now we need to clean up each article. Checkout a sample here: https://wiki.sochara.org/en/mfc-bulletins/302-303/communal-politics--a-threat-to-democracy
* Please request access to https://docs.google.com/spreadsheets/d/1gUdS2y8hwktbcHz8zbwvuRqT9RR8HdbSc9zbHK7bGdI/edit?usp=sharing and start editing. Update the column "Cleaned By" after you clean up a page. If you change the wiki URL, please change the wiki_link column also.
* If you are reviewing a page, update the column "Reviewed By".

**What does cleaning mean?**
- Removing articles before and after the required article.
- Ensuring paragraphs are separated neatly and there's no unnecessary paragraph breaks within a paragraph.
- Correcting spelling mistakes and OCR errors.
- Uploading images from the original PDF.
- Ensuring the author names, title, and month of publication is present.
- Handling any special conditions not covered above by discussing with the team.

### sochara.org migration to wordpress

* Set up a wordpress instance
* Recreate page structure
* Migrate content
* Redirect old links to new links


### Transcription-Translation of CHLP/MPH videos
* Select a video
* Download CC generated by vimeo
* Convert to your language
* Upload back

### Metadata for archives

* Work with the team to appropriately tag and add metadata to items

### Community health reading list

* Clean up formatting in [the reading list](https://wiki.sochara.org/en/community-health-reading-list)
* Create a communication asking people to review and suggest additions
* Follow-up with people and make updates on the reading list.

### Disability related content

* Work on the disability page (coordinate with Karthik)

### Caste related content

#### Institutional murders
* Create a wiki page for every student who was killed in institutional murders related to caste (otherwise known as suicide).
* Have an umbrella page that links to all the pages.


### Cataloging CDs and older audio-video stuff

This can only be done offline at the Koramangala office (location: [osm](https://www.openstreetmap.org/#map=19/12.92890/77.63665), [google](https://maps.app.goo.gl/SxFeX4xCJKjHWu2x7))

What needs to be done:
1. Take a recording (CD or VCD, etc) that's not yet catalogued
2. Put it in the player
3. See what's playing
4. Record in the catalogue
5. Repeat 1-4 till all the CDs are done.

We want to do this towards enabling the [Fifth People's Health Assembly](https://phmovement.org/pha5) in making an archive.

### Archiving mfc's email group

[mfc](https://mfcindia.org) has a very active email group which was initially on yahoo groups (actually it was on VSNL or something before that, but don't even ask me that), and then on google groups. We have an mbox format backup of the yahoo groups. How to get this online with sufficient safeguards of privacy (this is a closed group and we wouldn't want to make it public without everyone's consent) is the task.

This will require programming skills as I am not sure if there's any readmyade software that does this kind of "born digital" content preservation for mailing list archives.


### Oral history 

There are a large number of individuals in our circle who have unique stories that need to be captured.

You need to be:
* Someone who can either travel to these people's locations or get them to speak online and record these.
* Someone to read about the people and create a set of intelligent

### Disaster response related collection

We have a collection of content related to disaster relief and other responses (Bhopal, Andhra cyclone, Tsunami, and so many other disasters). Someone needs to go through and curate a proper collection.

**Priyanka Kalliath** is doing this at the moment.

### Pluralism related collection

We have a collection of content related to medical pluralism. Someone needs to go through and curate a proper collection.

### Coordinating tasks

There are several people who have the inclination to do some of these tasks, but need regular follow-up and reminders (management). If you can take this up, that's also great help.


## How to volunteer

See [the volunteer section above](#volunteer)