---
title: SOCHARA Wiki
description: The home page of SOCHARA's wiki on everything related to community health
published: true
date: 2024-06-15T15:53:58.670Z
tags: 
editor: markdown
dateCreated: 2022-12-30T11:50:33.679Z
---

Hello fellow traveler in this journey towards health for all. This is a place for you to pause, reflect, and replenish yourself!

This is a wiki, which means you're welcome to edit it and make it better. For creating an account, if you're facing any trouble, or if you want more information, please contact akshay@sochara.org .

You can use the search button above to find articles on this wiki. You can also see the list of articles on the left side (or by clicking the menu icon in bottom left corner if you're on a mobile device)