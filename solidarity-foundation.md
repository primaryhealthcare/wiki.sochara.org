---
title: Solidarity Foundation
description: 
published: true
date: 2023-09-21T10:56:11.820Z
tags: 
editor: markdown
dateCreated: 2023-09-21T10:56:11.820Z
---

# Solidarity Foundation

A registered trust based in India supporting sex workers and gender and sexual minorities. SF does this through building of grassroots collectives and leadership, livelihood initiatives, and spotlighting overlooked issues.

### External Links
* https://www.solidarityfoundation.in/